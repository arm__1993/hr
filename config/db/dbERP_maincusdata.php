<?php

return [
        'class' => 'yii\db\Connection',
        'dsn' => "mysql:host=$DB_HOST;dbname=".$params['dbConn']['ERP_maincusdata']."",
        'username' => $DB_USER,
        'password' => $DB_PWD,
		'charset' => 'utf8',
		];