-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 28, 2017 at 01:50 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_mapping`
--

CREATE TABLE `tax_income_mapping` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'รหัส',
  `tax_income_section_id` tinyint(3) NOT NULL COMMENT 'รหัสภาษีมาตรา 40',
  `add_deduct_template_id` tinyint(3) NOT NULL COMMENT 'รหัสรายการเพิ่ม',
  `tax_rate` decimal(4,2) DEFAULT NULL COMMENT 'อัตราภาษี',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_income_mapping`
--
ALTER TABLE `tax_income_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_income_section_id` (`tax_income_section_id`),
  ADD KEY `add_deduct_template_id` (`add_deduct_template_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_income_mapping`
--
ALTER TABLE `tax_income_mapping`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
