<?php

namespace app\modules\wscommondataold;

/**
 * wscommondataold module definition class
 */
class Wscommondataold extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\wscommondataold\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
