<?php

namespace app\modules\wscommon\models;

use Yii;

class Maincusdatajobtable extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'MainCusData_JobTable';
    }
    /**
     * @inheritdoc
     */
       /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SegmentGroup', 'TIS_ID_REF', 'TYPE_OF_CUS', 'Job_Old', 'status'], 'required'],
            [['SegmentGroup', 'TIS_ID_REF', 'TYPE_OF_CUS', 'Job_Old'], 'string', 'max' => 150],
            [['Job'], 'string', 'max' => 50],
            [['Job_Describe'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JobID' => 'Job ID',
            'SegmentGroup' => 'กลุ่มอาชีพ',
            'Job' => 'อาชีพ',
            'Job_Describe' => 'คำอธิบายอาชีพ',
            'TIS_ID_REF' => 'รหัสอ้างอิง TIS',
            'TYPE_OF_CUS' => 'ประเภทลูกค้า 1=บุคคล,2=ชมรม / สมาคม / นิติบุคล',
            'Job_Old' => 'old value',
            'status' => '99=ยกเลิกใช้',
        ];
    }

    public function selectjobbytypeofcus($keyword) // param
    {   
        if($keyword == 1){
            $sql = "select * from MainCusData_JobTable  where TYPE_OF_CUS = 1 ORDER BY Job ASC" ;
        }
        else if($keyword == 2){
            $sql = "select * from MainCusData_JobTable  where TYPE_OF_CUS = 2 ORDER BY Job ASC" ;
        }else{
            $sql = "select * from MainCusData_JobTable  ORDER BY Job ASC" ;
        }


        $job = Yii::$app->dbERP_maincusdata
       ->createCommand($sql)
       ->queryAll();

        return $job ;
    }
}
