<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:26
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;



$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/level.js?t='.time(), ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li class="active">ระดับ</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                    
                    </div>
                    <div class="col-md-2">
                        <a data-toggle="modal" onclick="reset()"><img src="<?php echo $imghr; ?>/add.png" class="img-circle">
                                                <span>เพิ่มระดับ</span></a>
                    </div>
                </div>
            <?php {
                Pjax::begin(['id' => 'pjax_grid_level']);
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                    'columns' => [
                            ['class' => 'yii\grid\SerialColumn', 
                                'headerOptions' => ['width' => '20'],
                            ],
                            [ 
                            'attribute' => 'level_code', 
                            'label' => 'รหัสระดับ', 
                            'value' => 'level_code', 
                            //'format' => 'raw', 
                            //'filter' => true, 
                            'contentOptions' => ['style' => 'width: 120px;','align=center'] 
                            ],
                            [ 
                            'attribute' => 'level_name', 
                            'label' => 'ชื่อระดับ',
                            'value' => 'level_name', 
                            //'format' => 'raw', 
                            //'filter' => true, 
                            'contentOptions' => ['style' => 'width: 120px;','align=center'] 
                            ],
                                [
                                'headerOptions' => ['width' => '100'],
                                'class' => 'yii\grid\ActionColumn',
                                'header' => 'จัดการระดับ',
                                'template' => '{update}{delete}',
                                'buttons' => [
                                    'update' => function ($url, $data)  use ($arrExistLevel){
                                        if(!in_array($data->level_id,$arrExistLevel)) {
                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                'title' => 'แก้ไข',
                                                'onclick' => '(function($event) {
                                                    editLevel(' . $data->level_id . ');
                                            })();'
                                            ]);
                                        }

                                    },

                                    'delete' => function ($url, $data) use ($arrExistLevel) {
                                        if(!in_array($data->level_id,$arrExistLevel)) {
                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                'title' => 'ลบ',
                                                'onclick' => '(function($event) {
                                                    bootbox.confirm({
                                                        size: "small",
                                                        message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->level_id . '? </h4>",
                                                        callback: function(result){
                                                            if(result==1) {
                                                                deleteLevelByid(' . $data->level_id . ');
                                                            }
                                                        }
                                                    });
                                            
                                            
                                            })();'
                                            ]);
                                        }
                                    },
                                ],
                            ],
                    ],
                ]); 
                Pjax::end(); //end pjax_gridcorclub
            }?>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
<div class="modal fade" id="modal-level" tabindex="-1" role="dialog" aria-labelledby="ConfirmDialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ยืนยันการบันทึก</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="AddLevelForm"  data-toggle="validator" onsubmit="return validsubmit();" method="post" class="form-horizontal">
                     <!--<input class="form-control" id="company" value="<?php echo $datacompany[0]['id']; ?>" type="hidden" name="company"/>-->
                     <input value="0" type="hidden" id="check" name="check"/>
                     <input value="0" type="hidden" id="id" name="id"/>
                    <div class="form-group">
                        <div class="row">
                           <div class="col-md-3" style="text-align:right">ชื่อระดับ <i style="color:#ff0000">*</i></div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="level_name"  name="level_name" required/>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3"  style="text-align:right">รหัสระดับ <i style="color:#ff0000">*</i></div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="level_code" maxlength="2" onkeypress="return onlyNumber(event);" name="level_code" required/>
                                <input type="hidden" class="form-control" id="level_status"  name="level_status" value="1" />
                            </div>
                        </div>
                    </div>
                     <br>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6"><u>ตัวอย่าง</u> 2 ใส่เป็นตัวเลขเท่านั้น</div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
