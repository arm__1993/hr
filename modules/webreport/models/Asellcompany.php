<?php

namespace app\modules\webreport\models;

use Yii;

/**
 * This is the model class for table "asell_company".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $emp_id
 * @property string $add_time
 * @property string $status
 * @property integer $idRef_vihicle
 */
class Asellcompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'working_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'emp_id', 'add_time', 'status', 'idRef_vihicle'], 'required'],
            [['address'], 'string'],
            [['idRef_vihicle'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['emp_id'], 'string', 'max' => 13],
            [['add_time'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'emp_id' => 'Emp ID',
            'add_time' => 'Add Time',
            'status' => 'Status',
            'idRef_vihicle' => 'Id Ref Vihicle',
        ];
    }

   
}
