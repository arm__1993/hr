<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Maincusdata;
//use app\modules\wscommon\models\Testmaincus;
use yii\helpers\ArrayHelper;


class DatamaincusdataController extends ActiveController
{


   	public $modelClass = 'app\modules\wscommon\models\Maincusdata';
	//public $modelClass = 'app\modules\wscommon\models\Testmaincus';

	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}

	public function actionListcustomer()
	{
		$showdatalimit10 = Maincusdata::find()->limit(5)->all();
    	return $showdatalimit10;
	}


	public function actionSearchcustomerdataforsalemobile()
    {	

        $c = \Yii::$app->request->get('c');
        $s = \Yii::$app->request->get('s');
        $i = \Yii::$app->request->get('i');
        $customerdata = new Maincusdata();
        $customerdataSearch = $customerdata->searchcustomerdataforsalemobile($c,$s,$i );
        return $customerdataSearch;
    }


}
        //$request = Yii::$app->request;
        //$get = $request->get();
        // equivalent to: $get = $_GET;
     //    $c = 'นรินทร์';
     //    $s = 'มูลธิ';
     //    //$c = \Yii::$app->request->get('c');
        // $cusmaster = Maincusdata::find()->where(
        //              [
        //              //Cus_Name LIKE '%'.$c.'%' AND Cus_Surename LIKE '%'.$s.'%'
        //              'like', 'Cus_Name', $c,
        //              'like', 'Cus_Surename', $s,
                    //  ])
        // ->all();
        // //$customerdataSearch = $cusmaster
        // return $cusmaster;


        // $c = \Yii::$app->request->get('c');
        // $s = \Yii::$app->request->get('s');
        // $i = \Yii::$app->request->get('i');
        // $customerdata = new Maincusdata();
        // $customerdataSearch = $customerdata->searchcustomerdataforsalemobile($c,$s,$i );
        // return $customerdataSearch;

