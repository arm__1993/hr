<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/10/2559
 * Time: 17:21
 */

namespace app\modules\webreport\apiwebreport;

use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiReport
{
    // ARRAY_WAGE_GROUP_CHECK_DISTANCE เช็คระยะ //
    const ARRAY_WAGE_GROUP_CHECK_DISTANCE = [
        '00' => ['1020','1030','1040','1050','1060','1070','1072','1100','2062','9910'] ,
        '01' => ['1090','1131','1161','1151','1017','1170','1171','4160','4162','1140','1133'],
        '02' => ['1030'],
        '03' => ['1061','1062'],
        '06' => ['1164'],
        '08' => ['1130','1150'],
        '09' => ['1290','1292','1163','1209','1210','1211','1065','1064',
                 '1212','1213','1214','1603','1604','1002','1026','1051',
                 '1502','1503','1504','1505','1506','1513','1514','1300'],
        '10' => ['1124'],
        '11' => ['9801'],
        '99' => ['1111','A400','A600','0005']
        ];
    // ARRAY_WAGE_GROUP_REPAIR_GENERAL ซ่อมทั่วไป //
    const ARRAY_WAGE_GROUP_REPAIR_GENERAL = [
        '00' => ['1112','1081','C001'] ,
        '01' => ['C006'],
        '11' => ['A001'],
        '99' => ['1140','1152','0005']
    ];



    /* สถานะการซ่อมรถ
       สถานะรวม ดูใน cvhc_main
       สถานะย่อย ดูใน status_time
    */
    const REPAIR_STATUS = [
        '0'  => 'ยามรับรถ',
        '1'  => 'sa ตรวจสภาพรถ',
        '2'  => 'sa รอแจ้งซ่อมกับลูกค้า(หลังจากตรวจสภาพรถเสร็จ)',
        '4'  => 'เช็คเกอร์รอจ่ายงานให้ช่าง',
        '5'  => 'ช่างกำลังซ่อม',
        '6'  => 'ช่างหยุดงานซ่อม',
        '7'  => 'ช่างเริ่มงานซ่อมต่อ',
        '8'  => 'หยุดงานเพื่อเสนองานซ่อมเพิ่ม',
        '9'  => 'อนุมัติงานที่ช่างเสนองานซ่อมเพิ่ม',
        '10' => 'นำรถออกไปทดสอบ',
        '11' => 'นำรถที่ไปทดสอบกลับมา',
        '12' => 'ซ่อมต่อ',
        '13' => 'ส่งงานต่อ',
        '14' => 'นำรถไปซ่อมนอกศูนย์',
        '15' => 'ปิดงานซ่อมรอล้างรถ',
        '16' => 'sa เช็คสภาพหลังจากล้างรถเรียบร้อยแล้ว',
        '17' => 'sa ติดต่อลูกค้ามารับรถ',
        '18' => 'รอลูกค้ามารับรถ',
        '19' => 'ลูกค้ามารับรถและรอคิวจ่ายเงิน / รับคิวจ่ายเงินแล้ว ',
        '20' => 'ลูกค้าจ่ายเงินพร้อมรับรถเรียบร้อย(ยังไม่รับรถ)',
        '21' => 'รอช่างรับงาน',
        '22' => 'sa ระหว่างแจ้งซ่อม',
        '23' => 'ปิดงานซ่อม รอsaตรวจสภาพรถ กรณีไม่ล้างรถ',
        '24' => 'นำรถที่ออกไปซ่อมข้างนอก กลับมาแล้ว',
        '25' => 'ลูกค้ารับรถเรียบร้อย',
        '26' => 'ลูกค้านำรถออกบริษัทแล้ว',
        '94' => 'รอชำระเงิน Mobile',
        '95' => 'กำลังแจ้งซ่อม Mobile',
        '96' => 'รอแจ้งซ่อม Mobile ',
        '97' => '"ย้อนสถานะจากการเงิน'
    ];




    public static function getMonthString()
    {
        return  ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
    }

    public static function getAllCompay()
    {
        $model = Yii::$app->dbERP_easyhr_OU
            ->createCommand("SELECT id,name FROM `working_company` WHERE `status` = 1 ")
            ->queryAll();
        $arrCompany = [];
        foreach ($model as $item) {
            $arrCompany[$item['id']] = $item['name'];
        }
        return $arrCompany;
    }

    public static function getAppYear()
    {
        $years = Yii::$app->dbERP_service
            ->createCommand("SELECT DISTINCT SUBSTR(`datePay`,1,4) as years FROM ytax WHERE datePay != 0000 GROUP BY years DESC")
            ->queryAll();
        $arrYear=[];
        foreach ($years as $item) {
            $arrYear[$item['years']] = $item['years'];
        }
        return $arrYear;
    }


    public static function getNameCompany()
    {

        $company = Yii::$app->dbERP_easyhr_OU
            ->createCommand("SELECT name FROM `working_company` WHERE `status` = 1 ")
            ->queryAll();
        $years = Yii::$app->dbERP_service
            ->createCommand("SELECT DISTINCT SUBSTR(`datePay`,1,4) as years FROM ytax WHERE datePay != 0000 GROUP BY years DESC")
            ->queryAll();


        return [
            'get_year' => $years,
            'get_company' => $company,
        ];

    }



    public static function getNameTechnician()
    {
        $name = Yii::$app->dbERP_service
            ->createCommand("SELECT technician_id, technician_name as name FROM erepair_send_all_mechanic GROUP BY technician_id ORDER BY name ASC")
            ->queryAll();
        $numname = count($name);
        return [
            'name' => $name,
            'numname' => $numname,
        ];
    }


    public static function getAllTechnicianSV()
    {
        $model = Yii::$app->dbERP_service
            ->createCommand("SELECT technician_id, technician_name FROM erepair_send_all_mechanic GROUP BY technician_id ORDER BY technician_name ASC")
            ->queryAll();
        $arrTechnician = [];
        foreach ($model as $item) {
            $arrTechnician[$item['technician_id']] = $item['technician_name'];
        }
        return $arrTechnician;
    }


    public static function printobj($obj)
    {
        echo '<pre>';
        print_r($obj);
        echo '</pre>';
    }

    public static function buildArrayWage($arrAllTechnician, $arrName, $arrTime)
    {
        $arrTechnicianTotal = [];
        foreach ($arrAllTechnician as $id_card) {
            if (array_key_exists($id_card, $arrTime)) {
                $arrTechnicianTotal[$id_card]['time'] = $arrTime[$id_card][0];
                $arrTechnicianTotal[$id_card]['wage'] = $arrTime[$id_card][1];
                if (array_key_exists($id_card, $arrName)) {
                    $arrTechnicianTotal[$id_card]['fullname'] = $arrName[$id_card][0];
                }
            }

        }
        return $arrTechnicianTotal;
        //self::printobj($arrTechnicianTotal);
    }

    public static function getAllTechnician()
    {
        $arrAllTechnician = [];
        $sql = "SELECT position_id ,`id_card`,  NameUse 
                    FROM `relation_position` 
                    WHERE `status` = 1 
                    AND position_id IN ( 
                    SELECT id FROM `position` WHERE `WorkType` LIKE  '%3%'  AND `Status` = 1) ";
        $model = Yii::$app->dbERP_easyhr_OU->createCommand($sql)->queryAll();
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        } else {
            foreach ($model as $item) {
                //echo $item['id_card'];
                $arrAllTechnician[$item['id_card']] = $item['id_card'];
            }
        }
        return $arrAllTechnician;
    }

    public static function getTypeTechnician($company, $typeTechnician)//function Car_repairman
    {
        $arrTechincian = [];
        $str_NameCar = "'%" . $typeTechnician . "%'";
        //$str_NameCar =  '%ช่างรถใหญ่%' , '%ช่างรถเล็ก%'

        $sql = "SELECT WorkCompany,Name,id_card,NameUse 
                    FROM ERP_easyhr_OU.position as a inner join relation_position as b on b.position_id = a.id 
                    WHERE a.WorkCompany = $company 
                    AND a.WorkType = 3
                    AND a.Name LIKE $str_NameCar 
                    AND a.Status = 1 and b.status = 1 group by id_card";
        $model = Yii::$app->dbERP_easyhr_OU->createCommand($sql)->queryAll();

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        } else {
            foreach ($model as $item) {
                $arrTechincian[] = $item;
            }
        }
        return [
            'techincian' => $arrTechincian,
        ];
    }

    public static function TypeCompany($company)
    {
        $str_company = "'" . $company . "'";
        $sql = "SELECT id, name FROM `working_company` WHERE `status` != 99 and name = $str_company";
        $model = Yii::$app->dbERP_easyhr_OU
            ->createCommand($sql)->queryAll();

        foreach ($model as $item) {
            $typeCompany = $item['id'];
        }

        if (!empty($typeCompany)) {
            $typeCompany;
        } elseif (empty($typeCompany)) {
            $typeCompany = null;
        }
        return $typeCompany;

    }


    public static function getNameCompanyPDF($id_company)
    {
        $str_company = "'" . $id_company . "'";
        $sql = "SELECT id, name FROM `working_company` WHERE `status` != 99 and id = $str_company";
        return $model = Yii::$app->dbERP_easyhr_OU
            ->createCommand($sql)->queryAll();
    }

    public static function CalMinute($time)
    {
        //$time = array("00:19:00", "00:02:00", "04:22:00", "00:51:00") or array(H:i:s);;
        $h = 0;
        $m = 0;
        foreach ($time as $value) {
            $checkTime = explode(":", $value);
            $h = $h + ($checkTime[0] * 60); //ชั่วโฒง * 60 ให่เป็นนาที
            $m = $m + $checkTime[1]; //นาที
        }
        $CalMinute = ($h + $m);
        return $CalMinute;
    }

    public static function statusA1($type_company, $StartDate, $EndDate)
    {
        //$type_company = รหัสบริษัท 
        //$StartDate = วันที่เริ่มต้น (เป็น strtotime) 
        //$EndDate = วันที่สิ้นสุด (เป็น strtotime) 
        $sql_A1 = "SELECT a.id, a.guard_time, a.sa_str_cktime, DATE_FORMAT(FROM_UNIXTIME(a.sa_str_cktime),'%Y-%m-%d %H:%i:%s') 
        as time_str_cktime, DATE_FORMAT(FROM_UNIXTIME(a.guard_time),'%Y-%m-%d %H:%i:%s') 
        as time_guard_time, TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(a.sa_str_cktime),'%Y-%m-%d %H:%i:%s'), 
        DATE_FORMAT(FROM_UNIXTIME(a.guard_time),'%Y-%m-%d %H:%i:%s')) as time_cal FROM cvhc_main as a 
        WHERE a.status not in ('27','99') and brach_id = '$type_company' and a.guard_time 
        between '$StartDate' and '$EndDate' 
        and TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(a.sa_str_cktime),'%Y-%m-%d %H:%i:%s'),
        DATE_FORMAT(FROM_UNIXTIME(a.guard_time),'%Y-%m-%d %H:%i:%s'))> '00:05:00'";

        echo $sql_A1;
        exit;

        $A1 = Yii::$app->dbERP_service->createCommand($sql_A1)->queryAll();
        foreach ($A1 as $value) {
            $totalA1[$value['id']]["status"] = "A1";
            $totalA1[$value['id']]["sa_str_cktime"] = $value["sa_str_cktime"];
            $totalA1[$value['id']]["time_str_cktime"] = $value["time_str_cktime"];
        }
        if (empty($totalA1)) {
            $totalA1 = null;
        }
        return $totalA1;
    }

    public static function statusB1($type_company, $StartDate, $EndDate)
    {
        //$type_company = รหัสบริษัท 
        //$StartDate = วันที่เริ่มต้น (เป็น strtotime) 
        //$EndDate = วันที่สิ้นสุด (เป็น strtotime) 
        $sql_B1 = "SELECT cvhc_main.id, erepair_send_all_mechanic.start_time_job,drepair_inform.start_time, 
        DATE_FORMAT(FROM_UNIXTIME(erepair_send_all_mechanic.start_time_job),'%Y-%m-%d %T') as start_job , 
        DATE_FORMAT(FROM_UNIXTIME(drepair_inform.start_time),'%Y-%m-%d %T') as start_time , 
        TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(erepair_send_all_mechanic.start_time_job),'%Y-%m-%d %T'), 
        DATE_FORMAT(FROM_UNIXTIME(drepair_inform.start_time),'%Y-%m-%d %T')) as time_cal 
        FROM cvhc_main inner join erepair_send_all_mechanic on erepair_send_all_mechanic.cvhc_main_id = cvhc_main.id 
        inner join drepair_inform on drepair_inform.cvhc_main_id = cvhc_main.id where cvhc_main.brach_id = '$type_company' 
        and cvhc_main.status <> 27 and cvhc_main.status <> 99 and cvhc_main.guard_time 
        between $StartDate and $EndDate and TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(erepair_send_all_mechanic.start_time_job),'%Y-%m-%d %T'), 
        DATE_FORMAT(FROM_UNIXTIME(drepair_inform.start_time),'%Y-%m-%d %T'))> '00:05:00'";
        $B1 = Yii::$app->db->createCommand($sql_B1)->queryAll();
        foreach ($B1 as $value) {
            $totalB1[$value['id']]["status"] = "B1";
            $totalB1[$value['id']]["start_time_job"] = $value["start_time_job"];
            $totalB1[$value['id']]["start_job"] = $value["start_job"];
        }
        if (empty($totalB1)) {
            $totalB1 = null;
        }
        return $totalB1;
    }

    public static function statusC1($type_company, $StartDate, $EndDate)
    {
        //$type_company = รหัสบริษัท 
        //$StartDate = วันที่เริ่มต้น (เป็น strtotime) 
        //$EndDate = วันที่สิ้นสุด (เป็น strtotime) 
        $sql_C1 = "SELECT cvhc_main.id,cvhc_main.time_warn,erepair_send.time_stop , 
        DATE_FORMAT(FROM_UNIXTIME(cvhc_main.time_warn),'%Y-%m-%d %T') as time_warn_start , 
        DATE_FORMAT(FROM_UNIXTIME(erepair_send.time_stop ),'%Y-%m-%d %T') as time_stop ,
        TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(cvhc_main.time_warn),'%Y-%m-%d %T'), 
        DATE_FORMAT(FROM_UNIXTIME(erepair_send.time_stop ),'%Y-%m-%d %T')) as time_cal 
        FROM cvhc_main inner join erepair_send on erepair_send.cvhc_main_id = cvhc_main.id 
        where cvhc_main.brach_id = '$type_company' and cvhc_main.status <> 27 and cvhc_main.status <> 99 
        and cvhc_main.guard_time between $StartDate and $EndDate 
        and TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(cvhc_main.time_warn),'%Y-%m-%d %T'), 
        DATE_FORMAT(FROM_UNIXTIME(erepair_send.time_stop ),'%Y-%m-%d %T')) > '00:15:00'";
        $C1 = Yii::$app->db->createCommand($sql_C1)->queryAll();
        foreach ($C1 as $value) {
            $totalC1[$value['id']]["status"] = "C1";
            $totalC1[$value['id']]["time_warn"] = $value["time_warn"];
            $totalC1[$value['id']]["time_warn_start"] = $value["time_warn_start"];
        }
        if (empty($totalC1)) {
            $totalC1 = null;
        }
        return $totalC1;
    }

}