-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2017 at 09:02 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_calculate`
--

CREATE TABLE `tax_calculate` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `emp_idcard` varchar(13) NOT NULL COMMENT 'รหัสบัตรประชาชนพนักงานจ่ายภาษี',
  `months` tinyint(3) NOT NULL COMMENT 'เดือน',
  `years` int(11) NOT NULL COMMENT 'ปี',
  `for_month` varchar(12) NOT NULL COMMENT 'รอบเดือนปีจ่ายเงินเดือน',
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_tax_code` varchar(30) DEFAULT NULL,
  `position_code` varchar(30) DEFAULT NULL,
  `position_name` varchar(200) DEFAULT NULL,
  `salary_wage_monthly` decimal(10,2) NOT NULL COMMENT 'เงินเดือน',
  `salary_wage_other` decimal(10,2) DEFAULT NULL COMMENT 'รายได้อื่นๆ 40(2)(8)',
  `total_monthly` decimal(10,2) DEFAULT NULL COMMENT 'เงินเดือนคำนวณเสมือนจ่ายทุกเดือน',
  `total_yearly` decimal(10,2) DEFAULT NULL COMMENT 'รายได้ต่อเสมือนได้รับทั้งปี',
  `total_deduction` decimal(10,2) DEFAULT NULL COMMENT 'รายการลดหย่อนรวม',
  `total_income` decimal(10,2) DEFAULT NULL COMMENT 'เงินได้สุทธิ',
  `total_tax_year` decimal(10,2) DEFAULT NULL COMMENT 'ยอดภาษีทั้งปี',
  `total_tax_month` decimal(10,2) DEFAULT NULL COMMENT 'ยอดภาษีรายเดือน',
  `createby_user` char(13) NOT NULL COMMENT 'เลขประจำตัวผู้สร้าง',
  `create_datetime` datetime NOT NULL COMMENT 'สร้างรายการเมื่อวันที่เวลา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ตารางจัดเก็บข้อมูลภาษีเงินได้รายเดือน';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_calculate`
--
ALTER TABLE `tax_calculate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_idcard` (`emp_idcard`),
  ADD KEY `months` (`months`),
  ADD KEY `years` (`years`),
  ADD KEY `for_month` (`for_month`),
  ADD KEY `createby_user` (`createby_user`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `company_tax_code` (`company_tax_code`),
  ADD KEY `position_code` (`position_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_calculate`
--
ALTER TABLE `tax_calculate`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
