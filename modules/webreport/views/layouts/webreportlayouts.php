<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use app\api\Common;

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

/*** DEFAULT ROUTING **/
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);




$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #btnLogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='logout';
                }
            }
          });
    });

});
JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';





//store action ID, wait to apply permission and left menu
//=======================================================
$arrActionID = [
    'menu1'=>['wage','pfm_mechanic'],
    'menu2'=>['pfm_repairman_day','pfm_repairman_month'],
    'menu3'=>['number_of_car_repair'],
    'menu4'=>['time_repair','order_repair'],
    'menu5'=>['sa_repair_day','sa_repair_month','avg_repair','sa_repair_report','avg_repair_report'],
    'menu6'=>['pb_repair_overtime','statistics_outsize_day','statistics_outsize_month'],
    'menu7'=>['loop_services'],
    'menu8'=>['repair_backlogs'],
    'menu9'=>['profit_lost'],
];

$arrCtrlName = ['default','backlogscar','duringrepair','number','personal','receivewage','repair','services','spares','time'];

//=======================================================

//Get Current Action ID
$CurrActionID = Yii::$app->controller->action->id;


$full_name = "อดิเทพ ไชยสาร";


?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title>EasyService :: Report phase I</title>
        <?php $this->head() ?>

    </head>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->homeUrl;?>/css/webreport/webreport.css">
    <body class="hold-transition skin-blue-light sidebar-mini">
    <?php $this->beginBody() ?>
    <!-- Site wrapper -->

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo SITE_URL;?>/default" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini logo_text"><b>E</b>SV</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg logo_text"><b>Easy</b>Service</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $img; ?>/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $full_name; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo $img; ?>/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    Alexander Pierce - Web Developer
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" id="btnProfile" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" id="btnLogout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <!-- <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                    -->
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo $img; ?>/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $full_name; ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <!--
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
                </div>
            </form> -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">เมนูรายงาน</li>
                <li class="treeview <?php echo ($crlName=='receivewage') ? 'active' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>รายงานยอดรายได้ค่าแรง<br/>และประสิทธิภาพรวม</span>
                        <span class="pull-right-container">
			              <i class="fa fa-angle-left pull-right"></i>
			            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php echo ($CurrActionID=='wage' || $CurrActionID=='receivewage') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/receivewage/wage" data-toggle="modal">
                                <i class="fa fa-chevron-circle-right"></i> รายงานรายได้ค่าแรงศูนย์<br/>บริการ
                            </a>
                        </li>
                        <li <?php echo ($CurrActionID=='pfm_mechanic' || $CurrActionID=='receivepfm') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/receivewage/pfm_mechanic">
                                <i class="fa fa-chevron-circle-right"></i> รายงานประสิทธิภาพงาน<br/>โดยรวม 
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="treeview <?php echo ($crlName=='personal') ? 'active' : ''; ?>">
                    <a href="">
                        <i class="fa fa-user"></i> 
                        <span>รายงานประสิทธิภาพงานของ<br/>ช่างรายคน</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>  
                    <ul class="treeview-menu">
                        <li <?php echo ($CurrActionID=='pfm_repairman_day' || $CurrActionID=='receive_repairman_day') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/personal/pfm_repairman_day" data-toggle="modal">
                                <i class="fa fa-chevron-circle-right"></i> รายงานประสิทธิภาพงานของ<br/>ช่างรายคน(รายวัน)
                            </a>
                        </li>
                        <li <?php echo ($CurrActionID=='pfm_repairman_month' || $CurrActionID=='receive_repairman' ) ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/personal/pfm_repairman_month">
                                <i class="fa fa-chevron-circle-right"></i> รายงานประสิทธิภาพงานของ<br/>ช่างรายคน(รายเดือน)
                            </a>
                        </li>
                    </ul>                 
                </li>

                <li class="treeview <?php echo ($crlName=='number') ? 'active' : ''; ?>">
                    <a href="<?php echo SITE_URL;?>/number/number_of_car_repair">
                        <i class="fa fa-car"></i> 
                        <span>รายงานยอดการซ่อมรถเล็ก <br/>ต่อ ช่าง 1 คน</span>       
                        <span class="pull-right-container"></span>
                    </a>                   
                </li>

                <li class="treeview <?php echo ($crlName=='time') ? 'active' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-clock-o"></i> 
                        <span>รายงานเวลาซ่อม โดยเฉลี่ย<br/>ต่อวัน</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php echo ($CurrActionID=='time_repair' || $CurrActionID=='time_average') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/time/time_repair" data-toggle="modal">
                                <i class="fa fa-chevron-circle-right"></i> รายงานเวลาซ่อม โดยเฉลี่ย<br/>ต่อวัน
                            </a>
                        </li>
                        <li <?php echo ($CurrActionID=='order_repair' || $CurrActionID=='order_repair') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/time/order_repair">
                                <i class="fa fa-chevron-circle-right"></i>รายงานจำนวนใบสั่งซ่อมแต่ละ<br/>ประเภทซ่อม
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="treeview <?php echo ($crlName=='repair') ? 'active' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-bar-chart"></i> 
                        <span>รายงานยอดรับแจ้งซ่อมต่อ<br/>ที่ปรึกษางานบริการ</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <!-- TODO:: Define POST controller -->
                        <li <?php echo ($CurrActionID=='sa_repair_day' || $CurrActionID=='receive_sa_repair') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/repair/sa_repair_day" data-toggle="modal">
                                <i class="fa fa-chevron-circle-right"></i> รายงานยอดรับแจ้งซ่อมต่อ<br/>SA รายคน(รายวัน)
                            </a>
                        </li>
                        <li <?php echo ($CurrActionID=='sa_repair_month' || $CurrActionID=='receive_sa_repair') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/repair/sa_repair_month" data-toggle="modal">
                                <i class="fa fa-chevron-circle-right"></i> รายงานยอดรับแจ้งซ่อมต่อ<br/>SA รายคน(รายเดือน)
                            </a>
                        </li>
                        <li <?php echo ($CurrActionID=='avg_repair' || $CurrActionID=='receive_avg_repair') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/repair/avg_repair">
                                <i class="fa fa-chevron-circle-right"></i> รายงานเวลารับแจ้งซ่อมเฉลี่ย<br/>ต่อคัน รายเดือน
                            </a>
                        </li>
                        <!-- TODO:: Define POST controller -->
                        <li <?php echo ($CurrActionID=='sa_repair_report' ) ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/repair/sa_repair_report" data-toggle="modal">
                                <i class="fa fa-chevron-circle-right"></i> ออกรายงานยอดรับแจ้งซ่อม<br/>SA รายคนตามใบงานซ่อมต่อเดือน
                            </a>
                        </li>
                        <!-- TODO:: Define POST controller -->
                        <li <?php echo ($CurrActionID=='avg_repair_report') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/repair/avg_repair_report">
                                <i class="fa fa-chevron-circle-right"></i> รายงานเวลารับแจ้งซ่อมเฉลี่ย<br/>ต่อคัน รายเดือน ตามใบงานซ่อม 
                            </a>                            
                        </li>
                    </ul>                    
                </li>

                <li class="treeview <?php echo ($crlName=='duringrepair') ? 'active' : ''; ?>">
                    <a href="#">
                        <i class="fa  fa-wrench"></i> <span>รายงานช่วงสถานะใบงาน<br/>ซ่อม</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php echo ($CurrActionID=='pb_repair_overtime' || $CurrActionID=='overtime') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/duringrepair/pb_repair_overtime" data-toggle="modal">
                                <i class="fa fa-chevron-circle-right"></i> รายงานสถานะใบงานซ่อมที่<br/>เกินเวลานัดหมายส่งมอบ
                            </a>
                        </li>
                        <li <?php echo ($CurrActionID=='statistics_outsize_day' || $CurrActionID=='time_daily') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/duringrepair/statistics_outsize_day">
                                <i class="fa fa-chevron-circle-right"></i> รายงานสถิติระยะเวลาใบงาน<br/>เกินเวลามาตรฐาน ต่อวัน
                            </a>
                        </li>
                        <li <?php echo ($CurrActionID=='statistics_outsize_month' || $CurrActionID=='time_month') ? 'class="active"' : ''; ?>>
                            <a href="<?php echo SITE_URL;?>/duringrepair/statistics_outsize_month">
                                <i class="fa fa-chevron-circle-right"></i> รายงานสถิติระยะเวลาใบงาน<br/>เกินเวลามาตรฐาน ต่อเดือน
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="treeview <?php echo ($crlName=='services') ? 'active' : ''; ?>">
                    <a href="<?php echo SITE_URL;?>/services/loop_services">
                        <i class="fa fa-area-chart"></i> <span>รายงานรอบการเข้ารับบริการ<br/>ของลูกค้า</span>
                        <span class="pull-right-container">
                        </span>
                    </a>
                </li>

                <li class="treeview <?php echo ($crlName=='backlogscar') ? 'active' : ''; ?>">
                    <a href="<?php echo SITE_URL;?>/backlogscar/repair_backlogs">
                        <i class="fa fa-server"></i> <span>รายงานรถค้างซ่อมสะสม</span>
                        <span class="pull-right-container">
                        </span>
                    </a>
                </li>

                <li class="treeview <?php echo ($crlName=='spares') ? 'active' : ''; ?>">
                    <a href="<?php echo SITE_URL;?>/spares/profit_lost">
                        <i class="fa  fa-line-chart"></i> <span>รายงานกำไรขาดทุนการขาย<br/>อะไหล่</span>
                        <span class="pull-right-container">
                        </span>
                    </a>
                </li>

                
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <div class="container">
        <!-- Modal -->
    </div>


    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <?php echo $content; ?>
        </section>
    </div>
    <!-- /.content-wrapper -->
    <!-- =============================================== -->


    <footer class="main-footer footer_text">
        <div class="pull-right hidden-xs">
            <b>EasyService Version </b> 0.98
        </div>
        <strong>Copyright &copy; 2016-2017 อีซูซุเชียงราย (2002) จำกัด. All rights reserved.</strong>
    </footer>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->

    <!-- ./wrapper -->
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>