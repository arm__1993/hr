<?php

namespace app\modules\vhc\controllers;

use app\modules\hr\apihr\ApiOT;
use app\modules\vhc\models\VHC\VhcBlame;
use app\modules\vhc\models\VHC\VhcGrouplist;
use app\modules\vhc\models\VHC\VhcList;
use app\modules\vhc\models\VHC\VhcListOption;
use app\modules\vhc\models\VHC\VhcModel;
use app\modules\vhc\models\VHC\VhcServicetype;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use Yii;
use yii\data\ArrayDataProvider;
use app\modules\hr\apihr\ApiHr;

class DataController extends \yii\web\Controller

{
    public $layout = 'vhclayout';
    public $idcardLogin;

    public function init()
    {
        $session = Yii::$app->session;
        if (!$session->has('fullname') || !$session->has('idcard')) {
            return $this->redirect(array('/auth/default/logout'), 302);
        }

        $this->idcardLogin = $session->get('idcard');
    }

    public function actionIndex()
    {
        /*  $searchModel = new VhcModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);*/
        $showvhc = new VhcModel();
        $showvhcindex = $showvhc->search(\Yii::$app->request->queryParams);

        $showblame = new VhcBlame();
        $showblameindex = $showblame->search(\Yii::$app->request->queryParams);

        $showservicetype = new VhcServicetype();
        $showservicetypeindex = $showservicetype->search(\Yii::$app->request->queryParams);

        $showvhcgrouplist = new VhcGrouplist();
        $showvhcgrouplistindex = $showvhcgrouplist->search(\Yii::$app->request->queryParams);

        $showvhclist = new VhcList();
        $showvhclistindex = $showvhclist->search(\Yii::$app->request->queryParams);

        $showvhclistoption = new VhcListOption();
        $showvhclistoptionindex = $showvhclistoption->search(\Yii::$app->request->queryParams);


        //print_r( $showservicetypeindex);
        //   exit();
        // $data = ApiHr::CheckEditvhc();
        /*print_r($showvhcindex);*/
        return $this->render('index', [

            'showvhc' => $showvhc,
            'showvhcindex' => $showvhcindex,

            'showblame' => $showblame,
            'showblameindex' => $showblameindex,

            'showservicetype' => $showservicetype,
            'showservicetypeindex' => $showservicetypeindex,

            'showvhcgrouplist' => $showvhcgrouplist,
            'showvhcgrouplistindex' => $showvhcgrouplistindex,

            'showvhclist' => $showvhclist,
            'showvhclistindex' => $showvhclistindex,

            'showvhclistoption' => $showvhclistoption,
            'showvhclistoptionindex' => $showvhclistoptionindex,

        ]);
    }

    /* public function behaviors()
     {
         return [
             'verbs' => [
                 'class' => VerbFilter::className(),
                 'actions' => [
                     'savevhcmodel' => ['post'],
                     'updateadddeduct' => ['post'],
                     'deleteaddeduct' => ['post'],
                     'savepayrollconfig' => ['post'],
                 ],
             ],
         ];
     }*/
    ////////////////////////////////////////-----VHC MODEL-----///////////////////////////////////////////////////////////////

    public function actionSavevhcmodel()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/
            $login = $this->idcardLogin;
            if ($post['hide_activityedit'] != '') {
                $model = VhcModel::findOne(['id' => $post['hide_activityedit']]);
                $model->model_name = $post['model_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new VhcModel();
                $model->model_name = $post['model_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }

    public function actionUpdatevhcmodel()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = VhcModel::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeletevhcmodel()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = VhcModel::findOne(['id' => $post['id']]);

            $model->record_status = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }

    public function actionSavepayrollconfig()
    {

        if (Yii::$app->request->isAjax) {

            $model = VhcModel::findOne(1);
            $setStep2 = false;
            //Check Model if not null, Clone it
            if ($model !== null) {
                $ObjCloneCfg = clone $model; // <== clone object;
                $setStep2 = true;
            }

            //If null create new instance
            if ($model === null) {
                $model = new VhcModel();
            }


            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                //when already save, go to step 2, step 2 update history data
                if ($setStep2) {

                    $newValue = Yii::$app->request->post();
                    $connection = Yii::$app->dbERP_easyhr_PAYROLL;
                    $transaction = $connection->beginTransaction();
                    try {

                        //update config ot template ID here
                        if ($ObjCloneCfg->ot_template_id != $newValue['PayrollConfigTemplate']['ot_template_id']) {

                            $N = $newValue['PayrollConfigTemplate']['ot_template_id'];
                            $O = $ObjCloneCfg->ot_template_id;
                            $sql = "UPDATE ADD_DEDUCT_HISTORY SET ADD_DEDUCT_THIS_MONTH_TMP_ID='$N' WHERE ADD_DEDUCT_THIS_MONTH_TMP_ID='$O'  ";
                            $connection->createCommand($sql)->execute();
                        }


                        $transaction->commit();

                    } catch (ErrorException $e) {
                        $transaction->rollBack();
                        throw new \Exception('ERROR' . $e->getMessage());
                    }

                }

                echo 1;

            } else {
                $error = \yii\widgets\ActiveForm::validate($model);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                print_r($error);
            }
        }
    }

    /////////////////////////////////////////-----VHC MODEL-----///////////////////////////////////////////////



    /////////////////////////////////////////-----VHC BLAME-----///////////////////////////////////////////////
    public function actionDeletevhcblame() //--ลบ--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = VhcBlame::findOne(['id' => $post['id']]);

            $model->record_status = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }

    public function actionSavevhcblame()//--เพิ่ม--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/
            $login = $this->idcardLogin;
            if ($post['hide_activityedits'] != '') {
                $model = VhcBlame::findOne(['id' => $post['hide_activityedits']]);
                $model->blame_name = $post['blame_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new VhcBlame();
                $model->blame_name = $post['blame_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }
    public function actionUpdatevhcblame()//--แก้ไข--//
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = VhcBlame::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
    /////////////////////////////////////////-----VHC BLAME-----///////////////////////////////////////////////

    /////////////////////////////////////////-----VHC  Servicetype-----///////////////////////////////////////////////

    public function actionServicetype()//--เพิ่ม--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/
            $login = $this->idcardLogin;
            if ($post['hide_activityedit_servicetype'] != '') {
                $model = VhcServicetype::findOne(['id' => $post['hide_activityedit_servicetype']]);
                $model->servicetype_name = $post['servicetype_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new VhcServicetype();
                $model->servicetype_name = $post['servicetype_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }
    public function actionUpdatevhcservicetype()//--แก้ไข--//
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = VhcServicetype::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeletevhcservicetype() //--ลบ--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = VhcServicetype::findOne(['id' => $post['id']]);

            $model->record_status = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }
    /////////////////////////////////////////-----VHC  Servicetype-----///////////////////////////////////////////////




    /////////////////////////////////////////-----VHC  Servicetype-----///////////////////////////////////////////////



    public function actionGrouplist()//--เพิ่ม--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/
            $login = $this->idcardLogin;
            if ($post['hide_activityedit_grouplist'] != '') {
                $model = VhcGrouplist::findOne(['id' => $post['hide_activityedit_grouplist']]);
                $model->model_id = $post ['model_id'];
                $model->group_name = $post['group_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new VhcGrouplist();
                $model->model_id = $post ['model_id'];
                $model->group_name = $post['group_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }
    public function actionUpdatevhcgrouplist()//--แก้ไข--//
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = VhcGrouplist::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeletevhcgrouplist() //--ลบ--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = VhcGrouplist::findOne(['id' => $post['id']]);

            $model->record_status = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }

////////////////////////////////////////////-----VHC  list-----///////////////////////////////////////////////

    public function actionSavelist()//--เพิ่ม--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/
            $login = $this->idcardLogin;
            if ($post['hide_activityedit_list'] != '') {
                $model = VhcList::findOne(['id' => $post['hide_activityedit_list']]);
                $model->group_id = $post ['group_id'];
                $model->list_name = $post['list_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new VhcList();
                $model->group_id = $post ['group_id'];
                $model->list_name = $post['list_name'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }
    public function actionUpdatevhclist()//--แก้ไข--//
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = VhcList::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeletevhclist() //--ลบ--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = VhcList::findOne(['id' => $post['id']]);

            $model->record_status = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }



/////////////////////////////////////////-----VHC  list-----///////////////////////////////////////////////





    public function actionSavelistoption()//--เพิ่ม--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/
            $login = $this->idcardLogin;
            if ($post['hide_activityedit_listoption'] != '') {
                $model = VhcListOption::findOne(['id' => $post['hide_activityedit_listoption']]);
                $model->list_id = $post ['list_id'];
                $model->option_name = $post['option_name'];
                $model->option_value = $post['option_value'];
                $model->comment = $post['comment'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new VhcListOption();
                $model->list_id = $post ['list_id'];
                $model->option_name = $post['option_name'];
                $model->option_value = $post['option_value'];
                $model->comment = $post['comment'];
                $model->record_status = ($post['record_status']) ? $post['record_status'] : 0;
                $model->create_by = $login;
                $model->update_by = $login;
                $model->create_date = new Expression('NOW()');
                $model->update_date = new Expression('NOW()');
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }
    public function actionUpdatevhclistoption()//--แก้ไข--//
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = VhcListOption::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeletevhclistoption() //--ลบ--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = VhcList::findOne(['id' => $post['id']]);

            $model->record_status = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }
}
