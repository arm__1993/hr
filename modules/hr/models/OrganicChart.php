<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "Organic_chart".
 *
 * @property integer $id
 * @property integer $Company
 * @property integer $Drepartment
 * @property integer $ref_id
 * @property string $Name
 * @property integer $Emp_id
 * @property integer $parent
 * @property integer $active_status
 * @property integer $level
 * @property integer $Posision_id
 * @property string $create_date
 * @property string $update_date
 * @property string $Emp_name
 * @property string $Emp_IDCard
 * @property string $positionCode
 */
class OrganicChart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Organic_chart';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Company', 'Drepartment', 'ref_id', 'Emp_id', 'parent', 'active_status', 'level', 'Posision_id'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['Name'], 'string', 'max' => 250],
            [['positionCode', 'Emp_name'], 'string', 'max' => 200],
            [['Emp_IDCard'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Company' => 'Company',
            'Drepartment' => 'Drepartment',
            'ref_id' => 'Ref ID',
            'Name' => 'Name',
            'positionCode' => 'Position Code',
            'Emp_id' => 'Emp ID',
            'parent' => 'Parent',
            'active_status' => 'Active Status',
            'level' => 'Level',
            'Posision_id' => 'Posision ID',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'Emp_name' => 'Emp Name',
            'Emp_IDCard' => 'Emp  Idcard',
        ];
    }
}