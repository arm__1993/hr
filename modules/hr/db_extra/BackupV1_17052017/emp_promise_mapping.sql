-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 03, 2017 at 11:17 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-10+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_promise_mapping`
--

CREATE TABLE `emp_promise_mapping` (
  `id` int(11) NOT NULL,
  `condition_id` varchar(50) DEFAULT NULL COMMENT 'emp_promise_mapping_condition_id',
  `param_text` varchar(50) DEFAULT NULL,
  `fixable_type` tinyint(1) DEFAULT NULL COMMENT '0:ค่าคงที่, 1:ดึงจาก table',
  `db_name` varchar(100) DEFAULT NULL,
  `tb_name` varchar(100) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `sel_command` varchar(200) DEFAULT NULL,
  `default_text` varchar(250) DEFAULT NULL,
  `desc_text` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_promise_mapping`
--
ALTER TABLE `emp_promise_mapping`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_promise_mapping`
--
ALTER TABLE `emp_promise_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
