<?php

namespace app\modules\gitreport\models;

use Yii;

/**
 * This is the model class for table "issue_status".
 *
 * @property integer $id
 * @property string $status_name
 */
class IssueStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'issue_status';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbGit_Report');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_name' => 'Status Name',
        ];
    }

    /**
     * @inheritdoc
     * @return IssueStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IssueStatusQuery(get_called_class());
    }
}
