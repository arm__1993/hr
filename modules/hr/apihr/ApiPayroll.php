<?php


/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/1/2017 AD
 * Time: 10:50
 */

namespace app\modules\hr\apihr;

use app\api\DateTime;
use app\api\Utility;
use app\modules\hr\models\AdddeductPermanent;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Deptdeductdetail;

use yii\db\Expression;
use yii\db\Transaction;
use app\api\DBConnect;
use yii;

class ApiPayroll
{


    public static function getArrayAdddeductTemplate()
    {
        $data = self::getDataDeductAll();
        $arrList=[];
        if(is_array($data))
        foreach ($data as $item) {
            $arrList[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item;
        }

        asort($arrList);
        return $arrList;
    }


    public static function getTemplateTax()
    {
        $data = Adddeducttemplate::find()
            ->select(['ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID as ADD_DEDUCT_TEMPLATE_ID',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_TEMPLATE.accounting_code_pk as accounting_code_pk',
                'ADD_DEDUCT_TEMPLATE.tax_rate as tax_rate',
                'tax_income_section.id as tax_section_id',
                'tax_income_section.tax_section_name as tax_section_name',
            ])
            ->from('ADD_DEDUCT_TEMPLATE')
            ->innerJoin('tax_income_section', 'ADD_DEDUCT_TEMPLATE.tax_section_id = tax_income_section.id')
            ->where('ADD_DEDUCT_TEMPLATE_STATUS <> 99 AND ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = 1')
            ->asArray()
            ->all();
        $arrList = [];
        if(is_array($data)) {
            foreach ($data as $item) {
                $arrList[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item;
            }
        }

        return $arrList;
    }




    public static function listdata_add_deduct_template()
    {
        $sql_listdata = "SELECT ADD_DEDUCT_TEMPLATE_ID,
         						 ADD_DEDUCT_TEMPLATE_NAME 
         				  FROM `ADD_DEDUCT_TEMPLATE`
                          WHERE  ADD_DEDUCT_TEMPLATE_TYPE = '1'
                          AND ADD_DEDUCT_TEMPLATE_STATUS <> '99'";
        return $listdata = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql_listdata)->queryAll();
    }



    public static function getDataDeductTemp()
    {
        $sql_listdata = "SELECT ADD_DEDUCT_TEMPLATE_ID,
                     ADD_DEDUCT_TEMPLATE_NAME 
                  FROM `ADD_DEDUCT_TEMPLATE`
                          WHERE  ADD_DEDUCT_TEMPLATE_TYPE = '2'
                          AND ADD_DEDUCT_TEMPLATE_STATUS <> '99'";
        return $listdata = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql_listdata)->queryAll();
    }



    public static function getDataDeductAll()
    {
        $data = Adddeducttemplate::find()
            ->where('ADD_DEDUCT_TEMPLATE_STATUS <> 99 ')
            ->asArray()
            ->all();

        return $data;
    }


    public static function Getworkhis()
    {
        $idupdate = Yii::$app->session->get('idcard');
        $data = \app\modules\hr\models\WorkHistory::find()->where(['emp_id'=>$idupdate])
        ->asArray()
        ->all();
        return $data;
    }

    public static function getEmpdata($id_crad = null)
    {
        $where = '';
        if ($id_crad != null) {
            $where = 'ID_Card = ' . $id_crad;
            $select = ['CONCAT(Name," ",Surname) as label', 'ID_Card as value', 'DataNo'];
        } else {
            $select = ['CONCAT(Name," ",Surname) as label', 'ID_Card as value'];
            $where = 'Prosonnal_Being <> 3';
        }
        $data = Empdata::find()
            ->select($select)
            ->where($where)
            ->asArray()
            ->all();

        return $data;
    }

    public static function getArrayEmpdata($id_crad = null)
    {
/*        $where = '';
        if ($id_crad != null) {
            $where = 'ID_Card = ' . $id_crad;
            $select = ['CONCAT(Name," ",Surname) as label', 'ID_Card as value', 'DataNo'];
        } else {
            $select = ['CONCAT(Name," ",Surname) as label', 'ID_Card as value'];
            $where = 'Prosonnal_Being <> 3';
        }
        $data = Empdata::find()
            ->select($select)
            ->where($where)
            ->asArray()
            ->all();*/

        $data = self::getEmpdata($id_crad);
        $arrList = [];
        foreach ($data as $item) {
            $arrList[$item['value']] = $item['label'];
        }

        return $arrList;
    }




    public static function getDatadeductdetail($id_card)
    {
        $data = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE as ADD_DEDUCT_DETAIL_TYPE',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_PAY_DATE as ADD_DEDUCT_DETAIL_PAY_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE as ADD_DEDUCT_DETAIL_START_USE_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE as ADD_DEDUCT_DETAIL_END_USE_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_UPDATE_DATE as ADD_DEDUCT_DETAIL_UPDATE_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_UPDATE_BY as ADD_DEDUCT_DETAIL_UPDATE_BY',
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID = ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID')
            ->where('ADD_DEDUCT_DETAIL_EMP_ID = :id_card AND ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS = "1"')
            ->addParams(['id_card' => $id_card,])
            ->asArray()
            ->all();
        return $data;
    }

    public static function seachEmpforDeductions($id_company, $id_department, $id_section,$id_emp=null)
    {
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $strCond = '';
        if($id_company != '') {
            $strCond .= " AND $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY = '$id_company' ";
        }

        if($id_department != '') {
            $strCond .= " AND  $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT = '$id_department'  ";
        }

        if($id_section != '') {
            $strCond .= " AND $db[pl].EMP_SALARY.EMP_SALARY_SECTION = $id_section  ";
        }

        if($id_section != '') {
            $strCond .= " AND $db[pl].EMP_SALARY.EMP_SALARY_SECTION = $id_section  ";
        }

        if($id_emp != null) {
            $strCond .= " AND $db[ct].emp_data.ID_Card='$id_emp'  ";
        }

        $sql = "SELECT $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[ou].working_company.name as CompanyName,
                        $db[ou].department.name as DepartmentName,
                        $db[ou].section.name as SectionName,
                        $db[pl].EMP_SALARY.EMP_SALARY_POSITION_CODE as Position
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].EMP_SALARY ON $db[ct].emp_data.ID_Card = $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD 
                  INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY 
                  INNER JOIN $db[ou].department ON $db[ou].department.id = $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT
                  INNER JOIN $db[ou].section ON  $db[ou].section.id = $db[pl].EMP_SALARY.EMP_SALARY_SECTION
                  WHERE  1=1 $strCond
                  AND   $db[ct].emp_data.Prosonnal_Being <> 3
                  AND   $db[ct].emp_data.username <> '#####Out Off###' ORDER BY $db[ou].working_company.id,$db[ou].department.id, $db[ou].section.id,  $db[ct].emp_data.Name ASC ";

        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();
        return $data;

    }

    public static function mapPaytype($type)
    {

        if ($type == Yii::$app->params['PAYROLL']['PAY_THISMONTH']) {
            return Yii::$app->params['PAYROLL']['PAY_THISMONTH_LABEL'];
        } else {
            return Yii::$app->params['PAYROLL']['PAY_EVERYMONTH_LABEL'];
        }
    }

    public static function getDataDeductDetailByIdcard($id_company, $id_department, $id_section, $selectdeduct, $monthselect)
    {
        //$monthselect = " '%$monthselect%' ";
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];


        $strCond = '';
        if($id_company != '') {
            $strCond .= " AND $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY = '$id_company' ";
        }

        if($id_department != '') {
            $strCond .= " AND  $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT = '$id_department'  ";
        }

        if($id_section != '') {
            $strCond .= " AND $db[pl].EMP_SALARY.EMP_SALARY_SECTION = $id_section  ";
        }

        if($selectdeduct !='') {
            $strCond .= " AND $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $selectdeduct ";
        }

        //ADD_DEDUCT_TEMPLATE_TYPE
        $sql = "SELECT $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT,
                        $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS as ADD_DEDUCT_DETAIL_STATUS,
                        $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL,
                        $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID as ADD_DEDUCT_ID,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE as ADD_DEDUCT_TEMPLATE_TYPE,
                        $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE as ADD_DEDUCT_DETAIL_START_USE_DATE,
                        $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID
                        
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].EMP_SALARY ON $db[ct].emp_data.ID_Card = $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD 
                  INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY 
                  INNER JOIN $db[ou].department ON $db[ou].department.id = $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT
                  INNER JOIN $db[ou].section ON  $db[ou].section.id = $db[pl].EMP_SALARY.EMP_SALARY_SECTION
                  INNER JOIN $db[pl].ADD_DEDUCT_DETAIL ON $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID = $db[ct].emp_data.ID_Card
                  INNER JOIN $db[pl].ADD_DEDUCT_TEMPLATE ON $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID
                  WHERE 1=1 $strCond
                  AND   $db[ct].emp_data.Prosonnal_Being <> 3
                  AND   $db[ct].emp_data.username <> '#####Out Off###'
                  AND   $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS = '2'
                  AND   $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_PAY_DATE = '$monthselect' ";




        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();
        return $data;


    }

    public static function deleteDataDeductdetailByAddDeductdetailId($add_deductdetail_id)
    {
        //echo $add_deductdetail_id;
        $model = Adddeductdetail::findOne($add_deductdetail_id);
        //print_r($model);
        $model->ADD_DEDUCT_DETAIL_STATUS = "99";
        $model->save();
        if ($model->save() !== false) {
            $a = "update successful";
        }
        return $a;

    }

    public static function deleteDataDeptDeductDetail($add_deductdetail_id)
    {
        //echo $add_deductdetail_id;
        $model = Deptdeductdetail::findOne($add_deductdetail_id);

    }

    public static function saveEditDeductdetailByAddDeductdetailId($add_deductdetail_id, $id_emp, $selecttypeadddeduc, $amounttotal, $detaildeduct)
    {
        $idupdate = Yii::$app->session->get('idcard');
        //echo $add_deductdetail_id;
        $model = Adddeductdetail::findOne($add_deductdetail_id);
        //print_r($model);
        $model->ADD_DEDUCT_DETAIL_STATUS = Yii::$app->params['INACTIVE_STATUS'];
        $model->ADD_DEDUCT_DETAIL_UPDATE_BY = $idupdate;
        $model->ADD_DEDUCT_DETAIL_RESP_PERSON = $idupdate;
        $model->ADD_DEDUCT_DETAIL_UPDATE_DATE = date('Y-m-d H:i:s');
        $model->save();

        $modelADD_DEDUCT_DETAIL_PAY_DATE = $model['ADD_DEDUCT_DETAIL_PAY_DATE'];
        $modelADD_DEDUCT_DETAIL_START_USE_DATE = $model['ADD_DEDUCT_DETAIL_START_USE_DATE'];
        $modelADD_DEDUCT_DETAIL_END_USE_DATE = $model['ADD_DEDUCT_DETAIL_END_USE_DATE'];

        if ($model->save() !== false) {
            $a = "update successful11";
            $modelnew = new Adddeductdetail();

            $modelnew->ADD_DEDUCT_ID = $selecttypeadddeduc;
            $modelnew->ADD_DEDUCT_DETAIL = $detaildeduct;
            $modelnew->ADD_DEDUCT_DETAIL_PAY_DATE = $modelADD_DEDUCT_DETAIL_PAY_DATE;
            $modelnew->ADD_DEDUCT_DETAIL_START_USE_DATE = $modelADD_DEDUCT_DETAIL_START_USE_DATE;
            $modelnew->ADD_DEDUCT_DETAIL_END_USE_DATE = $modelADD_DEDUCT_DETAIL_END_USE_DATE;
            $modelnew->ADD_DEDUCT_DETAIL_EMP_ID = $id_emp;
            $modelnew->ADD_DEDUCT_DETAIL_AMOUNT = Utility::removeCommar($amounttotal);
            $modelnew->ADD_DEDUCT_DETAIL_TYPE = Yii::$app->params['PAYROLL']['PAY_THISMONTH'];
            $modelnew->ADD_DEDUCT_DETAIL_STATUS = 2; //Yii::$app->params['ACTIVE_STATUS'];
            $modelnew->ADD_DEDUCT_DETAIL_CREATE_DATE = new Expression('NOW()');
            $modelnew->ADD_DEDUCT_DETAIL_CREATE_BY = $idupdate;
            $modelnew->ADD_DEDUCT_DETAIL_UPDATE_BY = $idupdate;
            $modelnew->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
            $modelnew->ADD_DEDUCT_DETAIL_RESP_PERSON = $idupdate;
            $modelnew->ADD_DEDUCT_DETAIL_UPDATE_DATE = date('Y-m-d H:i:s');
            $modelnew->save();

            if ($modelnew->save() !== false) {
                $a = "update successful";
            } else {
                $a = "update fail";
            }
        }

        return $a;

    }


    public static function saveEditDeductdetailForManagemonthly($id_emp, $selecttypepaylist, $selecttypeadddeduc, $monthpaydate, $monthusedate, $amounttotal, $add_deductdetail_id)
    {

        $idupdate = Yii::$app->session->get('idcard');
        //echo $add_deductdetail_id;
        $model = Adddeductdetail::findOne($add_deductdetail_id);
        //print_r($model);
        $model->ADD_DEDUCT_ID = $selecttypeadddeduc;
        $model->ADD_DEDUCT_DETAIL_PAY_DATE = $monthpaydate;
        $model->ADD_DEDUCT_DETAIL_END_USE_DATE = $monthusedate;
        $model->ADD_DEDUCT_DETAIL_EMP_ID = $id_emp;
        $model->ADD_DEDUCT_DETAIL_TYPE = $selecttypepaylist;
        $model->ADD_DEDUCT_DETAIL_AMOUNT = $amounttotal;
        $model->ADD_DEDUCT_DETAIL_UPDATE_BY = $idupdate;
        $model->ADD_DEDUCT_DETAIL_RESP_PERSON = $idupdate;
        $model->ADD_DEDUCT_DETAIL_UPDATE_DATE = date('Y-m-d H:i:s');
        $model->save();
        if ($model->save() !== false) {
            $a = "update successful";
        } else {
            return "error";
        }


    }

    public static function getAddDeductPermanent($id_card,$typepay)
    {
        $data = AdddeductPermanent::find()
            ->select('ADD_DEDUCT_PERMANENT.*, ADD_DEDUCT_TEMPLATE.*')
            ->from('ADD_DEDUCT_PERMANENT')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_PERMANENT.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID')
            ->where('ADD_DEDUCT_DETAIL_EMP_ID = :id_card AND ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = :typepay  AND ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS = "1" AND ADD_DEDUCT_PERMANENT.ADD_DEDUCT_DETAIL_STATUS=1 ')
            ->addParams(['id_card' => $id_card,':typepay' => $typepay])
            ->orderBy('ADD_DEDUCT_PERMANENT.ADD_DEDUCT_DETAIL_CREATE_DATE DESC , ADD_DEDUCT_PERMANENT.ADD_DEDUCT_TEMPLATE_ID ASC')
            ->asArray()
            ->all();
        return $data;
    }



    public static function getDatadeductdetailByIdcardAndTypepay($id_card, $typepay)
    {

        //Query status ==2  เอาเฉพาะรายการ ที่ เป็นรายการตั้งไว้
        $data = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE as ADD_DEDUCT_DETAIL_TYPE',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_PAY_DATE as ADD_DEDUCT_DETAIL_PAY_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE as ADD_DEDUCT_DETAIL_START_USE_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE as ADD_DEDUCT_DETAIL_END_USE_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_UPDATE_DATE as ADD_DEDUCT_DETAIL_UPDATE_DATE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_UPDATE_BY as ADD_DEDUCT_DETAIL_UPDATE_BY',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID as ADD_DEDUCT_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID = ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID')
            ->where('ADD_DEDUCT_DETAIL_EMP_ID = :id_card AND ADD_DEDUCT_TEMPLATE_TYPE = :typepay AND ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS = "2"')
            ->addParams(['id_card' => $id_card, ':typepay' => $typepay])
            ->orderBy('ADD_DEDUCT_DETAIL_CREATE_DATE DESC , ADD_DEDUCT_TEMPLATE_ID ASC')
            ->asArray()
            ->all();



        return $data;
    }

    public static function getDataDeductDetailForAdddeductbydept($id_company, $id_department, $id_section, $for_month, $for_year)
    {

        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        if (!empty($id_company) AND !empty($id_department) AND !empty($id_section)) {
            // $data = Empdata::find()
            //     ->select(['emp_data.ID_Card as ID_Card',
            //             'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname',
            //             'dept_deduct.id as id_dept_deduct',
            //             'dept_deduct_detail.add_deduct_template_id as add_deduct_template_id',
            //             'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname',
            //             'dept_deduct_detail.dept_deduct_amount as dept_amount',
            //             'dept_deduct_detail.add_deduct_template_name as add_deduct_template_name',
            //             'dept_deduct_detail.id as dept_deduct_detail_id'
            //             ])
            //     ->from('emp_data')
            //     ->innerJoin('dept_deduct_detail','dept_deduct_detail.id_card = emp_data.ID_Card')
            //     ->innerJoin('dept_deduct','dept_deduct.id = dept_deduct_detail.dept_deduct_id')
            //     ->innerJoin('ADD_DEDUCT_TEMPLATE','ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = dept_deduct_detail.add_deduct_template_id')
            //     ->where('dept_deduct.company_id = :id_company
            //         AND  dept_deduct.dept_id = :id_department
            //         AND  dept_deduct.division_id = :division_id
            //         AND  dept_deduct.for_month = :for_month
            //         AND  dept_deduct.for_year = :for_year
            //         AND  emp_data.username != "#####Out Off###"
            //         AND  dept_deduct_detail.is_record_approved != "99"'
            //             )
            //     ->addParams([':id_company' => $id_company,
            //                  ':id_department' => $id_department,
            //                  ':division_id' => $id_section,
            //                  ':for_month'=> $for_month,
            //                  ':for_year'=> $for_year,])
            //     ->asArray()
            //     ->all();

            // return $data;

            $sql = "SELECT $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[pl].dept_deduct.id as id_dept_deduct,
                        $db[pl].dept_deduct_detail.add_deduct_template_id as add_deduct_template_id,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname,
                        $db[pl].dept_deduct_detail.dept_deduct_amount as dept_amount,
                        $db[pl].dept_deduct_detail.add_deduct_template_name as add_deduct_template_name,
                        $db[pl].dept_deduct_detail.id as dept_deduct_detail_id
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].dept_deduct_detail ON $db[ct].emp_data.ID_Card = $db[pl].dept_deduct_detail.id_card 
                  INNER JOIN $db[pl].dept_deduct ON $db[pl].dept_deduct.id = $db[pl].dept_deduct_detail.dept_deduct_id 
                  INNER JOIN $db[pl].ADD_DEDUCT_TEMPLATE ON $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $db[pl].dept_deduct_detail.add_deduct_template_id
                  WHERE $db[ct].emp_data.Prosonnal_Being <> 3
                  AND   $db[ct].emp_data.username <> '#####Out Off###'
                  AND   $db[pl].dept_deduct.company_id = $id_company
                  AND   $db[pl].dept_deduct.dept_id = $id_department
                  AND   $db[pl].dept_deduct.division_id = $id_section
                  AND   $db[pl].dept_deduct.for_month = $for_month
                  AND   $db[pl].dept_deduct.for_year = $for_year
                  AND   $db[pl].dept_deduct_detail.is_record_approved != '99'
                  ";

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryAll();
            return $data;
        } elseif (!empty($id_company) AND !empty($id_department)) {
            // $data = Empdata::find()
            //     ->select(['emp_data.ID_Card as ID_Card',
            //             'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname',
            //             'dept_deduct.id as id_dept_deduct',
            //             'dept_deduct_detail.add_deduct_template_id as add_deduct_template_id',
            //             'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname',
            //             'dept_deduct_detail.dept_deduct_amount as dept_amount',
            //             'dept_deduct_detail.add_deduct_template_name as add_deduct_template_name',
            //             'dept_deduct_detail.id as dept_deduct_detail_id'
            //             ])
            //     ->from('emp_data')
            //     ->innerJoin('dept_deduct_detail','dept_deduct_detail.id_card = emp_data.ID_Card')
            //     ->innerJoin('dept_deduct','dept_deduct.id = dept_deduct_detail.dept_deduct_id')
            //     ->innerJoin('ADD_DEDUCT_TEMPLATE','ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = dept_deduct_detail.add_deduct_template_id')
            //     ->where('dept_deduct.company_id = :id_company
            //         AND  dept_deduct.dept_id = :id_department
            //         AND  dept_deduct.for_month = :for_month
            //         AND  dept_deduct.for_year = :for_year
            //         AND  emp_data.username != "#####Out Off###"
            //         AND  dept_deduct_detail.is_record_approved != "99"'
            //             )
            //     ->addParams([':id_company' => $id_company,
            //                  ':id_department' => $id_department,
            //                  ':for_month'=> $for_month,
            //                  ':for_year'=> $for_year,])
            //     ->asArray()
            //     ->all();

            // return $data; 

            $sql = "SELECT $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[pl].dept_deduct.id as id_dept_deduct,
                        $db[pl].dept_deduct_detail.add_deduct_template_id as add_deduct_template_id,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname,
                        $db[pl].dept_deduct_detail.dept_deduct_amount as dept_amount,
                        $db[pl].dept_deduct_detail.add_deduct_template_name as add_deduct_template_name,
                        $db[pl].dept_deduct_detail.id as dept_deduct_detail_id
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].dept_deduct_detail ON $db[ct].emp_data.ID_Card = $db[pl].dept_deduct_detail.id_card 
                  INNER JOIN $db[pl].dept_deduct ON $db[pl].dept_deduct.id = $db[pl].dept_deduct_detail.dept_deduct_id 
                  INNER JOIN $db[pl].ADD_DEDUCT_TEMPLATE ON $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $db[pl].dept_deduct_detail.add_deduct_template_id
                  WHERE $db[ct].emp_data.Prosonnal_Being <> 3
                  AND   $db[ct].emp_data.username <> '#####Out Off###'
                  AND   $db[pl].dept_deduct.company_id = $id_company
                  AND   $db[pl].dept_deduct.dept_id = $id_department
                  AND   $db[pl].dept_deduct.for_month = $for_month
                  AND   $db[pl].dept_deduct.for_year = $for_year
                  AND   $db[pl].dept_deduct_detail.is_record_approved != '99'
                  ";

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryAll();
            return $data;
        } else if (!empty($id_company)) {
            //    $data = Empdata::find()
            //         ->select(['emp_data.ID_Card as ID_Card',
            //                 'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname',
            //                 'dept_deduct.id as id_dept_deduct',
            //                 'dept_deduct_detail.add_deduct_template_id as add_deduct_template_id',
            //                 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname',
            //                 'dept_deduct_detail.dept_deduct_amount as dept_amount',
            //                 'dept_deduct_detail.add_deduct_template_name as add_deduct_template_name',
            //                 'dept_deduct_detail.id as dept_deduct_detail_id'
            //                 ])
            //         ->from('emp_data')
            //         ->innerJoin('dept_deduct_detail','dept_deduct_detail.id_card = emp_data.ID_Card')
            //         ->innerJoin('dept_deduct','dept_deduct.id = dept_deduct_detail.dept_deduct_id')
            //         ->innerJoin('ADD_DEDUCT_TEMPLATE','ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = dept_deduct_detail.add_deduct_template_id')
            //         ->where('dept_deduct.company_id = :id_company
            //             AND  dept_deduct.for_month = :for_month
            //             AND  dept_deduct.for_year = :for_year
            //             AND  emp_data.username != "#####Out Off###"
            //             AND  dept_deduct_detail.is_record_approved != "99"'
            //                 )
            //         ->addParams([':id_company' => $id_company,
            //                      ':for_month'=> $for_month,
            //                      ':for_year'=> $for_year,])
            //         ->asArray()
            //         ->all();

            //     return $data;

            $sql = "SELECT $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[pl].dept_deduct.id as id_dept_deduct,
                        $db[pl].dept_deduct_detail.add_deduct_template_id as add_deduct_template_id,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname,
                        $db[pl].dept_deduct_detail.dept_deduct_amount as dept_amount,
                        $db[pl].dept_deduct_detail.add_deduct_template_name as add_deduct_template_name,
                        $db[pl].dept_deduct_detail.id as dept_deduct_detail_id
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].dept_deduct_detail ON $db[ct].emp_data.ID_Card = $db[pl].dept_deduct_detail.id_card 
                  INNER JOIN $db[pl].dept_deduct ON $db[pl].dept_deduct.id = $db[pl].dept_deduct_detail.dept_deduct_id 
                  INNER JOIN $db[pl].ADD_DEDUCT_TEMPLATE ON $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $db[pl].dept_deduct_detail.add_deduct_template_id
                  WHERE $db[ct].emp_data.Prosonnal_Being <> 3
                  AND   $db[ct].emp_data.username <> '#####Out Off###'
                  AND   $db[pl].dept_deduct.company_id = $id_company
                  AND   $db[pl].dept_deduct.for_month = $for_month
                  AND   $db[pl].dept_deduct.for_year = $for_year
                  AND   $db[pl].dept_deduct_detail.is_record_approved != '99'
                  ";

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryAll();
            return $data;
        }

    }


    public static function saveEditDeptDeductDetail($id_emp, $selecttypeadddeduc, $amounttotal, $detaildeduct, $add_deductdetail_id)
    {

        $idupdate = Yii::$app->session->get('idcard');
        //echo $add_deductdetail_id;
        $model = Deptdeductdetail::findOne($add_deductdetail_id);
        // print_r($model);
        $model->id_card = $id_emp;
        $model->add_deduct_template_id = $selecttypeadddeduc;
        $model->dept_deduct_amount = $amounttotal;
        $model->add_deduct_template_name = $detaildeduct;
        $model->update_by = $idupdate;
        $model->update_datetime = date('Y-m-d H:i:s');
        $model->save();


        if ($model->save() !== false) {
            $a = "update successful222";
            //    $modelnew = new Deptdeductdetail();

            //     $modelnew->ADD_DEDUCT_ID = $selecttypeadddeduc;
            //     $modelnew->ADD_DEDUCT_DETAIL = $detaildeduct;
            //     $modelnew->ADD_DEDUCT_DETAIL_PAY_DATE = $modelADD_DEDUCT_DETAIL_PAY_DATE;
            //     $modelnew->ADD_DEDUCT_DETAIL_START_USE_DATE = $modelADD_DEDUCT_DETAIL_START_USE_DATE;
            //     $modelnew->ADD_DEDUCT_DETAIL_END_USE_DATE = $modelADD_DEDUCT_DETAIL_END_USE_DATE;
            //     $modelnew->ADD_DEDUCT_DETAIL_EMP_ID = $id_emp;
            //     $modelnew->ADD_DEDUCT_DETAIL_AMOUNT = $amounttotal;
            //     $modelnew->ADD_DEDUCT_DETAIL_TYPE = Yii::$app->params['PAYROLL']['PAY_THISMONTH'];
            //     $modelnew->ADD_DEDUCT_DETAIL_STATUS = Yii::$app->params['ACTIVE_STATUS'];
            //     $modelnew->ADD_DEDUCT_DETAIL_CREATE_DATE =  new Expression('NOW()');
            //     $modelnew->ADD_DEDUCT_DETAIL_CREATE_BY = $idupdate;
            //     $modelnew->ADD_DEDUCT_DETAIL_UPDATE_BY = $idupdate;
            //     $modelnew->ADD_DEDUCT_DETAIL_UPDATE_DATE =  new Expression('NOW()');
            //     $modelnew->ADD_DEDUCT_DETAIL_RESP_PERSON = $idupdate;
            //     $modelnew->ADD_DEDUCT_DETAIL_UPDATE_DATE = date('Y-m-d H:i:s');
            //     $modelnew->save();

            //      if ($modelnew->save() !== false) {
            //
            //     }else{
            //           $a= "update fail";
            //     }
        } else {
            return "error";
        }
    }

    public static function getDataDeductDetailForApproveddeductbydept($id_company, $id_department, $id_section, $for_month, $for_year)
    {
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];




        if (!empty($id_company) AND !empty($id_department) AND !empty($id_section)) {
            // $data = Empdata::find()
            //     ->select(['working_company.name as workingname',
            //             'dept_deduct_detail.doc_notice_id as doc_notice_id',
            //             'dept_deduct.for_year as for_year',
            //             'dept_deduct.for_month as for_month',
            //             'emp_data.ID_Card as ID_Card',
            //             'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname',
            //             'dept_deduct.id as id_dept_deduct',
            //             'dept_deduct_detail.add_deduct_template_id as add_deduct_template_id',
            //             'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname',
            //             'dept_deduct_detail.dept_deduct_amount as dept_amount',
            //             'dept_deduct_detail.add_deduct_template_name as add_deduct_template_name',
            //             'dept_deduct_detail.id as dept_deduct_detail_id',
            //             'dept_deduct_detail.is_record_approved as is_record_approved',
            //             'dept_deduct.create_by as create_by'
            //             ])
            //     ->from('emp_data')
            //     ->innerJoin('dept_deduct_detail','dept_deduct_detail.id_card = emp_data.ID_Card')
            //     ->innerJoin('dept_deduct','dept_deduct.id = dept_deduct_detail.dept_deduct_id')
            //     ->innerJoin('ADD_DEDUCT_TEMPLATE','ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = dept_deduct_detail.add_deduct_template_id')
            //     ->innerJoin('working_company','working_company.id = dept_deduct.company_id')
            //     ->where('dept_deduct.company_id = :id_company
            //         AND  dept_deduct.dept_id = :id_department
            //         AND  dept_deduct.division_id = :division_id
            //         AND  dept_deduct.for_month = :for_month
            //         AND  dept_deduct.for_year = :for_year
            //         AND  emp_data.username != "#####Out Off###"
            //         AND  dept_deduct_detail.is_record_approved != "99"')
            //     ->addParams([':id_company' => $id_company,
            //                  ':id_department' => $id_department,
            //                  ':division_id' => $id_section,
            //                  ':for_month'=> $for_month,
            //                  ':for_year'=> $for_year,])
            //     ->asArray()
            //     ->all();

            // return $data;


            $sql = "SELECT  $db[ou].working_company.name as workingname,
                        $db[pl].dept_deduct_detail.doc_notice_id as doc_notice_id,
                        $db[pl].dept_deduct.for_year as for_year,
                        $db[pl].dept_deduct.for_month as for_month,
                        $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[pl].dept_deduct.id as id_dept_deduct,
                        $db[pl].dept_deduct_detail.add_deduct_template_id as add_deduct_template_id,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname,
                        $db[pl].dept_deduct_detail.dept_deduct_amount as dept_amount,
                        $db[pl].dept_deduct_detail.add_deduct_template_name as add_deduct_template_name,
                        $db[pl].dept_deduct_detail.id as dept_deduct_detail_id,
                        $db[pl].dept_deduct_detail.is_record_approved as is_record_approved,
                        $db[pl].dept_deduct_detail.create_by as create_by
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].dept_deduct_detail ON $db[ct].emp_data.ID_Card = $db[pl].dept_deduct_detail.id_card 
                  INNER JOIN $db[pl].dept_deduct ON $db[pl].dept_deduct.id = $db[pl].dept_deduct_detail.dept_deduct_id 
                  INNER JOIN $db[pl].ADD_DEDUCT_TEMPLATE ON $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $db[pl].dept_deduct_detail.add_deduct_template_id
                  INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].dept_deduct.company_id
                  WHERE $db[ct].emp_data.Prosonnal_Being <> '3'
                  AND   $db[ct].emp_data.username <> '#####Out Off###'
                  AND   $db[pl].dept_deduct.company_id = $id_company
                  AND   $db[pl].dept_deduct.dept_id = $id_department
                  AND   $db[pl].dept_deduct.division_id = $id_section
                  AND   $db[pl].dept_deduct.for_month = $for_month
                  AND   $db[pl].dept_deduct.for_year = $for_year
                  AND   $db[pl].dept_deduct_detail.is_record_approved != '99'";

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryAll();
            return $data;

        } elseif (!empty($id_company) AND !empty($id_department)) {
            //    $data = Empdata::find()
            //         ->select(['working_company.name as workingname',
            //                 'dept_deduct_detail.doc_notice_id as doc_notice_id',
            //                 'dept_deduct.for_year as for_year',
            //                 'dept_deduct.for_month as for_month',
            //                 'emp_data.ID_Card as ID_Card',
            //                 'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname',
            //                 'dept_deduct.id as id_dept_deduct',
            //                 'dept_deduct_detail.add_deduct_template_id as add_deduct_template_id',
            //                 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname',
            //                 'dept_deduct_detail.dept_deduct_amount as dept_amount',
            //                 'dept_deduct_detail.add_deduct_template_name as add_deduct_template_name',
            //                 'dept_deduct_detail.id as dept_deduct_detail_id',
            //                 'dept_deduct_detail.is_record_approved as is_record_approved',
            //                 'dept_deduct.create_by as create_by'
            //                 ])
            //         ->from('emp_data')
            //         ->innerJoin('dept_deduct_detail','dept_deduct_detail.id_card = emp_data.ID_Card')
            //         ->innerJoin('dept_deduct','dept_deduct.id = dept_deduct_detail.dept_deduct_id')
            //         ->innerJoin('ADD_DEDUCT_TEMPLATE','ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = dept_deduct_detail.add_deduct_template_id')
            //         ->innerJoin('working_company','working_company.id = dept_deduct.company_id')
            //         ->where('dept_deduct.company_id = :id_company
            //             AND  dept_deduct.dept_id = :id_department
            //             AND  dept_deduct.for_month = :for_month
            //             AND  dept_deduct.for_year = :for_year
            //             AND  emp_data.username != "#####Out Off###"
            //             AND  dept_deduct_detail.is_record_approved != "99"')
            //         ->addParams([':id_company' => $id_company,
            //                      ':id_department' => $id_department,
            //                      ':for_month'=> $for_month,
            //                      ':for_year'=> $for_year,])
            //         ->asArray()
            //         ->all();

            //     return $data;

            $sql = "SELECT  $db[ou].working_company.name as workingname,
                        $db[pl].dept_deduct_detail.doc_notice_id as doc_notice_id,
                        $db[pl].dept_deduct.for_year as for_year,
                        $db[pl].dept_deduct.for_month as for_month,
                        $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[pl].dept_deduct.id as id_dept_deduct,
                        $db[pl].dept_deduct_detail.add_deduct_template_id as add_deduct_template_id,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname,
                        $db[pl].dept_deduct_detail.dept_deduct_amount as dept_amount,
                        $db[pl].dept_deduct_detail.add_deduct_template_name as add_deduct_template_name,
                        $db[pl].dept_deduct_detail.id as dept_deduct_detail_id,
                        $db[pl].dept_deduct_detail.is_record_approved as is_record_approved,
                        $db[pl].dept_deduct_detail.create_by as create_by
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].dept_deduct_detail ON $db[ct].emp_data.ID_Card = $db[pl].dept_deduct_detail.id_card 
                  INNER JOIN $db[pl].dept_deduct ON $db[pl].dept_deduct.id = $db[pl].dept_deduct_detail.dept_deduct_id 
                  INNER JOIN $db[pl].ADD_DEDUCT_TEMPLATE ON $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $db[pl].dept_deduct_detail.add_deduct_template_id
                  INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].dept_deduct.company_id
                  WHERE $db[ct].emp_data.Prosonnal_Being <> '3'
                  AND   $db[ct].emp_data.username <> '#####Out Off###'
                  AND   $db[pl].dept_deduct.company_id = $id_company
                  AND   $db[pl].dept_deduct.dept_id = $id_department
                  AND   $db[pl].dept_deduct.for_month = $for_month
                  AND   $db[pl].dept_deduct.for_year = $for_year
                  AND   $db[pl].dept_deduct_detail.is_record_approved != '99'";

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryAll();
            return $data;
        } else if (!empty($id_company)) {
            //    $data = Empdata::find()
            //     ->select(['working_company.name as workingname',
            //             'dept_deduct_detail.doc_notice_id as doc_notice_id',
            //             'dept_deduct.for_year as for_year',
            //             'dept_deduct.for_month as for_month',
            //             'emp_data.ID_Card as ID_Card',
            //             'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname',
            //             'dept_deduct.id as id_dept_deduct',
            //             'dept_deduct_detail.add_deduct_template_id as add_deduct_template_id',
            //             'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname',
            //             'dept_deduct_detail.dept_deduct_amount as dept_amount',
            //             'dept_deduct_detail.add_deduct_template_name as add_deduct_template_name',
            //             'dept_deduct_detail.id as dept_deduct_detail_id',
            //             'dept_deduct_detail.is_record_approved as is_record_approved',
            //             'dept_deduct.create_by as create_by'
            //             ])
            //     ->from('emp_data')
            //     ->innerJoin('dept_deduct_detail','dept_deduct_detail.id_card = emp_data.ID_Card')
            //     ->innerJoin('dept_deduct','dept_deduct.id = dept_deduct_detail.dept_deduct_id')
            //     ->innerJoin('ADD_DEDUCT_TEMPLATE','ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = dept_deduct_detail.add_deduct_template_id')
            //     ->innerJoin('working_company','working_company.id = dept_deduct.company_id')
            //     ->where('dept_deduct.company_id = :id_company
            //         AND  dept_deduct.for_month = :for_month
            //         AND  dept_deduct.for_year = :for_year
            //         AND  emp_data.username != "#####Out Off###"
            //         AND  dept_deduct_detail.is_record_approved != "99"')
            //     ->addParams([':id_company' => $id_company,
            //                  ':for_month'=> $for_month,
            //                  ':for_year'=> $for_year,])
            //     ->asArray()
            //     ->all();

            // return $data;  

            $sql = "SELECT  $db[ou].working_company.name as workingname,
                        $db[pl].dept_deduct_detail.doc_notice_id as doc_notice_id,
                        $db[pl].dept_deduct.for_year as for_year,
                        $db[pl].dept_deduct.for_month as for_month,
                        $db[ct].emp_data.ID_Card as ID_Card,
                        CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                        $db[pl].dept_deduct.id as id_dept_deduct,
                        $db[pl].dept_deduct_detail.add_deduct_template_id as add_deduct_template_id,
                        $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as tempname,
                        $db[pl].dept_deduct_detail.dept_deduct_amount as dept_amount,
                        $db[pl].dept_deduct_detail.add_deduct_template_name as add_deduct_template_name,
                        $db[pl].dept_deduct_detail.id as dept_deduct_detail_id,
                        $db[pl].dept_deduct_detail.is_record_approved as is_record_approved,
                        $db[pl].dept_deduct_detail.create_by as create_by
                  FROM $db[ct].emp_data
                  INNER JOIN $db[pl].dept_deduct_detail ON $db[ct].emp_data.ID_Card = $db[pl].dept_deduct_detail.id_card 
                  INNER JOIN $db[pl].dept_deduct ON $db[pl].dept_deduct.id = $db[pl].dept_deduct_detail.dept_deduct_id 
                  INNER JOIN $db[pl].ADD_DEDUCT_TEMPLATE ON $db[pl].ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = $db[pl].dept_deduct_detail.add_deduct_template_id
                  INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].dept_deduct.company_id
                  WHERE $db[ct].emp_data.Prosonnal_Being <> '3'
                  AND   $db[ct].emp_data.username <> '#####Out Off###'
                  AND   $db[pl].dept_deduct.company_id = $id_company
                  AND   $db[pl].dept_deduct.for_month = $for_month
                  AND   $db[pl].dept_deduct.for_year = $for_year
                  AND   $db[pl].dept_deduct_detail.is_record_approved != '99'";

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryAll();
            return $data;
        }

    }

    public static function saveUpdateapproveddeduct($arrpk_dept)
    {

        // $id_pk = "1,2";
        $idupdate = Yii::$app->session->get('idcard');
        $idapproved = Yii::$app->session->get('idcard');
        $inarr = implode(",", $arrpk_dept);
        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {
            $model = Deptdeductdetail::updateAll([
                'is_record_approved' => 1,
                'record_approved_datetime' => new Expression('NOW()'),
                'record_approved_by' => $idapproved,
                'update_by' => $idupdate,
                'update_datetime' => new Expression('NOW()'),
            ], 'id IN (' . $inarr . ')');

            if ($model !== false) {
                $id_create = Yii::$app->session->get('idcard');
                $modelselect = Deptdeductdetail::find()
                    ->where(['in', 'id', $arrpk_dept])
                    ->asArray()
                    ->all();
                $modelInsert = [];
                foreach ($modelselect as $key => $value) {

                   // $valueStartPaydate = substr($value['record_approved_datetime'], 5, 2);
                    //$valueYearPaydate = substr($value['record_approved_datetime'], 0, 4);
                    $arrDateRange = DateTime::makeDayFromMonthPicker($value['pay_date']);

                    $modelInsert[$key]['ADD_DEDUCT_ID'] = $value['add_deduct_template_id'];
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL'] = $value['record_comment'];
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_PAY_DATE'] = $value['pay_date'];
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_START_USE_DATE'] = $arrDateRange['start_date'];
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_END_USE_DATE'] = $arrDateRange['end_date'];
                    $modelInsert[$key]['INSTALLMENT_START'] = null;
                    $modelInsert[$key]['INSTALLMENT_END'] = null;
                    $modelInsert[$key]['INSTALLMENT_CURRENT'] = null;
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_EMP_ID'] = $value['id_card'];
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_AMOUNT'] = $value['dept_deduct_amount'];
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_TYPE'] = '2'; // เฉพาะเดือน
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_STATUS'] = '1';
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_HISTORY_STATUS'] = null;
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_GROUP_STATUS'] = null;
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_CREATE_DATE'] = new Expression('NOW()');
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_CREATE_BY'] = $id_create;
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_UPDATE_BY'] = null;
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_UPDATE_DATE'] = null;
                    $modelInsert[$key]['ADD_DEDUCT_DETAIL_RESP_PERSON'] =  $id_create;
                    $modelInsert[$key]['dept_deduct_id'] = null; //set because change req by aeedy
                    $modelInsert[$key]['dept_deduct_detail_id'] = $value['id'];
                }

                $connection->createCommand()
                    ->batchInsert('ADD_DEDUCT_DETAIL', ['ADD_DEDUCT_ID',
                        'ADD_DEDUCT_DETAIL',
                        'ADD_DEDUCT_DETAIL_PAY_DATE',
                        'ADD_DEDUCT_DETAIL_START_USE_DATE',
                        'ADD_DEDUCT_DETAIL_END_USE_DATE',
                        'INSTALLMENT_START',
                        'INSTALLMENT_END',
                        'INSTALLMENT_CURRENT',
                        'ADD_DEDUCT_DETAIL_EMP_ID',
                        'ADD_DEDUCT_DETAIL_AMOUNT',
                        'ADD_DEDUCT_DETAIL_TYPE',
                        'ADD_DEDUCT_DETAIL_STATUS',
                        'ADD_DEDUCT_DETAIL_HISTORY_STATUS',
                        'ADD_DEDUCT_DETAIL_GROUP_STATUS',
                        'ADD_DEDUCT_DETAIL_CREATE_DATE',
                        'ADD_DEDUCT_DETAIL_CREATE_BY',
                        'ADD_DEDUCT_DETAIL_UPDATE_BY',
                        'ADD_DEDUCT_DETAIL_UPDATE_DATE',
                        'ADD_DEDUCT_DETAIL_RESP_PERSON',
                        'dept_deduct_id',
                        'dept_deduct_detail_id'],
                        $modelInsert)
                    ->execute();
                $transaction->commit();
                return true;
            } else {
                return "error";
            }
        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }
    }

    public static function mergeDeduct($arr1, $arr2)
    {
        // $arr1 = ["1509901325106"=>17000.00,"1579900562503"=>5000.00,"111111111111"=>200.00];
        // $arr2 = ["1509901325106"=>1600.00,"9999999999999"=>3000.00];

        $arrDif = array_diff_key($arr2, $arr1);

        $arrFinal = [];
        foreach ($arr1 as $key => $value) {
            $arrItem = [];
            $arrItem[] = $value;
            if (isset($arr2[$key])) {
                $arrItem[] = $arr2[$key];
            }
            // $arrItem[] = (isset($arr2[$key])) ? $arr2[$key] :0;
            // $arrItem[] = (isset($arr2[$key])) ? $arr2[$key] : 0;
            $arrFinal[$key] = $arrItem;
        }

        foreach ($arrDif as $key => $value) {
            $arrItem = [];
            $arrItem[$key][] = $value;
            $arrFinal[$key] = $arrItem[$key];
        }

        // echo '<pre>';
        // print_r($arrDif);
        //  echo '<pre>';


        // echo '<pre>';
        // print_r($arrFinal);
        //  echo '<pre>';

        return ['arrFinal' => $arrFinal];


    }

    public static function mapPayStatus($status)
    {

        if ($status == Yii::$app->params['ACTIVE_STATUS']) {
            return Yii::$app->params['PAYROLL']['PAY_ACTIVE_LABEL'];
        } else {
            return Yii::$app->params['PAYROLL']['PAY_NOTACTIVE_LABEL'];
        }
    }


    public static function getAllStaffHaveIncome($arrSalaryAll)
    {

    }


    public static function getTaxReduce($idcard=null)
    {

    }


}

?>

