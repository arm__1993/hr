<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property string $code_name
 * @property integer $department
 * @property string $name
 * @property integer $status
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_name', 'department', 'name'], 'required'],
            [['department', 'status'], 'integer'],
            [['code_name'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_name' => 'Code Name',
            'department' => 'Department',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
     public function attributes()
    {
        return array_merge(parent::attributes(), [
            'sectionid',
            'sectioncode_name', 
            'sectiondepartment', 
            'sectionname',
            'sectionid',
            'departmentid',
            'departmentcode_name', 
            'departmentcompany', 
            'departmentname', 
            'companyid']);
    }
}
