<?php

namespace app\modules\baymanagement\controllers;

use app\api\Helper;
use app\modules\baymanagement\models\EholeBaylayout;
use app\modules\baymanagement\models\EholeJobLevelmaster;
use app\modules\baymanagement\models\EholeTake;
use app\modules\baymanagement\models\EholeTakeLevel;
use app\modules\baymanagement\models\EholeTechnicianLevelmaster;
use app\modules\baymanagement\models\ErepairGroup;
use app\modules\hr\apihr\ApiHr;
use yii\web\NotFoundHttpException;
use app\modules\baymanagement\models\EholeBaydetail;
//use app\modules\hr\models\Education;
use yii\web\Controller;
use Yii;
use yii\db\Expression;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `baymanagement` module
 */
//class DefaultController extends MasterController
class DefaultController extends Controller
{
    public $layout = 'baylayout';


    public $idcardLogin;
    public $idcompanyname;

    public function init()
    {
        $session = Yii::$app->session;
        if (!$session->has('fullname') || !$session->has('idcard')) {
            return $this->redirect(array('/auth/default/logout'), 302);
        }
        $this->idcompanyname = $session->get('companyid');
        $this->idcardLogin = $session->get('idcard');

    }

    /**
     * Renders the index view for the module
     * @return string
     */


    ////////////////////////////////////////index//////////////////////////////////////////////////////

    public function actionBaymanagement()
    {
        $data2 = ErepairGroup::find()
            ->where(['NOT IN', 'status', [99]])
            ->asArray()
            ->all();

        $arryItem2 = [];
        foreach ($data2 as $row2) {
            $arryItem2[$row2['id']] = $row2['name'];
        }


        $data1 = EholeTakeLevel::find()
            ->where(['NOT IN', 'status_active', [99]])
            ->asArray()
            ->orderBy([
                'emp_idcard' => SORT_ASC
            ])
            ->all();

        $arryItem = [];
        foreach ($data1 as $row) {
            $arryItem[$row['emp_idcard']][$row['erepair_group_id']] = $arryItem2[$row['erepair_group_id']];


            //   $arryItem[$row['emp_idcard']];


            //  $arryItem[$row['1570100016972']] = $arryItem2[$row['erepair_group_id']];

            // $arryItem[$row['emp_idcard']]=[$arryItem2[$row['erepair_group_id']]] ;

        }
        //  self::printobj ($arryItem);
        //$arrTemp = $arryItem;
        $arrItemData = [];
        foreach ($arryItem as $idcard => $items) {
            //$arr1Dim  = $arrTemp[$idcard];
            // $items['emp_idcard'];
            //$xxx = join(',',$arr1Dim);
            $arrData = [];
            foreach ($items as $erepair_id => $erepair_name) {
                $key = "($erepair_id) ";
                $text = $erepair_name;
                $arrData[] = $key . $text;
            }

            $arrItemData[$idcard] = join(',', $arrData);
        }


        // self::printobj ($arryItem);
        //        exit();

        // self::printobj($data1);


        $showeholetake = new EholeTake();
        $showeholetakeindex = $showeholetake->search(\Yii::$app->request->queryParams);


        $showeholetakelevel = new EholeTakeLevel();


        $showeholetakelevelindex = $showeholetakelevel->search(\Yii::$app->request->queryParams);


        //  self::printobj($showeholetakelevelindex);


        $showerepairgroup = new ErepairGroup();
        $showerepairgroupindex = $showerepairgroup->search(\Yii::$app->request->queryParams);
        $showerepairgroupfor = ErepairGroup::find()->where('status!= 99')->asArray()->all();


        $modeljob = new EholeJobLevelmaster();
        $modeljobindex = $modeljob->search(Yii::$app->request->queryParams);
        $modeljobfor = EholeJobLevelmaster::find()->where('status_active != 99')->asArray()->all();
        $modelcvhname = EholeTake::find()->where('status_available != 99')->asArray()->all();


        $modeleholelevelmaster = new EholeTechnicianLevelmaster();
        $modeleholelevelmasterindex = $modeleholelevelmaster->search(Yii::$app->request->queryParams);
        $modeleholelevelmasterfor = EholeTechnicianLevelmaster::find()->where('status_active != 99')->asArray()->all();
        $companyname = $this->idcompanyname;


        $showelevelmaster = new EholeTechnicianLevelmaster();
        $showelevelmasterindex = $showelevelmaster->search(\Yii::$app->request->queryParams);

        $showeholejoblevelmaster = new EholeJobLevelmaster();
        $showeholejoblevelmasterindex = $showeholejoblevelmaster->search(\Yii::$app->request->queryParams);

        $model3 = EholeBaylayout::find()
            ->where(['NOT IN', 'status_active', [99]])
            ->andWhere([
                'branch_id' => $companyname
            ])
            ->asArray()
            ->one();
        return $this->render('baymanagement', [
            'imgttr' => $model3,
            'showeholejoblevelmaster' => $showeholejoblevelmaster,
            'showeholejoblevelmasterindex' => $showeholejoblevelmasterindex,

            'showelevelmaster' => $showelevelmaster,
            'showelevelmasterindex' => $showelevelmasterindex,

            'showeholetake' => $showeholetake,
            'showeholetakeindex' => $showeholetakeindex,

            'showeholetakelevel' => $showeholetakelevel,
            'showeholetakelevelindex' => $showeholetakelevelindex,

            'showerepairgroup' => $showerepairgroup,
            'showerepairgroupindex' => $showerepairgroupindex,
            'modelcvhname' => $modelcvhname,

            'modeljob' => $modeljob,
            'modeljobid' => $modeljobindex,
            'modeljobfor' => $modeljobfor,

            'modeleholelevelmaster' => $modeleholelevelmaster,
            'modeleholelevelmasterindex' => $modeleholelevelmasterindex,
            'modeleholelevelmasterfor' => $modeleholelevelmasterfor,

            'showerepairgroupfor' => $showerepairgroupfor,

            'arrItemData' => $arrItemData,
        ]);
    }


    ////////////////////////////////////////END index//////////////////////////////////////////////////////


    public function actionIndex()
    {
        $companyname = $this->idcompanyname;
        $model3 = EholeBaylayout::find()
            ->where(['NOT IN', 'status_active', [99]])
            ->andWhere([
                'branch_id' => $companyname
            ])
            ->asArray()
            ->one();
        return $this->render('index', [
            'imgttr' => $model3
        ]);

    }

    public function actionGetindex()
    {

        $companyname = $this->idcompanyname;
        if (($model = EholeBaydetail::find()
                ->where([
                        'NOT IN', 'status_active', [99]  //ไม่แสดง 99
                    ]
                )
                ->andWhere(['like', 'branch_id', [$companyname]])
                ->all()) !== null) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    public function actionSaveboxx()
    {
        $login = $this->idcardLogin;
        $companyname = $this->idcompanyname;
        $connection = EholeBaydetail::getDb();
        $transaction = $connection->beginTransaction();

        try {
            $_table = "ehole_baydetail";
            $addDeDuctColumns = EholeBaydetail::getTableSchema()->getColumnNames();
            $var = Yii::$app->request->post();
            $data = json_decode($var['data']);
            $arrData = null;
            foreach ($data as $no => $inf) {

                if ($inf->comm == 'new') { //เพิ่ม
                    $arrData[] = [
                        $addDeDuctColumns[0] => null,
                        $addDeDuctColumns[1] => null,
                        $addDeDuctColumns[2] => $inf->name,
                        $addDeDuctColumns[3] => $companyname,
                        $addDeDuctColumns[4] => null,
                        $addDeDuctColumns[5] => null,
                        $addDeDuctColumns[6] => $inf->x,
                        $addDeDuctColumns[7] => $inf->y,
                        $addDeDuctColumns[8] => $inf->color,
                        $addDeDuctColumns[9] => '1',
                        $addDeDuctColumns[10] => $login,
                        $addDeDuctColumns[11] => $login,
                        $addDeDuctColumns[12] => new Expression('NOW()'),
                        $addDeDuctColumns[13] => new Expression('NOW()')

                    ];
                } else if ($inf->comm == 'edit') { //แก้ไข
                    $model = EholeBaydetail::findOne(['id' => $inf->id_box]);
                    $model->bay_x = $inf->x;
                    $model->bay_y = $inf->y;
                    $model->save();


                }

            }
            if (count($arrData) > 0) {
                $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $datadel = json_decode($var['datadel']); //ลบบบบบบ
        foreach ($datadel as $no => $box_id) {
            $model = EholeBaydetail::findOne(['id' => $box_id]);
            $model->status_active = '99';
            $model->save();
        }
        print_r($datadel);


        print_r($addDeDuctColumns);
    }

////////////////////////////////////////////////////////////////////////baylayout//////////////////////////////////////////////////////////////////////////////////////
    public function actionBaylayout()
    {

        $companyname = $this->idcompanyname;
        $model3 = EholeBaylayout::find()
            ->where(['NOT IN', 'status_active', [99]])
            ->andWhere([
                'branch_id' => $companyname
            ])
            ->asArray()
            ->one();
        return $this->render('baylayout', [

            'c' => $model3
        ]);
    }

    public function actionUplodeimg()
    {


        $fileObj = $_FILES['laout_upload'];

        $y = explode(".", $fileObj['name']);

        $ad = $y['1'];

        if ($ad != 'jpg') {

            echo 'ไม่สามารถบันทึกได้';
            return false;
        }


        $new_name = rand() . "." . $y['1'];

        echo($new_name);


        $strJson = Yii::$app->request->post();
        print_r($strJson);

        $sDir = "./upload/baymanagement/";//"/upload/personal/";
        $filename = $new_name;
        $tmp_name = $fileObj['tmp_name'];

        //  echo  ( $filename);


        if (move_uploaded_file($tmp_name, $sDir . $filename)) {
            $res['status'] = true;

            $login = $this->idcardLogin;
            $companyname = $this->idcompanyname;

            $model = new EholeBaylayout();
            $model->branch_id = $companyname;
            $model->layout_name = $companyname;
            $model->background_image = $filename;
            $model->status_active = 1;
            $model->createby_user = $login;
            $model->create_datetime = new Expression('NOW()');
            $model->updateby_user = $login;
            $model->update_datetime = new Expression('NOW()');
            if ($model->save()) {
                echo 'เซฟแล้วครับ';
            }


            /*$connection = EholeBaylayout::getDb();
            $transaction = $connection->beginTransaction();

            try {
                $_table = "ehole_baylayout";
                $addDeDuctColumns = EholeBaylayout::getTableSchema()->getColumnNames();
                $arrData = null;


                $arrData[] = [
                    $addDeDuctColumns[0] => null,
                    $addDeDuctColumns[1] => $companyname,
                    $addDeDuctColumns[2] => $companyname,
                    $addDeDuctColumns[3] => $filename,
                    $addDeDuctColumns[4] => '1',
                    $addDeDuctColumns[5] => $login,
                    $addDeDuctColumns[6] => $login,
                    $addDeDuctColumns[7] => new Expression('NOW()'),
                    $addDeDuctColumns[8] => new Expression('NOW()')

                ];


                if (count($arrData) > 0) {
                    $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }*/


        } else {

            $res['status'] = false;
            $res['msg'] = "copy file error.";
        }


    }

////////////////////////////////////////////////////////////////////////baylayout//////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////technician//////////////////////////////////////////////////////////////////////////////////////

    public function printobj($id)
    {
        echo '<pre>';
        print_r($id);
        echo '</pre>';
        //exit();

    }

    public function actionTechnician()
    {


        $data2 = ErepairGroup::find()
            ->where(['NOT IN', 'status', [99]])
            ->asArray()
            ->all();

        $arryItem2 = [];
        foreach ($data2 as $row2) {
            $arryItem2[$row2['id']] = $row2['name'];
        }


        $data1 = EholeTakeLevel::find()
            ->where(['NOT IN', 'status_active', [99]])
            ->asArray()
            ->orderBy([
                'emp_idcard' => SORT_ASC
            ])
            ->all();

        $arryItem = [];
        foreach ($data1 as $row) {
            $arryItem[$row['emp_idcard']][$row['erepair_group_id']] = $arryItem2[$row['erepair_group_id']];


            //   $arryItem[$row['emp_idcard']];


            //  $arryItem[$row['1570100016972']] = $arryItem2[$row['erepair_group_id']];

            // $arryItem[$row['emp_idcard']]=[$arryItem2[$row['erepair_group_id']]] ;

        }
        //  self::printobj ($arryItem);
        //$arrTemp = $arryItem;
        $arrItemData = [];
        foreach ($arryItem as $idcard => $items) {
            //$arr1Dim  = $arrTemp[$idcard];
            // $items['emp_idcard'];
            //$xxx = join(',',$arr1Dim);
            $arrData = [];
            foreach ($items as $erepair_id => $erepair_name) {
                $key = "($erepair_id) ";
                $text = $erepair_name;
                $arrData[] = $key . $text;
            }

            $arrItemData[$idcard] = join(',', $arrData);
        }





        $showeholetake = new EholeTake();
        $showeholetakeindex = $showeholetake->search(\Yii::$app->request->queryParams);


        $showeholetakelevel = new EholeTakeLevel();


        $showeholetakelevelindex = $showeholetakelevel->search(\Yii::$app->request->queryParams);


        //  self::printobj($showeholetakelevelindex);


        $showerepairgroup = new ErepairGroup();
        $showerepairgroupindex = $showerepairgroup->search(\Yii::$app->request->queryParams);
        $showerepairgroupfor = ErepairGroup::find()->where('status!= 99')->asArray()->all();


        $modeljob = new EholeJobLevelmaster();
        $modeljobindex = $modeljob->search(Yii::$app->request->queryParams);
        $modeljobfor = EholeJobLevelmaster::find()->where('status_active != 99')->asArray()->all();
        $modelcvhname = EholeTake::find()->where('status_available != 99')->asArray()->all();


        $modeleholelevelmaster = new EholeTechnicianLevelmaster();
        $modeleholelevelmasterindex = $modeleholelevelmaster->search(Yii::$app->request->queryParams);
        $modeleholelevelmasterfor = EholeTechnicianLevelmaster::find()->where('status_active != 99')->asArray()->all();

        //self::printobj($showerepairgroupfor);


        return $this->render('technician', [

            'showeholetake' => $showeholetake,
            'showeholetakeindex' => $showeholetakeindex,

            'showeholetakelevel' => $showeholetakelevel,
            'showeholetakelevelindex' => $showeholetakelevelindex,

            'showerepairgroup' => $showerepairgroup,
            'showerepairgroupindex' => $showerepairgroupindex,
            'modelcvhname' => $modelcvhname,

            'modeljob' => $modeljob,
            'modeljobid' => $modeljobindex,
            'modeljobfor' => $modeljobfor,

            'modeleholelevelmaster' => $modeleholelevelmaster,
            'modeleholelevelmasterindex' => $modeleholelevelmasterindex,
            'modeleholelevelmasterfor' => $modeleholelevelmasterfor,

            'showerepairgroupfor' => $showerepairgroupfor,

            'arrItemData' => $arrItemData,


        ]);
    }


    /*  public function actionTest()
      {
          $idcard = '1560100306369';
          $data = ApiHr::getSeachDataPersonalByIdcard($idcard);

          print_r($data['Name']);
      }*/


    public function actionSavetechnician()//--เพิ่ม--//
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();


            $login = $this->idcardLogin;
            $companyname = $this->idcompanyname;

            $idcard = $post['techician_name_sername'];
            $data = ApiHr::getSeachDataPersonalByIdcard($idcard);


            $idlevel = $post['erepair_group_id'];
            $data2 = ErepairGroup::findOne($idlevel);

            //self::printobj($data);

            $id3 = $post['technician_level_id'];
            $data3 = EholeTechnicianLevelmaster::findOne($id3);
            /*self::printobj($data3);*/
            //exit();

            if ($post['hide_activityedit_technician'] != '') {
                $getmodel = EholeTakeLevel::find()
                    ->where(['emp_idcard' => $idcard])
                    ->asArray()
                    ->all();
                $arrEregruopDB = [];
                foreach ($getmodel as $item) {
                    $arrEregruopDB[$item['erepair_group_id']] = $item['erepair_group_id'];
                }


                // exit();
                $arrEregruopPost = $post['erepair_group_id'];
                $arrmerge = array_merge($arrEregruopDB, $arrEregruopPost);
                $arrdif = array_diff($arrmerge, $arrEregruopDB);
                $arrLostItem = [];
                foreach ($arrEregruopDB as $item) {
                    if (in_array($item, $arrEregruopPost)) {
                    } else {
                        array_push($arrLostItem, $item);
                    }
                }
                EholeTakeLevel::updateAll(['status_active' => 99], ['emp_idcard' => $data['ID_Card'], 'erepair_group_id' => $arrLostItem]);
                //insert new

                foreach ($arrdif as $item) {

                    $model = new EholeTakeLevel();
                    $model->branch_id = $companyname;
                    $model->data_no = $data['DataNo'];
                    $model->emp_idcard = $data['ID_Card'];
                    $model->emp_code = $data['Code'];
                    $model->technician_level_id = $data3['id'];
                    $model->level_code = $data3['level_code'];
                    $model->technician_level_name = $data3 ['technician_level_name'];
                    $model->erepair_group_id = $item;
                    $model->status_active = ($post['status_active']) ? $post['status_active'] : 1;
                    $model->createby_user = $login;
                    $model->updateby_user = $login;
                    $model->create_datetime = new Expression('NOW()');
                    $model->update_datetime = new Expression('NOW()');

                    if ($model->save() !== false) {
                        echo '1';
                    } else {
                        $errors = $model->errors;
                        echo "model can't validater <pre>";
                        print_r($errors);
                        echo "</pre>";

                    }
                }


            } else {

                $getmodel = EholeTakeLevel::find()
                    ->where(['emp_idcard' => $idcard])
                    ->asArray()
                    ->all();
                $cek = ($getmodel[0]['emp_idcard']);
                // $arrcatid = ( $data['ID_Card']);

                if ($cek != null) {

                    return false;
                }


                foreach ($post['erepair_group_id'] as $k => $v) {
                    $model = new EholeTakeLevel();
                    $model->branch_id = $companyname;
                    $model->data_no = $data['DataNo'];
                    $model->emp_idcard = $data['ID_Card'];
                    $model->emp_code = $data['Code'];
                    $model->technician_level_id = $data3['id'];
                    $model->level_code = $data3['level_code'];
                    $model->technician_level_name = $data3 ['technician_level_name'];
                    $model->erepair_group_id = $v;
                    $model->status_active = 1;
                    $model->createby_user = $login;
                    $model->updateby_user = $login;
                    $model->create_datetime = new Expression('NOW()');
                    $model->update_datetime = new Expression('NOW()');


                    $model->save();
                    //   self::printobj($model);
                    if ($model->save() !== false) {
                        echo true;
                    } else {
                        $errors = $model->errors;
                        echo "model can't validater <pre>";
                        print_r($errors);
                        echo "</pre>";

                    }
                }
            }
        }
    }

    public
    function actionSavetechnicianss()
    {
        $login = $this->idcardLogin;
        $companyname = $this->idcompanyname;
        $connection = EholeTakeLevel::getDb();
        $transaction = $connection->beginTransaction();

        try {
            $_table = "ehole_take_level";
            $addDeDuctColumns = EholeTakeLevel::getTableSchema()->getColumnNames();
            $var = Yii::$app->request->post();
            $data = json_decode($var['data']);
            $arrData = null;
            foreach ($data as $no => $inf) {

                if ($inf->comm == 'new') { //เพิ่ม
                    $arrData[] = [
                        $addDeDuctColumns[0] => null,
                        $addDeDuctColumns[1] => null,
                        $addDeDuctColumns[2] => $inf->name,
                        $addDeDuctColumns[3] => $companyname,
                        $addDeDuctColumns[4] => null,
                        $addDeDuctColumns[5] => null,
                        $addDeDuctColumns[6] => $inf->x,
                        $addDeDuctColumns[7] => $inf->y,
                        $addDeDuctColumns[8] => $inf->color,
                        $addDeDuctColumns[9] => '1',
                        $addDeDuctColumns[10] => $login,
                        $addDeDuctColumns[11] => $login,
                        $addDeDuctColumns[12] => new Expression('NOW()'),
                        $addDeDuctColumns[13] => new Expression('NOW()')

                    ];
                } else if ($inf->comm == 'edit') { //แก้ไข
                    $model = EholeBaydetail::findOne(['id' => $inf->id_box]);
                    $model->bay_x = $inf->x;
                    $model->bay_y = $inf->y;
                    $model->save();


                }

            }
            if (count($arrData) > 0) {
                $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $datadel = json_decode($var['datadel']); //ลบบบบบบ
        foreach ($datadel as $no => $box_id) {
            $model = EholeBaydetail::findOne(['id' => $box_id]);
            $model->status_active = '99';
            $model->save();
        }
        print_r($datadel);


        print_r($addDeDuctColumns);
    }


    public
    function actionUpdatetechnician()//--แก้ไข--//
    {

        if (Yii::$app->request->isAjax) {
            $datas = Yii::$app->request->post();


            $model = EholeTakeLevel::find()
                ->where(['emp_idcard' => $datas])
                ->andwhere([
                    'technician_level_id' => $datas
                ])
                ->andWhere([
                    'NOT IN', 'status_active', [99]
                ])
                ->asArray()
                ->all();


            //  self::printobj ($model);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //  self::printobj($model);
            //  exit();
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');

            /*            if (($model = EholeTakeLevel::find
                            (['emp_idcard' => $id])) !== null) {
                            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                            return $model;
                        } else {
                            throw new NotFoundHttpException('The requested page does not exist.');
                        }*/
        }
    }


    public
    function actionDeletetechnicians() //--ลบ--//
    {

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            //EholeTakeLevel::updateAll(['status_active' => Yii::$app->params['DELETE_STATUS']]

            $model = EholeTakeLevel::updateAll(['status_active' => 99], ['emp_idcard' => $post['emp_idcard'], 'technician_level_id' => $post['technician_level_id']]);


            //  self::printobj($model);

            //    $model->status_active = Yii::$app->params['DELETE_STATUS'];

            /*$model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }*/
        }
    }


    public
    function actionDeletetechniciansaaaa() //--ลบ--//
    {

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            //EholeTakeLevel::updateAll(['status_active' => Yii::$app->params['DELETE_STATUS']]

            $model = EholeTakeLevel::updateAll(['status_active' => 99], ['like', 'emp_idcard', '1570300109940']);


            //  self::printobj($model);

            //    $model->status_active = Yii::$app->params['DELETE_STATUS'];

            /*$model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }*/
        }
    }

////////////////////////////////////////////////////////////////////////job//////////////////////////////////////////////////////////////////////////////////////
    public
    function actionEholejoblevelmaster()
    {


        $showeholejoblevelmaster = new EholeJobLevelmaster();
        $showeholejoblevelmasterindex = $showeholejoblevelmaster->search(\Yii::$app->request->queryParams);


        //   $modelcvhname = EholeTake::find()->where('status_available != 99')->asArray()->all();


        return $this->render('eholejoblevelmaster', [

            'showeholejoblevelmaster' => $showeholejoblevelmaster,
            'showeholejoblevelmasterindex' => $showeholejoblevelmasterindex,


        ]);
    }

    public
    function actionSaveeholejoblevelmaster()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/

            $login = $this->idcardLogin;
            if ($post['hide_activityedit_job'] != '') {
                $model = EholeJobLevelmaster::findOne(['id' => $post['hide_activityedit_job']]);
                $model->job_level_name = $post['job_level_name'];
                $model->status_active = ($post['status_active']) ? $post['status_active'] : 0;
                $model->createby_user = $login;
                $model->updateby_user = $login;
                $model->create_datetime = new Expression('NOW()');
                $model->update_datetime = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new EholeJobLevelmaster();
                $model->job_level_name = $post['job_level_name'];
                $model->status_active = ($post['status_active']) ? $post['status_active'] : 0;
                $model->createby_user = $login;
                $model->updateby_user = $login;
                $model->create_datetime = new Expression('NOW()');
                $model->update_datetime = new Expression('NOW()');

                $model->save();
                //   self::printobj($model);
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }

    public
    function actionUpdateeholejoblevelmaster()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = EholeJobLevelmaster::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public
    function actionDeleteeholejoblevelmaster()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = EholeJobLevelmaster::findOne(['id' => $post['id']]);

            $model->status_active = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }

////////////////////////////////////////////////////////////////////////ehole_technician_levelmaster//////////////////////////////////////////////////////////////////////////////////////
    public
    function actionLevelmaster()
    {


        $showelevelmaster = new EholeTechnicianLevelmaster();
        $showelevelmasterindex = $showelevelmaster->search(\Yii::$app->request->queryParams);


        //   $modelcvhname = EholeTake::find()->where('status_available != 99')->asArray()->all();

        //self::printobj ($showelevelmasterindex);($showelevelmasterindex);
        return $this->render('levelmaster', [

            'showelevelmaster' => $showelevelmaster,
            'showelevelmasterindex' => $showelevelmasterindex,


        ]);
    }

    public
    function actionSavelevelmaster()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            /* print_r($post);
             exit();*/

            $companyname = $this->idcompanyname;
            $login = $this->idcardLogin;
            if ($post['hide_activityedit_jobmaster'] != '') {
                $model = EholeTechnicianLevelmaster::findOne(['id' => $post['hide_activityedit_jobmaster']]);
                $model->technician_level_name = $post['technician_level_name'];
                $model->level_code =
                $model->branch_id = $companyname;
                $model->status_active = ($post['status_active']) ? $post['status_active'] : 0;
                $model->createby_user = $login;
                $model->updateby_user = $login;
                $model->create_datetime = new Expression('NOW()');
                $model->update_datetime = new Expression('NOW()');

                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = new EholeTechnicianLevelmaster();
                $model->technician_level_name = $post['technician_level_name'];
                $model->level_code =
                $model->branch_id = $companyname;
                $model->status_active = ($post['status_active']) ? $post['status_active'] : 0;
                $model->createby_user = $login;
                $model->updateby_user = $login;
                $model->create_datetime = new Expression('NOW()');
                $model->update_datetime = new Expression('NOW()');
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }

    public
    function actionDeletelevelmaster()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = EholeTechnicianLevelmaster::findOne(['id' => $post['id']]);

            $model->status_active = Yii::$app->params['DELETE_STATUS'];

            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
                // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }

    public
    function actionUpdatelevelmaster()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = EholeTechnicianLevelmaster::findOne(['id' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }


    public
    function actionLevelmasters()
    {

        $std = array('a' => 'aaa',
            'b' => 'bbb',
            'c' => 'xxc',
            'd' => 'ddd');

        self::printobj('A ==>');
        self::printobj($std);
        $add = array('x' => 'ddd',
            'y' => 'xxc',
            'z' => 'dssa');
        self::printobj('A ==>');
        self::printobj($add);

        print_r('ค่าซ้ำ');
        $difs = array_intersect($add, $std);
        foreach ($difs as $k => $v) {

            self::printobj("[$k]:[$v]");

        }

        print_r('ค่าต่าง......................................................');
        $dif = array_diff($std, $add);
        foreach ($dif as $k => $v) {

            self::printobj("[$k]:[$v]");
        }


        exit();
        return $this->render('levelmasters', [

            'ffff' => $ffff
        ]);
    }

////////////////////////////////////////////////////////////////////////ehole_technician_levelmaster//////////////////////////////////////////////////////////////////////////////////////
}
