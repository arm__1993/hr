<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Mainjobs;
use yii\helpers\ArrayHelper;


class MainjobController extends ActiveController
{

   	public $modelClass = 'app\modules\wscommon\models\Mainjobs';

	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	public function actionSearchjobcusformobile()
    {
        //return 1;
        $Cusno = \Yii::$app->request->get('Cusno');
        $datajob = new Mainjobs();
        $listJob = $datajob->searchjobcusformobile($Cusno);
        return $listJob;
    }

}
