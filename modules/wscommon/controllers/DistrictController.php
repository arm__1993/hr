<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Amaindistrict;
use yii\helpers\ArrayHelper;

class DistrictController extends ActiveController
{
    public $modelClass = 'app\modules\wscommon\models\Amaindistrict';


    public function behaviors()
    {
        return
            ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                ],
            ],
        ],
        parent::behaviors());
    }
    
    public function actionSearchamphurbyamphurid()
    {
        // $f = \Yii::$app->request->get('f');
        // $book = new Book();
        // $bookSearch = $book->searchbytitle($f);
        // return $bookSearch;
        $amphurid = \Yii::$app->request->get('amphurid');
        $district = new Amaindistrict();
        $districtSearch = $district->searchamphurbyamphurid($amphurid);
        return $districtSearch;
        //return 1;
    }


}
