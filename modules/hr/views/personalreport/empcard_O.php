<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:13
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

AppAsset::register($this);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/formEmpData.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/personalreport.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/dom-to-image-master/dist/dom-to-image.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/dom-to-image-master/dist/FileSaver.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java




?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li class="active">พิมพ์บัตรพนักงาน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-header with-border">
                <h3 class="box-title">พิมพ์บัตรพนักงาน</h3>
            </div>
            <div class="box-body" style="padding:20px">

                <div class="row search_content">
                   
                    <form role="form" id="empCardForm" class="form-horizontal  col-sm-11 col-sm-offset-1">

                        <div class="row ">
                            <div class="col-sm-5 form-group">
                                <label class="control-label col-sm-2" for="company">บริษัท:</label>
                                <div class="col-sm-10">
                                    <select id="company" name="company" style="width:333px"></select>
                                </div>
                            </div>
                            <div class="col-sm-7 form-group form-inline">
                                <label class="control-label" for="">วันที่เริ่มงาน</label>
                                <input type="date" class="form-control" style="width:178px" id="work_start_begin" >
                                <label class="control-label" for="work_start_end">ถึง</label>
                                <input type="date" class="form-control" style="width:178px"  id="work_start_end" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label class="control-label col-sm-2" for="department">แผนก:</label>
                                <div class="col-sm-10">
                                    <select id="department" name="department" style="width:333px"></select>
                                </div>
                            </div>
                            <div class="col-sm-7 form-group">
                                <label class="control-label col-sm-2" for="emp_code">รหัสพนักงาน</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" style="width:333px" id="emp_code" name="emp_code" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label class="control-label col-sm-2" for="section">ฝ่าย:</label>
                                <div class="col-sm-10">
                                    <select id="section" name="section" style="width:333px"></select>
                                </div>
                            </div>
                            <div class="col-sm-7 form-group">
                                <label class="control-label col-sm-2" for="emp_name">ชื่อ</label>
                                <div class="col-sm-7">
                                    <select multiple="" style="width:332px;display:none;" id="emp_name" name="emp_name">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-offset-3" style="margin-top:30px;padding-left:60px">
                                <button type="button" id="" class="btn btn-primary " onclick="empCardSearch()" style="margin-left:10px" onclick="">
                                    <i class="glyphicon glyphicon-search"></i> ค้นหา
                                </button>
                                <!--<button type="button" id="" class="btn btn-warning" style="margin-left:10px" onclick="">
                                    <i class="glyphicon glyphicon-refresh"></i> ล้างข้อมูล
                                </button>-->
                                <button type="button" disabled="disabled" id="empcardDownloadPng" class="btn btn-success" style="margin-left:10px" onclick="downloadEmpImageCard()">
                                    <i class="glyphicon glyphicon-download-alt"></i> download png
                                </button>
                                <button type="button" id="" class="btn btn-success" style="margin-left:10px" onclick="downloadEmpImageCardPDF()">
                                    <i class="glyphicon glyphicon-download-alt"></i> download pdf
                                </button>
                            </div>
                        </div>
                        
                    </form>

                </div>

                <div class="empcard_content_out" id="empcard_content_out_fix" style="padding-left:180px">
                    <div id="empcard_img_print" style="float:left">
                        <div class="empcard_content">
                            <div class="txt_bf_search" >ขนาด A4</div>
                           
                        </div>
                    </div>
                </div>
                
        
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->

<input type="hidden" value='<?php echo $companyinfo;?>' id="fullcompanyinfo" >
