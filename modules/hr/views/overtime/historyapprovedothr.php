<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 09:51
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/ot_historyapprovedhr.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/ot_historyapproved.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#"> ข้อมูลเงินเดือน</a>
                </li>
                <li>อนุมัติข้อมูลการทำงานล่วงเวลา</li>
                <li class="active">ประวัติการอนุมัติค่าล่วงเวลา (HR)</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>

        <form class="form-horizontal">
            <div class="box-header with-border">
                <h3 class="box-title">ค้นหาประวัติการอนุมัติค่าล่วงเวลา (HR)</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-1">
                        &nbsp;
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" name="company_id" id="company_id">
                            <option value="">เลือกบริษัท</option>
                            <?php $working = ApiHr::getWorking_company();
                            foreach ($working as $value) {
                                echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                            } ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="month_ot" class="col-sm-6 control-label">  รอบเงินเดือน </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control"  readonly="readonly" id="month_ot" name="month_ot" value="<?php echo date('m-Y');?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php
                            echo Html::button('<i class="fa fa-search"></i> ค้นหา',
                                [
                                    'class' => 'btn btn-primary',
                                    'id' => 'btnSearchApprovedHistory',
                                ]);
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">ประวัติการอนุมัติค่าล่วงเวลา (HR)</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive" id="dvloadot" style="padding: 20px;">
                        <img class="loading-image" src="../../images/global/ajax-loader.gif"
                             alt="loading..">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->