<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "command".
 *
 * @property integer $id
 * @property string $Leader
 * @property string $Leader_id_card
 * @property string $Follower
 * @property string $Follower_id_card
 * @property string $command_start
 * @property string $Command_start_use_date
 * @property integer $Status
 */
class Command extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'command';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Leader', 'Leader_id_card', 'Follower', 'Follower_id_card', 'command_start', 'Command_start_use_date'], 'required'],
            [['Leader_id_card', 'Follower_id_card'], 'string'],
            [['Command_start_use_date'], 'safe'],
            [['Status'], 'integer'],
            [['Leader', 'Follower'], 'string', 'max' => 20],
            [['command_start'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Leader' => 'Leader',
            'Leader_id_card' => 'Leader Id Card',
            'Follower' => 'Follower',
            'Follower_id_card' => 'Follower Id Card',
            'command_start' => 'Command Start',
            'Command_start_use_date' => 'Command Start Use Date',
            'Status' => 'Status',
        ];
    }
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'position_id',
            'id_card',
            'NameUse',
            'Note',
            'status',
            'relation_position_Start_date'
            ]);
    }
}
