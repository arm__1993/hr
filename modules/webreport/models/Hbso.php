<?php

namespace app\modules\webreport\models;

use Yii;

/**
 * This is the model class for table "hbso".
 *
 * @property integer $id
 * @property integer $order_type
 * @property string $order_date
 * @property double $order_amount
 */
class Hbso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hbso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_type', 'order_date', 'order_amount'], 'required'],
            [['order_type'], 'integer'],
            [['order_date'], 'safe'],
            [['order_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_type' => 'Order Type',
            'order_date' => 'Order Date',
            'order_amount' => 'Order Amount',
        ];
    }

    /*public function year($keyword)
    {
        $keyword = '('.$keyword.')';
        $year = Yii::$app->db
            ->createCommand("SELECT SUM(order_amount) FROM hbso 
                            WHERE YEAR(order_date) = 2015 GROUP BY MONTH:keyword")
            ->bindParam(':keyword', $keyword)
            ->queryAll();
        return ($year);
    }*/

    public function receivewage()
    {
        $company = $_POST['company'];
        $year = $_POST['year'];
    }
}
