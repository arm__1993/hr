<?php

namespace app\modules\hr\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Organizechart extends Model
{
    public $id;
    public $idReal;
    public $name;
    public $Emp_id=0;
    public $Emp_name;
    public $company;
    public $drepartment;
    public $ref_id;
    public $position;
    public $parent;
    public $level;
    public $type;
    public $status=1;
    public $active_status;
    public $img;
}
