$(document).ready(function() {
    $.get('vhc/grouplist', function(res) {
        $('#grouplist').html(res);
    });
    $.get('vhc/list', function(res) {
        $('#list').html(res);
    });
    $.get('vhc/listoption', function(res) {
        $('#listoption').html(res);
    });
    $.get('vhc/servicetype', function(res) {
        $('#servicetype').html(res);
    });
    $.get('vhc/blame', function(res) {
        $('#blame').html(res);
    });
    $.get('vhc/model', function(res) {
        $('#vhcmodel').html(res);
    });
    select_group();
    select_model();
});

function select_list() {
    var dropdrow = '';
    $.get('vhc/selectlist', function(res) {
        $.each(res, function(index, value) {
            dropdrow += '<option value="' + value.id + '"> ' + value.list_name + ' </option>';
        });
        $('#list_id').html(dropdrow).promise().done(function() {
            $('#list_id').SumoSelect({ search: true });
        });
    });
}

function select_model() {
    var dropdrow = '';
    $.get('vhc/selectmodel', function(res) {
        $.each(res, function(index, value) {
            dropdrow += '<option value="' + value.id + '"> ' + value.model_name + ' </option>';
        });
        $('#model_id').html(dropdrow).promise().done(function() {
            $('#model_id').SumoSelect({ search: true });
        });
    });
}

function modalshow_group(params) {
    $('#addDialog_group').modal('show');
}

function reset_group() {
    $('#form_group')[0].reset();
    modalshow_group();
}

function reset_blame(id = null) {
    $('#form_blame')[0].reset();
    if (id != null || id != 0) {
        modalshow_blame();
        $.get('vhc/editblame', { data: { id: id } }, function(res) {
            $.each(res, function(index, value) {
                if (index == 'record_status') {
                    if (value == '1') {
                        // $('#TypeCompany1').checked();
                        $("#record_status").prop('checked', true);
                    } else {
                        //$('#TypeCompany2').checked();
                        $("#record_status").prop('checked', false)
                    }
                }
                $('#' + index).val(value);
            });
        });
    } else {
        modalshow_blame();
    }
}

function modalshow_blame(params) {
    $('#addDialog_blame').modal('show');
}

function modalshow_list(params) {
    $('#addDialog_list').modal('show');
    select_group();
}

function reset_list() {
    $('#form_list')[0].reset();
    modalshow_list();
}

function reset_servicetype(id = null) {
    if (id != null || id != 0) {
        modalshow_servicetype();
        $.get('vhc/editservicetype', { data: { id: id } }, function(res) {
            $('#id_servicetype').val(res.id);
            $.each(res, function(index, value) {
                if (index == 'record_status') {
                    if (value == '1') {
                        // $('#TypeCompany1').checked();
                        $("#record_status").prop('checked', true);
                    } else {
                        //$('#TypeCompany2').checked();
                        $("#record_status").prop('checked', false)
                    }
                }
                $('#' + index).val(value);
            });
        });
    } else {
        modalshow_servicetype();
    }
    $('#form_servietype')[0].reset();
}

function modalshow_servicetype(params) {
    $('#addDialog_servietype').modal('show');
}

function modalshow_option(params) {
    $('#addDialog_option').modal('show');
    select_list();
}

function reset_option() {
    $('#form_option')[0].reset();
    modalshow_option();
}

function select_group() {
    var dropdrow = '';
    $.get('vhc/selectgroup', function(res) {
        $.each(res, function(index, value) {
            dropdrow += '<option value="' + value.id + '"> ' + value.group_name + ' </option>';
        });
        $('#group_id').html(dropdrow).promise().done(function() {
            $('#group_id').SumoSelect({ search: true });
        });
    });
}
// function select_vhc_model(params) {
//     var dropdrow = '';
//     $.get('vhc/selectgroup', function(res) {
//         $.each(res, function(index, value) {
//             dropdrow += '<option value="' + value.id + '"> ' + value.group_name + ' </option>';
//         });
//         $('#group_id').html(dropdrow).promise().done(function() {
//             $('#group_id').SumoSelect({ search: true });
//         });
//     });
// }
function save_servicetype() {

    var servicetype_value = null;
    servicetype_value = $('#form_servietype').serializeArrayAnything();
    servicetype_value.id = $('#id_servicetype').val();
    $.post('vhc/saveservicetype', { data: servicetype_value }, function(res) {
        if (res == 1) {
            showSaveSuccess();
            $('#addDialog_servietype').modal('hide');
            $.pjax.reload({ container: "#pjax_VhcServicetype" })
        } else {
            showSaveError();
        }
    });
}

function save_blame() {

    var servicetype_value = null;
    servicetype_value = $('#form_blame').serializeArrayAnything();
    servicetype_value.id = $('#blame_id').val();
    $.post('vhc/saveblame', { data: servicetype_value }, function(res) {
        if (res == 1) {
            showSaveSuccess();
            $('#addDialog_servietype').modal('hide');
            $.pjax.reload({ container: "#pjax_VhcServicetype" })
        } else {
            showSaveError();
        }
    });
}

function save_list() {
    var list_value = null;
    list_value = $('#form_list').serializeArrayAnything();
    $.post('vhc/savelist', { data: list_value }, function(res) {
        if (res == 1) {
            showSaveSuccess();
            $('#addDialog_list').modal('hide');
            $.pjax.reload({ container: "#pjax_VhcList" })
        } else {
            showSaveError();
        }
    });
}

function save_grouplist() {
    var group_value = null;
    group_value = $('#form_group').serializeArrayAnything();
    $.post('vhc/savegrouplist', { data: group_value }, function(res) {
        if (res == 1) {
            showSaveSuccess();
            $('#addDialog_group').modal('hide');
            $.pjax.reload({ container: '#pjax_VhcGroup' });
        } else {
            showSaveError();
        }
    });
}

function save_list() {
    var option_list_value = null;
    option_list_value = $('#form_list').serializeArrayAnything();
    $.post('vhc/savelist', { data: option_list_value }, function(res) {
        if (res == 1) {
            showSaveSuccess();
            $('#addDialog_list').modal('hide');
        } else {
            showSaveError();
            $('#addDialog_list').modal('hide');
        }
    });
}

function save_option_list() {
    var option_list_value = null;
    option_list_value = $('#form_option').serializeArrayAnything();
    $.post('vhc/savelistoption', { data: option_list_value }, function(res) {
        if (res == 1) {
            $('#addDialog_option').modal('hide');
            showSaveSuccess();
        } else {
            $('#addDialog_option').modal('hide');
            showSaveError();
        }
    });
}

function edit_grouplist(id) {
    $('#addDialog_group').modal('show');
    $.get('vhc/editgroup', { data: id }, function(res) {
        console.log(res);
        $.each(res, function(index, value) {
            if (index == 'record_status') {
                if (value == '1') {
                    // $('#TypeCompany1').checked();
                    $("#record_status").prop('checked', true);
                } else {
                    //$('#TypeCompany2').checked();
                    $("#record_status").prop('checked', false)
                }
            }
            $('#' + index).val(value);
        });
    });
}

function edit_model(id) {
    $('#addDialog_model').modal('show');
    $.get('vhc/editmodel', { data: id }, function(res) {
        console.log(res);
        $.each(res, function(index, value) {
            if (index == 'record_status') {
                if (value == '1') {
                    // $('#TypeCompany1').checked();
                    $("#record_status").prop('checked', true);
                } else {
                    //$('#TypeCompany2').checked();
                    $("#record_status").prop('checked', false)
                }
            }
            $('#' + index).val(value);
        });
    });
}

function leftAfteredit() {
    $.get('vhc/editlist', { data: id }, function(res) {
        $.each(res, function(index, value) {
            if (index == 'record_status') {
                if (value == '1') {
                    // $('#TypeCompany1').checked();
                    $("#record_status").prop('checked', true);
                } else {
                    //$('#TypeCompany2').checked();
                    $("#record_status").prop('checked', false)
                }
            }
            $('#' + index).val(value);
        });
    });
}

function edit_list(id) {
    $('#addDialog_list').modal('show');
    select_group();
    leftAfteredit();


}

function edit_option_list(id) {
    $('#addDialog_option').modal('show');
    $.get('vhc/editpotion', { data: id }, function(res) {
        $.each(res, function(index, value) {
            if (index == 'record_status') {
                if (value == '1') {
                    // $('#TypeCompany1').checked();
                    $("#record_status").prop('checked', true);
                } else {
                    //$('#TypeCompany2').checked();
                    $("#record_status").prop('checked', false)
                }
            }
            $('#' + index).val(value);
        });
    });
}


function delete_grouplist(id) {
    $.post('vhc/deletegroup', { data: id }, function(res) {
        if (res == 1) {
            showDeleteSuccess();
        } else {
            showDeleteError();
        }
    });
}

function delete_servicetype(id) {
    $.post('vhc/deleteservicetype', { data: id }, function(res) {
        if (res == 1) {
            showDeleteSuccess();
            $.pjax.reload({ container: "#pjax_VhcServicetype" })
        } else {
            showDeleteError();
        }
    });
}

function delete_list(id) {
    $.post('vhc/deletelist', { data: id }, function(res) {
        if (res == 1) {
            showDeleteSuccess();
        } else {
            showDeleteError();
        }
    });
}

function delete_option_list(id) {
    $.post('vhc/deletepotion', { data: id }, function(res) {
        if (res == 1) {
            showDeleteSuccess();
        } else {
            showDeleteError();
        }
    });
}