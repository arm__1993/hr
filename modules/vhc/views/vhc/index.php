<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\Utility;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\widgets\Pjax;

//use app\api\AjaxSubmitButton;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/vhc/group.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
AppAsset::register($this);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_structure.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_type.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_section.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_mapping.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_mapp.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/contron.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/js/jquery.sumoselect.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/permission/sumoselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li class="active">ตั้งค่าข้อมูลภาษี</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tax_income1" data-toggle="tab">vhc model</a></li>
                        <li><a href="#tax_income2" data-toggle="tab">กำหนดกลุ่ม vhc</a></li>
                        <li><a href="#tax_income3" data-toggle="tab">กำหนด รายการ vhc</a></li>
                        <li><a href="#tax_income5" data-toggle="tab">กำหนด ตัวเเลือกรายการ vhc</a></li>
                        <li><a href="#tax_income6" data-toggle="tab">ประเภทให้บริการ</a></li>
                        <li><a href="#tax_income7" data-toggle="tab">รอยตำหนิที่ไม่สามารถตรงจสอบ</a></li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="tax_income1">
                            <div class="row">
                                <div id="vhcmodel">

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tax_income2">

                            <div class="row">
                                <div id="grouplist">

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tax_income3">
                            <div class="row">
                                <div id="list">

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tax_income5">
                            <div class="row">
                                <div id="listoption">

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tax_income6">
                            <div class="row">
                                <div id="servicetype">

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tax_income7">
                            <div class="row">
                                <div id="blame">

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tax_income6">

                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">

                                    <form role="form" id="frmincomeType" onsubmit="return suubmitfrmincomeType();"
                                          data-toggle="validator">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>ชื่อประเภทภาษีเงินได้บุคคลธรรมดา <span>*</span></label>
                                                    <input id="taxincome_type" name="taxincome_type"
                                                           data-required="true" class="form-control" type="text"
                                                           placeholder="ชื่อภาษีเงินได้บุคคลธรรมดา" required>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control" id="type_status" name="type_status">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <?php echo Html::hiddenInput('id_type', null, ['id' => 'id_type']); ?>
                                                <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveconfigType', 'type' => 'submit']); ?>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <br/>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->