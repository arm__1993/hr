<?php

namespace app\modules\wssale\models;

use Yii;

class Cusint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CusInt';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'CusInt_Rank', 'Int_Type_Remark', 'SaleRec_posCode', 'Sale_Respon_posCode', 'BookingVolume', 'moneyVolume', 'cancelVolume', 'volume_remark', 'showroomClose', 'status_tis', 'Date_upstatus', 'Create_by_posCode', 'status_report', 'status_report_cancel', 'count_prepare', 'count_rep', 'count_rep_date'], 'required'],
            [['project_id', 'showroomClose', 'count_prepare', 'count_rep'], 'integer'],
            [['Int_Date', 'Create_date', 'terminate_date'], 'safe'],
            [['CusInt_Rank'], 'string'],
            [['Int_CusNo', 'Int_Time', 'BuyVolume', 'BookingVolume', 'moneyVolume', 'cancelVolume', 'Create_by', 'count_rep_date'], 'string', 'max' => 50],
            [['Int_Type', 'Int_Type_Remark', 'Branch_Data'], 'string', 'max' => 100],
            [['Prj_Name'], 'string', 'max' => 200],
            [['SaleRecThisTime', 'Sale_Responsibility', 'Content', 'volume_remark'], 'string', 'max' => 250],
            [['SaleRecThisTimeNo', 'Sale_ResponsibilityNo'], 'string', 'max' => 15],
            [['SaleRec_posCode', 'Sale_Respon_posCode', 'Grouping2_2', 'Status', 'Create_by_posCode'], 'string', 'max' => 30],
            [['Q1', 'Q2', 'Q3', 'Q4'], 'string', 'max' => 10],
            [['Grouping1_1', 'Grouping1_2', 'Grouping2_1'], 'string', 'max' => 60],
            [['Status_Remark'], 'string', 'max' => 255],
            [['status_tis'], 'string', 'max' => 5],
            [['Date_upstatus'], 'string', 'max' => 20],
            [['status_report'], 'string', 'max' => 3],
            [['status_report_cancel'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Int_Num' => 'หมายเลขความสนใจ',
            'Int_CusNo' => 'หมายเลขอ้างอิงลูกค้า',
            'Int_Time' => 'ครั้งที่สนใจ',
            'project_id' => 'ไอดีอ้างอิงจาก Table PROJECT',
            'Int_Date' => 'วันที่สนใจ',
            'Int_Type' => 'ชนิดการสนใจ',
            'CusInt_Rank' => 'ระดับความสำคัญ',
            'Int_Type_Remark' => 'ข้อความเพิ่มเติม กรีณีเป็น Other',
            'Prj_Name' => 'โครงการ',
            'SaleRecThisTime' => 'ชื่อผู้รับลูกค้า',
            'SaleRecThisTimeNo' => 'หมายเลขผู้รับลูกค้า',
            'SaleRec_posCode' => 'รหัสตำแหน่งผู้รับลูกค้า',
            'Sale_Responsibility' => 'ผู้ดูแลลูกค้าครั้งนี้',
            'Sale_ResponsibilityNo' => 'ผู้ดูแลลูกค้าครั้งนี้ No',
            'Sale_Respon_posCode' => 'รหัสตำแหน่งผู้ดูแลลูกค้าครั้งนี้',
            'Q1' => 'ลูกค้าสอบถามวิธีการชำระ yes/no',
            'Q2' => 'ลูกค้าต่อรองราคา yes/no',
            'Q3' => 'ลูกค้าพูดถึงเงื่อนไขคู่แข่ง yes/no',
            'Q4' => 'เห็นแคตตาล็อคต่างยี่ห้อ/เงื่อนไข yes/no',
            'Grouping1_1' => 'ความพร้อม',
            'Grouping1_2' => 'ความต้องการ',
            'Grouping2_1' => 'ลูกค้าเก่าที่',
            'Grouping2_2' => 'ระดับการเปรียบเทียบ',
            'Content' => 'สาระ',
            'BuyVolume' => 'จำนวนที่สนใจซื้อ',
            'BookingVolume' => 'จำนวนที่บันทึกจองแล้ว',
            'moneyVolume' => 'จำนวนที่รับเงินจอง',
            'cancelVolume' => 'จำนวนที่ยกเลิกสนใจ',
            'volume_remark' => 'หมายเหตุจากการยกเลิก BuyVolume ค่าเดิมของ BookVolume',
            'Branch_Data' => 'สาขาที่เกิดข้อมูล',
            'Status' => '1=กำลังติดตาม,2=นำเสนอการติดตาม,3=ออกติดตาม,4=ติดตามสำเร็จ,5=กำลังรอแต่ง,6=รอสั่งรถใหม่,7=จำนวนเงินจองไม่ถูกต้อง(ปิดหน้าร้าน),IC=ออกรถแล้ว,ยุติ=ยุติ,99=ยกเลิกการจอง',
            'showroomClose' => 'ปิการขายหน้าร้าน =1 ',
            'Status_Remark' => 'หมายเหตุสถานะ',
            'status_tis' => 'Status Tis',
            'Date_upstatus' => 'Date Upstatus',
            'Create_by' => 'ผู้สร้าง ใบลูกค้าสนใจ',
            'Create_by_posCode' => 'รหัสตำแหน่งผู้สร้างใบลูกค้าสนใจ',
            'Create_date' => 'วันที่สร้าง ใบลูกค้าสนใจ',
            'terminate_date' => 'วันที่ยุติการติดตาม',
            'status_report' => '0:ไม่มีการคำนวน 1:คำนวณลูค้าใหม่แล้ว',
            'status_report_cancel' => '1:คำนวณยกเลิกจองแล้ว,2:คำนวณเหตุผลยกเลิกจองแล้ว',
            'count_prepare' => 'จำนวนครั้งที่เสนอแผน',
            'count_rep' => 'จำนวนครั้งที่ติดตาม',
            'count_rep_date' => 'วันที่ติดตาม',
        ];
    }

    
    public function searchcusintformobile($cusno)
    {

        $sql = "Select CusInt.*, CusInt_Product.* FROM CusInt INNER JOIN CusInt_Product ON CusInt.Int_Num = CusInt_Product.Int_Num_Ref WHERE CusInt.Int_CusNo = '$cusno' AND CusInt_Product.Int_CusNo = '$cusno' ORDER BY Int_Num ASC";
        //echo $sql;
        $showdataeducusInt = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
       return $showdataeducusInt;
 
    }


}
