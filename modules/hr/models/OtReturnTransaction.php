<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ot_return_transaction".
 *
 * @property integer $id
 * @property string $id_card
 * @property string $total_amount
 * @property string $date_pay
 * @property integer $ot_requestmaster_id
 * @property integer $ot_requestdetail_id
 * @property integer $return_id
 * @property string $return_name
 * @property string $remark
 * @property string $createby_user
 * @property string $create_datetime
 * @property integer $is_used
 * @property string $used_byuser
 * @property string $used_datetime
 */
class OtReturnTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ot_return_transaction';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_card', 'total_amount', 'date_pay', 'ot_requestmaster_id', 'ot_requestdetail_id', 'return_id', 'return_name'], 'required'],
            [['total_amount'], 'number'],
            [['ot_requestmaster_id', 'ot_requestdetail_id', 'return_id', 'is_used'], 'integer'],
            [['create_datetime', 'used_datetime'], 'safe'],
            [['id_card', 'used_byuser'], 'string', 'max' => 13],
            [['date_pay'], 'string', 'max' => 7],
            [['return_name'], 'string', 'max' => 100],
            [['remark'], 'string', 'max' => 250],
            [['createby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_card' => 'Id Card',
            'total_amount' => 'Total Amount',
            'date_pay' => 'Date Pay',
            'ot_requestmaster_id' => 'Ot Requestmaster ID',
            'ot_requestdetail_id' => 'Ot Requestdetail ID',
            'return_id' => 'Return ID',
            'return_name' => 'Return Name',
            'remark' => 'Remark',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'is_used' => 'Is Used',
            'used_byuser' => 'Used Byuser',
            'used_datetime' => 'Used Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return OtReturnTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OtReturnTransactionQuery(get_called_class());
    }
}
