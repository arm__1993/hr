<?php

namespace app\modules\wssale\controllers;
use app\modules\wssale\models\Booking;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;

class BookingController extends ActiveController
{
    public $modelClass = 'app\modules\wssale\models\Booking';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	
	public function actionSearchbookingformobile()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $datacusbooking = new Booking();
        $showdatacusbooking = $datacusbooking->searchbookingformobile($cusno);
        return $showdatacusbooking;
    }


}
