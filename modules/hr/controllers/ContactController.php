<?php

namespace app\modules\hr\controllers;
use Yii;
use mPDF;
use kartik\mpdf\Pdf;
use app\modules\hr\controllers\MasterController;

class ContactController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionTestpdf()
    {

        /*
        $mpdf=new mPDF();
        $texttt= '
        <html><head><style>
        body {font-family: "thsarabun";}
        </style></head>
        <body>ทดสอบบบภาษาไทยจ้า </body>
        </html>';
        $mpdf->WriteHTML("$texttt");
        $mpdf->Output(dirname(__FILE__).'/new_created_file.pdf','I');
        */


        $html = '<style>
                dottab.menu {
                    outdent: 4em;
                }
                p.menu {
                    text-align: left;
                    padding-right: 4em;
                }
                body {font-family: "thsarabun"; font-size:1.5em;}
                </style>
                <h3>Menu</h3>
                <div style="border: 0.2mm solid #000088; padding: 1em;">
                <p class="menu">ด้วยกรมทรัพย์สินทางปัญญา ร่วมกับสำนักงานส่งเสริมอุตสาหกรรมซอฟต์แวร์แห่งชาติ (องค์การมหาชน) ได้จัดโครงการประกวดผลงานสร้างสรรค์โปรแกรมคอมพิวเตอร์ฟอนต์ขึ้น เพื่อส่งเสริมให้มีการพัฒนาและสร้างสรรค์ผลงานลิขสิทธิ์โปรแกรมคอมพิวเตอร์ฟอนต์เพิ่มขึ้น เป็นการสร้างงาน สร้างทักษะ และสร้างตลาดทางด้านอุตสาหกรรมซอฟต์แวร์ของประเทศ อันจะเป็นประโยชน์ต่อสังคมและผู้บริโภคที่สามารถเลือกใช้โปรแกรมคอมพิวเตอร์ฟอนต์ที่มีความหลากลายมากขึ้นต่อไป <dottab class="menu" />&nbsp;&pound;37.00</p>
                
                <p class="menu">Fusce eleifend neque sit amet erat. Integer consectetuer nulla non orci. Morbidi feugiat<dottab class="menu" />&pound;3700.00</p>
                
                <p class="menu">Cras odio. Donec mattis, nisi id euismod auctor, neque metus pellentesque risus, at eleifend lacus sapien et risus <dottab class="menu" />&nbsp;&pound;27.00</p>
                
                <p class="menu">Phasellus metus. Phasellus feugiat, lectus ac aliquam molestie, leo lacus tincidunt turpis, vel aliquam quam odio et sapien. Mauris ante pede, auctor ac, suscipit quis, malesuada sed, nulla. Integer sit amet odio sit amet lectus luctus euismod <dottab class="menu" />&nbsp;&pound;7.00</p>
                
                <p class="menu">Donec et nulla. Sed quis orci<dottab class="menu" />&pound;1137.00</p>
                </div>
                ';
        //require_once __DIR__ . '/../vendor/autoload.php';
        //$mpdf = new mPDF();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output(dirname(__FILE__).'/new_created_file.pdf','I');
    }


}
