<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\hr\apihr\ApiSSOExport;
use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;

use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\apihr\ApiHr;

$company = ApiSSOExport::select_company();
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/exporttorevenues.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$today = date('d/m/Y');

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>ส่งออกข้อมูล</li>
                <li class="active">ข้อมูลส่งสรรพากร</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>

        <form id="frmFormonth" name="frmFormonth" method="POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h4 class="box-title">เอกสารภาษี หัก ณ ที่จ่าย ประจำเดือน / ภงด 1 + เอกสารแนบ</h4>
                            </div>
                            <div class="row" style="padding-top: 10px;">
                                <div class="col-md-4">

                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect"
                                                       class="col-sm-4 control-label">รอบเงินเดือน</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" data-required="true"
                                                           id="month_pay" name="month_pay"
                                                           value="<?php echo date('m-Y'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <label for="monthselect" class="col-sm-4 control-label">ลงวันที่</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control datepick" data-required="true"
                                                       id="sign_date" name="sign_date" placeholder="dd/mm/yyyy"
                                                       value="<?php echo $today; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect"
                                                       class="col-sm-4 control-label">ชื่อพนักงาน</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="emp_id" id="emp_id"
                                                            required>
                                                        <option value="all">--- ทั้งหมด ---</option>
                                                        <?php
                                                        foreach ($arrEmp as $value) {
                                                            ?>
                                                            <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect"
                                                       class="col-sm-4 control-label">ชื่อบริษัท</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="company_id"
                                                            id="company_id" required>
                                                        <option value="all">--- ทั้งหมด ---</option>
                                                        <?php
                                                        foreach ($arrCompany as $value) {
                                                            ?>
                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <button type="button" id="btnExportWHT"
                                                    class="btn btn-sm btn-success btn-sm"><i
                                                        class="fa fa-cloud-download"></i> ส่งออก หัก ณ ที่จ่าย
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <button type="button" id="btnExportPND1" class="btn btn-sm btn-info btn-sm">
                                                <i class="fa fa-cloud-download"></i> ส่งออก ภงด 1
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        &nbsp;
                    </div>
                </div>
            </div>
            <input type="hidden" value="<?php echo Url::to(['taxexport/tvi50pdf']) ?>" id="url_to" name="url_to">
            <input type="hidden" value="<?php echo Url::to(['taxexport/tvi50pdfs']) ?>" id="tvi50pdfs" name="tvi50pdfs">
            <input type="hidden" value="<?php echo Url::to(['taxexport/pnd1aatt']) ?>" id="PND1a" name="PND1a">
            <input type="hidden" value="<?php echo Url::to(['taxexport/pndpdf1']) ?>" id="Pndpdf1" name="Pndpdf1">
        </form>
        <hr>
        <form id="frmForyear" name="frmForyear" method="POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">เอกสารภาษี 50ทวิ ประจำปี / ภงด 1ก + เอกสารแนบ</h4>
                            </div>
                            <div class="row" style="padding-top: 10px;">
                                <div class="col-md-4">

                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect" class="col-sm-4 control-label">ปี</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" id="year_pay" name="year_pay">
                                                        <?php

                                                        for ($i = date('Y') - 2; $i < date('Y') + 1; $i++) {
                                                            $sel = ($i == date('Y')) ? 'selected="selected" ' : '';
                                                            echo '<option value="' . $i . '" ' . $sel . '>' . $i . '</option>';
                                                        }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <label for="monthselect" class="col-sm-4 control-label">ลงวันที่</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control datepick" data-required="true"
                                                       id="sign_date" name="sign_date" placeholder="dd/mm/yyyy"
                                                       value="<?php echo $today; ?>">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect"
                                                       class="col-sm-4 control-label">ชื่อพนักงาน</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="emp_id" id="emp_id"
                                                            required>
                                                        <option value="all">--- ทั้งหมด ---</option>
                                                        <?php
                                                        foreach ($arrEmp as $value) {
                                                            ?>
                                                            <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect"
                                                       class="col-sm-4 control-label">ชื่อบริษัท</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="company_id"
                                                            id="company_id" required>
                                                        <option value="all">--- ทั้งหมด ---</option>
                                                        <?php
                                                        foreach ($arrCompany as $value) {
                                                            ?>
                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <button type="button" id="btnExport50Tvi"
                                                    class="btn btn-sm btn-primary btn-sm"><i
                                                        class="fa fa-cloud-download"></i> ส่งออก 50ทวิ
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <button type="button" id="btnExportPND1a"
                                                    class="btn btn-sm btn-warning btn-sm"><i
                                                        class="fa fa-cloud-download"></i> ส่งออก ภงด 1ก
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        &nbsp;
                    </div>
                </div>
            </div>
        </form>

        <form id="frmFormonthpnd" name="frmFormonthpnd" method="POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h4 class="box-title">ส่งออก Text file ภงด.</h4>
                            </div>
                            <div class="row" style="padding-top: 10px;">
                                <div class="col-md-4">

                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect"
                                                       class="col-sm-4 control-label">รอบเงินเดือน</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" data-required="true"
                                                           id="month_paypnd" name="month_paypnd"
                                                           value="<?php echo date('m-Y'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <label for="monthselect" class="col-sm-4 control-label">ลงวันที่</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control datepick" data-required="true"
                                                       id="sign_date" name="sign_date" placeholder="dd/mm/yyyy"
                                                       value="<?php echo $today; ?>">
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="col-md-4">

                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthselect"
                                                       class="col-sm-4 control-label">ชื่อบริษัท</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="company_idpnd"
                                                            id="company_idpnd" required>
                                                        <option value="all">--- ทั้งหมด ---</option>
                                                        <?php
                                                        foreach ($arrCompany as $value) {
                                                            ?>
                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <button type="button" id="btnExportpndText"
                                                    class="btn btn-sm btn-success btn-sm"><i
                                                        class="fa fa-cloud-download"></i> ส่งออก Text file ภงด.
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        &nbsp;
                    </div>
                </div>
            </div>
        </form>

    </div>
    <!-- /.box -->
</section><!-- /.content -->