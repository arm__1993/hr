<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\bundle;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */

/*
class AppAsset extends AssetBundle
{
    
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
*/


//Ininitialize Template AdminLTE
class AppAsset extends AssetBundle
{
    public $sourcePath = '@bower/admin-lte/';
    public $baseUrl = '@web';
    public $css = [
        'dist/css/AdminLTE.css',
        'bootstrap/css/ionicons.min.css',
        'bootstrap/css/font-awesome.min.css',
        //'plugins/font-awesome/css/font-awesome.min.css',
        //'plugins/ionicons/css/ionicons.min.css',
        'dist/css/AdminLTE.min.css',
        'dist/css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/blue.css',
        //'plugins/morris/morris.css',
        //'plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        //'plugins/datepicker/datepicker3.css',
        //'plugins/datepicker-thai/css/datepicker.css',
        //'plugins/daterangepicker/daterangepicker-bs3.css',
        //'plugins/timepicker/bootstrap-timepicker.min.css',

        'plugins/datepicker/datepicker3.css',
        'plugins/daterangepicker/daterangepicker.css',
        'plugins/select2/select2.min.css',
        'plugins/datatables/dataTables.bootstrap.css',

        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'plugins/iCheck/square/blue.css',
        'plugins/iCheck/all.css',
    ];
    public $js = [
        // 'dist/js/AdminLTE/app.js',
        //'plugins/jQuery/jquery-3.2.0.min.js',
        //'plugins/jQueryUI/jquery-ui.min.js',
        'plugins/input-mask/jquery.inputmask.js',
        // 'plugins/input-mask/jquery.inputmask.bundle.js',
        'plugins/input-mask/jquery.inputmask.date.extensions.js',
        'plugins/input-mask/jquery.inputmask.extensions.js',
        'plugins/input-mask/jquery.inputmask.numeric.extensions.js',
        'plugins/input-mask/jquery.inputmask.phone.extensions.js',
        'plugins/input-mask/jquery.inputmask.regex.extensions.js',

        //'dist/js/satusoftware_datemask.js',
        //'dist/js/satusoftware.js',
        //'bootstrap/js/bootstrap.min.js',
        //'dist/js/raphael-min.js',
        //'plugins/morris/morris.min.js',
        //'plugins/sparkline/jquery.sparkline.min.js',
        //'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        //'plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        //'plugins/knob/jquery.knob.js',

        'plugins/daterangepicker/moment.js',
        'plugins/daterangepicker/daterangepicker.js',
        'plugins/datepicker/bootstrap-datepicker.js',
        'plugins/datepicker/locales/bootstrap-datepicker.th.js',
        'plugins/select2/select2.full.min.js',
        'plugins/select2/i18n/th.js',
        'plugins/datatables/jquery.dataTables.min.js',
        'plugins/datatables/dataTables.bootstrap.min.js',

        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
        'plugins/fastclick/fastclick.min.js',
        'dist/js/app.min.js',
        'plugins/ckeditor/ckeditor.js',
        'plugins/iCheck/icheck.min.js',
        // 'js/hr/jquery-ui.js',
        // 'jquery-1.12.4.js',
        //'plugins/sparkline/jquery.sparkline.min.js',
        //'plugins/chartjs/Chart.min.js',
        //'plugins/datatables/jquery.dataTables.min.js',
        //'plugins/datatables/dataTables.bootstrap.min.js',
        //'plugins/chartjs/Chart.min.js',
        //'plugins/timepicker/bootstrap-timepicker.min.js',
        //'dist/js/satusoftware_editor1.js',
        //'dist/js/satusoftware_editor2.js',
        //'dist/js/pages/dashboard.js',
        //'dist/js/demo.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

