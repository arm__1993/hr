<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/21/2017 AD
 * Time: 14:12
 */

namespace app\modules\hr\apihr;


class CalculateTaxIncome
{
    private $idCard;
    private $totalInCome = 0;
    private $totalFixedReduce = 0;
    private $arrReduceOther = [];
    private $totalReduceOther = 0;
    private $hasChild;
    private $numberChild = 0;
    private $hasParent;
    private $numberParent = 0;
    private $isSigle;
    private $modelTaxStructure;
    private $totalTaxInYear = 0;  //ภาษีทั้งปี
    private $accumulateTax = 0;  //ภาษีจ่ายสะสมมาแล้ว
    private $totalThisMonth = 0;


    function __construct($_idcard)
    {
        $this->idCard = $_idcard;
    }

    function setTotalIncome($_totalIncome)
    {
        $this->totalInCome = $_totalIncome;
    }

    function setTotalFixedReduce($_totalFixReduce)
    {
        $this->totalFixedReduce = $_totalFixReduce;
    }

    function setReductOther($_arrReductOther)
    {
        $this->arrReduceOther = $_arrReductOther;
    }

    function setNumberChild($_numberChild)
    {
        $this->numberChild = $_numberChild;
        if($_numberChild >= 1) {
            $this->hasChild = true;
        }
        else {
            $this->hasChild = false;
        }
    }

    function setNumberParent($_numberParent)
    {
        $this->numberParent = $_numberParent;
        if($_numberParent >= 1) {
            $this->hasParent = true;
        }
        else {
            $this->hasParent = false;
        }
    }


    function setSingle($_single)
    {
        $this->isSigle = $_single;
    }


    function setModelTaxIncomeStructure($_model)
    {
        $this->modelTaxStructure = $_model;
        //TODO :: refactoring model tax structure
    }


    function setAccumulateTax($_accumulateTax)
    {
        $this->accumulateTax = $_accumulateTax;
    }

    function calculateReductOther()
    {
        $_total = 0;
        foreach ($this->arrReduceOther as $key => $value) {
            $_total += $value;
        }

        $this->totalReduceOther = $_total;
    }


    function calculateTaxIncome()
    {
        //TODO :: calculate tax income with model tax structure
        //step 1 : calculate total income
        //step 2 : calculate total reduce
        //step 3 : calculate tax step with model tax structure
        //step 4 : sum tax each step
        //step 5 : return value
    }

    function calculateTaxThisMonth()
    {

    }

    function calculateAccumulateTax()
    {

    }

}

?>