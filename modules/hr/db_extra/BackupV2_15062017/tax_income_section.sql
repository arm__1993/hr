-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2017 at 09:06 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_section`
--

CREATE TABLE `tax_income_section` (
  `id` tinyint(3) NOT NULL COMMENT 'รหัส',
  `tax_income_type_id` tinyint(3) NOT NULL COMMENT 'ประเภทภาษีบุคคลธรรมดา FK tax_income_type.id',
  `tax_section_name` varchar(255) NOT NULL COMMENT 'ประเภทเงินได้',
  `expenses_percent` decimal(4,2) DEFAULT NULL COMMENT 'เงินได้นี้หักค่าใช้จ่ายได้ %',
  `expenses_notover` decimal(10,2) DEFAULT NULL COMMENT 'หักค่าใช้จ่ายไม่เกิน',
  `comment` varchar(1000) DEFAULT NULL,
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_income_section`
--

INSERT INTO `tax_income_section` (`id`, `tax_income_type_id`, `tax_section_name`, `expenses_percent`, `expenses_notover`, `comment`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, 2, 'มาตรา 40(1) เงินได้จากการจ้างแรงงาน เช่น เงินเดือน ค่าจ้าง เบี้ยเลี้ยง บำนาญโบนัส ฯ', '50.00', '100000.00', ' เงินได้จากการจ้างแรงงาน เช่น เงินเดือน ค่าจ้าง เบี้ยเลี้ยง บำนาญโบนัส ฯ', 1, '1509901325106', '2017-06-14 16:43:44', '', NULL),
(2, 1, 'มาตรา 40(2) เงินได้จากตำแหน่งงานที่ทำหรือรับทำงานให้ เช่น เบี้ยประชุม ค่านายหน้า ค่าธรรมเนียมฯ', '50.00', '100000.00', 'มาตรา 40(2) เงินได้จากตำแหน่งงานที่ทำหรือรับทำงานให้ เช่น เบี้ยประชุม ค่านายหน้า ค่าธรรมเนียมฯ', 1, '1509901325106', '2017-06-14 16:45:07', '', NULL),
(3, 1, 'มาตรา 40(3) เงินได้จากค่าสิทธิ เช่น ค่าแห่งกู๊ดวิลล์ ค่าแห่งลิขสิทธิ์ ฯ', NULL, NULL, 'เงินได้จากค่าสิทธิ เช่น ค่าแห่งกู๊ดวิลล์ ค่าแห่งลิขสิทธิ์ ฯ', 1, '1509901325106', '2017-06-14 16:46:06', '', NULL),
(4, 1, 'มาตรา 40(4) เงินได้จากดอกเบี้ย เงินปันผลจากบริษัทต่างประเทศ เงินลดทุนเงินเพิ่มทุน ฯ', NULL, NULL, 'เงินได้จากดอกเบี้ย เงินปันผลจากบริษัทต่างประเทศ เงินลดทุนเงินเพิ่มทุน ฯ', 1, '1509901325106', '2017-06-14 16:46:28', '', NULL),
(5, 1, 'มาตรา 40(4)(ข) เงินได้จากเงินปันผลจากบริษัทฯ ที่ตั้งขึ้นตามกฎหมายไทย', NULL, NULL, 'มาตรา 40(4)(ข) เงินได้จากเงินปันผลจากบริษัทฯ ที่ตั้งขึ้นตามกฎหมายไทย', 1, '1509901325106', '2017-06-14 16:46:50', '', NULL),
(6, 1, 'มาตรา 40(5) เงินได้จากการให้เช่าทรัพย์สิน เช่น ค่าเช่าบ้าน ค่าเช่ายานพาหนะค่าเช่าที่ดิน ฯ', NULL, NULL, 'มาตรา 40(5) เงินได้จากการให้เช่าทรัพย์สิน เช่น ค่าเช่าบ้าน ค่าเช่ายานพาหนะค่าเช่าที่ดิน ฯ', 1, '1509901325106', '2017-06-14 16:47:08', '', NULL),
(7, 1, 'มาตรา 40(6) เงินได้จากวิชาชีพอิสระ เช่น การประกอบโรคศิลปะ วิชากฎหมายการบัญชี ฯ', NULL, NULL, 'มาตรา 40(6) เงินได้จากวิชาชีพอิสระ เช่น การประกอบโรคศิลปะ วิชากฎหมายการบัญชี ฯ', 1, '1509901325106', '2017-06-14 16:47:30', '', NULL),
(8, 1, 'มาตรา 40(7) เงินได้จากการรับเหมาที่ผู้รับเหมาต้องลงทุนด้วยการจัดหาสัมภาระในส่วนสำคัญ นอกจากเครื่องมือ', NULL, NULL, 'มาตรา 40(7) เงินได้จากการรับเหมาที่ผู้รับเหมาต้องลงทุนด้วยการจัดหาสัมภาระในส่วนสำคัญ นอกจากเครื่องมือ', 1, '1509901325106', '2017-06-14 16:47:56', '', NULL),
(9, 1, 'มาตรา 40(8) เงินได้จากการธุรกิจ การพาณิชย์ การเกษตร การอุตสาหกรรม การขนส่งเงินได้อื่นๆ นอกจากเงินได้ตามมาตรา 40(1)-(7) ข้างต้น', NULL, NULL, 'มาตรา 40(8) เงินได้จากการธุรกิจ การพาณิชย์ การเกษตร การอุตสาหกรรม การขนส่งเงินได้อื่นๆ นอกจากเงินได้ตามมาตรา 40(1)-(7) ข้างต้น', 1, '1509901325106', '2017-06-14 16:48:16', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_income_section`
--
ALTER TABLE `tax_income_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_income_type_id` (`tax_income_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_income_section`
--
ALTER TABLE `tax_income_section`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'รหัส', AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
