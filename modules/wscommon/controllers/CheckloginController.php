<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Empdata;
use app\modules\wscommon\models\Relationposition;
use app\modules\wscommon\models\Position;
use yii\helpers\ArrayHelper;

class CheckloginController extends ActiveController
{

    public $modelClass = 'app\modules\wscommon\models\Empdata';
   	public $modelClassRelationposition = 'app\modules\wscommon\models\Relationposition';
    public $modelClassPosition = 'app\modules\wscommon\models\Position';


    public function behaviors()
    {
        return
            ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                ],
            ],
        ],
        parent::behaviors());
    }
    
    public function actionSearcpositionloginmobile()
    {

        $username = $_GET['username'];
        $id_card = new Empdata();
        $id_card_em = $id_card->searcpositionloginmobile($username);
        $id_Reationpostion = new Relationposition();
        $id_postion_search = $id_Reationpostion->searchreationpostion($id_card_em);
        $list_position_cus = new Position();
        $showlistpostion= $list_position_cus->searchrlistposition($id_postion_search);
        return $showlistpostion;

       
        
    
    }

}
