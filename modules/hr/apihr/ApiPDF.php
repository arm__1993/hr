<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 8/3/2018 AD
 * Time: 10:33
 */

namespace app\modules\hr\apihr;


use app\modules\hr\models\Empdata;
use app\modules\hr\models\TaxWitholding;
use app\modules\hr\models\TaxWitholdingDetail;
use yii\helpers\ArrayHelper;
use app\modules\hr\models\SummaryMoneyWage;
use app\modules\hr\models\TaxCalculate;

class ApiPDF
{

    //input 06-2018 ==> output 2018-06
    public static function flip_month($paid_month)
    {
        $t = explode('-', $paid_month);
        return 'Withholding'.$t[1] . '-' . $t[0];
    }

    public static function flip_monthpnd($paid_month)
    {
        $t = explode('-', $paid_month);
        return  $t[0];
    }
    public static function flip_yearpnd($paid_month)
    {
        $t = explode('-', $paid_month);
        return  $t[1]+543;
    }
    public static function flip_datepnd($paid_month)
    {
        $t = explode('/', $paid_month);
        return  $t[0].$t[1].$t[2]+543;
    }

    public static function flip_monthpnd1($paid_month)
    {
        $t = explode('-', $paid_month);
        return 'income_tax_1_' . $t[1] . '-' . $t[0];
    }

    public static function flip_monthtv50($paid_month)
    {
        $t = explode('-', $paid_month);
        return $t[1] . $t[0];
    }


    public static function lookup_emp_wht($paid_month)
    {
        $wht = TaxWitholding::find()->select([
            'emp_idcard', 'years', 'date_pay'
        ])->where([
            'date_pay' => $paid_month
        ])->asArray()->all();

        return ArrayHelper::index($wht, 'ID_Card');
    }


    public static function extract_id_card($idcard)
    {
        $arrIDCard = [];
        for ($i = 0; $i < strlen($idcard); $i++) {
            $arrIDCard[($i + 1)] = $idcard[$i];
        }
        return $arrIDCard;
    }


    public static function get_emp_data_all()
    {


        //TODO : fixed invalid data for PDF
        $emp = Empdata::find()->select([
            'DataNo', 'Be', 'Name', 'Surname', 'ID_Card',
            'Address', 'Moo', 'Road', 'Village', 'SubDistrict', 'District', 'Province', 'Postcode'
        ])->asArray()->orderBy([
            'DataNo' => SORT_ASC
        ])->all();

        $arrList = [];
        foreach ($emp as $item) {
            if ($item['ID_Card']) {
                $arrIDCard = self::extract_id_card($item['ID_Card']);

                $_address = '';
                $_address .= ($item['Address']) ? $item['Address'] : '';
                $_address .= ($item['Moo'] != '' && $item['Moo'] != 'ไม่ระบุ') ? ' หมู่ ' . $item['Moo'] : '';

                if ($item['Road'] != '' && $item['Road'] != null && $item['Road'] != 'ไม่ระบุ') {
                    $_address .= ' ถนน' . $item['Road'];
                }

                if ($item['Village'] != '' && $item['Village'] != null && $item['Village'] != 'ไม่ระบุ') {
                    $_address .= (strpos($item['Village'], 'บ้าน') == false) ? ' บ้าน' . $item['Village'] : ' ' . $item['Village'];
//                    if(strpos($item['Village'], 'บ้าน') == false ) {
//                        $_address .= ' บ้าน'.$item['Village'];
//                    }else {
//                        $_address .= ' '.$item['Village'];
//                    }
                }

                if ($item['SubDistrict'] != '' && $item['SubDistrict'] != null && $item['SubDistrict'] != 'ไม่ระบุ') {
                    $_address .= ' ตำบล' . $item['SubDistrict'];
                }

                if ($item['District'] != '' && $item['District'] != null && $item['District'] != 'ไม่ระบุ') {
                    $_address .= ' อำเภอ' . $item['District'];
                }

                if ($item['Province'] != '' && $item['Province'] != null && $item['Province'] != 'ไม่ระบุ') {
                    $_address .= ' จังหวัด' . $item['Province'];
                }

                if ($item['Postcode'] != '' && $item['Postcode'] != null && $item['Postcode'] != 'ไม่ระบุ' && $item['Postcode'] != 'ไม่ระบุ') {
                    $_address .= '' . $item['Postcode'];
                }



                $_address2 = '';
                $_address2 .= ($item['Address']) ? $item['Address'] : '';
                $_address2 .= ($item['Moo'] != '' && $item['Moo'] != 'ไม่ระบุ') ? ' หมู่ ' . $item['Moo'] : '';

                if ($item['Road'] != '' && $item['Road'] != null && $item['Road'] != 'ไม่ระบุ') {
                    $_address2 .= ' ถนน' . $item['Road'];
                }

                if ($item['Village'] != '' && $item['Village'] != null && $item['Village'] != 'ไม่ระบุ') {
                    $_address2 .= (strpos($item['Village'], 'บ้าน') == false) ? ' บ้าน' . $item['Village'] : ' ' . $item['Village'];
//                    if(strpos($item['Village'], 'บ้าน') == false ) {
//                        $_address .= ' บ้าน'.$item['Village'];
//                    }else {
//                        $_address .= ' '.$item['Village'];
//                    }
                }

                if ($item['SubDistrict'] != '' && $item['SubDistrict'] != null && $item['SubDistrict'] != 'ไม่ระบุ') {
                    $_address2 .= ' ตำบล' . $item['SubDistrict'];
                }

                if ($item['District'] != '' && $item['District'] != null && $item['District'] != 'ไม่ระบุ') {
                    $_address2 .= ' อำเภอ' . $item['District'];
                }

                if ($item['Province'] != '' && $item['Province'] != null && $item['Province'] != 'ไม่ระบุ') {
                    $_address2 .= ' จังหวัด' . $item['Province'];
                }



                $emp_data = [
                    'array_idcard' => $arrIDCard,
                    'string_idcard' => $item['ID_Card'],
                    'Be' => $item['Be'],
                    'Name' => $item['Name'],
                    'BeName' => $item['Be'] . $item['Name'],
                    'Surname' => $item['Surname'],
                    'ID_Card' => $item['ID_Card'],
                    'full_name' => $item['Be'] . $item['Name'] . ' ' . $item['Surname'],
                    'full_namenotbe' =>$item['Name'] . ' ' . $item['Surname'],
                    'address' => $_address,
                    'address2'=> $_address2,
                    'Postcode'=>  $item['Postcode'],
                    //'address' => $item['Address'].' หมู่ '.$item['Moo'].' '.$item['Road'].' '.$item['Village'].' '.$item['SubDistrict'].' '.$item['District'].' '.$item['Province'].' '.$item['Postcode'],
                ];
                $arrList[$item['ID_Card']] = $emp_data;
            }
        }

        return $arrList;
    }

    public static function get_wht_data($date_pay, $company_id, $emp_idcard)
    {
        $data = [];
        if ($company_id == 'all' && $emp_idcard == 'all' || !isset($emp_idcard) && !isset($company_id)) {

            $data = TaxWitholding::find()->where([
                'date_pay' => $date_pay
            ])->asArray()->all();
           // echo '1';


        }
        if (isset($company_id) && $company_id != 'all' && !isset($emp_idcard)) {
            $data = TaxWitholding::find()->where([
                'company_id' => $company_id,
                'date_pay' => $date_pay
            ])->asArray()->all();
          //  echo '2';


        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && !isset($company_id)) {
            $data = TaxWitholding::find()->where([
                'emp_idcard' => $emp_idcard,
                'date_pay' => $date_pay
            ])->asArray()->all();
          //  echo '3';


        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && isset($company_id) && $company_id == 'all') {
            $data = TaxWitholding::find()->where([
                'emp_idcard' => $emp_idcard,
                'date_pay' => $date_pay
            ])->asArray()->all();
         //   echo '4';

        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && isset($company_id) && $company_id != 'all') {
            $data = TaxWitholding::find()->where([
                'emp_idcard' => $emp_idcard,
                'date_pay' => $date_pay
            ])->asArray()->all();
           // echo '5';


        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && isset($company_id) && $company_id != 'all') {
            $data = TaxWitholding::find()->where([
                'emp_idcard' => $emp_idcard,
                'date_pay' => $date_pay
            ])->asArray()->all();
         //   echo '6';


        }

        if (isset($company_id) && $company_id != 'all' && isset($emp_idcard) && $emp_idcard == 'all') {
            $data = TaxWitholding::find()->where([
                'company_id' => $company_id,
                'date_pay' => $date_pay
            ])->asArray()->all();
          //  echo '7';

        }


        return ArrayHelper::index($data, 'emp_idcard');

    }  //หัก ณ ที่จ่าย

    public static function get_wht_data_detail($date_pay, $company_id, $emp_idcard)
    {
        $data = [];
        if ($company_id == 'all' || $emp_idcard == 'all' || !isset($emp_idcard) || !isset($company_id)) {
            $data = TaxWitholding::find()
                ->select('*')
                ->from('tax_witholding')
                ->innerJoin('tax_witholding_detail','tax_witholding_detail.emp_idcard = tax_witholding.emp_idcard')
                ->where([
                    'tax_witholding.date_pay' => $date_pay,
                ])
                ->asArray()
                ->all();
        }

        if (isset($company_id) && $company_id != 'all' && !isset($emp_idcard)) {
            $data = TaxWitholding::find()
                ->select('*')
                ->from('tax_witholding')
                ->innerJoin('tax_witholding_detail','tax_witholding_detail.emp_idcard = tax_witholding.emp_idcard')
                ->where([
                    'tax_witholding.date_pay' => $date_pay,
                     'tax_witholding.company_id' => $company_id,
                ])
                ->asArray()
                ->all();
        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && !isset($company_id)) {
            $data = TaxWitholding::find()
                ->select('*')
                ->from('tax_witholding')
                ->innerJoin('tax_witholding_detail','tax_witholding_detail.emp_idcard = tax_witholding.emp_idcard')
                ->where([
                    'tax_witholding.date_pay' => $date_pay,
                    'tax_witholding.emp_idcard' => $emp_idcard,
                ])
                ->asArray()
                ->all();
            //  echo '3';


        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && isset($company_id) && $company_id == 'all') {
            $data = TaxWitholding::find()
                ->select('*')
                ->from('tax_witholding')
                ->innerJoin('tax_witholding_detail','tax_witholding_detail.emp_idcard = tax_witholding.emp_idcard')
                ->where([
                    'tax_witholding.date_pay' => $date_pay,
                    'tax_witholding.emp_idcard' => $emp_idcard,
                ])
                ->asArray()
                ->all();
            //   echo '4';

        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && isset($company_id) && $company_id != 'all') {
            $data = TaxWitholding::find()
                ->select('*')
                ->from('tax_witholding')
                ->innerJoin('tax_witholding_detail','tax_witholding_detail.emp_idcard = tax_witholding.emp_idcard')
                ->where([
                    'tax_witholding.date_pay' => $date_pay,
                    'tax_witholding.emp_idcard' => $emp_idcard,
                ])
                ->asArray()
                ->all();
            // echo '5';


        }

        if (isset($emp_idcard) && $emp_idcard != 'all' && isset($company_id) && $company_id != 'all') {
            $data = TaxWitholding::find()
                ->select('*')
                ->from('tax_witholding')
                ->innerJoin('tax_witholding_detail','tax_witholding_detail.emp_idcard = tax_witholding.emp_idcard')
                ->where([
                    'tax_witholding.date_pay' => $date_pay,
                    'tax_witholding.emp_idcard' => $emp_idcard,
                ])
                ->asArray()
                ->all();
            //   echo '6';


        }

        if (isset($company_id) && $company_id != 'all' && isset($emp_idcard) && $emp_idcard == 'all') {
            $data = TaxWitholding::find()
                ->select('*')
                ->from('tax_witholding')
                ->innerJoin('tax_witholding_detail','tax_witholding_detail.emp_idcard = tax_witholding.emp_idcard')
                ->where([
                    'tax_witholding.date_pay' => $date_pay,
                    'tax_witholding.company_id' => $company_id,
                ])
                ->asArray()
                ->all();
        }





        return ArrayHelper::index($data, 'id');

    } //หัก ณ ที่จ่าย

    public static function pdf_pnd1($company_id, $pay_date, $emp)
    {
        $data = [];
        if (isset($company_id) && $company_id != 'all' && !isset($emp)) {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date,
                    'company_id' => $company_id])
                ->asArray()
                ->all();

        }
        if (isset($company_id) && $emp == 'all') {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date,
                    'company_id' => $company_id])
                ->asArray()
                ->all();

        }

        if (!isset($company_id) && !isset($emp)) {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date])
                ->asArray()
                ->all();

        }
        if ($company_id == 'all' && $emp == 'all') {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date])
                ->asArray()
                ->all();

        }

        if (!isset($company_id) && isset($emp) && $emp == 'all') {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date])
                ->asArray()
                ->all();

        }

        if (isset($company_id) && $company_id == 'all' && !isset($emp)) {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date])
                ->asArray()
                ->all();

        }

        if (isset($company_id) && isset($emp) && $emp != 'all') {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date,
                    'emp_idcard' => $emp])
                ->asArray()
                ->all();

        }
        if (!isset($company_id) && isset($emp) && $emp != 'all') {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date,
                    'emp_idcard' => $emp])
                ->asArray()
                ->all();

        }
        if (isset($company_id) && $company_id == 'all' && isset($emp) && $emp != 'all') {
            $data = TaxCalculate::find()
                ->where(['pay_date' => $pay_date,
                    'emp_idcard' => $emp])
                ->asArray()
                ->all();

        }


        return ArrayHelper::index($data, 'emp_idcard');

    } //ภงด 1 พร้อมไฟล์แนบ

    public static function Pdf_pnd1_a($company_id, $pay_year, $id)
    {
        $data = [];
        if (isset($company_id) && $company_id != 'all' && !isset($id)) {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                ->andWhere('company_id=' . $company_id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '1';
        }
        if (isset($company_id) && $id == 'all') {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                ->andWhere('company_id=' . $company_id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '2';
        }

        if (!isset($company_id) && !isset($id)) {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                //  ->andWhere('company_id=' . $company_id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '3';
        }

        if (!isset($company_id) && isset($id) && $id == 'all') {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                //  ->andWhere('company_id=' . $company_id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '4a';
        }

        if (isset($company_id) && $company_id == 'all' && !isset($id)) {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                //  ->andWhere('emp_idcard=' . $id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '4b';
        }

        if (isset($company_id) && isset($id) && $id != 'all') {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                ->andWhere('emp_idcard=' . $id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '5';
        }
        if (!isset($company_id) && isset($id) && $id != 'all') {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                ->andWhere('emp_idcard=' . $id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '6';
        }
        if (isset($company_id) && $company_id == 'all' && isset($id) && $id != 'all') {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                ->andWhere('emp_idcard=' . $id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '7';
        }

        if ($company_id == 'all' && $id == 'all') {
            $data = TaxCalculate::find()
                ->select('company_id,pay_date,emp_idcard,MAX(id) as max_id')
                ->where("  years =" . $pay_year)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
            echo '4';
        }



        return ArrayHelper::index($data, 'emp_idcard');

    } //ภงด 1ก พร้อมไฟล์แนบ

    public static function Tvi50pdfs($id, $year, $company_id)
    {
        $model = [];

        if (!isset($id) && !isset($company_id)) {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }

        if ($id == 'all' && !isset($company_id)) {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }
        if (isset($id) && $id != 'all' && !isset($company_id)) {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->andWhere('emp_idcard=' . $id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }

        if ($company_id == 'all' && !isset($id)) {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }

        if (isset($company_id) && $company_id != 'all' && !isset($id)) {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->andWhere("company_id = " . $company_id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }

        if (isset($company_id) && $company_id != 'all' && isset($id) && $id != 'all') {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->andWhere('emp_idcard=' . $id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }

        if (isset($company_id) && $company_id != 'all' && isset($id) && $id == 'all') {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->andWhere("company_id = " . $company_id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }

        if (isset($company_id) && $company_id == 'all' && isset($id) && $id == 'all') {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }
        if (isset($id) && $id != 'all' && $company_id == 'all') {
            $model = SummaryMoneyWage::find()
                ->select('emp_idcard,MAX(id) as max_id')
                ->where("  pay_year =" . $year)
                ->andWhere('emp_idcard=' . $id)
                ->groupBy(['emp_idcard'])
                ->asArray()
                ->all();
        }

        return ArrayHelper::index($model, 'emp_idcard');

    } //TV50 by arm


}