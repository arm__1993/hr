<?php


namespace app\modules\hr\apihr;

use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\apihr\ApiHr;
use app\api\DBConnect;


class ApiSearchtaxpersonal
{
        public static function searchtaxpersonal($idcard,$monthyear){
            $monthyear = '%'.$monthyear.'%';

            $db['pl'] = DBConnect::getDBConn()['pl'];

            //echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getOtTemplateID(); //รหัสประเภทเทมเพลตของโอที
            //echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getSsoTemplateID(); //รหัสประเภทเทมเพลตของประกันสังคม
            $idtax91 = ApiHr::getPayrollConfigTemplate()->getTaxIncomeTemplateID(); //รหัสประเภทเทมเพลตของภาษี ภงด 91
            $idtax1 = ApiHr::getPayrollConfigTemplate()->getTaxPndTemplateID(); //รหัสประเภทเทมเพลตของภาษี ภงด 1
            $taxthis = ApiHr::getPayrollConfigTemplate()->getTaxTaviTemplateID(); //รหัสประเภทเทมเพลตของภาษี หัก ณ ที่จ่าย
            //echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getGuaranteeTemplateID(); //รหัสของประเภทเทมเพลตเงินค้ำประกัน


             $sql = "SELECT $db[pl].ADD_DEDUCT_HISTORY.* 
                         FROM $db[pl].ADD_DEDUCT_HISTORY 
                         WHERE $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS != '99'
                         AND $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN ($idtax91,$idtax1,$taxthis)
                         AND $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID  = '$idcard'
                         AND $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE  LIKE '$monthyear'
                         ";

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryAll();
            return $data;

        }


}