<?php

namespace app\modules\vhc\models\service;

use Yii;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_sevice_part".
 *
 * @property integer $id
 * @property integer $vhc_mapping_id
 * @property integer $vhc_labor_id
 * @property integer $part_id
 * @property string $part_bar_code
 * @property string $part_name
 * @property string $price
 * @property string $vat
 * @property string $invatprice
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcSevicePart extends MasterModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_sevice_part';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vhc_mapping_id', 'vhc_labor_id', 'part_id', 'part_bar_code', 'part_name', 'price', 'vat', 'invatprice', 'record_status', 'create_by', 'update_by', 'create_date', 'update_date'], 'required'],
            [['id', 'vhc_mapping_id', 'vhc_labor_id', 'part_id', 'record_status'], 'integer'],
            [['price', 'vat', 'invatprice'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['part_bar_code'], 'string', 'max' => 50],
            [['part_name'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vhc_mapping_id' => 'Vhc Mapping ID',
            'vhc_labor_id' => 'Vhc Labor ID',
            'part_id' => 'Part ID',
            'part_bar_code' => 'Part Bar Code',
            'part_name' => 'Part Name',
            'price' => 'Price',
            'vat' => 'Vat',
            'invatprice' => 'Invatprice',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
