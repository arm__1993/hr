<?php

namespace app\modules\baymanagement\models;

use Yii;

/**
 * This is the model class for table "ehole_baylayout".
 *
 * @property integer $id
 * @property integer $branch_id
 * @property string $layout_name
 * @property string $background_image
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class EholeBaylayout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ehole_baylayout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_id', 'layout_name', 'status_active', 'createby_user', 'create_datetime'], 'required'],
            [['branch_id', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['layout_name', 'background_image'], 'string', 'max' => 100],
            [['createby_user', 'updateby_user'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_id' => 'Branch ID',
            'layout_name' => 'Layout Name',
            'background_image' => 'Background Image',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }


}
