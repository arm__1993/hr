<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[TaxIncomeStructure]].
 *
 * @see TaxIncomeStructure
 */
class TaxIncomeStructureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaxIncomeStructure[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaxIncomeStructure|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
