<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:33
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/adjustsalary.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
// //$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
//$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);


$script = <<< JS
$(document).ready(function() {
setTimeout(function() {
$('#message').fadeOut('slow');
}, 3000);


$("#example2").DataTable({
"lengthChange": false,
"searching": true,
"paging": true,
"info": true,
"pageLength" : 10,
}
);

$("#tablelistdata").DataTable({
"lengthChange": false,
"searching": true,
"paging": true,
"info": true,
"pageLength" : 10,
}
);
});
JS;

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->baseUrl."/fonts/01thaifontcss.css");


$this->registerJs($script);

$inject_date = Yii::$app->request->get('injectdate');
$date_pay = (isset($inject_date) && $inject_date !='') ? $inject_date : date('m-Y');



?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li class="active">ปรับปรุงการจ่ายเงินเดือนรายบุคคล</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <form>
                <div class="row">
                    <div class="col-md-4">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="namefullemp" class="col-sm-2 control-label">ชื่อ</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" name="id_emp" id="id_emp">
                                    <?php $empdata = ApiHr::getempdataall();
                                    foreach ($empdata as $value) {
                                        ?>
                                        <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn  btn-primary btn-sm" id="seachdatalist"><i
                                        class="fa fa-plus-circle"></i> เพิ่มรายชื่อ
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="box box-info" style="display:none" id="showcontent">
                <div class="box-header with-border">
                    <h3 class="box-title">แสดงรายการ</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover dataTable" id="tbemployee">
                        <thead>
                        <tr>
                            <th width="3%" style="width: 3%;text-align: center;">
                                ลำดับ
                            </th>
                            <th width="15%" class="text-left">
                                ชื่อ-สกุล
                            </th>
                            <th width="10%" class="text-left">
                                ตำแหน่ง
                            </th>
                            <th width="10%" class="text-left">
                                ฝ่าย
                            </th>
                            <th width="20%" class="text-left">
                                แผนก
                            </th>
                            <th width="30%" class="text-left">
                                บริษัท
                            </th>
                            <th width="5%" class="text-center">
                                จัดการ
                            </th>
                        </tr>
                        </thead>
                        <tbody id="tblticket">

                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        &nbsp;
                    </div>
                    <div class="col-md-2" style="margin-bottom: 20px;">
                        <button type="button" class="btn btn-block btn-success" style="display:none"
                                id="btnRecal"><i class="fa fa-save"></i> ยืนยันปรับปรุงการสร้างเงินเดือน
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
    <span id="pay_date" title="<?php echo $date_pay; ?>"></span>
</section><!-- /.content -->