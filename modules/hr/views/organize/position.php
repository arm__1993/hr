<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:26
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$selectworking = ApiHr::getWorking_company();
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/position.js?t=' . time(), ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
?>
<section class="content">
    <span id="strSelector" title="<?php echo $strSelector;?>"></span>
    <span id="mode" title="<?php echo $mode;?>"></span>
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li class="active">ตำแหน่งงาน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <form id="reset" name="set">
            <div class="row">
                <div class="col-md-1">
                    &nbsp;
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp" class="col-sm-4 control-label">บริษัท</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="selectworking" id="selectworking"
                                    onchange="getCompanyForDepartment(this,0,0);">
                                <option value="">เลือกบริษัท</option>
                                <?php
                                foreach ($selectworking as $value) {
                                    echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp" class="col-sm-3 control-label">แผนก</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="selectdepartment" id="selectdepartment"
                                    onchange="getDepartmentForSection(this,0,0);">
                                <option value=""> ทั้งหมด</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp" class="col-sm-3 control-label">ฝ่าย</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="selectsection" id="selectsection">
                                <option value=""> ทั้งหมด</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row" style="text-align:center">
                <input type="button" onclick="Search();" class="btn btn-primary btn-md" value="ค้นหา">
                <input type="reset" class="btn btn-warning btn-md" value="ล้าง">
            </div>
            </form>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-10">

                </div>
                <div class="col-md-2">
                    <a data-toggle="modal" onclick="reset()"><img src="<?php echo $imghr; ?>/add.png" class="img-circle">
                        <span>เพิ่มตำแหน่งงาน</span></a>
                </div>
            </div>
            <?php
            {
                Pjax::begin(['id' => 'pjax_grid_position']);
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $modelSearch,
                    'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn',
                            'headerOptions' => ['width' => '20'],
                        ],
                        [
                            'attribute' => 'PositionCode',
                            'label' => 'รหัสตำแหน่ง',
                            'value' => 'PositionCode',
                            //'format' => 'raw',
                            //'filter' => true,
                            'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                        ],

                        [
                            'attribute' => 'companyname',
                            'label' => 'บริษัท',
                            'value' => 'companyname',
                            //'format' => 'raw',
                            //'filter' => true,
                            'contentOptions' => ['style' => 'width: 200px;', 'align=center']
                        ],
                        [
                            'attribute' => 'departmentname',
                            'label' => 'แผนก',
                            'value' => 'departmentname',
                            //'format' => 'raw',
                            //'filter' => true,
                            'contentOptions' => ['style' => 'width: 200px;', 'align=center']
                        ],
                        [
                            'attribute' => 'sectionname',
                            'label' => 'ฝ่าย',
                            'value' => 'sectionname',
                            //'format' => 'raw',
                            //'filter' => true,
                            'contentOptions' => ['style' => 'width: 200px;', 'align=center']
                        ],
                        [
                            'attribute' => 'Name',
                            'label' => 'ชื่อตำแหน่ง',
                            'value' => 'Name',
                            //'format' => 'raw',
                            //'filter' => true,
                            'contentOptions' => ['style' => 'width: 150px;', 'align=center']
                        ],

                        [
                            'headerOptions' => ['width' => '100'],
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'จัดการแผนก',
                            'template' => '{update}{delete}',
                            'buttons' => [
                                'update' => function ($url, $data) use ($arrExistPosition) {

                                    if(!in_array($data->id,$arrExistPosition)) {
                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                            'title' => 'แก้ไข',
                                            'onclick' => '(function($event) {
                                            editposition(' . $data->id . ');
                                    })();'
                                        ]);
                                    }

                                },

                                'delete' => function ($url, $data) use ($arrExistPosition){
                                    if(!in_array($data->id,$arrExistPosition))  {
                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                            'title' => 'ลบ',
                                            'onclick' => '(function($event) {
                                            bootbox.confirm({
                                                size: "small",
                                                message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->Name . '? </h4>",
                                                callback: function(result){
                                                    if(result==1) {
                                                        deletePositionByid(' . $data->id . ');
                                                    }
                                                }
                                            });
                                    
                                    
                                    })();'
                                        ]);
                                    }
                                },
                            ],
                        ],
                    ],
                ]);
                Pjax::end(); //end pjax_gridcorclub
            }
            ?>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
<div class="modal fade" id="addPosition" tabindex="-1" role="dialog" aria-labelledby="ConfirmDialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ยืนยันการบันทึก</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="AddPositionForm" onsubmit="return getdatesubmit();" data-toggle="validator" method="post"
                      class="form-horizontal">
                    <input class="form-control" id="depsrtmentid" value="<?php echo $iddepartment ?>" type="hidden"
                           name="department"/>
                    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" id="_csrf" name="_csrf">
                    <input value="0" type="hidden" id="check" name="check"/>
                    <input value="0" type="hidden" id="id" name="id"/>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">บริษัท</div>
                            <div class="col-md-6">
                                <select class="form-control" name="WorkCompany" id="WorkCompany"
                                        onchange="getCompanyForDepartment(this,1,0)" required/>
                                <option value="">กรุณาเลือก</option>
                                <?php
                                foreach ($selectworking as $value) {
                                    echo '<option value="' . $value['id']. '" data="'.$value['code_name'].'">' . $value['name'] . '</option>';
                                } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">แผนก</div>
                            <div class="col-sm-6">
                                <select class="form-control" name="Department" id="Department"
                                        onchange="getDepartmentForSection(this,1,0);" required>
                                    <option value=""> กรุณาเลือก</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">ฝ่าย</div>
                            <div class="col-sm-6">
                                <select class="form-control" name="Section" id="Section" onchange="GetLevel(this);"
                                        required>
                                    <option value=""> กรุณาเลือก</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">ประเภทงาน</div>
                            <div class="col-sm-6">
                                <select class="form-control" name="WorkType" id="WorkType"
                                        onchange="WorkTypeValue(this);" required>
                                    <option value=""> กรุณาเลือก</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">ระดับ</div>
                            <div class="col-sm-6">
                                <select class="form-control" name="Level" id="Level" onchange="setLevelCode(this);"
                                        required>
                                    <option value=""> กรุณาเลือก</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">ลำดับที่</div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="runNumber" name="runNumber" readonly
                                       required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">รหัสตำแหน่ง</div>
                            <div class="col-md-6">
                                 <input type="text" class="form-control" id="PositionCode" name="PositionCode" readonly="readonly" style="background-color:#00a65a; color: #FFF;font-weight: bold;" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">ชื่อตำแหน่ง</div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="Name" name="Name" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" style="text-align:right">หมายเหตุ</div>
                            <div class="col-md-6">
                                <textarea class="col-md-12" id="Note" name="Note" required> </textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
