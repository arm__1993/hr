<?php


/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/1/2017 AD
 * Time: 10:50
 */

namespace app\modules\hr\apihr;

use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\DetailRecord;
use app\modules\hr\models\Empsalary;
use app\modules\hr\models\HeaderRecord;
use app\modules\hr\models\SummaryMoneyWage;
use app\modules\hr\models\TaxCalculate;
use app\modules\hr\models\TaxCalculateStep;
use app\modules\hr\models\TaxWitholding;
use app\modules\hr\models\TaxWitholdingDetail;
use app\modules\hr\models\Wagehistory;
use app\modules\hr\models\Workingcompany;
use yii\db\Expression;
use yii\db\Transaction;
use yii;

use app\modules\hr\models\Empdata;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Salarystep;
use app\modules\hr\models\Wagethismonth;
use app\modules\hr\models\Vempsalarydetail;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\models\Socialsalary;
use app\modules\hr\models\Benefitfund;
use app\modules\hr\models\SsoIncomeStructure;

use app\api\Helper;
use app\api\DateTime;
use app\api\Utility;
use app\modules\hr\apihr\ApiSSO;
use app\modules\hr\apihr\ApiPayroll;
use app\api\DBConnect;

use app\modules\hr\apihr\ApiHr;

class ApiSalary
{




    //** Edit by Aeedy 4 DEC 2017 */
    public static function wage_date_diff($start, $end)
    {
        $s = strtotime($start);
        $e = strtotime($end);
        return (($e - $s) / 86400) + 1;
    }

    public static function wage_date_range($start,$end)
    {
        $st = strtotime($start);
        $et = strtotime($end);
        $day=0;
        for($i=$st;$i<=$et;$i+=86400) {
            $day++;
        }

        return $day;
    }

    public static function wage_salary_month($salary, $dayofmonth, $days)
    {
        //fix day = 30;
        $dayofmonth = 30;
        $t = ($salary/$dayofmonth) * $days;
        return Utility::wage_round_money($t);
    }


    //Calculate Benefit fund Amount
    public static function cal_benefit_fund($salary, $probation_status, $arrConfig)
    {
        if($probation_status==1) {
            $t = $salary * ($arrConfig['bnf_percent'] / 100);
            return Utility::wage_round_money($t);
        }else {
            return 0;
        }
    }


    //Calculate SSO Amount
    public static function cal_sso_money($salary,$arrConfig)
    {
        $arrSSOStructure = $arrConfig['sso_config'];
        $sso_amount = 0;
        foreach ($arrSSOStructure as $key => $item) {
            if($salary >= $item['income_floor'] && $salary <= $item['income_ceil']) {
                  $sso_amount = ($item['is_calculate_percent']==2) ? $item['sso_rate'] : $salary * ($item['sso_rate']/100);
            }

            if($salary >= $item['income_floor'] && ($item['income_ceil'] == null || $item['income_ceil']==0)) {
                $sso_amount = ($item['is_calculate_percent']==2) ? $item['sso_rate'] : $salary * ($item['sso_rate']/100);
                //echo 'salaray=>'.$salary.' key=>'.$key;
            }
        }
        return Utility::wage_round_money($sso_amount);
    }

    public static function map_sso_prefix($Be) {
        $PREFIX='';
        if ($Be == 'นาย'){
            $PREFIX = '003';
        }elseif ($Be == 'นาง'){
            $PREFIX = '004';
        }elseif ($Be == 'นางสาว'){
            $PREFIX = '005';
        }
        return $PREFIX;
    }


    //Load sso config
    public static function load_sso_config()
    {
        $data = SsoIncomeStructure::find()->where([
            'status_active' => 1
        ])->orderBy('id')->asArray()->all();

        if(count($data) == 0) {
            throw new \Exception('SSO Config not found');
        }

        return $data;
    }

    public static function loadall_recal_thismonth($idcard) {
        $model = Adddeductthismonth::find()
            ->select('*')
            ->from('ADD_DEDUCT_THIS_MONTH')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_THIS_MONTH.ADD_DEDUCT_ID')
            ->where([
                'ADD_DEDUCT_THIS_MONTH.ADD_DEDUCT_DETAIL_EMP_ID' => $idcard
            ])
            ->asArray()
            ->orderBy([
                'ADD_DEDUCT_DETAIL_EMP_ID' => SORT_ASC,
                'ADD_DEDUCT_TEMPLATE_TYPE' => SORT_ASC,
                'ADD_DEDUCT_TEMPLATE_ID' => SORT_ASC,
            ])
            ->all();

        return $model;
    }


    //Fetch all add deduct wage of employee
    public static function loadall_adddeduct($pay_date,$ACTION)
    {

        //** STATUS = 2 รายการ add deduct  ที่ถูกเพิ่มในเดือนนี้ */
        $model = null;
        if($ACTION['mode']=='recal') {

            $model = Adddeductdetail::find()
                ->select('*')
                ->from('ADD_DEDUCT_DETAIL')
                ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
                ->where([
                    'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date,
                    'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS' => 2
                ])
                ->andWhere([
                    'IN','ADD_DEDUCT_DETAIL_EMP_ID',$ACTION['recal_idcard'],
                ])
                ->asArray()
                ->orderBy([
                    'ADD_DEDUCT_DETAIL_EMP_ID' => SORT_ASC,
                    'ADD_DEDUCT_TEMPLATE_TYPE' => SORT_ASC,
                    'ADD_DEDUCT_TEMPLATE_ID' => SORT_ASC,
                ])
                ->all();
            return $model;
        }
        else {
            $model = Adddeductdetail::find()
                ->select('*')
                ->from('ADD_DEDUCT_DETAIL')
                ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
                ->where([
                    'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date,
                    'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS' => 2
                ])
                ->asArray()
                ->orderBy([
                    'ADD_DEDUCT_DETAIL_EMP_ID' => SORT_ASC,
                    'ADD_DEDUCT_TEMPLATE_TYPE' => SORT_ASC,
                    'ADD_DEDUCT_TEMPLATE_ID' => SORT_ASC,
                ])
                ->all();
        }

        return $model;
    }

    public static function loadall_adddeduct_idcard($pay_date,$idcard)
    {
        //** STATUS = 2 รายการ add deduct  ที่ถูกเพิ่มในเดือนนี้ */

        $model = Adddeductdetail::find()
            ->select('*')
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where([
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date,
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID' => $idcard,
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS' => 2
            ])
            ->asArray()
            ->all();

        return $model;
    }

    public static function loadall_adddeductthismonth_idcard($pay_date,$idcard)
    {
        //** STATUS = 2 รายการ add deduct  ที่ถูกเพิ่มในเดือนนี้ */
        //** Query add deduct from add deduct this month for validation salaray **/

        $model = Adddeductthismonth::find()
            ->select('*')
            ->from('ADD_DEDUCT_THIS_MONTH')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_THIS_MONTH.ADD_DEDUCT_ID')
            ->where([
                'ADD_DEDUCT_THIS_MONTH.ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date,
                'ADD_DEDUCT_THIS_MONTH.ADD_DEDUCT_DETAIL_EMP_ID' => $idcard,
            ])
            ->asArray()
            ->all();
        return $model;
    }



    public static function get_all_adddeduct_template() {
        $model = Adddeducttemplate::find()->where([
            'ADD_DEDUCT_TEMPLATE_STATUS' => 1
        ])->asArray()->orderBy(' ADD_DEDUCT_TEMPLATE_ID ASC')->all();
        return yii\helpers\ArrayHelper::index($model,'ADD_DEDUCT_TEMPLATE_ID');
    }

    //Build Add/Deduct wage saperate with IDCARD, TYPE, RECORD ID
    public static function buildAddDeductEmployee($model)
    {
        $arrList = [];
        if(count($model) > 0) {
            foreach ($model as $item) {
                $arrList[$item['ADD_DEDUCT_DETAIL_EMP_ID']][$item['ADD_DEDUCT_TEMPLATE_TYPE']][$item['ADD_DEDUCT_DETAIL_ID']] = $item;
                //print_r($arrList);
            }
        }
        return $arrList;
    }

    /** Method for build add money for employee this month รายการบวกเงินเพิ่มของพนักงาน */
    public static function buildAddMoneyEmployee($model) {
        $arrList = [];
        if(count($model) > 0) {
            foreach ($model as $item) {
                $arrList[$item['ADD_DEDUCT_DETAIL_EMP_ID']][1][$item['ADD_DEDUCT_DETAIL_ID']] = $item;
                //print_r($arrList);
            }
        }
        return $arrList;
    }

    /** Method for build minus money for employee this month  รายการหักเงินของพนักงาน */
    public static function buildMinusMoneyEmployee($model) {
        $arrList = [];
        if(count($model) > 0) {
            foreach ($model as $item) {
                $arrList[$item['ADD_DEDUCT_DETAIL_EMP_ID']][2][$item['ADD_DEDUCT_DETAIL_ID']] = $item;
                //print_r($arrList);
            }
        }
        return $arrList;
    }


    public static function buildAddDeductSummary($model)
    {
        $arrList = [];
        if(is_object($model) || is_array($model)) {
            foreach ($model as $item) {
                $arrList[$item['ADD_DEDUCT_DETAIL_EMP_ID']][$item['ADD_DEDUCT_TEMPLATE_TYPE']][$item['ADD_DEDUCT_DETAIL_ID']] = $item['ADD_DEDUCT_DETAIL_AMOUNT'];
            }
        }

        return $arrList;
    }



    public static function buildAddMoneyPND($arrAddDeDuctEmp) {

        $tax40_2 = 2;
        $tax40_8 = 9;
        $arrItemNormal = [];
        foreach ($arrAddDeDuctEmp as $idcard => $items) {
            if (isset($items[1]) && $idcard !='') {
                foreach ($items[1] as $item) {
                    if ($item['tax_section_id'] != $tax40_2 && $item['tax_section_id'] != $tax40_8) {  //หารายการที่ไม่เป็น 40(2) 40(8)
                        $arrItemNormal[$idcard] += $item['ADD_DEDUCT_DETAIL_AMOUNT'];
                    }
                }
            }
        }

        return $arrItemNormal;
    }



    public static function insert_cal($connection)
    {
        $empIDCard= '3560500575223';
        $sql_master = "INSERT INTO `pnd_test` (`id`, `idcard`) VALUES(null,'$empIDCard')";
        //echo $sql_master;
        //$connection = Yii::$app->dbERP_easyhr_PAYROLL;
        return $connection->createCommand($sql_master)->execute();
    }




    //*** Reset Salary and Option ***/

    public static function resetSalary($pay_date, $arrIDCard,$arrAutoTemplateID,$connection)
    {
        $arrEmpIDCard = (!is_array($arrIDCard)) ? $arrEmpIDCard[$arrIDCard] = $arrIDCard : $arrIDCard;
        $data = self::resetWageThisMonth($arrEmpIDCard);
        self::resetAddDeductThisMonth($arrEmpIDCard);
        self::resetSummaryWage($pay_date,$arrEmpIDCard,false);
        self::resetBenefitFund($pay_date,$arrEmpIDCard,false);
        //self::resetWitholdingTax($pay_date,$arrEmpIDCard,false);
        self::resetPNDTax($pay_date,$arrEmpIDCard,false);
        return $data;
    }

    public static function resetAddDeductThisMonth($arrEmpIDCard)
    {
        return Adddeductthismonth::deleteAll([
            'IN','ADD_DEDUCT_DETAIL_EMP_ID',$arrEmpIDCard
        ]);
    }


    public static function resetWageThisMonth($arrEmpIDCard)
    {
        return Wagethismonth::deleteAll([
            'IN','WAGE_EMP_ID',$arrEmpIDCard
        ]);
    }

    public static function resetWitholdingTax($pay_date,$arrEmpIDCard,$clear=false)
    {
        if($clear===true) {
            TaxWitholdingDetail::deleteAll([
                'date_pay' => $pay_date,
            ]);

            return TaxWitholding::deleteAll([
                'date_pay' => $pay_date
            ]);
        }
        else {
            $strEmpID = Utility::JoinArrayToString($arrEmpIDCard);
            $cond = " date_pay='$pay_date' AND emp_idcard IN ($strEmpID) ";
            TaxWitholdingDetail::deleteAll($cond);
            return TaxWitholding::deleteAll($cond);
        }
    }


    public static function resetPNDTax($pay_date,$arrEmpIDCard,$clear=false)
    {
        if($clear===true) {
            TaxCalculateStep::deleteAll([
                'pay_date' => $pay_date,
            ]);

            return TaxCalculate::deleteAll([
                'pay_date' => $pay_date
            ]);
        }
        else {
//            $strEmpID = Utility::JoinArrayToString($arrEmpIDCard);
//            $cond = " for_month='$pay_date' AND emp_idcard IN ($strEmpID) ";
//            TaxCalculateStep::deleteAll($cond);
//            return TaxCalculate::deleteAll($cond);

            TaxCalculateStep::deleteAll(['and',
                'pay_date = :pay_date',
                ['in', 'emp_idcard', $arrEmpIDCard]],
                [':pay_date' =>$pay_date]
            );

            return TaxCalculate::deleteAll(['and',
                'pay_date = :pay_date',
                ['in', 'emp_idcard', $arrEmpIDCard]],
                [':pay_date' =>$pay_date]
            );

        }

    }

    public static function Header_Record($pay_date)
    {
       HeaderRecord::deleteAll([
           'pay_date'=>$pay_date,
       ]);

    }

    public static function Detail_Record($pay_date)
    {
        DetailRecord::deleteAll([
            'pay_date'=>$pay_date,
        ]);

    }


    public static function resetSummaryWage($pay_date,$arrEmpIDCard,$clear=false)
    {
        if($clear==true) {
            return SummaryMoneyWage::deleteAll([
                'pay_date' => $pay_date,
            ]);
        }
        else {
            return SummaryMoneyWage::deleteAll(['and',
                'pay_date = :pay_date',
                ['in', 'emp_idcard', $arrEmpIDCard]],
                [':pay_date' =>$pay_date]
            );
        }
    }


    public static function resetBenefitFund($pay_date,$arrEmpIDCard,$clear=false)
    {
        if($clear==true) {
            return Benefitfund::deleteAll([
                'BENEFIT_SAVING_DATE' => $pay_date,
            ]);
        }
        else {
            return Benefitfund::deleteAll(['and',
                'BENEFIT_SAVING_DATE = :pay_date',
                ['in', 'BENEFIT_EMP_ID', $arrEmpIDCard]],
                [':pay_date' =>$pay_date]
            );
        }
    }



    public static function buildSalaryDateCondition($pay_date)
    {
        //Load config from DB
        //$cal_salary_date = ApiHr::getOrgMasterConfig()->getDayStartSalary();
        $HrConfig = ApiHr::getOrgMasterConfig();
        $cal_salary_date    = $HrConfig->getDayStartSalary();
        $workday_of_month   = $HrConfig->getWorkDayInMonth();
        $workhour_of_day    = $HrConfig->getWorkHourInDay();

        $sso_percent        = $HrConfig->getSSORate();
        $bnf_percent        = $HrConfig->getSavingRate();

        //Load SSO config
        $SSO_Config = ApiSalary::load_sso_config();

        //TODO : IF config date is 1
        $end_date = $cal_salary_date - 1;
        $end_between_date = $end_date - 1;


        $_temp = explode('-',$pay_date);
        $_m = $_temp[0];
        $_y = $_temp[1];
        $_d = $cal_salary_date;

        $salary_today_date = $_y.'-'.$_m.'-'.$_d;
        $salary_today_timestamp = strtotime($salary_today_date);  //current month timestamp
        $salary_prvday_timestamp = strtotime("-1 month",$salary_today_timestamp); //previous month timestamp

//        echo $_today;
//        $t = strtotime($_today);
//        echo '==>'.$t;
//        $lt  = strtotime("-1 month",$t);
//        echo 'last month time=>'.$lt;
//        echo 'last date=>'.date('Y-m-d',$lt);
//        //echo date('Y-m-d'
//        exit;


        //IF calculate today 2017-12-26
//        $salary_start_date = date('Y-m', strtotime("-1 month")) . '-' . $cal_salary_date;  //eg 2017-11-26
//        $salary_end_date = date('Y-m') . '-' . $end_date; //eg 2017-12-25
//        $salary_end_between_date = date('Y-m') . '-' . $end_between_date; //eg 2017-12-24
//        $salary_today_date = date('Y-m') . '-' . $cal_salary_date; //eg 2017-12-26

        $salary_start_date = date('Y-m', $salary_prvday_timestamp) . '-' . $cal_salary_date;  //eg 2017-11-26
        $salary_end_date = date('Y-m',$salary_today_timestamp) . '-' . $end_date; //eg 2017-12-25
        $salary_end_between_date = date('Y-m',$salary_today_timestamp) . '-' . $end_between_date; //eg 2017-12-24
        //$salary_today_date = $salary_today_date; //eg 2017-12-26



        //calculate date range
        $total_day = self::wage_date_range($salary_start_date,$salary_end_date);

        $data['start_date'] = $salary_start_date;
        $data['end_date'] = $salary_end_date;
        $data['end_between_date'] = $salary_end_between_date;
        $data['today_date'] = $salary_today_date;

        //$data['work_day'] = $workday_of_month;
        $data['work_day'] = $total_day;
        $data['work_hour'] = $workhour_of_day;
        $data['pay_date'] = $pay_date; //รอบเงินเดือน

        $data['sso_percent'] = $sso_percent;
        $data['bnf_percent'] = $bnf_percent;
        $data['sso_config']  = $SSO_Config;

        return $data;
    }

    public static function AdjustAddDeductDetail($connection,$pay_date,$arrDataItem,$Salary_template)
    {

        $arrData = null;
        $_table = 'ADD_DEDUCT_THIS_MONTH';
        $detailColumns = Adddeductthismonth::getTableSchema()->getColumnNames();
        $dateRang = DateTime::makeDayFromMonthPicker($pay_date);

        $end_date = $dateRang['end_date'];
        $action_user = Yii::$app->session->get('idcard');
        $eff = 0;
        $_template_id = $Salary_template['ADD_DEDUCT_TEMPLATE_ID'];
        $_template_name = $Salary_template['ADD_DEDUCT_TEMPLATE_NAME'];

        foreach ($arrDataItem as $item) {
            $extra = ($item['item_detail_extra']) ? ' ('.$item['item_detail_extra'].')' : null;
            $arrData[] = [
                $detailColumns[0]  => NULL,  //ADD_DEDUCT_DETAIL_ID
                $detailColumns[1]  => $_template_id, //ADD_DEDUCT_ID
                $detailColumns[2]  => $_template_name.$extra, //ADD_DEDUCT_DETAIL
                $detailColumns[3]  => $pay_date, //ADD_DEDUCT_DETAIL_PAY_DATE
                $detailColumns[4]  => $end_date, //ADD_DEDUCT_DETAIL_START_USE_DATE
                $detailColumns[5]  => $end_date, //ADD_DEDUCT_DETAIL_END_USE_DATE
                $detailColumns[6]  => null, //INSTALLMENT_START
                $detailColumns[7]  => null, //INSTALLMENT_END
                $detailColumns[8]  => null, //INSTALLMENT_CURRENT
                $detailColumns[9]  => $item['idcard'], //ADD_DEDUCT_DETAIL_EMP_ID
                $detailColumns[10] => $item['item_amount'],  //ADD_DEDUCT_DETAIL_AMOUNT
                $detailColumns[11] => '2',  //ADD_DEDUCT_DETAIL_TYPE
                $detailColumns[12] => '2',  //ADD_DEDUCT_DETAIL_STATUS
                $detailColumns[13] => null,  //ADD_DEDUCT_DETAIL_HISTORY_STATUS
                $detailColumns[14] => null,  //ADD_DEDUCT_DETAIL_GROUP_STATUS
                $detailColumns[15] => new Expression('NOW()'),  //ADD_DEDUCT_DETAIL_CREATE_DATE
                $detailColumns[16] => $action_user,  //ADD_DEDUCT_DETAIL_CREATE_BY
                $detailColumns[17] => $action_user,  //ADD_DEDUCT_DETAIL_UPDATE_BY
                $detailColumns[18] => new Expression('NOW()'),  //ADD_DEDUCT_DETAIL_UPDATE_DATE
                $detailColumns[19] => $action_user,  //ADD_DEDUCT_DETAIL_RESP_PERSON
                $detailColumns[20] => null,  //dept_deduct_id
                $detailColumns[21] => null,  //dept_deduct_detail_id
            ];
        }


        $eff += $connection->createCommand()->batchInsert($_table, $detailColumns, $arrData)->execute();
        return $eff;
    }

    public static function RecalInsertThisMonth($pay_date,$Salary_template,$recal_idcard,$amount)
    {

        $dateRang = DateTime::makeDayFromMonthPicker($pay_date);

        $end_date = $dateRang['end_date'];
        $action_user = Yii::$app->session->get('idcard');
        $eff = 0;
        $_template_id = $Salary_template['ADD_DEDUCT_TEMPLATE_ID'];
        $_template_name = $Salary_template['ADD_DEDUCT_TEMPLATE_NAME'];

        $ObjThisMonth = new Adddeductthismonth();
        $ObjThisMonth->ADD_DEDUCT_DETAIL_ID = null;
        $ObjThisMonth->ADD_DEDUCT_ID = "$_template_id";
        $ObjThisMonth->ADD_DEDUCT_DETAIL =$_template_name;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_PAY_DATE = $pay_date;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_START_USE_DATE = $dateRang['start_date'];
        $ObjThisMonth->ADD_DEDUCT_DETAIL_END_USE_DATE = $dateRang['end_date'];
        $ObjThisMonth->INSTALLMENT_START =  null;
        $ObjThisMonth->INSTALLMENT_END  = null;
        $ObjThisMonth->INSTALLMENT_CURRENT = null;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_EMP_ID = $recal_idcard;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_AMOUNT = $amount;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_TYPE = "2"; //รายเดือน
        $ObjThisMonth->ADD_DEDUCT_DETAIL_STATUS  = 2;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_HISTORY_STATUS = null;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_GROUP_STATUS = null;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_CREATE_DATE =  new Expression('NOW()');
        $ObjThisMonth->ADD_DEDUCT_DETAIL_CREATE_BY = $action_user;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_UPDATE_BY = $action_user;
        $ObjThisMonth->ADD_DEDUCT_DETAIL_UPDATE_DATE =  new Expression('NOW()');
        $ObjThisMonth->ADD_DEDUCT_DETAIL_RESP_PERSON = $action_user;
        $ObjThisMonth->dept_deduct_id = null;
        $ObjThisMonth->dept_deduct_detail_id = null;
        return $ObjThisMonth->save(false);
    }




    public static function queryEmpSalary()
    {

        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $sql = "SELECT 
                $db[ct].emp_data.DataNo as emp_id,
                $db[ct].emp_data.Code as MainCode,
                $db[ct].emp_data.Position as MainPosition,
                $db[ct].emp_data.Salary_Bank as Salary_Bank,
                $db[ct].emp_data.Salary_BankNo as Salary_BankNo,
                $db[ct].emp_data.SalaryViaBank as SalaryViaBank,
                $db[pl].EMP_SALARY.EMP_SALARY_POSITION_CODE as POSITION_CODE,
                $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY as WORKING_COMPANY,
                $db[pl].EMP_SALARY.EMP_SALARY_CHART as EMP_SALARY_CHART,
                $db[pl].EMP_SALARY.EMP_SALARY_STEP as EMP_SALARY_STEP,
                $db[pl].EMP_SALARY.EMP_SALARY_LEVEL as EMP_SALARY_LEVEL,
                $db[pl].EMP_SALARY.status as active_status,
                $db[pl].EMP_SALARY.statusSSO as is_cal_sso,
                $db[pl].EMP_SALARY.statusCalculate as is_cal_salary,
                $db[pl].EMP_SALARY.statusMainJob as is_main_job,
                $db[pl].EMP_SALARY.start_effective_date as start_effective_date,
                $db[pl].EMP_SALARY.end_effective_date as end_effective_date,
              
                $db[ct].emp_data.ID_Card as ID_Card,
                $db[ct].emp_data.Name as first_name,
                $db[ct].emp_data.Surname as last_name,
                $db[pl].EMP_SALARY.EMP_SALARY_WAGE as EMP_SALARY_WAGE,
                $db[pl].EMP_SALARY.EMP_SALARY_CHART_ID as EMP_SALARY_CHART_ID,
                $db[ct].emp_data.socialBenifitPercent as socialBenifitPercent,
                $db[ct].emp_data.Working_Company as MainWorking_Company,
                $db[ct].emp_data.Department as MainDepartment,
                $db[ct].emp_data.Section as MainSection,
                $db[ct].emp_data.socialBenefitStatus as socialBenefitStatus,
                $db[ct].emp_data.benefitFundPercent as benefitFundPercent,
                $db[ct].emp_data.benifitFundStatus as benifitFundStatus,
                $db[ct].emp_data.Start_date as Start_date,
	            $db[ct].emp_data.End_date as End_date,
	            $db[ct].emp_data.Prosonnal_Being as probation_status,
                $db[pl].SALARY_STEP.SALARY_STEP_CHART_NAME,
                $db[pl].SALARY_STEP.SALARY_STEP_START_SALARY,
                $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY as POS_COMPANY_ID,
                $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT as POS_DEPARTMENT_ID,
                $db[pl].EMP_SALARY.EMP_SALARY_SECTION as POS_SECTION_ID,
                $db[ct].emp_data.Be as Be
                FROM $db[ct].emp_data
                INNER JOIN $db[pl].EMP_SALARY ON $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD =  $db[ct].emp_data.ID_Card 
                LEFT  JOIN $db[pl].SALARY_STEP ON $db[pl].EMP_SALARY.EMP_SALARY_CHART_ID = $db[pl].SALARY_STEP.SALARY_STEP_ID
                WHERE 1=1 ";

        return $sql;
    }


    public static function getEmployeeSalary($arrConfig,$arrAddDuctSummary,$ACTION)
    {

        //Array SSO item for add deduct detail
        $arrSSOItems = [];

        //พนักงานปกติ และทดลองงาน รับเงินเดือนเต็มเดือน, พนักงานที่เริ่มงานก่อนรอบเงินเดือนนี้
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $date = $arrConfig['start_date'];
        $sql = self::queryEmpSalary();

        if($ACTION['mode']==='' || $ACTION['mode'] === null) {
            throw  new  \Exception('ERROR !!! action mode not found');
        }

        //mode recal
        if($ACTION['mode']==='recal')
        {
            $arrIDCard = $ACTION['recal_idcard']; //ID Card for recal
            $strIDCard = Utility::JoinArrayToString($arrIDCard);
            $sql .= " AND $db[ct].emp_data.ID_Card IN($strIDCard)  ";
        }

        //mode normal
        if($ACTION['mode']==='normal') {
            $sql .= " AND $db[ct].emp_data.Prosonnal_Being IN (1,2,4) AND $db[ct].emp_data.username != '#####Out Off###' ";
            $sql .= " AND $db[ct].emp_data.Start_date <= '$date' ";
            $sql .= " ORDER BY Working_Company,ID_Card,Code ASC ";
        }


        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();

        $arrList = [];
        foreach ($data as $item) {

            //sso and benefit
            $sso_money = ($item['is_cal_sso']==1) ? self::cal_sso_money($item['EMP_SALARY_WAGE'],$arrConfig) : 0;
            $bnf_money = self::cal_benefit_fund($item['EMP_SALARY_WAGE'],$item['probation_status'],$arrConfig);

            //add deduct calculate
            $add_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][1])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][1]) : 0; //ADD
            $deduct_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][2])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][2]) : 0; //DEDUCT

            //net income per month
            $_total_income  = ($item['EMP_SALARY_WAGE'] + $add_money_total);


            //calculate Withholding Tax


            //tax calculate
            //$pnd_tax = ($item['ID_Card']=='3560500575223') ? 850 : 0;
            //$wht_tax = ($item['ID_Card']=='3560500575223') ? 500 : 0;

            $pnd_tax = 0;
            $wht_tax = 0;
            $grt_money = 0;

            $_total_deduct = ($deduct_money_total + $sso_money + $pnd_tax + $wht_tax);

            //net total money =
            $net_total = $_total_income - $_total_deduct;

            $arrData = [
                'emp_id' => $item['emp_id'],
                'MainCode' => $item['MainCode'],
                'MainPosition' => $item['MainPosition'],
                'MainWorking_Company' => $item['MainWorking_Company'],
                'MainDepartment' => $item['MainDepartment'],
                'MainSection' => $item['MainSection'],
                'Salary_Bank' => $item['Salary_Bank'],
                'Salary_BankNo' => $item['Salary_BankNo'],
                'SalaryViaBank' => $item['SalaryViaBank'],
                'POSITION_CODE' => $item['POSITION_CODE'],
                'WORKING_COMPANY' => $item['WORKING_COMPANY'],
                'EMP_SALARY_CHART' => $item['EMP_SALARY_CHART'],
                'EMP_SALARY_STEP' => $item['EMP_SALARY_STEP'],
                'EMP_SALARY_LEVEL' => $item['EMP_SALARY_LEVEL'],
                'active_status' => $item['active_status'],
                'is_cal_sso' => $item['is_cal_sso'],
                'is_cal_salary' => $item['is_cal_salary'],
                'is_main_job' => $item['is_main_job'],
                'start_effective_date' => $item['start_effective_date'],
                'end_effective_date' => $item['end_effective_date'],
                'ID_Card' => $item['ID_Card'],
                'first_name' => $item['first_name'],
                'last_name' => $item['last_name'],
                'POS_COMPANY_ID' => $item['POS_COMPANY_ID'],
                'POS_DEPARTMENT_ID' => $item['POS_DEPARTMENT_ID'],
                'POS_SECTION_ID' => $item['POS_SECTION_ID'],
                'EMP_SALARY_WAGE' => $item['EMP_SALARY_WAGE'],
                'EMP_SALARY_CHART_ID' => $item['EMP_SALARY_CHART_ID'],
                'socialBenifitPercent' => $item['socialBenifitPercent'],
                'socialBenefitStatus' => $item['socialBenefitStatus'],
                'benefitFundPercent' => $item['benefitFundPercent'],
                'benifitFundStatus' => $item['benifitFundStatus'],
                'Start_date' => $item['Start_date'],
                'End_date' => $item['End_date'],
                'probation_status' => $item['probation_status'],
                'SALARY_STEP_CHART_NAME' => $item['SALARY_STEP_CHART_NAME'],
                'SALARY_STEP_START_SALARY' => $item['SALARY_STEP_START_SALARY'],
                'get_start_date' => $arrConfig['start_date'],
                'get_end_date' => $arrConfig['end_date'],
                'get_total_date' => $arrConfig['work_day'],
                'is_monthly' => 1,
                'cal_grt_amount' => $grt_money, //guarantee money
                'cal_bnf_amount'=> $bnf_money, //benefit fund
                'cal_sso_amount'=> $sso_money, //sso fund
                'cal_pnd_amount'=>$pnd_tax, //PND Tax
                'cal_wht_amount'=>$wht_tax, //Withoding Tax
                'cal_add_money' =>$add_money_total, //add money
                'cal_deduct_money' => $_total_deduct, //deduct money
                'cal_net_amount'=> $net_total,
                'cal_total_income' =>$_total_income,
                'pay_date'=>$arrConfig['pay_date'],
                'Be' => $item['Be']

            ];

            $arrList[] = $arrData;
        }

        return $arrList;
    }

    public static function getEmployeeSalaryStartBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION)
    {
        //พนักงานเริ่มงานระหว่างเดือนรับเงินเดือนตามวันที่ทำงาน นำเงินเดือนมาหารเป็นวัน
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $date = $arrConfig['start_date'];
        $sql = self::queryEmpSalary();


        if($ACTION['mode']==='' || $ACTION['mode'] === null) {
            throw  new  \Exception('ERROR !!! action mode not found');
        }

        //mode recal
        if($ACTION['mode']==='recal')
        {
            $arrIDCard = $ACTION['recal_idcard']; //ID Card for recal
            $strIDCard = Utility::JoinArrayToString($arrIDCard);
            $sql .= " AND $db[ct].emp_data.ID_Card IN($strIDCard)  ";
        }

        $sql .= " AND $db[ct].emp_data.Prosonnal_Being IN (1,2)  ";
        $sql .= " AND $db[ct].emp_data.Start_date > '$date' ";
        $sql .= " ORDER BY Working_Company,ID_Card,Code ASC ";


        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();

        $arrList = [];
        foreach ($data as $item) {

            $_total_work_day = self::wage_date_diff($item['Start_date'],$arrConfig['end_date']);
            $_total_salary = self::wage_salary_month($item['EMP_SALARY_WAGE'],$arrConfig['work_day'],$_total_work_day);


            //sso and benefit
            $sso_money = ($item['is_cal_sso']==1) ? self::cal_sso_money($_total_salary,$arrConfig) : 0;
            $bnf_money = self::cal_benefit_fund($_total_salary,$item['probation_status'],$arrConfig);

            //add deduct calculate
            $add_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][1])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][1]) : 0; //ADD
            $deduct_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][2])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][2]) : 0; //DEDUCT

            //tax calculate
            $pnd_tax = 0;
            $wht_tax = 0;
            $grt_money = 0;



            $_total_income  = ($_total_salary + $add_money_total);
            $_total_deduct = ($deduct_money_total + $sso_money + $pnd_tax + $wht_tax);
            $net_total = $_total_income - $_total_deduct;


            $arrData = [
                'emp_id' => $item['emp_id'],
                'MainCode' => $item['MainCode'],
                'MainPosition' => $item['MainPosition'],
                'MainWorking_Company' => $item['MainWorking_Company'],
                'MainDepartment' => $item['MainDepartment'],
                'MainSection' => $item['MainSection'],
                'Salary_Bank' => $item['Salary_Bank'],
                'Salary_BankNo' => $item['Salary_BankNo'],
                'SalaryViaBank' => $item['SalaryViaBank'],
                'POSITION_CODE' => $item['POSITION_CODE'],
                'WORKING_COMPANY' => $item['WORKING_COMPANY'],
                'EMP_SALARY_CHART' => $item['EMP_SALARY_CHART'],
                'EMP_SALARY_STEP' => $item['EMP_SALARY_STEP'],
                'EMP_SALARY_LEVEL' => $item['EMP_SALARY_LEVEL'],
                'active_status' => $item['active_status'],
                'is_cal_sso' => $item['is_cal_sso'],
                'is_cal_salary' => $item['is_cal_salary'],
                'is_main_job' => $item['is_main_job'],
                'start_effective_date' => $item['start_effective_date'],
                'end_effective_date' => $item['end_effective_date'],
                'ID_Card' => $item['ID_Card'],
                'first_name' => $item['first_name'],
                'last_name' => $item['last_name'],
                'POS_COMPANY_ID' => $item['POS_COMPANY_ID'],
                'POS_DEPARTMENT_ID' => $item['POS_DEPARTMENT_ID'],
                'POS_SECTION_ID' => $item['POS_SECTION_ID'],
                'EMP_SALARY_WAGE' => $_total_salary,
                'EMP_SALARY_CHART_ID' => $item['EMP_SALARY_CHART_ID'],
                'socialBenifitPercent' => $item['socialBenifitPercent'],
                'socialBenefitStatus' => $item['socialBenefitStatus'],
                'benefitFundPercent' => $item['benefitFundPercent'],
                'benifitFundStatus' => $item['benifitFundStatus'],
                'Start_date' => $item['Start_date'],
                'End_date' => $item['End_date'],
                'probation_status' => $item['probation_status'],
                'SALARY_STEP_CHART_NAME' => $item['SALARY_STEP_CHART_NAME'],
                'SALARY_STEP_START_SALARY' => $item['SALARY_STEP_START_SALARY'],
                'get_start_date' => $item['Start_date'],
                'get_end_date' => $arrConfig['end_date'],
                'get_total_date' => $_total_work_day,
                'is_monthly' => 0,
                'cal_grt_amount' => $grt_money, //guarantee money
                'cal_bnf_amount'=> $bnf_money, //benefit fund
                'cal_sso_amount'=> $sso_money, //sso fund
                'cal_pnd_amount'=>$pnd_tax, //PND Tax
                'cal_wht_amount'=>$wht_tax, //Withoding Tax
                'cal_add_money' =>$add_money_total, //add money
                'cal_deduct_money' => $_total_deduct, //deduct money
                'cal_net_amount'=> $net_total,
                'cal_total_income' => $_total_income,
                'pay_date'=>$arrConfig['pay_date'],
                'Be' => $item['Be']
            ];

            $arrList[] = $arrData;
        }

        return $arrList;
    }

    public static function getEmployeeSalaryOffBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION)
    {
        //พนักงานลาออกระหว่างเดือนรับเงินเดือนตามวันที่ทำงาน นำเงินเดือนมาหารเป็นวัน
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $date = $arrConfig['start_date'];
        $date_between = $arrConfig['end_between_date'];

        $sql = self::queryEmpSalary();

        if($ACTION['mode']==='' || $ACTION['mode'] === null) {
            throw  new  \Exception('ERROR !!! action mode not found');
        }


        //mode recal
        if($ACTION['mode']==='recal')
        {
            $arrIDCard = $ACTION['recal_idcard']; //ID Card for recal
            $strIDCard = Utility::JoinArrayToString($arrIDCard);
            $sql .= " AND $db[ct].emp_data.ID_Card IN($strIDCard)  ";
        }


        $sql .= " AND $db[ct].emp_data.Prosonnal_Being = 3 ";
        $sql .= " AND $db[ct].emp_data.End_date > '$date' AND  $db[ct].emp_data.End_date <= '$date_between'";
        $sql .= " ORDER BY Working_Company,ID_Card,Code ASC ";


        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();

        $arrList = [];
        foreach ($data as $item) {

            $_total_work_day = self::wage_date_diff($arrConfig['start_date'],$item['End_date']);
            $_total_salary = self::wage_salary_month($item['EMP_SALARY_WAGE'],$arrConfig['work_day'],$_total_work_day);

            //sso and benefit
            $sso_money = ($item['is_cal_sso']==1) ? self::cal_sso_money($_total_salary,$arrConfig) : 0;
            $bnf_money = self::cal_benefit_fund($_total_salary,$item['probation_status'],$arrConfig);

            //add deduct calculate
            $add_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][1])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][1]) : 0; //ADD
            $deduct_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][2])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][2]) : 0; //DEDUCT

            //tax calculate
            $pnd_tax = 0;
            $wht_tax = 0;
            $grt_money = 0;


            $_total_income = ($_total_salary + $add_money_total);
            $_total_deduct = ($deduct_money_total + $sso_money + $pnd_tax + $wht_tax);

            //net total money =
            $net_total = $_total_income - $_total_deduct;

            $arrData = [
                'emp_id' => $item['emp_id'],
                'MainCode' => $item['MainCode'],
                'MainPosition' => $item['MainPosition'],
                'MainWorking_Company' => $item['MainWorking_Company'],
                'MainDepartment' => $item['MainDepartment'],
                'MainSection' => $item['MainSection'],
                'Salary_Bank' => $item['Salary_Bank'],
                'Salary_BankNo' => $item['Salary_BankNo'],
                'SalaryViaBank' => $item['SalaryViaBank'],
                'POSITION_CODE' => $item['POSITION_CODE'],
                'WORKING_COMPANY' => $item['WORKING_COMPANY'],
                'EMP_SALARY_CHART' => $item['EMP_SALARY_CHART'],
                'EMP_SALARY_STEP' => $item['EMP_SALARY_STEP'],
                'EMP_SALARY_LEVEL' => $item['EMP_SALARY_LEVEL'],
                'active_status' => $item['active_status'],
                'is_cal_sso' => $item['is_cal_sso'],
                'is_cal_salary' => $item['is_cal_salary'],
                'is_main_job' => $item['is_main_job'],
                'start_effective_date' => $item['start_effective_date'],
                'end_effective_date' => $item['end_effective_date'],
                'ID_Card' => $item['ID_Card'],
                'first_name' => $item['first_name'],
                'last_name' => $item['last_name'],
                'POS_COMPANY_ID' => $item['POS_COMPANY_ID'],
                'POS_DEPARTMENT_ID' => $item['POS_DEPARTMENT_ID'],
                'POS_SECTION_ID' => $item['POS_SECTION_ID'],
                'EMP_SALARY_WAGE' => $_total_salary,
                'EMP_SALARY_CHART_ID' => $item['EMP_SALARY_CHART_ID'],
                'socialBenifitPercent' => $item['socialBenifitPercent'],
                'socialBenefitStatus' => $item['socialBenefitStatus'],
                'benefitFundPercent' => $item['benefitFundPercent'],
                'benifitFundStatus' => $item['benifitFundStatus'],
                'Start_date' => $item['Start_date'],
                'End_date' => $item['End_date'],
                'probation_status' => $item['probation_status'],
                'SALARY_STEP_CHART_NAME' => $item['SALARY_STEP_CHART_NAME'],
                'SALARY_STEP_START_SALARY' => $item['SALARY_STEP_START_SALARY'],
                'get_start_date' => $arrConfig['start_date'],
                'get_end_date' => $item['End_date'],
                'get_total_date' => $_total_work_day,
                'is_monthly' => 0,
                'cal_grt_amount' => $grt_money, //guarantee money
                'cal_bnf_amount'=> $bnf_money, //benefit fund
                'cal_sso_amount'=> $sso_money, //sso fund
                'cal_pnd_amount'=>$pnd_tax, //PND Tax
                'cal_wht_amount'=>$wht_tax, //Withoding Tax
                'cal_add_money' =>$add_money_total, //add money
                'cal_deduct_money' => $_total_deduct, //deduct money
                'cal_net_amount'=> $net_total,
                'cal_total_income' => $_total_income,
                'pay_date'=>$arrConfig['pay_date'],
                'Be' => $item['Be']

            ];

            $arrList[] = $arrData;
        }

        return $arrList;
    }

    public static function getEmployeeSalaryOffThisMonth($arrConfig, $arrAddDuctSummary, $ACTION)
    {

        //พนักงานลาออกวันสุดท้ายของเดือนรับเงินเดือนเต็มเดือน
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $end_date = $arrConfig['end_date'];
        $sql = self::queryEmpSalary();

        if($ACTION['mode']==='' || $ACTION['mode'] === null) {
            throw  new  \Exception('ERROR !!! action mode not found');
        }

        //mode recal
        if($ACTION['mode']==='recal')
        {
            $arrIDCard = $ACTION['recal_idcard']; //ID Card for recal
            $strIDCard = Utility::JoinArrayToString($arrIDCard);
            $sql .= " AND $db[ct].emp_data.ID_Card IN($strIDCard)  ";
        }



        $sql .= " AND $db[ct].emp_data.Prosonnal_Being = 3 ";
        $sql .= " AND $db[ct].emp_data.End_date = '$end_date'";
        $sql .= " ORDER BY Working_Company,ID_Card,Code ASC ";

        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();

        $arrList = [];
        foreach ($data as $item) {

            //sso and benefit
            $sso_money = ($item['is_cal_sso']==1) ? self::cal_sso_money($item['EMP_SALARY_WAGE'],$arrConfig) : 0;
            $bnf_money = self::cal_benefit_fund($item['EMP_SALARY_WAGE'],$item['probation_status'],$arrConfig);

            //add deduct calculate
            $add_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][1])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][1]) : 0; //ADD
            $deduct_money_total = (isset($arrAddDuctSummary[$item['ID_Card']][2])) ? array_sum($arrAddDuctSummary[$item['ID_Card']][2]) : 0; //DEDUCT

            //tax calculate
            $pnd_tax = 0;
            $wht_tax = 0;
            $grt_money = 0;

            $_total_income  = ($item['EMP_SALARY_WAGE'] + $add_money_total);

            $_total_deduct = ($deduct_money_total + $sso_money + $pnd_tax + $wht_tax);

            //net total money =
            $net_total = $_total_income - $_total_deduct;

            $arrData = [
                'emp_id' => $item['emp_id'],
                'MainCode' => $item['MainCode'],
                'MainPosition' => $item['MainPosition'],
                'MainWorking_Company' => $item['MainWorking_Company'],
                'MainDepartment' => $item['MainDepartment'],
                'MainSection' => $item['MainSection'],
                'Salary_Bank' => $item['Salary_Bank'],
                'Salary_BankNo' => $item['Salary_BankNo'],
                'SalaryViaBank' => $item['SalaryViaBank'],
                'POSITION_CODE' => $item['POSITION_CODE'],
                'WORKING_COMPANY' => $item['WORKING_COMPANY'],
                'EMP_SALARY_CHART' => $item['EMP_SALARY_CHART'],
                'EMP_SALARY_STEP' => $item['EMP_SALARY_STEP'],
                'EMP_SALARY_LEVEL' => $item['EMP_SALARY_LEVEL'],
                'active_status' => $item['active_status'],
                'is_cal_sso' => $item['is_cal_sso'],
                'is_cal_salary' => $item['is_cal_salary'],
                'is_main_job' => $item['is_main_job'],
                'start_effective_date' => $item['start_effective_date'],
                'end_effective_date' => $item['end_effective_date'],
                'ID_Card' => $item['ID_Card'],
                'first_name' => $item['first_name'],
                'last_name' => $item['last_name'],
                'POS_COMPANY_ID' => $item['POS_COMPANY_ID'],
                'POS_DEPARTMENT_ID' => $item['POS_DEPARTMENT_ID'],
                'POS_SECTION_ID' => $item['POS_SECTION_ID'],
                'EMP_SALARY_WAGE' => $item['EMP_SALARY_WAGE'],
                'EMP_SALARY_CHART_ID' => $item['EMP_SALARY_CHART_ID'],
                'socialBenifitPercent' => $item['socialBenifitPercent'],
                'socialBenefitStatus' => $item['socialBenefitStatus'],
                'benefitFundPercent' => $item['benefitFundPercent'],
                'benifitFundStatus' => $item['benifitFundStatus'],
                'Start_date' => $item['Start_date'],
                'End_date' => $item['End_date'],
                'probation_status' => $item['probation_status'],
                'SALARY_STEP_CHART_NAME' => $item['SALARY_STEP_CHART_NAME'],
                'SALARY_STEP_START_SALARY' => $item['SALARY_STEP_START_SALARY'],
                'get_start_date' => $arrConfig['start_date'],
                'get_end_date' => $arrConfig['end_date'],
                'get_total_date' => $arrConfig['work_day'],
                'is_monthly' => 1,
                'cal_grt_amount' => $grt_money, //guarantee money
                'cal_bnf_amount'=> $bnf_money, //benefit fund
                'cal_sso_amount'=> $sso_money, //sso fund
                'cal_pnd_amount'=>$pnd_tax, //PND Tax
                'cal_wht_amount'=>$wht_tax, //Withoding Tax
                'cal_add_money' =>$add_money_total, //add money
                'cal_deduct_money' => $_total_deduct, //deduct money
                'cal_net_amount'=> $net_total,
                'cal_total_income' =>$_total_income,
                'pay_date'=>$arrConfig['pay_date'],
                'Be' => $item['Be']
            ];

            $arrList[] = $arrData;
        }

        return $arrList;
    }

    public static function buildInsertThisMonth($connection,$arrSalary,$data_adddeduct)
    {

        $arrCompany = ApiHr::getWorking_company();
        $arrDepartment = ApiHr::getDepartmentAll();
        $arrSection = ApiHr::getSectionAll();

        $transaction = $connection->beginTransaction();
        $eff = 0;
        try {

            //** Insert to Add Deduct This month */
            /* ------------------------------------------------------------- */
            /* บันทึกรายการคำนวณลงตาราง  ADD_DEDUCT_THIS_MONTH  Batch Insert   */
            /* ------------------------------------------------------------- */
            $i = 1;
            $_table = 'ADD_DEDUCT_THIS_MONTH';
            $_arrTransactionCol = Adddeductthismonth::getTableSchema()->getColumnNames();

            foreach ($data_adddeduct as $key => $value) {
                $arrData[] = [
                    $_arrTransactionCol[0] => NULL,  //ADD_DEDUCT_DETAIL_ID
                    $_arrTransactionCol[1] => $value['ADD_DEDUCT_ID'],  //ADD_DEDUCT_ID
                    $_arrTransactionCol[2] => $value['ADD_DEDUCT_DETAIL'],  //ADD_DEDUCT_DETAIL
                    $_arrTransactionCol[3] => $value['ADD_DEDUCT_DETAIL_PAY_DATE'],  //ADD_DEDUCT_DETAIL_PAY_DATE
                    $_arrTransactionCol[4] => $value['ADD_DEDUCT_DETAIL_START_USE_DATE'],  //ADD_DEDUCT_DETAIL_START_USE_DATE
                    $_arrTransactionCol[5] => $value['ADD_DEDUCT_DETAIL_END_USE_DATE'], //ADD_DEDUCT_DETAIL_END_USE_DATE
                    $_arrTransactionCol[6] => $value['INSTALLMENT_START'],  //INSTALLMENT_START
                    $_arrTransactionCol[7] => $value['INSTALLMENT_END'],  //INSTALLMENT_END
                    $_arrTransactionCol[8] => $value['INSTALLMENT_CURRENT'],  //INSTALLMENT_CURRENT
                    $_arrTransactionCol[9] => $value['ADD_DEDUCT_DETAIL_EMP_ID'],  //ADD_DEDUCT_DETAIL_EMP_ID
                    $_arrTransactionCol[10] => $value['ADD_DEDUCT_DETAIL_AMOUNT'],  //ADD_DEDUCT_DETAIL_AMOUNT
                    $_arrTransactionCol[11] => $value['ADD_DEDUCT_DETAIL_TYPE'],  //ADD_DEDUCT_DETAIL_TYPE
                    $_arrTransactionCol[12] =>  $value['ADD_DEDUCT_DETAIL_STATUS'],  //ADD_DEDUCT_DETAIL_STATUS
                    $_arrTransactionCol[13] =>  $value['ADD_DEDUCT_DETAIL_HISTORY_STATUS'],   //ADD_DEDUCT_DETAIL_HISTORY_STATUS
                    $_arrTransactionCol[14] =>  $value['ADD_DEDUCT_DETAIL_GROUP_STATUS'],   //ADD_DEDUCT_DETAIL_GROUP_STATUS
                    $_arrTransactionCol[15] =>  $value['ADD_DEDUCT_DETAIL_CREATE_DATE'],   //ADD_DEDUCT_DETAIL_CREATE_DATE
                    $_arrTransactionCol[16] =>  $value['ADD_DEDUCT_DETAIL_CREATE_BY'],   //ADD_DEDUCT_DETAIL_CREATE_BY
                    $_arrTransactionCol[17] =>  $value['ADD_DEDUCT_DETAIL_UPDATE_BY'],   //ADD_DEDUCT_DETAIL_UPDATE_BY
                    $_arrTransactionCol[18] =>  $value['ADD_DEDUCT_DETAIL_UPDATE_DATE'],   //ADD_DEDUCT_DETAIL_UPDATE_DATE
                    $_arrTransactionCol[19] =>  $value['ADD_DEDUCT_DETAIL_RESP_PERSON'],   //ADD_DEDUCT_DETAIL_RESP_PERSON
                    $_arrTransactionCol[20] =>  $value['dept_deduct_id'],   //dept_deduct_id
                    $_arrTransactionCol[21] =>  $value['dept_deduct_detail_id'],   //dept_deduct_detail_id

                ];
            }

            $eff += $connection->createCommand()->batchInsert($_table, $_arrTransactionCol, $arrData)->execute();


            //** Insert to wage This month */
            /* ------------------------------------------------------------- */
            /* บันทึกรายการคำนวณ SUMMARY ลงตาราง  WAGE_THIS_MONTH  Batch Insert   */
            /* ------------------------------------------------------------- */

            $i = 1;
            $arrInsertWageThismonth = [];
            foreach ($arrSalary as $key => $value) {



                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_ROW_NUM'] = $i;
                $arrInsertWageThismonth[$key]['WAGE_EMP_ID'] = $value['ID_Card'];//idcard
                $arrInsertWageThismonth[$key]['WAGE_FIRST_NAME'] = $value['first_name'];//first_name
                $arrInsertWageThismonth[$key]['WAGE_LAST_NAME'] = $value['last_name'];//last_name
                $arrInsertWageThismonth[$key]['WAGE_PAY_DATE'] = $value['pay_date'];//01-2017
                $arrInsertWageThismonth[$key]['WAGE_POSITION_CODE'] = $value['MainCode'];//OPD803301
                $arrInsertWageThismonth[$key]['WAGE_POSITION_NAME'] = $value['MainPosition'];//Developer
                $arrInsertWageThismonth[$key]['WAGE_SECTION_ID'] = $value['MainSection'];//274
                $arrInsertWageThismonth[$key]['WAGE_DEPARTMENT_ID'] = $value['MainDepartment'];//72
                $arrInsertWageThismonth[$key]['WAGE_WORKING_COMPANY'] = $value['MainWorking_Company'];//56
                $arrInsertWageThismonth[$key]['WAGE_BANK_NAME'] = $value['Salary_Bank'];//KTB บิ๊กซีเชียงราย
                $arrInsertWageThismonth[$key]['WAGE_ACCOUNT_NUMBER'] = $value['Salary_BankNo'];//5950
                $arrInsertWageThismonth[$key]['WAGE_GET_MONEY_TYPE'] = $value['SalaryViaBank'];//1
                $arrInsertWageThismonth[$key]['WAGE_SALARY_CHART'] = $value['EMP_SALARY_CHART'];//HlLv13
                $arrInsertWageThismonth[$key]['WAGE_SALARY_LEVEL'] = $value['EMP_SALARY_LEVEL'];//2
                $arrInsertWageThismonth[$key]['WAGE_SALARY_STEP'] = $value['EMP_SALARY_STEP'];//1
                $arrInsertWageThismonth[$key]['WAGE_SALARY'] = $value['EMP_SALARY_WAGE'];//10000.00
                $arrInsertWageThismonth[$key]['WAGE_SALARY_BY_CHART'] = $value['SALARY_STEP_START_SALARY'];//10000.00
                $arrInsertWageThismonth[$key]['WAGE_TOTAL_ADDITION'] = $value['cal_add_money'];//6000.00
                $arrInsertWageThismonth[$key]['WAGE_EARN_PLUS_ADD'] = ($value['EMP_SALARY_WAGE'] + $value['cal_add_money']);//
                //$arrInsertWageThismonth[$key]['Social_Salary'] = $value['cal_sso_amount'];//เงินประกันสังคม
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_TAX'] = 0;//เงินประกันสังคม
                $arrInsertWageThismonth[$key]['WAGE_TOTAL_DEDUCTION'] = $value['cal_deduct_money'];
                $arrInsertWageThismonth[$key]['WAGE_EARN_MINUS_DEDUCT'] = ($value['EMP_SALARY_WAGE'] + $value['cal_add_money']) - $value['cal_deduct_money'];
                $arrInsertWageThismonth[$key]['WAGE_NET_SALARY'] = $value['cal_net_amount'];
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_CONFIRM'] = 0;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_CONFIRM_BY'] = null;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_CONFIRM_DATE'] = null;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_EMPLOYEE_LOCK'] = 0;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_DIRECTOR_LOCK'] = 0;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_STATUS'] = 1;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_STATUS'] = null;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_DATE'] = null;
                $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_BY'] = null;
                $arrInsertWageThismonth[$key]['WAGE_REMARK'] = null;
                $arrInsertWageThismonth[$key]['WAGE_CREATE_DATE'] = new Expression('NOW()');
                $arrInsertWageThismonth[$key]['WAGE_CREATE_BY'] = Yii::$app->session->get('idcard');
                $arrInsertWageThismonth[$key]['WAGE_UPDATE_DATE'] = null;
                $arrInsertWageThismonth[$key]['WAGE_UPDATE_BY'] = null;
                $arrInsertWageThismonth[$key]['is_endofmonth'] = null;
                $arrInsertWageThismonth[$key]['cal_times'] = null;
                $arrInsertWageThismonth[$key]['cal_date'] = null;
                $arrInsertWageThismonth[$key]['wage_calculate_id'] = null;
                $arrInsertWageThismonth[$key]['COMPANY_NAME'] = $arrCompany[$value['POS_COMPANY_ID']]['name'];
                $arrInsertWageThismonth[$key]['DEPARTMENT_NAME'] = $arrDepartment[$value['POS_DEPARTMENT_ID']]['name'];
                $arrInsertWageThismonth[$key]['SECTION_NAME'] = $arrSection[$value['POS_SECTION_ID']]['name'];
                $arrInsertWageThismonth[$key]['WTH_AMOUNT'] = $value['cal_wht_amount'];
                $arrInsertWageThismonth[$key]['WTH_AMOUNT_TAX'] = $value['cal_wht_amount'];
                $arrInsertWageThismonth[$key]['BNF_AMOUNT'] = $value['cal_bnf_amount'];

                $arrInsertWageThismonth[$key]['PND_TOTAL_INCOME'] = $value['cal_total_income'];
                $arrInsertWageThismonth[$key]['PND_AMOUNT_REDUCE'] = 0; //TODO : calculate reduce
                $arrInsertWageThismonth[$key]['PND_AMOUNT_TAX'] = $value['cal_pnd_amount'];
                $arrInsertWageThismonth[$key]['SSO_AMOUNT'] = $value['cal_sso_amount'];

                $i++;
            }

            //  print_r($arrInsertWageThismonth);

            $eff += $connection->createCommand()
                ->batchInsert(
                    'WAGE_THIS_MONTH',[
                        'WAGE_THIS_MONTH_ROW_NUM',
                        'WAGE_EMP_ID',
                        'WAGE_FIRST_NAME',
                        'WAGE_LAST_NAME',
                        'WAGE_PAY_DATE',
                        'WAGE_POSITION_CODE',
                        'WAGE_POSITION_NAME',
                        'WAGE_SECTION_ID',
                        'WAGE_DEPARTMENT_ID',
                        'WAGE_WORKING_COMPANY',
                        'WAGE_BANK_NAME',
                        'WAGE_ACCOUNT_NUMBER',
                        'WAGE_GET_MONEY_TYPE',
                        'WAGE_SALARY_CHART',
                        'WAGE_SALARY_LEVEL',
                        'WAGE_SALARY_STEP',
                        'WAGE_SALARY',
                        'WAGE_SALARY_BY_CHART',
                        'WAGE_TOTAL_ADDITION',
                        'WAGE_EARN_PLUS_ADD',
                        'WAGE_THIS_MONTH_TAX',
                        'WAGE_TOTAL_DEDUCTION',
                        'WAGE_EARN_MINUS_DEDUCT',
                        'WAGE_NET_SALARY',
                        'WAGE_THIS_MONTH_CONFIRM',
                        'WAGE_THIS_MONTH_CONFIRM_BY',
                        'WAGE_THIS_MONTH_CONFIRM_DATE',
                        'WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                        'WAGE_THIS_MONTH_DIRECTOR_LOCK',
                        'WAGE_THIS_MONTH_STATUS',
                        'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS',
                        'WAGE_THIS_MONTH_BANK_CONFIRM_DATE',
                        'WAGE_THIS_MONTH_BANK_CONFIRM_BY',
                        'WAGE_REMARK',
                        'WAGE_CREATE_DATE',
                        'WAGE_CREATE_BY',
                        'WAGE_UPDATE_DATE',
                        'WAGE_UPDATE_BY',
                        'is_endofmonth',
                        'cal_times',
                        'cal_date',
                        'wage_calculate_id',
                        'COMPANY_NAME',
                        'DEPARTMENT_NAME',
                        'SECTION_NAME',
                        'WTH_AMOUNT',
                        'WTH_AMOUNT_TAX',
                        'BNF_AMOUNT',
                        'PND_TOTAL_INCOME',
                        'PND_AMOUNT_REDUCE',
                        'PND_AMOUNT_TAX',
                        'SSO_AMOUNT'
                ],
                    $arrInsertWageThismonth)
                ->execute();


            return $eff;
        }
        catch (\ErrorException $e) {
            $transaction->rollBack();
            throw new \Exception($e->getMessage());
        }
    }


    public static function getAllEmpSalaryInfo()
    {
        $arrCompanyPosition = Workingcompany::find()
            ->select(['working_company.id as company_id',
                'working_company.name as company_name',
                'position.PositionCode as position_code',
                'position.Name as position_name',
            ])
            ->from('working_company')
            ->innerJoin('position', 'working_company.id = position.WorkCompany')
            ->where('working_company.status = 1')
            ->asArray()
            ->all();

        $dataCompanyPosition = [];
        foreach ($arrCompanyPosition as $item) {
            $dataCompanyPosition[$item['position_code']] = $item;
        }

        $EmpSalary = Empsalary::find()->where([
            'status'=>1
        ])->asArray()->all();

        $arrList = [];
        foreach ($EmpSalary as $item) {
            if(isset($dataCompanyPosition[$item['EMP_SALARY_POSITION_CODE']])) {
                $itemData = [
                    'id_card'=>$item['EMP_SALARY_ID_CARD'],
                    'company_id'=>$dataCompanyPosition[$item['EMP_SALARY_POSITION_CODE']]['company_id'],
                    'company_name'=>$dataCompanyPosition[$item['EMP_SALARY_POSITION_CODE']]['company_name'],
                    'position_code'=>$dataCompanyPosition[$item['EMP_SALARY_POSITION_CODE']]['position_code'],
                    'position_name'=>$dataCompanyPosition[$item['EMP_SALARY_POSITION_CODE']]['position_name'],
                    'emp_salary_chart' => $item['EMP_SALARY_CHART'],
                    'emp_salary_wage' => $item['EMP_SALARY_WAGE'],
                    'emp_salary_chart' => $item['EMP_SALARY_CHART'],
                    'status_actived' => $item['status'],
                    'status_sso' => $item['statusSSO'],
                    'status_calculat' => $item['statusCalculate'],
                    'status_mainjob' => $item['statusMainJob'],
                    'start_effective_date' => $item['start_effective_date'],
                    'end_effective_date' => $item['end_effective_date'],
                    'id_ref_orgchart' => $item['id_ref_orgchart'],
                ];
                $arrList[$item['EMP_SALARY_ID_CARD']][$item['EMP_SALARY_POSITION_CODE']] = $itemData;
            }
        }

        return $arrList;
    }


    public static function buildWageSummary($connection,$pay_date,$arrSalary)
    {

       $arrCompanyPosition = Workingcompany::find()
           ->select(['working_company.id as company_id',
               'working_company.name as company_name',
               'position.PositionCode as position_code',
               'position.Name as position_name',
           ])
           ->from('working_company')
           ->innerJoin('position', 'working_company.id = position.WorkCompany')
           ->where('working_company.status = 1')
           ->asArray()
           ->all();
       $dataCompanyPosition = [];
       foreach ($arrCompanyPosition as $item) {
           $dataCompanyPosition[$item['position_code']] = $item;
       }


        $arrCirCleMonth = DateTime::findCircleMonth($pay_date);
        $_detail = 'เดือน '.DateTime::convertMonth($arrCirCleMonth['cur_month']);
        $year = $arrCirCleMonth['cur_year'];

        //load summary and build array
        $data = null;
        if($arrCirCleMonth['cur_month'] > 1) {
            $data = SummaryMoneyWage::find()->where([
                'pay_date' => $arrCirCleMonth['prv_date_pay']
            ])->orderBy('id ASC')->asArray()->all();
        }


        $arrList = [];
        if(is_array($data) || is_object($data)) {
            foreach ($data as $item) {
                $arrList[$item['emp_idcard']][$item['position_code']]['tax_total_amount'] = $item['tax_total_amount'];
                $arrList[$item['emp_idcard']][$item['position_code']]['sso_total_amount'] = $item['sso_total_amount'];
                $arrList[$item['emp_idcard']][$item['position_code']]['income_total_amount'] = $item['income_total_amount'];
                $arrList[$item['emp_idcard']][$item['position_code']]['wht_total_amount'] = $item['wht_total_amount'];
                $arrList[$item['emp_idcard']][$item['position_code']]['bnf_total_amount'] = $item['bnf_total_amount'];
            }
        }


        //loop from array salary all
        $arrDataItem = [];
        foreach ($arrSalary as $key => $value) {

            $idcard = $value['ID_Card'];
            $positioncode = $value['POSITION_CODE'];

            $company_id = $value['POS_COMPANY_ID'];
            //$company_id = $value['POS_COMPANY_ID'];


            $prv_total_tax = $arrList[$idcard][$positioncode]['tax_total_amount'];
            $prv_total_sso = $arrList[$idcard][$positioncode]['sso_total_amount'];
            $prv_total_income = $arrList[$idcard][$positioncode]['income_total_amount'];
            $prv_total_wht = $arrList[$idcard][$positioncode]['wht_total_amount'];
            $prv_total_bnf = $arrList[$idcard][$positioncode]['bnf_total_amount'];

            $_income = ($value['EMP_SALARY_WAGE']+$value['cal_add_money']);
            $_total_income = $_income + $prv_total_income;
            $_total_tax = $value['cal_pnd_amount'] + $prv_total_tax;
            $_total_sso = $value['cal_sso_amount'] + $prv_total_sso;
            $_total_wht = $value['cal_wht_amount'] + $prv_total_wht;

            $_bnf_amount  = $value['cal_bnf_amount'];
            $_bnf_total_amount = $_bnf_amount + $prv_total_bnf;

            $arrDataItem[$key]['emp_idcard'] = $idcard;
            $arrDataItem[$key]['company_id'] = $company_id;
            $arrDataItem[$key]['company_name'] = $dataCompanyPosition[$positioncode]['company_name'];
            $arrDataItem[$key]['position_code'] = $positioncode;
            $arrDataItem[$key]['position_name'] = $dataCompanyPosition[$positioncode]['position_name'];;
            $arrDataItem[$key]['pay_date'] = $pay_date;
            $arrDataItem[$key]['pay_year']= $year;
            $arrDataItem[$key]['income_date']= date('Y-m-t');
            $arrDataItem[$key]['income_detail']= '';
            $arrDataItem[$key]['income_amount']= $_income;
            $arrDataItem[$key]['income_total_amount']= $_total_income;
            $arrDataItem[$key]['tax_amount']= $value['cal_pnd_amount'];
            $arrDataItem[$key]['tax_total_amount']= $_total_tax;
            $arrDataItem[$key]['sso_amount']= $value['cal_sso_amount'];
            $arrDataItem[$key]['sso_total_amount']= $_total_sso;
            $arrDataItem[$key]['wht_amount']= $value['cal_wht_amount'];
            $arrDataItem[$key]['wht_total_amount']= $_total_wht;

            $arrDataItem[$key]['bnf_amount']= $_bnf_amount;
            $arrDataItem[$key]['bnf_total_amount']= $_bnf_total_amount;

            $arrDataItem[$key]['transaction_by']= Yii::$app->session->get('idcard');
            $arrDataItem[$key]['transaction_datetime']= new Expression('NOW()');
            $arrDataItem[$key]['status_active']= 1;
            $arrDataItem[$key]['end_effective_date']= $value['End_date'];
            $arrDataItem[$key]['remark']= null;
        }

        $eff = $connection->createCommand()
            ->batchInsert('summary_money_wage',[
                'emp_idcard',
                'company_id',
                'company_name',
                'position_code',
                'position_name',
                'pay_date',
                'pay_year',
                'income_date',
                'income_detail',
                'income_amount',
                'income_total_amount',
                'tax_amount',
                'tax_total_amount',
                'sso_amount',
                'sso_total_amount',
                'wht_amount',
                'wht_total_amount',
                'bnf_amount',
                'bnf_total_amount',
                'transaction_by',
                'transaction_datetime',
                'status_active',
                'end_effective_date',
                'remark'
            ],$arrDataItem)->execute();

    }


    public static function buildBenefitFund($connection,$pay_date,$arrSalary)
    {
        $arrCirCleMonth = DateTime::findCircleMonth($pay_date);
        $_detail = 'เดือน '.DateTime::convertMonth($arrCirCleMonth['cur_month']);

        //load summary and build array
        $data = Benefitfund::find()->where([
            'BENEFIT_SAVING_DATE'=>$arrCirCleMonth['prv_date_pay']
        ])->orderBy('BENEFIT_ID ASC')->asArray()->all();


        $arrList = [];
        if(is_array($data) || is_object($data)) {
            foreach ($data as $item) {
                $arrList[$item['BENEFIT_EMP_ID']] = $item['BENEFIT_TOTAL_AMOUNT'];
            }
        }

        $arrDataItem = [];
        foreach ($arrSalary as $key => $value) {

            $idcard = $value['ID_Card'];
            $bnf_thismonth = $value['cal_bnf_amount'];
            $old_bnf =  (isset($arrList[$idcard])) ? $arrList[$idcard] : 0;
            $balance_bnf = $old_bnf + $bnf_thismonth;

            $arrDataItem[$key]['BENEFIT_EMP_ID'] = $idcard;
            $arrDataItem[$key]['BENEFIT_SAVING_DATE'] = $pay_date;
            $arrDataItem[$key]['BENEFIT_DETAIL'] = $_detail;
            $arrDataItem[$key]['BENEFIT_AMOUNT'] = $bnf_thismonth;
            $arrDataItem[$key]['BENEFIT_TOTAL_AMOUNT'] = $balance_bnf;
            $arrDataItem[$key]['BENEFIT_TRANSACTION_BY'] =  Yii::$app->session->get('idcard');
            $arrDataItem[$key]['BENEFIT_TRANSACTION_DATE'] = new Expression('NOW()');
            $arrDataItem[$key]['BENEFIT_FUND_STATUS'] = '1';
            $arrDataItem[$key]['BENEFIT_FUND_REMARK'] ='';
        }

        return $connection->createCommand()
            ->batchInsert('BENEFIT_FUND',[
                'BENEFIT_EMP_ID',
                'BENEFIT_SAVING_DATE',
                'BENEFIT_DETAIL',
                'BENEFIT_AMOUNT',
                'BENEFIT_TOTAL_AMOUNT',
                'BENEFIT_TRANSACTION_BY',
                'BENEFIT_TRANSACTION_DATE',
                'BENEFIT_FUND_STATUS',
                'BENEFIT_FUND_REMARK'
            ],$arrDataItem)->execute();
    }


    //Build item data to add deduct detail
    public static function buildSSOdata($connection,$pay_date,$arrSalary,$SSO_Template)
    {
        $arrItemList = [];
        $arrAllCompany = $arrAllEmp =  [];

        $SSO_DATE = DateTime::makeDayforSSO($pay_date);
        $dtMY = explode('-',$pay_date);


        foreach ($arrSalary as $key => $value) {

            /** ---------------------------------------------------------------------------
             *  ค้นหาพนักงานที่มีการจ่ายประกันสังคม เพื่อนำไปสร้างรายการ
             *----------------------------------------------------------------------------*/
            if($value['is_cal_sso'] ==1 && $value['cal_sso_amount'] > 0) {
                $arrItemList[$key]['idcard'] = $value['ID_Card'];
                $arrItemList[$key]['item_amount'] = $value['cal_sso_amount'];

                //Build Items for table Head_Record & Detail_Record
                $arrAllCompany[$value['POS_COMPANY_ID']] = [
                    'COMPANY_ID'=>$value['POS_COMPANY_ID'],
                    'RATE'=>$value['socialBenifitPercent'],
                    'YEAR' => (int)$dtMY[1],
                    'MONTH' => (int)$dtMY[0],
                    'PAID_DATE' => $SSO_DATE['PAID_DATE'],
                    'PAID_PERIOD' => $SSO_DATE['PAID_PERIOD'],
                ];

                $data_items = [
                    'SSO_ID'=>$value['ID_Card'],
                    'PREFIX' => ApiSalary::map_sso_prefix($value['Be']),
                    'FNAME' => $value['first_name'],
                    'LNAME' => $value['last_name'],
                    'WAGES' => $value['EMP_SALARY_WAGE'],
                    'PAID_AMOUNT' => $value['cal_sso_amount'],
                    'BLANK'=> '',
                    'COMPANY_ID'=>$value['POS_COMPANY_ID']
                ];

                $arrAllEmp[$value['POS_COMPANY_ID']][$value['ID_Card']] = $data_items;
            }
        }


        //ค้นหารายการของพนักงานในบริษัท
        $arrCompanySum = [];
        foreach ($arrAllCompany as $CompanyID => $item) {
            if(isset($arrAllEmp[$CompanyID])) {
                foreach ($arrAllEmp[$CompanyID] as $IDCard => $emp) {
                    $arrCompanySum[$CompanyID]['SUM_PAID_AMOUNT'] += $emp['PAID_AMOUNT'];
                    $arrCompanySum[$CompanyID]['SUM_WAGES'] += $emp['WAGES'];
                    $arrCompanySum[$CompanyID]['SUM_EMPLOYER_PAID'] += $emp['PAID_AMOUNT'];
                    $arrCompanySum[$CompanyID]['SUM_EMPLOYEE'] += 1;
                    $arrCompanySum[$CompanyID]['RATE'] = $item['RATE'];

                }
            }
        }


        //insert to Header_Record & Detail_Record
        ApiSSOExport::InsertDataSSO135($connection,$arrAllCompany,$arrCompanySum,$arrAllEmp,$pay_date);

        return ApiSalary::AdjustAddDeductDetail($connection,$pay_date,$arrItemList,$SSO_Template);

    }


    public static function listDataSalaryEmpNormal($datemakesalary)
    {
        $time = strtotime($datemakesalary);
        $final = date("Y-m-d", strtotime("-1 month", $time));
        //$dateStartCaluculate = date('', strtotime("-1 month"));
        //echo $dateStartCaluculate;

        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];
//        $model = Empdata::find()
//            ->select(['emp_data.Code as Code',
//                'emp_data.Position as Position',
//                'emp_data.Salary_Bank as Salary_Bank',
//                'emp_data.Salary_BankNo as Salary_BankNo',
//                'emp_data.SalaryViaBank as SalaryViaBank',
//                'EMP_SALARY.EMP_SALARY_CHART as EMP_SALARY_CHART',
//                'EMP_SALARY.EMP_SALARY_STEP as EMP_SALARY_STEP',
//                'EMP_SALARY.EMP_SALARY_LEVEL as EMP_SALARY_LEVEL',
//                'emp_data.ID_Card as empIdCard',
//                'EMP_SALARY.EMP_SALARY_WAGE as EMP_SALARY_WAGE',
//                'EMP_SALARY.EMP_SALARY_CHART_ID as EMP_SALARY_CHART_ID',
//                'emp_data.socialBenifitPercent as socialBenifitPercent',
//                'emp_data.Working_Company as Working_Company',
//                'emp_data.Department as Department',
//                'emp_data.Section as Section',
//                'emp_data.socialBenefitStatus as socialBenefitStatus',
//                'emp_data.benefitFundPercent as benefitFundPercent',
//                'emp_data.benifitFundStatus as benifitFundStatus'])
//            ->from('emp_data')
//            ->innerJoin('EMP_SALARY', 'EMP_SALARY.EMP_SALARY_ID_CARD = emp_data.ID_Card')
//            ->where('emp_data.Prosonnal_Being IN (1,2)
//                            AND  emp_data.username != "#####Out Off###"
//                            AND emp_data.to_salary_status = "1"
//                            AND emp_data.Start_date <= :dateStartCaluculate')
//            ->addParams([':dateStartCaluculate' => $dateStartCaluculate,])
//            ->asArray()
//            ->all();

//       print_r($model);


        $sql = "SELECT $db[ct].emp_data.Code as Code,
                $db[ct].emp_data.Position as Position,
                $db[ct].emp_data.Salary_Bank as Salary_Bank,
                $db[ct].emp_data.Salary_BankNo as Salary_BankNo,
                $db[ct].emp_data.SalaryViaBank as SalaryViaBank,
                $db[pl].EMP_SALARY.EMP_SALARY_CHART as EMP_SALARY_CHART,
                $db[pl].EMP_SALARY.EMP_SALARY_STEP as EMP_SALARY_STEP,
                $db[pl].EMP_SALARY.EMP_SALARY_LEVEL as EMP_SALARY_LEVEL,
                $db[ct].emp_data.ID_Card as empIdCard,
                $db[pl].EMP_SALARY.EMP_SALARY_WAGE as EMP_SALARY_WAGE,
                $db[pl].EMP_SALARY.EMP_SALARY_CHART_ID as EMP_SALARY_CHART_ID,
                $db[ct].emp_data.socialBenifitPercent as socialBenifitPercent,
                $db[ct].emp_data.Working_Company as Working_Company,
                $db[ct].emp_data.Department as Department,
                $db[ct].emp_data.Section as Section,
                $db[ct].emp_data.socialBenefitStatus as socialBenefitStatus,
                $db[ct].emp_data.benefitFundPercent as benefitFundPercent,
                $db[ct].emp_data.benifitFundStatus as benifitFundStatus
                FROM $db[ct].emp_data
                INNER JOIN $db[pl].EMP_SALARY ON $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD =  $db[ct].emp_data.ID_Card
                WHERE $db[ct].emp_data.Prosonnal_Being IN (1,2)
                AND $db[ct].emp_data.username != '#####Out Off###'
                AND $db[ct].emp_data.to_salary_status = '1'
                AND $db[ct].emp_data.Start_date <= '$final'
                ORDER BY Working_Company,empIdCard,Code ASC
                ";


        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();
        return $data;
////        return $model;
    }


    public static function CalculateSalary($arrIDCard, $MonthYear)
    {

//        try {
        $ADD_DEDUCT_DETAIL_EMP_ID = [];
        foreach ($arrIDCard as $value) {
            $ADD_DEDUCT_DETAIL_EMP_ID[] = $value['empIdCard'];
        }

        $lastDateNow = date('Y-m-t');
        $resultYear = DateTime::selectMonthHaveZero($MonthYear);
        $resultFormatMonthYear = date($resultYear['monthresult'] . "-" . $resultYear['year']);
        $ID_CRAD_TRANSACTION_BY = Yii::$app->session->get('idcard');


        ////////////////////////////////// เริ่มส่วนเพิ่ม //////////////////////////////////

        $modelAddEveryMonthNoDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'SUM(ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT) as SUM_ADD_DEDUCT_DETAIL_AMOUNT'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE <= "0000-00-00"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE = "0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->groupBy('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID')
            ->asArray()
            ->all();

        $modelAddEveryMonthHaveDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'SUM(ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT) as SUM_ADD_DEDUCT_DETAIL_AMOUNT'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE >= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE <= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE !="0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->groupBy('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID')
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();


        $sumResultAddEveryMonthNoDateStart = $sumResultAddEveryMonthHaveDateStart = $arrResultAddLast = [];
        foreach ($modelAddEveryMonthNoDateStart as $key => $value) {
            $sumResultAddEveryMonthNoDateStart[$value['ADD_DEDUCT_DETAIL_EMP_ID']] = $value['SUM_ADD_DEDUCT_DETAIL_AMOUNT'];
        }
        // print_r($sumResultAddEveryMonthNoDateStart);

        foreach ($modelAddEveryMonthHaveDateStart as $key => $value) {
            $sumResultAddEveryMonthHaveDateStart[$value['ADD_DEDUCT_DETAIL_EMP_ID']] = $value['SUM_ADD_DEDUCT_DETAIL_AMOUNT'];
        }
        // print_r($sumResultAddEveryMonthHaveDateStart);


        $mergeNoDateHaveDate = ApiPayroll::mergeDeduct($sumResultAddEveryMonthNoDateStart, $sumResultAddEveryMonthHaveDateStart);
        foreach ($mergeNoDateHaveDate as $value) {
            foreach ($value as $key => $values) {
                $arrResultAddLast[$key] = array_sum($values);
            }
        }


        $modelAddThisMonth = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'SUM(ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT) as SUM_ADD_DEDUCT_DETAIL_AMOUNT'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE = :lastDateNow
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->groupBy('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID')
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();

        // print_r($modelAddThisMonth);

        $resultTotalThismonth = [];
        foreach ($modelAddThisMonth as $value) {
            $resultTotalThismonth[$value['ADD_DEDUCT_DETAIL_EMP_ID']] = $value['SUM_ADD_DEDUCT_DETAIL_AMOUNT'];
        }
        //  print_r($resultTotalThismonth);


        $sumLatsTotalResultAdd = [];
        $mergeArrAddthismonthAll = ApiPayroll::mergeDeduct($arrResultAddLast, $resultTotalThismonth);
        foreach ($mergeArrAddthismonthAll as $value) {
            foreach ($value as $key => $values) {
                $sumLatsTotalResultAdd[$key] = array_sum($values);
            }
        }

        // print_r($sumLatsTotalResultAdd);


        ////////////////////////////////// สิ้นสุดส่วนลด //////////////////////////////////
        ////////////////////////////////// เริ่มส่วนลด //////////////////////////////////

        $modelDeEveryMonthNoDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'SUM(ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT) as SUM_ADD_DEDUCT_DETAIL_AMOUNT'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE <= "0000-00-00"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE = "0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->groupBy('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID')
            ->asArray()
            ->all();
        // print_r($modelDeEveryMonthNoDateStart);


        $modelDeEveryMonthHaveDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'SUM(ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT) as SUM_ADD_DEDUCT_DETAIL_AMOUNT'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE >= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE <= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE !="0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->groupBy('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID')
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();


        $sumResultDeEveryMonthNoDateStart = $sumResultDeEveryMonthHaveDateStart = $arrResultDeLast = [];

        foreach ($modelDeEveryMonthNoDateStart as $key => $value) {
            $sumResultDeEveryMonthNoDateStart[$value['ADD_DEDUCT_DETAIL_EMP_ID']] = $value['SUM_ADD_DEDUCT_DETAIL_AMOUNT'];
        }
        // print_r($sumResultDeEveryMonthNoDateStart);

        foreach ($modelDeEveryMonthHaveDateStart as $key => $value) {
            $sumResultDeEveryMonthHaveDateStart[$value['ADD_DEDUCT_DETAIL_EMP_ID']] = $value['SUM_ADD_DEDUCT_DETAIL_AMOUNT'];
        }

        $mergeDeNoDateHaveDate = ApiPayroll::mergeDeduct($sumResultDeEveryMonthNoDateStart, $sumResultDeEveryMonthHaveDateStart);
        foreach ($mergeDeNoDateHaveDate as $value) {
            foreach ($value as $key => $values) {
                $arrResultDeLast[$key] = array_sum($values);
            }
        }


        // print_r($arrResultDeLast);

        $modelDeThisMonth = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'SUM(ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT) as SUM_ADD_DEDUCT_DETAIL_AMOUNT'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE = :lastDateNow
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->groupBy('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID')
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();

        $resultDeTotalThismonth = [];
        foreach ($modelDeThisMonth as $value) {
            $resultDeTotalThismonth[$value['ADD_DEDUCT_DETAIL_EMP_ID']] = $value['SUM_ADD_DEDUCT_DETAIL_AMOUNT'];
        }

        $sumLatsTotalResultDe = [];
        $mergeArrDethismonthAll = ApiPayroll::mergeDeduct($arrResultDeLast, $resultDeTotalThismonth);
        foreach ($mergeArrDethismonthAll as $value) {
            foreach ($value as $key => $values) {
                $sumLatsTotalResultDe[$key] = array_sum($values);
            }
        }

        ////////////////////////////////// สิ้นสุดส่วนลด //////////////////////////////////

        ///////////////////////////////// เริ่มinsertลงbase  Wage this month/////////////////////////////////
        // print_r($sumLatsTotalResultAdd);        ///ส่วนเพิ่ม
        // print_r($sumLatsTotalResultDe);         ///ส่วนลบ
        $modelListSSOThismonth = ApiSSO::ListSSOThisMonth($arrIDCard, $MonthYear);
        ///ApiBenfit
        // print_r($modelListSSOThismonth);
        $valueListSSO = [];
        foreach ($modelListSSOThismonth as $value) {
            $valueListSSO[$value['SOCIAL_SALARY_EMP_ID']] = $value['SOCIAL_SALARY_AMOUNT'];
        }


        // print_r($valueListSSO);
        // print_r($arrIDCard);
        $Chart_ID = [];
        foreach ($arrIDCard as $value) {
            $Chart_ID[] = $value['EMP_SALARY_CHART_ID'];
        }
        $Id_For_Chart = array_unique($Chart_ID);

        $modelChartSalaryStep = Salarystep::find()
            ->where(['IN', 'SALARY_STEP_ID', $Id_For_Chart])
            ->asArray()
            ->all();


        $resultChartSalary = [];
        foreach ($modelChartSalaryStep as $value) {
            $resultChartSalary[$value['SALARY_STEP_ID']] = $value['SALARY_STEP_START_SALARY'];
        }


        $arrInsertWageThismonth = [];
        $i = 1;
        foreach ($arrIDCard as $key => $value) {
            $EMP_SALARY_CHART_ID = $value['EMP_SALARY_CHART_ID'];
            if (array_key_exists($EMP_SALARY_CHART_ID, $resultChartSalary)) {
                $valueChartSalary = $resultChartSalary[$EMP_SALARY_CHART_ID];
            } else {
                $valueChartSalary = 0;
            }

            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_ROW_NUM'] = $i;
            $arrInsertWageThismonth[$key]['WAGE_EMP_ID'] = $value['empIdCard'];//idcard
            $arrInsertWageThismonth[$key]['WAGE_PAY_DATE'] = $MonthYear;//01-2017
            $arrInsertWageThismonth[$key]['WAGE_POSITION_CODE'] = $value['Code'];//OPD803301
            $arrInsertWageThismonth[$key]['WAGE_POSITION_NAME'] = $value['Position'];//Developer
            $arrInsertWageThismonth[$key]['WAGE_SECTION_ID'] = $value['Section'];//274
            $arrInsertWageThismonth[$key]['WAGE_DEPARTMENT_ID'] = $value['Department'];//72
            $arrInsertWageThismonth[$key]['WAGE_WORKING_COMPANY'] = $value['Working_Company'];//56
            $arrInsertWageThismonth[$key]['WAGE_BANK_NAME'] = $value['Salary_Bank'];//KTB บิ๊กซีเชียงราย
            $arrInsertWageThismonth[$key]['WAGE_ACCOUNT_NUMBER'] = $value['Salary_BankNo'];//5950
            $arrInsertWageThismonth[$key]['WAGE_GET_MONEY_TYPE'] = $value['SalaryViaBank'];//1
            $arrInsertWageThismonth[$key]['WAGE_SALARY_CHART'] = $value['EMP_SALARY_CHART'];//HlLv13
            $arrInsertWageThismonth[$key]['WAGE_SALARY_LEVEL'] = $value['EMP_SALARY_LEVEL'];//2
            $arrInsertWageThismonth[$key]['WAGE_SALARY_STEP'] = $value['EMP_SALARY_STEP'];//1
            $arrInsertWageThismonth[$key]['WAGE_SALARY'] = $value['EMP_SALARY_WAGE'];//10000.00
            $arrInsertWageThismonth[$key]['WAGE_SALARY_BY_CHART'] = $valueChartSalary;//10000.00
            $arrInsertWageThismonth[$key]['WAGE_TOTAL_ADDITION'] = $sumLatsTotalResultAdd[$value['empIdCard']];//6000.00
            $arrInsertWageThismonth[$key]['WAGE_EARN_PLUS_ADD'] = $value['EMP_SALARY_WAGE'] + $sumLatsTotalResultAdd[$value['empIdCard']];//
            $arrInsertWageThismonth[$key]['Social_Salary'] = $valueListSSO[$value['empIdCard']];//เงินประกันสังคม
            $arrInsertWageThismonth[$key]['WAGE_TOTAL_DEDUCTION'] = $sumLatsTotalResultDe[$value['empIdCard']];
            $arrInsertWageThismonth[$key]['WAGE_EARN_MINUS_DEDUCT'] = $value['EMP_SALARY_WAGE'] + $sumLatsTotalResultAdd[$value['empIdCard']] - $sumLatsTotalResultDe[$value['empIdCard']] - $valueListSSO[$value['empIdCard']];
            $arrInsertWageThismonth[$key]['WAGE_NET_SALARY'] = $value['EMP_SALARY_WAGE'] + $sumLatsTotalResultAdd[$value['empIdCard']] - $sumLatsTotalResultDe[$value['empIdCard']] - $valueListSSO[$value['empIdCard']];
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_CONFIRM'] = 0;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_CONFIRM_BY'] = null;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_CONFIRM_DATE'] = null;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_EMPLOYEE_LOCK'] = 1;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_DIRECTOR_LOCK'] = 1;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_STATUS'] = 1;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_STATUS'] = null;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_DATE'] = null;
            $arrInsertWageThismonth[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_BY'] = null;
            $arrInsertWageThismonth[$key]['WAGE_REMARK'] = null;
            $arrInsertWageThismonth[$key]['WAGE_CREATE_DATE'] = new Expression('NOW()');
            $arrInsertWageThismonth[$key]['WAGE_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrInsertWageThismonth[$key]['WAGE_UPDATE_DATE'] = null;
            $arrInsertWageThismonth[$key]['WAGE_UPDATE_BY'] = null;
            $i++;

        }

        //  print_r($arrInsertWageThismonth);

        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('WAGE_THIS_MONTH', ['WAGE_THIS_MONTH_ROW_NUM',
                'WAGE_EMP_ID',
                'WAGE_PAY_DATE',
                'WAGE_POSITION_CODE',
                'WAGE_POSITION_NAME',
                'WAGE_SECTION_ID',
                'WAGE_DEPARTMENT_ID',
                'WAGE_WORKING_COMPANY',
                'WAGE_BANK_NAME',
                'WAGE_ACCOUNT_NUMBER',
                'WAGE_GET_MONEY_TYPE',
                'WAGE_SALARY_CHART',
                'WAGE_SALARY_LEVEL',
                'WAGE_SALARY_STEP',
                'WAGE_SALARY',
                'WAGE_SALARY_BY_CHART',
                'WAGE_TOTAL_ADDITION',
                'WAGE_EARN_PLUS_ADD',
                'Social_Salary',
                'WAGE_TOTAL_DEDUCTION',
                'WAGE_EARN_MINUS_DEDUCT',
                'WAGE_NET_SALARY',
                'WAGE_THIS_MONTH_CONFIRM',
                'WAGE_THIS_MONTH_CONFIRM_BY',
                'WAGE_THIS_MONTH_CONFIRM_DATE',
                'WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                'WAGE_THIS_MONTH_DIRECTOR_LOCK',
                'WAGE_THIS_MONTH_STATUS',
                'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS',
                'WAGE_THIS_MONTH_BANK_CONFIRM_DATE',
                'WAGE_THIS_MONTH_BANK_CONFIRM_BY',
                'WAGE_REMARK',
                'WAGE_CREATE_DATE',
                'WAGE_CREATE_BY',
                'WAGE_UPDATE_DATE',
                'WAGE_UPDATE_BY'],
                $arrInsertWageThismonth)
            ->execute();


        $modelWageThisMonth = Wagethismonth::find()
            ->asArray()
            ->all();

        // print_r($modelWageThisMonth);

        $arrWageThisMonth = [];
        foreach ($modelWageThisMonth as $value) {
            $arrWageThisMonth[$value['WAGE_EMP_ID']] = $value['WAGE_ID'];
        }
        //  print_r($arrWageThisMonth);
        ////////////////////////////////// รายละเอียดเริ่มส่วนเพิ่ม //////////////////////////////////

        $modelDetailAddAllNoDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID as ADD_DEDUCT_TEMPLATE_ID',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE as ADD_DEDUCT_TEMPLATE_TYPE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS as ADD_DEDUCT_DETAIL_STATUS'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE <= "0000-00-00"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE = "0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->asArray()
            ->all();

        // print_r($modelDetailAddAllNoDateStart);
        $arrModelDetailAddAllNoDateStart = [];
        foreach ($modelDetailAddAllNoDateStart as $key => $value) {
            $ID_CRAD = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            if (array_key_exists($ID_CRAD, $arrWageThisMonth)) {
                $resultWageID = $arrWageThisMonth[$ID_CRAD];
            }
            $arrModelDetailAddAllNoDateStart[$key]['WAGE_THIS_MONTH_ID'] = $resultWageID;
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_EMP_ID'] = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_PAY_DATE'] = $MonthYear;
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_ID'] = $value['ADD_DEDUCT_TEMPLATE_ID'];
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] = $value['ADD_DEDUCT_TEMPLATE_NAME'];
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL_ID'] = $value['ADD_DEDUCT_DETAIL_ID'];
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL'] = $value['ADD_DEDUCT_DETAIL'];
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_AMOUNT'] = $value['ADD_DEDUCT_DETAIL_AMOUNT'];
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TYPE'] = $value['ADD_DEDUCT_TEMPLATE_TYPE'];
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_STATUS'] = '1';
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'] = 1;
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_DATE'] = new Expression('NOW()');
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'] = null;
            $arrModelDetailAddAllNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_BY'] = null;
        }
        // print_r($arrModelDetailAddAllNoDateStart);
        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('ADD_DEDUCT_THIS_MONTH', ['WAGE_THIS_MONTH_ID',
                'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                'ADD_DEDUCT_THIS_MONTH_DETAIL',
                'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                'ADD_DEDUCT_THIS_MONTH_TYPE',
                'ADD_DEDUCT_THIS_MONTH_STATUS',
                'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'],
                $arrModelDetailAddAllNoDateStart)
            ->execute();


        $modelDetailAddAllHaveDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID as ADD_DEDUCT_TEMPLATE_ID',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE as ADD_DEDUCT_TEMPLATE_TYPE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS as ADD_DEDUCT_DETAIL_STATUS'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE >= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE <= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE !="0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();
        //print_r($modelDetailAddAllHaveDateStart);
        $arrModelDetailAddAllHaveDateStart = [];
        foreach ($modelDetailAddAllHaveDateStart as $key => $value) {
            $ID_CRAD = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            if (array_key_exists($ID_CRAD, $arrWageThisMonth)) {
                $resultWageID = $arrWageThisMonth[$ID_CRAD];
            }
            $arrModelDetailAddAllHaveDateStart[$key]['WAGE_THIS_MONTH_ID'] = $resultWageID;
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_EMP_ID'] = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_PAY_DATE'] = $MonthYear;
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_ID'] = $value['ADD_DEDUCT_TEMPLATE_ID'];
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] = $value['ADD_DEDUCT_TEMPLATE_NAME'];
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL_ID'] = $value['ADD_DEDUCT_DETAIL_ID'];
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL'] = $value['ADD_DEDUCT_DETAIL'];
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_AMOUNT'] = $value['ADD_DEDUCT_DETAIL_AMOUNT'];
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TYPE'] = $value['ADD_DEDUCT_TEMPLATE_TYPE'];
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_STATUS'] = '1';
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'] = 1;
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_DATE'] = new Expression('NOW()');
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'] = null;
            $arrModelDetailAddAllHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_BY'] = null;
        }
        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('ADD_DEDUCT_THIS_MONTH', ['WAGE_THIS_MONTH_ID',
                'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                'ADD_DEDUCT_THIS_MONTH_DETAIL',
                'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                'ADD_DEDUCT_THIS_MONTH_TYPE',
                'ADD_DEDUCT_THIS_MONTH_STATUS',
                'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'],
                $arrModelDetailAddAllHaveDateStart)
            ->execute();
        //  print_r($arrModelDetailAddAllHaveDateStart);


        $modelDetailAddThisMonth = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID as ADD_DEDUCT_TEMPLATE_ID',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE as ADD_DEDUCT_TEMPLATE_TYPE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS as ADD_DEDUCT_DETAIL_STATUS'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE = :lastDateNow
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();
        //print_r($modelDetailAddThisMonth);
        $arrModelDetailAddThisMonth = [];
        foreach ($modelDetailAddThisMonth as $key => $value) {
            $ID_CRAD = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            if (array_key_exists($ID_CRAD, $arrWageThisMonth)) {
                $resultWageID = $arrWageThisMonth[$ID_CRAD];
            }
            $arrModelDetailAddThisMonth[$key]['WAGE_THIS_MONTH_ID'] = $resultWageID;
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_EMP_ID'] = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_PAY_DATE'] = $MonthYear;
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_TMP_ID'] = $value['ADD_DEDUCT_TEMPLATE_ID'];
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] = $value['ADD_DEDUCT_TEMPLATE_NAME'];
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL_ID'] = $value['ADD_DEDUCT_DETAIL_ID'];
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL'] = $value['ADD_DEDUCT_DETAIL'];
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_AMOUNT'] = $value['ADD_DEDUCT_DETAIL_AMOUNT'];
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_TYPE'] = $value['ADD_DEDUCT_TEMPLATE_TYPE'];
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_STATUS'] = '1';
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'] = 1;
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_DATE'] = new Expression('NOW()');
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'] = null;
            $arrModelDetailAddThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_BY'] = null;
        }
        //print_r($arrModelDetailAddThisMonth);
        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('ADD_DEDUCT_THIS_MONTH', ['WAGE_THIS_MONTH_ID',
                'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                'ADD_DEDUCT_THIS_MONTH_DETAIL',
                'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                'ADD_DEDUCT_THIS_MONTH_TYPE',
                'ADD_DEDUCT_THIS_MONTH_STATUS',
                'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'],
                $arrModelDetailAddThisMonth)
            ->execute();


        //         ////////////////////////////////// สิ้นสุดส่วนลด //////////////////////////////////
        //         ////////////////////////////////// รายละเอียดเริ่มส่วนลด //////////////////////////////////

        $modelDetailDeAllMonthNoDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID as ADD_DEDUCT_TEMPLATE_ID',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE as ADD_DEDUCT_TEMPLATE_TYPE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS as ADD_DEDUCT_DETAIL_STATUS'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE <= "0000-00-00"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE = "0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->asArray()
            ->all();
        // print_r($modelDetailDeAllMonthNoDateStart);
        $arrModelDetailDeAllMonthNoDateStart = [];
        foreach ($modelDetailDeAllMonthNoDateStart as $key => $value) {
            $ID_CRAD = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            if (array_key_exists($ID_CRAD, $arrWageThisMonth)) {
                $resultWageID = $arrWageThisMonth[$ID_CRAD];
            }
            $arrModelDetailDeAllMonthNoDateStart[$key]['WAGE_THIS_MONTH_ID'] = $resultWageID;
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_EMP_ID'] = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_PAY_DATE'] = $MonthYear;
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_ID'] = $value['ADD_DEDUCT_TEMPLATE_ID'];
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] = $value['ADD_DEDUCT_TEMPLATE_NAME'];
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL_ID'] = $value['ADD_DEDUCT_DETAIL_ID'];
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL'] = $value['ADD_DEDUCT_DETAIL'];
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_AMOUNT'] = $value['ADD_DEDUCT_DETAIL_AMOUNT'];
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TYPE'] = $value['ADD_DEDUCT_TEMPLATE_TYPE'];
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_STATUS'] = '1';
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'] = 1;
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_DATE'] = new Expression('NOW()');
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'] = null;
            $arrModelDetailDeAllMonthNoDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_BY'] = null;
        }
        // print_r($arrModelDetailDeAllMonthNoDateStart);
        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('ADD_DEDUCT_THIS_MONTH', ['WAGE_THIS_MONTH_ID',
                'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                'ADD_DEDUCT_THIS_MONTH_DETAIL',
                'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                'ADD_DEDUCT_THIS_MONTH_TYPE',
                'ADD_DEDUCT_THIS_MONTH_STATUS',
                'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'],
                $arrModelDetailDeAllMonthNoDateStart)
            ->execute();

        $modelDetailDeAllMonthHaveDateStart = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID as ADD_DEDUCT_TEMPLATE_ID',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE as ADD_DEDUCT_TEMPLATE_TYPE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS as ADD_DEDUCT_DETAIL_STATUS'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "1"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE >= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE <= :lastDateNow
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_END_USE_DATE !="0000-00-00"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"
                                             ')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();
        //print_r($modelDetailDeAllMonthHaveDateStart);
        $arrModelDetailDeAllMonthHaveDateStart = [];
        foreach ($modelDetailDeAllMonthHaveDateStart as $key => $value) {
            $ID_CRAD = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            if (array_key_exists($ID_CRAD, $arrWageThisMonth)) {
                $resultWageID = $arrWageThisMonth[$ID_CRAD];
            }
            $arrModelDetailDeAllMonthHaveDateStart[$key]['WAGE_THIS_MONTH_ID'] = $resultWageID;
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_EMP_ID'] = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_PAY_DATE'] = $MonthYear;
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_ID'] = $value['ADD_DEDUCT_TEMPLATE_ID'];
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] = $value['ADD_DEDUCT_TEMPLATE_NAME'];
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL_ID'] = $value['ADD_DEDUCT_DETAIL_ID'];
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL'] = $value['ADD_DEDUCT_DETAIL'];
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_AMOUNT'] = $value['ADD_DEDUCT_DETAIL_AMOUNT'];
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_TYPE'] = $value['ADD_DEDUCT_TEMPLATE_TYPE'];
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_STATUS'] = '1';
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'] = 1;
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_DATE'] = new Expression('NOW()');
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'] = null;
            $arrModelDetailDeAllMonthHaveDateStart[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_BY'] = null;
        }
//
        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('ADD_DEDUCT_THIS_MONTH', ['WAGE_THIS_MONTH_ID',
                'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                'ADD_DEDUCT_THIS_MONTH_DETAIL',
                'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                'ADD_DEDUCT_THIS_MONTH_TYPE',
                'ADD_DEDUCT_THIS_MONTH_STATUS',
                'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'],
                $arrModelDetailDeAllMonthHaveDateStart)
            ->execute();

        $modelDetailDeThisMonth = Adddeductdetail::find()
            ->select(['ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_EMP_ID as ADD_DEDUCT_DETAIL_EMP_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_AMOUNT as ADD_DEDUCT_DETAIL_AMOUNT',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID as ADD_DEDUCT_TEMPLATE_ID',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_NAME as ADD_DEDUCT_TEMPLATE_NAME',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID as ADD_DEDUCT_DETAIL_ID',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL as ADD_DEDUCT_DETAIL',
                'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE as ADD_DEDUCT_TEMPLATE_TYPE',
                'ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS as ADD_DEDUCT_DETAIL_STATUS'
            ])
            ->from('ADD_DEDUCT_DETAIL')
            ->innerJoin('ADD_DEDUCT_TEMPLATE', 'ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_ID = ADD_DEDUCT_DETAIL.ADD_DEDUCT_ID')
            ->where('ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_STATUS != "99"
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_TYPE = "2"
                                            AND  ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_START_USE_DATE = :lastDateNow
                                            AND  ADD_DEDUCT_TEMPLATE.ADD_DEDUCT_TEMPLATE_STATUS != "99"')
            ->andWhere(['IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $ADD_DEDUCT_DETAIL_EMP_ID])
            ->addParams([':lastDateNow' => $lastDateNow,])
            ->asArray()
            ->all();

        $arrModelDetailDeThisMonth = [];
        foreach ($modelDetailDeThisMonth as $key => $value) {
            $ID_CRAD = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            if (array_key_exists($ID_CRAD, $arrWageThisMonth)) {
                $resultWageID = $arrWageThisMonth[$ID_CRAD];
            }
            $arrModelDetailDeThisMonth[$key]['WAGE_THIS_MONTH_ID'] = $resultWageID;
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_EMP_ID'] = $value['ADD_DEDUCT_DETAIL_EMP_ID'];
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_PAY_DATE'] = $MonthYear;
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_TMP_ID'] = $value['ADD_DEDUCT_TEMPLATE_ID'];
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] = $value['ADD_DEDUCT_TEMPLATE_NAME'];
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL_ID'] = $value['ADD_DEDUCT_DETAIL_ID'];
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL'] = $value['ADD_DEDUCT_DETAIL'];
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_AMOUNT'] = $value['ADD_DEDUCT_DETAIL_AMOUNT'];
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_TYPE'] = $value['ADD_DEDUCT_TEMPLATE_TYPE'];
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_STATUS'] = '1';
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'] = 1;
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_DATE'] = new Expression('NOW()');
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'] = null;
            $arrModelDetailDeThisMonth[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_BY'] = null;
        }
//
        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('ADD_DEDUCT_THIS_MONTH', ['WAGE_THIS_MONTH_ID',
                'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                'ADD_DEDUCT_THIS_MONTH_DETAIL',
                'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                'ADD_DEDUCT_THIS_MONTH_TYPE',
                'ADD_DEDUCT_THIS_MONTH_STATUS',
                'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'],
                $arrModelDetailDeThisMonth)
            ->execute();
        //  print_r($arrModelDetailDeThisMonth);

        ////////////////////////////////// สิ้นสุดส่วนลด //////////////////////////////////

        ////////////////////////////////// เริ่มประกันสังคมคิด //////////////////////////////////

        // print_r($modelListSSOThismonth);

        $arrDeducSSO = [];
        foreach ($modelListSSOThismonth as $key => $value) {
            $ID_CRAD = $value['SOCIAL_SALARY_EMP_ID'];
            if (array_key_exists($ID_CRAD, $arrWageThisMonth)) {
                $resultWageID = $arrWageThisMonth[$ID_CRAD];
            }
            $arrDeducSSO[$key]['WAGE_THIS_MONTH_ID'] = $resultWageID;
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_EMP_ID'] = $value['SOCIAL_SALARY_EMP_ID'];
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_PAY_DATE'] = $MonthYear;
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_TMP_ID'] = '';
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] = 'ประกันสังคม';
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL_ID'] = '';
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_DETAIL'] = '';
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_AMOUNT'] = $value['SOCIAL_SALARY_AMOUNT'];
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_TYPE'] = 2;
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_STATUS'] = '1';
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'] = 1;
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_DATE'] = new Expression('NOW()');
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_CREATE_BY'] = $ID_CRAD_TRANSACTION_BY;
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'] = null;
            $arrDeducSSO[$key]['ADD_DEDUCT_THIS_MONTH_UPDATE_BY'] = null;
        }

        Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
            ->batchInsert('ADD_DEDUCT_THIS_MONTH', ['WAGE_THIS_MONTH_ID',
                'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                'ADD_DEDUCT_THIS_MONTH_DETAIL',
                'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                'ADD_DEDUCT_THIS_MONTH_TYPE',
                'ADD_DEDUCT_THIS_MONTH_STATUS',
                'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'],
                $arrDeducSSO)
            ->execute();
        //////////////////////////////// สิ้นสุดประกันสังคมคิด //////////////////////////////////

//
//            $transaction->commit();
//
//        } catch (ErrorException $e) {
//            $transaction->rollBack();
//            // Yii::warning("Division by zero.");
//            throw new \Exception('ERROR');
//        }


    }


    //calculate social
    //format $arrIDCard = ['xxxx','yyyy']
    //format $strMonthYear is 02-2017
    public static function CalculateSSO($arrIDCard, $MonthYear)
    {
        //print_r($arrIDCard);
        //print_r($MonthYear);

//      print_r($resultYear);
//exit;

        //    $monthDetail = DateTime::mapMonth($resultYear);

        $modelSsoIncomeStructure = SsoIncomeStructure::find()
            ->asArray()
            ->all();

        //print_r($modelSsoIncomeStructure);
        $setMaxSalary = $modelSsoIncomeStructure['2']['income_floor'];
        $setMaxValueSSO = $modelSsoIncomeStructure['2']['sso_rate'];
        echo ">>>>>" . $modelSsoIncomeStructure['1']['income_ceil'];
        echo ">>>>>" . $modelSsoIncomeStructure['1']['income_floor'];


        $ID_CRAD_TRANSACTION_BY = Yii::$app->session->get('idcard');

        //print_r($resultYear);
        $resultCalculateSSO = [];
        foreach ($arrIDCard as $key => $value) {
            $valueRoundDecimalResult = "";
            if ($value['EMP_SALARY_WAGE'] >= $setMaxSalary) {
                $valueRoundDecimalResult = $setMaxValueSSO;
            } else if (($modelSsoIncomeStructure['1']['income_floor'] < $value['EMP_SALARY_WAGE']) or ($value['EMP_SALARY_WAGE'] >= $modelSsoIncomeStructure['1']['income_ceil'])) {
                $valueresultSSO = $value['EMP_SALARY_WAGE'] * ($modelSsoIncomeStructure['1']['sso_rate'] / 100);
                $valueDecimalResult = Helper::displayDecimal($valueresultSSO);
                $valueRoundDecimalResult = Utility::MakeRound($valueDecimalResult);
            } else if ($value['EMP_SALARY_WAGE'] <= $modelSsoIncomeStructure['0']['income_floor']) {
                $valueRoundDecimalResult = $value['EMP_SALARY_WAGE'] . ">>>>>>>83";
            }

            if ($value['socialBenefitStatus'] == 1) {
                $resultCalculateSSO[$key]['SOCIAL_SALARY_DATE'] = $MonthYear;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_EMP_ID'] = $value['empIdCard'];
                $resultCalculateSSO[$key]['SOCIAL_SALARY_PERCENT'] = $value['socialBenifitPercent'];
                $resultCalculateSSO[$key]['SOCIAL_SALARY_AMOUNT'] = $valueRoundDecimalResult;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_STATUS'] = 1;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_TRANSACTION_BY'] = $ID_CRAD_TRANSACTION_BY;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_TRANSACTION_DATE'] = new Expression('NOW()');
            } else {
                $resultCalculateSSO[$key]['SOCIAL_SALARY_DATE'] = $MonthYear;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_EMP_ID'] = $value['empIdCard'];
                $resultCalculateSSO[$key]['SOCIAL_SALARY_PERCENT'] = $value['socialBenifitPercent'];
                $resultCalculateSSO[$key]['SOCIAL_SALARY_AMOUNT'] = 0;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_STATUS'] = 1;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_TRANSACTION_BY'] = $ID_CRAD_TRANSACTION_BY;
                $resultCalculateSSO[$key]['SOCIAL_SALARY_TRANSACTION_DATE'] = new Expression('NOW()');
            }
        }

//            print_r($resultCalculateSSO);
//            exit;


        $model = ApiSSO::SaveCalculateSSO($resultCalculateSSO);

    }



    //calculate social
    //format $arrIDCard = ['xxxx','yyyy']
    //format $strMonthYear is 02-2017
    public static function CalculatePersonalTax()
    {
        return true;
    }

    public static function GetlistWagesSalary($date_pay)
    {

        $model = Wagethismonth::find()
            ->where(['WAGE_PAY_DATE'=>$date_pay])
            ->asArray()
            ->all();
        if ($model) {
            return true;
        } else {
            return false;
        }
    }

    public static function GetlistWagesSalaryByIdcard($id_card)
    {
        $model = Wagethismonth::find()
            ->where('WAGE_EMP_ID = :id_card')
            ->addParams([':id_card' => $id_card,])
            ->asArray()
            ->all();
        return $model;
    }

    public static function GetlistWagesSalaryDeatilByIdcard($id_card)
    {
        $model = Vempsalarydetail::find()
            ->where('ID_Card = :id_card')
            ->addParams([':id_card' => $id_card,])
            ->asArray()
            ->all();
        return $model;
    }

    public static function Confirmfirst($id_wage_confirm, $id_card_confirm)
    {
        //echo "1111";
        $model = Wagethismonth::findOne($id_wage_confirm);
        $model->WAGE_THIS_MONTH_CONFIRM = "1";
        $model->WAGE_THIS_MONTH_EMPLOYEE_LOCK = 1;
        $model->WAGE_THIS_MONTH_CONFIRM_BY = $id_card_confirm;
        $model->WAGE_THIS_MONTH_CONFIRM_DATE = new Expression('NOW()');
        $model->save();
        if ($model->save() !== false) {
            $a = "update successful";
        }
        return $a;

    }

    public static function Confirmlast($id_wage_confirm, $id_card_confirm)
    {
        //echo "1111";
        $model = Wagethismonth::findOne($id_wage_confirm);
        $model->WAGE_THIS_MONTH_DIRECTOR_LOCK = "1";
        $model->WAGE_UPDATE_BY = $id_card_confirm;
        $model->WAGE_UPDATE_DATE = new Expression('NOW()');
        $model->save();
        if ($model->save() !== false) {
            $a = "update successful";
        }
        return $a;

    }

    public static function seachEmpAndCompanyGetlistWagesSalary($id_company, $id_department, $id_section)
    {

        if (!empty($id_company) AND !empty($id_department) AND !empty($id_section)) {
            $data = Vempsalarydetail::find()
                ->select(['ID_Card as ID_Card',
                    'CONCAT(empname," ",surname) as Fullname',
                    'workname as CompanyName',
                    'departmentname as DepartmentName',
                    'empSection as SectionName',
                    'empposition as Position',
                    'WAGE_THIS_MONTH_CONFIRM as WAGE_THIS_MONTH_CONFIRM',
                    'WAGE_THIS_MONTH_EMPLOYEE_LOCK as WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                    'WAGE_THIS_MONTH_DIRECTOR_LOCK as WAGE_THIS_MONTH_DIRECTOR_LOCK',
                    'WAGE_THIS_MONTH_CONFIRM_BY as WAGE_THIS_MONTH_CONFIRM_BY',
                    'WAGE_THIS_MONTH_CONFIRM_DATE as WAGE_THIS_MONTH_CONFIRM_DATE'
                ])
                ->where('WAGE_WORKING_COMPANY = :id_company
                        AND WAGE_DEPARTMENT_ID = :id_department
                        AND WAGE_SECTION_ID = :id_section')
                ->addParams([':id_company' => $id_company,
                    ':id_department' => $id_department,
                    ':id_section' => $id_section,
                ])
                ->groupBy('ID_Card')
                ->asArray()
                ->all();

            return $data;
        } elseif (!empty($id_company) AND !empty($id_department)) {
            $data = Vempsalarydetail::find()
                ->select(['ID_Card as ID_Card',
                    'CONCAT(empname," ",surname) as Fullname',
                    'workname as CompanyName',
                    'departmentname as DepartmentName',
                    'empSection as SectionName',
                    'empposition as Position',
                    'WAGE_THIS_MONTH_CONFIRM as WAGE_THIS_MONTH_CONFIRM',
                    'WAGE_THIS_MONTH_EMPLOYEE_LOCK as WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                    'WAGE_THIS_MONTH_DIRECTOR_LOCK as WAGE_THIS_MONTH_DIRECTOR_LOCK',
                    'WAGE_THIS_MONTH_CONFIRM_BY as WAGE_THIS_MONTH_CONFIRM_BY',
                    'WAGE_THIS_MONTH_CONFIRM_DATE as WAGE_THIS_MONTH_CONFIRM_DATE'
                ])
                ->where('WAGE_WORKING_COMPANY = :id_company
                        AND WAGE_DEPARTMENT_ID = :id_department')
                ->addParams([':id_company' => $id_company,
                    ':id_department' => $id_department,
                ])
                ->groupBy('ID_Card')
                ->asArray()
                ->all();

            return $data;
        } else if (!empty($id_company)) {
            $data = Vempsalarydetail::find()
                ->select(['ID_Card as ID_Card',
                    'CONCAT(empname," ",surname) as Fullname',
                    'workname as CompanyName',
                    'departmentname as DepartmentName',
                    'empSection as SectionName',
                    'empposition as Position',
                    'WAGE_THIS_MONTH_CONFIRM as WAGE_THIS_MONTH_CONFIRM',
                    'WAGE_THIS_MONTH_EMPLOYEE_LOCK as WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                    'WAGE_THIS_MONTH_DIRECTOR_LOCK as WAGE_THIS_MONTH_DIRECTOR_LOCK',
                    'WAGE_THIS_MONTH_CONFIRM_BY as WAGE_THIS_MONTH_CONFIRM_BY',
                    'WAGE_THIS_MONTH_CONFIRM_DATE as WAGE_THIS_MONTH_CONFIRM_DATE'
                ])
                ->where('WAGE_WORKING_COMPANY = :id_company')
                ->addParams([':id_company' => $id_company,
                ])
                ->groupBy('ID_Card')
                ->asArray()
                ->all();

            return $data;
        } else {
            $data = Vempsalarydetail::find()
                ->select(['ID_Card as ID_Card',
                    'CONCAT(empname," ",surname) as Fullname',
                    'workname as CompanyName',
                    'departmentname as DepartmentName',
                    'empSection as SectionName',
                    'empposition as Position',
                    'WAGE_THIS_MONTH_CONFIRM as WAGE_THIS_MONTH_CONFIRM',
                    'WAGE_THIS_MONTH_EMPLOYEE_LOCK as WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                    'WAGE_THIS_MONTH_DIRECTOR_LOCK as WAGE_THIS_MONTH_DIRECTOR_LOCK',
                    'WAGE_THIS_MONTH_CONFIRM_BY as WAGE_THIS_MONTH_CONFIRM_BY',
                    'WAGE_THIS_MONTH_CONFIRM_DATE as WAGE_THIS_MONTH_CONFIRM_DATE'
                ])
                ->groupBy('ID_Card')
                ->asArray()
                ->all();

            return $data;
        }


    }

    public static function seachEmpIdcradGetlistWagesSalary($id_emp)
    {

        $data = Vempsalarydetail::find()
            ->select(['ID_Card as ID_Card',
                'CONCAT(empname," ",surname) as Fullname',
                'workname as CompanyName',
                'departmentname as DepartmentName',
                'empSection as SectionName',
                'empposition as Position',
                'WAGE_THIS_MONTH_CONFIRM as WAGE_THIS_MONTH_CONFIRM',
                'WAGE_THIS_MONTH_EMPLOYEE_LOCK as WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                'WAGE_THIS_MONTH_DIRECTOR_LOCK as WAGE_THIS_MONTH_DIRECTOR_LOCK',
                'WAGE_THIS_MONTH_CONFIRM_BY as WAGE_THIS_MONTH_CONFIRM_BY',
                'WAGE_THIS_MONTH_CONFIRM_DATE as WAGE_THIS_MONTH_CONFIRM_DATE'
            ])
            ->where('ID_Card = :ID_Card')
            ->addParams([':ID_Card' => $id_emp,
            ])
            ->groupBy('ID_Card')
            ->asArray()
            ->all();

        //print_r($data);

        return $data;


    }

    public static function deleteEmpSalary($dateTime)
    {
        echo $dateTime;
        $modelDeleteSalary = Wagethismonth::deleteAll('WAGE_PAY_DATE = "' . $dateTime . '"');
        $modelAdddeductThismonth = Adddeductthismonth::deleteAll('ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $dateTime . '"');
        $modelSocial = Socialsalary::deleteAll('SOCIAL_SALARY_DATE = "' . $dateTime . '"');
        $modelBenefitfund = Benefitfund::deleteAll('BENEFIT_SAVING_DATE = "' . $dateTime . '"');
        //$dateTime
    }


    public static function moveWageThismountToHistory($idworkingcompany)
    {

        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();

        try {
            foreach ($idworkingcompany as $key => $value) {
                $model = Wagethismonth::find()
                    ->where('WAGE_WORKING_COMPANY = ' . $value . '')
                    ->andWhere('WAGE_THIS_MONTH_CONFIRM = 1')
                    ->asArray()
                    ->all();

                $arrValue = $arrIdcard = [];

                foreach ($model as $key => $value) {
                    $arrValue[$key]['WAGE_ID'] = $value['WAGE_ID'];
                    $arrValue[$key]['WAGE_THIS_MONTH_ROW_NUM'] = $value['WAGE_THIS_MONTH_ROW_NUM'];
                    $arrValue[$key]['WAGE_EMP_ID'] = $value['WAGE_EMP_ID'];
                    $arrValue[$key]['WAGE_PAY_DATE'] = $value['WAGE_PAY_DATE'];
                    $arrValue[$key]['WAGE_POSITION_CODE'] = $value['WAGE_POSITION_CODE'];
                    $arrValue[$key]['WAGE_POSITION_NAME'] = $value['WAGE_POSITION_NAME'];
                    $arrValue[$key]['WAGE_SECTION_ID'] = $value['WAGE_SECTION_ID'];
                    $arrValue[$key]['WAGE_DEPARTMENT_ID'] = $value['WAGE_DEPARTMENT_ID'];
                    $arrValue[$key]['WAGE_WORKING_COMPANY'] = $value['WAGE_WORKING_COMPANY'];
                    $arrValue[$key]['WAGE_BANK_NAME'] = $value['WAGE_BANK_NAME'];
                    $arrValue[$key]['WAGE_ACCOUNT_NUMBER'] = $value['WAGE_ACCOUNT_NUMBER'];
                    $arrValue[$key]['WAGE_GET_MONEY_TYPE'] = $value['WAGE_GET_MONEY_TYPE'];
                    $arrValue[$key]['WAGE_SALARY_CHART'] = $value['WAGE_SALARY_CHART'];
                    $arrValue[$key]['WAGE_SALARY_LEVEL'] = $value['WAGE_SALARY_LEVEL'];
                    $arrValue[$key]['WAGE_SALARY_STEP'] = $value['WAGE_SALARY_STEP'];
                    $arrValue[$key]['WAGE_SALARY'] = $value['WAGE_SALARY'];
                    $arrValue[$key]['WAGE_SALARY_BY_CHART'] = $value['WAGE_SALARY_BY_CHART'];
                    $arrValue[$key]['WAGE_TOTAL_ADDITION'] = $value['WAGE_TOTAL_ADDITION'];
                    $arrValue[$key]['WAGE_EARN_PLUS_ADD'] = $value['WAGE_EARN_PLUS_ADD'];
                    $arrValue[$key]['WAGE_TOTAL_DEDUCTION'] = $value['WAGE_TOTAL_DEDUCTION'];
                    $arrValue[$key]['WAGE_EARN_MINUS_DEDUCT'] = $value['WAGE_EARN_MINUS_DEDUCT'];
                    $arrValue[$key]['WAGE_NET_SALARY'] = $value['WAGE_NET_SALARY'];
                    $arrValue[$key]['WAGE_THIS_MONTH_CONFIRM'] = $value['WAGE_THIS_MONTH_CONFIRM'];
                    $arrValue[$key]['WAGE_THIS_MONTH_CONFIRM_BY'] = $value['WAGE_THIS_MONTH_CONFIRM_BY'];
                    $arrValue[$key]['WAGE_THIS_MONTH_CONFIRM_DATE'] = $value['WAGE_THIS_MONTH_CONFIRM_DATE'];
                    $arrValue[$key]['WAGE_THIS_MONTH_EMPLOYEE_LOCK'] = $value['WAGE_THIS_MONTH_EMPLOYEE_LOCK'];
                    $arrValue[$key]['WAGE_THIS_MONTH_DIRECTOR_LOCK'] = $value['WAGE_THIS_MONTH_DIRECTOR_LOCK'];
                    $arrValue[$key]['WAGE_THIS_MONTH_STATUS'] = $value['WAGE_THIS_MONTH_STATUS'];
                    $arrValue[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_STATUS'] = $value['WAGE_THIS_MONTH_BANK_CONFIRM_STATUS'];
                    $arrValue[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_DATE'] = $value['WAGE_THIS_MONTH_BANK_CONFIRM_DATE'];
                    $arrValue[$key]['WAGE_THIS_MONTH_BANK_CONFIRM_BY'] = $value['WAGE_THIS_MONTH_BANK_CONFIRM_BY'];
                    $arrValue[$key]['WAGE_REMARK'] = $value['WAGE_REMARK'];
                    $arrValue[$key]['WAGE_CREATE_DATE'] = $value['WAGE_CREATE_DATE'];
                    $arrValue[$key]['WAGE_CREATE_BY'] = $value['WAGE_CREATE_BY'];
                    $arrValue[$key]['WAGE_UPDATE_DATE'] = $value['WAGE_UPDATE_DATE'];
                    $arrValue[$key]['WAGE_UPDATE_BY'] = $value['WAGE_UPDATE_BY'];

                    $arrIdcard [$key]['WAGE_EMP_ID'] = $value['WAGE_EMP_ID'];

                }
                //print_r($arrIdcard);

                $connection->createCommand()
                    ->batchInsert('WAGE_HISTORY', ['WAGE_ID',
                        'WAGE_THIS_MONTH_ROW_NUM',
                        'WAGE_EMP_ID',
                        'WAGE_PAY_DATE',
                        'WAGE_POSITION_CODE',
                        'WAGE_POSITION_NAME',
                        'WAGE_SECTION_ID',
                        'WAGE_DEPARTMENT_ID',
                        'WAGE_WORKING_COMPANY',
                        'WAGE_BANK_NAME',
                        'WAGE_ACCOUNT_NUMBER',
                        'WAGE_GET_MONEY_TYPE',
                        'WAGE_SALARY_CHART',
                        'WAGE_SALARY_LEVEL',
                        'WAGE_SALARY_STEP',
                        'WAGE_SALARY',
                        'WAGE_SALARY_BY_CHART',
                        'WAGE_TOTAL_ADDITION',
                        'WAGE_EARN_PLUS_ADD',
                        'WAGE_TOTAL_DEDUCTION',
                        'WAGE_EARN_MINUS_DEDUCT',
                        'WAGE_NET_SALARY',
                        'WAGE_THIS_MONTH_CONFIRM',
                        'WAGE_THIS_MONTH_CONFIRM_BY',
                        'WAGE_THIS_MONTH_CONFIRM_DATE',
                        'WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                        'WAGE_THIS_MONTH_DIRECTOR_LOCK',
                        'WAGE_THIS_MONTH_STATUS',
                        'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS',
                        'WAGE_THIS_MONTH_BANK_CONFIRM_DATE',
                        'WAGE_THIS_MONTH_BANK_CONFIRM_BY',
                        'WAGE_REMARK',
                        'WAGE_CREATE_DATE',
                        'WAGE_CREATE_BY',
                        'WAGE_UPDATE_DATE',
                        'WAGE_UPDATE_BY',],
                        $arrValue)
                    ->execute();



                foreach ($arrIdcard as $no => $valueIdcard) {
                    $modelAddeduchist = Adddeductthismonth::find()
                        ->where('ADD_DEDUCT_THIS_MONTH_EMP_ID = ' . $valueIdcard['WAGE_EMP_ID'] . '')
                        ->asArray()
                        ->all();

                    $connection->createCommand()
                        ->batchInsert('ADD_DEDUCT_HISTORY', ['ADD_DEDUCT_THIS_MONTH_ID',
                            'WAGE_THIS_MONTH_ID',
                            'ADD_DEDUCT_THIS_MONTH_EMP_ID',
                            'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                            'ADD_DEDUCT_THIS_MONTH_TMP_ID',
                            'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                            'ADD_DEDUCT_THIS_MONTH_DETAIL_ID',
                            'ADD_DEDUCT_THIS_MONTH_DETAIL',
                            'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                            'ADD_DEDUCT_THIS_MONTH_TYPE',
                            'ADD_DEDUCT_THIS_MONTH_STATUS',
                            'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS',
                            'ADD_DEDUCT_THIS_MONTH_CREATE_DATE',
                            'ADD_DEDUCT_THIS_MONTH_CREATE_BY',
                            'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                            'ADD_DEDUCT_THIS_MONTH_UPDATE_BY',],
                            $modelAddeduchist)
                        ->execute();

                }

                $transaction->commit();
            }
        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }


    }

}


?>