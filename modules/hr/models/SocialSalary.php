<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "SOCIAL_SALARY".
 *
 * @property int $SOCIAL_SALARY_ID
 * @property string $SOCIAL_SALARY_DATE
 * @property string $SOCIAL_SALARY_EMP_ID
 * @property string $SOCIAL_SALARY_PERCENT
 * @property string $SOCIAL_SALARY_AMOUNT
 * @property string $SOCIAL_SALARY_STATUS
 * @property string $SOCIAL_SALARY_TRANSACTION_BY
 * @property string $SOCIAL_SALARY_TRANSACTION_DATE
 */
class SocialSalary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'SOCIAL_SALARY';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SOCIAL_SALARY_DATE', 'SOCIAL_SALARY_EMP_ID', 'SOCIAL_SALARY_PERCENT', 'SOCIAL_SALARY_AMOUNT', 'SOCIAL_SALARY_STATUS', 'SOCIAL_SALARY_TRANSACTION_BY', 'SOCIAL_SALARY_TRANSACTION_DATE'], 'required'],
            [['SOCIAL_SALARY_AMOUNT'], 'number'],
            [['SOCIAL_SALARY_TRANSACTION_DATE'], 'safe'],
            [['SOCIAL_SALARY_DATE'], 'string', 'max' => 100],
            [['SOCIAL_SALARY_EMP_ID'], 'string', 'max' => 15],
            [['SOCIAL_SALARY_PERCENT', 'SOCIAL_SALARY_STATUS'], 'string', 'max' => 2],
            [['SOCIAL_SALARY_TRANSACTION_BY'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SOCIAL_SALARY_ID' => 'Social  Salary  ID',
            'SOCIAL_SALARY_DATE' => 'Social  Salary  Date',
            'SOCIAL_SALARY_EMP_ID' => 'Social  Salary  Emp  ID',
            'SOCIAL_SALARY_PERCENT' => 'Social  Salary  Percent',
            'SOCIAL_SALARY_AMOUNT' => 'Social  Salary  Amount',
            'SOCIAL_SALARY_STATUS' => 'Social  Salary  Status',
            'SOCIAL_SALARY_TRANSACTION_BY' => 'Social  Salary  Transaction  By',
            'SOCIAL_SALARY_TRANSACTION_DATE' => 'Social  Salary  Transaction  Date',
        ];
    }
}
