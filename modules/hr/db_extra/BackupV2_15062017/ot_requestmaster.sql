-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 08, 2017 at 03:00 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_requestmaster`
--

CREATE TABLE `ot_requestmaster` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `request_no` varchar(20) NOT NULL COMMENT 'เลขที่คำขอโอที',
  `company_id` tinyint(3) NOT NULL COMMENT 'รหัสบริษัท',
  `division_id` int(11) NOT NULL COMMENT 'รหัสแผนก',
  `section_id` int(11) NOT NULL COMMENT 'รหัสฝ่าย',
  `activity_id` tinyint(3) NOT NULL COMMENT 'รหัสกิจกรรม',
  `activity_name` varchar(250) NOT NULL COMMENT 'ชื่อกิจกรรมโอที',
  `ot_calculate_type` tinyint(3) NOT NULL COMMENT 'ประเภทการคิดโอที 1=ปกติ, 2=นอกเวลาปกติ, 3=นอกเวลาวันหยุด',
  `activity_date` date NOT NULL COMMENT 'วันที่ทำกิจกรรมโอที',
  `has_profile_route` tinyint(3) DEFAULT NULL COMMENT 'มีค่าเดินทาง 1=มี, 0 = ไม่มี',
  `profile_route_id` tinyint(3) DEFAULT NULL COMMENT 'รหัสโปรไฟล์การเดินทาง',
  `distance_amount` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนระยะทางเป็น กิโลเมตร',
  `has_motel_profile` tinyint(3) DEFAULT NULL COMMENT 'มีค่าที่พัก 1=มี, 0 = ไม่มี',
  `motel_price` decimal(10,2) DEFAULT NULL COMMENT 'ราคาค่าที่พัก',
  `request_byuser` char(13) NOT NULL COMMENT 'เสนอขอโดยผู้ใช้',
  `request_date` date NOT NULL COMMENT 'วันที่ที่เสนอขอ',
  `request_time` time NOT NULL,
  `is_approved` tinyint(3) NOT NULL COMMENT 'สถานะ approved โดย HR, 2 = ตีกลับไปแก้ไข, 1= approved, 0 =  Not approved',
  `approved_byuser` char(13) DEFAULT NULL COMMENT 'approved โดยผู้ใช้',
  `approved_date` date DEFAULT NULL COMMENT 'วันที่ approved',
  `approved_time` time DEFAULT NULL COMMENT 'เวลาที่ approved',
  `status_active` tinyint(3) NOT NULL COMMENT 'สถานะ active=1, inactive =0, ',
  `reject_byuser` char(13) DEFAULT NULL COMMENT 'approved โดยผู้ใช้',
  `reject_date` date DEFAULT NULL COMMENT 'วันที่ approved',
  `reject_time` time DEFAULT NULL COMMENT 'เวลาที่ approved',
  `createby_user` varchar(30) NOT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime NOT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_requestmaster`
--
ALTER TABLE `ot_requestmaster`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `request_no` (`request_no`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `division_id` (`division_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `activity_id` (`activity_id`),
  ADD KEY `profile_route_id` (`profile_route_id`),
  ADD KEY `request_byuser` (`request_byuser`),
  ADD KEY `approved_byuser` (`approved_byuser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ot_requestmaster`
--
ALTER TABLE `ot_requestmaster`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
