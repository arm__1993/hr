<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "PND_RECORD".
 *
 * @property int $id
 * @property string $FORMTYPE กําหนดใหเป็น “00” คือยื่น ปกติ
 * @property string $COMPIN เฉพาะกรณีเป็นบุคคลธรรมดา แต่ถ้าเป็นกรณนีติบุคคลให้ บันทึกเลข 0 จำนวน 13 หลัก
 * @property string $COMTIN ถ้าไม่มีข้อมูลให้บันทึกเลข 0 จํานวน 10 หลัก
 * @property string $BRANO ถ้าไม่มีข้อมูลให้บันทึกเลข 0 จํานวน 4 หลัก
 * @property string $PIN เลขบัตรประชาชน
 * @property string $TIN เลขประจําตัวผู้เสียภาษีอากร ผู้มีเงินได้(ถ้าไม่มีข้อมูลให้บันทึกเลข 0 จํานวน 10 หลัก)
 * @property string $PER_N1 คํานําหน้าชื่อผู้มีเงินได้
 * @property string $NAME1 ชื่อผู้มีเงินได้
 * @property string $SUR_N1 นามสกุลผู้มีเงินได้
 * @property string $ADDRESS1
 * @property string $ADDRESS2 ถ้าไม่มีให้คั่นด้วย | 
 * @property string $POSCOD รหัสไปรษณีย์
 * @property string $TAXMONTH เดือนภาษี
 * @property string $TAXYEAR ปีภาษี(ต้องบันทึกเป็นพ.ศ.เต็ม 4 หลักให้ถูกต้อง)
 * @property string $INCOMECODE รหัสเงินได้(ต้องบันทึกให้ถูกต้อง ดังนี้ 1=40(1) 2 = 40(2)ร้อยละ3  )3 = 40(1)(2) ออกจากงาน (4 = 40(2)ผู้รับอยู่ในประเทศ ไทย)5 = 40(2)ผู้รับไม่อยู่ในประเทศ ไทย)
 * @property string $PAYDATE วันที่จ่ายเงินได้รูป แบบDDMMYYYY 
 * @property string $TAXRATE อัตราภาษีร้อยละ
 * @property string $PAYMENT ถ้าไม่มีข้อมูลให้บันทึก 0.00 
 * @property string $TAX จํานวนเงินภาษีที่หักและ นําส่ง
 * @property string $TAXCONDITION
 * @property int $working_company_id working_company_id
 */
class PNDRECORD extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PND_RECORD';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FORMTYPE', 'COMPIN', 'COMTIN', 'BRANO', 'PIN', 'TIN', 'PER_N1', 'NAME1', 'SUR_N1', 'ADDRESS1', 'POSCOD', 'TAXMONTH', 'TAXYEAR', 'INCOMECODE', 'PAYDATE', 'TAXRATE', 'PAYMENT', 'TAX', 'TAXCONDITION', 'working_company_id'], 'required'],
            [['working_company_id'], 'integer'],
            [['FORMTYPE', 'TAXMONTH'], 'string', 'max' => 2],
            [['COMPIN', 'PIN', 'PAYMENT', 'TAX'], 'string', 'max' => 13],
            [['COMTIN', 'TIN'], 'string', 'max' => 10],
            [['BRANO', 'TAXYEAR'], 'string', 'max' => 4],
            [['PER_N1'], 'string', 'max' => 40],
            [['NAME1', 'SUR_N1', 'ADDRESS1', 'ADDRESS2'], 'string', 'max' => 80],
            [['POSCOD'], 'string', 'max' => 5],
            [['INCOMECODE', 'TAXCONDITION'], 'string', 'max' => 1],
            [['PAYDATE'], 'string', 'max' => 8],
            [['TAXRATE'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FORMTYPE' => 'Formtype',
            'COMPIN' => 'Compin',
            'COMTIN' => 'Comtin',
            'BRANO' => 'Brano',
            'PIN' => 'Pin',
            'TIN' => 'Tin',
            'PER_N1' => 'Per  N1',
            'NAME1' => 'Name1',
            'SUR_N1' => 'Sur  N1',
            'ADDRESS1' => 'Address1',
            'ADDRESS2' => 'Address2',
            'POSCOD' => 'Poscod',
            'TAXMONTH' => 'Taxmonth',
            'TAXYEAR' => 'Taxyear',
            'INCOMECODE' => 'Incomecode',
            'PAYDATE' => 'Paydate',
            'TAXRATE' => 'Taxrate',
            'PAYMENT' => 'Payment',
            'TAX' => 'Tax',
            'TAXCONDITION' => 'Taxcondition',
            'working_company_id' => 'Working Company ID',
        ];
    }
}
