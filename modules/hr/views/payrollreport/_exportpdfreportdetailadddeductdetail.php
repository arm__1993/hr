<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;
$modelEmp = $session->get('modelEmp');
$modelTemp = $session->get('modelTemp');
$modelSalary = $session->get('modelSalary');
$modelAdddeductdtail = $session->get('modelAdddeductdtail');
$modelSumTotal = $session->get('modelSumTotal');
$statusSalary = $session->get('statusSalary');
$modelWorkingcompany = $session->get('modelWorkingcompany');
$querySalaryTotalThisMonth6 = $session->get('querySalaryTotalThisMonth6');
$monthselect = $session->get('monthresult');

?>
<h4 style="text-align:center;">รายละเอียดรายงานเงินเดือน</h4>

<div class="row">
    <div class="row">
    </div>
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-6">
            <h4><?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                //print_r($monthselect);
                $monthGetReport1 = $getMonth['monthresult'];
                $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
                $yearGetReport1 = $getMonth['year'];
                ?>
                รายงานสรุปผลเงินเดือนรายบุคคล (รายการเพิ่ม) จ่าย <?php echo $resultMonthReport1;
                echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h4>
        </div>
    </div>
    <?php //if($querySalaryTotalThisMonth){
    //  echo "<pre>";
    //   print_r($reportSalaryTotal);?>
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
        <?php foreach ($modelTemp AS $valueTemp) {
            $ADD_DEDUCT_TEMPLATE_ID = $valueTemp['ADD_DEDUCT_TEMPLATE_ID'];
            ?>
            <tr>
                <td><h4>รายงานการเพิ่ม <?php echo $valueTemp['ADD_DEDUCT_TEMPLATE_NAME']; ?> จ่ายวันที่ </h4></td>
            </tr>
            <?php foreach ($modelWorkingcompany as $valueCompany) {

                if ($valueCompany['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $ADD_DEDUCT_TEMPLATE_ID) {
                    $working_companyname = $valueCompany['working_companyname'];

                    ?>
                    <tr>
                        <td><h5>บริษัท <?php echo $working_companyname; ?> [ <?php echo $valueCompany['short_name']; ?>
                                ]</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
                                   aria-describedby="example1_wrapper" width="100%" border="1" cellpadding="5"
                                   cellspacing="0">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-sort="ascending" width="20%">
                                        ชื่อ - สกุล
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-sort="ascending" width="10%">
                                        แผนก
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-sort="ascending" width="10%">
                                        จำนวนเงิน
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-sort="ascending" width="10%">
                                        รายละเอียด
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $sumtotal = [];
                                foreach ($modelAdddeductdtail as $valueAdddeduct) {
                                    if ($valueAdddeduct['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $ADD_DEDUCT_TEMPLATE_ID &&
                                        $valueAdddeduct['WAGE_WORKING_COMPANY'] == $valueCompany['working_companyid']
                                    ) {
                                        // echo $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_TMP_ID'];
                                        // echo "<br>";
                                        // echo $valueAdddeduct['WAGE_WORKING_COMPANY'];
                                        $sumtotal[] = $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_AMOUNT'];; ?>
                                        <tr>
                                            <td><?php echo $valueAdddeduct['NAMEEMP']; ?></td>
                                            <td><?php echo $valueAdddeduct['departmentname']; ?></td>
                                            <td align="right"><?php echo Helper::displayDecimal($valueAdddeduct['ADD_DEDUCT_THIS_MONTH_AMOUNT']); ?></td>
                                            <td><?php echo $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_DETAIL']; ?></td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2" align="right"><b>รวม</b></td>
                                    <td align="right"><?php echo Helper::displayDecimal(array_sum($sumtotal)); ?></td>
                                    <td></td>
                                </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                <?php }
            } ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        <?php } ?>
    </table>
</div>
