-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 17, 2017 at 04:55 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `org_configmaster`
--

CREATE TABLE `org_configmaster` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `work_day_inmonth` decimal(4,2) NOT NULL COMMENT 'จำนวนวันทำงานในเดือน',
  `work_hour_inday` decimal(4,2) NOT NULL COMMENT 'จำนวนชั่วโมงทำงานในวัน',
  `work_start_hour_inday` time NOT NULL COMMENT 'เวลาเริ่มงานวันปกติ',
  `work_end_hour_inday` time NOT NULL COMMENT 'เวลาสิ้นสุดวันปกติ',
  `sso_rate` decimal(3,0) NOT NULL COMMENT 'ค่าเริ่มต้นอัตราการหักเงินประกันสังคม',
  `saving_rate` decimal(3,0) NOT NULL COMMENT 'ค่าเริ่มต้นอัตราการหักเงินสะสม',
  `day_start_salary` tinyint(3) UNSIGNED NOT NULL COMMENT 'วันที่เริ่มต้นการคิดเงินเดือน',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org_configmaster`
--

INSERT INTO `org_configmaster` (`id`, `work_day_inmonth`, `work_hour_inday`, `work_start_hour_inday`, `work_end_hour_inday`, `sso_rate`, `saving_rate`, `day_start_salary`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, '30.00', '8.00', '08:00:00', '17:00:00', '5', '3', 26, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `org_configmaster`
--
ALTER TABLE `org_configmaster`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `org_configmaster`
--
ALTER TABLE `org_configmaster`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
