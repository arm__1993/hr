<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "summary_money_wage".
 *
 * @property integer $id
 * @property string $emp_idcard
 * @property integer $company_id
 * @property string $company_name
 * @property string $position_code
 * @property string $position_name
 * @property string $pay_date
 * @property integer $pay_year
 * @property string $income_date
 * @property string $income_detail
 * @property string $income_amount
 * @property string $income_total_amount
 * @property string $tax_amount
 * @property string $tax_total_amount
 * @property string $sso_amount
 * @property string $sso_total_amount
 * @property string $bnf_amount
 * @property string $bnf_total_amount
 * @property string $transaction_by
 * @property string $transaction_datetime
 * @property integer $status_active
 * @property string $end_effective_date
 * @property string $remark
 */
class SummaryMoneyWage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'summary_money_wage';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'pay_year', 'status_active'], 'integer'],
            [['income_date', 'transaction_datetime', 'end_effective_date'], 'safe'],
            [['income_amount', 'income_total_amount', 'tax_amount', 'tax_total_amount', 'sso_amount', 'sso_total_amount','bnf_amount','bnf_total_amount'], 'number'],
            [['emp_idcard'], 'string', 'max' => 13],
            [['company_name', 'position_name', 'remark'], 'string', 'max' => 250],
            [['position_code'], 'string', 'max' => 20],
            [['pay_date'], 'string', 'max' => 7],
            [['income_detail'], 'string', 'max' => 100],
            [['transaction_by'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_idcard' => 'Emp Idcard',
            'company_id' => 'Company ID',
            'company_name' => 'Company Name',
            'position_code' => 'Position Code',
            'position_name' => 'Position Name',
            'pay_date' => 'Pay Date',
            'pay_year' => 'Pay Year',
            'income_date' => 'Income Date',
            'income_detail' => 'Income Detail',
            'income_amount' => 'Income Amount',
            'income_total_amount' => 'Income Total Amount',
            'tax_amount' => 'Tax Amount',
            'tax_total_amount' => 'Tax Total Amount',
            'sso_amount' => 'Sso Amount',
            'sso_total_amount' => 'Sso Total Amount',
            'bnf_amount' => 'BNF Amount',
            'bnf_total_amount' => 'BNF Total Amount',
            'transaction_by' => 'Transaction By',
            'transaction_datetime' => 'Transaction Datetime',
            'status_active' => 'Status Active',
            'end_effective_date' => 'End Effective Date',
            'remark' => 'Remark',
        ];
    }
}
