<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 09:51
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/employee-lookup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/ot_manageot.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/ot_manageot.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

$arrHour = DateTime::makeHour();
$arrMinute = DateTime::makeMinute();

$today = date('d/m/Y');

/*$arrOTReturn = [
    '1' => 'เงิน',
    '2' => 'วันหยุด'
];*/

//$arrOTReturn = $this->ReturnOTBenefit;
//echo $OtConfig['pay_motel'];


//TODO : 1. Javascript Validate, 2. Save Success show popup
//TODO : Session for user create , approved




?>

<form id="frmListOTRequest" name="frmListOTRequest" action="#" method="post">
    <section class="content">
        <!-- Default box -->
        <div class="box box-danger">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#"> ข้อมูลเงินเดือน</a>
                    </li>
                    <li>บันทึกข้อมูลการทำงานล่วงเวลา</li>
                    <li class="active">บันทึกข้อมูลการทำงานล่วงเวลา</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">


                <div class="box-body">
                    <div class="row">
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>บริษัท <span>*</span></label>
                            <select class="form-control" name="xcompany" id="xcompany">
                                <option value="">เลือกบริษัท</option>
                                <?php
                                $working = ApiHr::getWorking_company();
                                foreach ($working as $value) {
                                    echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                }
                                ?>
                            </select>

                        </div>
                        <div class="col-sm-3">
                            <label>แผนก <span>*</span></label>
                            <select class="form-control" name="xdepartment" id="xdepartment">
                                <option value=""> เลือกแผนก</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label>ฝ่าย <span>*</span></label>
                            <select class="form-control" name="xsection" id="xsection">
                                <option value=""> เลือกฝ่าย</option>
                            </select>
                        </div>
                        <div class="col-md-1">&nbsp;</div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-sm-1">
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <label>กิจกรรมโอที <span>*</span></label>
                            <?php echo Html::dropDownList('activity_id', null, $arrOTActivity, [
                                'id' => 'activity_id',
                                'prompt' => 'กรุณาเลือกกิจกรรม',
                                'class' => 'form-control',
                                'data-required' => 'true',
                            ]); ?>
                        </div>

                        <div class="col-sm-4">
                            <label>ประเภทการคิดโอที </label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_calculate_type" value="1" checked>
                                    <span>ปกติ</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_calculate_type" value="2">
                                    <span>วันหยุด</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_calculate_type" value="3">
                                    <span>นอกเวลาวันหยุด</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>วันที่ทำกิจกรรมโอที <span>*</span></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" readonly="readonly" name="ot_date" data-required="true" id="ot_date"
                                       value="<?php echo $today; ?>" class="form-control pull-right"
                                       placeholder="dd/mm/yyyy">
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>มีค่าเดินทาง <span>*</span></label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_route" value="1" id="openrouteprofile">
                                    <span>มี</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_route" value="0" id="closerouteprofile" checked>
                                    <span>ไม่มี</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>คิดค่าเดินทาง <span>*</span></label>
                            <?php
                            echo Html::dropDownList('profileroute_id', null, $arrOTProfileRoute, [
                                'id' => 'profileroute_id',
                                //'prompt' => 'กรุณาเลือกค่าเดินทาง',
                                'class' => 'form-control',
                                'data-required' => 'true',
                            ]); ?>
                        </div>
                        <div class="col-sm-2">
                            <label>จำนวนระยะทาง (กม.) <span>*</span></label>
                            <input type="text" class="form-control" id="ot_distance" name="ot_distance">
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>มีค่าที่พัก <span>*</span></label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_motel" value="1" id="openmotel">
                                    <span>มี</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_motel" value="0" id="closemotel" checked>
                                    <span>ไม่มี</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>จ่ายตามจริง (บาท) <span>*</span></label>
                            <input type="text" class="form-control"
                                   value="<?php echo ($OtConfig) ? $OtConfig['pay_motel'] : null; ?>"
                                   readonly="readonly" id="motel_price" name="motel_price">
                        </div>
                        <div class="col-sm-2">
                            <label>รอบเงินเดือน <span>*</span></label>
                            <input type="text" class="form-control" data-required="true" id="month_pay" name="month_pay" value="<?php echo date('m-Y');?>">
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                    <div class="row">
                        <div class="modal fade modal-wide" id="modalAdd" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                        <h4 id="myModalLabel" class="modal-title">เพิ่มข้อมูล</h4>
                                    </div>
                                    <form action="#" method="post">
                                        <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>"
                                               name="_csrf">
                                        <div class="modal-body" width="100%"
                                             class="table table-bordered table-hover dataTable">
                                            <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label">ชื่อ</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control">
                                                </div>
                                                <label for="genderEmp" class="col-sm-1 control-label">วันที่</label>
                                                <div class="col-sm-3">
                                                    <input type="date" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="genderEmp"
                                                       class="col-sm-2 control-label">เวลาเข้า</label>
                                                <div class="col-sm-3">
                                                    <input type="time" class="form-control">
                                                </div>
                                                <label for="genderEmp"
                                                       class="col-sm-1 control-label">เวลาออก</label>
                                                <div class="col-sm-3">
                                                    <input type="time" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="genderEmp"
                                                       class="col-sm-2 control-label">มีค่าเดินทาง:</label>
                                                <div class="col-sm-3">
                                                    <input type="radio" name="genderEmp" value="1">
                                                    มี
                                                    <input type="radio" name="genderEmp" value="2">
                                                    ไม่มี
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="genderEmp"
                                                       class="col-sm-2 control-label">คิดค่าเดินทาง</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                        <option>HBSO</option>
                                                    </select>
                                                </div>
                                                <label for="genderEmp"
                                                       class="col-sm-1 control-label">จำนวนระยะทาง</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="genderEmp"
                                                       class="col-sm-2 control-label">คิดค่าที่พัก</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                        <option>HBSO</option>
                                                    </select>
                                                </div>
                                                <label for="genderEmp"
                                                       class="col-sm-1 control-label">จ่ายตามจริงขั้น</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" value="">
                                                </div>

                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-primary" value="บันทึกข้อมูล">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                    </div>
                </div>


                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">รายชื่อพนักงานทำกิจกรรมโอที</h3>
                        <div class="pull-right" style="margin-bottom: 10px;">
                            <?php
                            echo Html::button('<i class="fa fa-plus-circle"></i> เพิ่มแถว',
                                [
                                    'class' => 'btn btn-success',
                                    'id' => 'btnAddNewRow',
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive" id="dvEmpList">

                                    <?php
                                    Pjax::begin(['id' => 'pjax_tbemployeeot']);

                                    ?>
                                    <table id="tbemployeeot" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 2%">ลำดับ</th>
                                            <th style="width: 18%">ชื่อ-สกุล</th>
                                            <th style="width: 12%">แผนก</th>
                                            <th style="width: 10%">เวลาเข้า OT</th>
                                            <th style="width: 10%">เวลาออก OT</th>
                                            <th style="width: 5%">รวมเวลา</th>
                                            <th style="width: 5%">รวมเงิน</th>
                                            <th style="width: 10%">เลือกค่าตอบแทน</th>
                                            <th style="width: 2%">ลบ</th>
                                        </tr>
                                        <thead>
                                        <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>
                                                <select class="form-control select2" style="width: 100%;"
                                                        id="otEmployee" name="otEmployee[]">
                                                </select>
                                            </td>
                                            <td><input type="text" id="txtDepartment" name="txtDepartment[]" readonly="readonly" class="form-control"></td>
                                            <td>
                                                <?php
                                                echo Html::dropDownList('start_hour[]', null, $arrHour, [
                                                    'id' => 'start_hour',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?> : <?php
                                                echo Html::dropDownList('start_minute[]', null, $arrMinute, [
                                                    'id' => 'start_minute',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?></td>
                                            <td>
                                                <?php
                                                echo Html::dropDownList('end_hour[]', null, $arrHour, [
                                                    'id' => 'end_hour',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?> : <?php
                                                echo Html::dropDownList('end_minute[]', null, $arrMinute, [
                                                    'id' => 'end_minute',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?></td>
                                            <td><input type="text" id="totaltime" name="totaltime[]" readonly="readonly" class="form-control"></td>
                                            <td><input type="text" id="totalmoney" name="totalmoney[]" readonly="readonly" class="form-control"></td>
                                            <td>
                                                <?php

                                                echo Html::dropDownList('otreturn_id[]', null, $arrOTReturn, [
                                                    'id' => 'otreturn_id',
                                                    'prompt' => 'ค่าตอบแทน',
                                                    'class' => 'form-control',
                                                    'data-required' => 'true',
                                                ]); ?>
                                                <input type="hidden" name="remark[]">
                                                <input type="hidden" name="wage[]">
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <?php
                                    Pjax::end();  //end pjax_tbemployeeot

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center" style="margin-bottom: 40px !important;">
                        <?php
                        echo Html::button('<i class="fa fa-save"></i> บันทึกข้อมูล',
                            [
                                'class' => 'btn btn-primary',
                                'id' => 'btnSaveManageOT',
                            ]);
                        ?>
                    </div>
                    <div style="height: 20px;"></div>
                </div>

            </div>
        </div>
        <!-- /.box -->
    </section><!-- /.content -->
    <span id="mode" title="add"></span>
</form>


