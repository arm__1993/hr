<?php

namespace app\modules\wscommondataold\models;

use Yii;

class Maincusdataold extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MainCusData';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Date_Receive', 'UpdateT', 'respon_date'], 'safe'],
            [['CusNo_Old', 'BeType', 'company_type', 'cus_branch_id', 'cus_branch_txt', 'Cus_Big_Picture', 'Cus_Small_Picture', 'mary_id', 'C_copy', 'C_GEO', 'Cus_Coppy', 'Cus_GEO', 'JobID', 'JobSegmentGroup', 'W_Coppy', 'W_GEO', 'Resperson', 'Resperson_posCode', 'respon_date', 'Time_for_talk', 'Pleasure_value', 'rcheck', 'cleansing_status', 'cleansing_date', 'status_up'], 'required'],
            [['Picture', 'C_Add', 'Cus_Add', 'Job_Des', 'W_Add', 'Remark'], 'string'],
            [['CusNo_Old', 'Be', 'mary_id', 'C_AddGPSLatitude', 'C_AddGPSLongtitude', 'Cus_AddGPSLatitude', 'Cus_AddGPSLongtitude', 'Cus_WGPSLatitude', 'Cus_WGPSLongtitude', 'Email', 'Time_for_talk'], 'string', 'max' => 100],
            [['BeType', 'C_Code', 'Cus_Code', 'W_Code'], 'string', 'max' => 5],
            [['company_type', 'rcheck'], 'string', 'max' => 1],
            [['Cus_Name'], 'string', 'max' => 200],
            [['Cus_Surename', 'JobID', 'Title', 'W_Email', '1stSourceData'], 'string', 'max' => 50],
            [['Cus_Nickname', 'Status', 'W_Fax', 'Resperson'], 'string', 'max' => 15],
            [['cus_branch_id', 'cus_branch_txt', 'C_AddMapDescribe', 'Cus_AddMapDescribe', 'Cus_WMapDescribe'], 'string', 'max' => 250],
            [['Cus_Big_Picture', 'Cus_Small_Picture'], 'string', 'max' => 255],
            [['Sex', 'Cus_Tum', 'Cus_Type', 'Pleasure_value'], 'string', 'max' => 20],
            [['DateOfBirth', 'C_copy', 'Cus_Coppy', 'W_Coppy'], 'string', 'max' => 10],
            [['Cus_IDNo', 'Data_Received_Num', 'UpdaterNO'], 'string', 'max' => 13],
            [['Primary_Edu', 'Secondary_Edu', 'Highschool_Edu', 'Bachelor', 'Master', 'Phd', 'C_Vill', 'C_Tum', 'C_Amp', 'C_Pro', 'Cus_Vil', 'Cus_Amp', 'Job', 'W_Vill', 'W_Tum', 'W_Amp', 'W_Pro', 'Resperson_posCode', 'cleansing_status', 'cleansing_date', 'status_up'], 'string', 'max' => 30],
            [['C_GEO', 'Cus_Pro', 'Cus_GEO', 'W_GEO'], 'string', 'max' => 25],
            [['NotMail'], 'string', 'max' => 3],
            [['H_Tel', 'JobSegmentGroup', 'W_Tel', 'Pagger', 'M_Tel'], 'string', 'max' => 150],
            [['W_Name'], 'string', 'max' => 80],
            [['Data_Received', 'Updater'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Date_Receive' => ' วันที่รับข้อมูล',
            'CusNo' => 'Cus No',
            'CusNo_Old' => '๏ฟฝ๏ฟฝ๏ฟฝ๏ฟฝ๏ฟฝลข๏ฟฝูก๏ฟฝ๏ฟฝ๏ฟฝ',
            'Picture' => 'รูปภาพลูกค้า',
            'Be' => 'คำนำหน้า',
            'BeType' => '1= บุคคล , 2 = ชมรม / สมาคม / นิติบุคล',
            'company_type' => '1=สำนักงานใหญ่,2=สาขา',
            'Cus_Name' => 'ชื่อ',
            'Cus_Surename' => 'นามสกุล',
            'Cus_Nickname' => 'ชื่อเล่น',
            'cus_branch_id' => 'เลขสาขาลูกค้า',
            'cus_branch_txt' => 'ชื่อสาขาลูกค้า',
            'Cus_Big_Picture' => 'ภาพขนาดใหญ่',
            'Cus_Small_Picture' => 'ภาพขนาดย่อ',
            'Sex' => 'เพศ',
            'DateOfBirth' => 'วันเกิด',
            'Cus_IDNo' => 'เลขบัตรประชาชน',
            'Primary_Edu' => 'ประถมศึกษา',
            'Secondary_Edu' => 'ม.ต้น',
            'Highschool_Edu' => 'ม.ปลาย / ปวช.',
            'Bachelor' => 'ปริญญาตรี',
            'Master' => 'ปริญญาโท',
            'Phd' => 'ปริญญาเอก',
            'Status' => 'สถานะ',
            'mary_id' => 'รหัสคู่สมรส',
            'C_copy' => 'C Copy',
            'C_Add' => 'ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Vill' => 'หมู่บ้านที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Tum' => 'ตำบล ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Amp' => 'อำเภอ ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Pro' => 'จังหวัด ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_GEO' => 'ภาค',
            'C_Code' => 'รหัสไปรษณีย์',
            'C_AddGPSLatitude' => 'ที่อยู่ทางการ GPS ละติจูด',
            'C_AddGPSLongtitude' => 'ที่อยู่ทางการ GPS ลองติจูด',
            'C_AddMapDescribe' => 'คำอธิบายแผนที่',
            'Cus_Coppy' => 'Cus  Coppy',
            'Cus_Add' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Vil' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Tum' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Amp' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Pro' => 'ที่อยู่ส่งเอกสาร',
            'Cus_GEO' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Code' => 'ที่อยู่ส่งเอกสาร',
            'Cus_AddGPSLatitude' => 'ที่อยู่ส่งเอกสาร GPS ละติจูด',
            'Cus_AddGPSLongtitude' => 'ที่อยู่ส่งเอกสาร GPS ลองติจูด',
            'Cus_AddMapDescribe' => 'คำอธิบายแผนที่',
            'NotMail' => 'ไม่ส่งจดหมาย 1=Yes/No',
            'H_Tel' => 'โทรศัพท์บ้าน',
            'JobID' => 'refer job id',
            'Job' => 'อาชีพ',
            'JobSegmentGroup' => 'กลุ่มอาชีพ',
            'Job_Des' => 'ส่วนขยายอาชีพ',
            'W_Name' => 'ชื่อที่ทำงาน',
            'Title' => 'ตำแหน่ง',
            'W_Coppy' => 'W  Coppy',
            'W_Add' => 'ที่ทำงาน',
            'W_Vill' => 'ที่ทำงาน',
            'W_Tum' => 'ที่ทำงาน',
            'W_Amp' => 'ที่ทำงาน',
            'W_Pro' => 'ที่ทำงาน',
            'W_GEO' => 'ที่ทำงาน',
            'W_Code' => 'ที่ทำงาน',
            'W_Tel' => 'ที่ทำงาน',
            'W_Fax' => 'ที่ทำงาน',
            'W_Email' => 'ที่ทำงาน',
            'Cus_WGPSLatitude' => 'ที่อยู่ส่งเอกสารที่ทำงาน GPS ละติจูด',
            'Cus_WGPSLongtitude' => 'ที่อยู่ส่งเอกสาร GPS ลองติจูด',
            'Cus_WMapDescribe' => 'คำอธิบายแผนที่',
            'Pagger' => 'เพจเจอร์',
            'M_Tel' => 'มือถือ',
            'Email' => 'Email',
            'Cus_Type' => 'ชนิดลูกค้า',
            'Data_Received' => 'ผู้รับข้อมูล',
            'Data_Received_Num' => 'ผู้รับข้อมูลNo',
            '1stSourceData' => 'แหล่งข้อมูลครั้งแรก',
            'Remark' => 'หมายเหตุ',
            'Updater' => 'ผู้ป้อนข้อมูล',
            'UpdaterNO' => 'ผู้ป้อนข้อมูล ID',
            'UpdateT' => 'วันที่Update',
            'Resperson' => 'ผู้รับผิดชอบลูกค้า',
            'Resperson_posCode' => 'รหัสตำแหน่งผู้รับผิดชอบลูกค้า',
            'respon_date' => 'วันที่เริ่มดูแลลูกค้า',
            'Time_for_talk' => 'เวลาที่ลูกค้าสะดวกคุย',
            'Pleasure_value' => 'ค่าความพึงพอใจ',
            'rcheck' => 'Rcheck',
            'cleansing_status' => 'Cleansing Status',
            'cleansing_date' => 'Cleansing Date',
            'status_up' => 'Status Up',
        ];
    }
     public function insertmaincusdataold($Be,
                                          $BeType,
                                          $Cus_Name,
                                          $Cus_Surename,
                                          $Cus_Nickname,
                                          $Sex,
                                          $DateOfBirth,
                                          $Cus_IDNo_Type,
                                          $Cus_IDNo,
                                          $Married_Status,
                                          $Date_Receive,
                                          $Data_Received,
                                          $Data_Received_Num,
                                          $stSourceData,
                                          $Remark,
                                          $Updater,
                                          $UpdaterNO,
                                          $UpdateT,
                                          $company_type,
                                          $cus_branch_id,
                                          $cus_branch_txt)
    {
 
        $sql = "insert into `MainCusData`(`Date_Receive`,
                                         `CusNo`,
                                         `CusNo_Old`,
                                         `Picture`,
                                         `Be`,
                                         `BeType`, 
                                         `company_type`, 
                                         `Cus_Name`, 
                                         `Cus_Surename`, 
                                         `Cus_Nickname`, 
                                         `cus_branch_id`, 
                                         `cus_branch_txt`, 
                                         `Cus_Big_Picture`, 
                                         `Cus_Small_Picture`, 
                                         `Sex`, 
                                         `DateOfBirth`, 
                                         `Cus_IDNo`, 
                                         `Primary_Edu`, 
                                         `Secondary_Edu`, 
                                         `Highschool_Edu`, 
                                         `Bachelor`, 
                                         `Master`, 
                                         `Phd`, 
                                         `Status`, 
                                         `mary_id`, 
                                         `C_copy`, 
                                         `C_Add`, 
                                         `C_Vill`, 
                                         `C_Tum`, 
                                         `C_Amp`, 
                                         `C_Pro`, 
                                         `C_GEO`, 
                                         `C_Code`, 
                                         `C_AddGPSLatitude`, 
                                         `C_AddGPSLongtitude`, 
                                         `C_AddMapDescribe`, 
                                         `Cus_Coppy`, 
                                         `Cus_Add`, 
                                         `Cus_Vil`, 
                                         `Cus_Tum`, 
                                         `Cus_Amp`, 
                                         `Cus_Pro`, 
                                         `Cus_GEO`, 
                                         `Cus_Code`, 
                                         `Cus_AddGPSLatitude`, 
                                         `Cus_AddGPSLongtitude`, 
                                         `Cus_AddMapDescribe`, 
                                         `NotMail`, 
                                         `H_Tel`, 
                                         `JobID`, 
                                         `Job`, 
                                         `JobSegmentGroup`, 
                                         `Job_Des`, 
                                         `W_Name`, 
                                         `Title`, 
                                         `W_Coppy`, 
                                         `W_Add`, 
                                         `W_Vill`, 
                                         `W_Tum`, 
                                         `W_Amp`, 
                                         `W_Pro`, 
                                         `W_GEO`, 
                                         `W_Code`, 
                                         `W_Tel`, 
                                         `W_Fax`, 
                                         `W_Email`, 
                                         `Cus_WGPSLatitude`, 
                                         `Cus_WGPSLongtitude`, 
                                         `Cus_WMapDescribe`, 
                                         `Pagger`, 
                                         `M_Tel`, 
                                         `Email`, 
                                         `Cus_Type`, 
                                         `Data_Received`, 
                                         `Data_Received_Num`, 
                                         `1stSourceData`, 
                                         `Remark`, 
                                         `Updater`, 
                                         `UpdaterNO`, 
                                         `UpdateT`, 
                                         `Resperson`, 
                                         `Resperson_posCode`, 
                                         `respon_date`, 
                                         `Time_for_talk`, 
                                         `Pleasure_value`, 
                                         `rcheck`, 
                                         `cleansing_status`, 
                                         `cleansing_date`, 
                                         `status_up`) 
                                VALUES ( :Date_Receive,
                                         NULL,
                                         '',
                                         '',
                                         :Be,
                                         :BeType, 
                                         :company_type, 
                                         :Cus_Name, 
                                         :Cus_Surename, 
                                         :Cus_Nickname, 
                                         :cus_branch_id, 
                                         :cus_branch_txt, 
                                         '',
                                         '', 
                                         :Sex, 
                                         :DateOfBirth, 
                                         :Cus_IDNo, 
                                         :Primary_Edu, 
                                         :Secondary_Edu, 
                                         :Highschool_Edu, 
                                         :Bachelor, 
                                         :Master, 
                                         :Phd, 
                                         :Status, 
                                         :mary_id, 
                                         :C_copy, 
                                         :C_Add, 
                                         :C_Vill, 
                                         :C_Tum, 
                                         :C_Amp, 
                                         :C_Pro, 
                                         :C_GEO, 
                                         :C_Code, 
                                         :C_AddGPSLatitude, 
                                         :C_AddGPSLongtitude, 
                                         :C_AddMapDescribe, 
                                         :Cus_Coppy, 
                                         :Cus_Add, 
                                         :Cus_Vil, 
                                         :Cus_Tum, 
                                         :Cus_Amp, 
                                         :Cus_Pro, 
                                         :Cus_GEO, 
                                         :Cus_Code, 
                                         :Cus_AddGPSLatitude, 
                                         :Cus_AddGPSLongtitude, 
                                         :Cus_AddMapDescribe, 
                                         :NotMail, 
                                         :H_Tel, 
                                         :JobID, 
                                         :Job, 
                                         :JobSegmentGroup, 
                                         :Job_Des, 
                                         :W_Name, 
                                         :Title, 
                                         :W_Coppy, 
                                         :W_Add, 
                                         :W_Vill, 
                                         :W_Tum, 
                                         :W_Amp, 
                                         :W_Pro, 
                                         :W_GEO, 
                                         :W_Code, 
                                         :W_Tel, 
                                         :W_Fax, 
                                         :W_Email, 
                                         :Cus_WGPSLatitude, 
                                         :Cus_WGPSLongtitude, 
                                         :Cus_WMapDescribe, 
                                         :Pagger, 
                                         :M_Tel, 
                                         :Email, 
                                         :Cus_Type, 
                                         :Data_Received, 
                                         :Data_Received_Num, 
                                         :1stSourceData, 
                                         :Remark, 
                                         '2016-09-17', 
                                         :UpdaterNO, 
                                         :UpdateT, 
                                         :Resperson, 
                                         :Resperson_posCode, 
                                         :respon_date, 
                                         :Time_for_talk, 
                                         :Pleasure_value, 
                                         '', 
                                         '', 
                                         '', 
                                         '')";

        Yii::$app->dbERP_maincusdata
       ->createCommand($sql)
       ->bindParam(':Date_Receive',$Date_Receive)
       
       ->bindParam(':CusNo_Old',$CusNo_Old)
       ->bindParam(':Picture',$Picture)
       ->bindParam(':Be',$Be)
       ->bindParam(':BeType',$BeType)
       ->bindParam(':company_type',$company_type)
       ->bindParam(':Cus_Name',$Cus_Name)
       ->bindParam(':Cus_Surename',$Cus_Surename)
       ->bindParam(':Cus_Nickname',$Cus_Nickname)
       ->bindParam(':cus_branch_id',$cus_branch_id)
       ->bindParam(':cus_branch_txt',$cus_branch_txt)
       ->bindParam(':Cus_Big_Picture',$Cus_Big_Picture)
       ->bindParam(':Cus_Small_Picture',$Cus_Small_Picture)
       ->bindParam(':Sex',$Sex)
       ->bindParam(':DateOfBirth',$DateOfBirth)
       ->bindParam(':Cus_IDNo',$Cus_IDNo)
       ->bindParam(':Primary_Edu',$Primary_Edu)
       ->bindParam(':Secondary_Edu',$Secondary_Edu)
       ->bindParam(':Highschool_Edu',$Highschool_Edu)
       ->bindParam(':Bachelor',$Bachelor)
       ->bindParam(':Master',$Master)
       ->bindParam(':Phd',$Phd)
       ->bindParam(':Status',$Married_Status)
       ->bindParam(':mary_id',$mary_id)
       ->bindParam(':C_copy',$C_copy)
       ->bindParam(':C_Add',$C_Add)
       ->bindParam(':C_Vill',$C_Vill)
       ->bindParam(':C_Tum',$C_Tum)
       ->bindParam(':C_Amp',$C_Amp)
       ->bindParam(':C_Pro',$C_Pro)
       ->bindParam(':C_GEO',$C_GEO)
       ->bindParam(':C_Code',$C_Code)
       ->bindParam(':C_AddGPSLatitude',$C_AddGPSLatitude)
       ->bindParam(':C_AddGPSLongtitude',$C_AddGPSLongtitude)
       ->bindParam(':C_AddMapDescribe',$C_AddMapDescribe)
       ->bindParam(':Cus_Coppy',$Cus_Coppy)
       ->bindParam(':Cus_Add',$Cus_Add)
       ->bindParam(':Cus_Vil',$Cus_Vil)
       ->bindParam(':Cus_Tum',$Cus_Tum)
       ->bindParam(':Cus_Amp',$Cus_Amp)
       ->bindParam(':Cus_Pro',$Cus_Pro)
       ->bindParam(':Cus_GEO',$Cus_GEO)
       ->bindParam(':Cus_Code',$Cus_Code)
       ->bindParam(':Cus_AddGPSLatitude',$Cus_AddGPSLatitude)
       ->bindParam(':Cus_AddGPSLongtitude',$Cus_AddGPSLongtitude)
       ->bindParam(':Cus_AddMapDescribe',$Cus_AddMapDescribe)
       ->bindParam(':NotMail',$NotMail)
       ->bindParam(':H_Tel',$H_Tel)
       ->bindParam(':JobID',$JobID)
       ->bindParam(':Job',$Job)
       ->bindParam(':JobSegmentGroup',$JobSegmentGroup)
       ->bindParam(':Job_Des',$Job_Des)
       ->bindParam(':W_Name',$W_Name)
       ->bindParam(':Title',$Title)
       ->bindParam(':W_Coppy',$W_Coppy)
       ->bindParam(':W_Add',$W_Add)
       ->bindParam('W_Vill',$W_Vill)
       ->bindParam('W_Tum',$W_Tum)
       ->bindParam('W_Amp',$W_Amp)
       ->bindParam('W_Pro',$W_Pro)
       ->bindParam('W_GEO',$W_GEO)
       ->bindParam('W_Code',$W_Code)
       ->bindParam('W_Tel',$W_Tel)
       ->bindParam('W_Fax',$W_Fax)
       ->bindParam('W_Email',$W_Email)
       ->bindParam('Cus_WGPSLatitude',$Cus_WGPSLatitude)
       ->bindParam('Cus_WGPSLongtitude',$Cus_WGPSLongtitude)
       ->bindParam('Cus_WMapDescribe',$Cus_WMapDescribe)
       ->bindParam('Pagger',$Pagger)
       ->bindParam('M_Tel',$M_Tel)
       ->bindParam('Email',$Email)
       ->bindParam('Cus_Type',$Cus_Type)
       ->bindParam('Data_Received',$Data_Received)
       ->bindParam('Data_Received_Num',$Data_Received_Num)
       ->bindParam('1stSourceData',$stSourceData)
       ->bindParam('Remark',$Remark)
       ->bindParam('Updater',$Updater)
       ->bindParam('UpdaterNO',$UpdaterNO)
       ->bindParam('UpdateT',$UpdateT)
       ->bindParam('Resperson',$Resperson)
       ->bindParam('Resperson_posCode',$Resperson_posCode)
       ->bindParam('respon_date',$respon_date)
       ->bindParam('Time_for_talk',$Time_for_talk)
       ->bindParam('Pleasure_value',$Pleasure_value)
       ->bindParam('rcheck',$rcheck)
       ->bindParam('cleansing_status',$cleansing_status)
       ->bindParam('cleansing_date',$cleansing_date)
       ->bindParam('status_up',$status_up)
       ->execute();
        $LastId = Yii::$app->dbERP_maincusdata->getLastInsertID();
        return $LastId;
    }

}
