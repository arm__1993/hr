<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "tax_witholding".
 *
 * @property integer $id
 * @property string $emp_idcard
 * @property integer $years
 * @property string $date_pay
 * @property integer $company_id
 * @property string $company_name
 * @property string $position_code
 * @property string $position_name
 * @property string $salary_wage_monthly
 * @property string $total_adddeduct
 * @property string $total_income
 * @property string $wht_month_amount
 * @property string $wht_cumulative_amount
 * @property string $createby_user
 * @property string $create_datetime
 */
class TaxWitholding extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_witholding';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['years', 'company_id'], 'integer'],
            [['salary_wage_monthly', 'total_adddeduct', 'total_income', 'wht_month_amount', 'wht_cumulative_amount'], 'number'],
            [['create_datetime'], 'safe'],
            [['emp_idcard', 'createby_user'], 'string', 'max' => 13],
            [['date_pay'], 'string', 'max' => 7],
            [['company_name'], 'string', 'max' => 255],
            [['position_code'], 'string', 'max' => 30],
            [['position_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_idcard' => 'Emp Idcard',
            'years' => 'Years',
            'date_pay' => 'Date Pay',
            'company_id' => 'Company ID',
            'company_name' => 'Company Name',
            'position_code' => 'Position Code',
            'position_name' => 'Position Name',
            'salary_wage_monthly' => 'Salary Wage Monthly',
            'total_adddeduct' => 'Total Adddeduct',
            'total_income' => 'Total Income',
            'wht_month_amount' => 'Wht Month Amount',
            'wht_cumulative_amount' => 'Wht Cumulative Amount',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
        ];
    }
}
