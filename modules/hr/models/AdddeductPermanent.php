<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ADD_DEDUCT_PERMANENT".
 *
 * @property integer $ID
 * @property integer $ADD_DEDUCT_TEMPLATE_ID
 * @property integer $TEMPLATE_TYPE_ID
 * @property string $ADD_DEDUCT_DETAIL
 * @property string $ADD_DEDUCT_DETAIL_START_USE_DATE
 * @property string $ADD_DEDUCT_DETAIL_END_USE_DATE
 * @property string $ADD_DEDUCT_DETAIL_EMP_ID
 * @property string $ADD_DEDUCT_DETAIL_AMOUNT
 * @property integer $ADD_DEDUCT_DETAIL_TYPE
 * @property integer $ADD_DEDUCT_DETAIL_STATUS
 * @property string $ADD_DEDUCT_DETAIL_CREATE_DATE
 * @property string $ADD_DEDUCT_DETAIL_CREATE_BY
 * @property string $ADD_DEDUCT_DETAIL_UPDATE_BY
 * @property string $ADD_DEDUCT_DETAIL_UPDATE_DATE
 */
class AdddeductPermanent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ADD_DEDUCT_PERMANENT';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ADD_DEDUCT_TEMPLATE_ID', 'TEMPLATE_TYPE_ID', 'ADD_DEDUCT_DETAIL_TYPE', 'ADD_DEDUCT_DETAIL_STATUS'], 'integer'],
            [['ADD_DEDUCT_DETAIL_START_USE_DATE', 'ADD_DEDUCT_DETAIL_END_USE_DATE', 'ADD_DEDUCT_DETAIL_CREATE_DATE', 'ADD_DEDUCT_DETAIL_UPDATE_DATE'], 'safe'],
            [['ADD_DEDUCT_DETAIL_AMOUNT'], 'number'],
            [['ADD_DEDUCT_DETAIL'], 'string', 'max' => 250],
            [['ADD_DEDUCT_DETAIL_EMP_ID', 'ADD_DEDUCT_DETAIL_CREATE_BY', 'ADD_DEDUCT_DETAIL_UPDATE_BY'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ADD_DEDUCT_TEMPLATE_ID' => 'Add  Deduct  Template  ID',
            'TEMPLATE_TYPE_ID' => 'Template  Type  ID',
            'ADD_DEDUCT_DETAIL' => 'Add  Deduct  Detail',
            'ADD_DEDUCT_DETAIL_START_USE_DATE' => 'Add  Deduct  Detail  Start  Use  Date',
            'ADD_DEDUCT_DETAIL_END_USE_DATE' => 'Add  Deduct  Detail  End  Use  Date',
            'ADD_DEDUCT_DETAIL_EMP_ID' => 'Add  Deduct  Detail  Emp  ID',
            'ADD_DEDUCT_DETAIL_AMOUNT' => 'Add  Deduct  Detail  Amount',
            'ADD_DEDUCT_DETAIL_TYPE' => 'Add  Deduct  Detail  Type',
            'ADD_DEDUCT_DETAIL_STATUS' => 'Add  Deduct  Detail  Status',
            'ADD_DEDUCT_DETAIL_CREATE_DATE' => 'Add  Deduct  Detail  Create  Date',
            'ADD_DEDUCT_DETAIL_CREATE_BY' => 'Add  Deduct  Detail  Create  By',
            'ADD_DEDUCT_DETAIL_UPDATE_BY' => 'Add  Deduct  Detail  Update  By',
            'ADD_DEDUCT_DETAIL_UPDATE_DATE' => 'Add  Deduct  Detail  Update  Date',
        ];
    }
}
