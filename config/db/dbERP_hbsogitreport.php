<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host=$DB_HOST;dbname=".$params['dbConn']['hbso_gitreport']."",
    'username' => $DB_USER,
    'password' => $DB_PWD,
    'charset' => 'utf8',
];