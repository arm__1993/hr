-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 17, 2017 at 04:43 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `wage_calculate`
--

CREATE TABLE `wage_calculate` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'PK',
  `wage_calculate_name` varchar(250) NOT NULL COMMENT 'ชื่อการคำนวณเงินเดือนระหว่างเดือน',
  `pay_date` char(7) NOT NULL COMMENT 'เดือนที่จ่าย',
  `is_cal_salary` tinyint(3) DEFAULT NULL COMMENT 'ระบุการคำนวณเงินเดือนทั้งเดือนด้วย',
  `is_cal_day` tinyint(3) DEFAULT NULL COMMENT 'ระบุการคำนวณเงินเดือนแบบรายวัน',
  `cal_date_start` date DEFAULT NULL COMMENT 'หากมีการระบุคำนวณเงินเดือนรายวันให้เก็บวันเริ่มต้น',
  `cal_date_stop` date DEFAULT NULL COMMENT 'หากมีการระบุคำนวณเงินเดือนรายวันให้เก็บวันสิ้นสุด',
  `emp_idcard` mediumtext COMMENT 'รหัส ID Card  พนักงานที่คำนวณตามเงื่อนไข คั่นด้วย comma',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ 1=actived,  0=inactived, 2=transaction complete, 99=delete',
  `create_by` char(13) DEFAULT NULL COMMENT 'สร้างรายการโดย',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันเวลาที่สร้างรายการ',
  `approved_by` char(13) DEFAULT NULL COMMENT 'อัพเดทโดย',
  `approved_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่อัพเดท'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wage_calculate`
--
ALTER TABLE `wage_calculate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pay_date` (`pay_date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wage_calculate`
--
ALTER TABLE `wage_calculate`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
