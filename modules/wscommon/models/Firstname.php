<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "first_name".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $who_input
 * @property string $position_input
 * @property string $time_input
 * @property string $who_edit
 * @property string $position_edit
 * @property string $time_edit
 */
class Firstname extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'first_name';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'who_input', 'position_input', 'time_input', 'who_edit', 'position_edit', 'time_edit'], 'required'],
            [['time_input', 'time_edit'], 'safe'],
            [['first_name'], 'string', 'max' => 100],
            [['who_input', 'position_input', 'who_edit', 'position_edit'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'who_input' => 'ผู้สร้างข้อมูล',
            'position_input' => 'positioncode ของผู้บันทึกขั้อมูล',
            'time_input' => 'วันที่และเวลาที่สร้างข้อมูล',
            'who_edit' => 'ผู้แก้ไขข้อมูล',
            'position_edit' => 'positioncode ของผู้แก้ไขข้อมูล',
            'time_edit' => 'วันที่และเวลาที่แก้ไขข้อมูล',
        ];
    }


    public function searchbename($firstname) // param
    {   
         
 
        $datacustomer = Yii::$app->dbERP_maincusdata
       ->createCommand("select * from first_name ")
       ->bindParam(':firstname',$firstname)
       ->queryAll();

      // echo $sql;
        return $datacustomer;

    }

    
}
