-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 26, 2017 at 05:41 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-10+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_money_bonds_tel`
--

CREATE TABLE `emp_money_bonds_tel` (
  `id` int(11) NOT NULL,
  `emp_data_id` int(11) DEFAULT NULL,
  `emp_money_bonds_id` int(11) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `main_active` tinyint(1) DEFAULT NULL COMMENT '1:เบอร์โทรหลัก'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_money_bonds_tel`
--
ALTER TABLE `emp_money_bonds_tel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_data_id` (`emp_data_id`),
  ADD KEY `emp_money_bonds_id` (`emp_money_bonds_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_money_bonds_tel`
--
ALTER TABLE `emp_money_bonds_tel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
