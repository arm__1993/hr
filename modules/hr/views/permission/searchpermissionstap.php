<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:27
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
$selectProgram = ApiHr::selectProgram();
$selectworking = ApiHr::getWorking_company();

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/permission.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/js/jquery.sumoselect.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/permission/sumoselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/menulink.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/menulink.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
?>
<style>
    .tree {
        min-height:20px;
        padding:19px;
        margin-bottom:20px;
        background-color:#fbfbfb;
        border:1px solid #999;
        -webkit-border-radius:4px;
        -moz-border-radius:4px;
        border-radius:4px;
        -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
        -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
        box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
    }
    .tree li {
        list-style-type:none;
        margin:0;
        padding:10px 5px 0 5px;
        position:relative
    }
    .tree li::before, .tree li::after {
        content:'';
        left:-20px;
        position:absolute;
        right:auto
    }
    .tree li::before {
        border-left:1px solid #999;
        bottom:50px;
        height:100%;
        top:0;
        width:1px
    }
    .tree li::after {
        border-top:1px solid #999;
        height:20px;
        top:25px;
        width:25px
    }
    .tree li span {
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
        border:1px solid #999;
        border-radius:5px;
        display:inline-block;
        padding:3px 8px;
        text-decoration:none
    }
    .tree li.parent_li>span {
        cursor:pointer
    }
    .tree>ul>li::before, .tree>ul>li::after {
        border:0
    }
    .tree li:last-child::before {
        height:30px
    }
    .tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
        background:#eee;
        border:1px solid #94a0b4;
        color:#000
}
</style>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
       
        <div class="box-body">

            <div class="tree well">
            <?php
            foreach($resultParent as $program_id => $menu1)
            {
               
              // $parent_key = array_keys($menu1);
               
                ?>
                    <ul>
                        <li>
                            <span><i class="icon-folder-open"></i><?php echo $arrProgram[$program_id]['name']; ?> </span> <a href=""></a>
                            <div class="col-sm-12" id="manage_menu_list" style="padding-top:20px">
                                <div class="pre-hole-text-load">
                            
                                    รอสักครู่..
                                    
                                </div>
                            </div>
                        </li>
                    </ul>
                    <?php            
             } 
             ?>
            </div>
        </div>
    </div>

    <!-- /.box -->
</section><!-- /.content -->
<div id='dataserat'>
</div>
<script>
    $(function () {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
        $('.tree li.parent_li > span').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
            }
            e.stopPropagation();
        });
    });
</script>