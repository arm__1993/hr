<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 5/19/2017 AD
 * Time: 13:59
 */

return [
    'adminEmail' => 'admin@example.com',
    'TYPE_DECIMAL'=>2,
    'PAGE_SIZE'=>10,
    'PAGE_SIZE_REPORT'=>50,
    'DELETE_VALUE'=>-1,
    'DELETE_LOG_TYPE'=>'DELETE',
    'UPDATE_LOG_TYPE'=>'UPDATE',
    'INSERT_LOG_TYPE'=>'INSERT',
    'ACTIVE_STATUS'=>'1',
    'INACTIVE_STATUS'=>'0',
];