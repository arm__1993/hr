<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:22
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use kartik\date\DatePicker;


use app\modules\hr\apihr\ApiHr;

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/paysliphr.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hr-config.js?t=' . time());
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/MonthPicker.min.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/MonthPicker.min.css");


$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>ออกรายงาน</li>
                <li class="active">สลิปเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="row">
                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4 text-right">
                        <label>เลือกประเภท</label>
                        <div class="btn-group">
                            <select class="form-control" name="selectTypeSeach" id="selectTypeSeach"
                                    onchange="getSelectType(this);">
                                <option>เลือกประเภท</option>
                                <option value="1">เลือกบริษัท</option>
                                <option value="2">เลือกบุคคล</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>รอบเงินเดือน</label>
                        <div class="btn-group">
                            <input type="text" class="form-control" name="date_pay" id="date_pay"
                                   value="<?php echo date('m-Y'); ?>">
                        </div>
                    </div>
                    <div class="col-md-2"></div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-12" style="display: none;" id="showCompany">
                        <center>
                            <label>บริษัท</label>
                            &nbsp
                            <div class="btn-group">
                                <select class="form-control" name="selectworking" id="selectworking"
                                        onchange="getCompanyForDepartment(this);" ;>
                                    <option value="">เลือกบริษัท</option>
                                    <?php $working = ApiHr::getWorking_company();
                                    foreach ($working as $value) {
                                        echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                    } ?>
                                </select>
                                <span id="urlGetDepartment"
                                      title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                            </div>
                            &nbsp;&nbsp;
                            <label>แผนก</label>
                            <div class="btn-group">
                                <select class="form-control" name="selectdepartment" id="selectdepartment"
                                        onchange="getDepartmentForSection(this);">
                                    <option value=""> เลือกแผนก</option>
                                </select>
                                <span id="urlGetSection"
                                      title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                            </div>
                            &nbsp;&nbsp;
                            <label>ฝ่าย</label>
                            <div class="btn-group">
                                <select class="form-control" name="selectsection" id="selectsection">
                                    <option value=""> เลือกฝ่าย</option>
                                </select>
                            </div>&nbsp;
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="display: none;" id="showEmp">
                        <center>
                            <label>เลือกพนักงาน</label>
                            &nbsp
                            <div class="btn-group">
                                <select class="form-control" name="id_emp" id="id_emp" style="width: 250px !important;">
                                    <?php
                                    $arrList = ApiHr::getempdataall();
                                    foreach ($arrList as $key => $value) {
                                        echo '<option value="' . $value['value'] . '">' . $value['label'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </center>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-12">
                        <center>
                            <div class="btn-group">
                                <button type="button" id="saveAdddeduct" class="btn btn-block btn-primary btn-sm">
                                    ค้นหา
                                </button>
                            </div>
                            <div class="btn-group">
                                <button type="reset" class="btn btn-block btn-danger btn-sm">ล้างข้อมูล</button>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->