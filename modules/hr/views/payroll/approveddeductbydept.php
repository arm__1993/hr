<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/28/2017 AD
 * Time: 14:36
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.config.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/approveddeductbydept.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>รายการหักจากหน่วยงาน</li>
                <li class="active">อนุมัติรายการหัก</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
         <div class="box-body">
              <form class="form-horizontal">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ข้อมูลรายการ</h3>
                    </div>
                        <div class="box-body">
                            <div class="row">
                                    <div class="col-md-1">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="numberPassportEmp"  class="col-sm-4 control-label">บริษัท</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" name="selectworking" id="selectworking"
                                                            onchange="getCompanyForDepartment(this);" ;>
                                                        <option value="">เลือกบริษัท</option>
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                    </select>
                                        <span id="urlGetDepartment" title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="numberPassportEmp"  class="col-sm-3 control-label">แผนก</label>
                                                <div class="col-sm-9">
                                                     <select class="form-control" name="selectdepartment" id="selectdepartment"
                                                            onchange="getDepartmentForSection(this);">
                                                        <option value=""> เลือกแผนก</option>
                                                     </select>
                                                    <span id="urlGetSection" title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="numberPassportEmp"  class="col-sm-3 control-label">ฝ่าย</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control" name="selectsection" id="selectsection">
                                                        <option value=""> เลือกฝ่าย</option>
                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-1">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="monthselect"  class="col-sm-4 control-label">รอบเงินเดือน</label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control" id="monthselect" value="">
                                                </div>
                                        </div>
                                    </div><div class="col-md-3">
                                    <div class="form-group">
                                        <label for="selectdeduct"  class="col-sm-3 control-label">รายการหัก</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="add_deduct_template_id"
                                                    id="add_deduct_template_id">
                                                <option value="0">ทั้งหมด</option>
                                                <?php
                                                $arrList = ApiPayroll::getDataDeductTemp();
                                                foreach ($arrList as $key => $value) {
                                                    echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    &nbsp;
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn  btn-primary btn-sm" id="seachdatalist"><i class="fa fa-search"></i> ค้นหา</button>
                                        <button type="reset" class="btn  btn-danger btn-sm"><i class="fa fa-reply"></i> ล้างข้อมูล</button>
                                    </div>
                                </div>
                            </div>
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">แสดงรายการ</h3>
                                </div>
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover dataTable" >
                                            <thead>
                                                 <tr>
                                                    <th width="5%" style="text-align: center"><input type="checkbox" id="checkAll">ทั้งหมด</th>
                                                    <th width="10%" style="text-align: center">รอบเงินเดือน</th>
                                                    <th width="15%">ชื่อ-สกุล(ผู้หัก)</th>
                                                    <th width="15%">ชื่อรายการ</th>
                                                    <th width="10%" style="text-align: right">จำนวน</th>
                                                    <th width="15%">รายละเอียด</th>
                                                    <th width="15%">ผู้ส่งข้อมูล</th>
                                                    <th width="10%" style="text-align: center">จัดการ</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tblticket">
                                               
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div align="right" style="padding-right: 30px;">
                                                 <button type="button" class="btn  btn-success btn-lg" id="saveapprovededuct"><i class="fa fa-check-circle"></i> อนุมัติ</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>   
                                        
                        </div>
                    </div>
                </div>
            </form>   
        </div>

    </div>
    <!-- /.box -->
</section><!-- /.content -->