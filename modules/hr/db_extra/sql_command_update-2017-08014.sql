/** Table emp_data **/
ALTER TABLE `emp_data`
ADD `BenameEN` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'คำนำหน้าภาษาอังกฤษ' AFTER `username`,
ADD `nameEmpEN` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ชื่อภาษาอังกฤษ' AFTER `BenameEN`,
ADD `lastNameEmpEN` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'นามสกุลภาษาอังกฤษ' AFTER `nameEmpEN`,
ADD `nickNameEmpEN` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ชื่อเล่นภาษาอังกฤษ' AFTER `lastNameEmpEN`,
ADD `race` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'เชื้อชาติ' AFTER `Birthday`,
ADD `districtRelease` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ออกให้โดย (เขต/อำเภอ)' AFTER `ID_Card`,
ADD `DateOfIssueIdcrad` DATE NULL COMMENT 'วันที่ออกบัตร' AFTER `districtRelease`,
ADD `DateOfExpiryIdcrad` DATE NULL COMMENT 'วันที่บัตรหมดอายุ' AFTER `DateOfIssueIdcrad`,
ADD `Personal_daily` TINYINT(3) NULL COMMENT '1=รายวัน 2= รายเดือน' AFTER `Prosonnal_Being`,
ADD `disease` TINYINT(1) NULL COMMENT 'โรคประจำตัว 0:ไม่มี, 1 มี' AFTER `to_salary_status`,
ADD `diseaseDetail` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  COMMENT 'ระบุโรคประจำตัว' AFTER `disease`,
ADD `numberPassportEmp` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'หนังสือเดินทางเลขที่' AFTER `diseaseDetail`,
ADD `DateOfIssuePassport` DATE NULL COMMENT 'วันที่หนังสือเดินบัตรออกบัตร' AFTER `numberPassportEmp`,
ADD `DateOfExpiryPassport` DATE NULL COMMENT 'วันที่หนังสือเดินบัตรหมดอายุ' AFTER `DateOfIssuePassport`,
ADD `Pic_map` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'รูปแผนที่บ้าน' AFTER `DateOfExpiryPassport`,
ADD `Latitude` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ละติจูด (E)' AFTER `Pic_map`,
ADD `Longitude` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ลองติจูด (N)' AFTER `Latitude`,
ADD `job_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ประเภทการจ้างงาน' AFTER `status_confirmData`;

ALTER TABLE `emp_data`
ADD `promtpay_type` TINYINT(3) NULL COMMENT 'ประเภทหมายเลขพร้อมเพย์ 1=Mobile No, 3=ID Card' AFTER `SalaryViaBank`,
ADD `promtpay_number` VARCHAR(13) NULL COMMENT 'หมายเลขพร้อมเพย์' AFTER `promtpay_type`;

ALTER TABLE `emp_data` CHANGE `password_test` `password_test` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `password_backup_by_kenz` `password_backup_by_kenz` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `Spouse_prefix` `Spouse_prefix` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'คำนำหน้าชื่อ คู่สมรส', CHANGE `Spouse_career` `Spouse_career` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'อาชีพคู่สมรส', CHANGE `Spouse_workplace` `Spouse_workplace` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'สถานที่ทำงานคู่สมรส', CHANGE `Moo` `Moo` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'หมู่ที่', CHANGE `Road` `Road` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ถนน', CHANGE `Resident_type` `Resident_type` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ประเภทที่พัก', CHANGE `work_type_id` `work_type_id` VARCHAR(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `Emergency_Call_Person` `Emergency_Call_Person` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'บุคคลที่ติดต่อกรณีฉุกเฉิน', CHANGE `Emergency_Relation` `Emergency_Relation` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ความสัมพันธ์ (บุคคลที่ติดต่อกรณีฉุกเฉิน)', CHANGE `Emergency_Phone_Num` `Emergency_Phone_Num` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '้เบอร์โทรของบุคคลที่ติดต่อกรณีฉุกเฉิน', CHANGE `socialBenifitPercent` `socialBenifitPercent` VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '% ประกันสังคม', CHANGE `socialBenefitStatus` `socialBenefitStatus` VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'สถานะ 0ไม่คิด 1 คิด', CHANGE `benefitFundPercent` `benefitFundPercent` VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '% เงินสะสม', CHANGE `benifitFundStatus` `benifitFundStatus` VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'สถานะ 0 ไม่คิด 1 คิด', CHANGE `to_salary_status` `to_salary_status` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '1 คิดเงินเดือน 2 ไม่คิดเงินเดือน', CHANGE `diseaseDetail` `diseaseDetail` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ระบุโรคประจำตัว', CHANGE `numberPassportEmp` `numberPassportEmp` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'หนังสือเดินทางเลขที่';
ALTER TABLE `emp_data` CHANGE `Live_with` `Live_with` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'พักอยู่กับ';
ALTER TABLE `emp_data` CHANGE `UPDATE_BY` `UPDATE_BY` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `emp_data` CHANGE `status_confirmData` `status_confirmData` INT(1) NULL DEFAULT NULL COMMENT 'สถานะการยืนยันการบันทึกข้อมูล 0:ยืนยัน, 1:ไม่ยืนยัน';
ALTER TABLE `emp_data` CHANGE `disease` `disease` TINYINT(1) NULL DEFAULT NULL COMMENT 'โรคประจำตัว 0:ไม่มี, 1 มี';


ALTER TABLE `emp_family` ADD INDEX(`emp_data_id`);

ALTER TABLE `emp_promise` ADD INDEX(`emp_promise_type_id`);
ALTER TABLE `emp_promise` ADD INDEX(`emp_data_id`);
ALTER TABLE `emp_promise` ADD INDEX(`emp_idcard`);
ALTER TABLE `emp_promise` ADD INDEX(`promise_code`);

ALTER TABLE `emp_promise_mapping` ADD INDEX(`condition_id`);

//WAGE_HISTORY,WAGE_THIS_MONTH,ADD_DEDUCT_HISTORY,ADD_DEDUCT_THIS_MONTH
ALTER TABLE `WAGE_HISTORY` ADD `wage_calculate_id` INT(11) NULL COMMENT 'รหัสการคำนวนเงินเดือนระหว่างเดือน' AFTER `WAGE_THIS_MONTH_DIRECTOR_LOCK`;
ALTER TABLE `WAGE_THIS_MONTH` ADD `wage_calculate_id` INT(11) NULL COMMENT 'รหัสการคำนวนเงินเดือนระหว่างเดือน' AFTER `cal_date`;
ALTER TABLE `ADD_DEDUCT_HISTORY` ADD `wage_calculate_id` INT(11) NULL COMMENT 'รหัสการคำนวนเงินเดือนระหว่างเดือน' AFTER `cal_date`;
ALTER TABLE `ADD_DEDUCT_THIS_MONTH` ADD `wage_calculate_id` INT(11) NULL COMMENT 'รหัสการคำนวนเงินเดือนระหว่างเดือน' AFTER `cal_date`;


//EMP Salary
ALTER TABLE `EMP_SALARY` CHANGE `dateusestart` `start_effective_date` DATE NULL DEFAULT NULL;
ALTER TABLE `EMP_SALARY` ADD `end_effective_date` DATE NULL COMMENT 'วันที่มีผลสิ้นสุด' AFTER `start_effective_date`;
ALTER TABLE `EMP_SALARY` ADD `statusSSO` TINYINT(3) NULL COMMENT 'สถานะคิดประกันสังคม 1=คิดประกันสังคม' AFTER `status`;

ALTER TABLE `EMP_SALARY` ADD `SALARY_STEP_ADD` DECIMAL(10,2) NULL COMMENT 'ขั้นที่เพิ่ม' AFTER `EMP_SALARY_UPDATE_BY`, ADD `SALARY_CHANGE_ID` TINYINT NULL COMMENT 'ID การเปลี่ยนแปลง' AFTER `SALARY_STEP_ADD`, ADD `SALARY_CHANGE_NAME` VARCHAR(200) NULL COMMENT 'ชื่อการเปลี่ยนแปลง' AFTER `SALARY_CHANGE_ID`;
ALTER TABLE `EMP_SALARY` ADD `id_ref_orgchart` INT(11) NULL COMMENT 'ID Ref ตาราง Organic_Chart' AFTER `SALARY_CHANGE_NAME`;

//Salary change
ALTER TABLE `SALARY_CHANGE` ADD `start_effective_date` DATE NULL COMMENT 'วันที่เริ่มมีผล' AFTER `SALARY_CHANGE_UPDATE_BY`, ADD `end_effective_date` DATE NULL COMMENT 'วันที่สิ้นสุดมีผล' AFTER `start_effective_date`;
ALTER TABLE `SALARY_CHANGE` ADD `statusSSO` TINYINT(3) NULL COMMENT 'สถานะจ่ายประกันสังคม 1=จ่าย' AFTER `end_effective_date`, ADD `statusCalculate` TINYINT(3) NULL COMMENT 'สถานจ่ายเงินเดือน 1=จ่าย' AFTER `statusSSO`, ADD `statusMainJob` TINYINT(3) NULL COMMENT 'สถานะตำแหน่งหลัก 1=ตำแหน่งหลัก' AFTER `statusCalculate`;


ALTER TABLE `SALARY_CHANGE` ADD `SALARY_STEP_ADD_OLD` DECIMAL(10,2) NULL COMMENT ' ขั้นที่เพิ่มเดิม' AFTER `statusMainJob`, ADD `SALARY_STEP_ADD_NEW` DECIMAL(10,2) NULL COMMENT 'ขั้นที่เพิ่มใหม่' AFTER `SALARY_STEP_ADD_OLD`, ADD `WHAT_CHANGE_ID` TINYINT(4) NULL COMMENT 'ID สาเหตุการเปลี่ยนแปลง' AFTER `SALARY_STEP_ADD_NEW`, ADD `WHAT_CHANGE_NAME` VARCHAR(200) NULL COMMENT 'ชื่อสาเหตุการเปลี่ยนแปลง' AFTER `WHAT_CHANGE_ID`;




//update 2017-11-16
ALTER TABLE `ADD_DEDUCT_DETAIL` CHANGE `ADD_DEDUCT_DETAIL_START_USE_DATE` `ADD_DEDUCT_DETAIL_START_USE_DATE` DATE NULL DEFAULT NULL COMMENT 'วันที่เริ่มใช้';
ALTER TABLE `ADD_DEDUCT_DETAIL` CHANGE `ADD_DEDUCT_DETAIL_END_USE_DATE` `ADD_DEDUCT_DETAIL_END_USE_DATE` DATE NULL DEFAULT NULL COMMENT 'วันที่หยุดใช้';
ALTER TABLE `ADD_DEDUCT_DETAIL` CHANGE `ADD_DEDUCT_DETAIL_CREATE_DATE` `ADD_DEDUCT_DETAIL_CREATE_DATE` DATETIME NULL DEFAULT NULL;

//update 2017-11-21
ALTER TABLE `dept_deduct` ADD `pay_date` CHAR(7) NULL COMMENT 'รอบเงินเดือน' AFTER `for_month`;

//update 2017-12-04

ALTER TABLE `ot_requestmaster` ADD `is_hr_approved` TINYINT(3) NULL COMMENT 'hr approved' AFTER `update_datetime`, ADD `hr_approved_date` DATE NULL COMMENT 'hr approved date' AFTER `is_hr_approved`, ADD `hr_approved_time` TIME NULL COMMENT 'hr approved time' AFTER `hr_approved_date`;

ALTER TABLE `ot_requestdetail` ADD `money_total` DECIMAL(10,2) NULL COMMENT 'เงินโอทีรวม' AFTER `create_datetime`, ADD `remak` TEXT NULL COMMENT 'หมายเหตุรายละเอียด' AFTER `money_total`, ADD `is_hr_approved` TINYINT(3) NULL COMMENT 'hr approved' AFTER `remak`, ADD `hr_approved_date` DATE NULL COMMENT 'hr approved date' AFTER `is_hr_approved`, ADD `hr_approved_time` TIME NULL COMMENT 'hr approved time' AFTER `hr_approved_date`;

ALTER TABLE `ot_requestmaster` ADD `hr_approved_by` CHAR(13) NULL COMMENT 'user hr approved' AFTER `hr_approved_time`;
ALTER TABLE `ot_requestdetail` ADD `hr_approved_by` CHAR(13) NULL COMMENT 'user hr approved' AFTER `hr_approved_time`


//update 2017-12-11
ALTER TABLE `WAGE_THIS_MONTH` ADD `COMPANY_NAME` VARCHAR(255) NULL AFTER `wage_calculate_id`, ADD `DEPARTMENT_NAME` VARCHAR(255) NULL AFTER `COMPANY_NAME`, ADD `SECTION_NAME` VARCHAR(255) NULL AFTER `DEPARTMENT_NAME`;
ALTER TABLE `WAGE_THIS_MONTH` ADD `WAGE_FIRST_NAME` VARCHAR(200) NULL AFTER `WAGE_EMP_ID`, ADD `WAGE_LAST_NAME` VARCHAR(200) NULL AFTER `WAGE_FIRST_NAME`;

//update 2017-12-12
ALTER TABLE `WAGE_THIS_MONTH` CHANGE `WAGE_SECTION_ID` `WAGE_SECTION_ID` INT(11) NULL DEFAULT NULL COMMENT 'ฝ่าย', CHANGE `WAGE_DEPARTMENT_ID` `WAGE_DEPARTMENT_ID` INT(11) NULL DEFAULT NULL COMMENT 'แผนก', CHANGE `WAGE_WORKING_COMPANY` `WAGE_WORKING_COMPANY` INT(11) NULL DEFAULT NULL COMMENT 'บริษัท', CHANGE `WAGE_GET_MONEY_TYPE` `WAGE_GET_MONEY_TYPE` TINYINT(3) NULL DEFAULT NULL COMMENT '0 รับเงินสด 1 รับเงินผ่านธนาคาร', CHANGE `WAGE_SALARY_STEP` `WAGE_SALARY_STEP` TINYINT(3) NULL DEFAULT NULL COMMENT 'ขั้น', CHANGE `WAGE_THIS_MONTH_CONFIRM` `WAGE_THIS_MONTH_CONFIRM` TINYINT(3) NULL DEFAULT NULL COMMENT '1 confirm', CHANGE `WAGE_THIS_MONTH_EMPLOYEE_LOCK` `WAGE_THIS_MONTH_EMPLOYEE_LOCK` TINYINT(3) NULL DEFAULT NULL, CHANGE `WAGE_THIS_MONTH_STATUS` `WAGE_THIS_MONTH_STATUS` TINYINT(3) NULL DEFAULT NULL, CHANGE `WAGE_THIS_MONTH_BANK_CONFIRM_STATUS` `WAGE_THIS_MONTH_BANK_CONFIRM_STATUS` TINYINT(3) NULL DEFAULT NULL;
ALTER TABLE `WAGE_THIS_MONTH` CHANGE `WAGE_THIS_MONTH_CONFIRM_BY` `WAGE_THIS_MONTH_CONFIRM_BY` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `WAGE_THIS_MONTH` ADD `DIRECTOR_LOCK_COMFIRM_BY` VARCHAR(250) NULL AFTER `SECTION_NAME`, ADD `DIRECTOR_LOCK_COMFIRM_DATE` DATETIME NULL AFTER `DIRECTOR_LOCK_COMFIRM_BY`;
ALTER TABLE `WAGE_THIS_MONTH` CHANGE `WAGE_THIS_MONTH_CONFIRM_DATE` `WAGE_THIS_MONTH_CONFIRM_DATE` DATETIME NULL DEFAULT NULL;



ALTER TABLE `WAGE_THIS_MONTH` ADD `WTH_AMOUNT` DECIMAL NULL COMMENT 'ยอดเงินที่จะหัก ณ ที่จ่ายในเดือนนี้' AFTER `DIRECTOR_LOCK_COMFIRM_DATE`, ADD `WTH_AMOUNT_TAX` DECIMAL NULL COMMENT 'ยอดภาษี หัก ณ ที่จ่ายในเดือนนี้' AFTER `WTH_AMOUNT`, ADD `BNF_AMOUNT` DECIMAL NULL COMMENT 'ยอดเงินสะสมในเดือนนี้' AFTER `WTH_AMOUNT_TAX`;


//update 2017-12-26
ALTER TABLE `tax_calculate` CHANGE `emp_idcard` `emp_idcard` CHAR(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัสบัตรประชาชนพนักงานจ่ายภาษี';


//update 2018-01-11
ALTER TABLE `tax_witholding_detail` ADD `date_pay` CHAR(7) NULL AFTER `tax_rate`, ADD `emp_idcard` CHAR(13) NULL AFTER `date_pay`;










