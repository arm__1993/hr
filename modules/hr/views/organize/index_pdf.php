<?php
/**
 * Created by PhpStorm.
 * User: watcharaphan
 * Date: 13/11/2018 AD
 * Time: 01:04
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\models\Position;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$selectworking = ApiHr::getWorking_company();
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jobreport.js?t=' . time(), ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$dataitem = $data;
$preperNumber = str_split($dataitem[0][tax_regis_code]);
$branchNumber = str_split((strlen($dataitem[0][branch_no]) < 5) ? str_pad($dataitem[0][branch_no], 5, "0", STR_PAD_LEFT) : $dataitem[0][branch_no]);
$post_code = str_split($dataitem[0][post_code])
?>
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun";
        font-size: 25px;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 18px;
        /*font-weight: bold;*/
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }

    .textbox_value {
        font-family: "THSarabun";
        font-size: 13px;
    }

    .text_title {
        font-family: "THSarabun";
        font-size: 14px;
    }

    .text_italic {
        font-style: italic;
    }

    .text_header {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        text-align: center !important;
    }

    .text_header2 {
        font-family: "THSarabun" !important;
        font-size: 20px !important;
        font-weight: bold;
        text-align: center !important;
    }

    .container-page {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 200px;
        padding: 3px 3px;
    }

    .addr_company {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 80px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .addr_pay {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 120px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .income_detail {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 430px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .pay_in {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 1px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .pay_by {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 30px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .warning {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 43%;
        height: 100px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        float: left;
        padding: 3px 3px;
    }

    .sign {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 54%;
        height: 100px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        float: right;
        padding: 3px 3px;
    }

    td {
        font-family: "THSarabun";
        font-size: 25px;


    }

    .circle {
        width: 70px;
        height: 70px;
        border-radius: 10%;
        font-family: "THSarabun" !important;
        font-size: 16px;
        color: #000000;
        line-height: 30px;
        text-align: center;
        background: #F5F5F5
    }

    .bod {
        background-image: url('https://scontent.fbkk1-3.fna.fbcdn.net/v/t1.0-9/38126643_2183521141689528_2569118921001009152_n.jpg?_nc_fx=fbkk1-2&_nc_cat=0&oh=af97bc756dd37051b6ef52bcc3372370&oe=5C01704B');
        background-repeat: no-repeat;
        background-position: 99% 92%;

        background-attachment: fixed;
        background-size: 200px
    }

    th {
        border: 1px solid #000000;
        font-family: "THSarabun";
        font-size: 25px;
    }

</style>


<div class="box-body">

    <center><p style="padding-bottom:-10px;"><b>รายงานตำแหน่งว่าง</b> </p> </center>
    <h3 style='padding-bottom:-20px;padding-top:-30px;'>ออกรายงงานวันที่ <? echo date("j-n-").(date("Y") + 543); ?></h3>
    <table class='text_title' width="100%" cellpadding='0' cellspacing='0'>
        <thead>
        <tr>
            <th width="5%">ที่</th>
            <th width="50%">บริษัท</th>
            <th width="20%">แผนก</th>
            <th width="20%">ฝ่าย</th>
            <th width="20%">ชื่อตำแหน่ง</th>
            <th width="10%">ผังโครงสร้าง</th>
            <th width="10%">บรรจุ</th>
            <th width="10%">ทดลองงาน</th>
            <th width="10%">สรรหา</th>
            <th width="10%">ปิดสรรหา</th>
            <th width="10%">ปิดตำแหน่ง</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        $total = 0;
        $total1 = 0;
        $total2 = 0;
        $total3 = 0;
        $total4 = 0;
        $total5 = 0;

        foreach ($dataProvider as $item) {

            $total += $item['STATUS_19'];
            $total1 += $item['pass'];
            $total2 += $item['notpass'];
            $total3 += $item['STATUS1'];
            $total4 += $item['STATUS_2'];
            $total5 += $item['STATUS_99'];

            ?>
            <tr>
                <td scope="row" style="  text-align: center;"><? echo $i++ ?></td>
                <td><? echo $item['companyname'] ?></td>
                <td><? echo $item['departmentname'] ?></td>
                <td><? echo $item['sectionname'] ?></td>
                <td ><? echo $item['Name'] ?></td>
                <td style="  text-align: center;"><? echo $item['STATUS_19'] ?></td>
                <td style="  text-align: center;"><? echo $item['pass'] ?></td>
                <td style="  text-align: center;"><? echo $item['notpass'] ?></td>
                <td style="  text-align: center;"><? echo $item['STATUS1'] ?></td>
                <td style="  text-align: center;"><? echo $item['STATUS_2'] ?></td>
                <td style="  text-align: center;"><? echo $item['STATUS_99'] ?></td>

            </tr>
        <? }
        ?>

        <tr>
            <td  colspan='5' style="  border-bottom: 1px solid #000000;border-top: 1px solid #000000;" ><center><b>รวม</b></center></td>

            <td style="  border-bottom: 1px solid #000000;border-top: 1px solid #000000;   text-align: center;" ><? echo number_format($total, 0, ".", ","); ?></td>
            <td style="  border-bottom: 1px solid #000000;border-top: 1px solid #000000;  text-align: center;" ><? echo number_format($total1, 0, ".", ","); ?></td>
            <td style="  border-bottom: 1px solid #000000;border-top: 1px solid #000000;   text-align: center;" ><? echo number_format($total2, 0, ".", ","); ?></td>
            <td style="  border-bottom: 1px solid #000000;border-top: 1px solid #000000;  text-align: center;" ><? echo number_format($total3, 0, ".", ","); ?></td>
            <td style="  border-bottom: 1px solid #000000;border-top: 1px solid #000000;  text-align: center;" ><? echo number_format($total4, 0, ".", ","); ?></td>
            <td style="  border-bottom: 1px solid #000000;border-top: 1px solid #000000;  text-align: center;" ><? echo number_format($total5, 0, ".", ","); ?></td>

        </tr>
        </tbody>
    </table>

</div>