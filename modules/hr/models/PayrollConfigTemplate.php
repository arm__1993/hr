<?php

namespace app\modules\hr\models;


use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;


/**
 * This is the model class for table "payroll_config_template".
 *
 * @property integer $id
 * @property integer $ot_template_id
 * @property integer $sso_template_id
 * @property integer $taxincome_template_id
 * @property integer $taxpnd_template_id
 * @property integer $taxtavi_template_id
 * @property integer $guarantee_template_id
 * @property integer $deposit_bnf_id
 * @property integer $withdraw_bnf_id
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class PayrollConfigTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_config_template';
    }

    public function behaviors()
    {
        $session = Yii::$app->session;
        $session->open();
        $_account = $session->get('idcard');
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_datetime'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_datetime'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createby_user'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateby_user'],
                ],
                'value' => $_account, //$_SESSION['USER_ACCOUNT'],
            ],
        ];
    }



    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ot_template_id', 'sso_template_id', 'taxincome_template_id', 'taxpnd_template_id', 'taxtavi_template_id', 'guarantee_template_id','deposit_bnf_id','withdraw_bnf_id'], 'required'],
            [['ot_template_id', 'sso_template_id', 'taxincome_template_id', 'taxpnd_template_id', 'taxtavi_template_id', 'guarantee_template_id','deposit_bnf_id','withdraw_bnf_id'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['createby_user', 'updateby_user'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ot_template_id' => 'รหัสประเภทการจ่ายค่าล่วงเวลา',
            'sso_template_id' => 'รหัสประเภทการหักเงินประกันสังคม',
            'taxincome_template_id' => 'รหัสประเภทการหักเงินภาษี ภงด. 91',
            'taxpnd_template_id' => 'รหัสประเภทการหักเงินภาษี ภงด. 1',
            'taxtavi_template_id' => 'รหัสประเภทการหักเงินภาษี หัก ณ ที่จ่าย',
            'guarantee_template_id' => 'รหัสประเภทการหักเงินค้ำประกัน',
            'deposit_bnf_id' => 'รหัสเงินสะสมฝากแต่ละเดือน',
            'withdraw_bnf_id' => 'รหัสเงินฝากสะสมถอนแต่ละเดือน',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }


    public function getOtTemplateID()
    {
        return $this->ot_template_id;
    }


    public function getSsoTemplateID()
    {
        return $this->sso_template_id;
    }


    public function getTaxIncomeTemplateID()
    {
        return $this->taxincome_template_id;
    }

    public function getTaxPndTemplateID()
    {
        return $this->taxpnd_template_id;
    }


    public function getTaxTaviTemplateID()
    {
        return $this->taxtavi_template_id;
    }

    public function getGuaranteeTemplateID()
    {
        return $this->guarantee_template_id;
    }

    public function getDepositBNFTemplateID()
    {
        return $this->deposit_bnf_id;
    }

    public function getWithdrawBNFTemplateID()
    {
        return $this->withdraw_bnf_id;
    }

}
