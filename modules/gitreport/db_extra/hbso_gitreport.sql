-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 09, 2017 at 11:39 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hbso_gitreport`
--

-- --------------------------------------------------------

--
-- Table structure for table `hbso_dev`
--

CREATE TABLE `hbso_dev` (
  `id` tinyint(3) NOT NULL,
  `dev_fullname` varchar(250) DEFAULT NULL,
  `dev_nickname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hbso_dev`
--

INSERT INTO `hbso_dev` (`id`, `dev_fullname`, `dev_nickname`) VALUES
(1, 'Kritsada Wongnunta', 'kenz'),
(2, 'Adithep Chaisan', 'aeedy'),
(3, 'Bunyut Punyagon', 'golf'),
(4, 'Nattawut Jumpar', 'kwan'),
(5, 'Pacharapol Kanuei', 'em');

-- --------------------------------------------------------

--
-- Table structure for table `hbso_issue`
--

CREATE TABLE `hbso_issue` (
  `id` int(11) UNSIGNED NOT NULL,
  `project_id` tinyint(3) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `dev_id` tinyint(3) DEFAULT NULL,
  `status_id` tinyint(3) DEFAULT NULL,
  `issue_total` int(11) DEFAULT NULL,
  `for_year` int(11) DEFAULT NULL,
  `for_month` tinyint(3) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `update_datetime` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hbso_issue`
--

INSERT INTO `hbso_issue` (`id`, `project_id`, `milestone_id`, `dev_id`, `status_id`, `issue_total`, `for_year`, `for_month`, `create_datetime`, `create_by`, `update_datetime`, `update_by`) VALUES
(1, 1, 1, 3, 1, 0, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(2, 1, 1, 3, 2, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(3, 1, 1, 3, 3, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(4, 1, 1, 3, 4, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(5, 1, 1, 3, 5, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(6, 1, 1, 3, 6, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(7, 1, 1, 4, 1, 0, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(8, 1, 1, 4, 2, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(9, 1, 1, 4, 3, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(10, 1, 1, 4, 4, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(11, 1, 1, 4, 5, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy'),
(12, 1, 1, 4, 6, 10, 2017, 2, '2017-02-08 14:30:05', 'aeedy', '2017-02-08 14:30:05', 'aeedy');

-- --------------------------------------------------------

--
-- Table structure for table `hbso_milestone`
--

CREATE TABLE `hbso_milestone` (
  `id` int(3) NOT NULL,
  `milestone_name` varchar(250) DEFAULT NULL,
  `project_id` tinyint(3) DEFAULT NULL,
  `milestone_start_date` date DEFAULT NULL,
  `milestone_end_date` date DEFAULT NULL,
  `status_active` tinyint(3) DEFAULT NULL COMMENT '1 = active, 0=inactived',
  `create_by` varchar(20) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hbso_milestone`
--

INSERT INTO `hbso_milestone` (`id`, `milestone_name`, `project_id`, `milestone_start_date`, `milestone_end_date`, `status_active`, `create_by`, `create_datetime`) VALUES
(1, '[ERP EasyService] - Milestones 1-Feb', 1, '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 11:46:06'),
(2, '[ERP EasySale] - Milestones 1-Feb', 2, '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 11:46:06'),
(4, '[ERP EasyAccount] - Milestones 1-Feb', 3, '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 12:13:04'),
(5, '[ERP EasyCrm] - Milestones 1-Feb', 4, '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 12:14:04'),
(6, '[ERP EasyHr] - Milestones 1-Feb', 5, '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 12:15:04'),
(7, '[ERP EasyHr] - Milestones 1-Mar', 5, '2017-03-01', '2017-03-31', 1, 'aeedy', '2017-02-08 12:16:04'),
(8, '[ERP EasyHr] - Milestones 1-Apr', 5, '2017-04-01', '2017-04-30', 1, 'aeedy', '2017-02-08 12:17:04'),
(9, '[ERP EasyLoan] - Milestones 1-Feb', 6, '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 12:17:04');

-- --------------------------------------------------------

--
-- Table structure for table `hbso_project`
--

CREATE TABLE `hbso_project` (
  `id` tinyint(3) NOT NULL,
  `project_name` varchar(250) DEFAULT NULL,
  `project_start_date` date DEFAULT NULL,
  `project_end_date` date DEFAULT NULL,
  `status_active` tinyint(3) DEFAULT NULL COMMENT '1=active , 0 =inactive',
  `create_by` varchar(50) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hbso_project`
--

INSERT INTO `hbso_project` (`id`, `project_name`, `project_start_date`, `project_end_date`, `status_active`, `create_by`, `create_datetime`) VALUES
(1, 'ERP EasyService', '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 11:41:06'),
(2, 'ERP EasySale', '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 11:43:06'),
(3, 'ERP EasyAccount', '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 11:45:06'),
(4, 'ERP EasyCrm', '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 11:46:06'),
(5, 'ERP EasyHr', '2017-02-01', '2017-04-30', 1, 'aeedy', '2017-02-08 11:47:06'),
(6, 'ERP EasyLoan', '2017-02-01', '2017-02-28', 1, 'aeedy', '2017-02-08 11:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `issue_status`
--

CREATE TABLE `issue_status` (
  `id` tinyint(3) NOT NULL,
  `status_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issue_status`
--

INSERT INTO `issue_status` (`id`, `status_name`) VALUES
(1, 'Noassign/Backlog'),
(2, 'Inprogress'),
(3, 'Testing'),
(4, 'UAT'),
(5, 'Ready'),
(6, 'Installed');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hbso_dev`
--
ALTER TABLE `hbso_dev`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hbso_issue`
--
ALTER TABLE `hbso_issue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `milestone_id` (`milestone_id`),
  ADD KEY `dev_id` (`dev_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `for_year` (`for_year`),
  ADD KEY `for_month` (`for_month`);

--
-- Indexes for table `hbso_milestone`
--
ALTER TABLE `hbso_milestone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `milestone_start_date` (`milestone_start_date`),
  ADD KEY `milestone_end_date` (`milestone_end_date`);

--
-- Indexes for table `hbso_project`
--
ALTER TABLE `hbso_project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_start_date` (`project_start_date`),
  ADD KEY `project_end_date` (`project_end_date`);

--
-- Indexes for table `issue_status`
--
ALTER TABLE `issue_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hbso_dev`
--
ALTER TABLE `hbso_dev`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hbso_issue`
--
ALTER TABLE `hbso_issue`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `hbso_milestone`
--
ALTER TABLE `hbso_milestone`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hbso_project`
--
ALTER TABLE `hbso_project`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `issue_status`
--
ALTER TABLE `issue_status`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
