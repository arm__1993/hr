<?php

namespace app\modules\wssale\models;

use Yii;
 
class Maincompetitor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Main_Competitor';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'NickName', 'Updater', 'Updater_posCode', 'UpdateTime', 'Status', 'Remark'], 'required'],
            [['Name', 'NickName', 'Remark'], 'string', 'max' => 250],
            [['Updater'], 'string', 'max' => 15],
            [['Updater_posCode'], 'string', 'max' => 30],
            [['UpdateTime', 'Status'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'ชื่อบริษัท',
            'NickName' => 'Nick Name',
            'Updater' => 'Updater',
            'Updater_posCode' => 'รหัสตำแหน่งผู้อัพเดทข้อมูล',
            'UpdateTime' => 'Update Time',
            'Status' => 'Status',
            'Remark' => 'Remark',
        ];
    }

    public function carband()
    {
        $sql = "select DISTINCT(Name) FROM Main_Competitor ORDER by Name DESC" ;
        $dataMaincompetitor = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->queryAll();

        return $dataMaincompetitor ;
    }
    
    public function carbandtoselectcompanyformobile($NameCarband)
    {

     // echo  $NameCarband ;

        $sql = "select DISTINCT(NickName) FROM Main_Competitor Where Name = '$NameCarband'  ORDER by NickName DESC" ;
        $dataMaincompetitor = Yii::$app->dbERP_Easysale_icmba
       
       ->createCommand($sql)
       ->queryAll();

       return $dataMaincompetitor ;
    }


}
