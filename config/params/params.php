<?php

//Load parameter from file each Modules
$paramsGitReport = require(__DIR__ . '/paramsGitReport.php');
$paramsOU = require(__DIR__ . '/paramsOU.php');
$paramsPersonal = require(__DIR__ . '/paramsPersonal.php');
$paramsPayroll = require(__DIR__ . '/paramsPayroll.php');

//Set DB Prefix
$prefixDB = (PRD_MODE) ? DB_PRD_PREFIX : DB_DEMO_PREFIX;
return [
    'COMPANY'=>'บริษัท อีซูซุเชียงราย จำกัด',
    'PROGRAM_NAME' =>'Easy HR',
    'adminEmail'    => 'aeedy.c@gmail.com',
    'TYPE_DECIMAL'  => 2,
    'DELETE_STATUS' => 99,
    'ACTIVE_STATUS' => 1,
    'INACTIVE_STATUS' => 0,
    'ACTIVE_STATUS_LABEL'=>'Active',
    'INACTIVE_STATUS_LABEL'=>'Inactive',
    'PAGE_SIZE'=>20,
    'MALE' => 1,
    'FEMALE' => 2,

    //Additional Params
    'GIT_REPORT'    => $paramsGitReport,
    'PAYROLL'       => $paramsPayroll,
    'OU'            => $paramsOU,
    'PERSONAL'      => $paramsPersonal,


    /** setting db connect each BU
     * CUATION DO NOT change in your left hand side
     * Left is index array, right is Database Name
     */
    'dbConn' =>[
        'ERP_Easysale_Log'                  =>$prefixDB.'ERP_Easysale_Log',
        'ERP_Easysale_icboard'              =>$prefixDB.'ERP_Easysale_icboard',
        'ERP_Easysale_icmba'                =>$prefixDB.'ERP_Easysale_icmba',
        'ERP_Easysale_icmba_Backup'         =>$prefixDB.'ERP_Easysale_icmba_Backup',
        'ERP_Easysale_mobile'               =>$prefixDB.'ERP_Easysale_mobile',
        'ERP_Easysale_todolist'             =>$prefixDB.'ERP_Easysale_todolist',
        'ERP_crm'                           =>$prefixDB.'ERP_crm',
        'ERP_crmLog'                        =>$prefixDB.'ERP_crmLog',
        'ERP_easyaccount'                   =>$prefixDB.'ERP_easyaccount',
        'ERP_easyhr_OU'                     =>$prefixDB.'ERP_easyhr_OU',
        'ERP_easyhr_PAYROLL'                =>$prefixDB.'ERP_easyhr_PAYROLL',
        'ERP_easyhr_TIME_ATTENDANCE'        =>$prefixDB.'ERP_easyhr_TIME_ATTENDANCE',
        'ERP_easyhr_checktime'              =>$prefixDB.'ERP_easyhr_checktime',
        'ERP_easyloan'                      =>$prefixDB.'ERP_easyloan',
        'ERP_maincusdata'                   =>$prefixDB.'ERP_maincusdata',
        'ERP_master_data'                   =>$prefixDB.'ERP_maincusdata',
        'ERP_new_cusdata'                   =>$prefixDB.'ERP_new_cusdata',
        'ERP_new_vehicle'                   =>$prefixDB.'ERP_new_vehicle',
        'ERP_service'                       =>$prefixDB.'ERP_service',
        'ERP_serviceLog'                    =>$prefixDB.'ERP_serviceLog',
        'ERP_service_PreUp'                 =>$prefixDB.'ERP_service_PreUp',
        'ERP_service_backup'                =>$prefixDB.'ERP_service_backup',
        'ERP_service_check'                 =>$prefixDB.'ERP_service_check',
        'hbso_gitreport'                    =>$prefixDB.'hbso_gitreport',
        'config_acc_hr'                     =>$prefixDB.'config_acc_hr',
        'config_acc_loan'                   =>$prefixDB.'config_acc_loan',
        'config_acc_sales'                  =>$prefixDB.'config_acc_sales',
        'config_acc_service'                =>$prefixDB.'config_acc_service',
        'easyhr_icchecktime2'               =>$prefixDB.'easyhr_icchecktime2',
        'log_ERP_Easysale_icboard'          =>$prefixDB.'log_ERP_Easysale_icboard',
        'log_ERP_Easysale_icmba'            =>$prefixDB.'log_ERP_Easysale_icmba',
        'log_ERP_Easysale_mobile'           =>$prefixDB.'log_ERP_Easysale_mobile',
        'log_ERP_Easysale_todolist'         =>$prefixDB.'log_ERP_Easysale_todolist',
        'log_ERP_crm'                       =>$prefixDB.'log_ERP_crm',
        'log_ERP_easyaccount'               =>$prefixDB.'log_ERP_easyaccount',
        'log_ERP_easyhr_OU'                 =>$prefixDB.'log_ERP_easyhr_OU',
        'log_ERP_easyhr_PAYROLL'            =>$prefixDB.'log_ERP_easyhr_PAYROLL',
        'log_ERP_easyhr_TIME_ATTENDANCE'    =>$prefixDB.'log_ERP_easyhr_TIME_ATTENDANCE',
        'log_ERP_easyhr_checktime'          =>$prefixDB.'log_ERP_easyhr_checktime',
        'log_ERP_easyloan'                  =>$prefixDB.'log_ERP_easyloan',
        'log_ERP_maincusdata'               =>$prefixDB.'log_ERP_maincusdata',
        'log_ERP_master_data'               =>$prefixDB.'log_ERP_master_data',
        'log_ERP_new_cusdata'               =>$prefixDB.'log_ERP_new_cusdata',
        'log_ERP_new_vehicle'               =>$prefixDB.'log_ERP_new_vehicle',
        'log_ERP_replace_address'           =>$prefixDB.'log_ERP_replace_address',
        'log_ERP_service'                   =>$prefixDB.'log_ERP_service',
        'log_config_acc_hr'                 =>$prefixDB.'log_config_acc_hr',
        'log_config_acc_loan'               =>$prefixDB.'log_config_acc_loan',
        'log_config_acc_sales'              =>$prefixDB.'log_config_acc_sales',
        'log_config_acc_service'            =>$prefixDB.'log_config_acc_service',
    ],

];


// ========================================================== //
 // Example Access parameter
 // *  Yii::$app->params['DELETE_STATUS'];
 // *  Yii::$app->params['PAYROLL']['example'];
 // *  Yii::$app->params['GIT_REPORT']['ACTIVE_STATUS'];
 // *
 // *
 
