<div class="container_card">
    <div class="card_block sizeA">
        <img src="../../upload/personal/<?php echo $emp['Pictures_HyperL'];?>" class="" alt="รูปพนักงาน" >
        <div class="emp_card_name syt1"><?php echo $emp['Name'];?></div>
        <div class="emp_card_surename syt1"><?php echo $emp['Surname'];?></div>
        <div class="emp_card_position_name syt1"><?php echo $emp['positionName'];?></div>
    </div>
    <div class="card_back_block sizeA">
        <div class="text_back_card">
            <div>1.กรุณาติดบัตรทุกครั้งเมื่อปฏิบัติงาน</div>
            <div>2.บัตรนี้ใช้ได้เฉพาะเจ้าของบัตรนี้เท่านั้น</div>
            <div>3.บัตรนี้เป็นสมบัติของกลุ่มนกเงือกบริการ</div>
            <div>ท่านผู้ได้เก็บได้โปรดนำส่งคืนกลุ่มนกเงือกบริการ</div>
            <div>4.หากรูดบัตรไม่ได้กรุณาติดต่อฝ่ายบุคคล</div>
            <div>ด้วยตัวเองเท่านั้น</div>
            <div>5.ห้ามดัดแปลงหรือแก้ไขบัตร ทุกกรณี</div>
            <div>6.บริษัทขอสงวนสิทธิ์ในการยกเลิกบัตร</div>
            <div>โดยไม่ต้องแจ้งล่วงหน้า</div>

            <img src="data:image/png;base64,<?php echo $emp_barcode;?>" style="width: 190px;margin-top: 40px;" class="" alt="barcode" >
        </div>
    </div>
</div>