<?php
/**
 * Created by PhpStorm.
 * User: watcharaphan
 * Date: 25/5/2018 AD
 * Time: 09:51
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use yii\helpers\Url;
use app\modules\baymanagement\models\EholeTake;
use app\modules\baymanagement\models\EholeTakeLevel;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use app\api\Utility;
use app\modules\baymanagement\models\ErepairGroup;
use app\modules\baymanagement\models\EholeJobLevelmaster;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/baylayout.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/technician.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/baymanagement/armbox.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$today = date('d/m/Y');

/*
echo '<pre>';
print_r($arrItemData);
echo '</pre>';
exit();*/


?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">จัดการแผนที่โรงซ่อม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">


                    <!--modal start-->
                    <div class="row">
                        <div class="col-md-11"></div>

                        <div class="col-md-1">
                            <?php
                            Modal::begin([
                                'id' => 'modalfrmAddtechnician',
                                'header' => '<strong>เพิ่มช่าง </strong>',
                                'toggleButton' => [
                                    'id' => 'btnAddNewtechnician',
                                    'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                    'class' => 'btn btn-success'
                                ],
                                'closeButton' => [
                                    'label' => '<i class="fa fa-close"></i>',
                                    //'class' => 'close pull-right',
                                    'class' => 'btn btn-success btn-sm pull-right'
                                ],
                                'size' => 'modal-md',
                            ]);
                            ?>
                            <form role="form" id="frmAddtechnician">

                                <div class="box-body">
                                    <div class="col-md-12">
                                        <label class="col-md-2">เลือกช่าง</label>

                                        <div class="col-sm-10">

                                            <select id="techician_name_sername" name="techician_name_sername"
                                                    class="form-control select2">
                                                <option value="">เลือกช่าง</option>
                                                <?php foreach ($modelcvhname as $k => $v) { ?>
                                                    <option value="<?php echo $v['technician'] ?>"><?php echo $v['techician_name_sername'] ?></option>
                                                <?php }; ?>
                                            </select>

                                        </div>
                                    </div>
                                    <br><br><br><br>

                                    <div class="col-md-12">
                                        <label class="col-md-2">เลือกlavel</label>

                                        <div class="col-sm-10">

                                            <select id="technician_level_id" name="technician_level_id"
                                                    class="form-control ">
                                                <?php foreach ($modeleholelevelmasterfor as $k => $v) { ?>
                                                    <option value="<?php echo $v['id'] ?>"><?php echo $v['technician_level_name'] ?></option>
                                                <?php }; ?>
                                            </select>

                                        </div>
                                    </div>
                                    <br><br><br><br>
                                    <?php

                                    // print_r($modelcvhname);
                                    ?>
                                    <div class="col-md-12">
                                        <?php foreach ($showerepairgroupfor as $k => $v) { ?>
                                            <input type="checkbox" class="form-check-input col-md-2 checkinputarm_<?php echo $v['id'] ?> "
                                                   id="erepair_group_id" name="erepair_group_id[]"
                                                   value="<?php echo $v['id'] ?>">
                                            <label class="form-check-label col-sm-10"
                                                   for="exampleCheck1"><?php echo $v['name'] ?></label>
                                        <?php };?>
                                    </div>
                                </div>

                             <!--   <input type="hidden" class="status_active"
                                       id="status_active" value="1"
                                       name="status_active">
-->
                                <!-- /.box-body -->


                                <div class="box-body">
                                    <?php echo Html::hiddenInput('hide_activityedit_technician', null, ['id' => 'hide_activityedit_technician', 'class' => 'hide_activityedit_technician']); ?>
                                    <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivitytechnician']); ?>
                                </div>

                            </form>
                            <?php
                            Modal::end();
                            ?>
                        </div>

                    </div>
                    <!--end start-->

                    <!--table start-->
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            Pjax::begin(['id' => 'pjax_tb_technician']);
                            echo GridView::widget([
                                'dataProvider' => $showeholetakelevelindex,
                                'filterModel' => $showeholetakelevel,
                                'columns' => [
                                    [
                                        'header' => 'ที่',
                                        'class' => 'yii\grid\SerialColumn',
                                        'headerOptions' => ['width' => '23'],
                                    ],

                                    [
                                        'attribute' => 'emp_idcard',
                                        'header' => 'ชื่อช่าง',
                                        'value' => function ($data) {
                                            $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->emp_idcard);
                                            return $modelResultemp['0']['Fullname'];
                                        },
                                        'label' => 'ชื่อช่าง',
                                        //'format' => 'raw',
                                        'filter' => false,
                                        'contentOptions' => ['style' => 'width: 155px;', 'align=center']

                                    ],

                                    [

                                        //'attribute' => 'create_by',
                                        'label' => 'ระดับความชำนาญ',
                                        // 'value' => 'emp_idcard',
                                        'value' => 'technician_level_name',
                                        'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                    ],
                                    [

                                        //'attribute' => 'create_by',
                                        'label' => 'กลุ่มงานซ้อม',
                                        // 'value' => 'emp_idcard',
                                        'value' => function ($data) use ($arrItemData) {
                                            //return ;


                                            $arraya = $arrItemData[$data->emp_idcard];
                                            return $arraya;

                                            /* echo '<pre>';
                                             print_r($arraya);
                                             echo '</pre>';*/

                                            //  return $abxc;
                                            //return count($arraya);
                                        },
                                        /* 'value' => 'erepair_group_id',*/
                                        'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                    ],

                                    [
                                        /* 'attribute' => 'record_status',*/
                                        'label' => 'สถานะ',
                                        // 'value' => 'record_status',
                                        'format' => 'image',
                                        'value' => function ($data) {
                                            return Utility::dispActive($data->status_active);
                                        },
                                        'filter' => false,
                                        'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                    ],
                                    [

                                        'class' => 'yii\grid\ActionColumn',
                                        'header' => 'จัดการข้อมูลบริษัท',
                                        'template' => '{update}  &nbsp;&nbsp; {delete}',
                                        'buttons' => [
                                            'update' => function ($url, $data) {
                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                    'title' => 'แก้ไข',
                                                    'onclick' => '(function($event) {
                                                                        updatetechnician(' . $data->emp_idcard . ',' . $data->technician_level_id . ');
                                                                })();'
                                                ]);
                                            },


                                            'delete' => function ($url, $data) {
                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                    'title' => 'ลบ',
                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->technician_level_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletetechnicians('. $data->emp_idcard . ',' . $data->technician_level_id .');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                ]);
                                            },
                                        ],
                                        'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                    ],

                                ],

                            ]);
                            Pjax::end();
                            ?>
                        </div>
                    </div>
                    <!--table end-->


                </div>
            </div>
        </div>


</section>