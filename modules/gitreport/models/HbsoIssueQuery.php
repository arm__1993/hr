<?php

namespace app\modules\gitreport\models;

/**
 * This is the ActiveQuery class for [[HbsoIssue]].
 *
 * @see HbsoIssue
 */
class HbsoIssueQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return HbsoIssue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HbsoIssue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
