<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "SALARY_STEP".
 *
 * @property integer $SALARY_STEP_ID
 * @property string $SALARY_STEP_CHART_NAME
 * @property string $SALARY_STEP_LEVEL
 * @property string $SALARY_STEP_START_SALARY
 * @property string $SALARY_STEP_VALUE
 * @property string $SALARY_STEP_LIMIT
 * @property string $SALARY_STEP_YEAR
 * @property string $SALARY_STEP_START_DATE
 * @property string $SALARY_STEP_END_DATE
 * @property string $SALARY_STEP_STATUS
 * @property string $SALARY_STEP_CREATE_DATE
 * @property string $SALARY_STEP_CREATE_BY
 * @property string $SALARY_STEP_UPDATE_DATE
 * @property string $SALARY_STEP_UPDATE_BY
 */
class Salarystep extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SALARY_STEP';
    }

    public static function primarykey()
    {
        return ['SALARY_STEP_ID'];
    }



    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SALARY_STEP_CHART_NAME', 'SALARY_STEP_LEVEL', 'SALARY_STEP_START_SALARY', 'SALARY_STEP_VALUE', 'SALARY_STEP_LIMIT', 'SALARY_STEP_YEAR', 'SALARY_STEP_START_DATE', 'SALARY_STEP_END_DATE', 'SALARY_STEP_STATUS', 'SALARY_STEP_CREATE_DATE', 'SALARY_STEP_CREATE_BY', 'SALARY_STEP_UPDATE_BY'], 'required'],
            [['SALARY_STEP_START_SALARY', 'SALARY_STEP_VALUE'], 'number'],
            [['SALARY_STEP_START_DATE', 'SALARY_STEP_END_DATE', 'SALARY_STEP_CREATE_DATE', 'SALARY_STEP_UPDATE_DATE'], 'safe'],
            [['SALARY_STEP_CHART_NAME', 'SALARY_STEP_CREATE_BY', 'SALARY_STEP_UPDATE_BY'], 'string', 'max' => 250],
            [['SALARY_STEP_LEVEL'], 'string', 'max' => 20],
            [['SALARY_STEP_LIMIT', 'SALARY_STEP_YEAR'], 'string', 'max' => 10],
            [['SALARY_STEP_STATUS'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SALARY_STEP_ID' => 'Salary  Step  ID',
            'SALARY_STEP_CHART_NAME' => 'Salary  Step  Chart  Name',
            'SALARY_STEP_LEVEL' => 'Salary  Step  Level',
            'SALARY_STEP_START_SALARY' => 'Salary  Step  Start  Salary',
            'SALARY_STEP_VALUE' => 'Salary  Step  Value',
            'SALARY_STEP_LIMIT' => 'Salary  Step  Limit',
            'SALARY_STEP_YEAR' => 'Salary  Step  Year',
            'SALARY_STEP_START_DATE' => 'Salary  Step  Start  Date',
            'SALARY_STEP_END_DATE' => 'Salary  Step  End  Date',
            'SALARY_STEP_STATUS' => 'Salary  Step  Status',
            'SALARY_STEP_CREATE_DATE' => 'Salary  Step  Create  Date',
            'SALARY_STEP_CREATE_BY' => 'Salary  Step  Create  By',
            'SALARY_STEP_UPDATE_DATE' => 'Salary  Step  Update  Date',
            'SALARY_STEP_UPDATE_BY' => 'Salary  Step  Update  By',
        ];
    }
}
