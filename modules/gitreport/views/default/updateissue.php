<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/6/2017 AD
 * Time: 18:03
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;



$this->registerJsFile(Yii::$app->request->baseUrl . '/js/gitreport/update_issue.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<form>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Update Issue</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">

        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
</form>
