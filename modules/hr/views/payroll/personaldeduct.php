<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 6/6/2017 AD
 * Time: 8:41 PM
 */

?>


<section class="content">
    <!-- Default box -->
    <!--<div class="box box-danger">-->
    <!--<div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">ข้อมูลพนักงาน</a>
            </li>
            <li>เพิ่มข้อมูลพนักงาน</li>
            <li class="active">ประกันสังคมและเงินสะสม</li>
        </ul>
    </div>-->
    <div class="box-body">
        <input type="hidden" id="idcardset" value="1509901325106">
        <form class="form-horizontal">
            <div class="row">
                <div class="col-md-12" id="showMonth">
                    <center>
                        <label>เลือกเดือนที่จ่าย</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="selectmonth" id="selectmonth" >
                                <?php
                                for ($i = 1; $i <= 12; $i++) {
                                    $sel = ($i == date('m')) ? 'selected="selected"' : '';
                                    echo '<option value="' . $i . '" ' . $sel . '>' . \app\api\DateTime::convertMonth($i) . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <div class="btn-group">
                            <div class="btn-group">
                                <select class="form-control" name="selectyear" id="selectyear">


                                </select>
                            </div>
                        </div>
                        &nbsp;&nbsp;
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <div class="btn-group">
                                        <button type="button" onclick="seachPersonaldeduct()" class="btn btn-block btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="reset" class="btn btn-block btn-danger btn-sm">ล้างข้อมูล</button>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
            <div>
                <br>
            </div>
            <div class="row">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>รายการหัก</th>
                                <th>จำนวน(บาท)</th>
                                <th>รายละเอียด</th>
                                <th>หักจากหน่วยงาน</th>
                            </tr>
                        </thead>
                        <tbody id="showdetaildeduct">

                        </tbody>
                    </table>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section><!-- /.content -->

