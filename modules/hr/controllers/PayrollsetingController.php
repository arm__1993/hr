<?php

namespace app\modules\hr\controllers;

use app\modules\hr\apihr\ApiOT;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\models\Adddeducttemplate;

use app\modules\hr\models\PayrollConfigTemplate;
use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\hr\controllers\MasterController;

use Yii;

class PayrollsetingController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if (!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'), 302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'saveadddeductactivity' => ['post'],
                    'updateadddeduct' => ['post'],
                    'deleteaddeduct' => ['post'],
                    'savepayrollconfig' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        //return $this->render('index');

        $param = Yii::$app->request->queryParams;

        $Adddeducttemplate = new Adddeducttemplate();
        $dataProvider = $Adddeducttemplate->searchaddeduct($param);
        $dataProviderDeduct = $Adddeducttemplate->searchdeduct($param);


        $PayrollConfigTemplate = PayrollConfigTemplate::findOne(1);
        if ($PayrollConfigTemplate === null) {
            $PayrollConfigTemplate = new PayrollConfigTemplate();
        }


        $arrTemplateID = ApiOT::getArrayDeductTemplate('all');
        //$arrDepositTemplateID = ApiOT::getArrayDeductTemplate(1);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'Adddeducttemplate' => $Adddeducttemplate,
            'dataProviderDeduct' => $dataProviderDeduct,
            'PayrollConfigTemplate' => $PayrollConfigTemplate,
            'arrTemplateID' => $arrTemplateID,
           // 'arrDepositTemplateID'=>$arrDepositTemplateID
        ]);
    }

    public function actionDeleteaddeduct()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = Adddeducttemplate::findOne(['ADD_DEDUCT_TEMPLATE_ID' => $post['id']]);
            // $Adddeducttemplateloanreport = $model['ADD_DEDUCT_TEMPLATE_LOAN_REPORT'];
            // if($Adddeducttemplateloanreport=''){
            //     $Adddeducttemplateloanreport = ' ';
            // }else{
            //     $Adddeducttemplateloanreport = $Adddeducttemplateloanreport;
            // }
            $model->ADD_DEDUCT_TEMPLATE_STATUS = Yii::$app->params['DELETE_STATUS'];
            //$model->ADD_DEDUCT_TEMPLATE_LOAN_REPORT = $Adddeducttemplateloanreport;  //TODO :  ตัวแปรมาจากไหน
            $model->save();
            if ($model->save() !== false) {
                echo true;
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
               // echo '>>>>' . $Adddeducttemplateloanreport;
            }
        }
    }

    public function actionSaveadddeductactivity()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login = $this->idcardLogin;

            if ($post['hide_activityedit'] != '') {
                $model = Adddeducttemplate::findOne(['ADD_DEDUCT_TEMPLATE_ID' => $post['hide_activityedit']]);
                $model->ADD_DEDUCT_TEMPLATE_NAME = $post['ADD_DEDUCT_TEMPLATE_NAME'];
                $model->accounting_code_pk = $post['accounting_code_pk'];
                $model->ADD_DEDUCT_TEMPLATE_TYPE = $post['ADD_DEDUCT_TEMPLATE_TYPE'];
                $model->ADD_DEDUCT_TEMPLATE_STATUS = $post['ADD_DEDUCT_TEMPLATE_STATUS'];
                $model->ADD_DEDUCT_TEMPLATE_LOAN_REPORT = $post['ADD_DEDUCT_TEMPLATE_LOAN_REPORT'];
                $model->ADD_DEDUCT_TEMPLATE_CREATE_DATE = new Expression('NOW()');
                $model->ADD_DEDUCT_TEMPLATE_CREATE_BY = $login;
                $model->ADD_DEDUCT_TEMPLATE_UPDATE_DATE = new Expression('NOW()');
                $model->ADD_DEDUCT_TEMPLATE_UPDATE_BY = $login;
                $model->tax_section_id = (int)$post['tax_section_id'];
                $model->taxincome_type_id = (int)$post['taxincome_type_id'];
                $model->tax_rate = $post['tax_rate'];
                //$model->tax_section_id=$post['tax_section_id'];
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";

                }
            } else {
                $model = new Adddeducttemplate();
                $model->ADD_DEDUCT_TEMPLATE_NAME = $post['ADD_DEDUCT_TEMPLATE_NAME'];
                $model->accounting_code_pk = $post['accounting_code_pk'];
                $model->ADD_DEDUCT_TEMPLATE_TYPE = $post['ADD_DEDUCT_TEMPLATE_TYPE'];
                $model->ADD_DEDUCT_TEMPLATE_STATUS = $post['ADD_DEDUCT_TEMPLATE_STATUS'];
                $model->ADD_DEDUCT_TEMPLATE_LOAN_REPORT = $post['ADD_DEDUCT_TEMPLATE_LOAN_REPORT'];
                $model->ADD_DEDUCT_TEMPLATE_CREATE_DATE = new Expression('NOW()');
                $model->ADD_DEDUCT_TEMPLATE_CREATE_BY = $login;
                $model->ADD_DEDUCT_TEMPLATE_UPDATE_BY = $login;
                $model->tax_section_id = (int)$post['tax_section_id'];
                $model->taxincome_type_id = (int)$post['taxincome_type_id'];
                $model->tax_rate = $post['tax_rate'];
                //$model->tax_section_id=$post['tax_section_id'];
                $model->save();
                if ($model->save() !== false) {
                    echo true;
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";

                }
            }


        }

    }

    public function actionUpdateadddeduct()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = Adddeducttemplate::findOne(['ADD_DEDUCT_TEMPLATE_ID' => $id])) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }


    public function actionSavepayrollconfig()
    {

        if (Yii::$app->request->isAjax) {

            $model = PayrollConfigTemplate::findOne(1);
            $setStep2 = false;
            //Check Model if not null, Clone it
            if ($model !== null) {
                $ObjCloneCfg = clone $model; // <== clone object;
                $setStep2 = true;
            }

            //If null create new instance
            if ($model === null) {
                $model = new PayrollConfigTemplate();
            }


            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                //when already save, go to step 2, step 2 update history data
                if ($setStep2) {

                    $newValue = Yii::$app->request->post();
                    $connection = Yii::$app->dbERP_easyhr_PAYROLL;
                    $transaction = $connection->beginTransaction();
                    try {

                        //update config ot template ID here
                        if ($ObjCloneCfg->ot_template_id != $newValue['PayrollConfigTemplate']['ot_template_id']) {

                            $N = $newValue['PayrollConfigTemplate']['ot_template_id'];
                            $O = $ObjCloneCfg->ot_template_id;
                            $sql="UPDATE ADD_DEDUCT_HISTORY SET ADD_DEDUCT_THIS_MONTH_TMP_ID='$N' WHERE ADD_DEDUCT_THIS_MONTH_TMP_ID='$O'  ";
                            $connection->createCommand($sql)->execute();
                        }

                        //update config sso template ID here
                        if ($ObjCloneCfg->sso_template_id != $newValue['PayrollConfigTemplate']['sso_template_id']) {
                            $N = $newValue['PayrollConfigTemplate']['sso_template_id'];
                            $O = $ObjCloneCfg->sso_template_id;
                            $sql="UPDATE ADD_DEDUCT_HISTORY SET  ADD_DEDUCT_THIS_MONTH_TMP_ID='$N' WHERE ADD_DEDUCT_THIS_MONTH_TMP_ID='$O'  ";
                            $c = $connection->createCommand($sql)->execute();
                        }

                        //update config tax income template ID here
                        if ($ObjCloneCfg->taxincome_template_id != $newValue['PayrollConfigTemplate']['taxincome_template_id']) {
                            $N = $newValue['PayrollConfigTemplate']['taxincome_template_id'];
                            $O = $ObjCloneCfg->taxincome_template_id;
                            $sql="UPDATE ADD_DEDUCT_HISTORY SET  ADD_DEDUCT_THIS_MONTH_TMP_ID='$N' WHERE ADD_DEDUCT_THIS_MONTH_TMP_ID='$O'  ";
                            $c = $connection->createCommand($sql)->execute();
                        }

                        //update config tax pnd template ID here
                        if ($ObjCloneCfg->taxpnd_template_id != $newValue['PayrollConfigTemplate']['taxpnd_template_id']) {
                            $N = $newValue['PayrollConfigTemplate']['taxpnd_template_id'];
                            $O = $ObjCloneCfg->taxpnd_template_id;
                            $sql="UPDATE ADD_DEDUCT_HISTORY SET  ADD_DEDUCT_THIS_MONTH_TMP_ID='$N' WHERE ADD_DEDUCT_THIS_MONTH_TMP_ID='$O'  ";
                            $c = $connection->createCommand($sql)->execute();
                        }

                        //update config tax tavi 50 template ID here
                        if ($ObjCloneCfg->taxtavi_template_id != $newValue['PayrollConfigTemplate']['taxtavi_template_id']) {
                            $N = $newValue['PayrollConfigTemplate']['taxtavi_template_id'];
                            $O = $ObjCloneCfg->taxtavi_template_id;
                            $sql="UPDATE ADD_DEDUCT_HISTORY SET  ADD_DEDUCT_THIS_MONTH_TMP_ID='$N' WHERE ADD_DEDUCT_THIS_MONTH_TMP_ID='$O'  ";
                            $c = $connection->createCommand($sql)->execute();
                        }

                        //update config guarantee money template ID here
                        if ($ObjCloneCfg->guarantee_template_id != $newValue['PayrollConfigTemplate']['guarantee_template_id']) {
                            $N = $newValue['PayrollConfigTemplate']['guarantee_template_id'];
                            $O = $ObjCloneCfg->guarantee_template_id;
                            $sql="UPDATE ADD_DEDUCT_HISTORY SET  ADD_DEDUCT_THIS_MONTH_TMP_ID='$N' WHERE ADD_DEDUCT_THIS_MONTH_TMP_ID='$O'  ";
                            $c = $connection->createCommand($sql)->execute();
                        }


                        $transaction->commit();

                    } catch (ErrorException $e) {
                        $transaction->rollBack();
                        throw new \Exception('ERROR' . $e->getMessage());
                    }

                }

                echo 1;

            } else {
                $error = \yii\widgets\ActiveForm::validate($model);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                print_r($error);
            }
        }
    }


}
