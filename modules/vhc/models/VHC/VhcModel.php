<?php

namespace app\modules\vhc\models\VHC;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\vhc\models\MasterModel;

/**
 * This is the model class for table "vhc_model".
 *
 * @property integer $id
 * @property string $model_name
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    protected $_pageSize;

    public static function tableName()//ไปหาตารางไหน = vhc
    {
        return 'vhc_model';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() //B--ไม่ได้ใช้
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules() //--กฏเกนของโมงเดล
    {
        return [
            [['model_name'], 'required'],
            [['record_status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['model_name'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()//--
    {
        return [
            'id' => 'ID',
            'model_name' => 'Model Name',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }

    /*public function search($params)
    {

        $query = VhcModel::find();
        $showvhcindex = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->_pageSize,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(
            ['like', 'list_name', $this->model_name]
        );

        return $showvhcindex;
    }*/

     public function search($params)
     {
         $data = VhcModel::find() ->where('record_status <> 99 ');
         $this->load($params);
         $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
         $data->andFilterWhere(['like','model_name',$this->model_name]); //	ชื่อเต็มบริษัท
         return $dataProvider = new ActiveDataProvider([
             'query' => $data,
             'pagination' => [
                 'pageSize' => 10,
             ]
         ]);

     }
}
