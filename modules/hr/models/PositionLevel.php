<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "position_level".
 *
 * @property integer $level_id
 * @property string $level_code
 * @property string $level_name
 * @property integer $level_status
 */
class PositionLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position_level';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level_name'], 'required'],
            [['level_status','level_code'], 'integer'],
            [['level_name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'level_id' => 'Level ID',
            'level_code' => 'Level Code',
            'level_name' => 'Level Name',
            'level_status' => 'Level Status',
        ];
    }

    /**
     * @inheritdoc
     * @return PositionLevelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PositionLevelQuery(get_called_class());
    }
}
