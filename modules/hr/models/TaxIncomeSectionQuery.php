<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[TaxIncomeSection]].
 *
 * @see TaxIncomeSection
 */
class TaxIncomeSectionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaxIncomeSection[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaxIncomeSection|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
