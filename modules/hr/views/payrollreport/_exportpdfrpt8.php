<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;
//$monthselect = $session->get('monthselect');
//$reportSalaryTotal = $session->get('reportSalaryTotal');

$emp_data = $session->get('emp_data');
$data_bnf = $session->get('data_bnf');
$display_text = $session->get('display_text');


?>
<style>
    .container{
        font-family: "THSarabun";
        font-size: 16px;
    }
    p {
        font-family: "THSarabun";
        font-size: 16px;
    }
    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }
    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;
    }
    .rpt tr.odd { background: #f3f3f3; }
    .rpt tr.even { background: #FFF; }


</style>

<div class="row container">

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-6">
            <h3 style="text-align: center"><?php echo $display_text; ?></h3>
        </div>
    </div>
    <?php

    $tbl = '';

    $tbl .= '<div class="row">
                <div class="col-md-12">
                    <table  cellspacing="0" class="table table-striped table-bordered rpt" width="80%" align="center">
                        <thead>
                        <tr role="row">
                            <th aria-sort="ascending" style="text-align: center;width: 10%">ลำดับ</th>
                            <th aria-sort="ascending" style="text-align: center;width: 30%">เงินเดือน</th>
                            <th aria-sort="ascending" style="text-align: center;width: 20%">รอบเงินเดือน</th>
                            <th aria-sort="ascending" style="text-align: center;width: 15%">เงินสะสม</th>
                            <th aria-sort="ascending" style="text-align: center;width: 20%">เงินสะสมรวมทั้งหมด</th>
                        </tr>
                        </thead>
                        <tbody>';

                        $rec = 1;
                        foreach ($data_bnf as $value) {
                            $cls = ($rec%2==0) ? 'odd' : 'even';
                        $tbl .='<tr class="'.$cls.'" role="row" style="font-size: 12px;">
                                    <td style="text-align: center;">'.$rec.'</td>
                                    <td>'.$value['BENEFIT_DETAIL'].'</td>
                                    <td style="text-align: center">'.$value['BENEFIT_SAVING_DATE'] .'</td>
                                    <td style="text-align: right;">'.Helper::displayDecimal($value['BENEFIT_AMOUNT']) .'</td>
                                    <td style="text-align: right;">'.Helper::displayDecimal($value['BENEFIT_TOTAL_AMOUNT']).'</td>
                                </tr>';
                        $rec++;
                        }
                        $tbl .='</tbody>
                    </table>
                </div>
            </div>';


    echo $tbl;
    ?>

</div>