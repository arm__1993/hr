<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/4/2016 AD
 * Time: 11:02 AM
 */


namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
//use app\modules\WScustomercenter\models\Amaingeography;
use app\modules\wscommon\models\Amainamphur;
use yii\helpers\ArrayHelper; //vendor/yiisoft/yii2/helpers/ArrayHelper

class AmphurController extends ActiveController
{

    // adjust the model class to match your model
    public $modelClass = 'app\modules\wscommon\models\Amainamphur';


    public function behaviors()
    {
        return
            ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                ],
            ],
        ],
        parent::behaviors());
    }
    
    public function actionSearchamphurbyprovinceid()
    {
        
        $provinceId = \Yii::$app->request->get('provinceId');
        $amphur = new Amainamphur();
        $amphurSearch = $amphur->searchamphurbyprovinceid($provinceId);
        return $amphurSearch;
    }


}