<?php

namespace app\modules\vhc\controllers;
use Yii;
use app\modules\vhc\controllers\MasterController;
use app\modules\vhc\models\VHC\VhcGrouplist;
use app\modules\vhc\models\VHC\VhcList;
use app\modules\vhc\models\VHC\VhcListOption;
use app\modules\vhc\models\VHC\VhcServicetype;
use app\modules\vhc\models\VHC\VhcBlame;
use app\modules\vhc\models\VHC\VhcModel;
class VhcController extends MasterController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionBlame()
    {
        $VhcBlameSearch = new VhcBlame();
        $VhcBlameProvider = $VhcBlameSearch->search(Yii::$app->request->queryParams);

        return $this->renderPartial('_blame', [
            'VhcBlameSearch' => $VhcBlameSearch,
            'VhcBlameProvider' => $VhcBlameProvider,
        ]);
    }
    public function actionGrouplist()
    {
        $VhcGroupSearch = new VhcGrouplist();
        $VhcGroupProvider = $VhcGroupSearch->search(Yii::$app->request->queryParams);

        return $this->renderPartial('_group', [
            'VhcGroupSearch' => $VhcGroupSearch,
            'VhcGroupProvider' => $VhcGroupProvider,
        ]);
    }
    public function actionModel()
    {
        $VhcModelSearch = new VhcModel();
        $VhcModelProvider = $VhcModelSearch->search(Yii::$app->request->queryParams);

        return $this->renderPartial('_model', [
            'VhcModelSearch' => $VhcModelSearch,
            'VhcModelProvider' => $VhcModelProvider,
        ]);
    }
    public function actionList()
    {
        $VhcListSearch = new VhcList();
        $VhcListProvider = $VhcListSearch->search(Yii::$app->request->queryParams);

        return $this->renderPartial('_list', [
            'VhcListSearch' => $VhcListSearch,
            'VhcListProvider' => $VhcListProvider,
        ]);
    }
    public function actionListoption()
    {
        $VhcListOptionSearch = new VhcListOption();
        $VhcListOptionProvider = $VhcListOptionSearch->search(Yii::$app->request->queryParams);

        return $this->renderPartial('_listoption', [
            'VhcListOptionSearch' => $VhcListOptionSearch,
            'VhcListOptionProvider' => $VhcListOptionProvider,
        ]);
    }
    public function actionSavegrouplist()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;
            $postValue = Yii::$app->request->post('data');
            $group_name= $postValue['group_name'];
            $model_id= $postValue['model_id'];
            $record_status= $postValue['record_status'];
            $model = new VhcGrouplist();
            if($postValue['id'] == '0'){
                $model->model_id = $model_id;
                $model->group_name = $group_name;
                $model->record_status = $record_status;
            }else{
                $model = VhcGrouplist::findOne($postValue['id']);
                $model->model_id = $model_id;
                $model->group_name = $group_name;
                $model->record_status = $record_status;
            }

            if ($model->validate()) {
                $statusSave = $model->save();
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }

            return $statusSave;
        }
    }
    public function actionSavelist()
    {
            if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;
            $postValue = Yii::$app->request->post('data');
            $group_id= $postValue['group_id'];
            $list_name= $postValue['list_name'];
            $record_status= $postValue['record_status'];
            $model = new VhcList();
            if($postValue['id'] == 0){
                $model->group_id = $group_id;
                $model->list_name = $list_name;
                $model->record_status = $record_status;
            }else{
                $model = VhcGrouplist::findOne($postValue['id']);
                $model->group_id = $group_id;
                $model->group_name = $group_name;
                $model->record_status = $record_status;
            }
            
            if ($model->validate()) {
                $statusSave = $model->save();
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }

            return $statusSave;
        }
    }
    public function actionSavelistoption()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;
            $postValue = Yii::$app->request->post('data');
            // ['list_id', 'option_name', 'option_value', ''],
            $list_id= $postValue['list_id'];
            $option_name= $postValue['option_name'];
            $option_value= $postValue['option_value'];
            $record_status= $postValue['record_status'];
            $conment = $postValue['comment'];
            $model = new VhcListOption();
            if($postValue['id'] == '0'){
                $model->list_id = $list_id;
                $model->option_name = $option_name;
                $model->option_value = $option_value;
                $model->record_status = $record_status;
                $model->comment = $conment;
            }else{
                $model = VhcGrouplist::findOne($postValue['id']);
                $model->list_id = $list_id;
                $model->option_name = $option_name;
                $model->option_value = $option_value;
                $model->record_status = $record_status;
            }
            
            if ($model->validate()) {
                $statusSave = $model->save();
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }

            return $statusSave;
        }
    }
    public function actionSaveservicetype()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;
            $postValue = Yii::$app->request->post('data');
            $servicetype_name= $postValue['servicetype_name'];
            $record_status= $postValue['record_status'];
            $model = new VhcServicetype();
            if($postValue['id'] == 0){
                $model->servicetype_name = $servicetype_name;
                $model->record_status = $record_status;
            }else{
                $model =  VhcServicetype::findOne($postValue['id']);
                $model->servicetype_name = $servicetype_name;
                $model->record_status = $record_status;
            }
            
            if ($model->validate()) {
                $statusSave = $model->save();
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }

            return $statusSave;
        }
    }
    public function actionSaveblame()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;
            $postValue = Yii::$app->request->post('data');
            $blame_name= $postValue['blame_name'];
            $record_status= $postValue['record_status'];
            $model = new VhcBlame();
            if($postValue['id'] == 0){
                $model->blame_name = $blame_name;
                $model->record_status = $record_status;
            }else{
                $model =  VhcBlame::findOne($postValue['id']);
                $model->blame_name = $blame_name;
                $model->record_status = $record_status;
            }
            
            if ($model->validate()) {
                $statusSave = $model->save();
            } else {
                $errors = $model->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }

            return $statusSave;
        }
    }


    public function actionSelectgroup()
    {
        $group = VhcGrouplist::find()->where(['!=','record_status','99'])->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $group;
    }
    public function actionSelectmodel()
    {
        $model = VhcModel::find()->where(['!=','record_status','99'])->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $model;
    }
    public function actionSelectlist()
    {
        $list = VhcList::find()->where(['!=','record_status','99'])->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $list;
    }
    
  
    public function actionDeletegroup()
    {
        $getValue = Yii::$app->request->post('data');
        $group = VhcGrouplist::findOne($getValue);
        $group->record_status = 99;
        $statussave = $group->save();
        return $statussave;
    }
    public function actionDeletelist()
    {
        $getValue = Yii::$app->request->post('data');
        $list = VhcList::findOne($getValue);
        $list->record_status = 99;
        $statussave = $list->save();
        return $statussave;
    }
    public function actionDeletepotion()
    {
        $getValue = Yii::$app->request->post('data');
        $option = VhcListOption::findOne($getValue);
        $option->record_status = 99;
        $statussave = $option->save();
        return $statussave;
    }
    public function actionDeleteservicetype()
    {
        $getValue = Yii::$app->request->post('data');
        $option = VhcServicetype::findOne($getValue);
        $option->record_status = 99;
        $statussave = $option->save();
        return $statussave;
    }




    public function actionEditgroup()
    {
        $getValue = Yii::$app->request->get('data');
        $group = VhcGrouplist::find()->where(['id'=>$getValue])->asArray()->one();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $group;
    }
    public function actionEditmodel()
    {
        $getValue = Yii::$app->request->get('data');
        $Model = VhcModel::find()->where(['id'=>$getValue])->asArray()->one();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $Model;
    }
    public function actionEditlist()
    {
        $getValue = Yii::$app->request->get('data');
        $list = VhcList::find()->where(['id'=>$getValue])->asArray()->one();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $list;
    }
    public function actionEditpotion()
    {
        $getValue = Yii::$app->request->get('data');
        $option = VhcListOption::find()->where(['id'=>$getValue])->asArray()->one();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $option;
    }
    public function actionEditservicetype()
    {
        $getValue = Yii::$app->request->get('data');
        $servicetype = VhcServicetype::find()->where(['id'=>$getValue['id']])->asArray()->one();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $servicetype;
    }
    public function actionEditblame()
    {
        $getValue = Yii::$app->request->get('data');
        $Blame = VhcBlame::find()->where(['id'=>$getValue['id']])->asArray()->one();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $Blame;
    }
    public function actionServicetype()
    {
        $VhcServicetypeSearch = new VhcServicetype();
        $VhcServicetypeProvider = $VhcServicetypeSearch->search(Yii::$app->request->queryParams);

        return $this->renderPartial('_servicetype', [
            'VhcServicetypeSearch' => $VhcServicetypeSearch,
            'VhcServicetypeProvider' => $VhcServicetypeProvider,
        ]);
    }
    



    
    /*public function actionEditservicetype()
    {
        $getValue = Yii::$app->request->get('data');
        $group = VhcGrouplist::find()->where(['id'=>$getValue])->asArray()->one();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $group;
    }*/
}