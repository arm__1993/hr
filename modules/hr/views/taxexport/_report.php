<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun";
        font-size: 16px;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }


</style>
<div class="container">
    <table class="rpt">
        <tr>
            <th class="container">สอบถามการลง font thai ใน mpdf 6.0 แล้วเป็นสี่เหลี่ยมครับ </th>
            <th>Table Header</th>
            <th>ฟหกดฟหกดฟหก</th>
        </tr>
        <tr>
            <td>ฟหกดฟหกดฟหก</td>
            <td>Table cell</td>
            <td>Table cell</td>
        </tr>
        <tr>
            <td>Table cell</td>
            <td>Table cell</td>
            <td>Table cell</td>
        </tr>
    </table>
</div>

