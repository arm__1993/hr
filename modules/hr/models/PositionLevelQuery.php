<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[PositionLevel]].
 *
 * @see PositionLevel
 */
class PositionLevelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PositionLevel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PositionLevel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
