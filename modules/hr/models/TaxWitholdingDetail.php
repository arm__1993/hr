<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "tax_witholding_detail".
 *
 * @property integer $id
 * @property integer $tax_witholding_id
 * @property integer $template_id
 * @property string $template_name
 * @property string $tax_rate
 * @property string $date_pay
 * @property string $emp_idcard
 * @property string $deduct_amount
 * @property string $wht_amount
 * @property integer $tax_section_id
 * @property string $tax_section_name
 * @property string $accounting_code
 */
class TaxWitholdingDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_witholding_detail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_witholding_id', 'template_id', 'tax_section_id'], 'integer'],
            [['tax_rate', 'deduct_amount', 'wht_amount'], 'number'],
            [['template_name', 'tax_section_name'], 'string', 'max' => 255],
            [['date_pay'], 'string', 'max' => 7],
            [['emp_idcard'], 'string', 'max' => 13],
            [['accounting_code'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tax_witholding_id' => 'Tax Witholding ID',
            'template_id' => 'Template ID',
            'template_name' => 'Template Name',
            'tax_rate' => 'Tax Rate',
            'date_pay' => 'Date Pay',
            'emp_idcard' => 'Emp Idcard',
            'deduct_amount' => 'Deduct Amount',
            'wht_amount' => 'Wht Amount',
            'tax_section_id' => 'Tax Section ID',
            'tax_section_name' => 'Tax Section Name',
            'accounting_code' => 'Accounting Code',
        ];
    }
}
