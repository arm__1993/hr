-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 28, 2017 at 10:10 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.27-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_promise`
--

CREATE TABLE `emp_promise` (
  `id` int(11) NOT NULL,
  `emp_promise_type_id` int(11) DEFAULT NULL,
  `emp_data_id` int(11) DEFAULT NULL,
  `emp_idcard` varchar(13) DEFAULT NULL,
  `promise_code` varchar(20) DEFAULT NULL,
  `promise_name` varchar(200) DEFAULT NULL,
  `promise_detail` text COMMENT 'รายละเอียดสัญญาจ้าง',
  `promise_bond` text COMMENT 'รายละเอียดสัญญาค้ำประกัน',
  `promise_attach` text COMMENT 'รายละเอียดบันทึกแนบ',
  `promise_sign_status` tinyint(1) DEFAULT NULL COMMENT 'สถานะเช็นสัญญา 0:ยังไม่เช็น 1:เช็น',
  `promise_sign_date` datetime DEFAULT NULL COMMENT 'วันที่เช็นสัญญา',
  `create_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_promise`
--
ALTER TABLE `emp_promise`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_promise`
--
ALTER TABLE `emp_promise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
