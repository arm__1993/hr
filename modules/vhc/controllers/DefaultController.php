<?php

namespace app\modules\vhc\controllers;

use yii\web\Controller;
use app\modules\vhc\controllers\MasterController;
/**
 * Default controller for the `vhc` module
 */
class DefaultController extends MasterController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionZyx()
    {
        return $this->render('zyx');
    }
    public function actionXyz()
    {
        return $this->render('xyz');
    }
}
