
<?php

/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 8/10/2559
 * Time: 13:48
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );
    
  });
 
});
JS;


$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานจำนวนใบสั่งซ่อมแต่ละประเภทซ่อม</h3>


    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>        
        
        <div class="box-body">

                <div class="row">

                <form method="post" action="count_orderrepair">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key=>$value) {
                                $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                            }
                            ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                         <!-- <label>เลือกเดือน</label>
                         &nbsp;
                         <div class="btn-group">
                            <select class="form-control" name="month" id="month" style="width: 150px">
                                <option value="">เลือกเดือน</option>
                                    <?php
                                    /*for ($i=1 ; $i<=12 ; $i++ ){
                                    $sel = ($i==date('m')) ? 'selected="selected"': '';
                                    echo '<option value="'.$i.'" '.$sel.'>'.\app\api\DateTime::convertMonth($i).'</option>';
                                    }*/
                                    ?>
                            </select>
                        </div>
                        &nbsp;&nbsp; -->
                        <label>เลือกปี</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="year" id="year">
                                <?php
                                $get_year = ApiReport::getAppYear();
                                //$year = ($get_year['get_year']);
                                foreach ($get_year as $key=>$value) {
                                    $sel = ($select_year==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                $countyear = count($years);
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                    <label>เลือกช่าง</label>
                    &nbsp;
                    <div class="btn-group">
                    <input type="radio" name="technician" value="1" checked> ช่างรถเล็ก
                    &nbsp;
                    <input type="radio" name="technician" value="2"> ช่างรถใหญ่
                    </div>
                    </center>                      

                    <center><br>
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/time/btn_reportorder">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                    </center>
                    </div>
                    <!--/col-md-12-->
                </form>
            </div>
            <!--/row-->
        </div>
        <!--/box-body-->
<?php if($query) {?>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-info">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                  <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1" data-toggle="tab">
                                            งานเช็คระยะ
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_2" data-toggle="tab">
                                            งานเช็คระยะ+งานซ่อมทั่วไป
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_3" data-toggle="tab">
                                            งานซ่อมทั่วไป
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_4" data-toggle="tab">
                                            งานฟรี
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                    ระยะเวลาในการซ่อม
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                                                    ม.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                    ก.พ.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                    มี.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    เม.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    มิ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ส.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ต.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ธ.ค.
                                                    </th>           
                                                </tr>
                                            </thead>
                                                <tbody>
                                                <tr role="row" class="even">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                        1 วัน
                                                    </th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMDiffDateResult1'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                        2 วัน
                                                    </th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMDiffDateResult2'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                        3 วัน
                                                    </th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMDiffDateResult3'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                        4-7 วัน
                                                    </th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMDiffDateResult47'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                        8-14 วัน
                                                    </th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMDiffDateResult814'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                        15-30 วัน
                                                    </th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMDiffDateResult1530'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                        30 วันขึ้นไป
                                                    </th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMDiffDateResultmore30'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>รวม</th>
                                                    <?php foreach ($resultDreparirDistance AS $value) {?>
                                                        <td><?php echo $value['SUMAllResult'];?></td>
                                                    <?php } ?>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                      <!-- /.tab-pane -->
                                      <div class="tab-pane" id="tab_2">
                                        <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                    ระยะเวลาในการซ่อม
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                                                    ม.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                    ก.พ.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                    มี.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    เม.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    มิ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ส.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ต.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ธ.ค.
                                                    </th>           
                                                </tr>
                                            </thead>

                                            <tbody>
                                            <tr role="row" class="even">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    1 วัน
                                                </th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult1'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    2 วัน
                                                </th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult2'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="even">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    3 วัน
                                                </th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult3'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    4-7 วัน
                                                </th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult47'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    8-14 วัน
                                                </th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult814'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    15-30 วัน
                                                </th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult1530'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    30 วันขึ้นไป
                                                </th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResultmore30'];?></td>
                                                <?php } ?>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>รวม</th>
                                                <?php foreach ($resultDreparirIntersect AS $value) {?>
                                                    <td><?php echo $value['SUMAllResult'];?></td>
                                                <?php } ?>
                                            </tr>
                                            </tfoot>
                                            </table>
                                      </div>
                                      <!-- /.tab-pane -->
                                      <div class="tab-pane" id="tab_3">
                                        <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                    ระยะเวลาในการซ่อม
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                                                    ม.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                    ก.พ.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                    มี.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    เม.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    มิ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ส.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ต.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ธ.ค.
                                                    </th>           
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr role="row" class="even">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    1 วัน
                                                </th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult1'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    2 วัน
                                                </th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult2'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="even">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    3 วัน
                                                </th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult3'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    4-7 วัน
                                                </th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult47'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    8-14 วัน
                                                </th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult814'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    15-30 วัน
                                                </th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResult1530'];?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    30 วันขึ้นไป
                                                </th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMDiffDateResultmore30'];?></td>
                                                <?php } ?>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>รวม</th>
                                                <?php foreach ($resultDreparirGenaral AS $value) {?>
                                                    <td><?php echo $value['SUMAllResult'];?></td>
                                                <?php } ?>
                                            </tr>
                                            </tfoot>
                                            </table>
                                      </div>
                                      <!-- /.tab-pane -->
                                      <div class="tab-pane" id="tab_4">
                                       <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                    ระยะเวลาในการซ่อม
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                                                    ม.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                    ก.พ.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                    มี.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    เม.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    มิ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ส.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ก.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ต.ค.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    พ.ย.
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                    ธ.ค.
                                                    </th>           
                                                </tr>
                                            </thead>
                                           <tbody>
                                           <tr role="row" class="even">
                                               <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                   1 วัน
                                               </th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMDiffDateResult1'];?></td>
                                               <?php } ?>
                                           </tr>
                                           <tr role="row" class="odd">
                                               <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                   2 วัน
                                               </th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMDiffDateResult2'];?></td>
                                               <?php } ?>
                                           </tr>
                                           <tr role="row" class="even">
                                               <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                   3 วัน
                                               </th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMDiffDateResult3'];?></td>
                                               <?php } ?>
                                           </tr>
                                           <tr role="row" class="odd">
                                               <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                   4-7 วัน
                                               </th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMDiffDateResult47'];?></td>
                                               <?php } ?>
                                           </tr>
                                           <tr role="row" class="odd">
                                               <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                   8-14 วัน
                                               </th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMDiffDateResult814'];?></td>
                                               <?php } ?>
                                           </tr>
                                           <tr role="row" class="odd">
                                               <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                   15-30 วัน
                                               </th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMDiffDateResult1530'];?></td>
                                               <?php } ?>
                                           </tr>
                                           <tr role="row" class="odd">
                                               <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                   30 วันขึ้นไป
                                               </th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMDiffDateResultmore30'];?></td>
                                               <?php } ?>
                                           </tr>
                                           </tbody>
                                           <tfoot>
                                           <tr>
                                               <th>รวม</th>
                                               <?php foreach ($resultDreparirFree AS $value) {?>
                                                   <td><?php echo $value['SUMAllResult'];?></td>
                                               <?php } ?>
                                           </tr>
                                           </tfoot>
                                            </table>
                                      </div>
                                      <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                  </div>
                                  <!-- nav-tabs-custom -->
                            </div>
                            <!-- col-sm-12 -->
                        </div>
                        <!-- row -->
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA TABLE -->
            </div>
        </div>        
    </section>
<?php } ?>
</div><!--box-->
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>