<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-chart/jquery.orgchart.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerCssFile(Yii::$app->request->baseUrl . '/js/hr/jquery-chart/font-awesome.min.css', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/js/hr/jquery-chart/jquery.orgchart.min.css', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);

$this->registerCss("


#chart-container {
  position: relative;
  display: inline-block;
  top: 5px;
  left: 5px;
  height: 600px;
  width: calc(100% - 24px);

  overflow: auto;
  text-align: center;
}

.orgchart { background: #fff; }
.orgchart td.left, .orgchart td.right, .orgchart td.top { border-color: #aaa; }
.orgchart td>.down { background-color: #aaa; }
.orgchart .middle-level .title { background-color: #006699; }
.orgchart .middle-level .content { border-color: #006699; }
.orgchart .product-dept .title { background-color: #009933; }
.orgchart .product-dept .content { border-color: #009933; }
.orgchart .rd-dept .title { background-color: #993366; }
.orgchart .rd-dept .content { border-color: #993366; }
.orgchart .pipeline1 .title { background-color: #996633; }
.orgchart .pipeline1 .content { border-color: #996633; }
.orgchart .frontend1 .title { background-color: #cc0066; }
.orgchart .frontend1 .content { border-color: #cc0066; }

orgchart .node .dcontent {
    box-sizing: border-box;
    width: 100%;
    height: 20px !important;
    font-size: 11px;
    line-height: 18px;
    border: 1px solid rgba(217,83,79,.8);
    border-radius: 0 0 4px 4px;
    text-align: center;
    background-color: #fff;
    color: #333;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}


.orgchart .node {
    display: inline-block;
    position: relative;
    margin: 0;
    padding: 3px;
    border: 2px dashed transparent;
    text-align: center;
    width: 120px !important;
}

.orgchart .top-level .title {
background-color: #006699;
}
.orgchart .top-level .content {
border-color: #006699;
}
.orgchart .middle-level .title {
background-color: #009933;
}
.orgchart .middle-level .content {
border-color: #009933;
}
.orgchart .bottom-level .title {
background-color: #993366;
}
.orgchart .bottom-level .content {
border-color: #993366;
}

.orgchart .node .title {
    background-color: none;
}

.node .default-level  {
    background-color: #009933;
}

.orgchart .recruit-enabled .title {
    background-color: #d9534f;
}

.orgchart .recruit-enablednotnull .title {
    background-color: #009933;
}

.orgchart .recruit-enablednot .title {
    background-color: #FFBF00;
}


.orgchart .recruit-disbled .title {
    background-color: #000000;
}

.orgchart .recruit-close .title {
    background-color: #7F8C8D;
}

.orgchart .recruit-open .title  {
    background-color: #FF0000;
    
    
    
   
    
    
    
    
}


.node .recruit-enablednodeder{

  background-color: #FFBF00;

}



.btnfa {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 10px 10px;
    font-size: 4px;
    cursor: pointer;
}

/* Darker background on mouse-over */
.btnfa:hover {
    background-color: RoyalBlue;
}


");


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/OrgchartViewOnly.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดการผังองค์กร</a>
                </li>
                <li class="active">เรียกดูผังโครงสร้างองค์กร</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="form-group">
                <center><label for="monthselect" class="col-sm-4 control-label">ชื่อบริษัท</label>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="company_id" id="company_id" required>
                            <option value="all">--- ทั้งหมด ---</option>
                            <?php
                            foreach ($arrCompany as $value) {
                                ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <button type="button" id="btnExport"
                                class="btn btn-sm btn-success btn-sm"><i class="fa fa fa-search"></i> ค้นหา
                        </button>
                    </div>
                </center>
            </div>

            <div id="chart-container"></div>

        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">สรรหา</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="btnSave">
                <div class="modal-body">
                    <input id="oid" type="hidden">
                    <input id="idorg" type="hidden">
                    <input id="idref" type="hidden">
                    <select id="reffser" class="col-lg-12">
                        <option value="">เลือก</option>
                        <option value="1">สรรหา</option>
                        <option value="2">ปิดสรรหา</option>
                        <option value="99">ปิดตำแหน่ง</option>
                    </select>
                </div>
            </form>
            <hr>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" onclick="saveref()">ยืนยันการสันหา</button>
            </div>
        </div>
    </div>
</div>
