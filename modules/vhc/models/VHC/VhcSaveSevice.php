<?php

namespace app\modules\vhc\models\VHC;

use Yii;

/**
 * This is the model class for table "vhc_save_sevice".
 *
 * @property integer $id
 * @property integer $ahistory_id
 * @property integer $vhc_model_id
 * @property integer $vhc_master_id
 * @property integer $cvhc_main_id
 * @property integer $mail_in
 * @property string $Idcrad_sa
 * @property string $listNmae
 * @property string $comment
 * @property string $sumprice
 * @property string $sumvat
 * @property string $suminvatprice
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcSaveSevice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_save_sevice';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ahistory_id', 'vhc_model_id', 'vhc_master_id', 'cvhc_main_id', 'mail_in', 'Idcrad_sa', 'listNmae', 'comment', 'sumprice', 'sumvat', 'suminvatprice'], 'required'],
            [['ahistory_id', 'vhc_model_id', 'vhc_master_id', 'cvhc_main_id', 'mail_in', 'status'], 'integer'],
            [['comment'], 'string'],
            [['sumprice', 'sumvat', 'suminvatprice'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['Idcrad_sa', 'create_by', 'update_by'], 'string', 'max' => 13],
            [['listNmae'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ahistory_id' => 'Ahistory ID',
            'vhc_model_id' => 'Vhc Model ID',
            'vhc_master_id' => 'Vhc Master ID',
            'cvhc_main_id' => 'Cvhc Main ID',
            'mail_in' => 'Mail In',
            'Idcrad_sa' => 'Idcrad Sa',
            'listNmae' => 'List Nmae',
            'comment' => 'Comment',
            'sumprice' => 'Sumprice',
            'sumvat' => 'Sumvat',
            'suminvatprice' => 'Suminvatprice',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
