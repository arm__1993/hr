<?php

namespace app\modules\vhc\models\VHC;

use Yii;

/**
 * This is the model class for table "vhc_save_voice".
 *
 * @property integer $id
 * @property integer $vhc_save_list_id
 * @property integer $part
 * @property integer $status
 * @property integer $create_by
 * @property integer $update_by
 * @property integer $create_date
 * @property integer $update_date
 */
class VhcSaveVoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_save_voice';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vhc_save_list_id', 'part'], 'required'],
            [['vhc_save_list_id', 'part', 'status', 'create_by', 'update_by', 'create_date', 'update_date'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vhc_save_list_id' => 'Vhc Save List ID',
            'part' => 'Part',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
