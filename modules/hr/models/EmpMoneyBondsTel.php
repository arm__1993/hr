<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_money_bonds_tel".
 *
 * @property integer $id
 * @property integer $emp_data_id
 * @property integer $emp_money_bonds_id
 * @property string $tel
 * @property integer $main_active
 */
class EmpMoneyBondsTel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_money_bonds_tel';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emp_data_id', 'emp_money_bonds_id', 'main_active'], 'integer'],
            [['tel'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_data_id' => 'Emp Data ID',
            'emp_money_bonds_id' => 'Emp Money Bonds ID',
            'tel' => 'Tel',
            'main_active' => 'Main Active',
        ];
    }
}
