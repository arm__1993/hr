<?php

namespace app\modules\wssale\controllers;
use app\modules\wssale\models\Project;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;


class ProjectController extends ActiveController
{
    public $modelClass = 'app\modules\wssale\models\Project';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	

}
