<?php

namespace app\modules\wssale\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wssale\models\Cusint;
use yii\helpers\ArrayHelper;


class CusintController extends ActiveController
{
    public $modelClass = 'app\modules\wssale\models\Cusint';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	
	public function actionSearchcusintformobile()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $datacusint = new Cusint();
        $showdatacusint = $datacusint->searchcusintformobile($cusno);
        return $showdatacusint;
    }

}
