<?php
use app\modules\hr\controllers\PersonalController;

?>

<div class="row">

    
    <div class="col-md-12">

        <!--ประวัติการทำงานเดิม-->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">ประวัติการทำงานเดิม</h3>
            </div>
            <div class="box-body">
                <div class="pull-right" style="margin-bottom:3px"> 
                    <button type="button" style="" class="btn btn-sm  <?php echo ($optional['viewOnly'] == true) ? 'empDisplayNone':''; ?>" onclick="showAddWorkHistory(1)">เพิ่มข้อมูล</button>
                </div>
               <table id="workhis_tb_1"  data-del="[]" class="table table-striped table-bordered tb_workhis_series">
                    <tr>
                        <th>ลำดับ</th>
                        <th>บริษัท</th>
                        <th>ตำแหน่งงาน</th>
                        <th>ระยะเวลา</th>
                        <th>เงินเดือน</th>
                        <th>การจัดการ</th>
                    </tr>
                    <?php if(empty($empWorkHis[1])){ ?>
                    <tr r class="tr_emty_data">
                        <td style="text-align:center" colspan="6">
                            <span>ไม่มีข้อมูล</span>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($empWorkHis[1])){ ?>
                    <?php foreach($empWorkHis[1] as $no => $wHis){ 
                    ?>
                        
                    <tr class="rdata" data-inf='<?php 
                        
                        $wHis->work_since_full = PersonalController::toggleToDataPicker($wHis->work_since_full);
                        $wHis->work_until_full = PersonalController::toggleToDataPicker($wHis->work_until_full);
                        echo json_encode($wHis->attributes);?>'>
                        <td><?php echo ($no +1);?></td>
                        <td><?php echo $wHis->old_work_company;?></td>
                        <td><?php echo $wHis->old_work_position;?></td>
                        <td class="calDateDiff">
                        <?php 
                            $t1 = new DateTime(PersonalController::toggleToDataPicker($wHis->work_since_full));
                            $t2 = new DateTime(PersonalController::toggleToDataPicker($wHis->work_until_full));
                            $interval = $t1->diff($t2);
                            echo $interval->format('%y ปี').' '.$interval->format('%m เดือน');
                        ?>
                        </td>
                        <td><?php echo $wHis->old_work_salary;?></td>
                        <td>
                            <a href="#" class="btn btn-info btn-sm" onclick="showAddWorkHistory(1,this)" >
                                <span class="glyphicon glyphicon-edit"></span> แก้ไข
                            </a>
                            <a href="#" class="btn btn-danger btn-sm" onclick="delTrWorkhis(this)" style="margin-left:5px">
                                <span class="glyphicon glyphicon-trash"></span> ลบ 
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </table>


            </div>
        </div>

        <!--ประวัติการเข้า-ออกงานกลุ่มนกเงือก-->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">ประวัติการเข้า-ออกงานกลุ่มนกเงือก</h3>
            </div>
            <div class="box-body">

               <table class="table table-striped table-bordered">
                    <tr>
                        <th>ลำดับ</th>
                        <th>วันที่เริ่มงาน</th>
                        <th>วันที่ผ่านงาน</th>
                        <th>เหตุผลที่ลาออก</th>
                        <th>รายละเอียดการลาออก</th>
                        <th>รวมระยะเวลา</th>
                    </tr>
                    <?php if(empty($empWorkHis[2])){ ?>
                    <tr>
                        <td style="text-align:center" colspan="6">
                            <span>ไม่มีข้อมูล</span>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($empWorkHis[2])){ ?>
                    <?php foreach($empWorkHis[2] as $no => $wHis){ ?>
                        
                    <tr>
                        <td><?php echo ($no +1);?></td>
                        <td><?php echo $wHis->work_since_full;?></td>
                        <td><?php echo $wHis->work_until_full;?></td>
                        <td>
                            <?php echo $wHis->reason_out_type;?>
                        </td>
                        <td><?php echo $wHis->reason_out_detail;?></td>
                        <td>
                             <?php 
                                $t1 = new DateTime($wHis->work_since_full);
                                $t2 = new DateTime($wHis->work_until_full);
                                $interval = $t1->diff($t2);
                                echo $interval->format('%y');
                            ?>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </table>


            </div>
        </div>

        <!--ประวัติการทำงานที่ได้รับมอบหมายกลุ่มนกเงือก-->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">ประวัติการทำงานที่ได้รับมอบหมายกลุ่มนกเงือก</h3>
            </div>
            <div class="box-body">
                <div class="pull-right" style="margin-bottom:3px"> 
                    <button type="button" style="" class="btn btn-sm  <?php echo ($optional['viewOnly'] == true) ? 'empDisplayNone':''; ?>" onclick="showAddWorkHistory(2)">เพิ่มข้อมูล</button>
                </div>
               <table id="workhis_tb_2"   class="table table-striped table-bordered">
                    <tr>
                        <th>ลำดับ</th>
                        <th>วันที่เริ่มงาน</th>
                        <th>วันที่ผ่านงาน</th>
                        <th>Position Code</th>
                        <th>ชื่อตำแหน่ง</th>
                        <th>งานที่มอบหมาย</th>
                        <th>ผลการดำเนินงาน</th>
                        <th>การจัดการ</th>
                    </tr>
                    <?php if(empty($empWorkHis[3])){ ?>
                    <tr class="tr_emty_data">
                        <td style="text-align:center"  colspan="6">
                            <span>ไม่มีข้อมูล</span>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($empWorkHis[3])){ ?>
                    <?php foreach($empWorkHis[3] as $no => $wHis){ ?>
                        
                    <tr class="rdata" data-inf='<?php 
                        $wHis->work_since_full = PersonalController::toggleToDataPicker($wHis->work_since_full);
                        $wHis->work_until_full = PersonalController::toggleToDataPicker($wHis->work_until_full);
                        echo json_encode($wHis->attributes);?>'>
                        <td><?php echo ($no +1);?></td>
                        <td><?php echo $wHis->work_since_full;?></td>
                        <td><?php echo $wHis->work_until_full;?></td>
                        <td><?php echo $wHis->position_code;?></td>
                        <td><?php echo $wHis->old_work_position;?></td>
                        <td><?php echo $wHis->assigned;?></td>
                        <td><?php echo $wHis->work_performance;?></td>
                        <td>
                             <a href="#" class="btn btn-info btn-sm" onclick="showAddWorkHistory(2,this)" >
                                <span class="glyphicon glyphicon-edit"></span> แก้ไข
                            </a>
                            <a href="#" class="btn btn-danger btn-sm" onclick="delTrWorkhis(this)" style="margin-left:5px">
                                <span class="glyphicon glyphicon-trash"></span> ลบ 
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </table>


            </div>
        </div>
 
    </div>
</div>

<div class="modal fade modal-wide" id="modalAddWorkHistory1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มสัญญา</h4>
            </div>
            <div class="modal-body" width="100%" >
           
                <div class="row">

                    <form role="form" id="empWorkHistoryForm" class="form-horizontal  col-sm-12" data-refopen="">
                        <input type="hidden" value="" id="id" name="id" />
                        <div class="col-sm-6">
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-4">
                                    <label class="control-label" for="old_work_company">บริษัท:</label>
                                </div>
                                <div class="col-sm-8">
                                     <input type="text" 
                                        class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                        id="old_work_company" 
                                        name="old_work_company" 
                                        value="" />
                                </div>
                            </div>

                            <div class="row form-group col-sm-12">
                                <div class="col-sm-4">
                                    <label class="control-label" for="company_type">ประเภทธุรกิจ:</label>
                                </div>
                                <div class="col-sm-8">
                                     <input type="text" 
                                        class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                        id="company_type" 
                                        name="company_type" 
                                        value="" />
                                </div>
                            </div>

                            <div class="row form-group col-sm-12">
                                <div class="col-sm-4">
                                    <label class="control-label" for="old_work_position">ตำแหน่ง:</label>
                                </div>
                                <div class="col-sm-8">
                                     <input type="text" 
                                        class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                        id="old_work_position" 
                                        name="old_work_position" 
                                        value="" />
                                </div>
                            </div>

                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12">
                                    <label class="control-label" for="work_detail">ลักษณะงาน / ความรับผิดชอบ:</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12">
                                    <textarea 
                                        id="work_detail" 
                                        name="work_detail" 
                                        class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                        rows="3" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="row form-group col-sm-12">
                                <div class="col-sm-4">
                                    <label class="control-label" for="reason_out_type">สาเหตุที่ออก:</label>
                                </div>
                                <div class="col-sm-8">
                                  

                                    <select id="reason_out_type" 
                                        name="reason_out_type" 
                                        class="form-control <?php echo $optional['dontouch'];?>" 
                                        <?php echo $optional['disabled'];?> >
                                        <?php echo $rel['reason_out_type'];?>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12">
                                    <label class="control-label" for="reason_out_detail">อธิบายสาเหตุที่ออก:</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12">
                                    <textarea 
                                        id="reason_out_detail" 
                                        name="reason_out_detail" 
                                        class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                        rows="3" placeholder=""></textarea>
                                </div>
                            </div>
                           
                           
                        </div>

                        <div class="col-sm-6">
                         
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-4">
                                    <label class="control-label" for="old_work_salary">เงินเดือน:</label>
                                </div>
                                <div class="col-sm-8 form-inline">
                                     <input type="text" 
                                        style="width:150px"
                                        class="form-control  workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                        id="old_work_salary" 
                                        name="old_work_salary" 
                                        value="" />
                                    <label class="control-label" for="">บาท/เดือน</label>
                                </div>
                                
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-4">
                                    <label class="control-label" for="bonus">โบนัส/คอมมิชั่น:</label>
                                </div>
                                <div class="col-sm-8 form-inline">
                                     <input type="text" 
                                        style="width:150px"
                                        class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                        id="bonus" 
                                        name="bonus" 
                                        value="" />
                                    <label class="control-label" for="">บาท/เดือน</label>
                                </div>
                                
                            </div>

                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12">
                                    <label class="control-label" for="">ระยะเวลาในการทำงาน</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                
                                <div class="col-sm-6 form-inline">
                                    <label class="control-label" for="work_since_full">ตั้งแต่:</label>
                              
                                    <div class="input-group date">
                                        <input type="text"  readonly="readonly" class="form-control" 
                                            id="work_since_full" 
                                            name="work_since_full" 
                                            placeholder="dd/mm/yyyy" />

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
    
                                </div>
                                
                                <div class="col-sm-6 form-inline">
                                    <label class="control-label" for="work_until_full">ถึง:</label>
                                

                                    <div class="input-group date">
                                        <input type="text"  readonly="readonly" class="form-control" 
                                            id="work_until_full" 
                                            name="work_until_full" 
                                            placeholder="dd/mm/yyyy" />

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12">
                                    <label class="control-label" for="">สวัสดิการที่ได้รับ</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12 form-inline">
                                 
                                    <label class="control-label col-sm-6" for="amount_uniforms">เครื่องแบบ:</label>
                                    <div class="col-sm-2">
                                        <input type="text" 
                                            style="width:75px"
                                            class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                            id="amount_uniforms" 
                                            name="amount_uniforms" 
                                            value="" />
                                    </div>
                                    <label class="control-label col-sm-4" for="">ชุด/ปี</label>
                                    
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12 form-inline">
                                    <label class="control-label col-sm-6" for="amount_food">อาหารฟรี:</label>
                                    <div class="col-sm-2">
                                        <input type="text" 
                                            style="width:75px"
                                            class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                            id="amount_food" 
                                            name="amount_food" 
                                            value="" />
                                    </div>

                                    <label class="control-label  col-sm-4" for="">มื้อ/วัน</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12 form-inline">
                                    <label class="control-label  col-sm-6" for="amount_buses">รถรับส่งพนักงาน:</label>
                                    <div class="col-sm-2">
                                        <input type="text" 
                                            style="width:75px"
                                            class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                            id="amount_buses" 
                                            name="amount_buses" 
                                            value="" />
                                    </div>

                                    <label class="control-label col-sm-4" for="">บาท/เดือน</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12 form-inline">
                                    <label class="control-label  col-sm-6" for="amount_habitat">ที่พักฟรีหรือคิดค่าเช่า:</label>
                                    <div class="col-sm-2">
                                        <input type="text" 
                                            style="width:75px"
                                            class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                            id="amount_habitat" 
                                            name="amount_habitat" 
                                            value="" />
                                    </div>

                                    <label class="control-label col-sm-4" for="">บาท/เดือน</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12 form-inline">
                                    <label class="control-label  col-sm-6" for="amount_oil">ค่าน้ำมัน/ค่าพาหนะ:</label>
                                    <div class="col-sm-2">
                                        <input type="text" 
                                            style="width:75px"
                                            class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                            id="amount_oil" 
                                            name="amount_oil" 
                                            value="" />
                                    </div>

                                    <label class="control-label col-sm-4" for="">บาท/เดือน</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12 form-inline">
                                    <label class="control-label  col-sm-6" for="amount_insurance">ประกันสุขภาพหรือค่ารักษาพยาบาล:</label>
                                    <div class="col-sm-2">
                                        <input type="text" 
                                            style="width:75px"
                                            class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                            id="amount_insurance" 
                                            name="amount_insurance" 
                                            value="" />
                                    </div>

                                    <label class="control-label col-sm-4" for="">บาท/ปี</label>
                                </div>
                            </div>
                            <div class="row form-group col-sm-12">
                                <div class="col-sm-12 form-inline">
                                    <label class="control-label  col-sm-6" for="other">อื่นๆ (ถ้ามี):</label>
                                    <div class="col-sm-2">
                                        <input type="text" 
                                            style="width:75px"
                                            class="form-control workhisDecimal <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                            id="other" 
                                            name="other" 
                                            value="" />
                                    </div>

                                    <label class="control-label col-sm-4" for=""></label>
                                </div>
                            </div>

                        </div>
    
                    </form>
                  
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addPrepareDataWorkHistory()" >เพิ่ม</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  




<div class="modal fade modal-wide" id="modalAddWorkHistory2"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มสัญญา</h4>
            </div>
            <div class="modal-body" width="100%" >
           
                <div class="row">

                     <form role="form" id="empWorkHistoryForm2" class="form-horizontal  col-sm-12" data-refopen="">
                        <input type="hidden" value="" id="id" name="id" />
                       
                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                                <label class="control-label" for="work_since_full">วันที่เริ่มงานที่ได้รับมอบหมาย:</label>
                            </div>
                            <div class="col-sm-8">
                                 
                                  
                                    <div class="input-group date">
                                        <input type="text"  readonly="readonly" class="form-control" 
                                            id="work_since_full" 
                                            name="work_since_full" 
                                            placeholder="dd/mm/yyyy" />

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>

                            </div>
                        </div>

                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                                <label class="control-label" for="work_until_full">วันที่สิ้นงานที่ได้รับมอบหมาย:</label>
                            </div>
                            <div class="col-sm-8">
                                  
                                    <div class="input-group date">
                                        <input type="text"  readonly="readonly" class="form-control" 
                                            id="work_until_full" 
                                            name="work_until_full" 
                                            placeholder="dd/mm/yyyy" />

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                                <label class="control-label" for="position_code">ตำแหน่ง:</label>
                            </div>
                            <div class="col-sm-8">
                                    <!--<input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>  
                                    id="position_code" 
                                    name="position_code" 
                                    value="" />-->

                                    <select style="width:532px;" id="position_code" name="position_code">
                                    </select>
                            </div>
                        </div>
                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                               
                            </div>
                            <div class="col-sm-8">
                                    
                                    <input type="text" 
                                    style="display:none"
                                    class="form-control " 
                                    id="old_work_position" 
                                    name="old_work_position" 
                                    value="" />
                            </div>
                        </div>

                       
                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                                <label class="control-label" for="position_code">งานที่มอบหมาย:</label>
                            </div>
                            <div class="col-sm-8">
                                    <textarea 
                                        id="assigned" 
                                        name="assigned" 
                                        class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                        rows="3" placeholder=""></textarea>
                            </div>
                        </div>

                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                                <label class="control-label" for="work_performance">ผลการดำเนินงาน:</label>
                            </div>
                            <div class="col-sm-8">
                                    <textarea 
                                        id="work_performance" 
                                        name="work_performance" 
                                        class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                        rows="3" placeholder=""></textarea>
                            </div>
                        </div>


                     
    
                    </form>
                  
                  
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addPrepareDataWorkHistoryAssign()" >เพิ่ม</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  



