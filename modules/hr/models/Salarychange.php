<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "SALARY_CHANGE".
 *
 * @property integer $SALARY_CHANGE_ID
 * @property string $EVALUATION_ID
 * @property string $SALARY_CHANGE_EMP_ID
 * @property string $SALARY_CHANGE_POSITION_NAME
 * @property string $SALARY_CHANGE_POSITION_CODE
 * @property string $SALARY_CHANGE_DATE
 * @property string $SALARY_CHANGE_ROUND
 * @property string $EVALUATION_RESULT_CHART
 * @property string $SALARY_CHANGE_TYPE
 * @property string $SALARY_CHANGE_CHART_OLD
 * @property string $SALARY_CHANGE_LEVEL_OLD
 * @property string $SALARY_CHANGE_STEP_OLD
 * @property string $SALARY_CHANGE_WAGE_OLD
 * @property string $SALARY_CHANGE_CHART_NEW
 * @property string $SALARY_CHANGE_LEVEL_NEW
 * @property string $SALARY_CHANGE_STEP_NEW
 * @property string $SALARY_CHANGE_ADD_STEP
 * @property string $SALARY_CHANGE_WAGE_NEW
 * @property string $SALARY_CHANGE_EMP_REMARK
 * @property string $SALARY_CHANGE_REMARK
 * @property string $SALARY_EFFECTIVE_DATE
 * @property string $SALARY_CHANGE_STATUS
 * @property string $SALARY_CHANGE_IN_SLIP
 * @property string $SALARY_CHANGE_CREATE_DATE
 * @property string $SALARY_CHANGE_CREATE_BY
 * @property string $SALARY_CHANGE_UPDATE_DATE
 * @property string $SALARY_CHANGE_UPDATE_BY
 * @property string $start_effective_date
 * @property string $end_effective_date
 * @property integer $statusSSO
 * @property integer $statusCalculate
 * @property integer $statusMainJob
 * @property string $SALARY_STEP_ADD_OLD
 * @property string $SALARY_STEP_ADD_NEW
 * @property integer $WHAT_CHANGE_ID
 * @property string $WHAT_CHANGE_NAME
 */
class Salarychange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SALARY_CHANGE';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SALARY_CHANGE_EMP_ID', 'SALARY_CHANGE_POSITION_NAME', 'SALARY_CHANGE_POSITION_CODE', 'SALARY_CHANGE_DATE', 'SALARY_CHANGE_ROUND', 'SALARY_CHANGE_TYPE', 'SALARY_CHANGE_STATUS', 'SALARY_CHANGE_CREATE_DATE', 'SALARY_CHANGE_CREATE_BY'], 'required'],
            [['SALARY_CHANGE_DATE', 'SALARY_EFFECTIVE_DATE', 'SALARY_CHANGE_CREATE_DATE', 'SALARY_CHANGE_UPDATE_DATE', 'start_effective_date', 'end_effective_date'], 'safe'],
            [['SALARY_CHANGE_WAGE_OLD', 'SALARY_CHANGE_WAGE_NEW', 'SALARY_STEP_ADD_OLD', 'SALARY_STEP_ADD_NEW'], 'number'],
            [['statusSSO', 'statusCalculate', 'statusMainJob', 'WHAT_CHANGE_ID'], 'integer'],
            [['EVALUATION_ID', 'EVALUATION_RESULT_CHART'], 'string', 'max' => 100],
            [['SALARY_CHANGE_EMP_ID'], 'string', 'max' => 20],
            [['SALARY_CHANGE_POSITION_NAME', 'SALARY_CHANGE_TYPE', 'SALARY_CHANGE_EMP_REMARK', 'SALARY_CHANGE_REMARK', 'SALARY_CHANGE_CREATE_BY', 'SALARY_CHANGE_UPDATE_BY'], 'string', 'max' => 250],
            [['SALARY_CHANGE_POSITION_CODE', 'SALARY_CHANGE_ROUND', 'SALARY_CHANGE_CHART_OLD', 'SALARY_CHANGE_CHART_NEW', 'WHAT_CHANGE_NAME'], 'string', 'max' => 200],
            [['SALARY_CHANGE_LEVEL_OLD', 'SALARY_CHANGE_STEP_OLD', 'SALARY_CHANGE_LEVEL_NEW', 'SALARY_CHANGE_STEP_NEW', 'SALARY_CHANGE_ADD_STEP'], 'string', 'max' => 10],
            [['SALARY_CHANGE_STATUS'], 'string', 'max' => 2],
            [['SALARY_CHANGE_IN_SLIP'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SALARY_CHANGE_ID' => 'Salary  Change  ID',
            'EVALUATION_ID' => 'Evaluation  ID',
            'SALARY_CHANGE_EMP_ID' => 'Salary  Change  Emp  ID',
            'SALARY_CHANGE_POSITION_NAME' => 'Salary  Change  Position  Name',
            'SALARY_CHANGE_POSITION_CODE' => 'Salary  Change  Position  Code',
            'SALARY_CHANGE_DATE' => 'Salary  Change  Date',
            'SALARY_CHANGE_ROUND' => 'Salary  Change  Round',
            'EVALUATION_RESULT_CHART' => 'Evaluation  Result  Chart',
            'SALARY_CHANGE_TYPE' => 'Salary  Change  Type',
            'SALARY_CHANGE_CHART_OLD' => 'Salary  Change  Chart  Old',
            'SALARY_CHANGE_LEVEL_OLD' => 'Salary  Change  Level  Old',
            'SALARY_CHANGE_STEP_OLD' => 'Salary  Change  Step  Old',
            'SALARY_CHANGE_WAGE_OLD' => 'Salary  Change  Wage  Old',
            'SALARY_CHANGE_CHART_NEW' => 'Salary  Change  Chart  New',
            'SALARY_CHANGE_LEVEL_NEW' => 'Salary  Change  Level  New',
            'SALARY_CHANGE_STEP_NEW' => 'Salary  Change  Step  New',
            'SALARY_CHANGE_ADD_STEP' => 'Salary  Change  Add  Step',
            'SALARY_CHANGE_WAGE_NEW' => 'Salary  Change  Wage  New',
            'SALARY_CHANGE_EMP_REMARK' => 'Salary  Change  Emp  Remark',
            'SALARY_CHANGE_REMARK' => 'Salary  Change  Remark',
            'SALARY_EFFECTIVE_DATE' => 'Salary  Effective  Date',
            'SALARY_CHANGE_STATUS' => 'Salary  Change  Status',
            'SALARY_CHANGE_IN_SLIP' => 'Salary  Change  In  Slip',
            'SALARY_CHANGE_CREATE_DATE' => 'Salary  Change  Create  Date',
            'SALARY_CHANGE_CREATE_BY' => 'Salary  Change  Create  By',
            'SALARY_CHANGE_UPDATE_DATE' => 'Salary  Change  Update  Date',
            'SALARY_CHANGE_UPDATE_BY' => 'Salary  Change  Update  By',
            'start_effective_date' => 'Start Effective Date',
            'end_effective_date' => 'End Effective Date',
            'statusSSO' => 'Status Sso',
            'statusCalculate' => 'Status Calculate',
            'statusMainJob' => 'Status Main Job',
            'SALARY_STEP_ADD_OLD' => 'Salary  Step  Add  Old',
            'SALARY_STEP_ADD_NEW' => 'Salary  Step  Add  New',
            'WHAT_CHANGE_ID' => 'What  Change  ID',
            'WHAT_CHANGE_NAME' => 'What  Change  Name',
        ];
    }
}
