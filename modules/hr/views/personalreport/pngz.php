<?php
/**
 * Created by PhpStorm.
 * User: watcharaphan
 * Date: 30/6/2018 AD
 * Time: 10:18
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

AppAsset::register($this);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/formEmpData.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/personalreport.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/dom-to-image-master/dist/dom-to-image.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/dom-to-image-master/dist/FileSaver.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$i = 1;
?>

<div class="container">
    <div class="row" style="margin-top: 10px;">


        <? foreach ($data as $k => $v) {

            ?>

            <div class="col-sm-4" style="padding-top: 10px !important; background-color: #EAEAEA;">
                <table id="img_pint">

                    <tr class="imglodess">
                        <td style="padding: 3px;" id="idlodeF<?php echo $i++ ?>">

                            <img src="<?php echo Yii::$app->request->BaseUrl.'/upload/emp_img/'.$v['ID_Card'].'/page.png'?>"
                                 style="margin:auto; display:block; text-align:center;"
                                 class="img-responsive">
                        </td>

                        <td style="padding: 3px;" id="idlodeB<?php echo $i++ ?>">
                            <img src="<?php echo Yii::$app->request->BaseUrl.'/upload/emp_img/'.$v['ID_Card'].'/after.png'?>"
                                 style="margin:auto; display:block; text-align:center;"
                                 class="img-responsive">
                        </td>
                    </tr>
                </table>
            </div>
        <?php } ?>


    </div>

</div>

