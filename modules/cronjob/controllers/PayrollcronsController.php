<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/08/2017 AD
 * Time: 9:55 AM
 */



namespace app\modules\cronjob\controllers;

use app\api\DateTime;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\AdddeductPermanent;
use yii\db\Exception;
use yii\db\Expression;

class PayrollcronsController extends \yii\web\Controller
{
    public $layout = 'cronjoblayout';

    private function picker_date($date) {
        $tmp = explode('-',$date);
        $datecond = $tmp[1].'-'.$tmp[0].'-'.date('d');
        $startDate = $tmp[1].'-'.$tmp[0].'-01';
        $startTime = strtotime($startDate);
        $endDate = date('Y-m-t',$startTime);

        return [
            'datecond'=>$datecond,
            'startDate'=>$startDate,
            'startTime'=>$startTime,
            'endDate'=>$endDate,
            'pay_date' =>$date,
        ];
    }

    private function convert_month($pay_date) {
        $tmp = explode('-',$pay_date);
        $month_text = DateTime::mappingMonthContraction($tmp[0]);
        $y =  ((int)$tmp[1] + 543);
        return $month_text.' '.substr($y,2);
    }


    public function actionMakelist()
    {

        $datecond = null;
        $pay_date = null;
        $startDate = null;
        $startTime = null;
        $endDate =  null;

        $injectdate = \Yii::$app->request->post('injectdate');

        if($injectdate != null && $injectdate !='') {
            $arrDate = $this->picker_date($injectdate);
            $datecond = $arrDate['datecond'];
            $pay_date = $arrDate['pay_date'];
            $startDate = $arrDate['startDate'];
            $startTime = $arrDate['startTime'];
            $endDate =  $arrDate['endDate'];
        }
        else {
            $datecond = date('Y-m-d');
            $pay_date = date('m-Y');
            $startDate = date('Y-m-01');
            $startTime = strtotime($startDate);
            $endDate = date('Y-m-t', $startTime);
        }

        //check expired IF YES/ change status
        $this->setPermanentExpired($datecond);

        $connection =  AdddeductPermanent::getDb();
        $transaction = $connection->beginTransaction();
        try {

            $arrAddDeductDetailBefore = $this->getAddDeductDetailBefore($pay_date);
            //print_r($arrAddDeductDetailBefore);

            $arrAddDeductPermanent = $this->getAddDeductPermanent($datecond);
            $_table = 'ADD_DEDUCT_DETAIL';
            $_arrTransactionCol = Adddeductdetail::getTableSchema()->getColumnNames();
            $arrData = null;
            $eff = 0;
            foreach ($arrAddDeductPermanent as $item) {

                $is_items_exist = isset($arrAddDeductDetailBefore[$item['ADD_DEDUCT_DETAIL_EMP_ID']][$item['ADD_DEDUCT_TEMPLATE_ID']]) ? true : false;

                if($item['ADD_DEDUCT_DETAIL_AMOUNT'] > 0 && $is_items_exist == false) {
                    $arrData[] = [
                        $_arrTransactionCol[0] => null,                             //ADD_DEDUCT_DETAIL_ID
                        $_arrTransactionCol[1] => $item['ADD_DEDUCT_TEMPLATE_ID'],  //ADD_DEDUCT_ID
                        $_arrTransactionCol[2] => 'ประจำเดือน :'.$this->convert_month($pay_date),              //ADD_DEDUCT_DETAIL
                        $_arrTransactionCol[3] => $pay_date,                        //ADD_DEDUCT_DETAIL_PAY_DATE
                        $_arrTransactionCol[4] => $startDate,                       //ADD_DEDUCT_DETAIL_START_USE_DATE
                        $_arrTransactionCol[5] => $endDate,                         //ADD_DEDUCT_DETAIL_END_USE_DATE
                        $_arrTransactionCol[6] => null,                             //INSTALLMENT_START
                        $_arrTransactionCol[7] => null,                             //INSTALLMENT_END
                        $_arrTransactionCol[8] => null,                             //INSTALLMENT_CURRENT
                        $_arrTransactionCol[9] => $item['ADD_DEDUCT_DETAIL_EMP_ID'],//ADD_DEDUCT_DETAIL_EMP_ID
                        $_arrTransactionCol[10] => $item['ADD_DEDUCT_DETAIL_AMOUNT'],//ADD_DEDUCT_DETAIL_AMOUNT
                        $_arrTransactionCol[11] => $item['ADD_DEDUCT_DETAIL_TYPE'], //ADD_DEDUCT_DETAIL_TYPE  1=ถาวร, 2=รายเดือน
                        $_arrTransactionCol[12] => 2,                               //ADD_DEDUCT_DETAIL_STATUS  //status ==> 2, รอคำนวณ​ใน หน้า add deduct detail
                        $_arrTransactionCol[13] => null,                            //ADD_DEDUCT_DETAIL_HISTORY_STATUS
                        $_arrTransactionCol[14] => null,                            //ADD_DEDUCT_DETAIL_GROUP_STATUS
                        $_arrTransactionCol[15] => new Expression('NOW()'),//ADD_DEDUCT_DETAIL_CREATE_DATE
                        $_arrTransactionCol[16] => '9822332951942',                 //ADD_DEDUCT_DETAIL_CREATE_BY  admin isuzu
                        $_arrTransactionCol[17] => null,                            //ADD_DEDUCT_DETAIL_UPDATE_BY
                        $_arrTransactionCol[18] => null,                            //ADD_DEDUCT_DETAIL_UPDATE_DATE
                        $_arrTransactionCol[19] => '9822332951942',                 //ADD_DEDUCT_DETAIL_RESP_PERSON admin isuzu
                        $_arrTransactionCol[20] => null,                            //dept_deduct_id
                        $_arrTransactionCol[21] => null,                            //dept_deduct_detail_id
                    ];
                }
            }

            $eff = $connection->createCommand()->batchInsert($_table, $_arrTransactionCol, $arrData)->execute();
            if($eff > 0) {
                echo "======== Save effective total $eff Records ==========";
                $transaction->commit();
            }
            else {
                echo "======== No effective Records ==========";
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    private function getAddDeductDetailBefore($pay_date)
    {

        $model = Adddeductdetail::find()->where([
            'ADD_DEDUCT_DETAIL_STATUS'=>2,
            'ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date
        ])->asArray()->all();
        $arrList = [];
        if(is_array($model)) {
            foreach ($model as $item) {
                $arrList[$item['ADD_DEDUCT_DETAIL_EMP_ID']][$item['ADD_DEDUCT_ID']] = $item;
            }
        }

        return $arrList;
    }

    private function getAddDeductPermanent($datecond)
    {
        $model1 = AdddeductPermanent::find()->where(' ADD_DEDUCT_DETAIL_STATUS=1 AND ADD_DEDUCT_DETAIL_END_USE_DATE IS NULL')->asArray()->all();
/*        echo '<pre>';
        print_r($model1);
        echo '</pre>';*/

        $model2 = AdddeductPermanent::find()->where("  ADD_DEDUCT_DETAIL_STATUS=1 AND ADD_DEDUCT_DETAIL_END_USE_DATE >= '$datecond' ")->asArray()->all();
/*        echo '<pre>';
        print_r($model2);
        echo '</pre>';*/

        $model3 = AdddeductPermanent::find()->where("  ADD_DEDUCT_DETAIL_STATUS=1 AND ADD_DEDUCT_DETAIL_END_USE_DATE = '0000-00-00' ")->asArray()->all();

        return  array_merge($model1,$model2,$model3);
    }


    //Function check Expired of Permanent ADD/DEDUCT
    private function setPermanentExpired($datecond)
    {
        $connection =  AdddeductPermanent::getDb();
        $transaction = $connection->beginTransaction();
        try {

            $model = AdddeductPermanent::find()->where([
                'ADD_DEDUCT_DETAIL_STATUS'=>1,
            ])->asArray()->all();

            $markerTimes = strtotime($datecond);
            $arrPermanentIDExpired = [];
            foreach ($model as $item) {
                if(!empty($item['ADD_DEDUCT_DETAIL_START_USE_DATE']) && $item['ADD_DEDUCT_DETAIL_START_USE_DATE'] != '0000-00-00' && !empty($item['ADD_DEDUCT_DETAIL_END_USE_DATE']) && $item['ADD_DEDUCT_DETAIL_END_USE_DATE'] != '0000-00-00' ) {
                    $endTimes  = strtotime($item['ADD_DEDUCT_DETAIL_END_USE_DATE']);
                    if($endTimes < $markerTimes) {
                        $arrPermanentIDExpired[] = $item['ID'];
                    }
                }
            }

            if(count($arrPermanentIDExpired) > 0 ) {
                AdddeductPermanent::updateAll([
                    'ADD_DEDUCT_DETAIL_STATUS'=>0,
                ],[
                    'IN','ID',$arrPermanentIDExpired
                ]);

                $transaction->commit();
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

    }


    public function actionTest()
    {
        echo 'work';
    }

}
