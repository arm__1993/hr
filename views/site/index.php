<?php
use yii\grid\GridView;

use app\api\Utility;


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Hello Admin LTE</h3>
        </div>
        <div class="box-body">
            content here
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
