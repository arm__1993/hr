-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 03, 2017 at 11:55 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-10+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_promise_mapping`
--

CREATE TABLE `emp_promise_mapping` (
  `id` int(11) NOT NULL,
  `condition_id` varchar(50) DEFAULT NULL COMMENT 'emp_promise_mapping_condition_id',
  `param_text` varchar(50) DEFAULT NULL,
  `fixable_type` tinyint(1) DEFAULT NULL COMMENT '0:ค่าคงที่, 1:ดึงจาก table',
  `db_name` varchar(100) DEFAULT NULL,
  `tb_name` varchar(100) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `sel_command` varchar(200) DEFAULT NULL,
  `default_text` varchar(250) DEFAULT NULL,
  `desc_text` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emp_promise_mapping`
--

INSERT INTO `emp_promise_mapping` (`id`, `condition_id`, `param_text`, `fixable_type`, `db_name`, `tb_name`, `field_name`, `sel_command`, `default_text`, `desc_text`) VALUES
(1, NULL, '{{company_write_at}}', 0, NULL, NULL, NULL, NULL, 'บริษัท อีซูซุเชียงราย จำกัด', NULL),
(2, '1', '{{emp_name}}', 1, 'ERP_easyhr', 'emp_data', 'Name', NULL, 'หาไม่เจอ', NULL),
(3, '1', '{{prefix}}', 1, 'ERP_easyhr', 'emp_data', 'Be', NULL, NULL, NULL),
(4, '1', '{{surename}}', NULL, 'ERP_easyhr', 'emp_data', 'Surname', NULL, NULL, NULL),
(5, '1', '{{idcard}}', NULL, 'ERP_easyhr', 'emp_data', 'ID_Card', NULL, NULL, NULL),
(6, '1', '{{addr}}', NULL, 'ERP_easyhr', 'emp_data', 'Address', NULL, NULL, NULL),
(7, '1', '{{moo}}', NULL, 'ERP_easyhr', 'emp_data', 'Moo', NULL, NULL, NULL),
(8, '1', '{{subdistrict}}', NULL, 'ERP_easyhr', 'emp_data', 'SubDistrict', NULL, NULL, NULL),
(9, '1', '{{district}}', NULL, 'ERP_easyhr', 'emp_data', 'District', NULL, NULL, NULL),
(10, '1', '{{province}}', NULL, 'ERP_easyhr', 'emp_data', 'Province', NULL, NULL, NULL),
(11, '1', '{{position}}', NULL, 'ERP_easyhr', 'emp_data', 'Position', NULL, NULL, NULL),
(12, '1', '{{age}}', NULL, 'ERP_easyhr', 'emp_data', 'Birthday', 'TIMESTAMPDIFF(YEAR,Birthday,NOW())', NULL, NULL),
(13, '12', '{{bond_prefix}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'bondsman_prefix', NULL, NULL, NULL),
(14, '12', '{{bond_name}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'bondsman_name', NULL, NULL, NULL),
(15, '12', '{{bond_surename}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'bondsman_surname', NULL, NULL, NULL),
(16, '12', '{{bond_idcard}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'bondsman_idcard', NULL, NULL, NULL),
(17, '12', '{{bond_addr}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'addr_idcard_address', NULL, NULL, NULL),
(18, '12', '{{bond_moo}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'addr_idcard_moo', NULL, NULL, NULL),
(19, '12', '{{bond_road}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'addr_idcard_road', NULL, NULL, NULL),
(20, '12', '{{bond_district}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'addr_idcard_district', NULL, NULL, NULL),
(21, '12', '{{bond_amphur}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'addr_idcard_amphur', NULL, NULL, NULL),
(22, '12', '{{bond_province}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'addr_idcard_province', NULL, NULL, NULL),
(23, '12', '{{bond_workplace}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'bondsman_work_place', NULL, NULL, NULL),
(24, '13,12', '{{bond_tel}}', 1, 'ERP_easyhr', 'emp_money_bonds_tel', 'tel', NULL, NULL, NULL),
(25, '12', '{{bond_position}}', 1, 'ERP_easyhr', 'emp_money_bonds', 'bondsman_business', NULL, NULL, NULL),
(26, '12', '{{bond_year}}', 1, 'ERP_easyhr ', 'emp_money_bonds', 'bondsman_year', NULL, NULL, NULL),
(27, '12', '{{bond_age}}', 1, 'ERP_easyhr ', 'emp_money_bonds', 'bondsman_birthday', 'TIMESTAMPDIFF(YEAR,bondsman_birthday,NOW())', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_promise_mapping`
--
ALTER TABLE `emp_promise_mapping`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_promise_mapping`
--
ALTER TABLE `emp_promise_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
