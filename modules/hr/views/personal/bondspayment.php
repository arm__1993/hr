<table class="table table-striped table-bordered">
    <tr>
        <th>วันที่จ่ายเงินค้ำประกัน</th>
        <th>รายละเอียด</th>
        <th>จำนวน/บาท</th>
    </tr>
    <?php if(empty($bmTel)){?>
        <tr><td colspan="3">ไม่มีข้อมูล</td></tr>
    <?php }?>
    <?php foreach($bmTel as $no => $obj){?>
    <tr>
        <td><?php echo $obj->bonds_date;?></td>
        <td><?php echo $obj->bonds_list;?></td>
        <td><?php echo $obj->bonds_amount;?></td>
    </tr>
    <?php }?>
</table>