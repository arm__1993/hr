<?php

namespace app\modules\hr\controllers;

use app\api\DateTime;
use app\api\Helper;
use app\api\Utility;
use app\modules\hr\apihr\ApiOT;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Department;
use app\modules\hr\models\Empdata;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\OtConfigformular;
use app\modules\hr\models\OtRequestdetail;
use app\modules\hr\models\OtRequestmaster;
use app\modules\hr\models\OtReturnTransaction;
use app\modules\hr\models\Section;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\Url;



class OvertimeController extends MasterController
{
    public $layout = 'hrlayout';
    public $idcardLogin;

    /**
     * function init() check session active or session login, if not redirect to login page
     * @return \yii\web\Response
     */
    public function init()
    {
        $session = Yii::$app->session;
        if(!$session->has('fullname') || !$session->has('idcard')) {
            return $this->redirect(array('/auth/default/logout'),302);
        }

        $this->idcardLogin = $session->get('idcard');
    }




    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {


        //echo Yii::$app->params['dbConn']['ERP_Easysale_Log'];

        //echo Yii::$app->params['PAYROLL']['example'];
        //$traffic=new Traffic();
        //$transaction=$traffic->dbConnection->beginTransaction();
        return $this->render('index');
    }

    function cleanString($text)
    {
        $utf8 = array(
            '/ไม่/' => 'c',
            '/ระบุ/' => 'C',
            '/%/' => 'n',
            '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }


    public function actionTest()
    {
        $string = "ไม่ระบุ";

        $a = $this->cleanString($string);
        echo $a;


    }

    public function actionManageot()
    {

        $arrOTActivity = ApiOT::getOTActivity();
        $arrOTProfileRoute = ApiOT::getOTProfileRoute();
        $OtConfig = ApiOT::getOTConfig();

        $arrOTReturn = ApiOT::getOTReturnBenefit();
        return $this->render('manageot', [
            'arrOTActivity' => $arrOTActivity,
            'arrOTProfileRoute' => $arrOTProfileRoute,
            'OtConfig' => $OtConfig,
            'arrOTReturn' => $arrOTReturn,
        ]);
    }


    public function actionEditot()
    {

        $arrOTActivity = ApiOT::getOTActivity();
        $arrOTProfileRoute = ApiOT::getOTProfileRoute();
        $OtConfig = ApiOT::getOTConfig();

        $arrOTReturn = ApiOT::getOTReturnBenefit();
        return $this->render('editot', [
            'arrOTActivity' => $arrOTActivity,
            'arrOTProfileRoute' => $arrOTProfileRoute,
            'OtConfig' => $OtConfig,
            'arrOTReturn' => $arrOTReturn,
        ]);
    }

    public function actionEditotitem($id)
    {
        //$getValue = Yii::$app->request->get();
        if (!empty($id)) {
            $OtMaster = OtRequestmaster::findOne($id);

            if($OtMaster===null) { echo 'ไม่พบรายการที่ค้นหา'; exit;}
            if($OtMaster->status_active == 99){ echo 'รายการนี้โดนลบไปแล้ว'; exit;}

            $OtDetail = OtRequestdetail::find()->where([
                'ot_requestmaster_id' => $id,
            ])->andWhere([
                'IN', 'is_approved', [0,2]
            ])
                ->all();

            $arrCompany = ApiHr::getWorking_company();
            $arrDepartment = ApiHr::getDepartment($OtMaster['company_id']);
            $arrDeptID = [];
            if (is_array($arrDepartment) || is_object($arrDepartment))
                foreach ($arrDepartment as $dept) array_push($arrDeptID, $dept['id']);

            $arrSection = Section::find()->where([
                'IN', 'department', $arrDeptID
            ])->andWhere([
                'status' => 1,
            ])->asArray()->all();

            $arrOTActivity = ApiOT::getOTActivity();
            $arrOTProfileRoute = ApiOT::getOTProfileRoute();
            $OtConfig = ApiOT::getOTConfig();
            $arrOTReturn = ApiOT::getOTReturnBenefit();

            $arrTemplateID = $arrTemplateID = ApiOT::getArrayDeductTemplate("1");
            $OtTemplateID = ApiHr::getPayrollConfigTemplate()->getOtTemplateID();

            $arrEmpData = ApiOT::getArrayEmpDatawithRequestID($id);
            $OtConfigFormular = OtConfigformular::findOne(1);

            return $this->render('editotitem', [
                'arrOTActivity' => $arrOTActivity,
                'arrOTProfileRoute' => $arrOTProfileRoute,
                'OtConfig' => $OtConfig,
                'arrOTReturn' => $arrOTReturn,
                'OtMaster' => $OtMaster,
                'OtDetail' => $OtDetail,
                'arrCompany' => $arrCompany,
                'arrDepartment' => $arrDepartment,
                'arrSection' => $arrSection,
                'arrTemplateID' => $arrTemplateID,
                'OtConfigFormular' => $OtConfigFormular,
                'OtTemplateID'=>$OtTemplateID,
                'arrEmpData'=>$arrEmpData,
            ]);
        } else {
            return $this->redirect(['overtime/editot'], 302);
        }
    }


    public function actionViewotitem($id)
    {
        //$getValue = Yii::$app->request->get();
        if (!empty($id)) {

            $OtMaster = OtRequestmaster::findOne($id);
            if($OtMaster===null) { echo 'ไม่พบรายการที่ค้นหา'; exit;}
            if($OtMaster->status_active == 99){ echo 'รายการนี้โดนลบไปแล้ว'; exit;}

            $OtConfigFormular = OtConfigformular::findOne(1);

            $OtDetail = OtRequestdetail::find()->where([
                'ot_requestmaster_id' => $id,
            ])
                ->andWhere('is_approved <> 99')
                ->all();

            $arrCompany = ApiHr::getWorking_company();
            $arrDepartment = ApiHr::getDepartment($OtMaster['company_id']);
            $arrDeptID = [];
            if (is_array($arrDepartment) || is_object($arrDepartment))
                foreach ($arrDepartment as $dept) array_push($arrDeptID, $dept['id']);

            $arrSection = Section::find()->where([
                'IN', 'department', $arrDeptID
            ])->andWhere([
                'status' => 1,
            ])->asArray()->all();

            $arrOTActivity = ApiOT::getOTActivity();
            $arrOTProfileRoute = ApiOT::getOTProfileRoute();
            $OtConfig = ApiOT::getOTConfig();
            $arrOTReturn = ApiOT::getOTReturnBenefit();

            $arrTemplateID = $arrTemplateID = ApiOT::getArrayDeductTemplate("1");
            $OtTemplateID = ApiHr::getPayrollConfigTemplate()->getOtTemplateID();

            $arrEmpData = ApiOT::getArrayEmpDatawithRequestID($id);

            return $this->render('viewotitem', [
                'arrOTActivity' => $arrOTActivity,
                'arrOTProfileRoute' => $arrOTProfileRoute,
                'OtConfig' => $OtConfig,
                'arrOTReturn' => $arrOTReturn,
                'OtMaster' => $OtMaster,
                'OtDetail' => $OtDetail,
                'arrCompany' => $arrCompany,
                'arrDepartment' => $arrDepartment,
                'arrSection' => $arrSection,
                'arrTemplateID' => $arrTemplateID,
                'OtConfigFormular' => $OtConfigFormular,
                'OtTemplateID'=>$OtTemplateID,
                'arrEmpData'=>$arrEmpData,
            ]);
        } else {
            return $this->redirect(['overtime/editot'], 302);
        }
    }


    public function actionHistoryot()
    {

        // $arrYear = ApiOT::getOTYear();
        // $arrMonth = DateTime::createArrayMonth();
        return $this->render('historyot');

    }

    public function actionPaidot()
    {
        return $this->render('paidot');
    }


    //list ot for hr approved
    public function actionListothr()
    {
        return $this->render('listothr');
    }






    public function actionApprovedot($id, $approved)
    {
        //$getValue = Yii::$app->request->get();
        if (!empty($id) && $approved == 'Y') {
            $OtConfigFormular = OtConfigformular::findOne(1);
            $OtMaster = OtRequestmaster::findOne($id);
            /*if($OtMaster->is_approved==1 || $OtMaster->is_approved==2) {
                echo 'รายการนี้อนุมัติไปแล้ว หรือ ตีกลับให้แก้ไข';
                exit;
            }
            */

            $OtDetail = OtRequestdetail::find()->where([
                'ot_requestmaster_id' => $id,
            ])->andWhere('is_approved <> 99')->all();

            $companyID = $OtMaster['company_id'];
            $arrCompany = ApiHr::getWorking_company();
            $arrDepartment = ApiHr::getDepartment($companyID);
            $arrDeptID = [];
            if (is_array($arrDepartment) || is_object($arrDepartment))
                foreach ($arrDepartment as $dept) array_push($arrDeptID, $dept['id']);

            $arrSection = Section::find()->where([
                'IN', 'department', $arrDeptID
            ])->andWhere([
                'status' => 1,
            ])->asArray()->all();

            $arrOTActivity = ApiOT::getOTActivity();
            $arrOTProfileRoute = ApiOT::getOTProfileRoute();
            $OtConfig = ApiOT::getOTConfig();
            $arrOTReturn = ApiOT::getOTReturnBenefit();

            $arrTemplateID = $arrTemplateID = ApiOT::getArrayDeductTemplate("1");
            $OtTemplateID = ApiHr::getPayrollConfigTemplate()->getOtTemplateID();


            $EmpData = ApiOT::getArrayEmpData($companyID,'','');
            $arrEmpData = ApiOT::getArrayEmpDatawithRequestID($id);


            //$arrCompany2 = ApiOT::getCompany();
            $opt = (Yii::$app->request->get('opt')) ? Yii::$app->request->get('opt') : null;

            return $this->render('approvedot', [
                'arrOTActivity' => $arrOTActivity,
                'arrOTProfileRoute' => $arrOTProfileRoute,
                'OtConfig' => $OtConfig,
                'arrOTReturn' => $arrOTReturn,
                'OtMaster' => $OtMaster,
                'OtDetail' => $OtDetail,
                'arrCompany' => $arrCompany,
                'arrDepartment' => $arrDepartment,
                'arrSection' => $arrSection,
                'arrTemplateID' => $arrTemplateID,
                'OtTemplateID'=>$OtTemplateID,
                'OtConfigFormular' => $OtConfigFormular,
                'EmpData'=>$EmpData,
                'rqmaster_id'=>$id,
                'approved'=>$approved,
                'arrEmpData'=>$arrEmpData,
                'opt'=>$opt,
            ]);
        } else {
            return $this->redirect(['overtime/editot'], 302);
        }

    }

    public function actionTesta()
    {

        $x = '06:10:00';
        $xx = strtotime($x);
        $y = '08:00:00';
        $yy = strtotime($y);

        echo (int)($yy-$xx)/60;
        echo '<br/>';
        $k = ($yy-$xx)/60;
        echo $z = floor($k/30);
        echo '<br/>';
        echo $q = $k%30;

    }

    /*
     * Array
(
    [ot_route] => 0
    [ot_calculate_type] => 1
    [ot_motel] => 0
    [profileroute_id] => 1
    [ot_distance] =>
    [motel_price] => 500.00
    [salary] => 15325.00
    [totaltime] => 5
    [_csrf] => OXBGYnFMMEdcKCAnNCgdC1sKPlUkBmglYDUhNzYYdn9gGS8kF3RbLQ==
)

     */



    public function actionApprovedotbyhr__old()
    {

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $leaderIDCard = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            if(trim($postValue['approved'])) {

                $id = $postValue['rqmaster_id'];
                $arrItem_approved = $postValue['item_approved'];
                $arrItem_comment = $postValue['item_comment'];


                //begin transaction
                $connection =  OtRequestmaster::getDb();
                $transaction = $connection->beginTransaction();

                try {
                    $t1 = $t2 = $t3 = $t4 = 0;
                    $allProcess = true;
                    $arrRecApprovdOT = $arrRecOTIDCard = [];

                    /* ------------------------------------------------------------- */
                    //อัพเดทตารางหัว ปรับสถานะอนุมัติ  VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0  / กลับไปปรับปรุง => 2
                    /* ------------------------------------------------------------- */
                    $OtMaster = OtRequestmaster::findOne($id);
                    $OtMaster->is_approved = 1;
                    $OtMaster->approved_byuser = $leaderIDCard;
                    $OtMaster->approved_date = date('Y-m-d');
                    $OtMaster->approved_time = date('H:i:s');
                    $OtMaster->updateby_user = $leaderIDCard;
                    $OtMaster->update_datetime = new Expression('NOW()');
                    $t1 = $OtMaster->save();


                    /* ------------------------------------------------------------- */
                    /* อัพเดทตารางรายละเอียด  ปรับสถานะอนุมัติ VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0 / กลับไปปรับปรุง => 2 */
                    /* ------------------------------------------------------------- */
                    foreach ($arrItem_approved as $key => $value) {
                        $OtDetail = OtRequestdetail::findOne($key);
                        $OtDetail->is_approved = $value;
                        $OtDetail->unapproved_remark = $arrItem_comment[$key];
                        $OtDetail->approved_byuser = $leaderIDCard;
                        $OtDetail->approved_date = date('Y-m-d');
                        $OtDetail->approved_time = date('H:i:s');
                        $pass = $OtDetail->save();

                        //เลือกเอาเฉพาะรายการที่อนุมัติผ่านเท่านั้นไปคำนวณรายได้ต่อ
                        if($pass==1 && $value==1) {
                            $arrRecApprovdOT[$OtDetail->id] = $OtDetail->toArray();
                            $arrRecOTIDCard[$OtDetail->id_card] = $OtDetail->id_card;
                        }

                        $t2 += $pass;
                    }

                    /* ------------------------------------------------------------- */
                    /* การคำนวณรายได้/ดึงค่า config / ประเภทโอที มี 3 ประเภท
                     * ot_calculate_type => ประเภทการคิดโอที 1=ปกติ, 2=นอกเวลาปกติ, 3=นอกเวลาวันหยุด  */
                    //TODO : Adjust configformular add more attribute in API::OT
                    //$OtConfig = OtConfigformular::findOne(1);
                    $arrOTMaster = $OtMaster->toArray();


                    /**  ratio การคำนวณ / อัตราการคำนวณกับเงินเดือน  **/
                    /*                    $arrUniqIDCard = array_unique($arrRecOTIDCard);
                                        $ratioOT = ($arrOTMaster['ot_calculate_type']==1) ? $OtConfig['ot_ratio_workday_clockout'] : (($arrOTMaster['ot_calculate_type']==2) ? $OtConfig['ot_ratio_holiday'] : $OtConfig['ot_ratio_holiday_clockout']);
                                        $arrEmpSalary = ApiHr::getEmpSalaryByIDCard($arrUniqIDCard);
                                        $arrEmpWorkTime = ApiHr::getEmpWorkTimeByIDCard($arrUniqIDCard);*/



                    /* ------------------------------------------------------------- */
                    /** วนรอบคำนวณรายการอนุมัติรายคน */
                    /* ------------------------------------------------------------- */
                    $arrOTApproved = $arrAllEmpApproved = [];
                    /*                    $companyID = $arrOTMaster['company_id'];  //รหัสบริษัทที่ทำโอที
                                        $workDayPerMonth = 30;  //จำนวนวันทำงานในเดือน
                                        $workHourPerDay = 8;  //จำนวนชั่วโมงทำงานต่อวัน*/
                    foreach ($arrRecApprovdOT as $approvedID => $approvedDetail) {
                        $myOTTotal = 0;
                        $myIDCard = $approvedDetail['id_card'];
                        //$mySalary  = array_pop($arrEmpSalary[$myIDCard][$companyID]);

                        //$myOTTime = $approvedDetail['time_total'];
                        //$myWagePerHour = (($mySalary/$workDayPerMonth)/$workHourPerDay);  //เงินเดือนหาร จน.วัน หาร จน. ชั่วโมง
                        //$myOTWage = $myWagePerHour * $ratioOT * $myOTTime;  //คำนวณหาค่าแรงตามชั่วโมง

                        //IF has distance, calculate distance
                        //$myWorkHour = $arrEmpWorkTime[$myIDCard];
                        //$myDistance = ($arrOTMaster['has_profile_route']==1) ? ApiOT::calculateDistanceRule($arrOTMaster,$approvedDetail,$myWorkHour) : 0;

                        //IF has motel price, set motel price
                        //$myMotel = ($arrOTMaster['has_motel_profile']==1) ? $arrOTMaster['motel_price'] :  0;
                        // $myOTTotal = Helper::displayDecimal($myOTWage + $myDistance + $myMotel); //ผลรวมโอทีทั้งหมด
                        //$remarks = "$myIDCard ได้ ratio $ratioOT | เวลาทำโอที $myOTTime ชม. | ค่าเดินทาง $myDistance  | รวม $myOTTotal";
                        $data = [
                            'id_card'=>$approvedDetail['id_card'],
                            'total_amount'=>$approvedDetail['money_total'],
                            'date_pay'=>$arrOTMaster['wage_pay_date'],
                            'ot_requestmaster_id'=>$arrOTMaster['id'],
                            'ot_requestdetail_id'=>$approvedID,
                            'return_id'=>$approvedDetail['return_id'],
                            'return_name'=>$approvedDetail['return_name'],
                            'remark'=> $approvedDetail['remak'],
                            'createby_user'=>$this->idcardLogin,
                            'create_datetime'=> DateTime::getTodayDateTime(),
                        ];


                        // array_push($arrEmpSalary[$myIDCard][$companyID],$mySalary);
                        $arrOTApproved[$myIDCard][$approvedID] = $data;
                        $arrAllEmpApproved[$myIDCard] = $myIDCard;

                    }
                    /* ------------------------------------------------------------- */

                    /* ------------------------------------------------------------- */
                    /* บันทึกรายการคำนวณลงตาราง  ot_return_transaction  Batch Insert   */
                    /* ------------------------------------------------------------- */
                    $_table = 'ot_return_transaction';
                    $_arrTransactionCol = OtReturnTransaction::getTableSchema()->getColumnNames();
                    $arrData = null; $is_used = 1; $arrEmpGetOTMoney = $arrEmpGetOTHoliday = [];
                    foreach ($arrOTApproved as $IDCard => $approvedID) {
                        foreach ($approvedID as $data) {
                            $arrData[] = [
                                $_arrTransactionCol[0] => null,
                                $_arrTransactionCol[1] => $data['id_card'],
                                $_arrTransactionCol[2] => $data['total_amount'],
                                $_arrTransactionCol[3] => $data['date_pay'],
                                $_arrTransactionCol[4] => $data['ot_requestmaster_id'],
                                $_arrTransactionCol[5] => $data['ot_requestdetail_id'],
                                $_arrTransactionCol[6] => $data['return_id'],
                                $_arrTransactionCol[7] => $data['return_name'],
                                $_arrTransactionCol[8] => $data['remark'],
                                $_arrTransactionCol[9] => $data['createby_user'],
                                $_arrTransactionCol[10] => $data['create_datetime'],
                                $_arrTransactionCol[11] => $is_used,
                                $_arrTransactionCol[12] =>  null,
                                $_arrTransactionCol[13] =>  null,
                            ];

                            //IF return is money or ELSE holiday
                            if($data['return_id']==1)
                                $arrEmpGetOTMoney[$data['id_card']][] = $data['total_amount'];
                            else
                                $arrEmpGetOTHoliday[$data['id_card']][] = $data['total_amount'];
                        }
                    }

                    $t3 += $connection->createCommand()->batchInsert($_table, $_arrTransactionCol, $arrData)->execute();
                    /* --------------------------------------------------------------------- */

                    /* ----------------------------------------------------------------------------- */
                    /* หารายได้โอทีรวมของพนักงานแต่ละคน กรณีทำโอทีหลายช่วงเวลา แบบเลือกเป็นเงิน / เลือกเป็นวันหยุด */
                    /* ----------------------------------------------------------------------------- */
                    $arrEmpTotalOTMoney = $arrEmpTotalOTHoliday = [];
                    foreach ($arrAllEmpApproved as $idcard) {
                        if(count($arrEmpGetOTMoney) > 0)
                            $arrEmpTotalOTMoney[$idcard] = array_sum($arrEmpGetOTMoney[$idcard]);
                    }

                    $arrEmpTotalOTHoliday = [];
                    foreach ($arrAllEmpApproved as $idcard) {
                        if(count($arrEmpGetOTHoliday) > 0)
                            $arrEmpTotalOTHoliday[$idcard] = array_sum($arrEmpGetOTHoliday[$idcard]);
                    }

                    /* --------------------------------------------------------------------- */
                    /* หาเลข ID / Template ID จากที่กำหนดไหว้                                   */
                    /* --------------------------------------------------------------------- */
                    $OtTemplateID = ApiHr::getPayrollConfigTemplate()->getOtTemplateID();  //ดึง Config Template ID ของโอที จาก ตาราง payroll_config_template
                    if($OtTemplateID == null || $OtTemplateID =='') {
                        $allProcess = false;
                        throw new \Exception('ไม่พบ หมายเลข Template ID ของ OT');
                    }

                    $arrAllTemplates = ApiPayroll::getArrayAdddeductTemplate();
                    $TemplateID = $arrAllTemplates[$OtTemplateID]['ADD_DEDUCT_TEMPLATE_ID'];
                    $TemplateName = $arrAllTemplates[$OtTemplateID]['ADD_DEDUCT_TEMPLATE_NAME'];



                    /* --------------------------------------------------------------------- */
                    /* บันทึกรายการโอทีลงตาราง  add_deduct_detail                               */
                    /* --------------------------------------------------------------------- */
                    $_table = 'ADD_DEDUCT_DETAIL';
                    $addDeDuctColumns = Adddeductdetail::getTableSchema()->getColumnNames();
                    $arrData = null;

                    $arrDateRange = DateTime::makeDayFromMonthPicker($arrOTMaster['wage_pay_date']);

                    foreach($arrEmpTotalOTMoney as $idcard => $money) {

                        $arrData[] = [
                            $addDeDuctColumns[0]=>null, //$ADD_DEDUCT_DETAIL_ID
                            $addDeDuctColumns[1]=>$TemplateID, //$ADD_DEDUCT_ID
                            $addDeDuctColumns[2]=>$TemplateName, //$ADD_DEDUCT_DETAIL
                            $addDeDuctColumns[3]=>$arrOTMaster['wage_pay_date'], //$ADD_DEDUCT_DETAIL_PAY_DATE
                            $addDeDuctColumns[4]=>$arrDateRange['start_date'], //$ADD_DEDUCT_DETAIL_START_USE_DATE
                            $addDeDuctColumns[5]=>$arrDateRange['end_date'], //$ADD_DEDUCT_DETAIL_END_USE_DATE
                            $addDeDuctColumns[6]=>null, //$INSTALLMENT_START
                            $addDeDuctColumns[7]=>null, //$INSTALLMENT_END
                            $addDeDuctColumns[8]=>null, //$INSTALLMENT_CURRENT
                            $addDeDuctColumns[9]=>$idcard, //$ADD_DEDUCT_DETAIL_EMP_ID
                            $addDeDuctColumns[10]=>$money, //$ADD_DEDUCT_DETAIL_AMOUNT
                            $addDeDuctColumns[11]=>2, //$ADD_DEDUCT_DETAIL_TYPE //รายการเฉพาะเดือน
                            $addDeDuctColumns[12]=>Yii::$app->params['ACTIVE_STATUS'], //$ADD_DEDUCT_DETAIL_STATUS
                            $addDeDuctColumns[13]=>null, //$ADD_DEDUCT_DETAIL_HISTORY_STATUS
                            $addDeDuctColumns[14]=>null, //$ADD_DEDUCT_DETAIL_GROUP_STATUS
                            $addDeDuctColumns[15]=>DateTime::getTodayDateTime(), //$ADD_DEDUCT_DETAIL_CREATE_DATE
                            $addDeDuctColumns[16]=>$this->idcardLogin, //$ADD_DEDUCT_DETAIL_CREATE_BY
                            $addDeDuctColumns[17]=>null, //$ADD_DEDUCT_DETAIL_UPDATE_BY
                            $addDeDuctColumns[18]=>null, //$ADD_DEDUCT_DETAIL_UPDATE_DATE
                            $addDeDuctColumns[19]=>$this->idcardLogin, //$ADD_DEDUCT_DETAIL_RESP_PERSON
                            $addDeDuctColumns[20]=>null, //$dept_deduct_id
                            $addDeDuctColumns[21]=>null, //$dept_deduct_detail_id
                        ];
                    }

                    $t4 += $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();

                    /* --------------------------------------------------------------------- */
                    /* บันทึกรายการโอทีลงตาราง  checktime กรณีที่เลือกเป็นวันหยุด                      */
                    /* --------------------------------------------------------------------- */
                    //TODO : Later


                    if ($t1 > 0 && $t2 > 0 && $t3 > 0 && $t4 > 0 && $allProcess==true){
                        $transaction->commit();
                        echo 1;
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }

    }


    /**ฟังก์ชั่นคำนวณ รายได้ โอที
     * Algorith by @Aeedy 2017-11-28
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionCalculateot()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $arrOTMaster = [];
            $arrOTMaster['ot_calculate_type'] = $postValue['ot_calculate_type'];
            $arrOTMaster['has_profile_route'] = $postValue['ot_route'];
            $arrOTMaster['profile_route_id'] = $postValue['profileroute_id'];
            $arrOTMaster['distance_amount'] = $postValue['ot_distance'];
            $arrOTMaster['has_motel_profile'] = $postValue['ot_motel'];
            $arrOTMaster['motel_price'] = $postValue['motel_price'];

            $myIDCard = $postValue['idcard'];

            $approvedDetail = [];
            $approvedDetail['id_card'] = $myIDCard;
            $approvedDetail['time_start'] = $postValue['start_time'];
            $approvedDetail['time_end'] = $postValue['end_time'];
            $approvedDetail['time_total'] = $postValue['totaltime'];

            //load ot config
            $OtConfig = OtConfigformular::findOne(1);
            $ratioOT = ($arrOTMaster['ot_calculate_type']==1) ? $OtConfig['ot_ratio_workday_clockout'] : (($arrOTMaster['ot_calculate_type']==2) ? $OtConfig['ot_ratio_holiday'] : $OtConfig['ot_ratio_holiday_clockout']);

            //load work day config
            $workDayPerMonth = 30;
            $workHourPerDay = 8;

            $myOTTime = $postValue['totaltime'];
            $mySalary = $postValue['salary'];

            $myWagePerHour = (($mySalary/$workDayPerMonth)/$workHourPerDay);  //เงินเดือนหาร จน.วัน หาร จน. ชั่วโมง
            $myOTWage = $myWagePerHour * $ratioOT * $myOTTime;  //คำนวณหาค่าแรงตามชั่วโมง

            //IF has distance, calculate distance
            $arrEmpWorkTime = ApiHr::getEmpWorkTimeByIDCard($myIDCard);
            $myWorkHour = $arrEmpWorkTime[$myIDCard];

            $myDistance = ($arrOTMaster['has_profile_route']==1) ? ApiOT::calculateDistanceRule($arrOTMaster,$approvedDetail,$myWorkHour) : 0;

            //IF has motel price, set motel price
            $myMotel = ($arrOTMaster['has_motel_profile']==1) ? $arrOTMaster['motel_price'] :  0;
            $myOTTotal = Utility::ot_round_money($myOTWage + $myDistance + $myMotel); //ผลรวมโอทีทั้งหมด
            $remarks = "$myIDCard ได้ ratio $ratioOT | เวลาทำโอที $myOTTime ชม. | ค่าเดินทาง $myDistance  | รวม $myOTTotal";

            $ret['money'] = $myOTTotal;
            $ret['remark'] = $remarks;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $ret;

        }
    }



    /**ฟังก์ชั่นบันทึกการอนุมัติโอที
     * Algorith by @Aeedy 2017-12-04
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionSaveapprovedhr()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $leaderIDCard = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            if(trim($postValue['approved'])) {

                $id = $postValue['rqmaster_id'];
                $arrItem_approved = $postValue['item_approved'];
                $arrItem_comment = $postValue['item_comment'];


                //begin transaction
                $connection =  OtRequestmaster::getDb();
                $transaction = $connection->beginTransaction();

                try {

                    $t1 = $t2 = $t3 = $t4 = 0;
                    $allProcess = true;
                    $arrRecApprovdOT = $arrRecOTIDCard = [];

                    /* ------------------------------------------------------------- */
                    //อัพเดทตารางหัว ปรับสถานะอนุมัติ  VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0
                    /* ------------------------------------------------------------- */
                    $OtMaster = OtRequestmaster::findOne($id);
                    $OtMaster->is_hr_approved = 1;
                    $OtMaster->hr_approved_by = $this->idcardLogin;
                    $OtMaster->hr_approved_date = date('Y-m-d');
                    $OtMaster->hr_approved_time = date('H:i:s');
                    $OtMaster->updateby_user = $this->idcardLogin;
                    $OtMaster->update_datetime = new Expression('NOW()');
                    $t1 = $OtMaster->save();


                    /* ------------------------------------------------------------- */
                    /* อัพเดทตารางรายละเอียด  ปรับสถานะอนุมัติ VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0 */
                    /* ------------------------------------------------------------- */

                    foreach ($arrItem_approved as $key => $value) {
                        $OtDetail = OtRequestdetail::findOne($key);
                        $OtDetail->is_hr_approved = $value;
                        $OtDetail->unapproved_remark = $arrItem_comment[$key];
                        $OtDetail->hr_approved_by = $this->idcardLogin;
                        $OtDetail->hr_approved_date = date('Y-m-d');
                        $OtDetail->hr_approved_time = date('H:i:s');
                        $t2 += $OtDetail->save();


                        //เลือกเอาเฉพาะรายการที่อนุมัติผ่านเท่านั้นไปคำนวณรายได้ต่อ
                        if($value==1) {
                            $arrRecApprovdOT[$OtDetail->id] = $OtDetail->toArray();
                            $arrRecOTIDCard[$OtDetail->id_card] = $OtDetail->id_card;
                        }
                    }

                    $arrOTMaster = $OtMaster->toArray();


                    /* ------------------------------------------------------------- */
                    /** วนรอบคำนวณรายการอนุมัติรายคน */
                    /* ------------------------------------------------------------- */
                    $arrOTApproved = $arrAllEmpApproved = [];
                    foreach ($arrRecApprovdOT as $approvedID => $approvedDetail) {
                        $myOTTotal = 0;
                        $myIDCard = $approvedDetail['id_card'];
                        $data = [
                            'id_card'=>$approvedDetail['id_card'],
                            'total_amount'=>$approvedDetail['money_total'],
                            'date_pay'=>$arrOTMaster['wage_pay_date'],
                            'ot_requestmaster_id'=>$arrOTMaster['id'],
                            'ot_requestdetail_id'=>$approvedID,
                            'return_id'=>$approvedDetail['return_id'],
                            'return_name'=>$approvedDetail['return_name'],
                            'remark'=> $approvedDetail['remak'],
                            'createby_user'=>$this->idcardLogin,
                            'create_datetime'=> DateTime::getTodayDateTime(),
                        ];

                        $arrOTApproved[$myIDCard][$approvedID] = $data;
                        $arrAllEmpApproved[$myIDCard] = $myIDCard;

                    }


                    /* ------------------------------------------------------------- */
                    /* บันทึกรายการคำนวณลงตาราง  ot_return_transaction  Batch Insert   */
                    /* ------------------------------------------------------------- */
                    $_table = 'ot_return_transaction';
                    $_arrTransactionCol = OtReturnTransaction::getTableSchema()->getColumnNames();
                    $arrData = null; $is_used = 1; $arrEmpGetOTMoney = $arrEmpGetOTHoliday = [];
                    foreach ($arrOTApproved as $IDCard => $approvedID) {
                        foreach ($approvedID as $data) {
                            $arrData[] = [
                                $_arrTransactionCol[0] => null,
                                $_arrTransactionCol[1] => $data['id_card'],
                                $_arrTransactionCol[2] => $data['total_amount'],
                                $_arrTransactionCol[3] => $data['date_pay'],
                                $_arrTransactionCol[4] => $data['ot_requestmaster_id'],
                                $_arrTransactionCol[5] => $data['ot_requestdetail_id'],
                                $_arrTransactionCol[6] => $data['return_id'],
                                $_arrTransactionCol[7] => $data['return_name'],
                                $_arrTransactionCol[8] => $data['remark'],
                                $_arrTransactionCol[9] => $data['createby_user'],
                                $_arrTransactionCol[10] => $data['create_datetime'],
                                $_arrTransactionCol[11] => $is_used,
                                $_arrTransactionCol[12] =>  null,
                                $_arrTransactionCol[13] =>  null,
                            ];

                            //IF return is money or ELSE holiday
                            if($data['return_id']==1)
                                $arrEmpGetOTMoney[$data['id_card']][] = $data['total_amount'];
                            else
                                $arrEmpGetOTHoliday[$data['id_card']][] = $data['total_amount'];
                        }
                    }

                    $t3 += $connection->createCommand()->batchInsert($_table, $_arrTransactionCol, $arrData)->execute();


                    /* ----------------------------------------------------------------------------- */
                    /* หารายได้โอทีรวมของพนักงานแต่ละคน กรณีทำโอทีหลายช่วงเวลา แบบเลือกเป็นเงิน / เลือกเป็นวันหยุด */
                    /* ----------------------------------------------------------------------------- */
                    $arrEmpTotalOTMoney = $arrEmpTotalOTHoliday = [];
                    foreach ($arrAllEmpApproved as $idcard) {
                        if(count($arrEmpGetOTMoney) > 0)
                            $arrEmpTotalOTMoney[$idcard] = array_sum($arrEmpGetOTMoney[$idcard]);
                    }

                    $arrEmpTotalOTHoliday = [];
                    foreach ($arrAllEmpApproved as $idcard) {
                        if(count($arrEmpGetOTHoliday) > 0)
                            $arrEmpTotalOTHoliday[$idcard] = array_sum($arrEmpGetOTHoliday[$idcard]);
                    }

                    /* --------------------------------------------------------------------- */
                    /* หาเลข ID / Template ID จากที่กำหนดไหว้                                   */
                    /* --------------------------------------------------------------------- */
                    $OtTemplateID = ApiHr::getPayrollConfigTemplate()->getOtTemplateID();  //ดึง Config Template ID ของโอที จาก ตาราง payroll_config_template
                    if($OtTemplateID == null || $OtTemplateID =='') {
                        $allProcess = false;
                        throw new \Exception('ไม่พบ หมายเลข Template ID ของ OT');
                    }

                    $arrAllTemplates = ApiPayroll::getArrayAdddeductTemplate();
                    $TemplateID = $arrAllTemplates[$OtTemplateID]['ADD_DEDUCT_TEMPLATE_ID'];
                    $TemplateName = $arrAllTemplates[$OtTemplateID]['ADD_DEDUCT_TEMPLATE_NAME'];



                    /* --------------------------------------------------------------------- */
                    /* บันทึกรายการโอทีลงตาราง  add_deduct_detail                               */
                    /* --------------------------------------------------------------------- */
                    $_table = 'ADD_DEDUCT_DETAIL';
                    $addDeDuctColumns = Adddeductdetail::getTableSchema()->getColumnNames();
                    $arrData = null;

                    $arrDateRange = DateTime::makeDayFromMonthPicker($arrOTMaster['wage_pay_date']);

                    foreach($arrEmpTotalOTMoney as $idcard => $money) {

                        $arrData[] = [
                            $addDeDuctColumns[0]=>null, //$ADD_DEDUCT_DETAIL_ID
                            $addDeDuctColumns[1]=>$TemplateID, //$ADD_DEDUCT_ID
                            $addDeDuctColumns[2]=>$TemplateName, //$ADD_DEDUCT_DETAIL
                            $addDeDuctColumns[3]=>$arrOTMaster['wage_pay_date'], //$ADD_DEDUCT_DETAIL_PAY_DATE
                            $addDeDuctColumns[4]=>$arrDateRange['start_date'], //$ADD_DEDUCT_DETAIL_START_USE_DATE
                            $addDeDuctColumns[5]=>$arrDateRange['end_date'], //$ADD_DEDUCT_DETAIL_END_USE_DATE
                            $addDeDuctColumns[6]=>null, //$INSTALLMENT_START
                            $addDeDuctColumns[7]=>null, //$INSTALLMENT_END
                            $addDeDuctColumns[8]=>null, //$INSTALLMENT_CURRENT
                            $addDeDuctColumns[9]=>$idcard, //$ADD_DEDUCT_DETAIL_EMP_ID
                            $addDeDuctColumns[10]=>$money, //$ADD_DEDUCT_DETAIL_AMOUNT
                            $addDeDuctColumns[11]=>2, //$ADD_DEDUCT_DETAIL_TYPE //รายการเฉพาะเดือน
                            $addDeDuctColumns[12]=>Yii::$app->params['ACTIVE_STATUS'], //$ADD_DEDUCT_DETAIL_STATUS
                            $addDeDuctColumns[13]=>null, //$ADD_DEDUCT_DETAIL_HISTORY_STATUS
                            $addDeDuctColumns[14]=>null, //$ADD_DEDUCT_DETAIL_GROUP_STATUS
                            $addDeDuctColumns[15]=>DateTime::getTodayDateTime(), //$ADD_DEDUCT_DETAIL_CREATE_DATE
                            $addDeDuctColumns[16]=>$this->idcardLogin, //$ADD_DEDUCT_DETAIL_CREATE_BY
                            $addDeDuctColumns[17]=>null, //$ADD_DEDUCT_DETAIL_UPDATE_BY
                            $addDeDuctColumns[18]=>null, //$ADD_DEDUCT_DETAIL_UPDATE_DATE
                            $addDeDuctColumns[19]=>$this->idcardLogin, //$ADD_DEDUCT_DETAIL_RESP_PERSON
                            $addDeDuctColumns[20]=>null, //$dept_deduct_id
                            $addDeDuctColumns[21]=>null, //$dept_deduct_detail_id
                        ];
                    }

                    $t4 += $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();

                    /* --------------------------------------------------------------------- */
                    /* บันทึกรายการโอทีลงตาราง  checktime กรณีที่เลือกเป็นวันหยุด                      */
                    /* --------------------------------------------------------------------- */
                    //TODO : Later


                    if ($t1 > 0 && $t2 > 0 && $t3 > 0 && $t4 > 0 && $allProcess==true){
                        $transaction->commit();
                        echo 1;
                    }

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
    }


    /**ฟังก์ชั่นบันทึกการอนุมัติโอที
     * Algorith by @Aeedy 2017-12-04
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionSaveapproved()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $leaderIDCard = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            if(trim($postValue['approved'])) {

                $id = $postValue['rqmaster_id'];
                $arrItem_approved = $postValue['item_approved'];
                $arrItem_comment = $postValue['item_comment'];


                //begin transaction
                $connection =  OtRequestmaster::getDb();
                $transaction = $connection->beginTransaction();

                try {
                    $t1 = $t2 = 0;

                    /* ------------------------------------------------------------- */
                    //อัพเดทตารางหัว ปรับสถานะอนุมัติ  VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0  / กลับไปปรับปรุง => 2
                    /* ------------------------------------------------------------- */
                    $OtMaster = OtRequestmaster::findOne($id);
                    $OtMaster->is_approved = 1;
                    $OtMaster->approved_byuser = $leaderIDCard;
                    $OtMaster->approved_date = date('Y-m-d');
                    $OtMaster->approved_time = date('H:i:s');
                    $OtMaster->updateby_user = $leaderIDCard;
                    $OtMaster->update_datetime = new Expression('NOW()');
                    $t1 = $OtMaster->save();


                    /* ------------------------------------------------------------- */
                    /* อัพเดทตารางรายละเอียด  ปรับสถานะอนุมัติ VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0 / กลับไปปรับปรุง => 2 */
                    /* ------------------------------------------------------------- */

                    foreach ($arrItem_approved as $key => $value) {
                        $OtDetail = OtRequestdetail::findOne($key);
                        $OtDetail->is_approved = $value;
                        $OtDetail->unapproved_remark = $arrItem_comment[$key];
                        $OtDetail->approved_byuser = $leaderIDCard;
                        $OtDetail->approved_date = date('Y-m-d');
                        $OtDetail->approved_time = date('H:i:s');
                        $t2 += $OtDetail->save();
                    }


                    if ($t1 > 0 && $t2 > 0){
                        $transaction->commit();
                        echo 1;
                    }

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
    }



    /**ฟังก์ชั่นบันทึกการอนุมัติโอที พร้อมคำนวณ รายได้
     * Pennding / NOT USE
     * Algorith by @Aeedy 2017-08-27
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionSaveapproved_old()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $leaderIDCard = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            if(trim($postValue['approved'])) {

                $id = $postValue['rqmaster_id'];
                $arrItem_approved = $postValue['item_approved'];
                $arrItem_comment = $postValue['item_comment'];


                //begin transaction
                $connection =  OtRequestmaster::getDb();
                $transaction = $connection->beginTransaction();

                try {
                    $t1 = $t2 = $t3 = $t4 = 0;
                    $allProcess = true;
                    $arrRecApprovdOT = $arrRecOTIDCard = [];

                    /* ------------------------------------------------------------- */
                    //อัพเดทตารางหัว ปรับสถานะอนุมัติ  VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0  / กลับไปปรับปรุง => 2
                    /* ------------------------------------------------------------- */
                    $OtMaster = OtRequestmaster::findOne($id);
                    $OtMaster->is_approved = 1;
                    $OtMaster->approved_byuser = $leaderIDCard;
                    $OtMaster->approved_date = date('Y-m-d');
                    $OtMaster->approved_time = date('H:i:s');
                    $OtMaster->updateby_user = $leaderIDCard;
                    $OtMaster->update_datetime = new Expression('NOW()');
                    $t1 = $OtMaster->save();


                    /* ------------------------------------------------------------- */
                    /* อัพเดทตารางรายละเอียด  ปรับสถานะอนุมัติ VALUE อนุมัติ => 1 / ไม่อนุมัติ => 0 / กลับไปปรับปรุง => 2 */
                    /* ------------------------------------------------------------- */
                    foreach ($arrItem_approved as $key => $value) {
                        $OtDetail = OtRequestdetail::findOne($key);
                        $OtDetail->is_approved = $value;
                        $OtDetail->unapproved_remark = $arrItem_comment[$key];
                        $OtDetail->approved_byuser = $leaderIDCard;
                        $OtDetail->approved_date = date('Y-m-d');
                        $OtDetail->approved_time = date('H:i:s');
                        $pass = $OtDetail->save();

                        //เลือกเอาเฉพาะรายการที่อนุมัติผ่านเท่านั้นไปคำนวณรายได้ต่อ
                        if($pass==1 && $value==1) {
                            $arrRecApprovdOT[$OtDetail->id] = $OtDetail->toArray();
                            $arrRecOTIDCard[$OtDetail->id_card] = $OtDetail->id_card;
                        }

                        $t2 += $pass;
                    }

                    /* ------------------------------------------------------------- */
                    /* การคำนวณรายได้/ดึงค่า config / ประเภทโอที มี 3 ประเภท
                     * ot_calculate_type => ประเภทการคิดโอที 1=ปกติ, 2=นอกเวลาปกติ, 3=นอกเวลาวันหยุด  */
                    //TODO : Adjust configformular add more attribute in API::OT
                    $OtConfig = OtConfigformular::findOne(1);
                    $arrOTMaster = $OtMaster->toArray();


                    /**  ratio การคำนวณ / อัตราการคำนวณกับเงินเดือน  **/
                    $arrUniqIDCard = array_unique($arrRecOTIDCard);
                    $ratioOT = ($arrOTMaster['ot_calculate_type']==1) ? $OtConfig['ot_ratio_workday_clockout'] : (($arrOTMaster['ot_calculate_type']==2) ? $OtConfig['ot_ratio_holiday'] : $OtConfig['ot_ratio_holiday_clockout']);
                    $arrEmpSalary = ApiHr::getEmpSalaryByIDCard($arrUniqIDCard);
                    $arrEmpWorkTime = ApiHr::getEmpWorkTimeByIDCard($arrUniqIDCard);



                    /* ------------------------------------------------------------- */
                    /** วนรอบคำนวณรายการอนุมัติรายคน */
                    /* ------------------------------------------------------------- */
                    $arrOTApproved = $arrAllEmpApproved = [];
                    $companyID = $arrOTMaster['company_id'];  //รหัสบริษัทที่ทำโอที
                    $workDayPerMonth = 30;  //จำนวนวันทำงานในเดือน
                    $workHourPerDay = 8;  //จำนวนชั่วโมงทำงานต่อวัน
                    foreach ($arrRecApprovdOT as $approvedID => $approvedDetail) {
                        $myOTTotal = 0;
                        $myIDCard = $approvedDetail['id_card'];
                        $mySalary  = array_pop($arrEmpSalary[$myIDCard][$companyID]);

                        $myOTTime = $approvedDetail['time_total'];
                        $myWagePerHour = (($mySalary/$workDayPerMonth)/$workHourPerDay);  //เงินเดือนหาร จน.วัน หาร จน. ชั่วโมง
                        $myOTWage = $myWagePerHour * $ratioOT * $myOTTime;  //คำนวณหาค่าแรงตามชั่วโมง

                        //IF has distance, calculate distance
                        $myWorkHour = $arrEmpWorkTime[$myIDCard];
                        $myDistance = ($arrOTMaster['has_profile_route']==1) ? ApiOT::calculateDistanceRule($arrOTMaster,$approvedDetail,$myWorkHour) : 0;

                        //IF has motel price, set motel price
                        $myMotel = ($arrOTMaster['has_motel_profile']==1) ? $arrOTMaster['motel_price'] :  0;
                        $myOTTotal = Helper::displayDecimal($myOTWage + $myDistance + $myMotel); //ผลรวมโอทีทั้งหมด
                        $remarks = "$myIDCard ได้ ratio $ratioOT | เวลาทำโอที $myOTTime ชม. | ค่าเดินทาง $myDistance  | รวม $myOTTotal";
                        $data = [
                            'id_card'=>$myIDCard,
                            'total_amount'=>$myOTTotal,
                            'date_pay'=>$arrOTMaster['wage_pay_date'],
                            'ot_requestmaster_id'=>$arrOTMaster['id'],
                            'ot_requestdetail_id'=>$approvedID,
                            'return_id'=>$approvedDetail['return_id'],
                            'return_name'=>$approvedDetail['return_name'],
                            'remark'=> $remarks,
                            'createby_user'=>$this->idcardLogin,
                            'create_datetime'=> DateTime::getTodayDateTime(),
                            'salary'=>$mySalary,
                            'ratio'=>$ratioOT,
                        ];


                        // array_push($arrEmpSalary[$myIDCard][$companyID],$mySalary);
                        $arrOTApproved[$myIDCard][$approvedID] = $data;
                        $arrAllEmpApproved[$myIDCard] = $myIDCard;

                    }
                    /* ------------------------------------------------------------- */

                    /* ------------------------------------------------------------- */
                    /* บันทึกรายการคำนวณลงตาราง  ot_return_transaction  Batch Insert   */
                    /* ------------------------------------------------------------- */
                    $_table = 'ot_return_transaction';
                    $_arrTransactionCol = OtReturnTransaction::getTableSchema()->getColumnNames();
                    $arrData = null; $is_used = 1; $arrEmpGetOTMoney = $arrEmpGetOTHoliday = [];
                    foreach ($arrOTApproved as $IDCard => $approvedID) {
                        foreach ($approvedID as $data) {
                            $arrData[] = [
                                $_arrTransactionCol[0] => NULL,
                                $_arrTransactionCol[1] => $data['id_card'],
                                $_arrTransactionCol[2] => $data['total_amount'],
                                $_arrTransactionCol[3] => $data['date_pay'],
                                $_arrTransactionCol[4] => $data['ot_requestmaster_id'],
                                $_arrTransactionCol[5] => $data['ot_requestdetail_id'],
                                $_arrTransactionCol[6] => $data['return_id'],
                                $_arrTransactionCol[7] => $data['return_name'],
                                $_arrTransactionCol[8] => $data['remark'],
                                $_arrTransactionCol[9] => $data['createby_user'],
                                $_arrTransactionCol[10] => $data['create_datetime'],
                                $_arrTransactionCol[11] => $is_used,
                                $_arrTransactionCol[12] =>  NULL,
                                $_arrTransactionCol[13] =>  NULL,
                            ];

                            //IF return is money or ELSE holiday
                            if($data['return_id']==1)
                                $arrEmpGetOTMoney[$data['id_card']][] = $data['total_amount'];
                            else
                                $arrEmpGetOTHoliday[$data['id_card']][] = $data['total_amount'];
                        }
                    }

                    $t3 += $connection->createCommand()->batchInsert($_table, $_arrTransactionCol, $arrData)->execute();
                    /* --------------------------------------------------------------------- */

                    /* ----------------------------------------------------------------------------- */
                    /* หารายได้โอทีรวมของพนักงานแต่ละคน กรณีทำโอทีหลายช่วงเวลา แบบเลือกเป็นเงิน / เลือกเป็นวันหยุด */
                    /* ----------------------------------------------------------------------------- */
                    $arrEmpTotalOTMoney = $arrEmpTotalOTHoliday = [];
                    foreach ($arrAllEmpApproved as $idcard) {
                        if(count($arrEmpGetOTMoney) > 0)
                            $arrEmpTotalOTMoney[$idcard] = array_sum($arrEmpGetOTMoney[$idcard]);
                    }

                    $arrEmpTotalOTHoliday = [];
                    foreach ($arrAllEmpApproved as $idcard) {
                        if(count($arrEmpGetOTHoliday) > 0)
                            $arrEmpTotalOTHoliday[$idcard] = array_sum($arrEmpGetOTHoliday[$idcard]);
                    }

                    /* --------------------------------------------------------------------- */
                    /* หาเลข ID / Template ID จากที่กำหนดไหว้                                   */
                    /* --------------------------------------------------------------------- */
                    $OtTemplateID = ApiHr::getPayrollConfigTemplate()->getOtTemplateID();  //ดึง Config Template ID ของโอที จาก ตาราง payroll_config_template
                    if($OtTemplateID == null || $OtTemplateID =='') {
                        $allProcess = false;
                        throw new \Exception('ไม่พบ หมายเลข Template ID ของ OT');
                    }

                    $arrAllTemplates = ApiPayroll::getArrayAdddeductTemplate();
                    $TemplateID = $arrAllTemplates[$OtTemplateID]['ADD_DEDUCT_TEMPLATE_ID'];
                    $TemplateName = $arrAllTemplates[$OtTemplateID]['ADD_DEDUCT_TEMPLATE_NAME'];



                    /* --------------------------------------------------------------------- */
                    /* บันทึกรายการโอทีลงตาราง  add_deduct_detail                               */
                    /* --------------------------------------------------------------------- */
                    $_table = 'ADD_DEDUCT_DETAIL';
                    $addDeDuctColumns = Adddeductdetail::getTableSchema()->getColumnNames();
                    $arrData = null;
                    foreach($arrEmpTotalOTMoney as $idcard => $money) {
                        $arrData[] = [
                            $addDeDuctColumns[0]=>NULL, //$ADD_DEDUCT_DETAIL_ID
                            $addDeDuctColumns[1]=>$TemplateID, //$ADD_DEDUCT_ID
                            $addDeDuctColumns[2]=>$TemplateName, //$ADD_DEDUCT_DETAIL
                            $addDeDuctColumns[3]=>$arrOTMaster['wage_pay_date'], //$ADD_DEDUCT_DETAIL_PAY_DATE
                            $addDeDuctColumns[4]=>DateTime::getTodayDate(), //$ADD_DEDUCT_DETAIL_START_USE_DATE
                            $addDeDuctColumns[5]=>DateTime::getTodayDate(), //$ADD_DEDUCT_DETAIL_END_USE_DATE
                            $addDeDuctColumns[6]=>NULL, //$INSTALLMENT_START
                            $addDeDuctColumns[7]=>NULL, //$INSTALLMENT_END
                            $addDeDuctColumns[8]=>NULL, //$INSTALLMENT_CURRENT
                            $addDeDuctColumns[9]=>$idcard, //$ADD_DEDUCT_DETAIL_EMP_ID
                            $addDeDuctColumns[10]=>$money, //$ADD_DEDUCT_DETAIL_AMOUNT
                            $addDeDuctColumns[11]=>2, //$ADD_DEDUCT_DETAIL_TYPE //รายการเฉพาะเดือน
                            $addDeDuctColumns[12]=>Yii::$app->params['ACTIVE_STATUS'], //$ADD_DEDUCT_DETAIL_STATUS
                            $addDeDuctColumns[13]=>NULL, //$ADD_DEDUCT_DETAIL_HISTORY_STATUS
                            $addDeDuctColumns[14]=>NULL, //$ADD_DEDUCT_DETAIL_GROUP_STATUS
                            $addDeDuctColumns[15]=>DateTime::getTodayDateTime(), //$ADD_DEDUCT_DETAIL_CREATE_DATE
                            $addDeDuctColumns[16]=>$this->idcardLogin, //$ADD_DEDUCT_DETAIL_CREATE_BY
                            $addDeDuctColumns[17]=>NULL, //$ADD_DEDUCT_DETAIL_UPDATE_BY
                            $addDeDuctColumns[18]=>NULL, //$ADD_DEDUCT_DETAIL_UPDATE_DATE
                            $addDeDuctColumns[19]=>$this->idcardLogin, //$ADD_DEDUCT_DETAIL_RESP_PERSON
                            $addDeDuctColumns[20]=>NULL, //$dept_deduct_id
                            $addDeDuctColumns[21]=>NULL, //$dept_deduct_detail_id
                        ];
                    }

                    $t4 += $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();

                    /* --------------------------------------------------------------------- */
                    /* บันทึกรายการโอทีลงตาราง  checktime กรณีที่เลือกเป็นวันหยุด                      */
                    /* --------------------------------------------------------------------- */
                    //TODO : Later


                    if ($t1 > 0 && $t2 > 0 && $t3 > 0 && $t4 > 0 && $allProcess==true){
                        $transaction->commit();
                        echo 1;
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
    }


    public function actionHistoryapprovedot()
    {
        $arrOTActivity = ApiOT::getOTActivity();
        return $this->render('historyapprovedot',[
            'arrOTActivity'=>$arrOTActivity,
        ]);
    }

    public function actionHistoryapprovedothr()
    {
        $arrOTActivity = ApiOT::getOTActivity();
        return $this->render('historyapprovedothr',[
            'arrOTActivity'=>$arrOTActivity,
        ]);
    }


    public function actionGetdepartment()
    {
        $getValue = Yii::$app->request->get();
        $id_company = $getValue['id_company'];
        $model = ApiHr::getDepartment($id_company);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionGetsection()
    {
        $getValue = Yii::$app->request->get();
        $id_department = $getValue['id_department'];
        $model = ApiHr::getSection($id_department);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }


    public function actionSavemanageot()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            // print_r($postValue);

            //**  Load data */
            $arrOTReturn = ApiOT::getOTReturnBenefit();
            $arrOTActivity = ApiOT::getOTActivity();

            /*** Header info ***/
            $_company_id = (isset($postValue['xcompany']) && $postValue['xcompany'] > 0) ? $postValue['xcompany'] : 0;
            $_divistion_id = (isset($postValue['xdepartment']) && $postValue['xdepartment'] > 0) ? $postValue['xdepartment'] : 0;
            $_section_id = (isset($postValue['xsection']) && $postValue['xsection'] > 0) ? $postValue['xsection'] : 0;

            $_activity_id = $postValue['activity_id'];
            $_activity_name = $arrOTActivity[$_activity_id];
            $_ot_calculate_type = $postValue['ot_calculate_type'];
            $_ot_date = DateTime::DateToMysqlDB($postValue['ot_date']);

            $_ot_route = $postValue['ot_route'];
            $_profileroute_id = (isset($postValue['profileroute_id']) && $postValue['profileroute_id'] > 0) ? $postValue['profileroute_id'] : 0;
            $_ot_distance = (isset($postValue['ot_distance']) && $postValue['ot_distance'] > 0) ? $postValue['ot_distance'] : 0;

            $_ot_motel = $postValue['ot_motel'];
            $_motel_price = ($postValue['ot_motel'] == 1) ? $postValue['motel_price'] : 0;


            $arrOTEmployee = $postValue['otEmployee'];
            $arrStartHour = $postValue['start_hour'];
            $arrStartMinute = $postValue['start_minute'];
            $arrEndHour = $postValue['end_hour'];
            $arrEndMinute = $postValue['end_minute'];
            $arrReturnID = $postValue['otreturn_id'];

            $arrTimeTotal = $postValue['totaltime'];
            $arrMoneyTotal = $postValue['totalmoney'];

            $arrRemark = $postValue['remark'];

            $wage_pay_date = $postValue['month_pay'];


            $today = date('Y-m-d H:i:s');
            $req_date = date('Y-m-d');
            $req_time = date('H:i:s');
            // $creat_by = '9822332951942';
            $leaderIDCard = $this->idcardLogin;


            $connection = Yii::$app->dbERP_easyhr_PAYROLL;
            $transaction = $connection->beginTransaction();
            try {

                $requstNo = ApiOT::buildOTRequestNo();
                $eff = 0;
                $_table_master = 'ot_requestmaster';
                $sql_master = "INSERT INTO $_table_master (
                    id,
                    request_no,
                    wage_pay_date,
                    company_id,
                    division_id,
                    section_id,
                    activity_id,
                    activity_name,
                    ot_calculate_type,
                    activity_date,
                    has_profile_route,
                    profile_route_id,
                    distance_amount,
                    has_motel_profile,
                    motel_price,
                    request_byuser,
                    request_date,
                    request_time,
                    is_approved,
                    approved_byuser,
                    approved_date,
                    approved_time,
                    status_active,
                    createby_user,
                    create_datetime,
                    updateby_user,
                    update_datetime,
                    is_hr_approved,
                    hr_approved_date,
                    hr_approved_time,
                    hr_approved_by) VALUES (null,'$requstNo','$wage_pay_date', '$_company_id','$_divistion_id','$_section_id','$_activity_id','$_activity_name','$_ot_calculate_type','$_ot_date','$_ot_route','$_profileroute_id',
                    '$_ot_distance','$_ot_motel','$_motel_price','$leaderIDCard','$req_date','$req_time',0,null,null,null,1,'$leaderIDCard','$today',null,null,0,null,null,null) ";

                $eff = $connection->createCommand($sql_master)->execute();
                $masterID = $connection->getLastInsertID();
                //echo $eff;
                $_table_detail = 'ot_requestdetail';
                $_arrDetailCol = [
                    'id',
                    'ot_requestmaster_id',
                    'id_card',
                    'time_start',
                    'time_end',
                    'time_total',
                    'return_id',
                    'return_name',
                    'is_check',
                    'check_byuser',
                    'check_date',
                    'check_time',
                    'is_approved',
                    'approved_byuser',
                    'approved_date',
                    'approved_time',
                    'createby_user',
                    'create_datetime',
                    'money_total',
                    'remak',
                    'is_hr_approved',
                    'hr_approved_date',
                    'hr_approved_time',
                    'hr_approved_by'
                ];

                $eff = 0;
                $_arr_data = null;
                $idx = 0;
                foreach ($arrOTEmployee as $IDCard) {

                    $_starttime = DateTime::maketimeformat($arrStartHour[$idx], $arrStartMinute[$idx]);
                    $_endtime = DateTime::maketimeformat($arrEndHour[$idx], $arrEndMinute[$idx]);
                    $_timetotal = DateTime::calculate_time_diff($_starttime, $_endtime);

                    $_arr_data[] = [
                        $_arrDetailCol[0] => null,
                        $_arrDetailCol[1] => $masterID,  //ot_requestmaster_id
                        $_arrDetailCol[2] => $IDCard,
                        $_arrDetailCol[3] => $_starttime,   //time_start
                        $_arrDetailCol[4] => $_endtime,   //time_end
                        $_arrDetailCol[5] => $arrTimeTotal[$idx],   //time_total
                        $_arrDetailCol[6] => $arrReturnID[$idx],   //return_id
                        $_arrDetailCol[7] => $arrOTReturn[$arrReturnID[$idx]],   //return_name
                        $_arrDetailCol[8] => 1,   //is_check
                        $_arrDetailCol[9] => $leaderIDCard,   //check_byuser
                        $_arrDetailCol[10] => $today,  //check_date
                        $_arrDetailCol[11] => date('H:i:s'),  //check_time
                        $_arrDetailCol[12] => 0,  //is_approved
                        $_arrDetailCol[13] => null,  //approved_byuser
                        $_arrDetailCol[14] => null,  //approved_date
                        $_arrDetailCol[15] => null,  //approved_time
                        $_arrDetailCol[16] => $leaderIDCard,  //createby_user
                        $_arrDetailCol[17] => $today,  //create_datetime
                        $_arrDetailCol[18] => $arrMoneyTotal[$idx],  //money_total
                        $_arrDetailCol[19] => $arrRemark[$idx],  //remak
                        $_arrDetailCol[20] => 0,  //is_hr_approved
                        $_arrDetailCol[21] => null,  //hr_approved_date
                        $_arrDetailCol[22] => null,  //hr_approved_time
                        $_arrDetailCol[23] => null,  //hr_approved_by
                    ];

                    $idx++;
                }

                $eff += $connection->createCommand()->batchInsert($_table_detail, $_arrDetailCol, $_arr_data)->execute();

                $transaction->commit();
                echo $eff;

            } catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception('ERROR' . $e->getMessage());
            }

        }
    }

    public function actionSaveeditmanageot()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            // print_r($postValue);

            //**  Load data */
            $arrOTReturn = ApiOT::getOTReturnBenefit();
            $arrOTActivity = ApiOT::getOTActivity();

            /*** Header info ***/
            $_company_id = (isset($postValue['xcompany']) && $postValue['xcompany'] > 0) ? $postValue['xcompany'] : 0;
            $_divistion_id = (isset($postValue['xdepartment']) && $postValue['xdepartment'] > 0) ? $postValue['xdepartment'] : 0;
            $_section_id = (isset($postValue['xsection']) && $postValue['xsection'] > 0) ? $postValue['xsection'] : 0;

            $_activity_id = $postValue['activity_id'];
            $_activity_name = $arrOTActivity[$_activity_id];
            $_ot_calculate_type = $postValue['ot_calculate_type'];
            $_ot_date = DateTime::DateToMysqlDB($postValue['ot_date']);

            $_ot_route = $postValue['ot_route'];
            $_profileroute_id = (isset($postValue['profileroute_id']) && $postValue['profileroute_id'] > 0) ? $postValue['profileroute_id'] : 0;
            $_ot_distance = (isset($postValue['ot_distance']) && $postValue['ot_distance'] > 0) ? $postValue['ot_distance'] : 0;

            $_ot_motel = $postValue['ot_motel'];
            $_motel_price = ($postValue['ot_motel'] == 1) ? $postValue['motel_price'] : 0;


            $arrOTEmployee = $postValue['otEmployee'];
            $arrStartHour = $postValue['start_hour'];
            $arrStartMinute = $postValue['start_minute'];
            $arrEndHour = $postValue['end_hour'];
            $arrEndMinute = $postValue['end_minute'];
            $arrReturnID = $postValue['otreturn_id'];

            $arrTimeTotal = $postValue['totaltime'];
            $arrMoneyTotal = $postValue['totalmoney'];
            $arrRemark = $postValue['remark'];

            $wage_pay_date = $postValue['month_pay'];

            $hideEdit = $postValue['hide_edit'];

            $today = date('Y-m-d H:i:s');
            $req_date = date('Y-m-d');
            $req_time = date('H:i:s');
            // $creat_by = '9822332951942';
            $leaderIDCard = $this->idcardLogin;


            $connection = Yii::$app->dbERP_easyhr_PAYROLL;
            $transaction = $connection->beginTransaction();
            try {

                $eff = 0;
                $_table_master = 'ot_requestmaster';
                $sql_master = "UPDATE $_table_master SET 
                    wage_pay_date = '$wage_pay_date',
                    company_id = '$_company_id' ,
                    division_id = '$_divistion_id',
                    section_id = '$_section_id',
                    activity_id = '$_activity_id',
                    activity_name = '$_activity_name',
                    ot_calculate_type = '$_ot_calculate_type',
                    activity_date = '$_ot_date',
                    has_profile_route = '$_ot_route',
                    profile_route_id = '$_profileroute_id',
                    distance_amount = '$_ot_distance',
                    has_motel_profile = '$_ot_motel',
                    motel_price = '$_motel_price',
                    is_approved = 0,
                    updateby_user ='$leaderIDCard',
                    update_datetime = '$today'
                    WHERE id=$hideEdit ";

                $eff = $connection->createCommand($sql_master)->execute();

                //echo $eff;
                $_table_detail = 'ot_requestdetail';
                $_arrDetailCol = [
                    'id',
                    'ot_requestmaster_id',
                    'id_card',
                    'time_start',
                    'time_end',
                    'time_total',
                    'return_id',
                    'return_name',
                    'is_check',
                    'check_byuser',
                    'check_date',
                    'check_time',
                    'is_approved',
                    'approved_byuser',
                    'approved_date',
                    'approved_time',
                    'createby_user',
                    'create_datetime',
                    'money_total',
                    'remak',
                    'is_hr_approved',
                    'hr_approved_date',
                    'hr_approved_time',
                    'hr_approved_by'
                ];


                //echo 'xxxx'.count($arrOTEmployee);
                //exit;
                //$eff = 0;
                $_arr_data = null;
                $idx = 0;

                if(count($arrOTEmployee) > 0 && $arrOTEmployee[0] !='') {
                    foreach ($arrOTEmployee as $IDCard) {
                        $_starttime = DateTime::maketimeformat($arrStartHour[$idx], $arrStartMinute[$idx]);
                        $_endtime = DateTime::maketimeformat($arrEndHour[$idx], $arrEndMinute[$idx]);
                        //$_timetotal = DateTime::calculate_time_diff($_starttime, $_endtime);

                        $_arr_data[] = [
                            $_arrDetailCol[0] => null,
                            $_arrDetailCol[1] => $hideEdit,  //ot_requestmaster_id
                            $_arrDetailCol[2] => $IDCard,
                            $_arrDetailCol[3] => $_starttime,   //time_start
                            $_arrDetailCol[4] => $_endtime,   //time_end
                            $_arrDetailCol[5] => $arrTimeTotal[$idx],   //time_total
                            $_arrDetailCol[6] => $arrReturnID[$idx],   //return_id
                            $_arrDetailCol[7] => $arrOTReturn[$arrReturnID[$idx]],   //return_name
                            $_arrDetailCol[8] => 1,   //is_check
                            $_arrDetailCol[9] => $leaderIDCard,   //check_byuser
                            $_arrDetailCol[10] => $today,  //check_date
                            $_arrDetailCol[11] => date('H:i:s'),  //check_time
                            $_arrDetailCol[12] => 0,  //is_approved
                            $_arrDetailCol[13] => null,  //approved_byuser
                            $_arrDetailCol[14] => null,  //approved_date
                            $_arrDetailCol[15] => null,  //approved_time
                            $_arrDetailCol[16] => $leaderIDCard,  //createby_user
                            $_arrDetailCol[17] => $today,  //create_datetime
                            $_arrDetailCol[18] => $arrMoneyTotal[$idx],  //money_total
                            $_arrDetailCol[19] => $arrRemark[$idx],  //remak
                            $_arrDetailCol[20] => 0,  //is_hr_approved
                            $_arrDetailCol[21] => null,  //hr_approved_date
                            $_arrDetailCol[22] => null,  //hr_approved_time
                            $_arrDetailCol[23] => null,  //hr_approved_by
                        ];
                        $idx++;
                    }

                    $eff += $connection->createCommand()->batchInsert($_table_detail, $_arrDetailCol, $_arr_data)->execute();
                }
                else {
                    //$sqlupdate = "UPDATE ot_requestdetail SET is_approved=0,is_check=1,check_byuser='$leaderIDCard',check_date='$today',check_time='$req_time' WHERE ot_requestmaster_id=$hideEdit";
                    //$eff = $connection->createCommand($sqlupdate)->execute();
                }

                $transaction->commit();
                echo $eff;

            } catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception('ERROR' . $e->getMessage());
            }

        }
    }

    public function actionLoadotedit()
    {

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();

            $companyID = ($postValue['companyID']) ? $postValue['companyID'] : null;
            $departmentID = ($postValue['departmentID']) ? $postValue['departmentID'] : null;
            $sectionID = ($postValue['sectionID']) ? $postValue['sectionID'] : null;
            $returnID = ($postValue['returnID']) ? $postValue['returnID'] : null;
            $activityID = ($postValue['activityID']) ? $postValue['activityID'] : null;
            $wage_pay_date = ($postValue['monthPay']) ? $postValue['monthPay'] : null;

            $requestDate = ($postValue['requestDate']) ? DateTime::DateToMysqlDB($postValue['requestDate']) : null;
            $otDate = ($postValue['otDate']) ? DateTime::DateToMysqlDB($postValue['otDate']) : null;

            $opt = ($postValue['opt']) ? $postValue['opt'] : 0;

            $cond = '';
            if($opt==1) {
                $cond .= ($companyID !== null) ? " AND a.company_id=$companyID " : "";
                $cond .= ($departmentID !== null) ? " AND a.division_id=$departmentID " : "";
                $cond .= ($sectionID !== null) ? " AND a.section_id=$sectionID " : "";
                $cond .= ($returnID !== null) ? " AND b.return_id=$returnID " : "";
                $cond .= ($activityID !== null) ? " AND a.activity_id=$activityID " : "";

                $cond .= ($wage_pay_date !== null) ? " AND a.wage_pay_date='$wage_pay_date' " : "";
                $cond .= ($requestDate !== null) ? " AND a.request_date='$requestDate' " : "";
                $cond .= ($otDate !== null) ? " AND a.activity_date='$otDate' " : "";
            }

            $cond .= "  AND a.status_active !=99 ";


            $sql = "SELECT a.* FROM ot_requestmaster as a
                  INNER  JOIN ot_requestdetail as b ON a.id=b.ot_requestmaster_id
                  WHERE 1=1  $cond
                  AND (a.is_approved=0 OR a.is_approved=2)
                  GROUP BY a.id
                  ORDER BY a.id DESC, a.company_id ASC ";
            $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();


            $tbl = '<br/><table id="tbldatatable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 5%">ลำดับ</th>
                            <th style="width: 10%">คำขอเลขที่</th>
                            <th style="width: 10%">วันที่สร้างคำขอ</th>
                            <th style="width: 10%">วันที่ทำ OT</th>
                            <th style="width: 35%">ชื่อกิจกรรม</th>
                            <th style="width: 10%">สถานะ</th>
                            <th style="width: 15%;text-align: center;">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>';

            $idx = 1;
            foreach ($model as $item) {

                $view_url = Url::to(['overtime/viewotitem', 'id' => $item['id']]);
                $tbl .= '<tr>
                            <td>#' . $idx . '</td>
                            <td>' . $item['request_no'] . '</td>
                            <td>' . DateTime::CalendarDate($item['request_date']) . '</td>
                            <td>' . DateTime::CalendarDate($item['activity_date']) . '</td>
                            <td>' . $item['activity_name'] . '</td>
                            <td>' . ApiOT::mapOTstatus($item['is_approved']) . '</td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-info" onclick="goURL(\'' . $view_url . '\')"><i class="fa fa-eye"></i></button>';

                if ($item['is_approved'] != 1) {
                    $edit_url = Url::to(['overtime/editotitem', 'id' => $item['id']]);
                    $tbl .= ' <button type="button" class="btn btn-warning" onclick="goURL(\'' . $edit_url . '\')"><i class="fa fa-edit"></i></button>
                              <button type="button" class="btn btn-danger" onclick="doDeleteOT(' . $item['id'] . ')"><i class="fa fa-trash-o"></i></button>';
                }


                $tbl .= '</td></tr>';
                $idx++;
            }

            $tbl .= '</tbody></table>';

            return $tbl;
        }
    }



    public function actionLoadothr()
    {

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $companyID = $postValue['companyID'];


            $tbl = '<br/><table id="tbldatatable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 5%">ลำดับ</th>
                            <th style="width: 10%">คำขอเลขที่</th>
                            <th style="width: 10%">วันที่สร้างคำขอ</th>
                            <th style="width: 10%">วันที่ทำ OT</th>
                            <th style="width: 35%">ชื่อกิจกรรม</th>
                            <th style="width: 10%;text-align: center">สถานะ</th>
                            <th style="width: 5%;text-align: center">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>';

            $strCond = ' is_approved=1 AND is_hr_approved=0';
            if ($companyID > 0) {
                $strCond .=" AND company_id=$companyID ";
            }

            $model = OtRequestmaster::find()->where($strCond)->orderBy(['id' => SORT_DESC])->all();
            $idx = 1;
            foreach ($model as $item) {
                $tbl .= '<tr>
                            <td>#' . $idx . '</td>
                            <td>' . $item['request_no'] . '</td>
                            <td>' . DateTime::CalendarDate($item['request_date']) . '</td>
                            <td>' . DateTime::CalendarDate($item['activity_date']) . '</td>
                            <td>' . $item['activity_name'] . '</td>
                            <td style="text-align: center">' . ApiOT::mapOTstatus($item['is_hr_approved']) . '</td>
                            <td style="text-align: center;">';
                //<button type="button" class="btn btn-info" onclick="viewot(' . $item['id'] . ')"><i class="fa fa-eye"></i></button>';
                if ($item['is_hr_approved'] != 1) {

                    $approved_url = Url::to(['overtime/approvedot', 'id' => $item['id'], 'approved' => 'Y','opt' => 'hr']);
                    $tbl .= ' <button type="button" class="btn btn-info" onclick="goURL(\'' . $approved_url . '\')"><i class="fa fa-eye"></i> รายละเอียด</button>';
                    //<button type="button" class="btn btn-danger" onclick="delete(' . $item['id'] . ')"><i class="fa fa-trash-o"></i></button>
                }

                $tbl .= '</td></tr>';
                $idx++;
            }

            $tbl .= '</tbody></table>';

            return $tbl;
        }
    }



    public function actionLoadotpaid()
    {

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $companyID = $postValue['companyID'];


            $tbl = '<br/><table id="tbldatatable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 5%">ลำดับ</th>
                            <th style="width: 10%">คำขอเลขที่</th>
                            <th style="width: 10%">วันที่สร้างคำขอ</th>
                            <th style="width: 10%">วันที่ทำ OT</th>
                            <th style="width: 35%">ชื่อกิจกรรม</th>
                            <th style="width: 10%;text-align: center">สถานะ</th>
                            <th style="width: 5%;text-align: center">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>';

            $strCond = ' is_approved IN (0,2) ';
            if ($companyID > 0) {
                $strCond .=" AND company_id=$companyID ";
            }

            $model = OtRequestmaster::find()->where($strCond)->orderBy(['id' => SORT_DESC])->all();
            $idx = 1;
            foreach ($model as $item) {
                $tbl .= '<tr>
                            <td>#' . $idx . '</td>
                            <td>' . $item['request_no'] . '</td>
                            <td>' . DateTime::CalendarDate($item['request_date']) . '</td>
                            <td>' . DateTime::CalendarDate($item['activity_date']) . '</td>
                            <td>' . $item['activity_name'] . '</td>
                            <td style="text-align: center">' . ApiOT::mapOTstatus($item['is_approved']) . '</td>
                            <td style="text-align: center;">';
                //<button type="button" class="btn btn-info" onclick="viewot(' . $item['id'] . ')"><i class="fa fa-eye"></i></button>';
                if ($item['is_approved'] != 1) {

                    $approved_url = Url::to(['overtime/approvedot', 'id' => $item['id'], 'approved' => 'Y']);
                    $tbl .= ' <button type="button" class="btn btn-info" onclick="goURL(\'' . $approved_url . '\')"><i class="fa fa-eye"></i> รายละเอียด</button>';
                    //<button type="button" class="btn btn-danger" onclick="delete(' . $item['id'] . ')"><i class="fa fa-trash-o"></i></button>
                }

                $tbl .= '</td></tr>';
                $idx++;
            }

            $tbl .= '</tbody></table>';

            return $tbl;
        }
    }

    public function actionLoadhistoryot()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();

            $monthYear = ($postValue['monthOT']) ? $postValue['monthOT'] : null;
            $opt = ($postValue['opt']) ? $postValue['opt'] : 0;

            $cond = '';
            if($opt==1 && $monthYear !='' ) {
                $arrText = explode('-',$monthYear);
                $M = ($arrText[0]) ? (int)$arrText[0] : date('m');
                $Y = ($arrText[1]) ? (int)$arrText[1] : date('Y');
                $cond .="AND MONTH(activity_date)=$M AND YEAR(activity_date)=$Y ";
            }

            $tbl = '<br/><table id="tbldatatable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%">ลำดับ</th>
                                        <th style="width: 10%">คำขอเลขที่</th>
                                        <th style="width: 10%">วันที่สร้างคำขอ</th>
                                        <th style="width: 10%">วันที่ทำ OT</th>
                                        <th style="width: 10%">วันที่อนุมัติ</th>
                                        <th style="width: 20%">ชื่อกิจกรรม</th>
                                        <th style="width: 10%">สถานะ</th>
                                        <th style="width: 10%">ผู้อนุมัติ</th>
                                        <th style="width: 10%">เรียกดู</th>
                                    </tr>
                                    </thead>
                                    <tbody>';

            $arrUserFullNameApproved = ApiOT::mapUserApprovedFullname();
            $sql = "SELECT * FROM ot_requestmaster
                    WHERE 1=1 AND request_byuser='$this->idcardLogin'  $cond 
                    AND is_approved=1
                    ORDER BY activity_date DESC, company_id ASC ";
            $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $modelDetail = OtRequestdetail::find()->where('is_approved IN (1,2)')->asArray()->orderBy('ot_requestmaster_id, is_approved ASC')->all();
            $arrData = [];
            foreach($modelDetail as $item) {
                $arrData[$item['ot_requestmaster_id']][$item['is_approved']][] = $item;
            }

            //echo count($arrData[3][1]);
            $idx = 1;
            foreach ($model as $item) {

                $count_approved = (count($arrData[$item['id']][1])) ? count($arrData[$item['id']][1]) : 0;
                $count_reject = (count($arrData[$item['id']][2])) ? count($arrData[$item['id']][2]) : 0;

                if($count_reject > 0) {
                    $app_text = ApiOT::mapApprovedPatial($count_approved);
                    $app_text .='<br/>'. ApiOT::mapRejectedPatial($count_reject);
                }
                else {
                    $app_text = ApiOT::mapApprovedAll($count_approved);
                }

                $tbl .= '<tr>
                                <td>#' . $idx . '</td>
                                <td>' . $item['request_no'] . '</td>
                                <td>' . DateTime::CalendarDate($item['request_date']) . '</td>
                                <td>' . DateTime::CalendarDate($item['activity_date']) . '</td>
                                <td>' . DateTime::CalendarDate($item['approved_date']) . '</td>
                                <td>' . $item['activity_name'] . '</td>
                                <td>'.$app_text.'</td>
                                <td>' . $arrUserFullNameApproved[$item['approved_byuser']]['Fullname'] . '</td>
                                <td style="text-align: center;">
                                    <button type="button" class="btn btn-info" onclick="viewothistoryapproved(' . $item['id'] . ')"><i class="fa fa-search"></i></button>';

                if ($item['is_approved'] != 1) {
                    $tbl .= ' <button type="button" class="btn btn-warning" onclick="editot(' . $item['id'] . ')"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" onclick="delete(' . $item['id'] . ')"><i class="fa fa-trash-o"></i></button>';
                }

                $tbl .= '</td></tr>';
                $idx++;
            }
            $tbl .= '</tbody></table>';
        }


        return $tbl;
    }


    public function actionLoadothistoryapprovedhr()
    {

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();


            $tbl = '<br/><table id="tbldatatable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 5%">ลำดับ</th>
                            <th style="width: 10%">คำขอเลขที่</th>
                            <th style="width: 10%">วันที่สร้างคำขอ</th>
                            <th style="width: 10%">วันที่ทำ OT</th>
                            <th style="width: 10%">วันที่อนุมัติ</th>
                            <th style="width: 35%">ชื่อกิจกรรม</th>
                            <th style="width: 10%">สถานะ</th>
                            <th style="width: 15%">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>';


            $companyID = ($postValue['companyID']) ? $postValue['companyID'] : null;
            $monthYear = ($postValue['monthOT']) ? $postValue['monthOT'] : null;
            $opt = ($postValue['opt']) ? $postValue['opt'] : 0;


            $cond = '';
            if($opt==1) {
                $cond .= ($companyID !== null) ? " AND company_id=$companyID " : "";
                if($monthYear !=''){
                    $arrText = explode('-',$monthYear);
                    $M = ($arrText[0]) ? (int)$arrText[0] : date('m');
                    $Y = ($arrText[1]) ? (int)$arrText[1] : date('Y');
                    $cond .="AND MONTH(activity_date)=$M AND YEAR(activity_date)=$Y ";
                }
            }

            $sql = "SELECT * FROM ot_requestmaster
            WHERE 1=1  $cond
            AND is_hr_approved=1
            ORDER BY activity_date DESC, company_id ASC ";
            $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $idx = 1;
            foreach ($model as $item) {
                $tbl .= '<tr>
                            <td>#' . $idx . '</td>
                            <td>' . $item['request_no'] . '</td>
                            <td>' . DateTime::CalendarDate($item['request_date']) . '</td>
                            <td>' . DateTime::CalendarDate($item['activity_date']) . '</td>
                            <td>' . DateTime::CalendarDate($item['approved_date']) . '</td>
                            <td>' . $item['activity_name'] . '</td>
                            <td>' . ApiOT::mapOTstatus($item['is_hr_approved']) . '</td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-info" onclick="viewothistoryapprovedall(' . $item['id'] . ')"><i class="fa fa-eye"></i></button>';

                /*if ($item['is_hr_approved'] != 1) {
                    $tbl .= ' <button type="button" class="btn btn-warning" onclick="editot(' . $item['id'] . ')"><i class="fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger" onclick="delete(' . $item['id'] . ')"><i class="fa fa-trash-o"></i></button>';
                }*/
                $tbl .= '</td></tr>';
                $idx++;
            }
        }
        $tbl .= '</tbody></table>';

        return $tbl;
    }

    public function actionLoadothistoryapproved()
    {

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();


            $tbl = '<br/><table id="tbldatatable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 5%">ลำดับ</th>
                            <th style="width: 10%">คำขอเลขที่</th>
                            <th style="width: 10%">วันที่สร้างคำขอ</th>
                            <th style="width: 10%">วันที่ทำ OT</th>
                            <th style="width: 10%">วันที่อนุมัติ</th>
                            <th style="width: 35%">ชื่อกิจกรรม</th>
                            <th style="width: 10%">สถานะ</th>
                            <th style="width: 15%">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>';


            $companyID = ($postValue['companyID']) ? $postValue['companyID'] : null;
            $monthYear = ($postValue['monthOT']) ? $postValue['monthOT'] : null;
            $opt = ($postValue['opt']) ? $postValue['opt'] : 0;


            $cond = '';
            if($opt==1) {
                $cond .= ($companyID !== null) ? " AND company_id=$companyID " : "";
                if($monthYear !=''){
                    $arrText = explode('-',$monthYear);
                    $M = ($arrText[0]) ? (int)$arrText[0] : date('m');
                    $Y = ($arrText[1]) ? (int)$arrText[1] : date('Y');
                    $cond .="AND MONTH(activity_date)=$M AND YEAR(activity_date)=$Y ";
                }
            }

            $sql = "SELECT * FROM ot_requestmaster
            WHERE 1=1  $cond
            AND is_approved=1
            ORDER BY activity_date DESC, company_id ASC ";
            $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $idx = 1;
            foreach ($model as $item) {
                $tbl .= '<tr>
                            <td>#' . $idx . '</td>
                            <td>' . $item['request_no'] . '</td>
                            <td>' . DateTime::CalendarDate($item['request_date']) . '</td>
                            <td>' . DateTime::CalendarDate($item['activity_date']) . '</td>
                            <td>' . DateTime::CalendarDate($item['approved_date']) . '</td>
                            <td>' . $item['activity_name'] . '</td>
                            <td>' . ApiOT::mapOTstatus($item['is_approved']) . '</td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-info" onclick="viewothistoryapprovedall(' . $item['id'] . ')"><i class="fa fa-eye"></i></button>';

                if ($item['is_approved'] != 1) {
                    $tbl .= ' <button type="button" class="btn btn-warning" onclick="editot(' . $item['id'] . ')"><i class="fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger" onclick="delete(' . $item['id'] . ')"><i class="fa fa-trash-o"></i></button>';
                }
                $tbl .= '</td></tr>';
                $idx++;
            }
        }
        $tbl .= '</tbody></table>';

        return $tbl;
    }




    public function actionDeleteot()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $ID = $postValue['id'];
            $connection = Yii::$app->dbERP_easyhr_PAYROLL;
            $transaction = $connection->beginTransaction();
            try {

                $eff = $connection->createCommand()->update('ot_requestmaster', ['status_active' => 99], " id=$ID ")->execute();
                $eff2 = $connection->createCommand()->update('ot_requestdetail', ['is_approved' => 99], " ot_requestmaster_id=$ID ")->execute();
                $transaction->commit();
                echo $eff;
            } catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception('ERROR' . $e->getMessage());
            }
        }
    }

    public function actionDeleteempinot()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $ID = $postValue['id']; //requestdetail_id
            $connection = Yii::$app->dbERP_easyhr_PAYROLL;
            $transaction = $connection->beginTransaction();
            try {
                $eff = $connection->createCommand()->update('ot_requestdetail', ['is_approved' => 99,], " id=$ID ")->execute();
                $transaction->commit();
                echo $eff;

            } catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception('ERROR' . $e->getMessage());
            }
        }
    }

    public function actionGetdepartmentbyidcard()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $IDCard = $postValue['IDCard'];
            $model = ApiHr::getSeachDataPersonalByIdcard($IDCard);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionOtcancelreply()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $ID = $postValue['id'];
            $connection = Yii::$app->dbERP_easyhr_PAYROLL;
            $transaction = $connection->beginTransaction();
            try {

                $eff = $connection->createCommand()->update('ot_requestmaster', ['is_approved' => 2], " id=$ID ")->execute();
                $eff2 = $connection->createCommand()->update('ot_requestdetail', ['is_approved' => 2], " ot_requestmaster_id=$ID ")->execute();
                $transaction->commit();
                echo $eff;
            } catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception('ERROR' . $e->getMessage());
            }
        }
    }



}
