<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 20/10/2559
 * Time: 11:16 6.1
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use app\api\ApiReport;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;


HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/jquery.datetimepicker.full.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

 $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });

    //Date range as a button
   $('#daterange-btn').daterangepicker(
        {
          ranges: {            
            '': [moment(), moment()],
            'ย้อนหลัง 12 เดือน': [moment().subtract(11, 'month').startOf('days'), moment().subtract(0, 'month').endOf('days')],
            'ย้อนหลัง 36 เดือน': [moment().subtract(35, 'month').startOf('days'), moment().subtract(0, 'month').endOf('days')]
          },
          
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }
    );

});
JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานรอบการเข้ารับบริการของลูกค้า</h3>

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
    </div>
    <!-- ./box-header -->

    <div class="box-body">
        <div class="row">
            <form method="post" action="customer_service">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key=>$value) {
                                    $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>ช่วงวันที่</label>
                        &nbsp;
                          <div class="btn-group">                            
                            <div class="input-group" style="width: 250px">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" name="reservation" id="reservation" data-provide="datepicker" data-date-language="th" <?php if(!empty($selected_company)){ echo 'value="'.$selected_months.'"';}?>>
                            </div>
                          </div>
                          &nbsp;&nbsp;
                          <label>VIN ย้อนหลัง</label>
                        &nbsp;
                          <div class="btn-group">                            
                            <div class="input-group"">
                              <input type="text" class="btn btn-default pull-right"
                              name="timerange" id="daterange-btn" <?php if(!empty($selected_timerage)){ echo 'value="'.$selected_timerage.'"';}?>>
                            </div>
                          </div>
                          &nbsp;&nbsp;                          
                          </center>
                          <center><br>
                        <label>เลือกประเภทรถ</label>
                        &nbsp;
                        <div class="btn-group">
                        <input type="radio" name="technician" value="รถเล็ก" <?php echo ($selected_technician =='รถเล็ก') ? 'checked' : '';?> checked> รถเล็ก
                        &nbsp;
                        <input type="radio" name="technician" value="รถกลาง" <?php echo ($selected_technician =='รถกลาง') ? 'checked' : '';?>> รถกลาง
                        &nbsp;
                        <input type="radio" name="technician" value="รถใหญ่" <?php echo ($selected_technician =='รถใหญ่') ? 'checked' : '';?>> รถใหญ่
                        </div>
                        </center>                    

                    <center><br>
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/services/exportloop">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-file-excel-o"></i> ออกรายงาน Excel
                        </button>
                        </a>
                        </div>
                    </center>

                </form>
            </div>
        </div>
<?php if($query){ ?>        
        <!-- ./box-body -->
    <?php if(empty($customers)){ ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title">แบบแสดงรายการ</h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ชื่อ - นามสกุล
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เบอร์โทร
                                        </th>                                       
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ที่อยู่
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รุ่นรถ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขเครื่อง
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        แซสซิส
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ทะเบียน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันที่เข้าสุดท้าย
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขกิโลเมตรล่าสุด
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">สมคิด    คิดอ่าน</td>
                                        <td>081-8562365</td>
                                        <td>74/1 ต.5874_รอบเวียง อ.657_เมือง จ.45_เชียงราย 1_ภาคเหนือ 57010</td>
                                        <td>TFR54</td>
                                        <td>BC6352</td>
                                        <td>6T2563265</td>
                                        <td>ชรบธ8563</td>
                                        <td>12/06/1958</td>
                                        <td>123542</td>
                                    </tr>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php } else{ ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title">แบบแสดงรายการ</h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ชื่อ - นามสกุล
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เบอร์โทร
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ที่อยู่
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รุ่นรถ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขเครื่อง
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        แซสซิส
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ทะเบียน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันที่เข้าสุดท้าย
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขกิโลเมตรล่าสุด
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($customers as $value) {
                                    ?>                                    
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">
                                        <?php echo($value['name']) ? ($value['name']) : '-' ; ?> 
                                        </td>
                                        <td>
                                        <?php echo($value['tel']) ? ($value['tel']) : '-' ; ?>
                                        </td>
                                        <td>
                                        <?php echo($value['address']) ? ($value['address']) : '-' ; ?>
                                        </td>
                                        <td>
                                        <?php echo($value['model_name']) ? ($value['model_name'])  : '-' ; ?> 
                                        </td>
                                        <td>
                                        <?php echo($value['engin_id']) ? ($value['engin_id']) : '-' ; ?>
                                        </td>
                                        <td>
                                        <?php echo($value['chassis']) ? ($value['chassis']) : '-' ; ?>
                                        </td>
                                        <td>
                                        <?php echo($value['register']) ? ($value['register']) : '-' ; ?>
                                        </td>
                                        <td>
                                        <?php echo($value['date']) ? ($value['date']) : '-' ; ?>
                                        </td>
                                        <td>
                                        <?php echo($value['mile']) ? ($value['mile']) : '-' ; ?>
                                        </td>
                                    </tr>  
                                    <?php } ?>                                  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php } ?>

<?php if(empty($totalVIN)) { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-warning">
                 <div class="box-header">
                    <h3 class="box-title">แบบแสดงจำนวน</h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รอบVIN
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        จำนวน
                                        </th>                                        
                                    </tr>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">จำนวนVIN ย้อนหลัง 12 เดือน รถเล็ก</td>
                                        <td>4156</td>
                                    </tr>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-warning-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php } else{ ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-warning">
                 <div class="box-header">
                    <h3 class="box-title">แบบแสดงจำนวน</h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รอบVIN
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        จำนวน
                                        </th>                                        
                                    </tr>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">
                                        จำนวนVIN ย้อนหลัง
                                        <?php $year = $year_rageEnd-$year_rageStart;
                                        if($year == 1){
                                            echo "12";
                                        }else{
                                            echo "36";
                                        }
                                         ?>
                                        เดือน 
                                        <?php if($technician_car == "3,4"){
                                            echo "รถเล็ก";
                                        }elseif($technician_car == "1,2"){
                                            echo "รถใหญ่";
                                        }else{
                                            echo "รถกลาง";
                                        }?>
                                        </td>
                                        <td><?php print_r($totalVIN); ?></td>
                                    </tr>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-warning-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php } ?>
<?php } ?>
</div>
<!-- /.box-->
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>