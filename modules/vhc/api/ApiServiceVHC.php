<?php


/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/1/2017 AD
 * Time: 10:50
 */

namespace app\modules\vhc\api;


use app\api\Common;
use app\modules\vhc\models\VHC\VhcGrouplist;
use app\modules\vhc\models\VHC\VhcList;
use Yii;
use  yii\helpers\BaseJson;
use app\modules\vhc\controllers\MasterController;
use app\modules\vhc\models\serviceold\Crepair;
use app\modules\vhc\models\serviceold\CrepairList;
use app\modules\vhc\models\serviceold\Wage;
use app\modules\vhc\models\serviceold\Wagelist;
use app\modules\vhc\models\serviceold\WageModel;
use app\modules\vhc\models\serviceold\YtaxListParts;
use app\modules\vhc\models\serviceold\Amodel;
use app\modules\vhc\models\VHC\VhcModel;
use app\modules\vhc\models\serviceold\Parts;
use app\modules\vhc\models\service\VhcMapping;
use app\modules\vhc\models\service\VhcSeviceLabor;
use app\modules\vhc\models\service\VhcSevicePart;
use app\api\DBConnect;


class ApiServiceVHC
{
    public static function mapModelwage($groupModel,$wage_id)
    {
        $data_wage_model = WageModel::find()->where(['model_group'=>$groupModel])->andWhere(['id_wage'=>$wage_id])->asArray()->all();
        return $data_wage_model;
    }
    public static function saveServiceMapping($mapping_id=null,$model_id,$vhc_model_id)
    {
        if($mapping_id!=null||$mapping_id!=0){
            $model = VhcMapping::findOne($mapping_id);
            $model->amodel_id = $model_id;
            $model->vhc_model_id = $vhc_model_id;
            $model->record_status = 1;
        }else{
            $model = new VhcMapping();
            $model->amodel_id = $model_id;
            $model->vhc_model_id = $vhc_model_id;
            $model->record_status = 1;
        }
        $lassmodel = null;
        if ($model->validate()) {
            $statusSave = $model->save();
            $model->savestatus = $statusSave; 
        } else {
            $errors = $model->errors;
            echo "model can't validater <pre>";
            print_r($errors);
            echo "</pre>";
        }
        $lassmodel = clone $model;
        return $lassmodel->toArray();
    }
    public static function saveServiceParts($groupModel,$wage_id)
    {

    }
    public static function saveServiceLabor($VhcSeviceLabor_id=null,$modelwage,$mapModel,$crepair,$crepair_list,$labor)
    {
        if($mapping_id!=null||$mapping_id!=0){
            $model = VhcSeviceLabor::findOne($VhcSeviceLabor_id);
            $model->vhc_mapping_id = $mapModel['id'];
            $model->wage_id=$labor['id'];
            $model->crepair_id=$crepair['id'];
            $model->crepair_list_id=$crepair_list['id'];
            $model->name=$labor['name'];
            $model->price=$modelwage['LABOURFEE'];
            $model->vat = $modelwage['LABOURFEE'];
            $model->invatprice='';
            $model->record_status = 1;
        }else{
            $model = new VhcMapping();
            $model->amodel_id = $model_id;
            $model->vhc_model_id = $vhc_model_id;
            $model->record_status = 1;
        }
    }

    public static  function grouplistnamebyid($idmodel){

        $model = VhcModel::find()->where(['id'=>$idmodel])->asArray()->one();
       return $model;

    }

    public static  function grouplistnameall(){

        $model = VhcModel::find()->asArray()->all();
        $arrmodel = [];

        foreach ($model as $k => $v){
            $arrmodel[$v['id']] = $v['model_name'];
        }
        return $arrmodel;
    }
    public static  function listnamebyid($idmodellist){

        $modellist = VhcGrouplist::find()->where(['id'=>$idmodellist])->asArray()->one();
          return $modellist;


    }

}


?>