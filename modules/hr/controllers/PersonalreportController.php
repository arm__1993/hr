<?php

namespace app\modules\hr\controllers;

use app\api\Helper;
use app\modules\hr\apihr\ApiHr;
use Yii;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Department;
use app\modules\hr\models\Position;
use app\modules\hr\models\Section;
use Zend\Barcode\Barcode;
use mPDF;
use ZipArchive;
use app\modules\hr\controllers\MasterController;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use app\modules\hr\apihr\ApiPDF;
use Da\QrCode\QrCode;
use Zxing\QrReader;


class PersonalreportController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEmpcard()
    {

        $data = [];

        $companyinfo = $this->getObjCompany();

        return $this->render('empcard', [
            'data' => $data,
            'companyinfo' => json_encode($companyinfo)
        ]);
    }

    public function getObjCompany()
    {

        $companyinfo = [];

        $objWhere = ['!=', 'status', '99'];

        $companyinfo = [];
        $companys = Workingcompany::find()->where($objWhere)->all();
        $departments = Department::find()->where($objWhere)->all();
        $sections = Section::find()->where($objWhere)->all();

        $grpDep = [];
        foreach ($departments as $no => $dep) {

            $grpSec = [];
            foreach ($sections as $sNo => $sec) {

                if ($sec->department == $dep->id) {

                    $grpSec[] = ['id' => $sec->id, 'name' => $sec->name];
                }
            }

            $grpDep[$dep->company][$dep->id] = [

                'id' => $dep->id,
                'name' => $dep->name,
                'sections' => $grpSec
            ];

        }


        foreach ($companys as $no => $coms) {

            $companyinfo[$coms->id]['info'] = ['name' => $coms->name, 'id' => $coms->id];
            $companyinfo[$coms->id]['departments'] = $grpDep[$coms->id];

        }

        return $companyinfo;

    }

    public function actionEmpsso()
    {
        return $this->render('empsso');
    }

    public function actionSearchemp()
    {

        $str = Yii::$app->request->get();
        $empData = [];
        // $empData[] = ['id' => 1,'text' => 'aaa'];
        // $empData[] = ['id' => 2,'text' => 'bbb'];

        $empObj = Empdata::find()
            ->where(['like', 'Name', $str['q']])
            ->orWhere(['like', 'Surname', $str['q']])
            ->all();

        foreach ($empObj as $no => $emp) {
            $empData[] = ['id' => $emp->DataNo, 'text' => $emp->Name . ' ' . $emp->Surname];
        }

        return json_encode($empData);
    }

    public function actionSearchempcard()
    {

        $str = Yii::$app->request->post();

        $emps = $this->getEmpData($str);

        $sPage = $this->calPageSize('A4');

        $html = '<div id="show_content" class="empcard_content">';

        $cIndex = 0;
        foreach ($emps as $no => $emp) {

            $cIndex++;


            $picPath = dirname(dirname(dirname(dirname(__FILE__)))) . '/upload/personal/' . $emp['Pictures_HyperL'];
            //echo dirname(dirname(dirname(dirname(__FILE__)))).'<br>';
            //echo $picPath.'<br>';

            if (!file_exists($picPath)) {

                $emp['Pictures_HyperL'] = 'avatar2.png';
            }

            $setIdCard = ($emp['ID_Card'] == '') ? 'no id card' : $emp['ID_Card'];

            $imgBarcode = $this->writeBarcode($setIdCard);


            $html .= Yii::$app->controller->renderPartial('cardblock', [
                'emp' => $emp,
                'emp_barcode' => $imgBarcode
            ]);

            //echo  $cIndex." >= ".$sPage."<br>";
            if ($cIndex >= $sPage) {
                $html .= '</div>';
                $html .= '<div id="show_content" class="empcard_content">';
                $cIndex = 0;
            }

        }

        $html .= "</div>";

        return $html;

    }

    public function getEmpData($str)
    {

        $oStr = json_decode($str['info'], true);
        $havePos = false;
        $haveEmp = false;
        $fixEmp = false;

        //echo '<pre>';print_r($oStr);echo '</pre>';
        //create where
        $whereA = " ";
        //บริษัท
        if (isset($oStr['company']) && $oStr['company'] != '' && $oStr['company'] != 'null') {
            $whereA .= " AND p.WorkCompany = '" . $oStr['company'] . "' ";
            $havePos = true;
        }
        //department
        if (isset($oStr['department']) && $oStr['department'] != '' && $oStr['department'] != 'null') {
            $whereA .= " AND p.Department = '" . $oStr['department'] . "' ";
            $havePos = true;
        }
        //section
        if (isset($oStr['section']) && $oStr['section'] != '' && $oStr['section'] != 'null') {
            $whereA .= " AND p.Section = '" . $oStr['section'] . "' ";
            $havePos = true;
        }

        //--
        //emp
        $whereB = "";
        if (isset($oStr['work_start_begin']) && $oStr['work_start_begin'] != '') {
            $whereB .= " AND e.Start_date BETWEEN '" . $oStr['work_start_begin'] . "' AND '" . $oStr['work_start_end'] . "' ";
            $haveEmp = true;
        }
        if (isset($oStr['emp_code']) && $oStr['emp_code'] != '') {
            $whereB .= " AND e.Code LIKE '" . $oStr['emp_code'] . "%'  ";
            $haveEmp = true;
        }
        if (isset($oStr['emp_name']) && count($oStr['emp_name']) > 0) {
            $inId = implode(',', $oStr['emp_name']);
            $whereB .= " AND e.DataNo IN (" . $inId . ")  ";
            $haveEmp = true;
            $fixEmp = true;
        }
        //--


        $sql = "SELECT e.*,p.Name AS positionName ";
        $sql .= " FROM ";
        $sql .= " emp_data e";
        $sql .= ($havePos && !$fixEmp || true) ? " INNER JOIN ERP_easyhr_OU.relation_position rp ON(e.ID_Card = rp.id_card)" : "";
        $sql .= ($havePos && !$fixEmp || true) ? " INNER JOIN ERP_easyhr_OU.position p ON(rp.position_id = p.id)" : "";
        $sql .= " WHERE 1 ";
        $sql .= ($havePos && !$fixEmp) ? $whereA : " ";
        $sql .= ($haveEmp) ? $whereB : " ";
        //echo $sql;exit();
        //$sql .= " limit 0,20";

        $emps = Yii::$app
            ->dbERP_easyhr_checktime
            ->createCommand($sql)
            ->queryAll();
        return $emps;
    }

    public function actionPrintpdfempcard()
    {

        //$str        = Yii::$app->request->post();
        //echo '<pre>';print_r($str);echo '</pre>';exit();
        // $mpdf = new mPDF('th', 'Tharlon-Regular','12px');

        // $emp = Empdata::findOne(214);
        // $html = $str['html'];
        // $mpdf->WriteHTML( $html);
        // //$mpdf->Output();
        // $content = $mpdf->Output('', 'S'); // Saving pdf to attach to email
        // $content = chunk_split(base64_encode($content));
        // echo $content;
        // exit;

        $str = Yii::$app->request->get();
        $emps = $this->getEmpData($str);

        $html = '';
        $html = '<style>@page {

            margin: 20px;
        }</style>';


        $bgImp = imagecreatefrompng("images/wshr/template_empcard_front.png");
        $bgImpBack = imagecreatefrompng("images/wshr/template_empcard_back.png");
        //echo $bgImp;exit();
        foreach ($emps as $no => $emp) {

            $picPath = dirname(dirname(dirname(dirname(__FILE__)))) . '/upload/personal/' . $emp['Pictures_HyperL'];
            //echo $picPath;exit();
            if (!file_exists($picPath)) {

                $emp['Pictures_HyperL'] = imagecreatefromjpeg("upload/personal/3560500575223_m.jpg");
            }
            $fullPathImg = dirname(dirname(dirname(dirname(__FILE__)))) . '/upload/personal/' . $emp['Pictures_HyperL'];


            $setIdCard = ($emp['ID_Card'] == '') ? 'no id card' : $emp['ID_Card'];

            $imgBarcode = $this->writeBarcode($setIdCard);


            $html .= Yii::$app->controller->renderPartial('cardblockpdf', [
                'emp' => $emp,
                'emp_barcode' => $imgBarcode,
                'fullPathImg' => $fullPathImg,
                'bgImp' => $bgImp,
                'bgImpBack' => $bgImpBack
            ]);
        }

        //$pdf = new mPDF('th', 'Tharlon-Regular', '12px');
        //$pdf = new mPDF('th', 'A4');
        $pdf = new \Mpdf\Mpdf();

        $pdf->setAutoBottomMargin = false;

        $pdf->WriteHTML($html);
        $pdf->showImageErrors = true;
        $pdf->Output();

        exit();

    }


    function writeBarcode($idcard)//บาร์โค้ด
    {

        // Only the text to draw is required.
        $barcodeOptions = [
            'text' => $idcard,
            'barHeight' => 53,
            'factor' => 1.8];

        // No required options.
        $rendererOptions = [];

        // Draw the barcode, capturing the resource:
        $renderer = Barcode::factory(
            'code39',
            'image',
            $barcodeOptions,
            $rendererOptions
        );
        $imageResource = $renderer->draw();
//        $pahrt = Yii::$app->request->baseUrl . '/images/';
//        mkdir($pahrt, 0777, true);
        $store_image = imagepng($imageResource, 'upload/emp_img/barcode/' . $idcard . ".png");
        //$store_image = imagepng($imageResource);

        //echo '<img src="'.$store_image.'">';
        ob_start();
        imagepng($imageResource);
        // Capture the output
        $imagedata = ob_get_contents();
        // Clear the output buffer
        ob_end_clean();

        return base64_encode($imagedata);
    }


    function Qrcode($fullname,$adss,$setIdCard,$nickname)
    {


        $qrCode = (new QrCode('หมายเลขบัตรประชาชน '.$setIdCard.' ชื่อ '.$fullname.' ชื่อเล่น '.$nickname.' ที่อยู่ '.$adss))
            ->setSize(143)
            ->setMargin(5)
            ->useForegroundColor(0, 0, 0);
        $qrCode->writeFile('upload/emp_img/qrcode/'.$setIdCard.'.png');

    }
    public function actionQrcode()
    {
        $name = 'วัชรพันธ์  ผัดดี';
        $address = '236 หมู่ 6 ตำบล จำป่าหวาย';
        $ID_card = '156010030636900';
        $qrCode = (new QrCode($name.$address.$ID_card))
            ->setSize(133)
            ->setMargin(5)
            ->useForegroundColor(0, 0, 0);
        $qrCode->writeFile('upload/emp_img/barcode/code.png');
      // header('Content-Type: '.$qrCode->getContentType());
       // echo $qrCode->writeString();
       // $img = '<img src"' . $qrCode->writeDataUri() . '">';






    }


    function calPageSize($pageSize, $cardH = array('h' => 1004, 'w' => 638))
    {

        $cOut = 0;

        $pW = 0;
        $pH = 0;

        $cW = $cardH['w'];
        $cH = $cardH['h'];//px

        if ($pageSize == 'A4') {

            $pW = 638 * 5.4;
            $pH = 1004 * 8.5;//cm
        }

        //3.77
        $sW = floor($pW / $cW);
        $sH = floor($pH / $cH);


        return $sH * $sW;
    }

    public function actionPng()
    {

        for ($i = 1; $i <= 2; $i++) {

            $front = imagecreatefrompng('images/wshr/33.png');
            $back = imagecreatefrompng('images/wshr/template_empcard_back.png');
            $emp = imagecreatefrompng('upload/emp_img/3560500575223_56_HBSO.png');

            imagecopy($front, $emp, 223, 210, 0, 0, 325, 325);

            header('Content-type: image/png');
            $dir = 'upload/personal/aeedy' . $i;
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }


            imagepng($front, 'upload/personal/aeedy' . $i . '/aeedy' . $i . '.png');
            imagepng($back, 'upload/personal/aeedy' . $i . '/aeedy_back' . $i . '.png');
            imagedestroy($front);
            imagedestroy($emp);
        }


        return $this->render('png', [

        ]);
    }


    public function actionZiphiry()
    {


        $basePath = Yii::$app->basePath;

        $zipNamefile = "emp_Card_56";
        $destination = $basePath . "/upload/emp_img/emp_Card/" . $zipNamefile . ".zip";

        $source = $basePath . "/upload/emp_img/emp_Card/";

        $arrNamefile = [];


        $cdir = scandir($source);
//

        foreach ($cdir as $key => $values) {
            if (!in_array($values, array(".", "..", ".DS_Store"))) {
                if (is_dir($source . DIRECTORY_SEPARATOR . $values)) {
                    $arrNamefile[$values] = dirToArray($source . DIRECTORY_SEPARATOR . $values);
                    // echo DIRECTORY_SEPARATOR;

                    echo '<pre>';
                    print_r($values);
                    echo '</pre>';


                } else {
                    $arrNamefile[] = $values;


//                    echo '<pre>';
//                    print_r($values);
//                    echo '</pre>';
                }
            }
        }


        $zip = new ZipArchive;
        if ($zip->open($destination, ZipArchive::CREATE) === TRUE) {
            foreach ($arrNamefile as $key => $value) {
                $PATH = $source . $value;
                $zip->addFile($PATH, basename($PATH));
                //return Yii::$app->response->sendFile($PATH, basename($PATH));
                echo '<pre>';
                print_r($zip);
                echo '</pre>';
            }


            $zip->close();

            echo '<pre>';
            print_r($zip);
            echo '</pre>';


        }


    }

    public function get_zip_path()
    {
        return $this->path;
    }

    public function actionZippings()
    {


        $basePath = Yii::$app->basePath;

        $zipNamefile = "emp_Card_56";
        $destination = $basePath . "/upload/emp_img/emp_Card/" . $zipNamefile . ".zip";

        $source = $basePath . "/upload/emp_img/emp_Card/";

        $rootPath = realpath($source);


        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($destination, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Initialize empty "delete list"
        $filesToDelete = array();

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );


        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);

                // Add current file to "delete list"
                // delete it later cause ZipArchive create archive only after calling close function and ZipArchive lock files until archive created)
                if ($file->getFilename() != 'important.txt') {
                    $filesToDelete[] = $filePath;
                }
            }
        }

        // Zip archive will be created only after closing object


        $zip->close();


        //  Delete all files from "delete list"
        foreach ($filesToDelete as $file) {
            //unlink($file);
        }

        $paths = Yii::$app->request->baseUrl . '/upload/emp_img/emp_Card/';

        echo $paths;

    }


//    private function urldow(){
//        $zip_path = Yii::$app->basePath.'/upload/emp_img/emp_Card/emp_Card_56.zip';
//        header( "Pragma: public" );
//        header( "Expires: 0" );
//        header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
//        header( "Cache-Control: public" );
//        header( "Content-Description: File Transfer" );
//        header( "Content-type: application/zip" );
//        header( "Content-Disposition: attachment; filename=\"" . 'abc.zip' . "\"" );
//        header( "Content-Transfer-Encoding: binary" );
//        header( "Content-Length: " . filesize( $zip_path ) );
//
//        readfile(  Yii::$app->basePath.'/upload/emp_img/emp_Card/emp_Card_56.zip' );
//    }

    public function actionPngz()
    {
        $str = Yii::$app->request->post();
        $data = json_decode($str['info']);

        $compane = $data->company;
        $name = $data->emp_name[0];
        $department = $data->department;//แผนก
        $section = $data->section; //ฝ่าย
        $emp_code = $data->emp_code;// รหัสพนักงาน


        $model1 = ApiHr::Employee_Card($compane, $name, $department, $section, $emp_code);


        $all_company = ApiHr::getWorking_company();
        $emp_data = ApiPDF::get_emp_data_all();



//        3720100360121   IIA005101
//        CEX106109   3570101421648
//        AMG105101  3102201578501

        foreach ($model1 as $k => $v) {

            $empfullname = $emp_data[$v['ID_Card']];
            $v['image_card'];
            $v['Position'];
            $empfullname['full_namenotbe'];
            $v['ID_Card'];
            $nickname = $v['Nickname'];
            $adss = $empfullname['address'];
            $fullname = $empfullname['full_namenotbe'];

            $Working_Company = $all_company[$v['Working_Company']];

            $setIdCard = $v['ID_Card']; //1560100306365;
            $this->writeBarcode($setIdCard);

            $this->Qrcode($fullname,$adss,$setIdCard,$nickname);


            $barcode = imagecreatefrompng('upload/emp_img/barcode/' . $setIdCard . '.png');//บาร์โค้ด
            $qr = imagecreatefrompng('upload/emp_img/qrcode/' . $setIdCard . '.png');//QRcode



//            $front = imagecreatefrompng('upload/emp_img/employee_card/page.png');  //หน้าเก่า
//            $back = imagecreatefrompng('upload/emp_img/employee_card/after.png');  //หลังเก่า


            $front = imagecreatefrompng('upload/emp_img/employee_card/pageNew.png');  //หน้าใหม่
            $back = imagecreatefrompng('upload/emp_img/employee_card/afterNew.png');  //หลังใหม่


            $dir = 'upload/emp_img/Pictures_HyperL/'.$v['image_card'];





            if( file_exists($dir) )
            {
                $emp = imagecreatefrompng('upload/emp_img/Pictures_HyperL/'.$v['image_card']); //รูปคน
            }
            else
            {
                $emp = imagecreatefrompng('upload/emp_img/Pictures_HyperL/avatar_empty.png'); //รูปคน
            }

//
//            if (is_dir($dir)) {
//
//                echo is_dir($dir);
//
//
//                if ($dh = opendir($dir)) {
//                    while (($file = readdir($dh)) !== false) {
//
//
//                        echo  $file . "\n";
//
//                        if($v['image_card'].'.png' == $file){
//                            $emp = imagecreatefrompng('upload/emp_img/Pictures_HyperL/'.$v['image_card']); //รูปคน
//                        }elseif ($v['image_card'].'.png' != $file){
//                            $emp = imagecreatefrompng('upload/emp_img/Pictures_HyperL/avatar_empty.png'); //รูปคน
//                        }
//                        echo  $file . "\n";
//                    }
//                    closedir($dh);
//                }
//            }



           // exit();

            //$emp = imagecreatefrompng('upload/emp_img/Pictures_HyperL/'.$v['image_card']); //รูปคน





            header('Content-type: image/jpeg');
            $emp_name = $empfullname['full_namenotbe']; //'วัชรพันธ์' . ' ' . 'ผัดดี'; // ชื่อ
            $Position = $v['Position']; //'Developer'; // ตำแหน่งงาน


            $blackF = imagecolorallocate($front, 0, 0, 0);
            imagettftext($front, 35, 0, 230, 640, $blackF, "fonts/thsarabunnew_bold-webfont.ttf", $emp_name);//หน้าชื่อสกุล
            imagettftext($front, 30, 0, 235, 730, $blackF, "fonts/thsarabunnew_bold-webfont.ttf", $Position);//หน้าตำแหน่ง



//            imagecopy($front, $emp, 228, 210, 0, 0, 325, 325);//หน้ากับรูปบุคคลเก่า
//            imagecopy($front, $barcode, 150, 870, 0, 0, 466, 115);//หน้ากับบาร์โค้ดเก่า
//            imagecopy($back, $qr, 24, 675, 0, 0, 153, 153);//หลังกับคิวอาร์โค้ดเก่า


            imagecopy($front, $emp, 232, 207, 0, 0, 329, 329);//หน้ากับรูปบุคคลเก่า
            imagecopy($front, $barcode, 150, 870, 0, 0, 466, 115);//หน้ากับบาร์โค้ดเก่า
            imagecopy($back, $qr, 24, 675, 0, 0, 153, 153);//หลังกับคิวอาร์โค้ดเก่า



            header('Content-type: image/png');
            $dir = 'upload/emp_img/emp_Card/' . $v['ID_Card'];
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            imagepng($front, 'upload/emp_img/emp_Card/' . $v['ID_Card'] . '/page.png');
            imagepng($back, 'upload/emp_img/emp_Card/' . $v['ID_Card'] . '/after.png');
            imagedestroy($front);
            imagedestroy($emp);

        }

      $companey_working = $Working_Company['short_name'];

        $paths = $this->Zipping($companey_working);
       return  $this->renderPartial('png', [
            'data' => $model1,
           'paths' => $paths
        ]);
    }

    private function Zipping($companey_working)
    {


        $basePath = Yii::$app->basePath;

        $zipNamefile = $companey_working;
        $destination = $basePath . "/upload/emp_img/emp_Card/" . $zipNamefile . ".zip";

        $source = $basePath . "/upload/emp_img/emp_Card/";

        $rootPath = realpath($source);



        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($destination, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Initialize empty "delete list"
        $filesToDelete = array();

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        
        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);

                // Add current file to "delete list"
                // delete it later cause ZipArchive create archive only after calling close function and ZipArchive lock files until archive created)
                if ($file->getFilename() != 'important.txt') {
                    $filesToDelete[] = $filePath;
                }
            }
        }

        // Zip archive will be created only after closing object


        $zip->close();


        //  Delete all files from "delete list"
        foreach ($filesToDelete as $file) {
         //   unlink($file);//gendelet all
        }

        $paths = Yii::$app->request->baseUrl . '/upload/emp_img/emp_Card/'.$companey_working.'.zip';
//
        return  $paths;

    }
    public function actionUnpngz()
    {
        $month_paypnd = Yii::$app->request->post();
        $basePath = Yii::$app->basePath;

        foreach ($month_paypnd as $k => $v){

            $t = explode('_zip', $k);

            $name = $t[0].'.zip';
            $un = explode('/', $name);

            $destination = $basePath . "/upload/emp_img/emp_Card/".$un[5];
            unlink($destination);
        }
    }


}