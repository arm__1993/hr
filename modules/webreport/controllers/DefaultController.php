<?php

namespace app\modules\webreport\controllers;


use app\api\Common;
use app\api\DateTime;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use mPDF;


/**
 * Default controller for the `webreport` module
 */
class DefaultController extends Controller
{
    public $layout = 'webreportlayouts';


    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionSample()
    {

        $compa = ApiReport::getNameCompany();

        print_r($compa['get_years']);
        echo "<br>";
        print_r($compa['get_company']);
    } 


}
