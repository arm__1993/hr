<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_money_bonds".
 *
 * @property integer $id
 * @property integer $emp_data_id
 * @property integer $start_money_bonds
 * @property integer $start_pay_bonds
 * @property double $money_bonds
 * @property string $bank_name
 * @property string $bank_no
 * @property double $first_money
 * @property double $pay_money
 * @property string $start_date_pay
 * @property string $end_date_pay
 * @property string $bondsman_prefix
 * @property integer $bondsman_sex
 * @property string $bondsman_name
 * @property string $bondsman_surname
 * @property string $bondsman_idcard
 * @property string $bondsman_birthday
 * @property string $bondsman_business
 * @property string $bondsman_work_place
 * @property string $bondsman_month
 * @property string $bondsman_year
 * @property string $addr_idcard_address
 * @property string $addr_idcard_moo
 * @property string $addr_idcard_lane
 * @property string $addr_idcard_road
 * @property string $addr_idcard_village
 * @property string $addr_idcard_district
 * @property string $addr_idcard_amphur
 * @property string $addr_idcard_province
 * @property string $addr_idcard_postcode
 * @property string $addr_plance_address
 * @property string $addr_plance_moo
 * @property string $addr_plance_lane
 * @property string $addr_plance_road
 * @property string $addr_plance_village
 * @property string $addr_plance_district
 * @property string $addr_plance_amphur
 * @property string $addr_plance_province
 * @property string $addr_plance_postcode
 */
class EmpMoneyBonds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_money_bonds';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emp_data_id', 'start_money_bonds', 'start_pay_bonds', 'bondsman_sex'], 'integer'],
            [['money_bonds', 'first_money', 'pay_money'], 'number'],
            [['start_date_pay', 'end_date_pay', 'bondsman_birthday'], 'safe'],
            [['bank_name', 'bondsman_prefix', 'bondsman_name', 'bondsman_surname', 'addr_idcard_road', 'addr_idcard_village', 'addr_idcard_district', 'addr_idcard_amphur', 'addr_idcard_province', 'addr_plance_road', 'addr_plance_village', 'addr_plance_district', 'addr_plance_amphur', 'addr_plance_province'], 'string', 'max' => 100],
            [['bank_no'], 'string', 'max' => 20],
            [['bondsman_idcard'], 'string', 'max' => 13],
            [['bondsman_business', 'bondsman_work_place'], 'string', 'max' => 250],
            [['bondsman_month', 'bondsman_year'], 'string', 'max' => 2],
            [['addr_idcard_address', 'addr_idcard_postcode', 'addr_plance_address', 'addr_plance_postcode'], 'string', 'max' => 50],
            [['addr_idcard_moo', 'addr_idcard_lane', 'addr_plance_moo', 'addr_plance_lane'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_data_id' => 'Emp Data ID',
            'start_money_bonds' => 'Start Money Bonds',
            'start_pay_bonds' => 'Start Pay Bonds',
            'money_bonds' => 'Money Bonds',
            'bank_name' => 'Bank Name',
            'bank_no' => 'Bank No',
            'first_money' => 'First Money',
            'pay_money' => 'Pay Money',
            'start_date_pay' => 'Start Date Pay',
            'end_date_pay' => 'End Date Pay',
            'bondsman_prefix' => 'Bondsman Prefix',
            'bondsman_sex' => 'Bondsman Sex',
            'bondsman_name' => 'Bondsman Name',
            'bondsman_surname' => 'Bondsman Surname',
            'bondsman_idcard' => 'Bondsman Idcard',
            'bondsman_birthday' => 'Bondsman Birthday',
            'bondsman_business' => 'Bondsman Business',
            'bondsman_work_place' => 'Bondsman Work Place',
            'bondsman_month' => 'Bondsman Month',
            'bondsman_year' => 'Bondsman Year',
            'addr_idcard_address' => 'Addr Idcard Address',
            'addr_idcard_moo' => 'Addr Idcard Moo',
            'addr_idcard_lane' => 'Addr Idcard Lane',
            'addr_idcard_road' => 'Addr Idcard Road',
            'addr_idcard_village' => 'Addr Idcard Village',
            'addr_idcard_district' => 'Addr Idcard District',
            'addr_idcard_amphur' => 'Addr Idcard Amphur',
            'addr_idcard_province' => 'Addr Idcard Province',
            'addr_idcard_postcode' => 'Addr Idcard Postcode',
            'addr_plance_address' => 'Addr Plance Address',
            'addr_plance_moo' => 'Addr Plance Moo',
            'addr_plance_lane' => 'Addr Plance Lane',
            'addr_plance_road' => 'Addr Plance Road',
            'addr_plance_village' => 'Addr Plance Village',
            'addr_plance_district' => 'Addr Plance District',
            'addr_plance_amphur' => 'Addr Plance Amphur',
            'addr_plance_province' => 'Addr Plance Province',
            'addr_plance_postcode' => 'Addr Plance Postcode',
        ];
    }
}
