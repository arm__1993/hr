<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "tax_pnd_detail".
 *
 * @property integer $id
 * @property integer $tax_pnd_head_id
 * @property integer $emp_no
 * @property string $emp_idcard
 * @property string $emp_firstname
 * @property string $emp_lastname
 * @property string $emp_address
 * @property string $paid_date
 * @property string $paid_amount
 * @property string $paid_tax
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $income_gnr
 * @property string $tax_gnr
 * @property string $income_auth
 * @property string $tax_auth
 * @property string $income_onepaid
 * @property string $tax_onepaid
 * @property string $income_inthai
 * @property string $tax_inthai
 * @property string $income_notin
 * @property string $tax_notin
 */
class TaxPndDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_pnd_detail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_pnd_head_id', 'emp_no', 'emp_idcard', 'emp_firstname', 'emp_lastname', 'emp_address', 'paid_date', 'paid_amount', 'paid_tax', 'createby_user', 'create_datetime'], 'required'],
            [['tax_pnd_head_id', 'emp_no'], 'integer'],
            [['paid_date', 'create_datetime'], 'safe'],
            [['paid_amount', 'paid_tax', 'income_gnr', 'tax_gnr', 'income_auth', 'tax_auth', 'income_onepaid', 'tax_onepaid', 'income_inthai', 'tax_inthai', 'income_notin', 'tax_notin'], 'number'],
            [['emp_idcard', 'createby_user'], 'string', 'max' => 13],
            [['emp_firstname', 'emp_lastname'], 'string', 'max' => 100],
            [['emp_address'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tax_pnd_head_id' => 'Tax Pnd Head ID',
            'emp_no' => 'Emp No',
            'emp_idcard' => 'Emp Idcard',
            'emp_firstname' => 'Emp Firstname',
            'emp_lastname' => 'Emp Lastname',
            'emp_address' => 'Emp Address',
            'paid_date' => 'Paid Date',
            'paid_amount' => 'Paid Amount',
            'paid_tax' => 'Paid Tax',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'income_gnr' => 'Income Gnr',
            'tax_gnr' => 'Tax Gnr',
            'income_auth' => 'Income Auth',
            'tax_auth' => 'Tax Auth',
            'income_onepaid' => 'Income Onepaid',
            'tax_onepaid' => 'Tax Onepaid',
            'income_inthai' => 'Income Inthai',
            'tax_inthai' => 'Tax Inthai',
            'income_notin' => 'Income Notin',
            'tax_notin' => 'Tax Notin',
        ];
    }
}
