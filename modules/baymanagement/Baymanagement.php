<?php

namespace app\modules\baymanagement;

/**
 * baymanagement module definition class
 */
class Baymanagement extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\baymanagement\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
