<?php

namespace app\modules\hr\controllers;

use app\api\DateTime;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\hr\models\Place;
use app\modules\hr\models\Menu;
use app\modules\hr\models\MenuProcess;
use app\modules\hr\models\MenuManage;
use app\modules\hr\models\MenuManageRequest;
use app\modules\hr\models\Position;
use app\modules\hr\apihr\ApiOrganize;
use app\modules\hr\models\Section;
use app\modules\hr\models\Department;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelpers;
use yii\helpers\BaseJson;
use yii\helpers\Json;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPermission;
use yii\helpers\Url;
use app\api\DBConnect;
use app\modules\hr\controllers\MasterController;
use app\api\Utility;
class PermissionController extends MasterController
{
    public $arrdepartment = [];
    public  $arrsection = [];
    public $layout = 'hrlayout';
    
    public $arr_formate = [];
    /**
    * Renders the index view for the module
    * @return string
    */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionMenulink()
    {
        
        $pLists = Place::find()->where(['=','status',2])->all();
        
        //echo '<pre>';print_r($pLists);echo '</pre>';exit();
        //optionMenuAction($sel)
        $addition = [];
        $addition['optionMenuProcess'] = $this->optionMenuAction('');
        
        return $this->render('menulink',[
        'places' => $pLists,
        'addition' =>  $addition
        ]);
    }
    
    public function actionSavemenuall(){
        
        $resOut = array();
        $resOut['status']   = true;
        $resOut['msg']      = "";
        $resOut['html']     = "";
        
        
        $params    = Yii::$app->request->post();
        
        $menusAll = json_decode($params['menuObjAll'],true);
        $menuDels = json_decode($params['forDel'],true);//
        
        //fillter level 1
        $menus = [];
        foreach($menusAll as $no => $fMenu){
            
            if($fMenu['level'] == 1){
                $menus[] = $fMenu;
            }
        }
        
        
        //echo '<pre>';print_r($menus);echo '</pre>';exit();
        $this->menuDeptUpdate($menus);
        
        //delete
        foreach($menuDels as $mMenu){
            
            if($mMenu['action'] == 'del'){
                $res = $this->saveMenu($mMenu, '');
            }
            
        }
        
        
        
        //echo '<pre>';print_r($menus);echo '</pre>';exit();
        
        
        return json_encode($resOut);
        
    }
    
    public function menuDeptUpdate($menusAll, $parentId = ''){
        
        foreach($menusAll as $no => $menu){
            
            $lastId = '';
            //echo '<pre>';print_r($menu);echo '</pre>';
            if($menu['action'] != ''){
                //take db
                $res = $this->saveMenu($menu,$parentId);
                $lastId = $res['lastId'];
                
            }else{
                $lastId = $menu['id'];
            }
            
            if(count($menu['nodes']) > 0){
                
                $this->menuDeptUpdate($menu['nodes'], $lastId);
                
            }
            
        }
        
    }
    public function actionSeartpositionbyprogram()
    {
        $datachild = [];
        $childNode =[];
        $getValue = Yii::$app->request->get('data');
        if(end($getValue)=='All'){
            array_pop($getValue);
            $childNode = $this->getchild(end($getValue),$datachild);
        }else{
            $childNode = $this->getchild(end($getValue),$datachild);
        }
        foreach($childNode as $item){
            array_push($datachild,$item);
        }
        unset($getValue['selectworking']);
        $headMenu = [];
        foreach($getValue as $item){
            $hMenu = Menu::find()->where(['=','id',$item])->asArray()->all();
            $headMenu[] = $hMenu[0];
        }
        
        $mMenu = array_merge($headMenu,$childNode);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $mMenu;
    }
    public function getchild($id_parent,$datachild)
    {
        $data = ApiPermission::getchild($id_parent);
        if(count($data)!=0){
            foreach($data as $item){
                array_push($datachild,$item);
            }
            foreach($data as $item){
                $childNode =  $this->getchild($item['id'],$datachild);
                $datachild = $childNode;
            }
        }
        return $datachild;
    }
    public function actionSeartpositionbyemp()
    {
        $getValue = Yii::$app->request->get();
        $result = ApiPermission::PositionByEmp($getValue['data']);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }
    
    public function actionSeartreprot()
    {
        $postValue = Yii::$app->request->get();
        $obj = json_decode($postValue['data'],true);
        $arrcompany = ApiHr::getWorking_companyById($obj['selectworking']);
        $arrdepartment = ApiHr::getDepartment($obj['selectdepartment']);
        $arrsaction = ApiHr::getSectionInId($obj['selectsection']);
        $arrProgram = ApiPermission::getPlace($obj['programname']);
        $arrMenu = ApiPermission::getMenu($obj['programname']);
        $arrdata = [];
        foreach($arrcompany as $item){
            $arrdata['company'] = $item['name'] ;
        }
        foreach($arrdepartment as $item){
            $arrdata['drepartment'] = $item['name'] ;
        }
        foreach($arrsaction as $item){
            $arrdata['section'][] = $item['name'] ;
        }
        $arrdataView = [];
        $str_programName = '';
        foreach ($arrProgram as $value) {
            $str_programName['name'] = $value['name'];
            $str_programName['id'] = $value['id'];
        }
        foreach($arrMenu as $key => $item)
        {
            if(is_array($item)){
                $arrdataView[$str_programName['id']][$key]['program'] = $str_programName['name'];
                $arrdataView[$str_programName['id']][$key]['menu'] = $item['name'];
            }
        }
        $arrdatalevel['1'] = 'ผู้จัดการอวุโส';
        $arrdatalevel['2'] = 'ผู้จัดการ';
        $arrdatalevel['3'] = 'พนักงาน';
        return $this->renderPartial('reportprogramdata',['data'=>$arrdataView,'head1'=>$arrsaction,'head2'=>$arrdatalevel]);
    }
    public function actionGetdatareport()
    {
        $postValue = Yii::$app->request->get();
        $obj = json_decode($postValue['data'],true);
        $arrcompany = $obj['selectworking'];
        $arrdepartment = $obj['selectdepartment'];
        $arrsaction = $obj['selectsection'];
        $arrProgram = $obj['programname'];
        
        $data = ApiPermission::dataChecklist($arrcompany,$arrdepartment,$arrsaction,$arrProgram);
        return  Json::encode($data);
    }
    
    public function actionGetdepartmentbyid(){
        $id = Yii::$app->request->get('id');
        $department = ApiOrganize::getDepartment($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $department;
    }
    public function actionGetsectionbyid(){
        $id = Yii::$app->request->get('id');
        $section = ApiOrganize::getSection($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $section;
    }
    public function saveMenu($data, $parentId = ''){
        $resOut = array();
        $resOut['status']   = true;
        $resOut['msg']      = "";
        $resOut['html']     = "";
        
        $menu = $data;
        
        try{
            
            if($menu['action'] == 'del'){
                Menu::deleteAll(['=','id',$menu['id']]);
            }else{
                
                $menu['model']['parent_id'] = "".$parentId."";
                
                if($menu['id'] == ""){
                    //new
                    //save
                    $mObj     = new Menu();
                    $getAttr    = $mObj->attributes;
                    
                    unset($menu['model']['id']);
                    $mObj->attributes = array_merge($getAttr,$menu['model']);
                    
                }else{
                    //update
                    $mObj = Menu::findOne($menu['id']);
                    $mObj = $this->createDataUpdate($mObj,$menu['model']);
                    
                }
                
                //echo '<pre>';print_r($mObj->attributes);echo '</pre>';
                
                if ($mObj->validate()) {
                    
                    $afterSave = $mObj->save(false);
                    
                    if (!$afterSave) {
                        $resOut['status']   = false;
                        $resOut['msg']      .= "error save menu";
                    }else{
                        $resOut['lastId']   = $mObj->id;
                    }
                } else {
                    
                    $resOut['status']   = false;
                    $resOut['msg']      .= $this->createValiMsg($mObj->errors);
                    
                }
                
                
                if(!$resOut['status']){
                    throw new Exception($resOut['msg']);
                }
                
            }
            
        } catch(Exception $e){
            
            echo "เกิดข้อผิดพลาด<br>".$e->getMessage();exit();
            
        }
        
        
        
        //echo '<pre>';print_r($menus);echo '</pre>';exit();
        
        
        return $resOut;
    }
    
    
    public function actionSavemenu(){
        
        $resOut = array();
        $resOut['status']   = true;
        $resOut['msg']      = "";
        $resOut['html']     = "";
        
        
        $params    = Yii::$app->request->post();
        
        $menus = json_decode($params['menuObj'],true);
        
        foreach($menus as $no => $menu){
            
            if($menu['action'] == 'del'){
                Menu::deleteAll(['=','id',$menu['id']]);
            }else{
                
                if($menu['id'] == ""){
                    //new
                    //save
                    $mObj     = new Menu();
                    $getAttr    = $mObj->attributes;
                    
                    unset($menu['model']['id']);
                    $mObj->attributes = array_merge($getAttr,$menu['model']);
                    
                }else{
                    //update
                    $mObj = Menu::findOne($menu['id']);
                    $mObj = $this->createDataUpdate($mObj,$menu['model']);
                    
                }
                
                //echo '<pre>';print_r($mObj->attributes);echo '</pre>';exit();
                
                if ($mObj->validate()) {
                    
                    $afterSave = $mObj->save(false);
                    
                    if (!$afterSave) {
                        $resOut['status']   = false;
                        $resOut['msg']      .= "error save menu";
                    }else{
                        $resOut['lastId']   = $mObj->id;
                    }
                } else {
                    
                    $resOut['status']   = false;
                    $resOut['msg']      .= $this->createValiMsg($mObj->errors);
                }
                
                
            }
            
        }
        
        //echo '<pre>';print_r($menus);echo '</pre>';exit();
        
        
        return json_encode($resOut);
        
    }
    
    public function actionSaveplace(){
        
        $resOut = array();
        $resOut['status']   = true;
        $resOut['msg']      = "";
        $resOut['html']     = "";
        
        
        $params    = Yii::$app->request->post();

        $data = $params['data'];

        $Color = Utility::removeDashcolor($data['Color']);
        $fa = Utility::removefa($data['Icon']);
        $fa3x = Utility::removefa3x($fa);



        $data['status'] = '2';
        $data['Color'] = '#'.$Color;
        $data['Icon'] = 'fa '.$fa3x.' '.'fa-3x';
        if(isset($data['id']) && $data['id'] != ''){
            //update
            $pObj = Place::findOne($data['id']);


            $pObj = $this->createDataUpdate($pObj,$data);
            
        }else{
            //save
            $pObj     = new Place();

            $getAttr    = $pObj->attributes;
            $pObj->attributes = array_merge($getAttr,$data);
        }
        
        if ($pObj->validate()) {
            
            $afterSave = $pObj->save(false);
            
            if (!$afterSave) {
                $resOut['status']   = false;
                $resOut['msg']      = "error save place";
            }else{
                $resOut['lastId']   = $pObj->id;
            }
        } else {
            
            $resOut['status']   = false;
            $resOut['msg']      = $this->createValiMsg($pObj->errors);
        }
        
        $resOut['html']  = Yii::$app->controller->renderPartial('programlists',[
        'place' => $pObj
        ]);
        
        
        return json_encode($resOut);
    }
    public function actionDelplace(){
        
        $resOut = array();
        $resOut['status']   = true;
        $resOut['msg']      = "";
        
        $params    = Yii::$app->request->post();
        
        // $place = Place::findOne($param['place_id']);
        //echo ''.$params['place_id'].'';exit();
        // $place->delete();
        Place::deleteAll(['=','id',$params['place_id']]);
        
        
        
        return json_encode($resOut);
    }
    
    public function actionLoadmenulist(){
        
        $params = Yii::$app->request->get();
        
        $resMain = [];
        
        $mMenus = Menu::find()
        ->where(['=','place_id', $params['place_id']])
        ->andWhere(['=','status',''])
        //->andWhere(['=','id',407])
        ->orderBy('rankking')
        ->asArray()
        ->all();
        
        $menuObj = $this->genTreeObj($mMenus);
        
        $arrOut = [];
        foreach($menuObj as $no => $obj){
            $arrOut[] = $obj;
        }
        
        $resMain['nodes'] = $arrOut;
        
        //get max rank
        $sql = "SELECT max(`rankking`) as maxRank FROM `menu` WHERE 1";
        $rMax = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        
        
        $resMain['maxRank'] = $rMax[0]['maxRank'];
        
        return json_encode($resMain);
        
        
    }
    public function actionSeartdatapositionbyemp()
    {
        $getValue = Yii::$app->request->get();
        //$result = ApiPermission::DataMenuByEmp($getValue['data'],$this->arr_formate);
        
        $sql = 'SELECT place.* FROM place INNER JOIN menu on place.id = menu.place_id INNER JOIN menu_manage on menu.id = menu_manage.menu_id WHERE place.status != 99 AND menu_manage.status != 99 AND menu_manage.position_id = '.$getValue['data']['selectPosition'].' group by place.id order by place.id ASC';
        $modelProgram = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        // $modelProgram = Place::find()->where('status != 99')->orderBy('id ASC')->asArray()->all();
        $arrProgram = [];
        foreach($modelProgram as $item){
            
            $arrmenulist = $this->Loadmenulistprogramsearch($item,$getValue['data']['selectPosition']);
            $iTree[] = [
            
            'id' => $item['id'],
            'model'=> $item,
            'ref_id' => '',
            'text' => $item['name'],
            'action' => '',
            'rankking' => $moMenu['rankking'],
            'nodes' => $arrmenulist['nodes']
            
            ];
        }
        
        $resultParent = ApiPermission::DataMenuByEmpParent($getValue['data']);
        $result = ApiPermission::DataMenuByEmpA($getValue['data']);
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $iTree;
    }
    public function Loadmenulistprogramsearch($params,$id_position){
        
        
        $resMain = [];
        
        $mMenus = Menu::find()
        ->join('INNER JOIN','menu_manage','menu.id = menu_manage.menu_id')
        ->where(['=','place_id', $params['id']])
        ->andWhere(['=','menu.status',''])
        ->andWhere(['=','menu_manage.position_id',$id_position])
        ->orderBy('rankking')
        ->asArray()
        ->all();
        $arrh = $this->getHead($mMenus);
        $arraydata = array_merge($mMenus,$arrh);
        $menuObj = $this->genTreeObj($arraydata);
        
        $arrOut = [];
        foreach($menuObj as $no => $obj){
            $arrOut[] = $obj;
        }
        
        $resMain['nodes'] = $arrOut;
        
        //get max rank
        $sql = "SELECT max(`rankking`) as maxRank FROM `menu` WHERE 1";
        $rMax = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        
        
        $resMain['maxRank'] = $rMax[0]['maxRank'];
        
        return $resMain;
        
        
    }
    public function getHead($mMenus)
    {
        $arr = [];
        foreach($mMenus as $item){
            if($item['parent_id'] != ''){
                $data = ApiPermission::menuById($item['parent_id']);
                foreach($data as $i){
                    array_push($arr,$i);
                }
            }else{
                array_push($arr,$item);
            }
        }
        return $arr;
    }
    
    public function createMenuTree($nodeLists){
        
        $html = "";
        foreach($nodeLists as $no => $menus){
            
            //echo $no.'<pre>';print_r($menus);echo '</pre>';
            $html .= '<li>'. $menus['node']['name'];
            
            if(count($menus['node_lists']) > 0){
                $html .= '<ul>';
                $html .= $this->createMenuTree($menus['node_lists']);
                $html .= '</ul>';
            }
            
            $html .= '</li>';
            
        }
        
        return $html;
    }
    
    public function actionArrayhelp(){
        
        $arr = [
        ['id'=>1, 'name'=>'a', 'parent' => ''],
        ['id'=>2, 'name'=>'b', 'parent' => ''],
        ['id'=>3, 'name'=>'c', 'parent' => ''],
        ['id'=>4, 'name'=>'da', 'parent' => '2'],
        ['id'=>5, 'name'=>'e', 'parent' => '2']
        ];
        
        $item = array_filter($arr,function($value){
            echo 'fn<br>';
            echo '<pre>';print_r($value);echo '</pre>';
            
            return ($value['parent'] != '') ? $value:false;
        });
        
        echo '===><pre>';print_r($item);echo '</pre>';
    }
    
    public function genTreeObj($mMenus){
        
        $oTree = [];
        
        
        //keep head
        foreach($mMenus as $no => $moMenu){
            
            if($moMenu['parent_id'] == ''){
                
                $text = $moMenu['name'];
                
                //have parent
                $oTree[] = [
                
                'id' => $moMenu['id'],
                'model'=> $moMenu,
                'ref_id' => '',
                'text' => $text,
                'action' => '',
                'rankking' => $moMenu['rankking']
                
                ];
                
                unset($mMenus[$no]);
                
            }
        }
        
        ArrayHelper::multisort($oTree,'rankking',SORT_ASC);
        
        $total = count($oTree);
        foreach($oTree as $noM => $mMenu){
            
            
            $tags = $this->createTags($mMenu['id'],$mMenu,($noM+1),$total);
            //have parent
            $oTree[$noM]['tags'] =  [$tags];
            
        }
        //echo '<pre>';print_r($oTree);echo '</pre>';exit();
        
        //keep all child
        $oTree = $this->searchDeptArray($oTree, $mMenus);
        //--
        
        //echo '<pre>';print_r($oTree);echo '</pre>';exit();
        
        return $oTree;
    }
    
    
    
    public function searchDeptArray($oTree, $mMenus){
        
        $oTreeOut = $oTree;
        
        // echo 'mMenus:<pre>';print_r($oTree);echo '</pre>';
        
        foreach($oTree as $no => $mTree){
            
            
            $items = array_filter($mMenus,function($value) use ($mTree) {
                
                
                $resFn = false;
                if($mTree['id'] == $value['parent_id']){
                    
                    $resFn = true;
                    
                }
                
                return $resFn;
            });
            
            $passItem = [];
            
            
            foreach($items as $noItem => $item){
                
                $text = $item['name'];
                
                
                $passItem[] = [
                
                'id' => $item['id'],
                'model'=> $item,
                'ref_id' => $mTree['id'] ,
                'text' => $text,
                'action' => '',
                'rankking' => $item['rankking']
                ];
            }
            
            //echo 'items:<pre>';print_r($passItem);echo '</pre>';
            
            if(count($passItem) > 0){
                
                ArrayHelper::multisort($passItem,'rankking',SORT_ASC);
                
                $total = count($passItem);
                foreach($passItem as $pNo => $menuItem){
                    
                    $tags = $this->createTags($menuItem['id'],$menuItem,($pNo+1),$total);
                    //have parent
                    $passItem[$pNo]['tags'] =  [$tags];
                }
                //sub dept
                $passItem = $this->searchDeptArray($passItem, $mMenus);
                if(count($passItem) > 0){
                    $oTreeOut[$no]['nodes'] = $passItem;
                }
                
                
            }else{
                
            }
            
        }
        
        return $oTreeOut;
        
    }
    
    public function createTags($id,$obj,$no,$total){
        
        
        $tags = '<span class="pull-right action-menu">';
        
        $tags .= $this->createOptionRank($obj, $total,$no);
        
        $tags .= '<button type="button" onclick="showModelFormMenu(\'add_child\')" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</button>';
        $tags .= '<button type="button" onclick="showModelFormMenu(\'edit\','.$id.')" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-edit"></span> แก้ไข</button>';
        $tags .= '<button type="button" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-move"></span> ย้าย</button>';
        $tags .= '<button type="button" onclick="delMenuList(\''.$id.'\')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span> ลบ</button>';
        $tags .= '</span>';
        
        
        //return $tags;
        return '';
    }
    
    public function createOptionRank($obj, $total, $sel){
        
        $tags .= 'ลำดับ:<select class="sel-rank" onclick="clickInside(event)" onchange="changeRange(this, event)">';
        for($i = 1;$i <= $total;$i++){
            
            $select = ($sel == $i) ? 'selected="selected"':'';
            $tags .= '<option '.$select.' value="'.$obj['rankking'].'" >'.$i.'</option>';
        }
        $tags .= '</select>';
        return $tags;
    }
    
    public function optionMenuAction($sel){
        
        //MenuProcess
        $html = "";
        $proTypes = MenuProcess::find()->where(['=','status',''])->all();
        
        foreach($proTypes as $no => $proType){
            $selected = ($proType->id == $sel) ? "selected":"";
            $html .= '<option '.$selected.' value="'.$proType->id.'" >'.$proType->name.'</option>';
        }
        
        return $html;
        
    }
    
    
    public function actionManagerequest()
    {
        $placeModels = Place::find()->where(['=','status','2'])->all();
        
        
        
        $optionPlace = $this->getOptionPlace($placeModels);
        
        return $this->render('managerequest',[
        'optionPlace' => $optionPlace
        //'menuObj' => $menuObj
        ]);
    }
    
    public function getOptionPlace($models,$sel  = ''){
        
        $tags = '<option value="">เลือกโปรแกรม</option>';
        foreach($models as $no => $model){
            
            $select = ($sel == $model->id) ? 'selected="selected"':'';
            $tags .= '<option '.$select.' value="'.$model->id.'" >'.$model->name.'</option>';
        }
        
        return $tags;
    }
    
    public function actionSearchempdata(){
        
        $str    = Yii::$app->request->get();
        
        $conf['db_checktime'] = 'ERP_easyhr_checktime';
        $conf['db_ou'] = 'ERP_easyhr_OU';
        
        $empData = [];
        // $empData[] = ['id' => 1,'text' => 'aaa'];
        // $empData[] = ['id' => 2,'text' => 'bbb'];
        
        // $posObjs = Position::find()
        //             ->where(['like','PositionCode',$str['q']])
        //             ->orWhere(['like','Name',$str['q']])
        //             ->all();
        
        $sql = "SELECT e.* ,rp.position_id,p.PositionCode FROM ".$conf['db_checktime'].".emp_data e ";
        $sql .= " INNER JOIN ".$conf['db_ou'].".relation_position rp ON(e.ID_Card = rp.id_card)  ";
        $sql .= " INNER JOIN ".$conf['db_ou'].".position p ON (rp.position_id = p.id)  ";
        $sql .= " WHERE (e.Name like '".$str['q']."%') OR (e.Surname like '".$str['q']."%') OR (e.Code like '".$str['q']."%') ";
        

        $posObjs = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        
        foreach($posObjs as $no => $posObj){
            //echo '<pre>';print_r($posObj);echo '</pre>';
            $empData[] = [
            'id' => $posObj['DataNo'],
            'text' => $posObj['PositionCode'].' '.$posObj['Name'].' '.$posObj['Surname'],
            'name' => $posObj['PositionCode'].' '.$posObj['Name'],
            'code' => $posObj['PositionCode'],
            'id_card' => $posObj['ID_Card'],
            'position_id' => $posObj['position_id']
            
            ];
        }
        
        return json_encode($empData);
        
        
    }
    
    public function actionLoadallmenu(){
        
        $mMenus = Menu::find()
        ->where(['=','status',''])
        //->andWhere(['=','id',407])
        ->orderBy('rankking')
        ->asArray()
        ->all();
        
        $menuObj = $this->genTreeObj($mMenus);
        
        return json_encode($menuObj);
    }
    
    public function actionDelmanagerequest(){
        
        $resOut = array();
        $resOut['status']   = true;
        $resOut['msg']      = "";
        
        
        $params    = Yii::$app->request->post();
        
        MenuManageRequest::deleteAll(['=','id',$params['id']]);
        
        return json_encode($resOut);
        
    }
    
    public function actionLoadmanagerequestlists(){
        
        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = '';
        
        $resOut['head'] = $this->getHeaderMangeRequestTable();
        $resOut['body'] = $this->getDataManageRequestTable([]);
        
        
        return json_encode($resOut);
    }
    public function actionApprovedrequestlists(){
        
        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = '';
        
        $resOut['head'] = $this->getHeaderApprovedrequestTable();
        $resOut['body'] = $this->getDataApprovedrequestTable([]);
        
        // echo "<pre>";
        // print_r($resOut);
        // // echo "</pre>";
        // exit();
        
        return json_encode($resOut);
    }
    public function actionSavemanagerequest(){
        
        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = '';
        
        $str    = Yii::$app->request->post();
        $data = json_decode($str['data'],true);
        
        $data['approve_status'] = 0;
        $data['date_request'] = date('y-m-d h:i:s');
        $data['skill'] = 'r';
        
        
        $mObj     = new MenuManageRequest();
        $getAttr  = $mObj->attributes;
        
        $mObj->attributes = array_merge($getAttr,$data);
        
        if ($mObj->validate()) {
            
            $afterSave = $mObj->save(false);
            
            if (!$afterSave) {
                $resOut['status']   = false;
                $resOut['msg']      .= "error save menu";
            }else{
                $resOut['lastId']   = $mObj->id;
            }
        } else {
            
            $resOut['status']   = false;
            $resOut['msg']      .= $this->createValiMsg($mObj->errors);
            
        }
        
        
        return json_encode($resOut);
        
        
    }
    
    public function getDataManageRequestTable($filter)
    {
        
        $conf['db_checktime'] = 'ERP_easyhr_checktime';
        $conf['db_ou'] = 'ERP_easyhr_OU';
        
        $sql = "SELECT ";
        
        $sql .= "  mmr.id, ";
        $sql .= "  mmr.date_request,";
        $sql .= "  mmr.date_approve,";
        $sql .= "  mmr.approve_status,";
        $sql .= "  mmr.reason_request,";
        $sql .= "  mmr.change_to,";
        $sql .= "  p.name AS place_name,";
        $sql .= "  m.name AS menu_name,";
        $sql .= "  ps.PositionCode as position_code,";
        $sql .= "  ps.Name AS position_name,";
        $sql .= "  e.Name AS emp_name,";
        $sql .= "  e.Surname AS emp_surname,";
        $sql .= " e.ID_Card,";
        $sql .= " mmr.menu_id";
        
        $sql .= " FROM ".$conf['db_checktime'].".menu_manage_request AS mmr ";
        $sql .= "   INNER JOIN ".$conf['db_checktime'].".menu AS m ON (mmr.menu_id = m.id) ";
        $sql .= "   INNER JOIN ".$conf['db_ou'].".position AS ps ON (mmr.position_id = ps.id) ";
        $sql .= "   INNER JOIN ".$conf['db_checktime'].".emp_data AS e ON (mmr.id_card = e.ID_Card) ";
        $sql .= "   INNER JOIN ".$conf['db_checktime'].".place AS p ON (m.place_id = p.id) ";
        $sql .= " WHERE ";
        $sql .= "   mmr.approve_status = 0";
        //$sql .= "   AND mmr.status != 99 ";
        $sql .= " GROUP BY mmr.id, e.Name,e.Surname";
        
        //echo $sql;exit();
        $datas = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        
        
        $resOut = [];
        
        $keepIdcard = [];
        $keppEq = [];
        foreach($datas AS $no =>  $data){
            
            $keepIdcard[($no+1)] = $data['ID_Card'];
            $keppEq[($no+1)] = [
            'ID_Card' =>$data['ID_Card'],
            'menu_id' => $data['menu_id']
            ];
            
            $date1_sp = explode(' ',$data['date_request']);
            $date2_sp = explode(' ',$data['date_approve']);
            
            $stsApproveTxt = "รออนุมัติ";
            switch($data['approve_status']){
                case '1':
                    $stsApproveTxt = "อนุมัติ";
                    break;
                case '2':
                    $stsApproveTxt = "ไม่อนุมัติ";
                    break;
                default:
                    break;
        }
        
        
        $resOut[] = [
        
        'no' => ($no+1),
        'date_request' => DateTime::CalendarDate($date1_sp[0]),
        'date_approve' => DateTime::CalendarDate($date2_sp[0]),
        'place_name' => $data['place_name'],
        'menu_name' => $data['menu_name'],
        'position_code' => $data['position_code'],
        'position_name' => $data['position_name'],
        'emp_name' => $data['emp_name']." ".$data['emp_surname'],
        'privilege_current' => '',
        'privilege_request' =>( ($data['change_to'] == 1)?'กำหนดสิทธิ์':'ไม่กำหนดสิทธิ์' ),
        'status_approve' => $stsApproveTxt,
        'reason_request' => $data['reason_request'],
        'id' =>$data['id']
        ];
        
    }
    
    //update curent priv
    $sql = "SELECT * FROM ".$conf['db_checktime'].".menu_manage WHERE id_card IN ('".implode("','",$keepIdcard)."')  ";
    $currPriv = Yii::$app
    ->dbERP_easyhr_checktime
    ->createCommand($sql)
    ->queryAll();
    
    foreach($resOut as $no => $model)
    {
        
        $txt = '-';
        $chkNow = $keppEq[$model['no']];
        
        foreach($currPriv as $k => $mo){
            
            if( $chkNow['ID_Card'] ==  $mo['id_card'] && $chkNow['menu_id'] ==  $mo['menu_id'] ){
                $txt = 'มีการกำหนดสิทธิ์';
            }
            
        }
        
        $resOut[$no]['privilege_current'] = $txt;
    }
    //--
    return $resOut;
    
}
public function actionSaveapprovedrequest()
{
    $ValuePost    = Yii::$app->request->post('data');
    unset($ValuePost['showManageRequest1_length']);
    unset($ValuePost['showManageRequest2_length']);
    
    foreach ($ValuePost as $key => $item) {
        $mObj = MenuManageRequest::findOne($key);
        if(!is_null($mObj)){
            $mObj->approve_status = $item;
            $menumanege = new MenuManage();
            $cloneObj = clone $mObj; // <== clone object;
            $menumanege->id = null;
            $menumanege->menu_id = "".$cloneObj->menu_id."";
            $menumanege->working_company_id = "".$cloneObj->working_company_id."";
            $menumanege->department_id = "".$cloneObj->department_id."";
            $menumanege->section_id = "".$cloneObj->section_id."";
            $menumanege->position_id = "".$cloneObj->position_id."";
            $menumanege->id_card = "".$cloneObj->id_card."";
            $menumanege->skill = "".$cloneObj->skill."";
            $menumanege->status = '';
            $menumanege->isNewRecord = true;
            $statussave = $mObj->save();
            $statussave1 = $menumanege->save();
            if(($statussave && $statussave1) !== false){
                echo true;
            }else{
                $errors = $mObj->errors;
                $errors1 = $menumanege->errors;
                echo "model can't validater <pre>"; print_r($errors);print_r($errors1); echo "</pre>";
            }
        }
    }
    
}
public function getDataApprovedrequestTable($filter)
{
    $conf['db_checktime'] = 'ERP_easyhr_checktime';
    $conf['db_ou'] = 'ERP_easyhr_OU';
    
    $sql = "SELECT ";
    
    $sql .= "  mmr.id, ";
    $sql .= "  mmr.date_request,";
    $sql .= "  mmr.date_approve,";
    $sql .= "  mmr.approve_status,";
    $sql .= "  mmr.reason_request,";
    $sql .= "  mmr.change_to,";
    $sql .= "  p.name AS place_name,";
    $sql .= "  p.id AS place_id,";
    $sql .= "  m.name AS menu_name,";
    $sql .= "  ps.PositionCode as position_code,";
    $sql .= "  ps.Name AS position_name,";
    $sql .= "  e.Name AS emp_name,";
    $sql .= "  e.Surname AS emp_surname,";
    $sql .= " e.ID_Card,";
    $sql .= " mmr.menu_id";
    
    $sql .= " FROM ".$conf['db_checktime'].".menu_manage_request AS mmr ";
    $sql .= "   INNER JOIN ".$conf['db_checktime'].".menu AS m ON (mmr.menu_id = m.id) ";
    $sql .= "   INNER JOIN ".$conf['db_ou'].".position AS ps ON (mmr.position_id = ps.id) ";
    $sql .= "   INNER JOIN ".$conf['db_checktime'].".emp_data AS e ON (mmr.id_card = e.ID_Card) ";
    $sql .= "   INNER JOIN ".$conf['db_checktime'].".place AS p ON (m.place_id = p.id) ";
    $sql .= " WHERE ";
    $sql .= "   mmr.approve_status = 0";
    //$sql .= "   AND mmr.status != 99 ";
    $sql .= " GROUP BY mmr.id, e.Name,e.Surname";
    
    // echo $sql;exit();
    $datas = Yii::$app
    ->dbERP_easyhr_checktime
    ->createCommand($sql)
    ->queryAll();
    
    
    $resOut = [];
    
    $keepIdcard = [];
    $keppEq = [];
    foreach($datas AS $no =>  $data)
    {
        
        $keepIdcard[($no+1)] = $data['ID_Card'];
        $keppEq[($no+1)] = [
        'ID_Card' =>$data['ID_Card'],
        'menu_id' => $data['menu_id']
        ];
        
        $date1_sp = explode(' ',$data['date_request']);
        $date2_sp = explode(' ',$data['date_approve']);
        
        $stsApproveTxt = "รออนุมัติ";
        switch($data['approve_status'])
        {
            case '1':
                $stsApproveTxt = "อนุมัติ";
                break;
            case '2':
                $stsApproveTxt = "ไม่อนุมัติ";
                break;
            default:
                break;
    }
    
    
    $resOut[] = [
    'no' => ($no+1),
    'date_request' => DateTime::CalendarDate($date1_sp[0]),
    'date_approve' => DateTime::CalendarDate($date2_sp[0]),
    'place_name' => $data['place_name'],
    'place_id' => $data['place_id'],
    'menu_name' => $data['menu_name'],
    'position_code' => $data['position_code'],
    'position_name' => $data['position_name'],
    'emp_name' => $data['emp_name']." ".$data['emp_surname'],
    'privilege_current' => '',
    'privilege_request' =>( ($data['change_to'] == 1)?'กำหนดสิทธิ์':'ไม่กำหนดสิทธิ์' ),
    'status_approve' => $stsApproveTxt,
    'reason_request' => $data['reason_request'],
    'id' =>$data['id']
    ];
    
}

//update curent priv
$sql = "SELECT * FROM ".$conf['db_checktime'].".menu_manage WHERE id_card IN ('".implode("','",$keepIdcard)."')  ";
$currPriv = Yii::$app
->dbERP_easyhr_checktime
->createCommand($sql)
->queryAll();

$sql_place = 'SELECT * FROM `place`';
$place = Yii::$app
->dbERP_easyhr_checktime
->createCommand($sql_place)
->queryAll();
foreach($resOut as $no => $model){
    
    $txt = '-';
    $chkNow = $keppEq[$model['no']];
    
    foreach($currPriv as $k => $mo){
        
        if( $chkNow['ID_Card'] ==  $mo['id_card'] && $chkNow['menu_id'] ==  $mo['menu_id'] ){
            $txt = 'มีการกำหนดสิทธิ์';
        }
        
    }
    
    $resOut[$no]['privilege_current'] = $txt;
}
//--
$arr_res = [];
foreach($resOut as $item){
    $arr_res[$item['place_id']][] = $item;
    // $arr_res[$item['place_id']]['place_name'] = $item['place_name'];
}
return $arr_res;

}
public function getHeaderMangeRequestTable(){
    
    $outArr = [];
    $outArr[] = ['data' => 'no', 'title' => 'ลำดับ'];
    $outArr[] = ['data' => 'date_request', 'title' => 'วันที่สร้างคำขอ'];
    $outArr[] = ['data' => 'date_approve', 'title' => 'วันที่อนุมัติ'];
    $outArr[] = ['data' => 'place_name', 'title' => 'โปรแกรม'];
    $outArr[] = ['data' => 'menu_name', 'title' => 'เมนู'];
    $outArr[] = ['data' => 'position_code', 'title' => 'รหัสตำแหน่ง'];
    $outArr[] = ['data' => 'position_name', 'title' => 'ชื่อตำแหน่ง'];
    $outArr[] = ['data' => 'emp_name', 'title' => 'ชื่อพนักงาน'];
    $outArr[] = ['data' => 'privilege_current', 'title' => 'ข้อมูลรายงานสิทธิ์การใช้โปรแกรม'];
    $outArr[] = ['data' => 'privilege_request', 'title' => 'การเปลี่ยนแปลง'];
    $outArr[] = ['data' => 'status_approve', 'title' => 'สถานะ'];
    $outArr[] = ['data' => 'reason_request', 'title' => 'เหตุผล'];
    $outArr[] = ['data' => '', 'title' => 'จัดการ'];
    
    return $outArr;
    
}
public function getHeaderApprovedrequestTable(){
    
    $outArr = [];
    $outArr[] = ['data' => 'no', 'title' => 'ลำดับ'];
    $outArr[] = ['data' => 'date_request', 'title' => 'วันที่สร้างคำขอ'];
    $outArr[] = ['data' => 'date_approve', 'title' => 'วันที่อนุมัติ'];
    $outArr[] = ['data' => 'place_name', 'title' => 'โปรแกรม'];
    $outArr[] = ['data' => 'menu_name', 'title' => 'เมนู'];
    $outArr[] = ['data' => 'position_code', 'title' => 'รหัสตำแหน่ง'];
    $outArr[] = ['data' => 'position_name', 'title' => 'ชื่อตำแหน่ง'];
    $outArr[] = ['data' => 'emp_name', 'title' => 'ชื่อพนักงาน'];
    $outArr[] = ['data' => 'privilege_current', 'title' => 'ข้อมูลรายงานสิทธิ์การใช้โปรแกรม'];
    $outArr[] = ['data' => 'privilege_request', 'title' => 'การเปลี่ยนแปลง'];
    $outArr[] = ['data' => 'status_approve', 'title' => 'สถานะ'];
    $outArr[] = ['data' => 'reason_request', 'title' => 'เหตุผล'];
    $outArr[] = ['data' => '', 'title' => 'จัดการ'];
    
    return $outArr;
    
}

public function actionApprovedrequest()
{
    $placeModels = Place::find()->where(['=','status','2'])->all();
    $optionPlace = $this->getOptionPlace($placeModels);
    
    return $this->render('approvedrequest',[
    'optionPlace' => $optionPlace
    ]);
}

public function actionReportprogram()
{
    return $this->render('reportprogram');
}

public function actionUploadpermission()
{
    return $this->render('uploadpermission');
}
public function actionUploadfileexcal()
{
    $arrData = [];
    $ValuePost    = Yii::$app->request->post();
    $fileName = $_FILES['image_file']['name'];
    $fileType = $_FILES['image_file']['type'];
    $url = dirname(dirname(dirname(dirname(__FILE__))));
    $sDir = $url.'/upload/';
    if( move_uploaded_file($_FILES["image_file"]["tmp_name"], $sDir.$fileName) ){
        $objPHPExcel = \PHPExcel_IOFactory::load($sDir.$fileName);
        $highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestDataColumn();
        $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
        $sheetData = $objPHPExcel->getActiveSheet()->rangeToArray("A1:".$highestColumm.$highestRow);
        foreach ($objPHPExcel->setActiveSheetIndex(0)->getRowIterator() as $keyH => $row) {
            if (!is_null($row)){
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                foreach ($cellIterator as $key => $cell) {
                    if (!is_null($cell)) {
                        $value = $cell->getCalculatedValue();
                        $arrData[$keyH][$key] =$value;
                    }
                }
            }
        }
    }else{
        
        $res['status']  = false;
        $res['msg']     = "copy file error.";
    }
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return $arrData;
}
public function actionMunubyid()
{
    $idProgram    = Yii::$app->request->get('id');
    $menu = ApiPermission::getMenuHead($idProgram);
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return $menu;
}
public function actionSubmenu($value='')
{
    $idmenu    = Yii::$app->request->get('id');
    $submenu = ApiPermission::getSubmenu($idmenu);
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return $submenu;
}
public function actionManagepermission()
{
    return $this->render('reportpermissionexcel');
}
public function actionReportpermissionexcelfile()
{
    // $postValue = Yii::$app->request->get();
    // $obj = json_decode($postValue['data'],true);
    // $arrcompany = ApiHr::getWorking_companyById($obj['selectworking']);
    // $arrdepartment = ApiHr::getDepartment($obj['selectdepartment']);
    // $arrsaction = ApiHr::getSectionInId($obj['selectsection']);
    // $arrProgram = ApiPermission::getPlace($obj['programname']);
    // $arrMenu = ApiPermission::getMenu($obj['programname']);
    $data['programID']='30';
    $data['companyID']='56';
    $data['departmentID']='';//71
    $data['sectionID']='';//['191','192'];
    $arraydata= [];
    $cellelsx = ['ผู้จัดการอาวุโส','ผู้จัดการ','พนักงาน'];
    $programresult = Place::find()->where(['=','id',$data['programID']])->asArray()->one();
    $menuresult_base = Menu::find()->where(['=','place_id',$data['programID']])->orderBy('id')->asArray()->all();
    $menuresult = [];
    foreach ($menuresult_base as $key => $value) {
        $menuresult[$value['id']]=$value;
    }
    $where = $whereposition = '';
    if($data['companyID']!=''){
        $where = 'working_company_id = '.$data['companyID'];
        $join = 'position.WorkCompany = menu_manage.working_company_id';
        $whereposition =  '';
    }
    if($data['departmentID']!=''){
        $where = 'department_id = '.$data['departmentID'];
        $join = 'position.Department = menu_manage.department_id';
    }
    if($data['sectionID']!=''){
        $where = 'section_id in ('.JOIN(',',$data['sectionID']).')';
        $join = 'position.Section = menu_manage.section_id';
    }
    $OU =  DBConnect::getDBConn()['ou'];
    $menumanege=[];
    $connection = \Yii::$app->dbERP_easyhr_checktime;
    $command = $connection->createCommand("
    SELECT menu_manage.*,position.*
    FROM menu_manage INNER JOIN ".$OU.".position ON ".$join."
    WHERE ".$where."
    GROUP BY menu_manage.menu_id,position.name
    ORDER BY menu_manage.menu_id,position.id");
    $result = $command->queryAll();
    $Department = Department::find()->where(['=','company',$data['companyID']])->asArray()->all();
    $department = [];
    foreach ($Department as $value) {
        $department[]=$value['id'];
    }
    $Section_base = Section::find()->where(['in','department',$department])->asArray()->all();
    $section = [];
    foreach ($Section_base as $value) {
        $section[$value['id']] = $value;
    }
    foreach ($result as $key => $value) {
        $menumanege[$value['menu_id']][$value['Section']][]=$value;
        // $menumanege[$value['menu_id']][$value['Section']]['sectionName']=$section[$value['Section']]['name'];
    }
    $objPHPExcel = new \PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("PHPExcel Test Document")
    ->setSubject("PHPExcel Test Document")
    ->setDescription("Test document for PHPExcel, generated using PHP classes.")
        ->setKeywords("office PHPExcel php")
    ->setCategory("Test result file");
    $countrow = 3;
    
    $col = $countcol;
    $countVL = 0;
    $objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('A2:A3')
    ->mergeCells('B2:B3');
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'โปรแกรม')
    ->setCellValue('B2', 'ชื่อเมนู');
    foreach ($menuresult as $key => $value) {
        $countcol = 3;
        $data = $menumanege[$key];
        if(is_array($data)){
            $idx = 0;
            foreach ($data as $k => $item) {
                
                $col = ($idx==0) ?  3 : $countcol;
                $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells(\PHPExcel_Cell::stringFromColumnIndex($col).'3:'.\PHPExcel_Cell::stringFromColumnIndex($col+count($item)).'3');
                $sec = $section[$k]['name'];
                // echo $sec.'<br>';
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue(\PHPExcel_Cell::stringFromColumnIndex($col).'3', $k);
                foreach ($item as $keys => $items) {
                    // echo '->'.$items['Name'].'<br>';
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(\PHPExcel_Cell::stringFromColumnIndex($countcol).'4', $items['Name']);
                    $countcol++;
                }
                $idx++;
                unset($col);
            }
        }
        $countcol = 3;
        $active = '';
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$countrow, $programresult['name'])
        ->setCellValue('B'.$countrow, $value['name']);
        if(is_array($data)){
            if($data != null){
                $active='X';
            }
            foreach ($data as $k => $item) {
                foreach ($item as $keys => $items) {
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue(\PHPExcel_Cell::stringFromColumnIndex($countcol).$countrow, $active);
                $countcol++;
                }
            }
        }
        $countrow++;
    }
    // Set document properties
    echo date('H:i:s') , " Set document properties" , EOL;
    
    // Add some data
    echo date('H:i:s') , " Add some data" , EOL;
    
    // foreach ($data as $key => $value) {
    //     # code...
    //     $keyEnd = $key+3;
    //     $key = $key+2;
    //     $objPHPExcel->setActiveSheetIndex(0)
    //     ->mergeCells(\PHPExcel_Cell::stringFromColumnIndex($key).'2:'.\PHPExcel_Cell::stringFromColumnIndex($keyEnd).'2');
    //     $objPHPExcel->setActiveSheetIndex(0)
    //     ->setCellValue(\PHPExcel_Cell::stringFromColumnIndex($key).'2', 'ทรัพยากรมนุษย์');
    //     for ($i=0; $i < count($cellelsx) ; $i++) {
    //         $objPHPExcel->setActiveSheetIndex(0)
    //         ->setCellValue(\PHPExcel_Cell::stringFromColumnIndex($key+$i).'3', $cellelsx[$i]);
    //     }
    //     $key = $key-1;
    //     foreach ($value[0] as $k => $item) {
    //         # code...
    //         $keys = $key+$k+3;
    //         $objPHPExcel->setActiveSheetIndex(0)
    //         ->setCellValue('A'.$keys,$value[1]);
    //         echo $item.'<br>';
    //         $objPHPExcel->setActiveSheetIndex(0)
    //         ->setCellValue('B'.$keys,$item);
    //     }
    // }
    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Simple');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Save Excel 2007 file
    $callStartTime = microtime(true);
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
}
public function actionReportpermissionexcelfilebyemp()
{
    $data['programID']='30';
    $data['idcrad']='1579900499011';
    $data['positionId']='1027';
    $programresult = Place::find()->where(['=','id',$data['programID']])->asArray()->one();
    $menuresult = Menu::find()->where(['=','place_id',$data['programID']])->asArray()->all();
    $menumanege = MenuManage::find()->where(['=','position_id',$data['positionId']])->asArray()->all();
    
    $positionrelation = Position::find()
    ->select('position.*,relation_position.*')
    ->join('INNER JOIN', 'relation_position','position.id =relation_position.position_id ')
    ->where(['=','position.id',$data['positionId']])
    ->andwhere(['=','relation_position.id_card',$data['idcrad']])
    ->asArray()->one();
    $menumanegeresult= [];
    $arrposition = [];
    foreach ($menumanege as $key => $value) {
        $menumanegeresult[$value['menu_id']] = $value;
    }
    $count =3;
    foreach ($menuresult as $value) {
        $arrposition[$count]['program'] = $programresult['name'];
        $arrposition[$count]['menus']= $value['name'];
        if($menumanegeresult[$value['id']]!=null){
            $arrposition[$count]['active'] = 'O';
        }else {
            $arrposition[$count]['active'] = 'X';
        }
        $count++;
    }
    $objPHPExcel = new \PHPExcel();
    // Set document properties
    echo date('H:i:s') , " Set document properties" , EOL;
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("PHPExcel Test Document")
    ->setSubject("PHPExcel Test Document")
    ->setDescription("Test document for PHPExcel, generated using PHP classes.")
        ->setKeywords("office PHPExcel php")
    ->setCategory("Test result file");
    $C_name = $positionrelation['PositionCode'].' '.$positionrelation['Name'].' '.$positionrelation['NameUse'];
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'โปรแกรม')
    ->setCellValue('B2', 'ชื่อเมนู')
    ->setCellValue('C2', $C_name);
    foreach ($arrposition as $key => $value) {
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$key, $value['program'])
        ->setCellValue('B'.$key, $value['menus'])
        ->setCellValue('c'.$key, $value['active']);
    }
    // Rename worksheet
    foreach($objPHPExcel->getActiveSheet()->getColumnDimension() as $col) {
        $col->setAutoSize(true);
    }
    $objPHPExcel->getActiveSheet()->setTitle('Simple');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Save Excel 2007 file
    $callStartTime = microtime(true);
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save(str_replace('.php', 'emp.xlsx',__FILE__));
}
public function actionSearchpermission()
{
    
    return $this->render('searchpermission');
}

public function actionHistorylog()
{
    return $this->render('historylog');
}

function createDataUpdate($model,$data){
    
    foreach($data as $k => $v){
        $model[$k] = $v;
    }
    
    return $model;
}

public function createValiMsg($ar){
    $txt = "";
    foreach($ar as $f => $msgArrs){
        
        foreach($msgArrs as $no => $msg){
            $txt .= "- ".$msg."<br>";
        }
    }
    
    return $txt;
}

}