<?php

namespace app\modules\vhc\models\VHC;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_list_option".
 *
 * @property integer $id
 * @property integer $list_id
 * @property string $option_name
 * @property integer $option_value
 * @property string $comment
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcListOption extends MasterModel
{
    /**
     * @inheritdoc
     */
    
    protected $_pageSize;
    public static function tableName()
    {
        return 'vhc_list_option';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'option_name', 'option_value', 'record_status'], 'required'],
            [['list_id', 'option_value', 'record_status'], 'integer'],
            [['comment'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['option_name'], 'string', 'max' => 50],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'list_id' => 'List ID',
            'option_name' => 'Option Name',
            'option_value' => 'Option Value',
            'comment' => 'Comment',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
    public function searcha($params)
    {

        $query = VhcListOption::find()->where(['!=','record_status','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'group_name', $this->option_name],
                               ['like', 'option_value', $this->option_value]
                                );

        return $dataProvider;
    }

    public function search($params)
    {
        $data = VhcListOption::find() ->where('record_status <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','group_id',$this->list_id]); //รหัสบริษัท
        $data->andFilterWhere(['like','option_name',$this->option_name]);
        $data->andFilterWhere(['like','option_value',$this->option_value]);//	ชื่อเต็มบริษัท
        $data->andFilterWhere(['like','comment',$this->comment]);
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
