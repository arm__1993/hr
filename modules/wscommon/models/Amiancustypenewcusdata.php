<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "AMIAN_CUS_TYPE".
 *
 * @property integer $CUS_TYPE_ID
 * @property integer $CUS_TYPE_BU
 * @property string $CUS_TYPE_BU_DESC
 * @property integer $CUS_TYPE_KIND
 * @property string $CUS_TYPE_KIND_DESC
 * @property string $CUS_TYPE_LEVEL
 * @property string $CUS_TYPE_LEVEL_DESC
 * @property integer $CUS_TYPE_STATUS
 */
class Amiancustypenewcusdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AMIAN_CUS_TYPE';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_new_cusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CUS_TYPE_BU', 'CUS_TYPE_BU_DESC', 'CUS_TYPE_KIND', 'CUS_TYPE_KIND_DESC', 'CUS_TYPE_LEVEL', 'CUS_TYPE_LEVEL_DESC', 'CUS_TYPE_STATUS'], 'required'],
            [['CUS_TYPE_BU', 'CUS_TYPE_KIND', 'CUS_TYPE_STATUS'], 'integer'],
            [['CUS_TYPE_BU_DESC'], 'string', 'max' => 150],
            [['CUS_TYPE_KIND_DESC', 'CUS_TYPE_LEVEL_DESC'], 'string', 'max' => 255],
            [['CUS_TYPE_LEVEL'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CUS_TYPE_ID' => 'auto run',
            'CUS_TYPE_BU' => 'business unit',
            'CUS_TYPE_BU_DESC' => 'business unit desc',
            'CUS_TYPE_KIND' => 'ชนิด',
            'CUS_TYPE_KIND_DESC' => 'Cus  Type  Kind  Desc',
            'CUS_TYPE_LEVEL' => 'ระดับ',
            'CUS_TYPE_LEVEL_DESC' => 'Cus  Type  Level  Desc',
            'CUS_TYPE_STATUS' => 'Cus  Type  Status',
        ];
    }


    public function searchtypecusdatabyidcustomer($cusno)
    {

        $sql = "select AMIAN_CUS_TYPE.* from AMIAN_CUS_TYPE  INNER join MIAN_CUS_TYPE_REF on AMIAN_CUS_TYPE.CUS_TYPE_ID = MIAN_CUS_TYPE_REF.CTYPE_GRADE_REF Group by CTYPE_REF_CUSNO HAVING CTYPE_REF_CUSNO = '$cusno'";
        //echo ;
        $custype = Yii::$app->dbERP_new_cusdata
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
        return $custype;



    }
}
