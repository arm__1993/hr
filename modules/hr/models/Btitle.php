<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "b_title".
 *
 * @property integer $id
 * @property string $title_name_th
 * @property string $title_name_en
 * @property integer $status_active
 * @property integer $gender
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updatteby_user
 * @property string $update_datetime
 */
class Btitle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b_title';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['title_name_th', 'title_name_en', 'status_active', 'gender', 'createby_user', 'create_datetime', 'updatteby_user', 'update_datetime'], 'required'],
            [['status_active', 'gender'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['title_name_th', 'title_name_en', 'createby_user', 'updatteby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_name_th' => 'Title Name Th',
            'title_name_en' => 'Title Name En',
            'status_active' => 'Status Active',
            'gender' => 'Gender',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updatteby_user' => 'Updatteby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

     public function search($params){
 
        $model = Btitle::find();
        $dataProvider = new ActiveDataProvider(
                [
                    'query'=>$model,
                    'pagination' => ['pageSize' => 10,],
                ]);
        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $model->andFilterWhere(['like','title_name_th',$this->title_name_th]);
        $model->andFilterWhere(['like','title_name_en',$this->title_name_en]);
        $model->andFilterWhere(['not like','status_active','99']);
         return $dataProvider;
    }


}
