<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tax_reduce_other".
 *
 * @property integer $id
 * @property string $reduce_name
 * @property string $reduce_amount
 * @property string $reduce_amount_max
 * @property integer $for_year
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class TaxReduceOther extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_reduce_other';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reduce_name', 'reduce_amount'], 'required'],
            [['reduce_amount', 'reduce_amount_max'], 'number'],
            [['for_year', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['reduce_name'], 'string', 'max' => 200],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reduce_name' => 'Reduce Name',
            'reduce_amount' => 'Reduce Amount',
            'reduce_amount_max' => 'Reduce Amount Max',
            'for_year' => 'For Year',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }
}
