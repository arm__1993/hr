<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Workingcompany;
use yii\helpers\ArrayHelper;



class WorkingcompanyController extends ActiveController
{

   	public $modelClass = 'app\modules\wscommon\models\Workingcompany';

	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	public function actionListcompanyformobile()
    {
        //return 1;
     	$status = 1;
        $listcompany = new Workingcompany();
        $dataCompany = $listcompany->listcompanyformobile($status);
        return $dataCompany;
    }

	
}
