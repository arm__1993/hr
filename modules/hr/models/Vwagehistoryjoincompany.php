<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "V_WAGE_HISTORY_JOIN_COMPANY".
 *
 * @property integer $WAGE_ID
 * @property string $WAGE_EMP_ID
 * @property string $WAGE_PAY_DATE
 * @property string $WAGE_POSITION_CODE
 * @property string $WAGE_POSITION_NAME
 * @property string $WAGE_SECTION_ID
 * @property string $WAGE_DEPARTMENT_ID
 * @property string $WAGE_WORKING_COMPANY
 * @property string $WAGE_BANK_NAME
 * @property string $WAGE_ACCOUNT_NUMBER
 * @property string $WAGE_GET_MONEY_TYPE
 * @property string $WAGE_SALARY_CHART
 * @property string $WAGE_SALARY_LEVEL
 * @property integer $WAGE_SALARY_STEP
 * @property string $WAGE_SALARY
 * @property string $WAGE_SALARY_BY_CHART
 * @property string $WAGE_TOTAL_ADDITION
 * @property string $WAGE_EARN_PLUS_ADD
 * @property string $WAGE_TOTAL_DEDUCTION
 * @property string $WAGE_EARN_MINUS_DEDUCT
 * @property string $WAGE_NET_SALARY
 * @property string $WAGE_THIS_MONTH_CONFIRM
 * @property string $namecompanycode
 * @property string $companyname
 * @property string $departmentcode
 * @property string $departmentname
 * @property string $sectioncode
 * @property string $sectionname
 */
class Vwagehistoryjoincompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'V_WAGE_HISTORY_JOIN_COMPANY';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WAGE_ID', 'WAGE_SALARY_STEP'], 'integer'],
            [['WAGE_EMP_ID', 'WAGE_PAY_DATE', 'WAGE_POSITION_CODE', 'WAGE_POSITION_NAME', 'WAGE_SECTION_ID', 'WAGE_DEPARTMENT_ID', 'WAGE_WORKING_COMPANY', 'WAGE_BANK_NAME', 'WAGE_ACCOUNT_NUMBER', 'WAGE_GET_MONEY_TYPE', 'WAGE_SALARY_CHART', 'WAGE_SALARY_LEVEL', 'WAGE_SALARY_STEP', 'WAGE_SALARY', 'WAGE_SALARY_BY_CHART', 'WAGE_TOTAL_ADDITION', 'WAGE_EARN_PLUS_ADD', 'WAGE_TOTAL_DEDUCTION', 'WAGE_EARN_MINUS_DEDUCT', 'WAGE_NET_SALARY', 'WAGE_THIS_MONTH_CONFIRM', 'namecompanycode', 'companyname', 'departmentcode', 'departmentname', 'sectioncode', 'sectionname'], 'required'],
            [['WAGE_SALARY', 'WAGE_SALARY_BY_CHART', 'WAGE_TOTAL_ADDITION', 'WAGE_EARN_PLUS_ADD', 'WAGE_TOTAL_DEDUCTION', 'WAGE_EARN_MINUS_DEDUCT', 'WAGE_NET_SALARY'], 'number'],
            [['WAGE_EMP_ID'], 'string', 'max' => 20],
            [['WAGE_PAY_DATE', 'WAGE_POSITION_CODE', 'WAGE_SECTION_ID', 'WAGE_DEPARTMENT_ID', 'WAGE_WORKING_COMPANY'], 'string', 'max' => 100],
            [['WAGE_POSITION_NAME', 'WAGE_SALARY_CHART', 'WAGE_SALARY_LEVEL'], 'string', 'max' => 200],
            [['WAGE_BANK_NAME', 'WAGE_ACCOUNT_NUMBER', 'companyname', 'departmentname', 'sectionname'], 'string', 'max' => 250],
            [['WAGE_GET_MONEY_TYPE'], 'string', 'max' => 1],
            [['WAGE_THIS_MONTH_CONFIRM', 'namecompanycode', 'departmentcode'], 'string', 'max' => 2],
            [['sectioncode'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'WAGE_ID' => 'Wage  ID',
            'WAGE_EMP_ID' => 'Wage  Emp  ID',
            'WAGE_PAY_DATE' => 'Wage  Pay  Date',
            'WAGE_POSITION_CODE' => 'Wage  Position  Code',
            'WAGE_POSITION_NAME' => 'Wage  Position  Name',
            'WAGE_SECTION_ID' => 'Wage  Section  ID',
            'WAGE_DEPARTMENT_ID' => 'Wage  Department  ID',
            'WAGE_WORKING_COMPANY' => 'Wage  Working  Company',
            'WAGE_BANK_NAME' => 'Wage  Bank  Name',
            'WAGE_ACCOUNT_NUMBER' => 'Wage  Account  Number',
            'WAGE_GET_MONEY_TYPE' => 'Wage  Get  Money  Type',
            'WAGE_SALARY_CHART' => 'Wage  Salary  Chart',
            'WAGE_SALARY_LEVEL' => 'Wage  Salary  Level',
            'WAGE_SALARY_STEP' => 'Wage  Salary  Step',
            'WAGE_SALARY' => 'Wage  Salary',
            'WAGE_SALARY_BY_CHART' => 'Wage  Salary  By  Chart',
            'WAGE_TOTAL_ADDITION' => 'Wage  Total  Addition',
            'WAGE_EARN_PLUS_ADD' => 'Wage  Earn  Plus  Add',
            'WAGE_TOTAL_DEDUCTION' => 'Wage  Total  Deduction',
            'WAGE_EARN_MINUS_DEDUCT' => 'Wage  Earn  Minus  Deduct',
            'WAGE_NET_SALARY' => 'Wage  Net  Salary',
            'WAGE_THIS_MONTH_CONFIRM' => 'Wage  This  Month  Confirm',
            'namecompanycode' => 'Namecompanycode',
            'companyname' => 'Companyname',
            'departmentcode' => 'Departmentcode',
            'departmentname' => 'Departmentname',
            'sectioncode' => 'Sectioncode',
            'sectionname' => 'Sectionname',
        ];
    }
}
