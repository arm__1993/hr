<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 17:16
 */


namespace app\modules\webreport\apiwebreport;

use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiBacklogscar
{

    public static function getTotalUserName($type_company,$technician_car)
    {
        //$sql_typeCar แยกชนิดรถ
        $sql_typeCar = "SELECT cvhc_main.id, cvhc_main.guard_time, ahistory.register, ahistory.fullmodel_name, 
                erepair_send_all_mechanic.technician_name, cvhc_main.app_time FROM cvhc_main 
                INNER JOIN ahistory ON ahistory.id = cvhc_main.ahistory_id INNER JOIN erepair_send_all_mechanic 
                ON erepair_send_all_mechanic.cvhc_main_id = cvhc_main.id WHERE ahistory.status_new <>99 
                and cvhc_main.status !=25 and cvhc_main.status !=26 and cvhc_main.status !=27 and cvhc_main.status !=99 
                and cvhc_main.brach_id ='$type_company' and ahistory.agroup_id in ($technician_car)";

        $fullmodal = Yii::$app->dbERP_service->createCommand($sql_typeCar)->queryAll();

        $totalName =[];
        foreach ($fullmodal as $value) {
            $totalName[$value['id']]['guard_time'] = $value['guard_time'];
            $totalName[$value['id']]['register'] = $value['register'];
            $totalName[$value['id']]['fullmodel_name'] = $value['fullmodel_name'];
            $totalName[$value['id']]['technician_name'] = $value['technician_name'];
            $totalName[$value['id']]['app_time'] = $value['app_time'];
        }

        return $totalName;
    }

    public static function getTotalTime($type_company)
    {
        $sql_name = "SELECT cvhc_main.id ,ERP_easyhr_OU.relation_position.NameUse,drepair_inform.end_time 
                FROM cvhc_main inner join drepair_inform on drepair_inform.cvhc_main_id = cvhc_main.id 
                inner join ERP_easyhr_OU.relation_position on ERP_easyhr_OU.relation_position.id_card = drepair_inform.sa_id 
                and cvhc_main.status <> 25 and cvhc_main.status <> 26 
                and cvhc_main.status <> 27 and cvhc_main.status <> 99 
                and cvhc_main.brach_id = '$type_company' group by cvhc_main.id";

        $nameUser = Yii::$app->dbERP_service->createCommand($sql_name)->queryAll();
        $totalTime = [];
        foreach ($nameUser as $value) {
            $totalTime[$value['id']]['name'] = $value['NameUse'];
            $totalTime[$value['id']]['end_time'] = date("Y-m-d", $value['end_time']);
        }

        return $totalTime;
    }

    public static function getStatusOne($type_company,$technician_car)
    {
        //$sql_StatusOne กรองสถานะที่ 1
        $sql_Status_One = "SELECT cvhc_main.id,cvhc_main.status FROM cvhc_main 
                inner join ahistory on ahistory.id = cvhc_main.ahistory_id 
                where cvhc_main.status in (0,1,2,22,4,21) and cvhc_main.brach_id = $type_company 
                and ahistory.agroup_id in ($technician_car)";
        $model = Yii::$app->dbERP_service->createCommand($sql_Status_One)->queryAll();
        $statusOne = [];
        foreach ($model as $value) {
            $statusOne[$value['id']]['status'] = $value['status'];
        }

        return $statusOne;
    }

    public static function getDetail()
    {
        // $sql_StatusP_Cond กรณีเข้าก้อน2 -> status6 -> P_code
        $sql_StatusP_Cond = "SELECT id ,detail FROM erepair_p_code";
        $model = Yii::$app->dbERP_service->createCommand($sql_StatusP_Cond)->queryAll();
        $arrDetail = [];
        foreach ($model as $value) {
            $arrDetail[$value['id']]['detail'] = $value['detail'];
        }

        return $arrDetail;
    }

    public static function getStatusThree($type_company,$technician_car)
    {
        $sql_Status_three = "SELECT cvhc_main.id,cvhc_main.status FROM cvhc_main 
                inner join ahistory on ahistory.id = cvhc_main.ahistory_id 
                where cvhc_main.status in (16,17,18,19,20,97) 
                and cvhc_main.brach_id = '$type_company' and ahistory.agroup_id in ($technician_car)";
        $Statis_three = Yii::$app->dbERP_service->createCommand($sql_Status_three)->queryAll();
        $statusThree = [];
        foreach ($Statis_three as $value) {
            $statusThree[$value['id']]['status'] = $value['status'];
        }

        return $statusThree;
    }

    public static function getStatusTwo($type_company,$technician_car)
    {
        $sql_Status_Two = "SELECT cvhc_main.id,cvhc_main.status ,erepair_send.status_repair,
                    erepair_send.erepair_p_code_id FROM cvhc_main inner join ahistory 
                    on ahistory.id = cvhc_main.ahistory_id inner join erepair_send 
                    on erepair_send.cvhc_main_id=cvhc_main.id inner join erepair_p_code 
                    on erepair_p_code.id=erepair_send.erepair_p_code_id where cvhc_main.status 
                    in (5,6,7,8,9,10,11,12,13,14,24,23) and cvhc_main.brach_id = '$type_company' 
                    and ahistory.agroup_id in ($technician_car)";
        $model = Yii::$app->dbERP_service->createCommand($sql_Status_Two)->queryAll();
        $statusTwo = [];
        foreach ($model as $value) {
            $statusTwo[$value['id']]['status'] = $value['status'];
            $statusTwo[$value['id']]['status_repair'] = $value['status_repair'];
            $statusTwo[$value['id']]['erepair_p_code_id'] = $value['erepair_p_code_id'];
        }

        return $statusTwo;
    }

}