<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:47
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/bootstrap-filestyle.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/permission/upload.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/upload.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดการสิทธิ์</a>
                </li>
                <li class="active">UP File กำหนดสิิทธิ์การใช้โปรแกรม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="container">
                <div class="upload_form_cont">
                    <form id="upload_form" class='aling-tixt-center'>
                        <div class='row'>
                            <div class='col-md-3'></div>
                            <div class='col-md-5 form-group'>
                                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                <input type="file" class="filestyle"  name="image_file" id="image_file" data-buttonText="Find file">
                            </div>
                            <div class='col-md-3'></div>                            
                        </div>
                        
                        <div class='row'>
                             <div class='col-md-5'></div>
                            <div class='col-md-2 form-group'>
                                <input type="button" value="Upload" onclick="startUploading()" />
                            </div>
                            <div class='col-md-5'></div>   
                        </div>
                        <div id="fileinfo">
                            <div id="filename"></div>
                            <div id="filesize"></div>
                            <div id="filetype"></div>
                            <div id="filedim"></div>
                        </div>
                        <div id="error">You should select valid image files only!</div>
                        <div id="error2">An error occurred while uploading the file</div>
                        <div id="abort">The upload has been canceled by the user or the browser dropped the connection</div>
                        <div id="warnsize">Your file is very big. We can't accept it. Please select more small file</div>

                        <div id="progress_info" class='row'>
                            <div id="progress" calss='col-md-12' ></div>
                            <div id="progress_percent">&nbsp;</div>
                            <div class="clear_both"></div>
                            <div>
                                <div id="speed">&nbsp;</div>
                                <div id="remaining">&nbsp;</div>
                                <div id="b_transfered">&nbsp;</div>
                                <div class="clear_both"></div>
                            </div>
                            <div id="upload_response1" class='col-md-12' style="overflow: scroll;msgin-right:100%"></div>
                        </div>
                    </form>

                    <img id="preview" />
                </div>
            </div>
        </div>
    </div>
    <div class="container">
    <!-- /.box -->
</section><!-- /.content -->

    