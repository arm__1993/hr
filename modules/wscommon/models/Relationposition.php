<?php

namespace app\modules\wscommon\models;

use Yii;
 
class Relationposition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relation_position';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'id_card', 'NameUse', 'Note'], 'required'],
            [['position_id', 'status'], 'integer'],
            [['Note'], 'string'],
            [['relation_position_Start_date'], 'safe'],
            [['id_card'], 'string', 'max' => 14],
            [['NameUse'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_id' => 'id ตาราง position',
            'id_card' => 'รหัสบัตรประชาชนพนักงาน',
            'NameUse' => 'Name Use',
            'Note' => 'Note',
            'status' => '99 ไม่ใช้ 1 ใช้',
            'relation_position_Start_date' => 'วันที่เริ่มใช้ข้อมูล',
        ];
    }

    public function searchreationpostion($id_card_em) // param
    {
        //print_r($id_card_em) ;
        $seach_id =  $id_card_em['ID_Card'];

    // foreach ($id_card_em as $value) {
    //             $checkarray['ID_Card'] = $value;
    //     }
    
    //return  $seach_id;

        $sql = "select position_id from relation_position  where id_card = :seach_id AND status = 1 ";
        //return $sql;

        $show_postion_id = Yii::$app->dbERP_easyhr_OU
       ->createCommand($sql)
       ->bindParam(':seach_id',$seach_id)
       ->queryAll();
 
        return $show_postion_id;

    }
}
