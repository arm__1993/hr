<?php

namespace app\modules\hr\controllers;

use app\api\Common;
use app\api\DateTime;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPDF;
use app\modules\hr\models\TaxWitholding;
use app\modules\hr\models\TaxWitholdingDetail;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use mPDF;
use app\modules\hr\controllers\MasterController;
use app\modules\hr\apihr\ApiSSOExport;
use app\modules\hr\apihr\ApiPreexportSSO;
use kartik\mpdf\Pdf;
use app\modules\hr\models\TaxCalculate;
use app\modules\hr\models\TaxCalculateStep;
use app\modules\hr\models\SummaryMoneyWage;


class TaxexportController extends MasterController
{

    public $PDF_Path = 'upload/PDF/';
    // public $idcardLogin;


    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function actionIndex()
    {
        //$mpdf = new PDF('th', 'Tharlon-Regular');

        //$mpdf->WriteHTML($this->renderPartial('_report'));
        //$mpdf->Output();
        //exit;
        //return $this->render('index');

        //http://demos.krajee.com/mpdf


        $xxx = $this->renderPartial('_report');

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            //'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,

            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,

            // your html content input
            'content' => $xxx,
            'cssFile' => '@app/css/hr/mPDFcss.css',
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            // 'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/bootstrap.css',
            // any css to be embedded if required
            //'cssInline' => 'font-family: "THSarabunNew", serif !important;',
            //'cssInline' => 'body{font-size:12px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Preview Report Case: 0001'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Krajee Report Header'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionMpdf()
    {
        //$mpdf = new PDF('thsaraban', 'THSarabunNew');
        //$mpdf = new mPDF('th', 'A4-P', '', 'thsarabun');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->charset_in = 'utf-8';
        $mpdf->autoLangToFont = true;
        $mpdf->WriteHTML($this->renderPartial('_report'));
        $mpdf->Output();
    }


    public function actionExportpdfcertificate()
    {

        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_exporttocertificate'));
        $mpdf->Output();
        exit;

    }

    public function actionExportcompany()
    {
        $postValue = Yii::$app->request->post('data');
        //  echo "<pre>";
        //  print_r($postValue);
        // echo "<br>";
        // exit();
        $id_type = $postValue['type_data'];
        $id_company = $postValue['selectworking'];
        $month = $postValue['selectmonth'];
        $year = $postValue['selectyear'];
        $sso_date = $postValue['ssoDate'];
        ApiSSOExport::update($id_company, $month, $year, $sso_date);
        ApiSSOExport::type_data($id_type);
        $checknull = ApiSSOExport::find_company($id_company, $month, $year);
        if ($checknull == 0) {
            return 0;
            exit;
        }
        ApiSSOExport::find_detail($id_company);
        ApiSSOExport::builddata_head1($id_company, $month, $year);
        ApiSSOExport::builddata1($id_company);
        ApiSSOExport::builddata_head2($id_company, $month, $year);
        ApiSSOExport::builddata2($id_company);
        ApiSSOExport::setpath($id_company, $month, $year);
        $filepart = ApiSSOExport::writefile($id_company, $id_type, $month, $year);
        $checkstructure = ApiSSOExport::checkstructure($filepart);
        if ($checkstructure == 0) {
            return 0;
        }
        return $filepart;
    }

    public function actionDownloadfile()
    {
        $postValue = Yii::$app->request->get('data');
        $postValue = Json::decode($postValue, true);
        $id_type = $postValue['type_data'];
        $id_company = $postValue['selectworking'];
        $month = $postValue['selectmonth'];
        $year = $postValue['selectyear'];
        ApiSSOExport::download($id_company, $month, $year);
    }

    public function actionPredata()
    {
        // $for_month = date('m');
        // $for_year = date('Y');
        $for_month = '04';
        $for_year = '2014';
        $company_id = '43';
        $this->getdata($for_month, $for_year, $company_id);
    }

    public function getdata($month, $year, $branch)
    {
        $objModelHead = new SsoPaidHead();
        $objModelDetail = new SsoPaidDetail();
        $objApiPreexportSSO = new ApiPreexportSSO();
        $arrCompany = $arrWagehistory = [];
        $arr = [];
        $objApiPreexportSSO->setCompany();
        $dataCompany = $objApiPreexportSSO->getCompany($branch);
        $objApiPreexportSSO->setWagehistory($branch, $month, $year);
        $dataWagehistory = $objApiPreexportSSO->getWagehistory($branch);
        $objApiPreexportSSO->setadddeducthistiry($branch, $month, $year);
        $dataadddeducthistiry = $objApiPreexportSSO->getadddeducthistiry($dataWagehistory['WAGE_EMP_ID']);
        $arr['sso_paid_head'] = '0';
        $arr['for_month'] = '0';
        $arr['for_year'] = '0';
        $arr['RECORD_TYPE'] = '1';
        $arr['ACC_NO'] = str_replace("-", "", $dataCompany['soc_acc_number']);
        $arr['BRANCH_NO'] = $branch;
        $arr['PAID_PERIOD'] = $month . substr($year, 2, 2);
        $arr['RATE'] = ApiHr::getOrgMasterConfig()->getSavingRate();
        $arr['TOTAL_EMPLOYEE'] = str_pad($dataadddeducthistiry['TOTAL_EMPLOYEE'], 6, "0", STR_PAD_LEFT);
        $arr['TOTAL_WAGES'] = str_pad($dataadddeducthistiry['TOTAL_WAGES'], 15, "0", STR_PAD_LEFT);
        $arr['TOTAL_PAID'] = str_pad($dataadddeducthistiry['TOTAL_PAID'], 14, "0", STR_PAD_LEFT);
        $arr['TOTAL_PAID_BY_EMPLOYEE'] = str_pad($dataadddeducthistiry['TOTAL_PAID_BY_EMPLOYEE'], 12, "0", STR_PAD_LEFT);
        $arr['TOTAL_PAID_BY_EMPLOYER'] = str_pad($dataadddeducthistiry['TOTAL_PAID_BY_EMPLOYER'], 12, "0", STR_PAD_LEFT);
        $arr['YEARS'] = $year;
        $arr['MONTHS'] = $month;
        $arr['DEPARTMENT_ID'] = '0';
        $arr['COMPANY_ID'] = $branch;
        $arr['PAID_DATE'] = '';
        $arr['COMPANY_NAME'] = $dataCompany ['name'];
        $head = ApiPreexportSSO::InseartHead($arr);
        $detail = $objApiPreexportSSO->getadddeducthistiryASdetail();
        $arrDetail = [];
        foreach ($detail as $item) {
            $type = '';
            $dataEmp = $objApiPreexportSSO->getWagehistoryByEmp($item['ADD_DEDUCT_THIS_MONTH_EMP_ID']);
            //003=นาย,004=นางสาว,005=นาง
            if ($dataEmp['Be'] == 'นาย') {
                $type = '003';
            }
            if ($dataEmp['Be'] == 'นาง') {
                $type = '005';
            }
            if ($dataEmp['Be'] == 'นางสาว') {
                $type = '004';
            }
            $arrDetail['ID_Header'] = $head;
            $arrDetail['RECORD_TYPE'] = '2';
            $arrDetail['SSO_ID'] = $item['ADD_DEDUCT_THIS_MONTH_EMP_ID'];
            $arrDetail['PREFIX'] = $type;
            $arrDetail['FNAME'] = $dataEmp['Name'];
            $arrDetail['LNAME'] = $dataEmp['Surname'];
            $arrDetail['WAGES'] = str_pad(str_replace(".", "", $dataEmp['WAGE_SALARY']), 14, "0", STR_PAD_LEFT);
            $arrDetail['PAID_AMOUNT'] = str_pad(str_replace(".", "", $dataEmp['WAGE_TOTAL_DEDUCTION']), 12, "0", STR_PAD_LEFT);
            $arrDetail['BLANK'] = $item[''];
            $arrDetail['COMPANY_ID'] = $branch;
            if ($dataEmp != null) {
                ApiPreexportSSO::InseartDetail($arrDetail);
            }
        }
    }



    public function actionSamplepdf()
    {
        $id_company = '43';
        $y = '2014';
        $m = '4';
        $data = ApiSSOExport::sps10($id_company, $y, $m);
        $company = ApiSSOExport::getcompany($id_company);
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $stylesheet = file_get_contents('/media/lt0109/source1/htdoc/wseasyerp/css/hr/SSOexpoet/ssoexport.css');
        $mpdf->AddPage('L');
        $mpdf->WriteHTML($this->renderPartial('_reportView', ['data' => $data, 'company' => $company]));
        $mpdf->AddPage('P');
        $mpdf->WriteHTML($stylesheet, 1);
        $detaildata = ApiSSOExport::find_detail($id_company);
        $mpdf->WriteHTML($this->renderPartial('_reportViewssodetail', ['data' => $detaildata, 'company' => $company, 'dataH' => $data]));
        $mpdf->Output();
        exit;
    }


    private function mkdir($dir)
    {
        if (!file_exists($this->PDF_Path)) {
            $old = umask(0000);
            mkdir($this->PDF_Path, 0777, true);
            umask($old);
            return true;
        }

        if (!file_exists($this->PDF_Path . $dir)) {
            $old = umask(0000);
            mkdir($this->PDF_Path . $dir, 0777, true);
            umask($old);
            return true;
        }
    }

    public function printobj($id) //print_r
    {
        echo '<pre>';
        print_r($id);
        echo '</pre>';
        //exit();

    }


    public function actionTvi50pdf()
    {
        if (Yii::$app->request->isGet) {
            $postValue = Yii::$app->request->get();
            $month_pay = $postValue['month_pay'];
            $sign_date = DateTime::DateToMysqlDB($postValue['sign_date']);
            $_thai_date = $postValue['sign_date'];
            $company_id = $postValue['company_id'];
            $emp_id = $postValue['emp_id'];
            //check and create dir
            $dir = ApiPDF::flip_month($month_pay);
            $this->mkdir($dir);

            $emp_data = ApiPDF::get_emp_data_all();
            $all_company = ApiHr::getWorking_company();

           // $wht_data = ApiPDF::get_wht_data($month_pay, $company_id, $emp_id);
            $wht_data_detail = ApiPDF::get_wht_data_detail($month_pay, $company_id, $emp_id);//ค้นหา tax_witholding_detail && tax_witholding



            $mpdf = new \Mpdf\Mpdf();
            foreach ($wht_data_detail as $item_data) {

                $id_card = $item_data['emp_idcard'];
                $id_card_extract = ApiPDF::extract_id_card($id_card);
                $company_data = $all_company[$item_data['company_id']];
                $_emp_data = $emp_data[$id_card];



                $mpdf->AddPage('P', '', '', '', '', 5, 5, 5, 5, 10, 10);
                $mpdf->WriteHTML($this->renderPartial('_tvi50', [
                    'emp_data' => $emp_data[$id_card],
                    'company' => $company_data,
                    'company_code' => ApiPDF::extract_id_card($company_data['business_number']),
                    'item_data' => $item_data,
                    'sign_date' => $sign_date,
                    'thai_date' => $_thai_date,
                    'month_pay' => $month_pay,
                    'id_card_extract' => $id_card_extract,
                    'wht_data_detail' => $wht_data_detail[$id_card],
                ]));



            }
        }

//        $a = $company_data['short_name'];
//      ต้องการเรียกใช้ private function Addpage-->  $this->Addpage($a, $dir);
        $file_name = $dir . '_' . $company_data['short_name'] . '_' . $_emp_data['ID_Card'];
        $mpdf->Output('upload/PDF/' . $dir . '/' . $file_name . '.pdf','I'); // ถ้าต้องการเก็บ---->  \Mpdf\Output\Destination::FILE); //แบบธรรมดา----> $mpdf ->Output();
        exit;
    }//หัก ณ ที่จ่าย


    public function actionPndpdf1()
    {

        if (Yii::$app->request->isGet) {
            $postValue = Yii::$app->request->get();
            $pay_date = $postValue['month_pay'];
            $sign_date = DateTime::DateToMysqlDB($postValue['sign_date']);
            $sign_date2 = $postValue['sign_date'];
            $company_id = $postValue['company_id'];
            $emp = $postValue['emp_id'];
            $dir = ApiPDF::flip_monthpnd1($pay_date);
            $this->mkdir($dir);

            $model1 = ApiPDF::pdf_pnd1($company_id, $pay_date, $emp);

            $pay_date = [];
            $emp_idcard1 = [];
            foreach ($model1 as $row3) {

                $pay_date[$row3['pay_date']] = $row3['pay_date'];
                $emp_idcard1[$row3['emp_idcard']] = $row3['emp_idcard'];
            }

            $model = SummaryMoneyWage::find()
                ->select('*')
                ->from('summary_money_wage')
                ->innerJoin('tax_calculate', 'tax_calculate.emp_idcard = summary_money_wage.emp_idcard')
                ->where([
                    'summary_money_wage.emp_idcard' => $emp_idcard1,
                    'summary_money_wage.pay_date' => $pay_date,


                ])
                ->andWhere('tax_calculate.total_tax_month > 0 ')
                ->asArray()
                ->all();


            $all_company = ApiHr::getWorking_company();
            $arryItem = [];
            $arryItem2 = [];
            $total_monthly = [];
            foreach ($model as $row) {

                $arryItem[$row['tax_amount']] = $row['tax_amount'];
                $total_monthly[$row['income_amount']] = $row['income_amount'];
                $company_data = $all_company[$row['company_id']];
                $day = $row['months'];
                $arryItem2[$row['emp_idcard']] = $row['emp_idcard'];


            }
            $input_array = $model;

            $id_cardcompany = $company_data['inv_number'];

            $id_card_extract = ApiPDF::extract_id_card($id_cardcompany);

            $years = ($year = (date("Y") + 543));

            $count = (count(array_chunk($input_array, 5)));

            $countnum = (count($model));
            $taxincluded = (array_sum($arryItem));
            $total_monthlys = (array_sum($total_monthly));
            $p = 1;

            $mpdf = new \Mpdf\Mpdf();

            $mpdf->AddPage('P', '', '', '', '', 5, 5, 5, 5, 10, 10);

            $mpdf->WriteHTML($this->renderPartial('_pdfpnd1', [
                'id_card_extract' => $id_card_extract,
                'years' => $years,
                'm' => $day,
                'count' => $count,
                'countnum0' => $countnum,
                'taxincluded0' => $taxincluded,
                'company_data' => $company_data,
                'p' => $p,
                'total_monthlys0' => $total_monthlys,

            ]));


            $model = SummaryMoneyWage::find()
                ->select('*')
                ->from('summary_money_wage')
                ->innerJoin('tax_calculate', 'tax_calculate.emp_idcard = summary_money_wage.emp_idcard')
                ->where([
                    'summary_money_wage.emp_idcard' => $emp_idcard1,
                    'summary_money_wage.pay_date' => $pay_date,


                ])
                ->andWhere('tax_calculate.total_tax_month > 0 ')
                ->asArray()
                ->all();

            $all_company = ApiHr::getWorking_company();
            $arryItem = [];
            $arryItem2 = [];
            $total_monthly = [];
            $emp_data = ApiPDF::get_emp_data_all();
            foreach ($model as $row) {

                $arryItem[$row['total_tax_month']] = $row['total_tax_month'];
                $total_monthly[$row['total_monthly']] = $row['total_monthly'];
                $company_data = $all_company[$row['company_id']];
                $arryItem2[$row['emp_idcard']] = $row['emp_idcard'];


            }
            $id_cardcompany = $company_data['inv_number'];
            $id_card_extract = ApiPDF::extract_id_card($id_cardcompany);
            $fullname = ApiHr::getEmpNameForCreateByInIdcard($arryItem2);
            $arrmain = [];
            $arrsub = [];

            $c = 0;
            foreach ($model as $k => $v) {
                $c++;
                $arrsub[$k] = $v;
                if ($c == 5) {
                    $arrmain[] = $arrsub;
                    $arrsub = array();
                    $c = 0;
                    $i = $arrmain++;

                }

            }
            $arrmain[] = $arrsub;

            $count = count($arrmain);
            $page = 1;
            foreach ($arrmain as $arrmains) {
                $mpdf->AddPage('L', '', '', '', '', 5, 5, 5, 5, 10, 10);
                $mpdf->WriteHTML($this->renderPartial('pdfpnd1attach', [
                    'id_card_extract' => $id_card_extract,
                    'arrmains' => $arrmains,
                    'fullname' => $fullname,
                    'count' => $count,
                    'r' => $i,
                    'emp_data' => $emp_data,
                    'page' => $page,
                    'sign_date' => $sign_date2

                ]));
                $page++;
            }
        }


        $file_name = $dir . '_' . $company_data['short_name'];

        $mpdf->Output('upload/PDF/' . $dir . '/' . $file_name . '.pdf', 'I');
        $urlb = Yii::$app->request->baseUrl .'/upload/PDF/' . $dir . '/' . $file_name . '.pdf';
        self::printobj($urlb);
        exit;
    }  //ภงด 1 พร้อมไฟล์แนบ


    public function actionTvi50pdfs()
    {

        //POST_payrollexport/exporttorevenue
        //---------POST AJAX -------------//
//        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
//            $postValue = Yii::$app->request->post();
        //---------POST AJAX -------------//
        if (Yii::$app->request->isGet) {
            $postValue = Yii::$app->request->get();
            $day = DateTime::DateToMysqlDB($postValue['sign_date']);
            $sign_date2 = $postValue['sign_date'];
            $year = $postValue['year_pay'];
            $company_id = $postValue['company_id'];
            $id = $postValue['emp_id'];
            //POST_payrollexport/exporttorevenue


            $dirname = 'TV50_' . $year;
            $dir = ApiPDF::flip_monthtv50($dirname);
            $this->mkdir($dir);



            $model_max_id = ApiPDF::Tvi50pdfs($id, $year, $company_id);
            $arrMaxID = [];
            foreach ($model_max_id as $max) {
                array_push($arrMaxID, $max['max_id']);
            }
            $tax_calculate = SummaryMoneyWage::find()
                ->where([
                    'id' => $arrMaxID
                ])
                ->asArray()
                ->all();
            $emp_data = ApiPDF::get_emp_data_all();
            $all_company = ApiHr::getWorking_company();
            foreach ($tax_calculate as $row) {
                $arryItem[$row['total_tax_year']] = $row['total_tax_year'];
                $total_monthly[$row['total_yearly']] = $row['total_yearly'];
                $company_data = $all_company[$row['company_id']];
                $id_card = [$row['emp_idcard']];
                $id_cardcompany = $company_data['inv_number'];
                $id_card_extract = ApiPDF::extract_id_card($id_cardcompany);
                $idcard = ApiPDF::extract_id_card($id_card[0]);
            }


            $mpdf = new \Mpdf\Mpdf();
            foreach ($tax_calculate as $arrmains) {
                $mpdf->AddPage('P', '', '', '', '', 4, 4, 4, 4, 10, 10);
                $mpdf->WriteHTML($this->renderPartial('_tvi502', [
                    'id_card_extract' => $idcard,
                    'company_code' => $id_card_extract,
                    'company_data' => $company_data,
                    'arrmains' => $arrmains,
                    'emp_data' => $emp_data,
                    'day' => $sign_date2,

                ]));

            }

        }
        $file_name = $dir . '_' . $company_data['short_name'] . '_' . $emp_data['ID_Card'];

        $mpdf->Output('upload/PDF/' . $dir . '/' . $file_name . '.pdf','I'); //\Mpdf\Output\Destination::FILE);
        $urlb = Yii::$app->request->baseUrl .'/upload/PDF/' . $dir . '/' . $file_name . '.pdf';
        exit;
    } //TV50 by arm


    public function actionPnd1aatt()
    {
        //---------POST AJAX -------------//
//        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
//            $postValue = Yii::$app->request->post();
        //---------POST AJAX -------------//
        if (Yii::$app->request->isGet) {
            $postValue = Yii::$app->request->get();
            $day = DateTime::DateToMysqlDB($postValue['sign_date']);
            $pay_year = $postValue['year_pay'];
            $sign_date2 = $postValue['sign_date'];
            $company_id = $postValue['company_id'];
            $id = $postValue['emp_id'];
            //POST_payrollexport/exporttorevenue


            $dirname = 'income_tax_1A_' . $pay_year;
            $dir = ApiPDF::flip_monthtv50($dirname);
            $this->mkdir($dir);


            $model1 = ApiPDF::Pdf_pnd1_a($company_id, $pay_year, $id);


            $pay_date = [];
            $emp_idcard1 = [];
            foreach ($model1 as $row3) {

                $pay_date[$row3['pay_date']] = $row3['pay_date'];
                $emp_idcard1[$row3['emp_idcard']] = $row3['emp_idcard'];
            }


            $model = SummaryMoneyWage::find()
                ->select('*')
                ->from('summary_money_wage')
                ->innerJoin('tax_calculate', 'tax_calculate.emp_idcard = summary_money_wage.emp_idcard')
                ->where([
                    'summary_money_wage.emp_idcard' => $emp_idcard1,
                    'summary_money_wage.pay_date' => $pay_date,


                ])
                ->andWhere('tax_calculate.total_tax_month > 0 ')
                ->asArray()
                ->all();


            $all_company = ApiHr::getWorking_company();

            $arryItem = [];
            $id_card = [];
            $total_monthly = [];

            foreach ($model as $row) {

                $arryItem[$row['total_tax_year']] = $row['total_tax_year'];
                $total_monthly[$row['income_total_amount']] = $row['income_total_amount'];
                $company_data = $all_company[$row['company_id']];

                $day = $row['months'];
                $id_card[$row['emp_idcard']] = $row['emp_idcard'];


            }

            $input_array = $model;
            $id_cardcompany = $company_data['inv_number'];
            $id_card_extract = ApiPDF::extract_id_card($id_cardcompany);
            $years = ($year = (date("Y") + 543));
            $count = (count(array_chunk($input_array, 5)));
            $countnum = (count($model));
            $taxincluded = (array_sum($arryItem));
            $total_monthlys = (array_sum($total_monthly));
            $p = 1;


            $mpdf = new \Mpdf\Mpdf();
            //$stylesheet = file_get_contents('/media/lt0109/source1/htdoc/wseasyerp/css/hr/tvi50.css');
            //$mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage('P', '', '', '', '', 5, 5, 5, 5, 10, 10);
            $mpdf->WriteHTML($this->renderPartial('pdfpnd1a', [
                'id_card_extract' => $id_card_extract,
                'years' => $years,
                'm' => $day,
                'count' => $count,
                'countnum0' => $countnum,
                'taxincluded0' => $taxincluded,
                'company_data' => $company_data,
                'p' => $p,
                'total_monthlys0' => $total_monthlys,


            ]));

            $model = SummaryMoneyWage::find()
                ->select('*')
                ->from('summary_money_wage')
                ->innerJoin('tax_calculate', 'tax_calculate.emp_idcard = summary_money_wage.emp_idcard')
                ->where([
                    'summary_money_wage.emp_idcard' => $emp_idcard1,
                    'summary_money_wage.pay_date' => $pay_date,


                ])
                ->andWhere('tax_calculate.total_tax_month > 0 ')
                ->asArray()
                ->all();
            $all_company = ApiHr::getWorking_company();
            $_emp_data = ApiPDF::get_emp_data_all();

            $arryItem = [];
            $arryItem2 = [];
            $total_monthly = [];
            foreach ($model as $row) {


                $arryItem[$row['total_tax_year']] = $row['total_tax_year'];
                $total_monthly[$row['total_yearly']] = $row['total_yearly'];
                $company_data = $all_company[$row['company_id']];
                $arryItem2[$row['emp_idcard']] = $row['emp_idcard'];


            }
            $id_cardcompany = $company_data['inv_number'];
            $id_card_extract = ApiPDF::extract_id_card($id_cardcompany);
            $fullname = ApiHr::getEmpNameForCreateByInIdcard($arryItem2);


            $arrmain = [];
            $arrsub = [];
            $c = 0;
            $page = 1;
            foreach ($model as $k => $v) {

                $c++;
                $arrsub[$k] = $v;
                if ($c == 5) {
                    $arrmain[] = $arrsub;
                    $arrsub = array();
                    $c = 0;
                    $i = $arrmain++;
                }
            }
            $arrmain[] = $arrsub;
            $countt = count($arrmain);

            foreach ($arrmain as $arrmains) {
                $mpdf->AddPage('L', '', '', '', '', 5, 5, 5, 5, 10, 10);
                $mpdf->WriteHTML($this->renderPartial('pnd1a_attach', [
                    'id_card_extract' => $id_card_extract,
                    'arrmains' => $arrmains,
                    'fullname' => $fullname,
                    'count' => $countt,
                    'r' => $i,
                    'tax_calculatew' => $model,
                    'emp_data' => $_emp_data,
                    'page' => $page,

                ]));
                $page++;
            }
        }
        $file_name = $dir . '_' . $company_data['short_name'] . '_' . $_emp_data['ID_Card'];

        $mpdf->Output('upload/PDF/' . $dir . '/' . $file_name . '.pdf', 'I');//\Mpdf\Output\Destination::FILE);
        $urlb = Yii::$app->request->baseUrl .'/upload/PDF/' . $dir . '/' . $file_name . '.pdf';
        exit;
    } //ภงด 1ก พร้อมไฟล์แนบ






    private function Addpage($b, $dir)
    {
        $mpdf = new \Mpdf\Mpdf();
        $objScan = scandir(\Yii::$app->basePath . '/upload/PDF/' . $dir);
        $arrFile = [];
        foreach ($objScan as $a) {
            if ($a != '.' && $a != '..' && $a != '.DS_Store' && (stristr("$a", $b))) {
                array_push($arrFile, $a);
            }
        }
        foreach ($arrFile as $value) {
            $mpdf->AddPage('P', '', '', '', '', 5, 5, 5, 5, 10, 10);
            $mpdf->SetImportUse();
            $pagecount = $mpdf->SetSourceFile(\Yii::$app->basePath . '/upload/PDF/' . $dir . '/' . $value);
            $tplIdx = $mpdf->ImportPage($pagecount);
            $mpdf->UseTemplate($tplIdx);
        }
        $past =  Yii::$app->request->baseUrl.'/upload/PDF/'.$b.'.pdf';
        $mpdf->Output('upload/PDF/' . $b . '.pdf', \Mpdf\Output\Destination::FILE);
        print_r($past);
        return $this->redirect([$past]);
    }

    public function actionPnd1()
    {
        return $this->render('pnd1');
    }

    public function actionPnd1a()
    {
        return $this->render('pnd1a', ['error' => '']);
    }

    public function actionPnd1pdf()
    {
        $postValue = Yii::$app->request->post();
        $data = ApiSSOExport::getTaxPndHead($postValue['month'], $postValue['year'], $postValue['selectworking']);
        if (count($data) == 0) {
            return $this->render('pnd1', ['error' => 'ไม่มีข้อมมูลในเดือนที่เลือก']);
        }
        $dataDetail = ApiSSOExport::getTaxPndDetail($data[0]['id']);
        $arrayDetail = ApiSSOExport::manageArray($dataDetail);
        //$mpdf = new mPDF('th', 'Tharlon-Regular', 0, '', 15, 8, 5, 8, 8, 8);
        $mpdf = new \Mpdf\Mpdf();
        $stylesheet = file_get_contents(\Yii::$app->basePath . '/css/hr/tvi50.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->AddPage('P');
        $mpdf->WriteHTML($this->renderPartial('_pnd1', ['data' => $data]));
        $mpdf->AddPage();
        $mpdf->SetImportUse();
        $pagecount = $mpdf->SetSourceFile(\Yii::$app->basePath . '/images/wshr/pnd1_2.pdf');
        $tplIdx = $mpdf->ImportPage($pagecount);
        $mpdf->UseTemplate($tplIdx);
        $tplIdx = $mpdf->ImportPage($pagecount);
        $mpdf->UseTemplate($tplIdx);

        $datepay = date("d/m/", strtotime($data[0][signature_date])) . (date("Y", strtotime($data[0][signature_date])) + 543);
        $count_page = 1;
        $all_page = count($arrayDetail);
        // foreach($arrayDetail as $item){
        //      $mpdf->AddPage('L');
        //      $mpdf->WriteHTML($this->renderPartial('_pnd1_attachment',['data'=>$item,'date'=>$datepay,'count_page'=>$count_page,'all_page'=>$all_page,'datahead'=>$data]));
        //      $count_page++;
        // }
        $mpdf->Output();
        exit;
    }

    public function actionPnd1apdf()
    {
        $postValue = Yii::$app->request->post();
        $obj_Api = new ApiSSOExport();
        $dataid = ApiSSOExport::idTaxPndHead($postValue['year'], $postValue['selectworking']);
        $data = ApiSSOExport::getPad1aTaxPndHead($postValue['year'], $postValue['selectworking']);
        $dataAmount = $obj_Api->getPad1aTaxPndamount($dataid);
        $detail = $obj_Api->getDatadetail();
        $arrayDetail = ApiSSOExport::manageArray($detail);

        //$mpdf = new mPDF('th', 'Tharlon-Regular', 0, '', 15, 8, 5, 8, 8, 8);
        $mpdf = new \Mpdf\Mpdf();
        $stylesheet = file_get_contents(\Yii::$app->basePath . '/css/hr/tvi50.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->AddPage('P');
        $mpdf->WriteHTML($this->renderPartial('_pnd1a', ['data' => $data, 'data_amount' => $dataAmount]));
        $mpdf->AddPage();
        $mpdf->SetImportUse();
        $pagecount = $mpdf->SetSourceFile(\Yii::$app->basePath . '/images/wshr/pnd1_2.pdf');
        $tplIdx = $mpdf->ImportPage($pagecount);
        $mpdf->UseTemplate($tplIdx);
        $count_page = 1;
        $all_page = count($arrayDetail);
        foreach ($arrayDetail as $item) {
            $mpdf->AddPage('L');
            $mpdf->WriteHTML($this->renderPartial('_pnd1a_attachment', ['arrayDetail' => $item, 'data' => $data, 'count_page' => $count_page, 'all_page' => $all_page]));
            $mpdf->Output();
            $count_page++;
        }
        exit;
    }

    public function actionForceDownloadPdf()
    {
        //$mpdf=new mPDF();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('mpdf'));

        $mpdf->Output('MyPDF.pdf', 'D');
        exit;
    }


}
