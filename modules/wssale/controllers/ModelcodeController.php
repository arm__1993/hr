<?php

namespace app\modules\wssale\controllers;
use app\modules\wssale\models\Modelcode;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use app\api\Helper;
 

class ModelcodeController extends ActiveController
{
    public $modelClass = 'app\modules\wssale\models\Modelcode';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	
	function actionUnittype()
	{
        //return 1;
     	//$NameCarband = $_GET['NameCarband'];
        $listmodelcode = new Modelcode();
        $dataUnittypemodel = $listmodelcode->unittype();
        return $dataUnittypemodel;
    }
	function actionModelclasscar()
	{
        //return 1;
     	$unittype = $_GET['unittype'];
        $listdata = new Modelcode();
        $data = $listdata->modelclasscar($unittype);
        return $data;
    }
    function actionModelclassname()
    {

    	$unittype = $_GET['unittype'];
     	$modelclass = $_GET['modelclass'];
        $listdata = new Modelcode();
        $data = $listdata->modelclassname($unittype,$modelclass);
        return $data;

    }
    function actionModelgname()
    {

    	$unittype = $_GET['unittype'];
     	$modelclass = $_GET['modelclass'];
     	$modelcodename = $_GET['modelcodename'];


        $listdata = new Modelcode();
        $data = $listdata->modelgname($unittype,$modelclass,$modelcodename);
        return $data;

    }
    function actionCrpandairincvat()
    {

    	$unittype = $_GET['unittype'];
     	$modelclass = $_GET['modelclass'];
     	$modelcodename = $_GET['modelcodename'];
     	$modelgname = $_GET['modelgname'];

     	// echo "$unittype";
     	// echo "$modelclass";
     	// echo "$modelcodename";
     	// echo "$modelgname";
     	

        $listdata = new Modelcode();
        $data = $listdata->crpandairincvat($unittype,$modelclass,$modelcodename,$modelgname);
   
   		$counData = count($data);

   		
 		$valueCRPAndAirIncVatArrayJson = [] ;
        $i=0;
        while ($i < $counData ) {
        	 $valueCRPAndAirIncVat = Helper::displayDecimal($data[$i]['CRPAndAirIncVat']);
        	
        	 $valueCRPAndAirIncVatArray = array('CRPAndAirIncVat' => $valueCRPAndAirIncVat);
        	 $valueCRPAndAirIncVatArrayJson = $valueCRPAndAirIncVatArray;

        $i++;
        } 
         
         
         
         echo json_encode($valueCRPAndAirIncVatArrayJson);
    }

}
