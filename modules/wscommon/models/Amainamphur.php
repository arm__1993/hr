<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "aMain_amphur".
 *
 * @property integer $amphur_id
 * @property string $amphur_code
 * @property string $amphur_name
 * @property string $geo_id
 * @property integer $province_id
 */
class Amainamphur extends \yii\db\ActiveRecord
{
    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }
    public static function tableName()
    {
        return 'aMain_amphur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amphur_code', 'amphur_name', 'geo_id'], 'required'],
            [['province_id'], 'integer'],
            [['amphur_code'], 'string', 'max' => 4],
            [['amphur_name'], 'string', 'max' => 150],
            [['geo_id'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'amphur_id' => 'Amphur ID',
            'amphur_code' => 'Amphur Code',
            'amphur_name' => 'Amphur Name',
            'geo_id' => 'Geo ID',
            'province_id' => 'Province ID',
        ];
    }
    public function searchamphurbyprovinceid($keyword) // param
    {

        //echo $keyword;
        $amphur = Yii::$app->dbERP_maincusdata
       ->createCommand("select * from aMain_amphur  where province_id = :keyword ORDER BY amphur_name ASC")
       ->bindParam(':keyword',$keyword)->queryAll();
        return $amphur;
        

    }
}



