<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 17/10/2559
 * Time: 11:03
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

$objPHPExcel = new \PHPExcel();
$date_rpt = date('Ymd');

$session = Yii::$app->session;

$carRepairman = $session->get('carRepairman');


// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('AngsanaUPC')
    ->setSize(16);

// Add some data  วิว


$objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
$objWorkSheet->setCellValue('A1', 'ชื่อ ช่าง');
$objWorkSheet->setCellValue('B1', ' จำนวนคันที่ซ่อม');
/*$objWorkSheet->setCellValue('C1', 'Device');
$objWorkSheet->setCellValue('D1', 'Orig Device');
$objWorkSheet->setCellValue('E1', 'Status');
$objWorkSheet->setCellValue('F1', 'Customer Code');
$objWorkSheet->setCellValue('G1', 'Customer Name');
$objWorkSheet->setCellValue('H1', 'Tel.');
$objWorkSheet->setCellValue('I1', 'Center');
$objWorkSheet->setCellValue('J1', 'Sales');*/

$_srow = 2; // เริ่มใส่ข้อมูลบรรทัดที่ 2
$i = 0;
$value = count($carRepairman);
//foreach($result as $row) {
for($_srow=2;$_srow<=$value
;$_srow++)
{
    $objWorkSheet->setCellValue('A' . $_srow, ($carRepairman[$i]['tc_name']));
    $objWorkSheet->setCellValue('B' . $_srow, ($carRepairman[$i]['amount_car']));
    /*$objWorkSheet->setCellValue('C' . $_srow, 'ccc');
    $objWorkSheet->setCellValue('D' . $_srow, 'ddd');
    $objWorkSheet->setCellValue('E' . $_srow, 'EEE');
    $objWorkSheet->setCellValue('F' . $_srow, 'fff');
    $objWorkSheet->setCellValue('G' . $_srow, 'ggg');
    $objWorkSheet->setCellValue('H' . $_srow, 'hhh');
    $objWorkSheet->setCellValue('I' . $_srow, 'iii');
    $objWorkSheet->setCellValue('J' . $_srow, 'jjj');*/
    $i++;
}

//วิา
/*// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A4', 'Miscellaneous glyphs')
    ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');*/



// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('รายงานจำนวนรถที่ซ่อมต่อช่าง');//ชื่อในไฟล
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel2007)
foreach(range('A','B') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="รายงานจำนวนรถที่ซ่อมต่อช่าง'.$date_rpt.'.xlsx"');//=ชื่อไฟล์
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
<?php
session_start();
session_destroy();
?>