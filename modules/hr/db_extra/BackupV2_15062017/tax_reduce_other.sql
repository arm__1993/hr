-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2017 at 09:06 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_reduce_other`
--

CREATE TABLE `tax_reduce_other` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `reduce_name` varchar(200) NOT NULL COMMENT 'ชื่อการลดหย่อน',
  `reduce_amount` decimal(15,2) NOT NULL COMMENT 'จำนวนที่ลดหย่อนได้',
  `reduce_amount_max` decimal(15,2) DEFAULT NULL COMMENT 'จำนวนที่ลดหย่อนได้สูงสุด',
  `for_year` int(11) DEFAULT NULL COMMENT 'ปีที่ได้รับการลดหย่อน',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_reduce_other`
--

INSERT INTO `tax_reduce_other` (`id`, `reduce_name`, `reduce_amount`, `reduce_amount_max`, `for_year`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, 'ค่าการเดินทางท่องเที่ยวภายในประเทศ', '15000.00', '15000.00', 2559, 0, '1509901325106', '2017-06-14 17:14:30', '1509901325106', '2017-06-14 17:14:36'),
(2, 'ค่าใช้จ่ายกินเที่ยวช่วงสงกรานต์', '15000.00', '15000.00', 2559, 0, '1509901325106', '2017-06-14 17:15:27', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_reduce_other`
--
ALTER TABLE `tax_reduce_other`
  ADD PRIMARY KEY (`id`),
  ADD KEY `for_year` (`for_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_reduce_other`
--
ALTER TABLE `tax_reduce_other`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส', AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
