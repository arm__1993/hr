<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "org_configmaster".
 *
 * @property integer $id
 * @property string $work_day_inmonth
 * @property string $work_hour_inday
 * @property string $work_start_hour_inday
 * @property string $work_end_hour_inday
 * @property string $sso_rate
 * @property string $saving_rate
 * @property integer $day_start_salary
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class OrgConfigmaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org_configmaster';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['work_day_inmonth', 'work_hour_inday', 'work_start_hour_inday', 'work_end_hour_inday', 'sso_rate', 'saving_rate', 'day_start_salary'], 'required'],
            [['work_day_inmonth', 'work_hour_inday', 'sso_rate', 'saving_rate'], 'number'],
            [['work_start_hour_inday', 'work_end_hour_inday', 'create_datetime', 'update_datetime'], 'safe'],
            [['day_start_salary'], 'integer'],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
            [['work_start_hour_inday','work_end_hour_inday'], 'match', 'pattern' => '/([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?/i'] //match with time format [00:00:00] - [23:59:59]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'work_day_inmonth' => 'จำนวนวันทำงานในเดือน',
            'work_hour_inday' => 'จำนวนชั่วโมงทำงานในวัน',
            'work_start_hour_inday' => 'กำหนดเวลาเข้างานปกติ (eg. 08:00:00)',
            'work_end_hour_inday' => 'กำหนดเวลาเลิกงานปกติ (eg. 17:00:00)',
            'sso_rate' => 'อัตราการคิดเงินประกันสังคมพนักงาน (%)',
            'saving_rate' => 'อัตราการคิดเงินสะสมพนักงาน (%)',
            'day_start_salary' => 'วันเริ่มรอบเงินเดือน',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }



    public function getWorkDayInMonth()
    {
        return $this->work_day_inmonth;
    }


    public function getWorkHourInDay()
    {
        return $this->work_hour_inday;
    }


    public function getStartWorkHour()
    {
        return $this->work_start_hour_inday;
    }

    public function getEndWorkHour()
    {
        return $this->work_end_hour_inday;
    }

    public function getSSORate()
    {
        return $this->sso_rate;
    }

    public function getSavingRate()
    {
        return $this->saving_rate;
    }

    public function getDayStartSalary()
    {
        return $this->day_start_salary;
    }

}
