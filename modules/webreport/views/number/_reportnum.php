<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

// $script = <<< JS
// $(document).ready(function() {
//     $('#linklogout, #alogout').on("click",function(){
//        bootbox.confirm({
//             size: "small",
//             message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
//             callback: function(result){
//                 if(result==1) {
//                     window.location.href='index.php?r=login/logout';
//                 }
//             }
//           });
//     });
// $('#example-getting-started').multiselect();
// });
// JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$result_total = $session->get('result_total');
$selected_company = $session->get('selected_company');
$selected_daterange = $session->get('selected_daterange');
$technician_car = $session->get('technician_car');

/*print_r($totalTc);
exit();*/
?>


<h3><i class="fa fa-fw fa-file-text"></i>รายงานจำนวนรถที่ซ่อมต่อช่าง</h3>
<h5><?php  $namecompany = ApiReport::getNameCompanyPDF($selected_company) ;
        echo $namecompany['0']['name']; ?></h5>
 <!-- Default box -->
      <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th bgcolor="#6F6E6E" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            วันที่
                                            </th>
                                            <th bgcolor="#6F6E6E" class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending">
                                                จำนวนรถซ่อม <?php if($technician_car == 1){
                                                    echo "รถเล็ก";
                                                }else
                                                {
                                                    echo "รถใหญ่";
                                                }

                                                 ?>   
                                            </th>
                                            <th bgcolor="#6F6E6E" class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                จำนวนช่าง
                                            </th>
                                            <th bgcolor="#6F6E6E" class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                จำนวนคันที่ซ่อมต่อช่าง 1 คน
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php 
                                        foreach ($result_total as $key => $value) {
                                        ?>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1" align = "center">
                                        <?php echo $key ;?>
                                        </td>                                        
                                        <td align = "center">
                                         <?php echo $value['0'] ;?>
                                        </td>
                                        <td align = "center">
                                        <?php echo $value['1'] ;?>
                                        </td>
                                        <td align = "center">
                                        <?php $sum_result =  $value['0']/$value['1'] ;
                                            echo Helper::displayDecimal($sum_result);
                                        ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
<?php
session_start();
session_destroy();
?>