<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 09:51
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/employee-lookup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/ot_edit.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$today = date('d/m/Y');

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#"> ข้อมูลเงินเดือน</a>
                </li>
                <li>บันทึกข้อมูลการทำงานล่วงเวลา</li>
                <li class="active">แก้ไขข้อมูลการทำงานล่วงเวลา</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>

        <form class="form-horizontal" id="frmseachotedit" name="frmseachotedit" method="post">
            <div class="box-body">


                    <div class="box-header with-border">
                        <h3 class="box-title">ค้นหารายละเอียดข้อมูล</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-1">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="btn-group">
                                        <label for="numberPassportEmp" class="col-sm-4 control-label">บริษัท</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="xcompany" id="xcompany">
                                                <option value="">ทั้งหมด</option>
                                                <?php $working = ApiHr::getWorking_company();
                                                foreach ($working as $value) {
                                                    echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="numberPassportEmp" class="col-sm-3 control-label">แผนก</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="xdepartment" id="xdepartment">
                                            <option value=""> ทั้งหมด</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="numberPassportEmp" class="col-sm-3 control-label">ฝ่าย</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="xsection" id="xsection">
                                            <option value=""> ทั้งหมด</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="numberPassportEmp" class="col-sm-6 control-label">ประเภทการจ่ายค่าล่วงเวลา</label>
                                    <div class="col-sm-6">
                                        <?php

                                        echo Html::dropDownList('otreturn_id[]', null, $arrOTReturn, [
                                            'id' => 'otreturn_id',
                                            'prompt' => 'ทั้งหมด',
                                            'class' => 'form-control',
                                            'data-required' => 'true',
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="numberPassportEmp" class="col-sm-4 control-label">วันที่สร้างคำขอOT</label>
                                    <div class="col-sm-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" readonly="readonly" name="request_date"
                                                   data-required="true" id="request_date"
                                                   value="<?php echo $today; ?>" class="form-control pull-right"
                                                   placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="numberPassportEmp" class="col-sm-3 control-label">รอบเงินเดือน</label>
                                    <div class="col-sm-6">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" data-required="true" id="month_pay" name="month_pay" value="<?php echo date('m-Y');?>">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="numberPassportEmp" class="col-sm-4 control-label">ชื่อกิจกรรม</label>
                                    <div class="col-sm-8">
                                        <?php echo Html::dropDownList('activity_id', null, $arrOTActivity, [
                                            'id' => 'activity_id',
                                            'prompt' => 'ทั้งหมด',
                                            'class' => 'form-control',
                                            'data-required' => 'true',
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="numberPassportEmp" class="col-sm-4 control-label">วันที่ทำ OT</label>
                                    <div class="col-sm-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" readonly="readonly" name="ot_date" data-required="true"
                                                   id="ot_date"
                                                   value="<?php echo $today; ?>" class="form-control pull-right"
                                                   placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                    echo Html::button('<i class="fa fa-search"></i> ค้นหา',
                                        [
                                            'class' => 'btn btn-primary',
                                            'id' => 'btnSearch',
                                        ]);
                                    echo '&nbsp;';
                                    echo Html::button('<i class="fa fa-refresh"></i> ล้างข้อมูล',
                                        [
                                            'class' => 'btn btn-danger',
                                            'id' => 'btn​Cancel',
                                        ]);
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </form>

        <div class="box-body">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">แก้ไขข้อมูลการทำงานล่วงเวลา</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive" id="dvloadot">
                        <img class="loading-image" src="../../images/global/ajax-loader.gif" alt="loading..">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.box -->
</section><!-- /.content -->

<div id="modalfrmEditOT" class="fade modal" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <strong>แก้ไขการทำงานล่วงเวลา</strong>
            </div>
            <div class="modal-body">
                <form role="form" id="frmEditOT" name="frmEditOT">

                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalfrmViewOT" class="fade modal" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <strong>แก้ไขการทำงานล่วงเวลา</strong>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>