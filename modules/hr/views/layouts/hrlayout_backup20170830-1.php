<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use app\api\Common;

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


/*** DEFAULT ROUTING **/
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$img = Yii::$app->request->BaseUrl . '/images/global/';
$CurrActionID = Yii::$app->controller->action->id;


//Home logo Links
$currentController = Yii::$app->controller->id;
$homeLinks = 'index';
if($currentController != 'default') $homeLinks = '../default/index';


echo $siteURL = Common::siteURL().$basePath.'/'.$moduleID;
define('SITE_URL', $siteURL);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='../../auth/default/logout';
                }
            }
          });
    });
});
JS;


$this->registerCssFile(Yii::$app->request->BaseUrl."/fonts/01thaifontcss.css");
$this->registerJs($script);


?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?php echo Html::csrfMetaTags() ?>
    <title>บริษัท อีซูซุเชียงราย จำกัด</title>
    <?php $this->head() ?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->homeUrl;?>/css/hr/hr.css">
</head>


    <body class="hold-transition skin-green-light layout-top-nav">
    <?php $this->beginBody() ?>
    <!-- Site wrapper -->

    <header class="main-header">
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <div class="navbar-header">
                <a href="<?php echo $homeLinks;?>" class="navbar-brand"><span class="logo_text_white"><b>EasyHR</b></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ข้อมูลองค์กร <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>การจัดการผังองค์กร</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/organize/company" ><i class="fa fa-circle-thin"></i>ข้อมูลบริษัท</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/organize/department" ><i class="fa fa-circle-thin"></i>ข้อมูลแผนก</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/organize/division" ><i class="fa fa-circle-thin"></i>ข้อมูลฝ่าย</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/organize/level"><i class="fa fa-circle-thin"></i>ข้อมูลระดับ</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/organize/position"><i class="fa fa-circle-thin"></i>ข้อมูลตำแหน่งาน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/organize/orgchart"><i class="fa fa-circle-thin"></i>ผังโครงสร้างองค์กร</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/organize/orgchartview"><i class="fa fa-circle-thin"></i>เรียกดูผัังโครงสร้างองค์กร</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/organize/emptyposition"><i class="fa fa-circle-thin"></i>รายงานตำแหน่งงานว่าง</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>การจัดการสิทธิ์</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/permission/menulink"><i class="fa fa-circle-thin"></i>เมนูและลิงค์</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/permission/managerequest"><i class="fa fa-circle-thin"></i>สร้างคำขอเปิด/ปิด สิทธิ์การใช้งานโปรแกรม</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/permission/approvedrequest"><i class="fa fa-circle-thin"></i>อนุมัติการเปลี่ยนแปลงสิทธ์การใช้โปรแกรม</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/permission/reportprogram"><i class="fa fa-circle-thin"></i>รายงานสิทธิ์การใช้โปรแกรม</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/permission/uploadpermission"><i class="fa fa-circle-thin"></i>UP File กำหนดสิิทธิ์การใช้โปรแกรม</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/permission/managepermission"><i class="fa fa-circle-thin"></i>เพิ่ม/ลบ/แก้ไขสิทธ์การใช้โปรแกรม</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/permission/searchpermission"><i class="fa fa-circle-thin"></i>ค้นหาสิทธ์การใช้โปรแกรม</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/permission/historylog"><i class="fa fa-hand-o-right"></i>ประวัติการเปลี่ยนแปลงข้อมูลผังกำหนดสิทธิ์ &nbsp;</a></li>

                        </ul>
                    </li>

                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ข้อมูลพนักงาน <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <!--<li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>เพิ่มข้อมูลพนักงาน</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/personal/formaddnewemp"><i class="fa fa-circle-thin"></i>ข้อมูลบุคคล</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/personalsalary/position"><i class="fa fa-circle-thin"></i>ตำแหน่งเงินเดือน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/personalsalary/incomehistory"><i class="fa fa-circle-thin"></i>เปลี่ยนแปลงเงินเดือน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/personal/timeattendance"><i class="fa fa-circle-thin"></i>ลงเวลาเข้า–ออกงาน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/personal/workhistory"><i class="fa fa-circle-thin"></i>ประวัติการทำงาน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/sso/ssohistory"><i class="fa fa-circle-thin"></i>ประกันสังคมและเงินสะสม</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/guarantee/moneyguarantee"><i class="fa fa-circle-thin"></i>ข้อมูลเงินค้ำประกัน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/personal/permit"><i class="fa fa-circle-thin"></i>สัญญาจ้าง</a></li>
                                </ul>
                            </li> -->
                            <li><a href="<?php echo SITE_URL;?>/personal/formaddnewemp"><i class="fa fa-hand-o-right"></i>เพิ่มข้อมูลพนักงาน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/personal/searchemp"><i class="fa fa-hand-o-right"></i>ค้นหาข้อมูลพนักงาน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/personal/editemp"><i class="fa fa-hand-o-right"></i>ปรับปรุงข้อมูลพนักงาน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/personalreport/empcard"><i class="fa fa-hand-o-right"></i>พิมพ์บัตรพนักงาน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/personalreport/empsso"><i class="fa fa-hand-o-right"></i>ประกันสังคม</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/personalexport/empexport"><i class="fa fa-hand-o-right"></i>ส่งออกข้อมูลพนักงาน</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ข้อมูลเงินเดือน <b class="caret"></b></a>
                        <ul class="dropdown-menu">
<!--                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>บันทึกจ่ายเงินระหว่างเดือน </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php /*echo SITE_URL;*/?>/payrollextra/inputform"><i class="fa fa-circle-thin"></i>บันทึกรายการจ่ายระหว่างเดือน</a></li>
                                    <li><a href="<?php /*echo SITE_URL;*/?>/payrollextra/showdeduction"><i class="fa fa-circle-thin"></i>แสดงรายการจ่ายระหว่างเดือน</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>สร้างเงินจ่ายระหว่างเดือน </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php /*echo SITE_URL;*/?>/payrollextra/makededuction"><i class="fa fa-circle-thin"></i>สร้างเงินจ่ายระหว่างเดือน</a></li>
                                    <li><a href="<?php /*echo SITE_URL;*/?>/payrollextra/confirmdeduction"><i class="fa fa-circle-thin"></i>ยืนยันรายการจ่ายระหว่างเดือน</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>รายงานการจ่ายเงินระหว่างเดือน </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php /*echo SITE_URL;*/?>/payrollextra/exportfile"><i class="fa fa-circle-thin"></i>ส่งออกไฟล์</a></li>
                                    <li><a href="<?php /*echo SITE_URL;*/?>/payrollextra/reportdeduction"><i class="fa fa-circle-thin"></i>รายงานบัญชี/การเงิน</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>-->
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>บันทึกจ่ายเงิน </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/payroll/expensesthismonth"><i class="fa fa-circle-thin"></i>บันทึกรายการจ่ายเฉพาะเดือน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/expensesallmonth"><i class="fa fa-circle-thin"></i>บันทึกรายการจ่ายประจำเดือน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/histexpense"><i class="fa fa-circle-thin"></i>ประวัติการจ่ายรายการจ่าย</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>บันทึกหักเงิน </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/payroll/expensesdeducthismonth"><i class="fa fa-circle-thin"></i>บันทึกรายการหักเฉพาะเดือน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/expensesdeductallmonth"><i class="fa fa-circle-thin"></i>บันทึกรายการหักประจำเดือน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/histdeduct"><i class="fa fa-circle-thin"></i>ประวัติการจ่ายรายการหัก</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>รายการหักจากหน่วยงาน </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/payroll/deptdeductlist"><i class="fa fa-circle-thin"></i>รายการหักทั้งหมด</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/adddeductbydept"><i class="fa fa-circle-thin"></i>บันทึกรายการหัก</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/approveddeductbydept"><i class="fa fa-circle-thin"></i>อนุมัติรายการหัก</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/historydeductbydept"><i class="fa fa-circle-thin"></i>ประวัติการอนุมัติรายการหัก</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>ค้นหาและแก้ไขข้อมูลการจ่าย </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/payroll/searchandedit"><i class="fa fa-circle-thin"></i>ค้นหาและแก้ไขตามรายการจ่ายเงิน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payroll/managemonthly"><i class="fa fa-circle-thin"></i>บันทึกรายการประจำเดือน</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>บันทึกข้อมูลการทำงานล่วงเวลา &nbsp;</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/overtime/manageot"><i class="fa fa-circle-thin"></i>บันทึกข้อมูลการทำงานล่วงเวลา</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/overtime/editot"><i class="fa fa-circle-thin"></i>แก้ไขข้อมูลการทำงานล่วงเวลา</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/overtime/historyot"><i class="fa fa-circle-thin"></i>ประวัติการอนุมัติค่าล่วงเวลา</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>อนุมัติข้อมูลการทำงานล่วงเวลา &nbsp;</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/overtime/paidot"><i class="fa fa-circle-thin"></i>รายการจ่ายค่าล่วงเวลารออนุมัติ</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/overtime/historyapprovedot"><i class="fa fa-circle-thin"></i>ประวัติการอนุมัติค่าล่วงเวลา</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/payroll/makesalary"><i class="fa fa-hand-o-right"></i>สร้างข้อมูลเงินเดือน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/payroll/validatesalary"><i class="fa fa-hand-o-right"></i>ตรวจสอบข้อมูลเงินเดือน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/payroll/approvedsalary"><i class="fa fa-hand-o-right"></i>ยืนยันการจ่ายเงินเดือน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/payroll/adjustsalary"><i class="fa fa-hand-o-right"></i>ปรับปรุงการจ่ายเงินเดือนรายบุคคล &nbsp;</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>ส่งออกข้อมูล</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/payrollexport/exporttobank"><i class="fa fa-circle-thin"></i>ข้อมูลส่งธนาคาร</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payrollexport/exporttosso"><i class="fa fa-circle-thin"></i>ข้อมูลส่งประกันสังคม</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payrollexport/exporttorevenue"><i class="fa fa-circle-thin"></i>ข้อมูลส่งสรรพากร</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payrollexport/exporttorevenue"><i class="fa fa-circle-thin"></i>ข้อมูลส่งทวิ 50</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i>ออกรายงาน &nbsp;</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/payrollreport/payslip"><i class="fa fa-circle-thin"></i>สลิปเงินเดือน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payrollreport/account"><i class="fa fa-circle-thin"></i>บัญชี/การเงิน</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ประเมินและปรับตำแหน่ง <b class="caret"></b></a>
                        <!--<ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>-->
                    </li>

                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ข้อมูลลงเวลา <b class="caret"></b></a>
                        <!--<ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>-->
                    </li>

                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">เครื่องมือและการตั้งค่า <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-hand-o-right"></i> ตั้งค่าข้อมูลองค์กร &nbsp;</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo SITE_URL;?>/orgmaster/index"><i class="fa fa-circle-thin"></i>ตั้งค่าทั่วไป</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payrollmaster/index"><i class="fa fa-circle-thin"></i>ตั้งค่าทำงานล่วงเวลา (โอที)</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/payrollseting/index"><i class="fa fa-circle-thin"></i>ตั้งค่าข้อมูลการจ่ายเงิน/การหักเงิน</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/configpersonalgeneral/index"><i class="fa fa-circle-thin"></i>ตั้งค่าข้อมูลพื้นฐานส่วนบุคคล</a></li>
                                    <li><a href="<?php echo SITE_URL;?>/configssomaping/index"><i class="fa fa-circle-thin"></i>ตั้งการสถานะประกันสังคม</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo SITE_URL;?>/tax/index"><i class="fa fa-hand-o-right"></i> ตั้งค่าข้อมูลภาษี</a></li>
                        </ul>
                    </li>
                </ul>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo Yii::$app->session->get('photo'); ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo Yii::$app->session->get('fullname').'('.Yii::$app->session->get('nickname').')';?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo Yii::$app->session->get('photo'); ?>" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo Yii::$app->session->get('fullname');?> - <?php echo Yii::$app->session->get('positionname');?>

                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="index.php?r=config-hr/viewstaff&id=<?php echo $_SESSION['OBJ_STAFF']->id;?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" id="alogout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <!--<li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>-->
                    </ul>
                </div>
            </div><!--/.nav-collapse -->


        </nav>
    </header>




    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="bb-alert alert alert-success" style="display:none;">
            <span>The examples populate this alert with dummy content</span>
        </div>
        <?php echo $content; ?>
    </div>
    <!-- /.content-wrapper -->
    <!-- =============================================== -->


    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.9.8
        </div>
        <strong>Copyright &copy; 2017 บริษัท อีซูซุเชียงราย จำกัด</strong> All rights reserved.
    </footer>


    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>