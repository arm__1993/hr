<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:11
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPersonal;

$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/employee-lookup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/personalsalary/position.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

// $this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/ot_manageot.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css


?>
<style type="text/css">
      .modal-wide .modal-dialog {
               width: 80%; /* or whatever you wish */
        }
        .col-centered{
            display: block;
            margin-left:0 auto;
            margin-right:0 auto;
            text-align: center;
        }
      .datepicker{z-index:1151 !important;}
}
</style>

    <!-- Default box -->
        <form class="form-horizontal">
            <div class="box box-info">
                  
                        <div class="row showcontant">
                            <div class="col-md-2">
                            &nbsp;
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="SalaryViaBank" class="col-sm-2 control-label">รูปแบบการรับเงิน</label>
                                    <input type="radio" name="SalaryViaBank" id="SalaryViaBank0" value="0" >
                                    <span>เงินสด</span>
                                    <input type="radio" name="SalaryViaBank" id="SalaryViaBank1" value="1"  >
                                    <span>เงินโอน</span>
                                    <input type="radio" name="SalaryViaBank" id="SalaryViaBank3" value="2"  >
                                    <span>พร้อมเพย์</span>
                                </div>
                            </div>
                        </div>
                        <div class="row showcontant form-inline align-text-center col-centered" >
                            <div class="form-group col-centered col-md-1"> &nbsp;</div>
                            <div class="form-group col-centered col-md-4" id='form_group1'>
                                <label for="Salary_Bank" class="col-sm-3 control-label">ธนาคาร:</label>
                                <div class="col-sm-2">
                                    <!-- <input type="text" class="form-control" id="Salary_Bank" > -->
                                    <select class="form-control" name="Salary_Bank" id="Salary_Bank" >
                                        <option value=""> เลือกแผนก</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-centered col-md-4" id='form_group2'>
                                <label for="Salary_BankNo" class="col-sm-3 control-label">หมายเลขบัญชี:</label>
                                <div class="col-sm-2">
                                     <input type="text" class="form-control" id="Salary_BankNo" data-inputmask="'mask': ['999-9-99999-9']" data-mask > 
                                    
                                </div>
                            </div>
                            <div class="form-group col-centered col-md-12 form-inline" id='form_group3'>
                                <div class='row'>
                                    <div class=" col-md-3 form-group">
                                        <input type="radio" name="SalaryViaBankpn" id="SalaryViaBanktal" value="0" >
                                        <span>เบอร์โทรศัพท์</span>
                                        <input type="radio" name="SalaryViaBankpn" id="SalaryViaBankpn" value="1"  >
                                        <span>เลขบัตรประจำตัวประชาชน</span>
                                    </div>
                                </div>
                                <div class='row'>
                                <label for="Salary_PromptPay" id="Salary_PromptPay_lbl" class="col-sm-3 control-label">หมายเลขพร้อมเพย์:</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="Salary_PromptPay-tal" data-inputmask="'mask': ['9999999999']" data-mask>
                                    <input type="text" class="form-control" id="Salary_PromptPay-pn" data-inputmask="'mask': ['9-9999-99999-9-99']" data-mask>
                                </div>
                                </div>
                            </div>
                            <div class="form-group col-centered col-md-1"> &nbsp;</div>                            
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                &nbsp;
                            </div>
<!--                            <div class="col-md-3 showcontant" >-->
<!--                              <button type="button" class="btn btn-success " id="SaveSalaryBank" >บันทึก</button>-->
<!--                            </div>-->
                        </div>
                        <div align="right"> 
                            <button type="button" class="btn btn-primary showcontant"  data-toggle="modal" data-target="#modalAdd" onclick="reset()" style="display: none;" id="addSalary">เพิ่มข้อมูล</button>
                        </div>
                            <div class="modal fade modal-wide" id="modalAdd"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 id="myModalLabel" class="modal-title">จัดการตำแหน่งและเงินเดือน</h4>
                                        </div>
                                        <form action="#" method="post" id='modalfromAdd'>

                                            <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                            <input type="hidden" id="IdCardEmp">
                                            <input type="hidden" id="salarystepstartid">

                                            <div class="modal-body" width="100%" class="table table-bordered table-hover dataTable">
                                                <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label">บริษัท</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" name="selectworking" id="selectworking"
                                                                onchange="getCompanyForDepartment(this);" ;>
                                                            <option value="">เลือกบริษัท</option>
                                                            <?php $working = ApiHr::getWorking_company();
                                                            foreach ($working as $value) {
                                                                echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                            } ?>
                                                        </select>
                                                        <span id="urlGetDepartment" title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                                                    </div> 
                                                <label for="genderEmp" class="col-sm-1 control-label">วันที่มีผล</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="datapicker"  class="datepicker" readonly>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label">แผนก</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" name="selectdepartment" id="selectdepartment"
                                                                onchange="getDepartmentForSection(this);">
                                                            <option value=""> เลือกแผนก</option>
                                                        </select>
                                                        <span id="urlGetSection" title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                                                    </div> 
                                                <label for="genderEmp" class="col-sm-1 control-label">ลักษณะการเปลี่ยนแปลง</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" id="selectsalarychangetype">
                                                            <option value='0'>เลือกลักษณะการเปลี่ยนแปลง</option>
                                                            <?php  $modelResultchange = ApiPersonal::configchangesalary();
                                                                foreach($modelResultchange as $key => $value){
                                                            ?>
                                                            <option value="<?php echo $key ;?>"><?php echo $value ;?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label">ฝ่าย</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" name="selectsection" id="selectsection" onchange="selectSection(this)">
                                                            <option value=""> เลือกฝ่าย</option>
                                                        </select>
                                                    </div> 
                                                <label for="genderEmp" class="col-sm-1 control-label">ผังเงินเดือน</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" id="salarystepchartname" onchange="selectSalarystepchartname(this)">
                                                            <option>เลือกผังเงินเดือน</option>
                                                            <?php  $modelStepSalary = ApiPersonal::getsalarychart();
                                                                foreach($modelStepSalary as $valueStepSalary){
                                                            ?>
                                                            <option value="<?php echo $valueStepSalary['SALARY_STEP_CHART_NAME'] ;?>"><?php echo $valueStepSalary['SALARY_STEP_CHART_NAME'];?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label">ตำแหน่งงาน</label>
                                                    <div class="col-sm-3">
                                                        <select  class="form-control" id="selectposition">
                                                        <option>เลือกตำแหน่งงาน</option>
                                                        </select>
                                                    </div> 
                                                <label for="genderEmp" class="col-sm-1 control-label">ลงกระบอก</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" id="salarysteplevel" onchange="selectSalarysteplevel(this)">
                                                            <option>เลือกกระบอก</option>
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-3">
                                                       
                                                    </div> 
                                                <label for="genderEmp" class="col-sm-1 control-label">ขั้น</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" id="empsalarystep" onchange="selectStep(this)" >
                                                            <option>เลือกขั้น</option>
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-3">
                                                        
                                                    </div> 
                                                <label for="genderEmp" class="col-sm-1 control-label">ขั้นที่เพิ่ม</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" id='stapupnew' class="form-control" >
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-3">

                                                    </div> 
                                                <label for="genderEmp" class="col-sm-1 control-label">เงินเดือน</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="salarystepstartsalary" readonly >
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" onclick="submitAddsalary()" >บันทึกข้อมูล</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->  
                        <br>
                       <table class="table table-bordered table-hover dataTable showcontant" style="display: none;">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>เลือกจ่ายประกันสังคม</th>
                                    <th>เลือกตำแหน่งหลัก</th>
                                    <th>เลือกจ่ายเงินเดือน</th>
                                    <th>บริษัทสังกัด</th>
                                    <th>รหัสตำแหน่ง</th>
                                    <th>ตำแหน่ง</th>
                                    <th>ระยะเวลาดริ่มการใช้งาน</th>
                                    <th>ผังเงินเดือน</th>
                                    <th>กระบอก</th>
                                    <th>ขั้น</th>
                                    <th>เงินเดือน</th>
                                    <th>สถานะ</th>
                                    <th colspan="2">การจัดการ</th>
                                </tr>
                            </thead>
                            <tbody id="showsalaryemp">
                            </tbody>
                        </table>

                    </div>
                        <div class="row" style="float: right;">
                            <div class="col-md-3 showcontant" >
                                <button type="button" class="btn btn-success " id="SaveSalaryBank" >บันทึก</button>
                            </div>
                        <div>
                </div>
            </div>
            </form>

  
    <!-- /.box -->
