<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 17/10/2559
 * Time: 11:03
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;


$objPHPExcel = new \PHPExcel();
$date_rpt = date('Ymd');

$session = Yii::$app->session;

$totalParts = $session->get('totalParts');

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('AngsanaUPC')
    ->setSize(16);

// Add some data  วิว


$objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
$objWorkSheet->setCellValue('A1', 'เลขใบงาน');
$objWorkSheet->setCellValue('B1', ' รุ่นรถ');
$objWorkSheet->setCellValue('C1', 'ทะเบียน');
$objWorkSheet->setCellValue('D1', 'เลขเครื่อง');
$objWorkSheet->setCellValue('E1', 'เบอร์อะไหล่');
$objWorkSheet->setCellValue('F1', 'ชื่ออะไหล่');
$objWorkSheet->setCellValue('G1', 'จำนวน');
$objWorkSheet->setCellValue('H1', 'ราคาขาย');
$objWorkSheet->setCellValue('I1', 'ราคาทุน');
$objWorkSheet->setCellValue('J1', 'กำไร');

$_srow = 2; // เริ่มใส่ข้อมูลบรรทัดที่ 2
$i = 0;
$value = count($totalParts);

for($_srow=2;$_srow<=$value;$_srow++)
{
    $persen = ($totalParts[$i]['sum_novatcap']>0) ? (($totalParts[$i]['receive']-$totalParts[$i]['sum_novatcap'])*100)/$totalParts[$i]['sum_novatcap'] : 0;
    $objWorkSheet->setCellValue('A' . $_srow, ($totalParts[$i]['ref_id']) ? ($totalParts[$i]['ref_id']) : '-');
    $objWorkSheet->setCellValue('B' . $_srow, ($totalParts[$i]['model_name']) ? ($totalParts[$i]['model_name']) : '-');
    $objWorkSheet->setCellValue('C' . $_srow, ($totalParts[$i]['register']) ? ($totalParts[$i]['register']) : '-');
    $objWorkSheet->setCellValue('D' . $_srow, ($totalParts[$i]['engin_id']) ? ($totalParts[$i]['engin_id']) : '-');
    $objWorkSheet->setCellValue('E' . $_srow, ($totalParts[$i]['pw_code']) ? ($totalParts[$i]['pw_code']) : '-');
    $objWorkSheet->setCellValue('F' . $_srow, ($totalParts[$i]['pw_name']) ? ($totalParts[$i]['pw_name']) : '-');
    $objWorkSheet->setCellValue('G' . $_srow, ($totalParts[$i]['unit']) ? ($totalParts[$i]['unit']) : '-');
    $objWorkSheet->setCellValue('H' . $_srow, ($totalParts[$i]['receive']) ? ($totalParts[$i]['receive']) : '-');
    $objWorkSheet->setCellValue('I' . $_srow, ($totalParts[$i]['sum_novatcap']) ? ($totalParts[$i]['sum_novatcap']) : '-');
    $objWorkSheet->setCellValue('J' . $_srow, Helper::displayDecimal($persen).'%');
    $i++;
}




// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('รายงานกำไรขาดทุนขายอะไหล่');//ชื่อในไฟล
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel2007)
foreach(range('A','B') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}
foreach(range('C','D') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}
foreach(range('E','F') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}
foreach(range('G','H') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}
foreach(range('I','J') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="รายงานกำไรขาดทุนขายอะไหล่'.$date_rpt.'.xlsx"');//=ชื่อไฟล์
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
<?php
session_start();
session_destroy();
?>