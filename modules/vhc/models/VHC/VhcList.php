<?php

namespace app\modules\vhc\models\VHC;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_list".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $list_name
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcList extends MasterModel
{
    /**
     * @inheritdoc
     */
    protected $_pageSize;
    public static function tableName()
    {
        return 'vhc_list';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'list_name', 'record_status'], 'required'],
            [['group_id', 'record_status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['list_name'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'list_name' => 'List Name',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
    public function searcha($params)
    {

        $query = VhcList::find()->where(['!=','record_status','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(
                                ['like', 'list_name', $this->list_name]
                                );

        return $dataProvider;
    }


    public function search($params)
    {
        $data = VhcList::find() ->where('record_status <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','group_id',$this->group_id]); //รหัสบริษัท
        $data->andFilterWhere(['like','list_name',$this->list_name]); //	ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
