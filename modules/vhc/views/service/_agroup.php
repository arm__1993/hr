<?php
/* @var $this yii\web\View */



?>
<div>
    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <div style="width: 100%">
                <h2 class="headline text-red text-center" style="font-size: 4em; font-weight: bold;">พบข้อผิดพลาด</h2>
            </div>

            <div style="width: 100%">
                <div class="error-content">
                    <h3><i class="fa fa-warning text-red"></i> Oops! พบข้อผิดพลาดบางประการ </h3>
                    <p style="font-size: 1.2em;">
                        ++ ขอโทษครับ ++ เราเพิ่งพบปัญหาที่คุณกำลังเรียกอยู่นี้
                        คุณสามารถกลับไปยังหน้าแรกของระบบได้ที่นี่ <a href="../../auth/default">return to home</a>
                    </p>

                    <form class="search-form">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" name="submit" class="btn btn-danger btn-flat"><i
                                            class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.input-group -->
                    </form>

                </div>
            </div>


        </div>

        <!-- /.error-page -->

    </section>
    <!-- /.content -->
</div>
