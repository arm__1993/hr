<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "dept_deduct".
 *
 * @property integer $id
 * @property integer $division_id
 * @property integer $dept_id
 * @property integer $company_id
 * @property integer $for_year
 * @property integer $for_month
 * @property string $pay_date
 * @property string $subject
 * @property string $description
 * @property integer $is_approved
 * @property string $approved_datetime
 * @property string $approved_by
 * @property integer $dept_deduct_status
 * @property string $create_by
 * @property string $create_datetime
 * @property string $update_by
 * @property string $update_datetime
 * @property integer $by_dept_id
 * @property string $by_dept_name
 */
class Deptdeduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dept_deduct';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['division_id', 'dept_id', 'company_id', 'for_year', 'for_month', 'is_approved', 'dept_deduct_status', 'by_dept_id'], 'integer'],
            [['description'], 'string'],
            [['approved_datetime', 'create_datetime', 'update_datetime'], 'safe'],
            [['pay_date'], 'string', 'max' => 7],
            [['subject'], 'string', 'max' => 250],
            [['approved_by', 'create_by', 'update_by'], 'string', 'max' => 13],
            [['by_dept_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'division_id' => 'Division ID',
            'dept_id' => 'Dept ID',
            'company_id' => 'Company ID',
            'for_year' => 'For Year',
            'for_month' => 'For Month',
            'pay_date' => 'Pay Date',
            'subject' => 'Subject',
            'description' => 'Description',
            'is_approved' => 'Is Approved',
            'approved_datetime' => 'Approved Datetime',
            'approved_by' => 'Approved By',
            'dept_deduct_status' => 'Dept Deduct Status',
            'create_by' => 'Create By',
            'create_datetime' => 'Create Datetime',
            'update_by' => 'Update By',
            'update_datetime' => 'Update Datetime',
            'by_dept_id' => 'By Dept ID',
            'by_dept_name' => 'By Dept Name',
        ];
    }
}
