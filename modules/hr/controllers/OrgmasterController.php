<?php

namespace app\modules\hr\controllers;

use app\modules\hr\models\BenefitIncomeStructure;
use app\modules\hr\models\OrgConfigmaster;
use app\modules\hr\models\SsoIncomeStructure;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

use yii\helpers\VarDumper;
use app\modules\hr\controllers\MasterController;

//TODO : add data log
//TODO : add permission
//TODO : add check session with permission




class OrgmasterController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;


    /* public function init()
 {
     $session = Yii::$app->session;
     $session->open();
     if ($session->isActive){
         if (!$session->has('USER_ACCOUNT')) {
             return $this->redirect(array('/login/index'),302);
         }
     }
 }*/

    /**
     * function init() check session active or session login, if not redirect to login page
     * @return \yii\web\Response
     */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }



    public function behaviors()
    {
        return [
            'verbs' => [
                'class' =>VerbFilter::className(),
                'actions' => [
                    'savessoincomestructure' =>['post'],
                    'editssoincomestructure'=>['post'],
                    'deletessoincomestructure'=>['post'],
                    'savebenefitincomestructure' =>['post'],
                    'editbenefitincomestructure'=>['post'],
                    'deletebenefitincomestructure'=>['post'],
                ],
            ],
        ];
    }





    public function actionIndex()
    {
        $OrgConfigMaster = OrgConfigmaster::findOne(1);
        if($OrgConfigMaster===null) {
            $OrgConfigMaster = new OrgConfigmaster();
        }

        $SsoIncomeStructure = new SsoIncomeStructure();
        $SsoIncomeProvider = $SsoIncomeStructure->search(Yii::$app->request->queryParams);

        $BenefitIncomeStructure = new BenefitIncomeStructure();
        $BenefitIncomeProvider = $BenefitIncomeStructure->search(Yii::$app->request->queryParams);



        return $this->render('index',[
            'OrgConfigMaster'=>$OrgConfigMaster,
            'SsoIncomeStructure'=>$SsoIncomeStructure,
            'SsoIncomeProvider'=>$SsoIncomeProvider,

            'BenefitIncomeStructure'=>$BenefitIncomeStructure,
            'BenefitIncomeProvider'=>$BenefitIncomeProvider,

        ]);
    }


    public function actionSaveorgconfig()
    {

        $model = OrgConfigmaster::findOne(1);
        if($model===null) {
            $model = new OrgConfigmaster();
        }

        //VarDumper::dump($model->attributeLabels());
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            echo 1;
            //Yii::$app->response->format = trim(Response::FORMAT_JSON);
            //return $result;
        }else{
            $error = \yii\widgets\ActiveForm::validate($model);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            print_r($error);
        }

    }


    public function actionSavessoincomestructure()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            if(!empty($postValue['to_year']) &&  ($postValue['to_year'] < $postValue['from_year'])) {
             echo 0;  //invalid
            }

            $_ID = $postValue['hide_ssostructureedit'];
            if(!empty($_ID) && $_ID > 0)
            {
                $model = SsoIncomeStructure::findOne($_ID);
                $model->income_floor = $postValue['income_floor'];
                $model->income_ceil  = $postValue['income_ceil'];
                $model->sso_rate    = $postValue['sso_rate'];
                $model->is_calculate_percent = $postValue['is_calculate_percent'];
                $model->from_year   = $postValue['from_year'];
                $model->to_year     = $postValue['to_year'];
                $model->remark      = $postValue['remark'];
                $model->status_active = $postValue['ssoincome_status'];
                echo $model->save();
            }
            else
            {
                $model = new SsoIncomeStructure();
                $model->income_floor = $postValue['income_floor'];
                $model->income_ceil  = $postValue['income_ceil'];
                $model->sso_rate    = $postValue['sso_rate'];
                $model->is_calculate_percent = $postValue['is_calculate_percent'];
                $model->from_year   = $postValue['from_year'];
                $model->to_year     = $postValue['to_year'];
                $model->remark      = $postValue['remark'];
                $model->status_active = $postValue['ssoincome_status'];
                echo $model->save();
            }
            //exit;
        }
    }

    public function actionEditssoincomestructure()
    {
        if (Yii::$app->request->isAjax) {
            $ID = Yii::$app->request->post('id');
            $model = SsoIncomeStructure::find()->where(['id'=>$ID])->asArray()->one();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeletessoincomestructure()
    {
        $statusModel = null;
        if (Yii::$app->request->isAjax) {
            $ID = Yii::$app->request->post('id');
            $model = SsoIncomeStructure::findOne($ID);
            $model->status_active = 99;
            $statusModel = $model->update();
        }
        return $statusModel;
    }


    public function actionSavebenefitincomestructure()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            if(!empty($postValue['to_year']) &&  ($postValue['to_year'] < $postValue['from_year'])) {
                echo 0;  //invalid
            }

            $_ID = $postValue['hide_benefitstructureedit'];
            if(!empty($_ID) && $_ID > 0)
            {
                $model = BenefitIncomeStructure::findOne($_ID);
                $model->workage_floor = $postValue['workage_floor'];
                $model->workage_ceil  = $postValue['workage_ceil'];
                $model->benefit_rate    = $postValue['benefit_rate'];
                $model->from_year   = $postValue['be_from_year'];
                $model->to_year     = $postValue['be_to_year'];
                $model->remark      = $postValue['be_remark'];
                $model->status_active = $postValue['benefitincome_status'];
                echo $model->save();
            }
            else
            {
                $model = new BenefitIncomeStructure();
                $model->workage_floor = $postValue['workage_floor'];
                $model->workage_ceil  = $postValue['workage_ceil'];
                $model->benefit_rate    = $postValue['benefit_rate'];
                $model->from_year   = $postValue['be_from_year'];
                $model->to_year     = $postValue['be_to_year'];
                $model->remark      = $postValue['be_remark'];
                $model->status_active = $postValue['benefitincome_status'];
                echo $model->save();
            }
            //exit;
        }
    }

    public function actionEditbenefitincomestructure()
    {
        if (Yii::$app->request->isAjax) {
            $ID = Yii::$app->request->post('id');
            $model = BenefitIncomeStructure::find()->where(['id'=>$ID])->asArray()->one();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeletebenefitincomestructure()
    {
        $statusModel = null;
        if (Yii::$app->request->isAjax) {
            $ID = Yii::$app->request->post('id');
            $model = BenefitIncomeStructure::findOne($ID);
            $model->status_active = 99;
            $statusModel = $model->update();
        }
        return $statusModel;
    }
}
