<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/16/2017 AD
 * Time: 13:51
 */

namespace app\modules\hr\apihr;

use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\Department;
use app\modules\hr\models\OtConfigformular;
use app\modules\hr\models\OtConfigtimetable;
use app\modules\hr\models\OtRequestdetail;
use app\modules\hr\models\OtRequestmaster;
use app\modules\hr\models\OtReturnBenefit;
use app\modules\hr\models\Section;
use app\modules\hr\models\Workingcompany;
use Yii;
use app\modules\hr\models\OtActivity;
use yii\helpers\ArrayHelper;
use app\api\DBConnect;

class ApiOT
{


/*    public static function getOTActivity()
    {
        $model = OtActivity::find([
            'status_active'=>Yii::$app->params['ACTIVE_STATUS'],
        ])->all();
        $arrList = [];
        foreach ($model as $item) {
            $arrList[$item->id] = $item;
        }
        return $arrList;
    }*/


    public static function distanceFormular($km)
    {
        $ot_multiply = 40;
        $velocity = 90;
        return ($km*$ot_multiply)/$velocity;
    }

    public static function timeFormular($end,$start)
    {
        $tmp = ($end-$start)/60;
        return floor($tmp/30);
    }


    public static function calculateDistanceRule($OtMaster,$OtDetail,$myWorkHour)
    {
        $myDistanceMoney = 0;
        $ot_service_twoway = 50;
        $ot_thirtymin_value = 20;

        $workstart_time = strtotime($myWorkHour['start_time']);
        $workend_time = strtotime($myWorkHour['end_time']);
        $ot_starttime = strtotime($OtDetail['time_start']);
        $ot_endtime = strtotime($OtDetail['time_end']);

        //วันปกติ : ขาไปคิด 30 นาทีละ 20 บาท, Type=1
        if(($OtMaster['profile_route_id']==1 || $OtMaster['profile_route_id']==3) && ($ot_starttime < $workstart_time)){
            $myDistanceMoney = self::timeFormular($workstart_time,$ot_starttime) * $ot_thirtymin_value;
        }


        //วันปกติ : ขากลับคิด 30 นาทีละ 20 บาท, Type=2
        if(($OtMaster['profile_route_id']==2 || $OtMaster['profile_route_id']==3) && ($ot_starttime >= $workend_time)) {
            $myDistanceMoney = self::timeFormular($ot_endtime,$workend_time) * $ot_thirtymin_value;
        }


        //วันปกติ : ขาไปและกลับคิด 30 นาทีละ 20 บาท , Type=3
        //if($OtMaster['profile_route_id']==3){
           // echo 'c';
            //$go = self::timeFormular($workstart_time,$ot_starttime) * $ot_thirtymin_value;
            //$back = $myDistanceMoney = self::timeFormular($ot_endtime,$workend_time) * $ot_thirtymin_value;
            //$myDistanceMoney = $go+$back;
        //}

        //นอกเวลา : ลูกค้าเข้ารับบริการที่บริษัท (+50 บาท) , Type=4
        if($OtMaster['profile_route_id']==4){
            $myDistanceMoney = $ot_service_twoway;
        }

        //นอกเวลา : หลัง 17:00 น. , Type=5
        if($OtMaster['profile_route_id']==5){
            $myDistanceMoney = self::distanceFormular($OtMaster['distance_amount']);
        }

        //นอกเวลา : หลัง 18:00 น. (+50 บาท) , Type=6
        if($OtMaster['profile_route_id']==6){
            $myDistanceMoney = self::distanceFormular($OtMaster['distance_amount']) + $ot_service_twoway;
        }

        return $myDistanceMoney;
    }



    public static function showTextUnderline($text)
    {
        return ($text !='') ? '<span class="text_underline">'.$text.'</span>' : '';
    }

    public static function getArrayCompany()
    {
        $data = Workingcompany::find()
            ->where(['status'=>1])
            ->asArray()
            ->all();
        $arrCompany = [];
        foreach ($data as $item) {
            $arrCompany[$item['id']] = $item;
        }

        return $arrCompany;
    }

    public static function getArrayDepartment()
    {
        $data = Department::find()
            ->where(['status'=>1])
            ->asArray()
            ->all();
        $arrDepartment = [];
        foreach ($data as $item) {
            $arrDepartment[$item['company']][$item['id']] = $item;
        }

        return $arrDepartment;
    }

    public static function getArrayDeductTemplate($type="all")
    {
        $arrTemplateID = [];
        $model = null;
        if($type =='all') {
            $model = Adddeducttemplate::find()->where([
                'ADD_DEDUCT_TEMPLATE_STATUS' => '1',
            ])->asArray()->all();
        }
        else {
            $model = Adddeducttemplate::find()->where([
                'ADD_DEDUCT_TEMPLATE_STATUS' => '1',
                'ADD_DEDUCT_TEMPLATE_TYPE' => $type,
            ])->asArray()->all();
        }

        foreach ($model as $item) {
            $arrTemplateID[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item['ADD_DEDUCT_TEMPLATE_NAME'];
        }
        return $arrTemplateID;
    }

    public static function mapOTstatus($status)
    {
       return ($status==1) ?  '<span class="label label-success">อนุมัติแล้ว</span>' : (($status==2) ? '<span class="label label-danger"> ให้แก้ไข </span>' : '<span class="label label-warning"> รออนุมัติ </span>')  ;
    }

    public static function mapApprovedAll()
    {
        return '<span class="label label-success">อนุมัติ</span>';
    }

    public static function mapApprovedPatial($number)
    {
        return '<span class="label label-success">อนุมัติ ('.$number.')</span>';
    }

    public static function mapRejectedPatial($number)
    {
        return '<span class="label label-danger">ไม่อนุมัติ ('.$number.')</span>';
    }

    public static function getUserEmpIDApprovedOT()
    {
        $EmpID = OtRequestmaster::find()->select('approved_byuser')->distinct()->asArray()->all();
        $arrEmpID = [];
        foreach($EmpID as $id) {
            if($id['approved_byuser'] !='') array_push($arrEmpID,$id['approved_byuser']);
        }
        return $arrEmpID;
    }

    public static function mapUserApprovedFullname()
    {
        //get user whose approved 
        $arrUserApprovedID = self::getUserEmpIDApprovedOT();
        $arrUserFullNameApproved = ApiHr::getEmpNameForCreateByInIdcard($arrUserApprovedID);
        $arrUserFullName = [];
        foreach($arrUserFullNameApproved as $item) {
            $arrUserFullName[$item['ID_Card']] = $item;
        }
        return $arrUserFullName;
    }



    public static function getOTActivity()
    {
        $model = OtActivity::find([
            'status_active'=>Yii::$app->params['ACTIVE_STATUS'],
        ])->all();

        return ArrayHelper::map($model,'id','activity_name');
    }


    public static function getOTYear()
    {
        $sql = "SELECT DISTINCT(YEAR(activity_date)) as years  FROM ot_requestmaster ";
        $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
        $arrYear = [];
        foreach ($model as $item) {
            $arrYear[$item['years']] = $item['years'];
        }
        return $arrYear;
    }


    public static function getOTProfileRoute()
    {
        $model = OtConfigtimetable::find([
            'status_active'=>Yii::$app->params['ACTIVE_STATUS'],
        ])->all();

        return ArrayHelper::map($model,'id','profile_name');
    }

    public static function getOTReturnBenefit()
    {
        $model = OtReturnBenefit::find([
            'status_active'=>Yii::$app->params['ACTIVE_STATUS'],
        ])->all();

        return ArrayHelper::map($model,'id','return_name');
    }



    public static function getOTConfig()
    {
        $model = OtConfigformular::findOne(1);
        return ($model === null) ? null : $model;
    }



    public static function  getOTFormat()
    {
        //$fi = BCompany::findOne(['id'=>1]);
        return 'OT';
    }

    public static function  getLastRequestMaster()
    {
        //$max = BEmployee::find() // AQ instance
        //->select('max(id)') // we need only one column
        //->scalar(); // cool, huh?
        // return  ($max==null || $max==0 || $max=='') ? 1 : $max+1;

        $m = OtRequestmaster::find()
            ->orderBy(['id'=>SORT_DESC,])
            ->one();
        if($m==null) return 1;
        else {
            return $m->request_no;
        }
    }

    public static function revertOTRequestNo($code,$prefix)
    {
        $len = strlen($prefix);
        if($len > 0)
        {
            $text = substr($code,$len);
            $tmp = (int)$text;
            return $tmp+1;
        }
        else {
            $tmp = (int)$code;
            return $tmp+1;
        }
    }


    public static function  buildOTRequestNo()
    {
        $format = self::getOTFormat();
        $maxid = self::getLastRequestMaster();
        $nextID = self::revertOTRequestNo($maxid,$format);
        $strID = str_pad($nextID, 5, '0', STR_PAD_LEFT);
        $extID = $format.$strID;
        return $extID;
    }


    /**
     * @param $time eg  18:35:59, out put : 18:35
     */
    public static function showTime($time)
    {
        return substr($time,0,5);
    }

    public static function getArrayEmpData($companyID,$departmentID,$sectionID)
    {
        $dataModel = ApiHr::seachEmpAndCompany($companyID,$departmentID,$sectionID);
        $arrEmp = [];
        foreach ($dataModel as $data) {
            $arrEmp[$data['ID_Card']]['name'] = $data['Fullname'];
            $arrEmp[$data['ID_Card']]['deptname'] = $data['DepartmentName'];
        }

        return $arrEmp;
    }

    public static function getArrayEmpDatawithRequestID($ReqMasterID)
    {
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $sql="SELECT $db[ct].emp_data.ID_Card as ID_Card, CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname 
        FROM $db[ct].emp_data 
        INNER JOIN $db[pl].ot_requestdetail ON $db[ct].emp_data.ID_Card = $db[pl].ot_requestdetail.id_card
        WHERE $db[pl].ot_requestdetail.ot_requestmaster_id = $ReqMasterID ORDER BY ID_Card ASC";
        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();
        $arrEmp = [];
        foreach ($data as $item)  $arrEmp[$item['ID_Card']] = $item['Fullname'];

        return $arrEmp;
    }



}