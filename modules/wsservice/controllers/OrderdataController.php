<?php

namespace app\modules\wsservice\controllers;

use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use app\modules\wsservice\models\OrderData;
use yii\web\Controller;

class OrderdataController extends \yii\web\Controller
{
    // public function actionIndex()
    // {
    //     return $this->render('index');
    // }

    public $modelClass = 'app\modules\wsservice\models\OrderData';
	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}

	public function actionSearchme()
    {
        $f = \Yii::$app->request->get('f');
        $book = new OrderData();
        $bookSearch = $book->searchbytitle($f);
        return $bookSearch;
    }


}
