<?php
namespace app\modules\hr\apihr;
use yii;
use app\modules\hr\models\SsoPaidHead;
use app\modules\hr\models\SsoPaidDetail;
use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Wagehistory;
use yii\helpers\VarDumper;

Class ApiPreexportSSO
{
    private $arrayWagehistory = [];
    private $array_company = [];
    private $obj_ssoexport = [];
    private $obj_adddeducthistiry = [];
    private $wages = [];
    private $total_wages = 0;
    private $array_head = [];
    public function setadddeducthistiry($branch,$month,$year)
    {
        $arr = [];
        $adddeducthistiry = Adddeducthistory::find()
        ->join('INNER JOIN', 'WAGE_HISTORY','ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID =WAGE_HISTORY.WAGE_EMP_ID') 
        ->where(['=','ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID',''])
        ->andWhere(['!=','ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS','99'])
        ->andWhere(['=','ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE',$month.'-'.$year])
        ->andWhere(['=','WAGE_HISTORY.WAGE_WORKING_COMPANY',$branch])
        ->asArray()->all();
        foreach($adddeducthistiry as $item){
            $arr[$item['ADD_DEDUCT_THIS_MONTH_EMP_ID']] = $item;
        }
        $this->obj_adddeducthistiry = $arr;
    }
    public function getadddeducthistiry($value)
    {
        $array_ADD_DEDUCT_THIS_MONTH_AMOUNT = [];
        $TOTAL_EMPLOYEE = $TOTAL_PAID =  $TOTAL_PAID_BY_EMPLOYEE = 0;
        foreach($this->obj_adddeducthistiry as $item){
            $array_ADD_DEDUCT_THIS_MONTH_AMOUNT[] =$item['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
        }
        $TOTAL_EMPLOYEE = count($array_ADD_DEDUCT_THIS_MONTH_AMOUNT);
        $TOTAL_PAID_BY_EMPLOYEE = array_sum($array_ADD_DEDUCT_THIS_MONTH_AMOUNT);
        $TOTAL_PAID_BY_EMPLOYER = array_sum($array_ADD_DEDUCT_THIS_MONTH_AMOUNT);
        $this->array_head['TOTAL_EMPLOYEE'] = $TOTAL_EMPLOYEE;
        $this->array_head['TOTAL_PAID'] =$TOTAL_PAID_BY_EMPLOYEE+$TOTAL_PAID_BY_EMPLOYER;
        $this->array_head['TOTAL_WAGES'] = $this->total_wages;
        $this->array_head['TOTAL_PAID_BY_EMPLOYEE'] = $TOTAL_PAID_BY_EMPLOYEE;
        $this->array_head['TOTAL_PAID_BY_EMPLOYER'] = $TOTAL_PAID_BY_EMPLOYER;
        return $this->array_head;

    }
    public function getadddeducthistiryASdetail()
    {
        return $this->obj_adddeducthistiry;

    }
    public function setCompany()
    {
        $arr = [];
        $Workingcompany = Workingcompany::find()
        ->where(['!=','status','99'])
        ->asArray()->all();
        foreach($Workingcompany as $item){
            $arr[$item['id']]=$item;
        }
        $this->array_company=$arr;
    }
    public function getCompany($WAGE_WORKING_COMPANY){
        return $this->array_company[$WAGE_WORKING_COMPANY];
    }
    public function setWagehistory($branch,$month,$year)
    {
        $array_Wagehistory = [];
        $Wagehistory = Wagehistory::find()
        ->select('WAGE_HISTORY.*,emp_data.Be,emp_data.Name,emp_data.Surname')
        ->join('INNER JOIN','emp_data','WAGE_HISTORY.WAGE_EMP_ID=emp_data.ID_Card')
        ->where(['!=','WAGE_THIS_MONTH_STATUS','99'])
        ->andWhere(['=','WAGE_PAY_DATE',$month.'-'.$year])
        ->andWhere(['=','WAGE_WORKING_COMPANY',$branch])
        ->asArray()->all();
        foreach($Wagehistory as $item)
        {
            $this->total_wages += (int)$item['WAGE_SALARY'];
            $array_Wagehistory[$item['WAGE_EMP_ID']]=$item;
            $this->wages[$item['WAGE_EMP_ID']] = $item['WAGE_SALARY'];
        }
        
        $this->arrayWagehistory = $array_Wagehistory;
    }
    public function gettotal_wages()
    {
        return $this->total_wages;
    }
    public function get_wages()
    {
        return $this->total_wages;
    }
     public function getWagehistory()
    {
        return $this->arrayWagehistory;
    }
     public function getWagehistoryByEmp($emp_id)
    {
        return $this->arrayWagehistory[$emp_id];
    }
    public function printarr($value='')
    {
        echo '<pre>';
        print_r($value);
        echo '</pre>';
    }
    
    public static function InseartHead($data)
    {
        $objSsoPaidHead = new SsoPaidHead();
        $objSsoPaidHead->attributes=$data;
        if($objSsoPaidHead->validate()){
            $status = $objSsoPaidHead->save();
            return $objSsoPaidHead->id;
        }else{
            $errors = $objSsoPaidHead->errors;
            echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
        }
        
    }
    public static function InseartDetail($detail)
    {
        $objSsoPaidDetail = new SsoPaidDetail();
        $objSsoPaidDetail->attributes=$detail;
         if($objSsoPaidDetail->validate()){
            $status = $objSsoPaidDetail->save();
        }else{
            $errors = $objSsoPaidDetail->errors;
            echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
        }
        
        
    }
   
}

?>