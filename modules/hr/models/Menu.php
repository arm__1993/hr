<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $place_id
 * @property string $name
 * @property string $menu_desc
 * @property string $menu_image
 * @property string $link
 * @property string $parent_id
 * @property integer $rankking
 * @property string $process_id
 * @property string $status
 * @property integer $menu_manage_permission
 * @property string $create_by
 * @property string $create_date
 * @property string $modify_by
 * @property string $modify_date
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'link'], 'string'],
            [['rankking', 'menu_manage_permission'], 'integer'],
            [['create_date', 'modify_date'], 'safe'],
            [['place_id', 'parent_id', 'process_id', 'create_by', 'modify_by'], 'string', 'max' => 20],
            [['menu_desc', 'menu_image'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place_id' => 'Place ID',
            'name' => 'Name',
            'menu_desc' => 'Menu Desc',
            'menu_image' => 'Menu Image',
            'link' => 'Link',
            'parent_id' => 'Parent ID',
            'rankking' => 'Rankking',
            'process_id' => 'Process ID',
            'status' => 'Status',
            'menu_manage_permission' => 'Menu Manage Permission',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'modify_by' => 'Modify By',
            'modify_date' => 'Modify Date',
        ];
    }
}
