<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "menu_process".
 *
 * @property integer $id
 * @property string $name
 * @property string $process
 * @property string $re_mark
 * @property string $status
 */
class MenuProcess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_process';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'process', 're_mark'], 'string'],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'process' => 'Process',
            're_mark' => 'Re Mark',
            'status' => 'Status',
        ];
    }
}
