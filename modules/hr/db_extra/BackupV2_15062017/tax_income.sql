-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 15, 2017 at 10:56 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_income`
--

CREATE TABLE `tax_income` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `tax_personal` decimal(10,2) DEFAULT NULL COMMENT 'เบี้ยประกันชีวิตตัวเอง',
  `tax_keepback` decimal(10,2) DEFAULT NULL COMMENT 'เงินสะสมกองทุนสำรองเลี้ยงชีพ',
  `tax_rating` decimal(10,2) DEFAULT NULL COMMENT 'เงินสะสม กบข',
  `tax_teachers` decimal(10,2) DEFAULT NULL COMMENT 'เงินสะสมกองทุนสงเคราะห์ครูเอกชน',
  `tax_rmf` decimal(10,2) DEFAULT NULL COMMENT 'กองทุน RMF',
  `tax_ltf` decimal(10,2) DEFAULT NULL COMMENT 'กองทุน LTF',
  `tax_increase_home` decimal(10,2) DEFAULT NULL COMMENT 'ดอกเบี้ยเงินกู้เพื่อที่อยู่อาศัย',
  `tax_social` decimal(10,2) DEFAULT NULL COMMENT 'กองทุนประกันสังคม',
  `tax_education` decimal(10,2) DEFAULT NULL COMMENT 'เงินสนับสนุนเพื่อการศึกษา',
  `tax_income_spouse_status` tinyint(3) DEFAULT NULL COMMENT '1:คู่สมรสมีเงินได้, 2:คู่สมรสไม่มีเงินได้',
  `tax_income_with_spouse_status` tinyint(3) DEFAULT NULL COMMENT '1.ยื่นร่วมกับคู่สมรส, 2.แยกยื่นแบบกับคู่สมรส',
  `tax_income_spouse` decimal(10,2) DEFAULT NULL COMMENT 'รายได้คู่สมรส',
  `tax_insurance_spouse` decimal(10,2) DEFAULT NULL COMMENT 'เบี้ยประกันชีวิตคู่สมรส',
  `tax_income_children_status` tinyint(3) DEFAULT NULL COMMENT '1:บุตรศึกษา , 2:บุตรไม่ศึกษา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_income`
--
ALTER TABLE `tax_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_income`
--
ALTER TABLE `tax_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
