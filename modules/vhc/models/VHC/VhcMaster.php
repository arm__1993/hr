<?php

namespace app\modules\vhc\models\VHC;

use Yii;

/**
 * This is the model class for table "vhc_master".
 *
 * @property integer $id
 * @property string $vhc_date
 * @property string $vhc_time
 * @property integer $ahistory_id
 * @property integer $mile_in
 * @property string $running_queue
 * @property integer $vhc_sevice_id
 * @property integer $vhc_model_id
 * @property integer $cus_id
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcMaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_master';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vhc_date', 'vhc_time', 'ahistory_id', 'mile_in', 'running_queue', 'vhc_sevice_id', 'vhc_model_id', 'cus_id', 'status'], 'required'],
            [['vhc_date', 'vhc_time', 'create_date', 'update_date'], 'safe'],
            [['ahistory_id', 'mile_in', 'vhc_sevice_id', 'vhc_model_id', 'cus_id', 'status'], 'integer'],
            [['running_queue'], 'string', 'max' => 10],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vhc_date' => 'Vhc Date',
            'vhc_time' => 'Vhc Time',
            'ahistory_id' => 'Ahistory ID',
            'mile_in' => 'Mile In',
            'running_queue' => 'Running Queue',
            'vhc_sevice_id' => 'Vhc Sevice ID',
            'vhc_model_id' => 'Vhc Model ID',
            'cus_id' => 'Cus ID',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
