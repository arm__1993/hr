-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 03, 2017 at 11:55 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-10+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_promise_mapping_condition`
--

CREATE TABLE `emp_promise_mapping_condition` (
  `id` int(11) NOT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `param_name` varchar(100) DEFAULT NULL,
  `operation_name` varchar(100) DEFAULT NULL,
  `default_value` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emp_promise_mapping_condition`
--

INSERT INTO `emp_promise_mapping_condition` (`id`, `field_name`, `param_name`, `operation_name`, `default_value`) VALUES
(1, 'DataNo', 'emp_id', '=', NULL),
(12, 'emp_data_id', 'emp_id', '=', NULL),
(13, 'main_active', NULL, '=', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_promise_mapping_condition`
--
ALTER TABLE `emp_promise_mapping_condition`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_promise_mapping_condition`
--
ALTER TABLE `emp_promise_mapping_condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
