<?php

namespace app\modules\vhc\models\VHC;

use Yii;

/**
 * This is the model class for table "vhc_sevice_labor".
 *
 * @property integer $id
 * @property integer $vhc_service_id
 * @property integer $wage_id
 * @property integer $crepair_id
 * @property string $name
 * @property string $price
 * @property string $vat
 * @property string $invatprice
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcSeviceLabor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_sevice_labor';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vhc_service_id', 'wage_id', 'crepair_id', 'name', 'price', 'vat', 'invatprice', 'status'], 'required'],
            [['vhc_service_id', 'wage_id', 'crepair_id', 'status'], 'integer'],
            [['price', 'vat', 'invatprice'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vhc_service_id' => 'Vhc Service ID',
            'wage_id' => 'Wage ID',
            'crepair_id' => 'Crepair ID',
            'name' => 'Name',
            'price' => 'Price',
            'vat' => 'Vat',
            'invatprice' => 'Invatprice',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
