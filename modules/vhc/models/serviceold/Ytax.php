<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "ytax".
 *
 * @property integer $id
 * @property string $day
 * @property string $datePay
 * @property string $ref_ytax
 * @property string $day_edit
 * @property string $code_name
 * @property string $type_bill
 * @property string $tax_id
 * @property string $invoice
 * @property string $bill
 * @property string $dept_bill
 * @property string $SKB_id
 * @property string $emp_id
 * @property integer $ref_id
 * @property integer $ref_status
 * @property string $NO
 * @property string $MACH_NO
 * @property string $cus_id
 * @property string $cusch_id
 * @property string $typecustomer
 * @property integer $ahistory_id
 * @property string $name
 * @property string $address
 * @property string $tel
 * @property string $priceselldis
 * @property string $novat_price
 * @property string $vat
 * @property string $invat_price
 * @property string $pricewage
 * @property string $priceparts
 * @property string $priceall
 * @property string $diswage
 * @property string $disparts
 * @property string $disall
 * @property string $taxwage
 * @property string $taxparts
 * @property string $taxall
 * @property string $sum_avg_parts
 * @property string $sumwage
 * @property string $sumparts
 * @property string $sumall
 * @property string $tax_status
 * @property string $credit
 * @property integer $creditID
 * @property string $creditNo
 * @property string $creditExp
 * @property string $disprice
 * @property string $ysell_date
 * @property string $ydate_receiveparts
 * @property string $emp_sellpart
 * @property integer $ybranch
 * @property string $timeEdit
 * @property string $empEdit
 * @property integer $sent_acc_status
 * @property integer $sent_revenue_status
 * @property integer $sent_avg_status
 * @property integer $sent_sale_status
 * @property integer $sent_sale_cost
 * @property string $status
 * @property string $statusbill
 * @property string $guarantee
 * @property integer $sendTis
 * @property integer $dateSendTis
 * @property string $ChangeDateIn
 * @property string $ChangeDateOut
 * @property string $tis_part
 * @property string $tis_part_list
 * @property string $tis_all
 * @property string $tis_vat_part
 * @property string $tis_vat_part_list
 * @property string $tis_vat_all
 * @property string $tis_sum_all
 * @property string $tis_dis_part
 * @property string $tis_dis_labour
 * @property string $tis_dis_all
 * @property integer $tis_check_edit
 * @property integer $check_reduce
 * @property integer $StatusAttachCoupon
 * @property string $cusNo
 * @property integer $idRef_vihicle
 * @property integer $Budget
 * @property integer $sent_view_ytax
 * @property string $comp_inv_number
 * @property string $comp_type
 * @property string $comp_branch_id
 * @property string $comp_branch_txt
 * @property string $cus_inv_number
 * @property string $cus_type
 * @property string $cus_comptype
 * @property string $cus_comp_branch_id
 * @property string $cus_comp_branch_txt
 * @property string $refReceiveTis
 * @property integer $xml_notax_run
 * @property integer $status_sent_xml
 */
class Ytax extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ytax';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['day', 'datePay', 'ref_ytax', 'day_edit', 'code_name', 'type_bill', 'tax_id', 'invoice', 'bill', 'dept_bill', 'SKB_id', 'emp_id', 'ref_id', 'ref_status', 'NO', 'MACH_NO', 'cus_id', 'cusch_id', 'typecustomer', 'ahistory_id', 'name', 'address', 'tel', 'priceselldis', 'novat_price', 'vat', 'invat_price', 'pricewage', 'priceparts', 'priceall', 'diswage', 'disparts', 'disall', 'taxwage', 'taxparts', 'taxall', 'sum_avg_parts', 'sumwage', 'sumparts', 'sumall', 'tax_status', 'credit', 'creditID', 'creditNo', 'creditExp', 'disprice', 'ysell_date', 'ydate_receiveparts', 'emp_sellpart', 'ybranch', 'empEdit', 'sent_acc_status', 'sent_revenue_status', 'sent_avg_status', 'sent_sale_status', 'sent_sale_cost', 'status', 'statusbill', 'guarantee', 'sendTis', 'dateSendTis', 'ChangeDateIn', 'ChangeDateOut', 'tis_part', 'tis_part_list', 'tis_all', 'tis_vat_part', 'tis_vat_part_list', 'tis_vat_all', 'tis_sum_all', 'tis_dis_part', 'tis_dis_labour', 'tis_dis_all', 'tis_check_edit', 'check_reduce', 'StatusAttachCoupon', 'cusNo', 'idRef_vihicle', 'Budget', 'sent_view_ytax', 'comp_inv_number', 'comp_type', 'comp_branch_id', 'comp_branch_txt', 'cus_inv_number', 'cus_type', 'cus_comptype', 'cus_comp_branch_id', 'cus_comp_branch_txt', 'refReceiveTis', 'xml_notax_run', 'status_sent_xml'], 'required'],
            [['datePay', 'creditExp', 'timeEdit'], 'safe'],
            [['ref_id', 'ref_status', 'ahistory_id', 'creditID', 'ybranch', 'sent_acc_status', 'sent_revenue_status', 'sent_avg_status', 'sent_sale_status', 'sent_sale_cost', 'sendTis', 'dateSendTis', 'tis_check_edit', 'check_reduce', 'StatusAttachCoupon', 'idRef_vihicle', 'Budget', 'sent_view_ytax', 'xml_notax_run', 'status_sent_xml'], 'integer'],
            [['name', 'address'], 'string'],
            [['priceselldis', 'novat_price', 'vat', 'invat_price', 'pricewage', 'priceparts', 'priceall', 'diswage', 'disparts', 'disall', 'taxwage', 'taxparts', 'taxall', 'sum_avg_parts', 'sumwage', 'sumparts', 'sumall', 'disprice', 'tis_part', 'tis_part_list', 'tis_all', 'tis_vat_part', 'tis_vat_part_list', 'tis_vat_all', 'tis_sum_all', 'tis_dis_part', 'tis_dis_labour', 'tis_dis_all'], 'number'],
            [['day', 'ref_ytax', 'day_edit', 'code_name', 'type_bill', 'tax_id', 'invoice', 'bill', 'dept_bill', 'SKB_id', 'MACH_NO', 'ysell_date', 'ydate_receiveparts', 'empEdit', 'ChangeDateIn', 'ChangeDateOut', 'cusNo', 'comp_inv_number', 'cus_inv_number', 'cus_comp_branch_id', 'refReceiveTis'], 'string', 'max' => 20],
            [['emp_id', 'cus_id'], 'string', 'max' => 13],
            [['NO'], 'string', 'max' => 5],
            [['cusch_id'], 'string', 'max' => 250],
            [['typecustomer', 'tax_status', 'credit'], 'string', 'max' => 1],
            [['tel'], 'string', 'max' => 50],
            [['creditNo'], 'string', 'max' => 150],
            [['emp_sellpart'], 'string', 'max' => 15],
            [['status', 'statusbill', 'guarantee', 'comp_type', 'cus_type', 'cus_comptype'], 'string', 'max' => 2],
            [['comp_branch_id'], 'string', 'max' => 10],
            [['comp_branch_txt', 'cus_comp_branch_txt'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => 'Day',
            'datePay' => 'Date Pay',
            'ref_ytax' => 'Ref Ytax',
            'day_edit' => 'Day Edit',
            'code_name' => 'Code Name',
            'type_bill' => 'Type Bill',
            'tax_id' => 'Tax ID',
            'invoice' => 'Invoice',
            'bill' => 'Bill',
            'dept_bill' => 'Dept Bill',
            'SKB_id' => 'Skb ID',
            'emp_id' => 'Emp ID',
            'ref_id' => 'Ref ID',
            'ref_status' => 'Ref Status',
            'NO' => 'No',
            'MACH_NO' => 'Mach  No',
            'cus_id' => 'Cus ID',
            'cusch_id' => 'Cusch ID',
            'typecustomer' => 'Typecustomer',
            'ahistory_id' => 'Ahistory ID',
            'name' => 'Name',
            'address' => 'Address',
            'tel' => 'Tel',
            'priceselldis' => 'Priceselldis',
            'novat_price' => 'Novat Price',
            'vat' => 'Vat',
            'invat_price' => 'Invat Price',
            'pricewage' => 'Pricewage',
            'priceparts' => 'Priceparts',
            'priceall' => 'Priceall',
            'diswage' => 'Diswage',
            'disparts' => 'Disparts',
            'disall' => 'Disall',
            'taxwage' => 'Taxwage',
            'taxparts' => 'Taxparts',
            'taxall' => 'Taxall',
            'sum_avg_parts' => 'Sum Avg Parts',
            'sumwage' => 'Sumwage',
            'sumparts' => 'Sumparts',
            'sumall' => 'Sumall',
            'tax_status' => 'Tax Status',
            'credit' => 'Credit',
            'creditID' => 'Credit ID',
            'creditNo' => 'Credit No',
            'creditExp' => 'Credit Exp',
            'disprice' => 'Disprice',
            'ysell_date' => 'Ysell Date',
            'ydate_receiveparts' => 'Ydate Receiveparts',
            'emp_sellpart' => 'Emp Sellpart',
            'ybranch' => 'Ybranch',
            'timeEdit' => 'Time Edit',
            'empEdit' => 'Emp Edit',
            'sent_acc_status' => 'Sent Acc Status',
            'sent_revenue_status' => 'Sent Revenue Status',
            'sent_avg_status' => 'Sent Avg Status',
            'sent_sale_status' => 'Sent Sale Status',
            'sent_sale_cost' => 'Sent Sale Cost',
            'status' => 'Status',
            'statusbill' => 'Statusbill',
            'guarantee' => 'Guarantee',
            'sendTis' => 'Send Tis',
            'dateSendTis' => 'Date Send Tis',
            'ChangeDateIn' => 'Change Date In',
            'ChangeDateOut' => 'Change Date Out',
            'tis_part' => 'Tis Part',
            'tis_part_list' => 'Tis Part List',
            'tis_all' => 'Tis All',
            'tis_vat_part' => 'Tis Vat Part',
            'tis_vat_part_list' => 'Tis Vat Part List',
            'tis_vat_all' => 'Tis Vat All',
            'tis_sum_all' => 'Tis Sum All',
            'tis_dis_part' => 'Tis Dis Part',
            'tis_dis_labour' => 'Tis Dis Labour',
            'tis_dis_all' => 'Tis Dis All',
            'tis_check_edit' => 'Tis Check Edit',
            'check_reduce' => 'Check Reduce',
            'StatusAttachCoupon' => 'Status Attach Coupon',
            'cusNo' => 'Cus No',
            'idRef_vihicle' => 'Id Ref Vihicle',
            'Budget' => 'Budget',
            'sent_view_ytax' => 'Sent View Ytax',
            'comp_inv_number' => 'Comp Inv Number',
            'comp_type' => 'Comp Type',
            'comp_branch_id' => 'Comp Branch ID',
            'comp_branch_txt' => 'Comp Branch Txt',
            'cus_inv_number' => 'Cus Inv Number',
            'cus_type' => 'Cus Type',
            'cus_comptype' => 'Cus Comptype',
            'cus_comp_branch_id' => 'Cus Comp Branch ID',
            'cus_comp_branch_txt' => 'Cus Comp Branch Txt',
            'refReceiveTis' => 'Ref Receive Tis',
            'xml_notax_run' => 'Xml Notax Run',
            'status_sent_xml' => 'Status Sent Xml',
        ];
    }
}
