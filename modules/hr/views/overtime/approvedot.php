<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 6/6/2017 AD
 * Time: 14:29
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;


use app\modules\hr\apihr\ApiHr;
//use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiOT;


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/employee-lookup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/ot_approvedot.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/ot_manageot.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

$arrHour = DateTime::makeHour();
$arrMinute = DateTime::makeMinute();


//$today = date('d/m/Y');

/*echo '<pre>';
print_r($arrCompany2);
echo '</pre>';*/

/*echo '<pre>';
print_r($arrCompany2[56]);
echo '</pre>';


echo '<pre>';
print_r($arrCompany);
echo '</pre>';*/
?>

    <section class="content">
        <!-- Default box -->
        <div class="box box-danger">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#"> ข้อมูลเงินเดือน</a>
                    </li>
                    <li>บันทึกข้อมูลการทำงานล่วงเวลา</li>
                    <li class="active">ข้อมูลการทำงานล่วงเวลา</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">


                <div class="box-body">
                    <div class="row">
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>บริษัท : </label>
                            <?php
                            foreach ($arrCompany as $value) {
                                if ($OtMaster['company_id'] == $value['id']) {
                                    echo ApiOT::showTextUnderline($value['name']);
                                    break;
                                }
                            }
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <label>แผนก :</label>
                            <?php
                            echo ApiOT::showTextUnderline($arrDepartment[$OtMaster['division_id']]['name']);
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <label>ฝ่าย :</label>
                            <?php
                            echo ApiOT::showTextUnderline($arrDepartment[$OtMaster['section_id']]['name']);
                            ?>
                        </div>
                        <div class="col-md-1">&nbsp;</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <label>กิจกรรมโอที :</label>
                            <?php
                            echo ApiOT::showTextUnderline($arrOTActivity[$OtMaster['activity_id']]);
                            ?>
                        </div>

                        <div class="col-sm-3">
                            <label>ประเภทการคิดโอที : </label>
                            <?php
                            $tmp = ($OtMaster['ot_calculate_type'] == 1) ? 'ปกติ' : (($OtMaster['ot_calculate_type'] == 2) ? 'วันหยุด' : 'นอกเวลาวันหยุด');
                            echo ApiOT::showTextUnderline($tmp);
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <label>วันที่ทำกิจกรรมโอที :</label>
                            <?php echo ApiOT::showTextUnderline(DateTime::CalendarDate($OtMaster['activity_date'])); ?>
                        </div>
                    </div>
                    <div class="row">&nbsp;
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>มีค่าเดินทาง : </label>
                            <?php echo ApiOT::showTextUnderline(($OtMaster['has_profile_route'] == 1) ? 'มี' : 'ไม่มี'); ?>
                        </div>
                        <div class="col-sm-3">
                            <label>คิดค่าเดินทาง :</label>
                            <?php echo ApiOT::showTextUnderline(($OtMaster['has_profile_route'] == 1) ? $arrOTProfileRoute[$OtMaster['profile_route_id']] : 'ไม่มี'); ?>
                        </div>
                        <div class="col-sm-3">
                            <label>จำนวนระยะทาง (กม.) :</label>
                            <?php echo ApiOT::showTextUnderline(($OtMaster['has_profile_route'] == 1) ? $OtMaster['distance_amount'] : 'ไม่มี'); ?>
                        </div>
                    </div>
                    <div class="row">&nbsp;
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>มีค่าที่พัก :</label>
                            <?php echo ApiOT::showTextUnderline(($OtMaster['has_motel_profile'] == 1) ? 'มี' : 'ไม่มี'); ?>
                        </div>
                        <div class="col-sm-3">
                            <label>จ่ายตามจริง (บาท) : </label>
                            <?php echo ApiOT::showTextUnderline(($OtMaster['has_motel_profile'] == 1) ? $OtMaster['motel_price'] : 'ไม่มี'); ?>
                        </div>
                        <div class="col-sm-2">
                            <label>รอบเงินเดือน : </label>
                            <?php echo ApiOT::showTextUnderline($OtMaster['wage_pay_date']) ; ?>
                        </div>
                    </div>
                    <div class="row">&nbsp;
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>ประเภทการจ่ายค่าล่วงเวลา : </label>
                            <?php

                            $k = $arrTemplateID[$OtTemplateID];
                            $t = "เฉพาะเดือน ($k)";
                            echo ApiOT::showTextUnderline($t);

                            ?>
                        </div>
                        <div class="col-sm-3">
                            <label>เลขที่คำขอ : </label>
                            <?php echo ApiOT::showTextUnderline($OtMaster['request_no']); ?>
                        </div>
                        <div class="col-sm-2">
                            <label>วันที่สร้างคำขอ : </label>
                            <?php echo ApiOT::showTextUnderline(DateTime::CalendarDate($OtMaster['request_date'])); ?>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                </div>
                <form class="form-horizontal" id="frmApproved" name="frmApproved" method="POST" action="#">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">รายชื่อพนักงานทำกิจกรรมโอที</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table id="tbemployeeot" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th style="width: 2%">ลำดับ</th>
                                                <th style="width: 18%">ชื่อ-สกุล</th>
                                                <th style="width: 15%">แผนก</th>
                                                <th style="width: 10%;text-align: center;">เวลาเข้า OT (ชั่วโมง:นาที)
                                                </th>
                                                <th style="width: 10%;text-align: center;">เวลาออก OT (ชั่วโมง:นาที)
                                                </th>
                                                <th style="width: 9%;text-align: center">รวม(ชั่วโมง:นาที)</th>
                                                <th style="width: 9%;text-align: center">รวม(เงิน:บาท)</th>
                                                <th style="width: 10%;text-align: center">ค่าตอบแทน</th>
                                                <th style="width: 8%">สถานะ</th>
                                                <th colspan="2" style="width: 30%">
                                                    <table width="100%" cellspacing="3">
                                                        <tr style="height: 40px;">
                                                            <td colspan="2"><span class="label label-primary"
                                                                                  style="font-size: 1em;"> <strong>การอนุมัติ</strong></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-right: 2px;">
                                                                <button class="btn btn-success" type="button" id="btnApprovedAll"><i
                                                                            class="fa fa-legal"></i> ทั้งหมด
                                                                </button>
                                                            </td>
                                                            <td style="padding-left: 2px;">
                                                                <button class="btn btn-info" type="button"  id="btnApprovedPartial"><i
                                                                            class="fa fa-legal"></i> ไม่อนุมัติทั้งหมด
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                            <thead>
                                            <tbody>
                                            <?php
                                            $idx = 1;

                                            foreach ($OtDetail as $item) {

                                                ?>
                                                <tr>
                                                    <td><?php echo $idx; ?>.</td>
                                                    <td><?php echo $EmpData[$item->id_card]['name']; ?></td>
                                                    <td><?php echo $EmpData[$item->id_card]['deptname']; ?></td>
                                                    <td style="width: 10%;text-align: center;"><?php echo ApiOT::showTime($item->time_start);; ?></td>
                                                    <td style="width: 10%;text-align: center;"><?php echo ApiOT::showTime($item->time_end); ?></td>
                                                    <td style="width: 10%;text-align: center;"><?php echo ApiOT::showTime($item->time_total); ?></td>
                                                    <td style="width: 10%;text-align: center;"><?php echo Helper::displayDecimal($item->money_total); ?></td>
                                                    <td style="text-align: center;"><?php echo $item->return_name; ?></td>
                                                    <td style="width: 10%;text-align: center;"><?php echo ($opt=='hr') ?  ApiOT::mapOTstatus($item->is_hr_approved) :  ApiOT::mapOTstatus($item->is_approved); ?></td>
                                                    <td style="width: 8%;text-align: center;"><input type="radio"
                                                                                                     value="1"
                                                                                                     id="item_approveda_<?php echo $item->id ?>"
                                                                                                     name="item_approved[<?php echo $item->id ?>]" onclick="ApprovedOne(<?php echo $item->id ?>);">
                                                        อนุมัติ
                                                    </td>
                                                    <td style="width: 8%;text-align: center;"><input type="radio"
                                                                                                     value="0"
                                                                                                     id="item_approvedb_<?php echo $item->id ?>"
                                                                                                     name="item_approved[<?php echo $item->id ?>]" onclick="UnApprovedOne(<?php echo $item->id ?>);">
                                                        ไม่อนุมัติ
                                                        <input type="text" id="item_comment_<?php echo $item->id ?>"
                                                               name="item_comment[<?php echo $item->id ?>]"
                                                               class="form-control" style="display: none;">
                                                    </td>
                                                </tr>
                                                <?php
                                                $idx++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="height: 20px;"></div>
                    </div>
                        <?php
                        //process gm approved
                        if($OtMaster['is_approved'] != 1)
                            {
                                ?>
                        <div class="text-center">
                            <input type="hidden" id="rqmaster_id" name="rqmaster_id" value="<?php echo $rqmaster_id;?>">
                            <input type="hidden" id="approved" name="approved" value="<?php echo $approved;?>">
                            <button class="btn btn-success" id="btnSaveApproved" type="button"><i class="fa fa-save"></i> บันทึก</button>
                            <button class="btn btn-danger" id="btnCancelReply" type="button"><i class="fa fa-reply"></i> กลับไปปรับปรุง</button>
                        </div>
                    <?php } ?>

                    <?php
                    //process hr approved
                    if($OtMaster['is_hr_approved'] != 1 && $opt=='hr')
                    {
                        ?>
                        <div class="text-center">
                            <input type="hidden" id="rqmaster_id" name="rqmaster_id" value="<?php echo $rqmaster_id;?>">
                            <input type="hidden" id="approved" name="approved" value="<?php echo $approved;?>">
                            <button class="btn btn-success" id="btnSaveApprovedHr" type="button"><i class="fa fa-save"></i> บันทึก</button>
                        </div>
                    <?php } ?>

                    <input type="hidden" id="is_approved" name="is_approved" value="<?php echo $OtMaster['is_approved'];?>">
                    <input type="hidden" id="is_hr_approved" name="is_hr_approved" value="<?php echo $OtMaster['is_hr_approved'];?>">
                    <input type="hidden" id="opt" name="opt" value="<?php echo $opt;?>">
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section><!-- /.content -->







