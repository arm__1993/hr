<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "tax_calculate".
 *
 * @property int $id รหัส
 * @property string $emp_idcard รหัสบัตรประชาชนพนักงานจ่ายภาษี
 * @property int $months เดือน
 * @property int $years ปี
 * @property string $pay_date รอบเดือนปีจ่ายเงินเดือน
 * @property int $wage_type ประเภทการจ่ายเงินเดือน 1=ราย7วัน,2=ราย 15วัน,3=รายเดือน
 * @property int $wage_multiply ตัวคูณเพื่อคำนวณภาษีตามประเภทการจ่าย 52=ราย 7วัน,24=ราย15วัน,12= รายเดือน
 * @property int $company_id
 * @property string $company_name
 * @property string $company_tax_code
 * @property string $position_code
 * @property string $position_name
 * @property string $salary_wage_monthly เงินเดือน
 * @property string $salary_wage_other รายได้อื่นๆ 40(2)(8)
 * @property string $total_monthly เงินเดือนคำนวณเสมือนจ่ายทุกเดือน
 * @property string $total_yearly รายได้ต่อเสมือนได้รับทั้งปี
 * @property string $total_deduction รายการลดหย่อนรวม
 * @property string $total_income เงินได้สุทธิ
 * @property string $total_tax_year ยอดภาษีทั้งปี
 * @property string $total_tax_month ยอดภาษีรายเดือน
 * @property string $createby_user เลขประจำตัวผู้สร้าง
 * @property string $create_datetime สร้างรายการเมื่อวันที่เวลา
 */
class TaxCalculate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tax_calculate';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['months', 'years', 'wage_type', 'wage_multiply', 'company_id'], 'integer'],
            [['salary_wage_monthly', 'salary_wage_other', 'total_monthly', 'total_yearly', 'total_deduction', 'total_income', 'total_tax_year', 'total_tax_month'], 'number'],
            [['create_datetime'], 'safe'],
            [['emp_idcard', 'createby_user'], 'string', 'max' => 13],
            [['pay_date'], 'string', 'max' => 12],
            [['company_name'], 'string', 'max' => 255],
            [['company_tax_code', 'position_code'], 'string', 'max' => 30],
            [['position_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_idcard' => 'Emp Idcard',
            'months' => 'Months',
            'years' => 'Years',
            'pay_date' => 'Pay Date',
            'wage_type' => 'Wage Type',
            'wage_multiply' => 'Wage Multiply',
            'company_id' => 'Company ID',
            'company_name' => 'Company Name',
            'company_tax_code' => 'Company Tax Code',
            'position_code' => 'Position Code',
            'position_name' => 'Position Name',
            'salary_wage_monthly' => 'Salary Wage Monthly',
            'salary_wage_other' => 'Salary Wage Other',
            'total_monthly' => 'Total Monthly',
            'total_yearly' => 'Total Yearly',
            'total_deduction' => 'Total Deduction',
            'total_income' => 'Total Income',
            'total_tax_year' => 'Total Tax Year',
            'total_tax_month' => 'Total Tax Month',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
        ];
    }
}
