<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/6/2017 AD
 * Time: 11:57
 */


use app\bundle\AppAsset;
use app\modules\hr\apihr\ApiHr;



AppAsset::register($this);




$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payrollextra/exportfile.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css


?>



<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li><a href="#">ส่งออกไฟล์</a></li>
                <li class="active">ส่งออกไฟล์</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h4 class="box-title">เลือกวันที่ออกรายงานส่งธนาคาร </h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class ="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="numberPassportEmp"  class="col-sm-4 control-label">บริษัท</label>
                                <div class="col-sm-8">
                                    <select class="form-control multiselect" name="selectworking[]" id="arrComexport"
                                            multiple="multiple" required>

                                        <?php $working = ApiHr::getWorking_company();
                                        foreach ($working as $value) {
                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class ="row">
                        &nbsp;
                    </div>
                    <div class ="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="numberPassportEmp"  class="col-sm-4 control-label">เลือกวันที่จ่ายเงินเดือน</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control dateselect" name="datepayexport" id="datepayexport" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class ="row">
                        &nbsp;
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            &nbsp;
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="button" class="btn  btn-primary btn-md" id="submitexportbank"><i class="fa fa-download"></i> ส่งออกข้อมูล</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->


