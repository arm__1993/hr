<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานรายได้ค่าแรงศูนย์บริการ</h3>

<div class="box">
    <div class="col-xs-12">
        <br/>

        <?php
        $arrMonth = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
        ?>

        <?php $arrKeyYear = array_keys($checkarray);
        $arrValue = [];
        foreach ($arrKeyYear as $item) {

            foreach ($checkarray[$item] as $val) {
                $arrValue[$item][] = (float)$val;
            }
        } ?>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">ตารางเปรียบเทียบค่าแรงต่อปี</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>

                    <tr>
                        <td><h4>เดือน</h4></td>

                        <?php for ($col = 0; $col <= count($arrKeyYear) - 1; $col++) { ?>
                            <td><h4><?php echo "รายได้ค่าแรงปี " . $arrKeyYear  [$col]; ?></h4></td>
                        <?php } ?>
                        <td></td>
                        <td><h4> ยอดเป้าค่าแรงปี <?php echo $maxYear ?> </h4></td>
                    </tr>


                    <?php for ($row = 0; $row < count($arrValue[$item]); $row++) { ?>

                        <tr>

                            <h4>
                                <td>
                                    <?php echo $arrMonth[$row] ?>
                                </td>


                                <?php for ($col = 0; $col <= count($arrKeyYear); $col++) { ?>
                                    <td><?php echo $arrValue[$arrKeyYear  [$col]][$row] ?></td>
                                <?php } ?>

                                <td><?php echo $arrDataAVG[$row] ?></td>
                            </h4>

                        </tr>

                    <?php } ?>

                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>


    <!-- highchart-->

    <div class="box-body no-padding">

        <?php

        $arrData = $arrAVG = $arrDataSeries = [];
        $arrDataSeries = [];

        foreach ($arrKeyYear as $item) {

            $arrhighchart = [];
            foreach ($checkarray[$item] as $val) {
                $arrhighchart[] = (float)$val;
                //print_r($arrValue);
            }

            $arrData = [
                'type' => 'column',
                'name' => $item,
                'data' => $arrhighchart,
            ];

            array_push($arrDataSeries, $arrData); //Add array to Series
        }

        foreach ($arrDataAVG as $val) {
            $arrAVG[] = (float)$val;

        }

        //array average
        $avgLine = [
            'type' => 'spline',
            'name' => 'Average',
            'data' => $arrAVG,
            'marker' => [
                'lineWidth' => 2,
                'lineColor' => 'Highcharts.getOptions().colors[3]',
                'fillColor' => 'red',
            ]
        ];

        array_push($arrDataSeries, $avgLine); //Add AVG to series


        echo Highcharts::widget([
            'options' => [
                'title' => [
                    'text' => 'Combination chart'
                ],
                'xAxis' => [
                    'categories' => $arrMonth,
                ],
                'labels' => [
                    'items' => [
                        'html' => 'Total fruit consumption',
                        'style' => [
                            'left' => '50px',
                            'top' => '18px',
                            'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                        ]
                    ]
                ],

                'series' => $arrDataSeries
            ]
        ]);

        ?>

    </div>
</div>

<!-- /.box-body -->