<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

//use app\api\ApiReport;
//use app\api\LevelNumber;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use kartik\select2\Select2;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;


HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/select2.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/select2.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$script = <<< JS
$(document).ready(function() 
{

    
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

   $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });
    
    $("#name").select2(); 
   // var $eventSelect = $(".js-example-events");
  
});

JS;


$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);


?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานประสิทธิภาพช่างรายคน(รายวัน)</h3>

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <div class="row">
            <div class="col-md-12 text-center">
                <form method="post" action="receive_repairman_day">
                    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf">

                    <label>บริษัท</label>
                    &nbsp;
                    <div class="btn-group">
                        <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key => $value) {
                                $sel = ($selected_company == $key) ? ' selected="selected" ' : '';
                                echo '<option value="' . $key . '" ' . $sel . '>' . $value . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    &nbsp;&nbsp;
                    <label>ชื่อ</label>
                    &nbsp;
                    <div class="btn-group">
                        <select class="form-control" name="name" id="name" style="width: 200px">
                            <?php
                            $technicnames = ApiReport::getNameTechnician();
                            $technicname = $technicnames['name'];
                            foreach ($technicname as $name) {
                                $sel = ($selected_technician_id == $name['technician_id']) ? ' selected="selected" ' : '';
                                echo '<option value="' . $name['technician_id'] . '" ' . $sel . '>' . $name['name'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    &nbsp;&nbsp;
                    <label>ช่วงวันที่</label>
                    &nbsp;
                    <div class="btn-group">
                        <div class="input-group" style="width: 250px">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="reservation" id="reservation"
                                   value="<?php echo ($selected_date) ? $selected_date : ''; ?>"
                                   data-provide="datepicker" data-date-language="th">
                        </div>
                    </div>
                    &nbsp;&nbsp
                    <br>
                    <div class="btn-group">
                        <input type="radio" name="technician"
                               value="1" <?php echo ($selected_technician != 2 && $selected_technician != 3) ? 'checked' : ''; ?>>
                        ช่างรถเล็ก
                        &nbsp;
                        <input type="radio" name="technician"
                               value="2" <?php echo ($selected_technician == 2) ? 'checked' : ''; ?>> ช่างรถใหญ่
                        &nbsp;
                        <input type="radio" name="technician"
                               value="3" <?php echo ($selected_technician == 3) ? 'checked' : ''; ?>> ช่างทั้งหมด
                    </div>


                    <br>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                    </div>
                    <div class="btn-group">
                        <a href="<?php echo SITE_URL; ?>/personal/btn_reportday">
                            <button type="button" class="btn btn-success  pull-right btn-md">
                                <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                            </button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->


    <?php
    if (!empty($totalTC)) {

        ?>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                        data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php
                                        foreach ($totalTC as $value) {
                                            $fullname[] = $value['name'];
                                            $fullwage = $value['wage'];
                                            $checkTime = explode(":", $value['time']);
                                            $timeTc = ($checkTime[0] * 60) + $checkTime[1];
                                            $fulltime = $timeTc;
                                            $wageofsell[] = ($fulltime > 0) ? $fullwage / $fulltime : 0;
                                        }
                                        for ($j = 0; $j < count($wageofsell); $j++) {
                                            $percentfullwage[] = ($wageofsell[$j] * 100) / 240;
                                            $percentfullwages[] = (float)($percentfullwage[$j]);
                                            $performance[] = (float)($performance[$j]);
                                        }
                                        //print_r($percentfullwages);
                                        ?>

                                        <?php
                                        echo Highcharts::widget([
                                            'options' => [
                                                'chart' => [
                                                    'type' => 'column'
                                                ],
                                                'title' => [
                                                    'text' => 'กราฟแสดงจำนวนช่างต่อวัน'
                                                ],
                                                'xAxis' => [
                                                    'categories' => $fullname
                                                ],
                                                'yAxis' => [
                                                    'min' => 0,
                                                    'title' => [
                                                        'text' => 'ประสิทธิภาพ'
                                                    ]
                                                ],
                                                'credits' => [
                                                    'enabled' => false
                                                ],
                                                'series' => [
                                                    [
                                                        'type' => 'column',
                                                        'name' => 'ประสิทธิภาพช่าง',
                                                        'data' => $percentfullwages,//[50, 48, 89, 25, 36,22,36,45]
                                                    ],
                                                    [
                                                        'type' => 'spline',
                                                        'name' => 'ประสิทธิภาพช่าง' . $persona,
                                                        'data' => $performance,//[3, 2.67, 3, 6.33, 3.33],
                                                        'marker' => [
                                                            'lineWidth' => 2,
                                                            'lineColor' => 'Highcharts.getOptions().colors[3]',
                                                            'fillColor' => 'white',
                                                        ]
                                                    ],

                                                ]
                                            ]
                                        ]);
                                        ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-info -->
                </div>
            </div>
        </section>
    <?php } ?>

    <?php if (!empty($namePersonas)) { ?>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA TABLE -->
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">รายงานประสิทธิภาพช่างรายคน</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                        data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example2" class="table table-bordered table-hover" role="grid"
                                               aria-describedby="example2_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="Rendering engine: activate to sort column descending"
                                                    aria-sort="ascending">
                                                    ประสิทธิภาพงานซ่อมของ
                                                    <?php echo $namePersonas['name']; ?>
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1" aria-label="Browser: activate to sort column ascending">
                                                    จันท์ - เสาร์
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    อาทิตย์
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    ลา (ชม.)
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="CSS grade: activate to sort column ascending">
                                                    OT (ชม.)
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="CSS grade: activate to sort column ascending">
                                                    รวมชั่วโมงการทำงาน
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr role="row">
                                                <th>จำนวนวันทำงาน (เดือน)</th>
                                                <td>
                                                    <?php
                                                    $workday = $_arrRET['total_workday'];
                                                    if (empty($workday)) {
                                                        echo "0";
                                                    } elseif ($workday) {
                                                        echo $workday;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $sunday = $_arrRET['total_sunday'];
                                                    if (empty($sunday)) {
                                                        echo "0";
                                                    } elseif ($sunday) {
                                                        echo $sunday;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $leave = 8;
                                                    echo $leave;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $ot = 0;
                                                    echo $ot;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $workday = $_arrRET['total_workday'];
                                                    $sunday = $_arrRET['total_sunday'];
                                                    if (empty($workday || $sunday)) {
                                                        echo "0";
                                                    } elseif ($workday || $sunday) {
                                                        $workdays = $workday * 8;
                                                        $sundays = $sunday * 8;
                                                        $total_hour = ($workdays + $sundays) - $leave - $ot;
                                                        echo($total_hour);
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr role="row">
                                                <th>ชั่วโมงทำงานต่อวัน</th>
                                                <td>8</td>
                                                <td>8</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                            <tr role="row">
                                                <th>จำนวนช่าง</th>
                                                <td>1</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                            <tr role="row">
                                                <th>จำนวนชั่วโมงทำงาน (เดือน)</th>
                                                <td>
                                                    <?php
                                                    $workday = $_arrRET['total_workday'];
                                                    if (empty($workday)) {
                                                        echo "0";
                                                    } elseif ($workday) {
                                                        $workdays = $workday * 8;
                                                        echo($workdays);
                                                    }
                                                    ?>
                                                </td>
                                                <td> <?php
                                                    $sunday = $_arrRET['total_sunday'];
                                                    if (empty($sunday)) {
                                                        echo "0";
                                                    } elseif ($sunday) {
                                                        $sundays = $sunday * 8;
                                                        echo $sundays;
                                                    }
                                                    ?>
                                                </td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <h5>รายได้ค่าแรงของช่าง 1 คน&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php
                                            echo ($wagePersona) ? Helper::displayDecimal($wagePersona) : 0;
                                            ?>
                                        </h5>
                                        <h5>ค่าแรงต่อชั่วโมงที่ขาย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php
                                            echo ($timePersona) ? Helper::displayDecimal($timePersona) : 0;
                                            ?>
                                        </h5>
                                        <h5>ค่าแรงต่อชั่วโมงที่ขายได้&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php
                                            $percentwage = ($timePersona > 0) ? ($wagePersona / $timePersona) : 0;
                                            echo Helper::displayDecimal($percentwage);
                                            ?>
                                        </h5>
                                        <h5>ประสิทธิภาพงานซ่อมของช่าง 1 คน&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php
                                            $performance = ($total_hour > 0) ? ($percentwage * 100) / $total_hour : 0;
                                            echo Helper::displayDecimal($performance) . '%';
                                            ?>
                                        </h5>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.example2_wrapper -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box-success -->
                </div>
                <!-- /.col-md-12 -->
            </div>
        </section>
    <?php } ?>

    <?php if (!empty($totalTC)) { ?>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA TABLE -->
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">รายงานประสิทธิภาพช่าง</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                        data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-bordered table-hover" role="grid"
                                               aria-describedby="example2_info">

                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Rendering engine: activate to sort column descending"
                                                    aria-sort="ascending">
                                                    ที่
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1" aria-label="Browser: activate to sort column ascending">
                                                    ชื่อ - นามสกุล
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    รวมชั่วโมงทำงาน
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    รายได้ค่าแรง
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="CSS grade: activate to sort column ascending">
                                                    ประสิทธิภาพ
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1"
                                                    aria-label="CSS grade: activate to sort column ascending">
                                                    ลา (ชม.)
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?php
                                            $item = 1;
                                            foreach ($totalTC as $value) {
                                                $fullname = ($value['name']);
                                                $fullwage = ($value['wage']);
                                                $checkTime = explode(":", $value['time']);
                                                $timeTc = ($checkTime[0] * 60) + $checkTime[1];
                                                $fulltime = $timeTc;
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $item; ?></td>
                                                    <td><?php echo $fullname; ?></td>
                                                    <td class="text-right"><?php echo $fulltime; ?></td>
                                                    <td class="text-right"><?php echo Helper::displayDecimal($fullwage); ?></td>
                                                    <td class="text-right">
                                                        <?php
                                                        $wageofsell = ($fulltime > 0) ? $fullwage / $fulltime : 0;
                                                        $percentfullwage = ($wageofsell * 100) / 240;
                                                        echo Helper::displayDecimal($percentfullwage); ?>%
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <?php $item++;
                                            } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box-warning -->

                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </section>
    <?php } ?>


</div>
<!-- /.box -->
