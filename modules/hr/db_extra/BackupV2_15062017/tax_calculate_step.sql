-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2017 at 09:02 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_calculate_step`
--

CREATE TABLE `tax_calculate_step` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `tax_calculate_id` int(11) UNSIGNED NOT NULL COMMENT 'รหัสอ้างอิงไปยัง tax_calculate FK  ==>tax_calculate.id',
  `tax_income_structure_id` int(11) UNSIGNED NOT NULL COMMENT 'รหัสอ้างอิงไปยัง tax_income_structure FK  ==>tax_income_structure.id',
  `income_range` varchar(100) DEFAULT NULL COMMENT 'ช่วงรายได้ที่จะคำนวณภาษี',
  `tax_rate` decimal(10,2) DEFAULT NULL,
  `income_step` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนเงินได้ในช่วง',
  `gross_step_tax` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนภาษีเงินได้ในช่วง',
  `total_accumulate_tax` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนภาษีสะสมรวมแต่ละช่วง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ตารางจัดเก็บข้อมูลคำนวณแบบขั้นบันไดภาษีเงินได้รายเดือน';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_calculate_step`
--
ALTER TABLE `tax_calculate_step`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_calculate_id` (`tax_calculate_id`),
  ADD KEY `tax_income_structure_id` (`tax_income_structure_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_calculate_step`
--
ALTER TABLE `tax_calculate_step`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
