<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "menu_manage".
 *
 * @property integer $id
 * @property string $menu_id
 * @property string $working_company_id
 * @property string $department_id
 * @property string $section_id
 * @property string $position_id
 * @property string $id_card
 * @property string $skill
 * @property string $status
 */
class MenuManage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_manage';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'id_card', 'skill'], 'required'],
            [['menu_id', 'working_company_id', 'department_id', 'section_id', 'position_id'], 'string', 'max' => 20],
            [['id_card'], 'string', 'max' => 13],
            [['skill'], 'string', 'max' => 1],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'working_company_id' => 'Working Company ID',
            'department_id' => 'Department ID',
            'section_id' => 'Section ID',
            'position_id' => 'Position ID',
            'id_card' => 'Id Card',
            'skill' => 'Skill',
            'status' => 'Status',
        ];
    }
}
