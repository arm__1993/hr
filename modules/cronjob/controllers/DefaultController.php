<?php

namespace app\modules\cronjob\controllers;

use yii\web\Controller;

/**
 * Default controller for the `cronjob` module
 */
class DefaultController extends Controller
{
    public $layout = 'cronjoblayout';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
