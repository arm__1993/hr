<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:11
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;




?>
<style type="text/css">
      .modal-wide .modal-dialog {
               width: 70%; /* or whatever you wish */
        }
</style>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li>เพิ่มข้อมูลพนักงาน</li>
                <li class="active">ประวัติการทำงาน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
                    <div class="row>">
                        <form class="form-horizontal">
                                <div class="box box-info">
                                        <div class="box-header with-border">
                                        <h3 class="box-title">ประวัติการทำงานเดิม</h3>
                                        </div>
                                           
                                            <div class="box-body">
                                             <div align="right"> 
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddWorkhis">เพิ่มข้อมูล</button>
                                            </div>
                                            
                                            <div class="modal fade modal-wide" id="modalAddWorkhis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 id="myModalLabel" class="modal-title">เพิ่มข้อมูลประวัติการทำงาน</h4>
                                                            </div>
                                                             <form action="#" method="post">
                                                                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                                                    <div class="modal-body" width="100%" class="table table-bordered table-hover dataTable">
                                                                                                                                            <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-2 control-label">บริษัท</label>
                                                                    <div class="col-sm-2">
                                                                        <input type="text" class="form-control">
                                                                    </div> 
                                                                    <label for="genderEmp" class="col-sm-1 control-label">เงินเดือน</label>
                                                                    <div class="col-sm-2">
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                    <label class="col-sm-2 control-label">บาท/เดือน</label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-2 control-label">ประเภทธุรกิจ</label>
                                                                    <div class="col-sm-2">
                                                                        <input type="text" class="form-control">
                                                                    </div> 
                                                                    <label for="genderEmp" class="col-sm-1 control-label">โบนัส/คอมมิชั่น</label>
                                                                    <div class="col-sm-2">
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                    <label  class="col-sm-2 control-label">บาท/เดือน</label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-2 control-label">ตำแหน่ง</label>
                                                                    <div class="col-sm-2">
                                                                        <input type="text" class="form-control">
                                                                    </div> 
                                                                    <label for="genderEmp" class="col-sm-1 control-label">ระยะเวลาในการทำงาน</label>
                                                                    <div class="col-sm-2">
                                                                        <input type="month" class="form-control">
                                                                    </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-3 control-label">ลักษณะงาน / ความรับผิดชอบ</label>
                                                                    <label for="genderEmp" class="col-sm-2 control-label">สวัสดิการที่ได้รับ</label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-1 control-label"></label>
                                                                    <div class="col-sm-3">
                                                                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                                                    </div> 
                                                                    <div class="col-sm-3">
                                                                        <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                เครื่องแบบ
                                                                                </span>
                                                                            <input type="text" class="form-control" size="3">
                                                                        </div>
                                                                    </div>
                                                                    <label>ชุด/ปี</label>
                                                                    <br>
                                                                    <br>

                                                                    <div class="col-sm-3">
                                                                        <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                อาหารฟรี
                                                                                </span>
                                                                            <input type="text" class="form-control" size="3">
                                                                        </div>
                                                                    </div>
                                                                    <label>มื้อ/วัน</label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-2 control-label">สาเหตุที่ออก</label>
                                                                    <div class="col-sm-2">
                                                                        <select class="form-control">
                                                                            <option>HBSO</option>
                                                                        </select>
                                                                    </div> 
                                                                    <div class="col-sm-3">
                                                                        <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                รถรับส่งพนักงาน
                                                                                </span>
                                                                            <input type="text" class="form-control" size="3">
                                                                        </div>
                                                                    </div>
                                                                    <label>บาท/เดือน</label>
                                                                    </div>
                                                                </div>
                                                                 <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-2 control-label">อธิบายสาเหตุที่ออก</label>
                                                                    <div class="col-sm-2">
                                                                    
                                                                    </div> 
                                                                    <div class="col-sm-3">
                                                                        <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                ที่พักฟรีหรือคิดค่าเช่า
                                                                                </span>
                                                                            <input type="text" class="form-control" size="3">
                                                                        </div>
                                                                    </div>
                                                                    <label>บาท/เดือน</label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                    <label for="genderEmp" class="col-sm-1 control-label"></label>
                                                                    <div class="col-sm-3">
                                                                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                                                    </div> 
                                                                    <div class="col-sm-3">
                                                                        <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                ค่าน้ำมัน/ค่าพาหนะ
                                                                                </span>
                                                                            <input type="text" class="form-control" size="3">
                                                                        </div>
                                                                    </div>
                                                                    <label>บาท/เดือน</label>
                                                                    <br>
                                                                    <br>
                                                                    <div class="col-sm-5">
                                                                        <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                ประกันสุขภาพหรือค่ารักษาพยาบาล
                                                                                </span>
                                                                            <input type="text" class="form-control" size="3">
                                                                        </div>
                                                                    </div>
                                                                    <label>บาท/ปี</label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                        <label for="genderEmp" class="col-sm-2 control-label"></label>
                                                                        <div class="col-sm-2">
                                                                        </div> 
                                                                        <div class="col-sm-4">
                                                                        <label for="genderEmp" class="col-sm-4 control-label">อื่นๆ  (ถ้ามี)</label>
                                                                            <div class="col-sm-5">
                                                                                <input type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                                    <div class="modal-footer">
                                                                        <input type="submit" class="btn btn-primary" value="บันทึกข้อมูล">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                            </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-bordered table-hover dataTable">
                                                <tbody>
                                                    <tr>
                                                        <th>ลำดับ</th>
                                                        <th>บริษัท</th>
                                                        <th>ตำแหน่งงาน</th>
                                                        <th>ระยะเวลา</th>
                                                        <th>เงินเดือน</th>
                                                        <th>การจัดการ</th>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                        </form>
                    </div>
                    <div class="row>">
                        <form class="form-horizontal">
                                <div class="box box-info">
                                        <div class="box-header with-border">
                                        <h3 class="box-title">ประวัติการเข้า-ออกงานกลุ่มนกเงือก</h3>
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-bordered table-hover dataTable">
                                                <tbody>
                                                    <tr>
                                                        <th>ลำดับ</th>
                                                        <th>วันที่เริ่มงาน</th>
                                                        <th>วันที่ผ่านงาน</th>
                                                        <th>วันที่ผ่านงาน</th>
                                                        <th>เหตุผลที่ลาออก</th>
                                                        <th>รายละเอียดการลาออก</th>
                                                        <th>รวมระยะเวลา</th>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                        </form>
                    </div>
                    <div class="row>">
                        <form class="form-horizontal">
                                <div class="box box-info">
                                        <div class="box-header with-border">
                                        <h3 class="box-title">ประวัติการทำงานที่ได้รับมอบหมายกลุ่มนกเงือก</h3>
                                        </div>
                                        <div class="box-body">
                                             <div align="right"> 
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddWorkIngroup">เพิ่มข้อมูล</button>
                                            </div>
                                            <div class="modal fade modal-wide" id="modalAddWorkIngroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 id="myModalLabel" class="modal-title">เพิ่มข้อมูลประวัติการทำงานที่ได้รับมอบหมาย</h4>
                                                    </div>
                                                    <form action="#" method="post">
                                                        <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                                    <div class="modal-body" width="100%" class="table table-bordered table-hover dataTable">
                                                            <div class="form-group">
                                                            <label for="genderEmp" class="col-sm-4 control-label">วันที่เริ่มงานที่ได้รับมอบหมาย</label>
                                                            <div class="col-sm-3">
                                                                <input type="date" class="form-control">
                                                            </div> 
                                                            </div>
                                                             <div class="form-group">
                                                            <label for="genderEmp" class="col-sm-4 control-label">วันที่สิ้นงานที่ได้รับมอบหมาย</label>
                                                            <div class="col-sm-3">
                                                                <input type="date" class="form-control">
                                                            </div> 
                                                            </div>
                                                            <div class="form-group">
                                                            <label for="genderEmp" class="col-sm-4 control-label">ตำแหน่ง</label>
                                                            <div class="col-sm-3">
                                                                <select class="form-control">
                                                                    <option>option 1</option>
                                                                </select>
                                                            </div> 
                                                            </div>
                                                            <div class="form-group">
                                                            <label for="genderEmp" class="col-sm-4 control-label">งานที่มอบหมาย</label>
                                                            <div class="col-sm-3">
                                                                 <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                                            </div> 
                                                            </div>
                                                            <div class="form-group">
                                                            <label for="genderEmp" class="col-sm-4 control-label">ผลการดำเนินงาน</label>
                                                            <div class="col-sm-3">
                                                                 <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                                            </div> 
                                                            </div>
                                                      
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" class="btn btn-primary" value="บันทึกข้อมูล">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                    </form>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->  
                                            <table class="table table-bordered table-hover dataTable">
                                                <tbody>
                                                    <tr>
                                                        <th>ลำดับ</th>
                                                        <th>วันที่เริ่ม</th>
                                                        <th>วันที่สิ้นสุด</th>
                                                        <th>Position Code</th>
                                                        <th>ชื่อตำแหน่ง</th>
                                                        <th>งานที่มอบหมาย</th>
                                                        <th>ผลการดำเนินงาน</th>
                                                        <th>การจัดการ</th>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                        </form>
                    </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->