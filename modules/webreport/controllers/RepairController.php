<?php


namespace app\modules\webreport\controllers;

use app\api\Utility;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiRepair;
use mPDF;

class RepairController extends Controller
{

    public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSa_repair_day()
    {
        //return $this->render('sa_repair_day');
        return $this->render('sa_repair_day',['query'=>false,]);
    }

    public function actionSa_repair_month()
    {
        return $this->render('sa_repair_month',['query'=>false,]);
    }

    public function actionAvg_repair()
    {
        return $this->render('avg_repair',['query'=>false,]);
    }

    public function actionSa_repair_report()
    {
        //$monthStart = Yii::$app->request->post();

        return $this->render('sa_repair_report',['query'=>false,]);


    }

    public function actionAvg_repair_report()
    {
        return $this->render('avg_repair_report',['query'=>false,]);
    }

    public function  actionReceive_sa_repair_report()
    {

        $postValue = Yii::$app->request->post();

        $type_company = $postValue['company'];
        $Start_Month = $postValue['Start_Month'];
        $End_Month = $postValue['End_Month'];
        $Select_Year = $postValue['year'];
   //     Utility::printobj($postValue);


        $selected_company=$type_company;
        $selected_startmonth=$Start_Month;
        $selected_endmonth=$End_Month;
        $selected_year=$Select_Year;


        if(empty($Start_Month) or empty($End_Month) or $End_Month < $Start_Month )
        {
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('sa_repair_report',['query'=>false,]);

        }
        if(strlen($Start_Month) ==1) {
            $Start_Month = str_pad($Start_Month,2,"0",STR_PAD_LEFT);
        }


        if(strlen($End_Month) ==1) {
            $End_Month = str_pad($End_Month,2,"0",STR_PAD_LEFT);
        }


        $getValueRangeDate = ApiRepair::getDateStartToEnd($Select_Year,$Start_Month,$End_Month);
        $Str_Startmonth = $getValueRangeDate['startDate'];
        $Str_Endmonth =   $getValueRangeDate['endDate'];


        $listtime_Sa = ApiRepair::getListtimeSa($type_company,$Str_Startmonth,$Str_Endmonth);


        if(!empty($listtime_Sa)){
        $get_IdDrepair = [];
        foreach ($listtime_Sa as $value)
        {
            $get_IdDrepair[] = $value['b'];
        }
             $IN_get_IdDrepair = implode(",", $get_IdDrepair);

            $List_Drepairinformlist = ApiRepair::getListDrepairinformlist($IN_get_IdDrepair);


            $arrWageDistance = ApiReport::ARRAY_WAGE_GROUP_CHECK_DISTANCE;
            $arrWageGenaral = ApiReport::ARRAY_WAGE_GROUP_REPAIR_GENERAL;
            $drepair_inform_id_wage_distance = $drepair_inform_id_wage_genaral = [];

            foreach ($List_Drepairinformlist as $repair_id_header){

                $_repair_id_header = $repair_id_header['repair_id_header'];
                $_wage = $repair_id_header['wage'];
                foreach ($arrWageDistance as $key => $value) {
                    if($_repair_id_header==$key && in_array($_wage, $value) ) {
            $drepair_inform_id_wage_distance[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                    }
                }

                foreach ($arrWageGenaral as $key => $value) {
                    if($repair_id_header['repair_id_header']==$key && in_array($_wage, $value) ) {
                            $drepair_inform_id_wage_genaral[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                    }
                }
            }


        $drepair_inform_id_wage_distance = array_unique($drepair_inform_id_wage_distance);//เช็คระยะไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง
        $drepair_inform_id_wage_genaral = array_unique($drepair_inform_id_wage_genaral);//ซ่อมทั่วไปไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง
        $drepair_inform_id_wage_intersect =array_intersect($drepair_inform_id_wage_distance,$drepair_inform_id_wage_genaral); // $drepair_inform_id_wage_intersect ซ่อมทั่วไป+เช็คระยะ


        $drepair_inform_id_wage_distance_only = array_diff_key($drepair_inform_id_wage_distance,$drepair_inform_id_wage_intersect);


            $IN_IdDrepairinformwage_distance_only = implode(",", $drepair_inform_id_wage_distance_only);

          $CountDrepairInformDistanceOnly = sizeof($drepair_inform_id_wage_distance_only);

        if($CountDrepairInformDistanceOnly>0){

        $Listdatadrepairdistance_only = ApiRepair::getCvhcMainIdForSearch($IN_IdDrepairinformwage_distance_only);

        $IN_cvhcmain_wage_distance_only = [] ; // เช็คระยะอย่างเดียว
        foreach ($Listdatadrepairdistance_only as $value){
            $IN_cvhcmain_wage_distance_only[] =  $value['cvhc_main_id']; ;

        }

                  $IN_cvhcmain_wage_distance_only = implode(",", $IN_cvhcmain_wage_distance_only);

        $listtime_Sa_wage_distance_only = ApiRepair::getDataSeachByCvhcMain($type_company,$IN_cvhcmain_wage_distance_only);

        }else{

            $listtime_Sa_wage_distance_only = 0;
        }

        ///////////// ซ่อมทั่วไปอย่างเดียว /////////////

        $drepair_inform_id_wage_genaral_only = array_diff_key($drepair_inform_id_wage_genaral,$drepair_inform_id_wage_intersect);
        $IN_IdDrepairinformwage_genaral_only = implode(",", $drepair_inform_id_wage_genaral_only);

              $CountDrepairInformGenaralOnly = sizeof($drepair_inform_id_wage_genaral_only);


        if($CountDrepairInformGenaralOnly>0){

            $Listdatadrepairgenaral_only = ApiRepair::getCvhcMainIdForSearch($IN_IdDrepairinformwage_genaral_only);


            $IN_cvhcmain_wage_genaral_only = [] ; // ซ่อมทั่วไปอย่างเดียว
            foreach ($Listdatadrepairgenaral_only as $value){
                $IN_cvhcmain_wage_genaral_only[] =  $value['cvhc_main_id']; ;

            }

              $IN_cvhcmain_wage_genaral_only = implode(",", $IN_cvhcmain_wage_genaral_only);

            $listtime_Sa_wage_genaral_only = ApiRepair::getDataSeachByCvhcMain($type_company,$IN_cvhcmain_wage_genaral_only);

        }else{

            $listtime_Sa_wage_genaral_only = 0;
        }

        ///////////// ซ่อมทั่วไป+เช็คระยะ /////////////

        $drepair_inform_id_wage_intersectimplode = [] ;
        foreach ($drepair_inform_id_wage_intersect as $value){

            $drepair_inform_id_wage_intersectimplode[] =  $value ;

        }

             $IN_IdDrepairinformwage_intersect = implode(",", $drepair_inform_id_wage_intersectimplode);

        $CountDrepairInformGenaral = sizeof($drepair_inform_id_wage_intersect);

        if($CountDrepairInformGenaral>0){
            $Listdatadrepairgenaral_only = ApiRepair::getCvhcMainIdForSearch($IN_IdDrepairinformwage_intersect);


            $IN_cvhcmain_wage_intersect = [] ; // ซ่อมทั่วไป+เช็คระยะ
            foreach ($Listdatadrepairgenaral_only as $value){
                $IN_cvhcmain_wage_intersect[] =  $value['cvhc_main_id']; ;

            }

            $IN_cvhcmain_wage_intersect = implode(",", $IN_cvhcmain_wage_intersect);

            $listtime_Sa_wage_intersect = ApiRepair::getDataSeachByCvhcMain($type_company,$IN_cvhcmain_wage_intersect);


        }else{

            $listtime_Sa_wage_intersect = 0;
        }

            $session = Yii::$app->session;

            $session->set('dataSa_Wage_Distance_Only', $listtime_Sa_wage_distance_only);
            $session->set('dataSa_Wage_Genaral_Only', $listtime_Sa_wage_genaral_only);
            $session->set('dataSa_Wage_intersect', $listtime_Sa_wage_intersect);
            $session->set('selected_company', $selected_company);
            $session->set('selected_startmonth', $selected_startmonth);
            $session->set('selected_endmonth', $selected_endmonth);
            $session->set('selected_year', $selected_year);




            return $this->render('sa_repair_report',[
            'dataSa_Wage_Distance_Only' => $listtime_Sa_wage_distance_only,
            'dataSa_Wage_Genaral_Only' => $listtime_Sa_wage_genaral_only,
            'dataSa_Wage_intersect' => $listtime_Sa_wage_intersect,
            'query'=>true,
            'selected_company'=>$selected_company,
            'selected_startmonth'=>$selected_startmonth,
            'selected_endmonth'=>$selected_endmonth,
            'selected_year'=>$selected_year,
        ]);
        }else{
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('sa_repair_report',['query'=>false,]);

        }

    }
    public function actionReceive_avg_repair_report()
    {

        $postValue = Yii::$app->request->post();
        $selected_company=$GET_Company = $postValue['company'];
        $selected_year=$select_year = $postValue['year'];


        $listcvhc_main = ApiRepair::getListTimeOfYears($GET_Company,$select_year);


        if(!empty($listcvhc_main)){

            $get_IdDrepair = [];
            foreach ($listcvhc_main as $value)
            {
                $get_IdDrepair[] = $value['drepair_inform_id'];
            }
             $IN_get_IdDrepair = implode(",", $get_IdDrepair);

            $List_Drepairinformlist = ApiRepair::getListDrepairinformlist($IN_get_IdDrepair);

            $arrWageDistance = ApiReport::ARRAY_WAGE_GROUP_CHECK_DISTANCE;
            $arrWageGenaral = ApiReport::ARRAY_WAGE_GROUP_REPAIR_GENERAL;
            $drepair_inform_id_wage_distance = $drepair_inform_id_wage_genaral = [];

            foreach ($List_Drepairinformlist as $repair_id_header){

                $_repair_id_header = $repair_id_header['repair_id_header'];
                $_wage = $repair_id_header['wage'];

                foreach ($arrWageDistance as $key => $value) {
                    if($_repair_id_header==$key && in_array($_wage, $value) ) {
                        //if(in_array($repair_id_header['wage'], $value)) {
                        $drepair_inform_id_wage_distance[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                    }
                }

                foreach ($arrWageGenaral as $key => $value) {
                    if($repair_id_header['repair_id_header']==$key && in_array($_wage, $value) ) {
//                        if(in_array($repair_id_header['wage'], $value)) {
                        $drepair_inform_id_wage_genaral[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                        //                       }
                    }
                }


            }

            $drepair_inform_id_wage_distance = array_unique($drepair_inform_id_wage_distance);//เช็คระยะไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง
            $drepair_inform_id_wage_genaral = array_unique($drepair_inform_id_wage_genaral);//ซ่อมทั่วไปไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง
            $drepair_inform_id_wage_intersect =array_intersect($drepair_inform_id_wage_distance,$drepair_inform_id_wage_genaral); // $drepair_inform_id_wage_intersect ซ่อมทั่วไป+เช็คระยะ


            $drepair_inform_id_wage_distance_only = array_diff_key($drepair_inform_id_wage_distance,$drepair_inform_id_wage_intersect);

               $IN_IdDrepairinformwage_distance_only = implode(",", $drepair_inform_id_wage_distance_only);


            $CountDrepairInformDistanceOnly = sizeof($drepair_inform_id_wage_distance_only);


                   if($CountDrepairInformDistanceOnly>0){

                $Listdatadrepairdistance_only = ApiRepair::getCvhcMainIdForSearch($IN_IdDrepairinformwage_distance_only);

                $IN_cvhcmain_wage_distance_only = [] ; // เช็คระยะอย่างเดียว
                foreach ($Listdatadrepairdistance_only as $value){
                    $IN_cvhcmain_wage_distance_only[] =  $value['cvhc_main_id']; ;

                }

                        $IN_cvhcmain_wage_distance_only = implode(",", $IN_cvhcmain_wage_distance_only);

                $Time_Group_Mount_Wage_Distance_Only = ApiRepair::getCvhcMainIdForSearchReportYear($GET_Company,$IN_cvhcmain_wage_distance_only);

                   }else{

                       $Time_Group_Mount_Wage_Distance_Only = 0;
            }

            ///////////// ซ่อมทั่วไปอย่างเดียว /////////////
            $drepair_inform_id_wage_genaral_only = array_diff_key($drepair_inform_id_wage_genaral,$drepair_inform_id_wage_intersect);


            $IN_IdDrepairinformwage_genaral_only = implode(",", $drepair_inform_id_wage_genaral_only);

             $CountDrepairInformGenaralOnly = sizeof($drepair_inform_id_wage_genaral_only);


            if($CountDrepairInformGenaralOnly>0){
                $Listdatadrepairgenaral_only = ApiRepair::getCvhcMainIdForSearch($IN_IdDrepairinformwage_genaral_only);
                $IN_cvhcmain_wage_genaral_only = [] ; // ซ่อมทั่วไปอย่างเดียว
                foreach ($Listdatadrepairgenaral_only as $value){
                    $IN_cvhcmain_wage_genaral_only[] =  $value['cvhc_main_id']; ;

                }

                $IN_cvhcmain_wage_genaral_only = implode(",", $IN_cvhcmain_wage_genaral_only);

                $Time_Group_Mount_Wage_Genaral_Only = ApiRepair::getCvhcMainIdForSearchReportYear($GET_Company,$IN_cvhcmain_wage_genaral_only);
            }else{

                $Time_Group_Mount_Wage_Genaral_Only = 0;
            }

            ///////////// ซ่อมทั่วไป+เช็คระยะ /////////////

            $drepair_inform_id_wage_intersectimplode = [] ;
            foreach ($drepair_inform_id_wage_intersect as $value){

                $drepair_inform_id_wage_intersectimplode[] =  $value ;

            }

             $IN_IdDrepairinformwage_intersect = implode(",", $drepair_inform_id_wage_intersectimplode);

            $CountDrepairInformintersect = sizeof($drepair_inform_id_wage_intersect);

            if($CountDrepairInformintersect>0){

                $Listdatadrepairintersect = ApiRepair::getCvhcMainIdForSearch($IN_IdDrepairinformwage_intersect);
                $IN_cvhcmain_wage_intersect = [] ; // ซ่อมทั่วไป+เช็คระยะ
                foreach ($Listdatadrepairintersect as $value){
                    $IN_cvhcmain_wage_intersect[] =  $value['cvhc_main_id']; ;

                }

                $IN_cvhcmain_wage_intersect = implode(",", $IN_cvhcmain_wage_intersect);

                $Time_Group_Mount_Wage_wage_intersect = ApiRepair::getCvhcMainIdForSearchReportYear($GET_Company,$IN_cvhcmain_wage_intersect);
            }else{

                $Time_Group_Mount_Wage_wage_intersect = 0;
            }

            $session = Yii::$app->session;

            $session->set('dataTime_Wage_Distance_Only', $Time_Group_Mount_Wage_Distance_Only);
            $session->set('dataTime_Wage_Genaral_Only', $Time_Group_Mount_Wage_Genaral_Only);
            $session->set('dataTime_Wage_intersect', $Time_Group_Mount_Wage_wage_intersect);
            $session->set('selected_company', $selected_company);
            $session->set('selected_year', $selected_year);


            return $this->render('avg_repair_report',[
                'dataTime_Wage_Distance_Only' => $Time_Group_Mount_Wage_Distance_Only,
                'dataTime_Wage_Genaral_Only' => $Time_Group_Mount_Wage_Genaral_Only,
                'dataTime_Wage_intersect' => $Time_Group_Mount_Wage_wage_intersect,
                'query'=>true,
                'selected_company'=>$selected_company,
                'selected_year'=>$selected_year,
            ]);
        }else{
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('avg_repair_report',['query'=>false,'selected_company'=>$selected_company,
                'selected_year'=>$selected_year]);

        }

    }
    public function actionReceive_sa_repair()
    {
        $postCompany = Yii::$app->request->post();
        $GET_Company = $postCompany['company'];

        $type_company = $GET_Company ;
        $postReservation = Yii::$app->request->post();
        $months = ($postReservation['reservation']);

        $dayEnd = substr($months, 13, 2);
        $MonthEnd = substr($months, 16, -5);
        $yearEnd = substr($months, 19);
        $dayStart = substr($months, 0, -21);
        $MonthStart = substr($months, 3, -18);
        $yearStart = substr($months, 6, -13);
        $str_start = (strtotime("$dayStart-$MonthStart-$yearStart"));
        $str_end = (strtotime("$dayEnd-$MonthEnd-$yearEnd"));

        $StartDate = "$yearStart-$MonthStart-01";
        //$lastDate = date("t", strtotime($StartDate));//แสดงวันทั้งหมดของเดือนๆนั้น
        $EndDate = "$yearEnd-$MonthEnd-$dayEnd";
        $_arrRET = Helper::calculateSunday($StartDate, $EndDate);

        $ID_SaDerepair = ApiRepair::getNameUser($str_start,$str_end);

        $NameUser = $id = $SaOfMonth = $timeID = $totalSA = $sumTime = $Repair = [];
        foreach ($ID_SaDerepair as $value) {
            $NameUser[$value['cvhc_main_id']] = [$value['NameUse'], $value['id_derepair'], $value['id_card']];
            $Id_repaire[$value['id_derepair']] = [$value['NameUse'], $value['cvhc_main_id']];
        }

        $Time = ApiRepair::getTime();
        foreach ($Time as $value) {
            $timeID[] = $value['id_derepair'];
            $sumTime[$value['id_derepair']][] = $value['sum(time)'];
        }

        $ID_Company = ApiRepair::getIdCompany($type_company);
        foreach ($ID_Company as $value) { //รหัส index main_id
            $id[] = $value['cvhc_main_id'];
        }
        foreach ($id as $checkID) { // ชื่อของพนังงานที่อยู่บริษัทนั้นๆ (cvhc_id)
            if (array_key_exists($checkID, $NameUser)) {
                $SaOfMonth[$checkID]['name'] = $NameUser[$checkID][0];
                $SaOfMonth[$checkID]['id_derepair'] = $NameUser[$checkID][1];
                $SaOfMonth[$checkID]['id_card'] = $NameUser[$checkID][2];
            }
        }

        foreach ($SaOfMonth as $index => $value) {
            $totalRepair[$value['name']][] = $value['name'];
        }
        foreach ($SaOfMonth as $SA) {
            $Repair[$SA['id_derepair']]['name'] = $SA['name'];
        }
        if (empty($Repair)) {

            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('sa_repair_day',['query'=>false,]);
        } else {
            foreach ($timeID as $checkIdTime) { //หาชื่อ และ เวลารับแจ้งซ้อม
                if (array_key_exists($checkIdTime, $Repair)) {
                    $totalSA[$checkIdTime][] = $Repair[$checkIdTime]['name'];
                    if (array_key_exists($checkIdTime, $sumTime)) {
                        $totalSA[$checkIdTime][] = $sumTime[$checkIdTime][0];
                    }
                }
            }
            $arr = [];
            foreach ($totalSA as $index => $value) {
                $arr[$value[0]]['time'][] = $value[1];
            }
            foreach ($arr as $index => $value) {
                $arr_Time[$index] = array_sum($value['time']);

            }

            $keyTime = array_keys($arr_Time);
            foreach ($keyTime as $name) {
                if (array_key_exists($name, $totalRepair)) {
                    $Sa_ofMonth[$name][] = $totalRepair[$name];
                    if (array_key_exists($name, $arr)) {
                        $Sa_ofMonth[$name][] = $arr[$name]['time'];
                    }
                }
            }

            foreach ($Sa_ofMonth as $key => $value) {
                $fullname[] = $key;
                $sum[] = count($value[0]);
                $time[] = array_sum($value[1]);
                $avgdate[] = $_arrRET['total_workday']>0 ? count($value[0])/$_arrRET['total_workday'] :0;
                $avgtime[] = count($value[0])>0 ? array_sum($value[1])/count($value[0]) :0;
            }
        }
        $selected_company = $type_company;
        $selected_months = $months;

        $session = Yii::$app->session;

        $session->set('fullname', $fullname);
        $session->set('sum', $sum);
        $session->set('time', $time);
        $session->set('avgdate', $avgdate);
        $session->set('avgtime', $avgtime);
        $session->set('selected_company', $selected_company);
        $session->set('selected_months', $selected_months);



        return $this->render('sa_repair_day',[
            'fullname' => $fullname,
            'sum' => $sum,
            'time' => $time,
            'avgdate' => $avgdate,
            'avgtime' => $avgtime,
            'query'=>true,
            'selected_company'=>$selected_company,
            'selected_months'=>$selected_months,
        ]);

    }


    public function actionReceive_repair_month()
    {
        $postCompany = Yii::$app->request->post();
        $type_company = $postCompany['company'];
        // $type_company = ApiReport::TypeCompany($GET_Company);
        $postYear = Yii::$app->request->post();
        $year = $postYear['year'];
        $postmonth = Yii::$app->request->post();
        $months = $postmonth['month'];
        if (strlen($months) == 1) {
            $months = "0" . $months;
        } else {
            $months;
        }
        $str_year = "'%$year[0]-$months-%d'";
        $StartDate = "$year[0]-$months-01";
        $dayEnd = date("t", strtotime("$StartDate"));//แสดงวันทั้งหมดของเดือนๆนั้น
        $EndDate = "$year[0]-$months-$dayEnd";
        $_arrRET = Helper::calculateSunday($StartDate, $EndDate);

        $ID_SaDerepair = ApiRepair::getIDSaDerepairActionMonth($str_year);

        $NameUser = $id = $SaOfMonth = $timeID = $totalSA = $sumTime = $Repair = [];
        foreach ($ID_SaDerepair as $value) {
            $NameUser[$value['cvhc_main_id']] = [$value['NameUse'], $value['id_derepair'], $value['id_card']];
            $Id_repaire[$value['id_derepair']] = [$value['NameUse'], $value['cvhc_main_id']];
        }

        $Time = ApiRepair::getTime();

        foreach ($Time as $value) {
            $sumTime[$value['id_derepair']][] = $value['sum(time)'];
        }

        $ID_Company = ApiRepair::getIdCompany($type_company);
        foreach ($ID_Company as $value) { //รหัส index main_id
            $id[] = $value['cvhc_main_id'];
        }
        foreach ($id as $checkID) { // ชื่อของพนังงานที่อยู่บริษัทนั้นๆ (cvhc_id)
            if (array_key_exists($checkID, $NameUser)) {
                $SaOfMonth[$checkID]['name'] = $NameUser[$checkID][0];
                $SaOfMonth[$checkID]['id_derepair'] = $NameUser[$checkID][1];
                $SaOfMonth[$checkID]['id_card'] = $NameUser[$checkID][2];
            }
        }
        foreach ($SaOfMonth as $index => $value) {
            $totalRepair[$value['name']][] = $value['name'];
        }
        foreach ($SaOfMonth as $SA) {
            $Repair[$SA['id_derepair']]['name'] = $SA['name'];
        }
        if (empty($Repair)) {
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('sa_repair_month',['query'=>false,]) ;
        } else {
            foreach ($sumTime as $index => $value) { //หาชื่อ และ เวลารับแจ้งซ้อม
                if (array_key_exists($index, $Repair)) {
                    $totalSA[$index][] = $Repair[$index]['name'];
                    $totalSA[$index][] = $sumTime[$index][0];
                }
            }
            $arr = [];
            foreach ($totalSA as $index => $value) {
                $arr[$value[0]]['time'][] = $value[1];
            }
            foreach ($arr as $index => $value) {
                $arr_Time[$index] = array_sum($value['time']);
            }
            $keyTime = array_keys($arr_Time);
            foreach ($keyTime as $name) {
                if (array_key_exists($name, $totalRepair)) {
                    $Sa_ofMonth[$name][] = $totalRepair[$name];
                    if (array_key_exists($name, $arr)) {
                        $Sa_ofMonth[$name][] = $arr[$name]['time'];
                    }
                }
            }
            foreach ($Sa_ofMonth as $key => $value) {
                $fullname[] = $key;
                $sum[] = count($value[0]);
                $time[] = array_sum($value[1]);
                $avgdate[] = $_arrRET['total_workday']>0 ? count($value[0])/$_arrRET['total_workday'] :0;
                $avgtime[] = count($value[0])>0 ? array_sum($value[1])/count($value[0]) :0;
            }
        }

        $selected_company = $type_company;
        $selected_months = $months;
        $selected_year = $year['0'];


        $session = Yii::$app->session;

        $session->set('fullname', $fullname);
        $session->set('sum', $sum);
        $session->set('time', $time);
        $session->set('avgdate', $avgdate);
        $session->set('avgtime', $avgtime);
        $session->set('selected_company', $selected_company);
        $session->set('selected_months', $selected_months);
        $session->set('selected_year', $selected_year);



        return $this->render('sa_repair_month',[
            'fullname' => $fullname,
            'sum' => $sum,
            'time' => $time,
            'avgdate' => $avgdate,
            'avgtime' => $avgtime,
            'query' => true,
            'selected_company'=>$selected_company,
            'selected_months'=>$selected_months,
            'selected_year'=>$selected_year,
        ]);
    }

    public function actionReceive_avg_repair()
    {
        $postCompany = Yii::$app->request->post();
        $type_company  = $postCompany['company'];
        //$type_company = ApiReport::TypeCompany($GET_Company);
        $postMoths = Yii::$app->request->post();
        $Start_months =$select_startmonths= $postMoths['startmonth'];
        $postMoths = Yii::$app->request->post();
        $End_months=$select_endmonths = $postMoths['endmonth'];
        $postYear = Yii::$app->request->post();
        $GET_Year = $postYear['year'];
        $yearAD = $GET_Year[0]; //เปลี่ยน พ.ศ => ค.ศ



        if(isset($End_months) && isset($Start_months) && $End_months < $Start_months){

            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('avg_repair',['query'=>false,]);
        }else{


            $x = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
            for ($i = 0; $i < 12; $i++) {
                if ($x[$i] >= $Start_months and $x[$i] <= $End_months) {
                    $totalMonth[] = date("t", strtotime("$yearAD-$x[$i]-01"));
                    if (strlen($x[$i]) == 1) {
                        $numMonth[] = "0" . $x[$i];
                    } else {
                        $numMonth[] = $x[$i];
                    }
                } //$totalMonth จำนวนวันทั้งหมดของเดือนนั้นๆ
            }

            for ($i = 0; $i < count($totalMonth); $i++) {
                $unitMonth[] = strtotime("$yearAD-$numMonth[$i]-$totalMonth[$i]");
            } //เปลี่ยนวันที่รับมาให้เป็น unitTime

            $endMonth = end($totalMonth);

            if (strlen($Start_months) == 1) {
                $Start_months = "0" . $Start_months;
            } else {
                $Start_months;
            }
            if (strlen($End_months) == 1) {
                $End_months = "0" . $End_months;
            } else {
                $End_months;
            }

            $str_StarDate = strtotime("$yearAD-$Start_months-01");
            $str_EndDate = strtotime("$yearAD-$End_months-$endMonth");

            $NameUser = $Company = $SaAndCompany = $totalSA = $sumTime = $timeID = [];
            //$sql_Sa = id sa, id_derepair

            $ID_SaDerepair = ApiRepair::getIdSaDerepair($str_StarDate,$str_EndDate,$type_company);
            foreach ($ID_SaDerepair as $value) {
                $NameUser[$value['id_derepair']]['name'] = $value['NameUse'];
                $NameUser[$value['id_derepair']]['month'] = $value['month'];

            }

            $Time = ApiRepair::getTime();

            foreach ($Time as $value) {
                $sumTime[$value['id_derepair']]['time'] = $value['sum(time)'];
            }

            foreach ($NameUser as $index => $value) { //หาเวลาของพนังงาน (drepaire_ID)
                if (array_key_exists($index, $sumTime)) {
                    $totalTime[$value['month']]['name'] = $value['name'];
                    $totalTime[$value['month']]['worktime'] = $sumTime[$index]['time'];
                }
            }
            if (empty($totalTime)) {
                Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
                return $this->render('avg_repair',['query'=>false,]);
            } else {
                for ($i = 0; $i < count($unitMonth); $i++) {
                    $begin = strtotime("$yearAD-$numMonth[$i]-01");
                    foreach ($totalTime as $index => $value) {
                        if ($index >= $begin and $index <= $unitMonth[$i]) {
                            $groupby[$numMonth[$i]][] = $value['worktime'];
                        }
                    }
                }

                $select_company = $type_company;
                $select_year= $GET_Year;


                $session = Yii::$app->session;

                $session->set('groupby', $groupby);
                $session->set('selected_company', $select_company);
                $session->set('selected_months', $select_startmonths);
                $session->set('selected_year', $select_endmonths);
                $session->set('select_year', $select_year);


                return $this->render('avg_repair',[
                    'groupby' => $groupby,
                    'query' => true,
                    'selected_company'=>$select_company,
                    'select_startmonths'=>$select_startmonths,
                    'select_endmonths'=>$select_endmonths,
                    'selected_year'=>$select_year,
                ]);
            }
        }

    }

    public function actionBtn_reportsaday()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportsaday'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportsamonth()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportsamonth'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportavg()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportavg'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportsar()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportsar'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportavgr()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportavgr'));
        $mpdf->Output();
        exit;

    }
}