<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "MAIN_EDUCATION".
 *
 * @property integer $EDU_ID_NUM
 * @property integer $EDU_CUS_NO
 * @property string $EDU_NAME
 * @property string $EDU_ADDRESS
 * @property string $EDU_DETAIL
 * @property integer $EDU_TYPE
 */
class Maineducation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MAIN_EDUCATION';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_new_cusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EDU_CUS_NO', 'EDU_NAME', 'EDU_ADDRESS', 'EDU_DETAIL', 'EDU_TYPE'], 'required'],
            [['EDU_CUS_NO', 'EDU_TYPE'], 'integer'],
            [['EDU_ADDRESS'], 'string'],
            [['EDU_NAME'], 'string', 'max' => 150],
            [['EDU_DETAIL'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EDU_ID_NUM' => 'auto run',
            'EDU_CUS_NO' => 'Edu  Cus  No',
            'EDU_NAME' => 'ชื่อสถานศึกษา',
            'EDU_ADDRESS' => 'ที่อยู๋',
            'EDU_DETAIL' => 'รายละเอียด',
            'EDU_TYPE' => '1ประถม, 2.ม.ต้น,3.ม.ปลาย/ปวช,4.ตรี ,5.โท,6.เอก,7.อื่นๆ',
        ];
    }
    public function searchmaineducationcusformobile($cusno)
    {

        $sql = "select * from MAIN_EDUCATION where EDU_CUS_NO = :cusno ORDER BY MAIN_EDUCATION.EDU_ID_NUM ASC";
        //echo ;
        $showdataeducus = Yii::$app->dbERP_new_cusdata
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
        return $showdataeducus;
 
    }


}
