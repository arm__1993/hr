<?php

use yii\helpers\Html;
use app\api\DateTime;
use app\api\Utility;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$dataitem = $data;
$sumpaidmount = 0;
$sumpaidtax = 0;
$company_idcard = str_split($datahead[0]['tax_regis_code']);
$branchNumber = str_split((strlen($datahead[0][branch_no]) < 5) ? str_pad($datahead[0][branch_no], 5, "0", STR_PAD_LEFT) : $datahead[0][branch_no]);

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/payroll_report_pdf.css");
$imghr = Yii::$app->request->baseUrl . '/images/wshr';


?>
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .f-size {
        font-size: 20px !important;
    }

    .fo-size {
        font-size: 20px !important;
    }

    .fon-size {
        font-size: 20px !important;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }

    .textbox_value {
        font-family: "THSarabun";
        font-size: 13px;
    }

    .text_title {
        font-family: "THSarabun";
        font-size: 14px;
    }

    .text_italic {
        font-style: italic;
    }

    .text_header {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        text-align: center !important;
    }

    .text_header2 {
        font-family: "THSarabun" !important;
        font-size: 20px !important;
        font-weight: bold;
        text-align: center !important;
    }

    .container-page {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 200px;
        padding: 3px 3px;
    }

    .addr_company {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 80px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .addr_pay {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 100%;
        height: 120px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .income_detail {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 100%;
        height: 330px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .pay_in {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 80%;
        height: 100px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        float: left;
        padding: 0px 7px;

    }

    .pay_in2 {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /*border: 1px solid #2A2E31;*/
        width: 17%;
        height: 115px;
        /* border: 1px solid #2A2E31; */
        /*border-radius: 8px;*/
        float: right;
        padding: 0px 7px;
    }

    .pay_by {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 30px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .warning {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 46%;
        height: 300px;
        /* border: 1px solid #2A2E31; */
        border-radius: 0px;
        float: left;
        padding: 0px 7px;
    }

    .sign {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 50%;
        height: 300px;
        /* border: 1px solid #2A2E31; */
        border-radius: 0px;
        float: right;
        padding: 0px 7px;
    }

    td {
        font-family: "THSarabun" !important;
        font-size: 14px !important;
    }

    .bod {
        background-image: url('https://scontent.fbkk1-3.fna.fbcdn.net/v/t1.0-9/38126643_2183521141689528_2569118921001009152_n.jpg?_nc_fx=fbkk1-2&_nc_cat=0&oh=af97bc756dd37051b6ef52bcc3372370&oe=5C01704B');
        background-repeat: no-repeat;
        background-position: 80% 97%;

        background-attachment: fixed;
        background-size: 200px
    }

    .circle {
        width: 550px;
        height: 50px;
        border-radius: 10%;
        font-family: "THSarabun" !important;
        font-size: 50px;
        color: #000000;
        line-height: 30px;
        text-align: center;
        background: #ccd1de
    }

    p.thick {
        font-weight: bold;
    }

    .a {

        -ms-transform: rotate(-90deg); /* IE 9 */
        -webkit-transform: rotate(-90deg); /* Safari 3-8 */
        transform: rotate(-90deg);
    }


</style>
<div class="container">
    <div>
        <table width='100%'>
            <tr>
                <td width='20%'>
                    <table>
                        <tr>
                            <td><p style="font-size: 30px">ใบแนบ</p>
                            </td>
                            <td><p style="font-size: 35px"><strong>ภ.ง.ด.1</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width='80%'>
                    <table>
                        <tr>
                            <td><p style="font-size: 20px"><strong> เลขประจำตัวผู้เสียภาษีอากร</strong>
                                    <i>(ของผู้มีหน้าที่หักภาษี ณ ที่จ่าย)</i></p></td>
                            <td>
                                <table border='1'
                                       style="border-collapse: collapse; border: 2px solid #ccc padding-top:0px"
                                       class='borderunset' cellpadding='5' rowpadding='1' cellspacing='0' width='100%'>
                                    <tr>
                                        <td><?php echo $id_card_extract[1];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[2];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[3];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[4];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[5];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[6];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[7];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[8];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[9];?></td>
                                        <td><?php echo $id_card_extract[10];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[11];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[12];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[13];?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div>

        <div class="pay_in">
            <table>
                <tr>
                    <td>
                        <p style="font-size: 18px"><i>(ให้แยกกรอกรายการใน<strong>ใบแนบ</strong>นี้ตามเงินได้แต่ละประเภท โดยใส่เครื่องหมาย
                            “<img height="10" width="10"
                                  src="<?php echo $imghr; ?>/checkbox.png"
                                  class="img-circle">” ลงใน “<img height="10"
                                                                  width="10"
                                                                  src="<?php echo $imghr; ?>/uncheck.png"
                                                                  class="img-circle">” หน้าข้อความแล้วแต่กรณี
                            เพียงข้อเดียว)</i></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style='padding-top:-10px ;'>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top"><p style="font-size: 18px"><strong>ประเภทเงินได้</strong>
                                                </p></td>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php $m=1; echo ($m == 1) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(1)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (1)</b>
                                                เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-size: 18px"></td>
                                            <td valign="top" style="font-size: 18px">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 2) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(2)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (1)</b>
                                                เงินเดือน ค่าจ้าง ฯลฯ
                                                <br style="font-size: 18px"> กรณีได้รับอนุมัติจากกรมสรรพากรให้หักอัตรา
                                                <b style="font-size: 18px">ร้อยละ 3</b>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 3) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(3)</strong></td>
                                            <td valign="top" style="font-size: 18px">เงินได้ตาม <b>มาตรา 40 (1) (2)</b>
                                                กรณีนายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงาน
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 4) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(4)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 5) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(5)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย</b>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
        </div>

        <div class="pay_in2">

            <div align='right'>
                <table width='100%'>
                    <tr>
                        <td style='text-align:right'><p><strong>สาขาที่</strong></p></td>
                        <td valign='right' style='width:30%'>
                            <table border='1' style="border-collapse: collapse; border: 1px solid #ccc;border-top: 0px;border-bottom: 0px;  padding-top:0px" cellpadding='1' rowpadding='2' cellspacing='0'>
                                <tr>
                                    <td style="width:15px"><p>0</p></td>
                                    <td style="border-left:1px;width:15px"><p>0</p></td>
                                    <td style="border-left:0px;width:15px"><p>0</p></td>
                                    <td style="border-left:0px;width:15px"><p>0</p><br></td>
                                    <td>1</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
            <table style='valign:bottom;padding-top: 30px;font-size: 20px' height='100%'>
                <!-- <tr>
                <td style='font-size:8pt;padding-bottom:-15px;'>
                    <center><?php /*echo $count_page; */ ?></center>
                </td>
                <td style='font-size:8pt;padding-bottom:-15px'>
                    <center><?php /*echo $all_page; */ ?></center>
                </td>
            </tr>-->
                <tr style='font-size:20px'>
                    <td>

                        <p>แผ่นที่  <?php echo $page; ?>  </p>
                    </td>
                    <td>
                        <p>ในจำนวน <?php echo $count ?> แผ่น</p>
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <div>
        <table border='1' style='margin-top:3px;width:100%;border-collapse: collapse;margin-top:3px;' cellpadding='1'
               rowpadding='2' cellspacing='0'>
            <tr>
                <td rowspan='2' align='center'>
                    ลำดับที่
                </td>
                <td align='center'>
                    <strong>เลขประจำตัวผู้เสียภาษีอากร</strong><i>(ของผู้มีเงินได้)</i>
                </td>
                <td colspan='2' style='width:15%' align='center'>
                    รายละเอียดเกี่ยวกับการจ่ายเงิน
                </td>
                <td rowspan='2' align='center'>
                    จำนวนเงินภาษีที่หัก และนำส่งในครั้งนี้
                </td>
                <td rowspan='2'>

                    <p class="a" style="transform: rotate(-90deg);">เงื่อนไข *</p>

                </td>
            </tr>
            <tr align='center'>
                <td align='center'>
                    <strong>ชื่อผู้มีเงินได้</strong><i>(ให้ระบุชัดเจนว่าเป็น นาย นาง นางสาว หรือยศ)</i>
                </td>
                <td align='center'>
                    วัน เดือน ปี ที่จ่าย
                </td>
                <td align='center'>
                    จำนวนเงินได้ที่จ่ายในครั้งนี้
                </td>
            </tr>
            <?php
           // $totle_paid_amount=1;
            $totle_paid_tax=0;
            $L = 0;
            foreach ($arrmains as $i => $v) {

                $L++;
                $_idcard = $v['emp_idcard'];
                //for($i=0;$i<=4;$i++){
                $totle_paid_amount2 += $arrmains[$i]['income_amount'];
                $totle_paid_amount += $arrmains[$i]['tax_amount'];

                $_emp_data  =  $emp_data[$_idcard];
                $empname = $_emp_data['BeName'];
                $Surname = $_emp_data['Surname'];
                $address = $_emp_data['address'];
                $dataemp_idcard = $_emp_data['array_idcard'];

                ?>

                <tr>
                    <td>
                        <center><?php print($i+1); ?></center>
                    </td>
                    <td>
                        <table border='0' width='100%' style='font-size:9pt'>
                            <tr>
                                <td colspan='2'>
                                    <center><table border='1'
                                           style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc padding-top:0px"
                                           class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0'
                                           width='80%'>
                                        <tr>
                                            <td><?php echo $dataemp_idcard[1]; ?></td>
                                            <td style="border:0px;">-</td>
                                            <td><?php echo $dataemp_idcard[2]; ?></td>
                                            <td style="border-left:0px;"><?php echo $dataemp_idcard[3]; ?></td>
                                            <td style="border-left:0px;"><?php echo $dataemp_idcard[4]; ?></td>
                                            <td style="border-left:0px;"><?php echo $dataemp_idcard[5]; ?></td>
                                            <td style="border:0px;">-</td>
                                            <td><?php echo $dataemp_idcard[6]; ?></td>
                                            <td style="border-left:0px;"><?php echo $dataemp_idcard[7]; ?></td>
                                            <td style="border-left:0px;"><?php echo $dataemp_idcard[8]; ?></td>
                                            <td style="border-left:0px;"><?php echo $dataemp_idcard[9]; ?></td>
                                            <td><?php echo $dataemp_idcard[10]; ?></td>
                                            <td style="border:0px;">-</td>
                                            <td><?php echo $dataemp_idcard[11]; ?></td>
                                            <td style="border-left:0px;"><?php echo $dataemp_idcard[12]; ?></td>
                                            <td style="border:0px;">-</td>
                                            <td><?php echo $dataemp_idcard[13]; ?></td>
                                        </tr>

                                    </table></center>
                                </td>
                            </tr>
                            <tr>
                                <td width='50%' style='padding-bottom:-15px'
                                    align='center'><?php echo $empname; ?></td>
                                <td width='50%' style='padding-bottom:-15px'
                                   >   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Surname; ?></td>
                            </tr>
                            <center>  <tr>
                                <td width='50%' sytle='valign:left'  align='center'> ชื่อ..............................................................
                                </td>
                                <td width='50%' sytle='valign:left'>
                                    ชื่อสกุล...........................................................
                                </td>
                            </tr></center>
                        </table>

                    </td>

                    <td>
                        <br>
                        <center style='padding-bottom:-15px;fint-size:9pt;'><?php echo $sign_date; ?></center>
                        ............................................
                    </td>
                    <td style='text-align:right;fint-size:9pt;'>
                        <br>
                      <center><p style='padding-bottom:-15px;'><?php echo \app\api\Helper::displayDecimal($arrmains[$i]['income_amount']); ?></p></center>
                        ....................................................
                    </td>
                    <td style='text-align:right;fint-size:9pt;'>
                        <br>
                      <center><p style='padding-bottom:-15px;'><?php echo \app\api\Helper::displayDecimal($arrmains[$i]['tax_amount']); ?></p></center>
                        .......................................................................
                    </td>
                    <td>
                        <br>
                        <center>  <br> .........</center>
                    </td>
                </tr>
                <?php $sumpaidmount += $dataitem[$i]['paid_amount'];
                $sumpaidtax += $dataitem[$i]['paid_tax'];
            } ?>
            <tr>
                <td colspan='3' align='right' style='padding-bottom:-15px;border-bottom:0px'>
                </td>
                <td style='padding-bottom:-15px;border-bottom:0px' align='center'><?php echo \app\api\Helper::displayDecimal($totle_paid_amount2); ?></td>
                <td style='padding-bottom:-15px;border-bottom:0px' align='center'><?php echo \app\api\Helper::displayDecimal($totle_paid_amount); ?></td>
            </tr>
            <tr>
                <td colspan='3' align='right' style=' border-right:0px;border-top: 0px ' >
                    รวมยอดเงินได้และภาษีที่นำส่ง <i>(นำไปรวมกับ</i><strong>ใบแนบ ภ.ง.ด.1</strong><i>แผ่นอื่น (ถ้ามี))</i>
                </td>
                <td style='border-top:0px'><center>...........................................</center></td>
                <td style='border-top:0px;border-right: 0px'><center>..............................................................</center>
                <td style="border-right: 0px;border-top:0px;border-bottom:0px"></td>
            </tr>
            <tr>
                <td colspan='3' align='left'>
                    <table>
                        <tr>
                            <td colspan='2'>(ให้กรอกลำดับที่ต่อเนื่องกันไปทุกแผ่นตามเงินได้แต่ละประเภท)</td>
                        </tr>
                        <tr>
                            <td><strong><u>หมายเหตุ</u> * </strong></td>
                            <td> เงื่อนไขการหักภาษีให้กรอกดังนี้</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>หัก ณ ที่จ่าย กรอก <strong>1</strong></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>ออกให้ตลอดไป กรอก <strong>2</strong></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>ออกให้ครั้งเดียว กรอก <strong>3</strong></td>
                        </tr>
                    </table>
                </td>
                <td colspan='3' align='center'>
                    <table style='border: 1px solid #fff'>
                        <tr>
                            <td rowspan='4'><img height="60" width="60" src="<?php echo $imghr; ?>/stamp-icon.png"
                                                 class="img-circle"></td>
                            <td>
                                ลงชื่อ..................................................................................ผู้จ่ายเงิน
                            </td>
                        </tr>
                        <tr>
                            <td> (................................................................................)</td>
                        </tr>
                        <tr>
                            <td>ตำแหน่ง.............................................................................
                            </td>
                        </tr>
                        <tr>
                            <td>ยื่นวันที่...........เดือน..................................พ.ศ. ...................
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
    </div>
    <hr style='margin-top:0px'>
    <div style='width:45%;float:right;text-align:right;font-size:9pt;margin-top:-15px'><p><strong>
            พิมพ์ <?php echo DateTime::mappingMonthContraction(date('m')) . (date('Y') + 543); ?></strong></p></div>
    <div style='width:45%;float:left;text-align:lefts;font-size:9pt;margin-top:-5px'><p>
          <i> <strong>สอบถามข้อมูลเพิ่มเติมได้ที่ศูนย์สารนิเทศสรรพากร</strong> <img style='padding-bottom:-5px' height='20'
                                                                     src="<?php echo $imghr; ?>/210360_WHT1_kor.pdf.png"
                                                                     class="img-circle"><strong> โทร. 1161</strong></i></p></div>
</div>