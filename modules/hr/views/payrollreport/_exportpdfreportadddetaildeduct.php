<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;
$modelEmp = $session->get('modelEmp');
$modelTemp = $session->get('modelTemp');
$modelSalary = $session->get('modelSalary');
$modelAdddeductdtail = $session->get('modelAdddeductdtail');
$modelSumTotal = $session->get('modelSumTotal');
$statusSalary = $session->get('statusSalary');
$querySalaryTotalThisMonth3 = $session->get('querySalaryTotalThisMonth3');
$monthselect = $session->get('monthselect');


?>
<h3 style="text-align:center;">รายละเอียดรายงานเงินเดือน</h3>

<div class="row">
    <div class="row">
    </div>
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-6">
            <h3><?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                //print_r($getMonth);
                $monthGetReport1 = $getMonth['monthresult'];
                $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
                $yearGetReport1 = $getMonth['year'];
                ?>รายงานสรุปผลเงินเดือนรายบุคคล (รายการเพิ่ม) จ่าย <?php echo $resultMonthReport1;
                echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h3>
        </div>
    </div>
    <?php if ($querySalaryTotalThisMonth3) { ?>
        <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example1_wrapper" width="80%">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                    aria-sort="ascending" width="20%">
                    ชื่อพนักงาน
                </th>
                <?php foreach ($modelEmp as $key => $value) { ?>
                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                        aria-sort="ascending" width="10%">
                        <?php echo $value ['Fullname']; ?>
                    </th>
                <?php } ?>
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                    aria-sort="ascending" width="30%">
                    รวม
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if ($statusSalary) { ?>
                <tr>
                    <td>เงินเดือน</td>
                    <?php

                    foreach ($modelEmp as $key2 => $values) {
                        ?>
                        <td align="right"><?php
                            $sumsalary = '0.00';
                            foreach ($modelSalary AS $key => $valueResultSalary) {
                                switch ($valueResultSalary['WAGE_EMP_ID']) {
                                    case $valueResultSalary['WAGE_EMP_ID'] == $values['ID_Card'];
                                        $sumsalary = $valueResultSalary['WAGE_SALARY'];
                                        break;
                                }

                            }
                            echo Helper::displayDecimal($sumsalary);
                            $somrowsalary[] = $sumsalary;

                            ?></td>
                    <?php } ?>
                    <td><?php echo Helper::displayDecimal(array_sum($somrowsalary)); ?></td>
                </tr>
            <?php } ?>
            <?php foreach ($modelTemp as $key1 => $value) {
                $somrow = [];
                ?>
                <tr>
                    <td><?php echo $value['ADD_DEDUCT_TEMPLATE_NAME']; ?></td>
                    <?php foreach ($modelEmp as $key2 => $values) { ?>
                        <td align="right"><?php
                            $setZero = "0.00";
                            foreach ($modelAdddeductdtail AS $key => $valueResult) {
                                switch ($valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] AND $valueResult['WAGE_EMP_ID']) {
                                    case $valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $value['ADD_DEDUCT_TEMPLATE_ID'] && $valueResult['WAGE_EMP_ID'] == $values['ID_Card'];
                                        $setZero = $valueResult['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                                        break;
                                }

                            }
                            echo Helper::displayDecimal($setZero);
                            $somrow[] = $setZero;

                            ?></td>
                    <?php } ?>
                    <td><?php echo Helper::displayDecimal(array_sum($somrow)); ?></td>
                </tr>
                <?php ?>
            <?php } ?>
            <tr>
                <td><B>รวมรับ</B></td>
                <?php foreach ($modelEmp as $key2 => $values) { ?>
                    <td align="right"><?php
                        $setZeroSum = "0.00";
                        foreach ($modelSumTotal AS $key => $valueResultSum) {
                            switch ($valueResultSum['WAGE_EMP_ID']) {
                                case $valueResultSum['WAGE_EMP_ID'] == $values['ID_Card'];
                                    $setZeroSum = $valueResultSum['SUM_ADD_DEDUCT_HIS'] + $valueResultSum['WAGE_SALARY'];
                                    break;
                            }

                        }
                        echo Helper::displayDecimal($setZeroSum);
                        ?></td>
                <?php } ?>
            </tr>
            </tbody>
        </table>
    <?php } ?>

</div>
