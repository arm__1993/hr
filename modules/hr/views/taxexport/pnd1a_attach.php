<?php

use yii\helpers\Html;
use app\api\DateTime;
use app\api\Utility;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPDF;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$dataitem = $data;
$sumpaidmount = 0;
$sumpaidtax = 0;
$company_idcard = str_split($datahead[0]['tax_regis_code']);
$branchNumber = str_split((strlen($datahead[0][branch_no]) < 5) ? str_pad($datahead[0][branch_no], 5, "0", STR_PAD_LEFT) : $datahead[0][branch_no]);

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/payroll_report_pdf.css");
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$dataitem = array();
$dataitem['emp_firstname'] = ''; //'วัชรพันธ์ ';
$dataitem['emp_lastname'] = '';//'นาย วัชรพันธ์ ผัดดี';
$arrayDetail = array();
$arrayDetail['emp_address'] = '';// '99 หมู่9 ตำบล เก้า'
?>
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .f-size {
        font-size: 20px !important;
    }

    .fo-size {
        font-size: 20px !important;
    }

    .fon-size {
        font-size: 20px !important;
    }

    .rpt td {
        border: 0.5px solid #2b000a;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #0c0c0c;
    }

    .rpt tr.even {
        background: #ffffff;
    }

    .textbox_value {
        font-family: "THSarabun";
        font-size: 13px;
    }

    .text_title {
        font-family: "THSarabun";
        font-size: 14px;
    }

    .text_italic {
        font-style: italic;
    }

    .text_header {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        text-align: center !important;
    }

    .text_header2 {
        font-family: "THSarabun" !important;
        font-size: 20px !important;
        font-weight: bold;
        text-align: center !important;
    }

    .container-page {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 200px;
        padding: 3px 3px;
    }

    .addr_company {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 80px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .addr_pay {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 100%;
        height: 120px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .income_detail {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 100%;
        height: 330px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .pay_in {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 80%;
        height: 100px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        float: left;
        padding: 0px 7px;

    }

    .pay_in2 {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /*border: 1px solid #2A2E31;*/
        width: 17%;
        height: 115px;
        /* border: 1px solid #2A2E31; */
        /*border-radius: 8px;*/
        float: right;
        padding: 0px 7px;
    }

    .pay_by {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 30px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .warning {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 46%;
        height: 300px;
        /* border: 1px solid #2A2E31; */
        border-radius: 0px;
        float: left;
        padding: 0px 7px;
    }

    .sign {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 50%;
        height: 300px;
        /* border: 1px solid #2A2E31; */
        border-radius: 0px;
        float: right;
        padding: 0px 7px;
    }

    td {
        font-family: "THSarabun" !important;
        font-size: 14px !important;
        /*border: 0.5px solid #2b000a ;*/
    }

    .bod {
        background-image: url('https://scontent.fbkk1-3.fna.fbcdn.net/v/t1.0-9/38126643_2183521141689528_2569118921001009152_n.jpg?_nc_fx=fbkk1-2&_nc_cat=0&oh=af97bc756dd37051b6ef52bcc3372370&oe=5C01704B');
        background-repeat: no-repeat;
        background-position: 80% 97%;

        background-attachment: fixed;
        background-size: 200px
    }

    .circle {
        width: 550px;
        height: 50px;
        border-radius: 10%;
        font-family: "THSarabun" !important;
        font-size: 50px;
        color: #000000;
        line-height: 30px;
        text-align: center;
        background: #ccd1de
    }

    p.thick {
        font-weight: bold;
    }

    .a {

        -ms-transform: rotate(-90deg); /* IE 9 */
        -webkit-transform: rotate(-90deg); /* Safari 3-8 */
        transform: rotate(-90deg);
    }


</style>
<div class="container">

    <div>
        <table width='100%'>
            <tr>
                <td width='20%'>
                    <table>
                        <tr>
                            <td><p style="font-size: 30px">ใบแนบ</p>
                            </td>
                            <td><p style="font-size: 35px"><strong>ภ.ง.ด.1ก</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width='80%'>
                    <table>
                        <tr>
                            <td><p style="font-size: 20px"><strong> เลขประจำตัวผู้เสียภาษีอากร</strong>
                                    <i>(ของผู้มีหน้าที่หักภาษี ณ ที่จ่าย)</i></p></td>
                            <td>
                                <table border='1'
                                       style="border-collapse: collapse; border: 2px solid #ccc padding-top:0px"
                                       class='borderunset' cellpadding='5' rowpadding='1' cellspacing='0' width='100%'>
                                    <tr>
                                        <td><?php echo $id_card_extract[1];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[2];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[3];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[4];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[5];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[6];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[7];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[8];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[9];?></td>
                                        <td><?php echo $id_card_extract[10];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[11];?></td>
                                        <td style="border-left:0px;"><?php echo $id_card_extract[12];?></td>
                                        <td style="border:0px;">-</td>
                                        <td><?php echo $id_card_extract[13];?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div>

        <div class="pay_in">
            <table>
                <tr>
                    <td>
                        <p style="font-size: 18px">(ให้แยกกรอกรายการในใบแนบนี้ตามเงินได้แต่ละประเภท โดยใส่เครื่องหมาย
                            “<img height="10" width="10"
                                  src="<?php echo $imghr; ?>/checkbox.png"
                                  class="img-circle">” ลงใน “<img height="10"
                                                                  width="10"
                                                                  src="<?php echo $imghr; ?>/uncheck.png"
                                                                  class="img-circle"> ” หน้าข้อความแล้วแต่กรณี
                            เพียงข้อเดียว)</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style='padding-top:-10px ;'>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top"><p style="font-size: 18px"><strong>ประเภทเงินได้</strong>
                                                </p></td>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php $m=1; echo ($m == 1) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(1)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (1)</b>
                                                เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-size: 18px"></td>
                                            <td valign="top" style="font-size: 18px">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 2) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(2)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (1)</b>
                                                เงินเดือน ค่าจ้าง ฯลฯ
                                                <br style="font-size: 18px"> กรณีได้รับอนุมัติจากกรมสรรพากรให้หักอัตรา
                                                <b style="font-size: 18px">ร้อยละ 3</b>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 3) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(3)</strong></td>
                                            <td valign="top" style="font-size: 18px">เงินได้ตาม <b>มาตรา 40 (1) (2)</b>
                                                กรณีนายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงาน
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 4) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(4)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <img height="18" width="18"
                                                     src="<?php echo ($m == 5) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                     class="img-circle">
                                            </td>
                                            <td valign="top" style="font-size: 18px"><strong>(5)</strong></td>
                                            <td valign="top" style="font-size: 18px"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย</b>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
        </div>

        <div class="pay_in2">

            <div align='right'>
                <table width='100%'>
                    <tr>
                        <td style='text-align:right'><p><strong>สาขาที่</strong></p></td>
                        <td valign='right' style='width:30%'>
                            <table border='1'
                                   style="border-collapse: collapse; border: 1px solid #ccc;border-top: 0px;border-bottom: 0px;  padding-top:0px"
                                   cellpadding='1' rowpadding='2' cellspacing='0'>
                                <tr>
                                    <td><p>0</p></td>
                                    <td style="border-left:1px;"><p>0</p></td>
                                    <td style="border-left:0px;"><p>0</p></td>
                                    <td style="border-left:0px;"><p>0</p><br></td>
                                    <td>1</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
            <table style='valign:bottom;padding-top: 30px;font-size: 20px' height='100%'>
                <!-- <tr>
                <td style='font-size:8pt;padding-bottom:-15px;'>
                    <center><?php /*echo $count_page; */ ?></center>
                </td>
                <td style='font-size:8pt;padding-bottom:-15px'>
                    <center><?php /*echo $all_page; */ ?></center>
                </td>
            </tr>-->
                <tr style='font-size:20px'>
                    <td>
                        <p>แผ่นที่ <?php echo $page?></p>
                    </td>
                    <td>
                        <p>ในจำนวน <?php echo $count ?> แผ่น</p>
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <div>
        <table WIDTH="100%" border='1'
               style=' solid #ccc;width:100%;font-size:9pt;border-collapse: collapse;margin-top:3px; margin-bottom:10px'
               cellpadding='1' rowpadding='2' cellspacing='1'>
            <tr>
                <td rowspan='2' align='center' border='1'>
                    <p>ลำดับที่</p>
                </td>
                <td style='width:25%' align='center' rowspan='2'>
                    <p><strong>เลขประจำ ตัวผู้เสียภาษีอากร</strong><i>(ของผู้มีเงินได้)</i></p>
                </td>
                <td align='center' style='width:33%'>
                    <p><strong>ชื่อผู้มีเงินได้</strong><i>(ให้ระบุชัดเจนว่าเป็น นาย นาง นางสาว หรือยศ)</i></p>
                </td>
                <td rowspan='2' style='width:15%' align='center'>
                    <p>จำนวนเงินได้ที่จ่ายทั้งปี</p>
                </td>
                <td rowspan='2' style='width:15%' align='center'>
                    <p><strong>จำนวนเงินได้ที่หัก<br>และนำส่งทั้งปี</strong>
                </td>
                <td rowspan='2' align='center'>

                    <p class="a" style="transform: rotate(-90deg);">เงื่อนไข *</p>

                </td>
            </tr>
            <tr align='center'>

                <td align='center' style="">
                    <p><strong>ที่อยู่ของผู้มีเงินได้</strong><i>(ให่ระบุ ตรอก/ซอย ถนน ตำบล/แขวง อำเถอ/เขต จังหวัด)</i>
                    </p>
                </td>
            </tr>
            <?php
            // $totle_paid_amount=1;
            $totle_paid_tax = 0;
            $l = 0;
            foreach ($arrmains as $i => $v) {

            $l++;
                $_idcard = $v['emp_idcard'];
                //for($i=0;$i<=4;$i++){

                $totle_paid_amount += $arrmains[$i]['income_total_amount'];
                $totle_paid_tax2 += $arrmains[$i]['tax_total_amount'];


                $_emp_data  =  $emp_data[$_idcard];
                $empname = $_emp_data['BeName'];
                $Surname = $_emp_data['Surname'];
                $address = $_emp_data['address'];
                $dataemp_idcard = $_emp_data['array_idcard'];

                ?>
                <tr style="border: 1px solid #000000;border-top:0px">
                    <td valign='top' style="border: 1px solid #000000;border-top:0px">
                        <center>
                            <?php print($i+1); ?>
                        </center>
                    </td>
                    <td valign='top' colspan='2'>

                        <table border='1'
                               style="font-size:9pt;border-collapse: collapse; float: left;  padding-top:0px "
                               class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0'
                        >
                            <tr>
                                <td><?php echo $dataemp_idcard[1]; ?></td>
                                <td style="border:0px;">-</td>
                                <td><?php echo $dataemp_idcard[2]; ?></td>
                                <td style="border-left:0px;"><?php echo $dataemp_idcard[3]; ?></td>
                                <td style="border-left:0px;"><?php echo $dataemp_idcard[4]; ?></td>
                                <td style="border-left:0px;"><?php echo $dataemp_idcard[5]; ?></td>
                                <td style="border:0px;">-</td>
                                <td><?php echo $dataemp_idcard[6]; ?></td>
                                <td style="border-left:0px;"><?php echo $dataemp_idcard[7]; ?></td>
                                <td style="border-left:0px;"><?php echo $dataemp_idcard[8]; ?></td>
                                <td style="border-left:0px;"><?php echo $dataemp_idcard[9]; ?></td>
                                <td><?php echo $dataemp_idcard[10]; ?></td>
                                <td style="border:0px;">-</td>
                                <td><?php echo $dataemp_idcard[11]; ?></td>
                                <td style="border-left:0px;"><?php echo $dataemp_idcard[12]; ?></td>
                                <td style="border:0px;">-</td>
                                <td><?php echo $dataemp_idcard[13]; ?></td>
                            </tr>
                            <th style="border:0px;">

                            <td style="border:0px;"><p>
                                    <?php $_emp_firstname = ($empname) ? $empname : '.....................................................'; ?>
                                    <strong>ชื่อ</strong> &nbsp;&nbsp;&nbsp; <?php echo $_emp_firstname ?> &nbsp;&nbsp;&nbsp;

                                                                        <?php $_emp_lastname = ($Surname) ? $Surname : '..........................................';?>
                                                                        <strong>ชื่อสกุล</strong> &nbsp;&nbsp;&nbsp;
                                    <?php echo $_emp_lastname?> </p>
                            </td>
                            </th>

                            </th>
                        </table>
                        <table style=" float: right;">
                            <tr>

                            </tr>


                            <tr>
                                <td colspan='2' style=''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php $_emp_address = ($address) ? $address : '...............................................................................................'; ?>
                                    <strong>ที่่อยู่</strong> <?php echo $_emp_address ?>
                                </td>
                            </tr>
                        </table>

                    </td>

                    <td style="border: 1px solid #000000; border-top: 0px">
                        <br>
                        <center style='padding-bottom:-15px'>
                            <?php echo \app\api\Helper::displayDecimal($arrmains[$i]['income_total_amount']); ?>
                        </center>
                        ............................................
                    </td>
                    <td style="border: 1px solid #000000; border-top: 0px">
                        <br>
                        <center style='padding-bottom:-15px'>
                            <?php echo \app\api\Helper::displayDecimal($arrmains[$i]['tax_total_amount']); ?>
                        </center>
                        ........................................
                    </td>
                    <td style="border: 1px solid #000000">
                        <center>
                            <br> .........
                        </center>
                    </td>


                </tr>
            <?php } ?>
            <tr>
                <td colspan='3' align='right' style='padding-bottom:-15px;border-bottom:0px'>
                </td>
                <td style='padding-bottom:-15px;border-bottom:0px' align='center'>
                    <?php echo \app\api\Helper::displayDecimal($totle_paid_amount) ?>
                </td>
                <td style='padding-bottom:-15px;border-bottom:0px' align='center'>
                    <?php echo \app\api\Helper::displayDecimal($totle_paid_tax2) ?>
                </td>
            </tr>
            <tr style="border: 1px solid #000000;border-top: 0px">
                <td colspan='3' align='right' style='border-top:0px'>
                    <p><strong>รวม</strong>ยอดเงินได้และภาษีที่นำส่ง <i>(นำ ไปรวมกับ</i><strong>ใบแนบ ภ.ง.ด.1ก</strong>
                        <i>แผ่นอื่น (ถ้ามี))</i></p>
                </td>

                <td style='border-top:0px;'>
                    <center>................................................</center>
                </td>
                <td style='border-top:0px'>
                    <center>...........................................</center>
                </td>
                <td>
            </tr>
            <tr>
                <td colspan='6'>
                    <table>
                        <tr>
                            <td colspan='3' align='left'
                                style='padding-left:10px;border: 1px solid #000000;border-top:0px;border-left:0px;border-bottom:0px;'
                                width='60%'>
                                <table>
                                    <tr>
                                        <td colspan='2'><p><i>(ให้กรอกลำดับที่ต่อเนื่องกันไปทุกแผ่นตามเงินได้แต่ละประเภท)</i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td><strong>หมายเหตุ * </strong></td>
                                        <td><p>เงื่อนไขการหักภาษีให้กรอกดังนี้</p></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><p>หัก ณ ที่จ่าย กรอก<strong>1</strong></p></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><p> ออกให้ตลอดไป กรอก<strong> 2 </strong></p></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><p>ออกให้ครั้งเดียว กรอก<strong> 3 </strong></p></td>
                                    </tr>
                                </table>
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>


                            <td colspan='3' align='center'>
                                <table style='border: 1px solid #fff;'>
                                    <tr>
                                        <td rowspan='4'><img height="60" width="60"
                                                             src="<?php echo $imghr; ?>/stamp-icon.png"
                                                             class="img-circle"></td>
                                        <td>
                                            <p>
                                                ลงชื่อ..................................................................................ผู้จ่ายเงิน</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>
                                                (................................................................................)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>
                                                ตำแหน่ง.............................................................................</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p>ยื่นวันที่...........เดือน..................................พ.ศ.
                                                ...................</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>

        </table>
    </div>


    <div style='width:45%;float:right;text-align:right;font-size:9pt;margin-top:-20px'><p>
            <strong> พิมพ์ <?php echo DateTime::mappingMonthContraction(date('m')) . (date('Y') + 543); ?></strong></p>
    </div>
    <div style='width:45%;float:left;text-align:lefts;font-size:9pt;margin-top:-5px'><p>
            <strong><i> สอบถามข้อมูลเพิ่มเติมได้ที่ศูนย์สารนิเทศสรรพากร <img style='padding-bottom:-5px' height='20'
                                                                             src="<?php echo $imghr; ?>/210360_WHT1_kor.pdf.png"
                                                                             class="img-circle">โทร. 1161</i></strong>
        </p></div>
</div>