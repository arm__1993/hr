<?php

namespace app\modules\hr\apihr;
/*
*			ตั้งค่า
*			require_once ../gen_menu/GenerateMenu.php;
*
*			เรียกใช้
*			$genx = new GenerateMenu(30,$config['Member']);//programID,dataBase
*			$genx -> setAdmin(array('157xxxxxxxxxx'));//	กำหนด admin
*			$menu_list = $genx -> getMenuList();
*
*/
/**
* Created by PhpStorm.
* User: adithep
* Date: 2/1/2017 AD
* Time: 10:50
*/



use app\api\Common;
use app\modules\hr\models\Menu;
use app\modules\hr\models\MenuManage;
use app\modules\hr\models\Place;
use app\modules\hr\models\Position;
use app\api\DBConnect;
use Yii;
class ApiMenu {
    
    #param
    /*****************/
    private $programID 	= 42;
    private $programIDS 	= 42;
    private $dataBase 		= '';
    private $tbManage 		= 'menu_manage';
    private $tbProcess 		= 'menu_process';
    private $tbMenu 		= 'menu';
    private $show_all		= false;
    private $headChk 		= 0;
    private $process_html = array();
    private $adminlist		= array();
    private $sort_ranking	= false;
    /*****************/
    private $idCard 			= null;
    private $company 		= null;
    private $department 	= null;
    private $section 		= null;
    private $position 		= null;
    public $siteURL = '';
    /*****************/
   
    #initail function
    public function __construct($program_id = null,$db_name = null,$URL){
        #	ตรวจสอบ session
        $this->show_all = Yii::$app->request->get('show_all');
        $this->siteURL = $URL;
        if(!$this->setUseSession()){
            echo 'กรุณา login ให้ถูกต้อง';
            exit();
        }
        #	ตรวจสอบ ตัวแปร
        if($program_id == null || $db_name == null){
            echo 'กรุณาใส่ข้อมูลเบื้องต้นให้ครบ!';
            exit();
        }
        
        #	กำหนดค่าเบื้องต้น
        $arr_set_param		= array();
        $arr_set_param['programID'] 	= $program_id;
        $arr_set_param['dataBase'] 		= $db_name;
        $this->setParam($arr_set_param);
        #	กำหนด วิธีการ direct link ( process )
        $this->genProcessValue();
        #	ตรวจสอบสิทธิ์ admin
        $this->checkAdmin();
        
    }
    private function setParam($arr_info){
        foreach($arr_info AS $k => $val){
            $this->$k = $val;
        }
    }
    public function getParam($param_name = ''){
        return $this->$param_name;
    }
    public function getMenuList(){
        
        $showAll = $this->show_all;#	control admin
        
        $sql 		= "SELECT m.id,m.parent_id,m.name FROM 	".$this->dataBase.".".$this->tbMenu."	AS m  ";
        $sql      .= ($showAll) ? "":" LEFT JOIN 		".$this->dataBase.".".$this->tbManage." 	AS mm  ";
        $sql 		.= ($showAll) ? "":" ON (m.id = mm.menu_id) ";
        $sql 		.= " WHERE 1";
        $sql 		.= ($showAll) ? "":" AND mm.status 	!= '99'  ";
        $sql 		.= ($showAll) ? "":" AND ";
        $sql 		.= ($showAll) ? "":" ( mm.working_company_id 			= '".$this->company."' ";
        $sql 		.=	($showAll) ? "": " 	OR mm.department_id 			= '".$this->department."' ";
        $sql 		.= ($showAll) ? "":" 	OR mm.section_id 					= '".$this->section."' ";
        $sql 		.= ($showAll) ? "":" 	OR mm.position_id 					= '".$this->position."' ";
        $sql 		.= ($showAll) ? "":" ) ";
        $sql 		.= " AND m.status 		!= '99'  ";
        $sql 		.= " AND m.place_id = $this->programIDS ";
        #$sql 		.= " AND m.parent_id 	!= '' ";
        $sql 		.= " GROUP BY m.id ";
        $sql 		.= " ORDER BY m.rankking ";
        
    //    echo $sql.'<br>';
         $connection = \Yii::$app->dbERP_easyhr_checktime;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
        
        $arr_sent = array();

        // while($row =  $result){
		// 		$arr_sent[$row['parent_id']][$row['id']] = 'detail';
		// }
        foreach ($result as $row) {
            $arr_sent[$row['parent_id']][$row['id']] = 'detail';
        }
        
        #	เก็บโครงสร้างเมนู ทั้งหมด ลง array
        $lists_inArray =  $this->manageArrayMenu($arr_sent);
        
        // echo 'id menu<textarea style="width:400px;height:400px">';
        // print_r($lists_inArray);
        // echo '</textarea>';
        // exit();
        #	นำ array ไปจัดลำดับ ตาม ranking
        if($this->sort_ranking){
            $lists_inArray =  $this->sortByRanking($lists_inArray);
        }
        #	นำโครงสร้าง array ที่ gen ได้ ไปสร้าง html และ ใส่ link
        /* echo 'id menu<textarea style="width:400px;height:400px">';
        print_r($lists_inArray);
        echo '</textarea>'; */
        if(count($lists_inArray) == 1){
            foreach($lists_inArray AS $k => $value){
                if($k == ''){
                    $lists_inArray = $value;
                }
                break;
        }
    }else{
        unset($lists_inArray['']);
        
    }
    
    return $this->createTemplate('',$lists_inArray);
    
}
public function sortRanking($set){
    $this->sort_ranking = $set;
}
private function manageArrayMenu($arr_info ,$ck_out = 0){
    
    $arr_sent 		= array();
    $max_array 	= count($arr_info);
    $c 				= 0;
    
    /* echo '<br>begin==<br><textarea style="width:500px;height:200px">';
    print_r($arr_info);
    echo '</textarea>'; */
    foreach( $arr_info AS $key => $arr_v ){
        $ck_out++;
        #echo '<br>count:'.$ck_out.'<br>';
        
        $sql	= "SELECT id,parent_id FROM ".$this->dataBase.".".$this->tbMenu." WHERE id='".$key."' AND status != '99'  ORDER BY rankking   limit 1";
        // $rt	= mysql_query($sql);
        // $r		= mysql_fetch_assoc($rt);
         $connection = \Yii::$app->dbERP_easyhr_checktime;
        $command = $connection->createCommand($sql);
        $r = $command->queryAll();
        if($r[0]['parent_id'] == ''){
            #	node บนสุด
            
            #comment
            #echo '<br>a '.$sql.'<br>';
            #--
            
            #$arr_sent[$key] = $arr_v;
            foreach($arr_v AS $km => $v){
                $arr_sent[$key][$km] = 'detail';
            }
            
            #comment
            /* echo '<br>aaaaa arr_sent=<br><textarea style="width:400px;height:200px">';
            print_r($arr_sent);
            echo '</textarea>'; */
            #--
            $c++;
        }else{
            #	หา parent id ที่อยู่ใน array parant มิติ ที่ ใดๆ
            
            
            $rew = 	$this->findParantMatrixIndex($arr_sent,$r[0]['parent_id'],$key,$arr_v);
            
            #comment
            /* echo '<br>after<br><textarea style="width:400px;height:200px">';
            print_r($rew);
            echo '</textarea>'; */
            #--
            
            if(!is_array($rew)){
                $arr_sent[$r[0]['parent_id']][$key] = $arr_v;
            }else{
                $arr_sent		= $rew;
            }
            $c++;
            
            #comment
            /* echo '<br>arr_sent<br><textarea style="width:400px;height:200px">';
            print_r($arr_sent);
            echo '</textarea>'; */
            #--
        }
        
    }
    /* echo 'arr_sent<textarea style="width:400px;height:400px">';
    print_r($arr_sent);
    echo '</textarea>'; */
    
    if($max_array == $c || $ck_out > 500){
        return $arr_sent;
    }else{
        return $this->manageArrayMenu($arr_sent,$ck_out);
    }
}
private function findParantMatrixIndex($arr_sent,$new_parant_id,$id_now,$arr_val){
    
    $observ_status = false;
    $arr_new = array();
    if(!is_array($arr_sent) || count($arr_sent) >0){
        foreach($arr_sent AS $k => $val){
            if(is_array($val)){
                if($k == $new_parant_id){
                    
                    $arr_sent[$k][$id_now] 		= $arr_val;
                    $observ_status 	= true;
                    
                    break;
            }else{
                #	ส่งไปหาในมิติถัดไป
                
                $rew =  $this->findParantMatrixIndex($val,$new_parant_id,$id_now,$arr_val);
                if(!is_array($rew)){
                    $observ_status = false;
                    
                }else{
                    $arr_sent[$k] 		= $rew;
                    $observ_status 	= true;
                    break;
            }
            
        }
    }
}
}else{
    $arr_sent[$new_parant_id][$id_now] = $arr_val;
    $observ_status = true;
}

if(!$observ_status){
    #	ถ้า parent ไม่ตรงกับ parent ใน array ให้สร้าง parent ใหม่
    return false;
}else{
    return $arr_sent;
}


}
private function createTemplate($class,$v,$control_swap = false){
    
    $menu_lists = "";
    
    if(is_array($v)){
        foreach($v AS $k1 => $v1){
            if(is_array($v1)){
                $menu_lists .= $this-> createTemplateList($class,$k1,'parent',$v1);
            }else{
                $menu_lists .= $this-> createTemplateList($class,$k1,'child');
            }
        }
    }else{
        $menu_lists .= $this-> createTemplateList($class,$v,'child');
    }
    
    return $menu_lists;
    
}
private function createTemplateList($class,$id,$level,$v_ofparant = array()){
    
    $sql 	= "SELECT name,menu_desc,process_id,link,id FROM ".$this->dataBase.".".$this->tbMenu." WHERE id='".$id."'   limit 1";
    // $rt 	= mysql_query($sql);
    // $r 	= mysql_fetch_assoc($rt);
     $connection = \Yii::$app->dbERP_easyhr_checktime;
    $command = $connection->createCommand($sql);
    $r = $command->queryOne();
    
    if($level == 'parent'){
        $menu_lists = "<li class=\"".$class."\">";
        $menu_lists .= "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>";
        $menu_lists .= ($class=='')?"".$r['name']." <b class=\"caret\"></b></a>":"<i class=\"fa fa-hand-o-right\"></i> ".$r['name']." </a>";
        $menu_lists .= "<ul class='dropdown-menu'> ".$this->createTemplate('dropdown-submenu',$v_ofparant)." </ul>";
        $menu_lists .= "</li>";
        $this->i++;
    }
    else if($level == 'child'){
        
        
        #$patt  			=  "/.*('|\"){1}(?<name>.*)\.php(?<val>.*)(').*/";
        $patt  			=  "/.php\?/";
        if(preg_match($patt,$r['link'])){
            $r['link'] .= "&menu_id=".$r['id'];
        }else{
            $r['link'] .= "?menu_id=".$r['id'];
        }
        //<ul  class='dropdown-submenu'></ul>
        $menu_lists = "<li><a href='".$this->siteURL.$this->process_html[$r['process_id']][0].$r['link'].$this->process_html[$r['process_id']][1]."'/><i class=\"fa fa-circle-thin\"></i> ".$r['name']." </a></li>";
    }
    return $menu_lists;
}

private function setUseSession(){
    $this->idCard 		= $_SESSION['SESSION_ID_card'];
    $this->company 		= $_SESSION['SESSION_Working_Company'];
    $this->department 	= $_SESSION['SESSION_Department'];
    $this->section 		= $_SESSION['SESSION_Section'];
    $this->position 		= $_SESSION['SESSION_Position_id'];
    
    
    #print_r($_SESSION);
    
    
    if($this->idCard == null || $this->company == null ||$this->department == null ||$this->section == null ||$this->position == null ){
        return false;
    }else{
        return true;
    }
}
private function genProcessValue(){
    $sql = "SELECT * FROM ".$this->dataBase.".".$this->tbProcess." WHERE status != '99' ";
    // $rt = mysql_query($sql);
     $connection = \Yii::$app->dbERP_easyhr_checktime;
    $command = $connection->createCommand($sql);
    $result = $command->queryAll();
    // while($r = $result){
        
    //     $this->process_html[$r['id']] = explode('$url',$this->regular_onclick($r['process']));
        
    // }
    foreach ($result as $key => $r) {
        $this->process_html[$r['id']] = explode('$url',$this->regular_onclick($r['process']));
    }
}
private function regular_onclick($link){
    $return = str_replace('onClick','onclick',$link);
    $return = str_replace('\"','"',$return);
    return $return;
}
private function regular_links($link){
    $patt = "/(onClick|onclick)=\"(?<str>.*)(;\")>/";
    if(preg_match_all($patt,$link, $matches)){
        
        $return = 'onclick="'.$matches['str'][0].'"';
    }else{
        $return = '';
    }
    
    return $return;
}
public function setAdmin($arr_list){
    foreach($arr_list AS $no => $idc){
        $this->adminlist[] = $idc;
    }
    $this->checkAdmin();
}
private function checkAdmin(){
    foreach($this->adminlist AS $no => $idc){
        if($idc == $this->idCard){
            $this->show_all = true;
            break;
    }
}

}
private function sortByRanking($arr_id = array()){
    $arr_ranking = array();
    /* 	echo 'id menu<textarea style="width:400px;height:400px">';
    print_r($arr_id);
    echo '</textarea>';  */
    
    #	สร้าง array ranking จาก array id run
    $arr_ranking = $this->createArrayPototype($arr_id);
    
    /* 	echo 'ranking menu<textarea style="width:300px;height:400px">';
    print_r($arr_ranking);
    echo '</textarea>';  */
    
    #	sort key array ranking ทุกมิติ
    $arr_ranking = $this->sortByKeyEachLevel($arr_ranking);
    
    /* echo 'sort ranking<textarea style="width:300px;height:400px">';
    print_r($arr_ranking);
    echo '</textarea>';  */
    
    #	convert ranking array คืนเป็น id run array
    $arr_id = $this->convertRankingToId($arr_ranking);
    return $arr_id;
    
}
private function sortByKeyEachLevel($arr){
    
    ksort($arr);
    foreach($arr AS $key => $val){
        if(is_array($val)){
            $arr[$key] = $this->sortByKeyEachLevel($val);
        }
    }
    return $arr;
}
private function sortKeyArrays($arr_ranking,$arr_id){
    return $arr_ranking;
}
private function createArrayPototype($arr_id){
    
    $new_array = array();
    foreach($arr_id AS $id => $val){
         $connection = \Yii::$app->dbERP_easyhr_checktime;
        $sql 	= "SELECT rankking FROM ".$this->dataBase.".".$this->tbMenu." WHERE id='".$id."'   limit 1";
        $command = $connection->createCommand($sql);
        $r = $command->queryAll();
        
        $new_array[$r['rankking']] = $val;
        if(is_array($val)){
            $new_array[$r['rankking']] = $this->createArrayPototype($val);
        }
    }
    
    return $new_array;
}
private function convertRankingToId($arr_ranking){
    
    $new_array = array();
    foreach($arr_ranking AS $id => $val){
         $connection = \Yii::$app->dbERP_easyhr_checktime;
        $sql 	= "SELECT id,rankking FROM ".$this->dataBase.".".$this->tbMenu." WHERE rankking='".$id."'   limit 1";
        // $rt 	= mysql_query($sql);
        // $r 	= mysql_fetch_assoc($rt);
        $command = $connection->createCommand($sql);
        $r = $command->queryAll();
        
        $new_array[$r['id']] = $val;
        if(is_array($val)){
            $new_array[$r['id']] = $this->convertRankingToId($val);
        }
    }
    
    return $new_array;
}

}


?>