<?php

/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/16/2017 AD
 * Time: 09:05
 */

namespace app\components\validator;

use yii\validators\Validator;

class TimeValidator extends Validator
{
    public function validateValue($value){
        if (str_word_count($value) < $this->size) {
            return ['The number of words must be less than {size}', ['size' => $this->size]];
        }
        return false;
    }
}