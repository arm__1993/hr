<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/9/2017 AD
 * Time: 07:55
 */

namespace app\modules\gitreport\apigitreport;


class ApiGitReport
{
    public static function calculatePercentMileStone($arrIssue)
    {
        if(is_array($arrIssue)) {
            $issue = [];
            $total = 0;
            foreach ($arrIssue as $key => $value) {
                $issue[$key] = $value;
                $total += $value;
            }

            $not_complete = $issue[1] + $issue[2] + $issue[3] +  $issue[4];
            $complete = $issue[5]+$issue[6]+$issue[7];
            $percent = ($total > 0) ? ($complete * 100) / $total : 0;
            return \app\api\Utility::RoundUp($percent);
        }
        else return 0;

    }

    public static function calculatePercentProject($arrIssue)
    {

        if(is_array($arrIssue)) {
            $issue = [];
            $total = 0;
            foreach ($arrIssue as $key => $value) {
                $issue[$key] = $value;
                $total += $value;
            }

            $not_complete = $issue[1] + $issue[2] + $issue[3] +  $issue[4];
            $complete = $issue[5]+$issue[6]+$issue[7];
            $percent = ($total > 0) ? ($complete * 100) / $total : 0;
            return \app\api\Utility::RoundUp($percent);
        }
        else return 0;
    }


}