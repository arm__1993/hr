<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:48
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;




AppAsset::register($this);

$this->registerCssFile(Yii::$app->request->BaseUrl . "/js/hr/bootstrap-treeview/dist/bootstrap-treeview.min.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/bootstrap-treeview/dist/bootstrap-treeview.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/menulink.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/menulink.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/permission/jscolor.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java






?>




<style>
    #modal-form-add-place .modal-body {
        overflow-y: visible;
    }

    .howl-iconpicker .geticonval {
        background: #fcfcfc;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin: 5px;
        width: 48px;
        height: 48px;
        overflow: hidden;
        float: left;
    }
</style>


<input type="hidden">



<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดการสิทธิ์</a>
                </li>
                <li class="active">เมนูและลิงค์</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">





            <div id="icon"></div>


            <!--programs name-->
            <div class="col-sm-4">
                <div class="panel panel-success">
                    <div class="panel-body">
                        <div class="row col-sm-12">
                            <!--<h4><b>รายชื่อโปรแกรม</b></h4>-->
                            <label class="txt-head-1">
                                รายชื่อโปรแกรม
                            </label>
                            <div class="pull-right action-buttons">
                                <button type="button" id="" class="btn btn-success btn-xs"
                                        onclick="showModelFormPlace(this,'new',event)"/>
                                <i class="glyphicon glyphicon-plus"></i>
                                เพิ่ม
                                </button>
                            </div>
                        </div>
                        <div class="row col-sm-12" style="margin-top:10px">

                            <input type="hidden" value="" id="place_id_sel">

                            <ul class="list-group program-lists-ul">

                                <?php
                                foreach ($places AS $no => $place) {

                                    echo Yii::$app->controller->renderPartial('programlists', [
                                        'place' => $place
                                    ]);

                                }
                                ?>


                            </ul>


                        </div>
                    </div>
                </div>
            </div>

            <!--manage-->
            <div class="col-sm-8">
                <div class="panel panel-success">
                    <div id="manage-data-content" style="display:none" class="panel-body">
                        <div class="col-sm-12">
                            <label class="txt-head-1 col-sm-offset-5">
                                จัดการเมนู
                            </label>
                        </div>
                        <div class="col-sm-12" id="manage_menu" style="border-bottom:1px solid #ccc;margin-top:15px">
                            <label class="pull-left" id="manage_menu_title">
                                โปรแกรมบุคคล (EASYHR)
                            </label>
                            <label class="pull-right action-buttons">
                                <button type="button" onclick="showModelFormMenu('add_parent')" id=""
                                        class="btn btn-primary btn-xs"
                                        onclick=""/>
                                <i class="glyphicon glyphicon-plus"></i>
                                เพิ่มเมนู
                                </button>
                            </label>
                        </div>
                        <div class="col-sm-12" id="manage_menu_list" style="padding-top:20px">
                            <div class="pre-hole-text-load">

                                รอสักครู่..

                            </div>
                        </div>

                        <div class="col-md-offset-5">
                            <input type="button" class="btn btn-success" onclick="saveMenu()" value="บันทึกข้อมูล">

                        </div>

                    </div>


                    <div id="pre-load" class="panel-body">

                        <div class="pre-hole-text">

                            <span class="glyphicon glyphicon-triangle-left"></span>
                            เลือกโปรแกรมด้านซ้าย

                        </div>

                    </div>


                </div>
            </div>

        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->



<div class="modal fade modal-wide"
     id="modal-form-add-place"
     tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true"
     data-backdrop="static" data-keyboard="false">

    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มโปรแกรม</h4>
            </div>
            <div class="modal-body" width="100%">

                <div id="form-add-place">
                    <form id="manudetal" role="form" class="form-horizontal" data-sel-now="">

                        <input type="hidden" name="id" value=""/>

                        <div class="row" >
                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="name">ชื่อโปรแกรม:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"
                                           id="name"
                                           name="name"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="path">path:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"
                                           id="path"
                                           name="path"/>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="IP">IP:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"
                                           id="IP"
                                           name="IP"/>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3 " for="Color">Color:</label>
                                <div class="col-sm-9">

                                    <input  class="form-control jscolor {zIndex:9999} {hash:true}"
                                           id="Color"
                                           name="Color"  >

                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="Icon">Icon:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control input1 input "
                                           id="Icon"
                                           name="Icon"/>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" onclick="savePlaceList()" value="บันทึกข้อมูล">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade modal-wide"
     id="modal-form-add-menu"
     tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true"
     data-backdrop="static" data-keyboard="false">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มเมนู</h4>
            </div>
            <div class="modal-body" width="100%">

                <div id="form-add-menu">
                    <form role="form" class="form-horizontal" data-sel-now="" data-state-form="" data-save-runno="0">

                        <input type="hidden" name="id" value=""/>
                        <input type="hidden" name="ref_id" id="ref_id" value=""/>

                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="name">ชื่อเมนู:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"
                                           id="name"
                                           name="name"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="path">กระบวณการ:</label>
                                <div class="col-sm-9">
                                    <select id="process_id" name="process_id" class="form-control">
                                        <?php echo $addition['optionMenuProcess']; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="name">link:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"
                                           id="link"
                                           name="link"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="control-label col-sm-3" for="name">คำอธิบายเมนู:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"
                                           id="menu_desc"
                                           name="menu_desc"/>
                                </div>
                            </div>


                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" onclick="addMenuToObj()" value="บันทึกข้อมูล">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade modal-wide"
     id="modal-form-move-menu"
     tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true"

     data-backdrop="static" data-keyboard="false">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ย้ายเมนู</h4>
            </div>
            <div class="modal-body" width="100%">

                <div id="form-move-menu">
                    รอสักครู่..
                </div>

            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" onclick="moveMenu()" value="ย้ายเมนู">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

