<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:13
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

AppAsset::register($this);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/personalexport/personalexport.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/personalexport/personalexport.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li class="active">ส่งออกข้อมูลพนักงาน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
           
            <!--ค้นหาข้อมูล-->
            <div class="panel panel-success">
                <div class="panel-body">
                    <div class="col-sm-12" style="border-bottom:1px solid #ccc">
                        <h5>ค้นหาข้อมูล</h5>
                    </div>
                    <div class="col-sm-12" style="padding:20px">
                        <form role="form" class="form-horizontal" id="form_search_emp_export" >
                           
                            <div class="col-sm-6">
                                <div class="col-sm-12 form-group">
                                    <label class="control-label col-sm-2" for="company">บริษัท:</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="company" name="company" style="">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label class="control-label col-sm-2" for="department">แผนก:</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="department" name="department" style="">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label class="control-label col-sm-2" for="section">ฝ่าย:</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="section" name="section" style="">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-12 form-group">
                                    <div class="checkbox">
                                        <label class="control-label col-sm-6" style="text-align: left;">
                                            <input type="checkbox" id="" name="emp_active" checked value="1" >พนักงานปัจจุบัน</label>
                                        <label class="control-label col-sm-6" style="text-align: left;">
                                            <input type="checkbox"  id="" name="emp_out" value="1" >พนักงานลาออก</label>
                                    </div>
                                </div>
                                 <div class="col-sm-12 form-group form-inline">
                                   
                                        <label class="control-label" for="">วันที่เริ่มงาน</label>
                                        <div class="col-sm-4 input-group date">
                                            <input type="text"  readonly="readonly" class="form-control" 
                                                id="work_start_begin" 
                                                name="work_start_begin" 
                                                placeholder="dd/mm/yyyy"  />

                                            <label class="input-group-addon" for="work_start_begin" >
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </label>
                                        </div>
                                       
                                        <label class="control-label" >ถึง</label>
                                        <div class="col-sm-4  input-group date">
                                            <input type="text"  readonly="readonly" class="form-control" 
                                                id="work_start_end" 
                                                name="work_start_end" 
                                                placeholder="dd/mm/yyyy"  />

                                            <label class="input-group-addon" for="work_start_end">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </label>
                                        </div>
                                   
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>

            <!--เลือกข้อมูลที่ต้องการส่งออก-->
            <div class="panel panel-success">
                <div class="panel-body">
                    <div class="col-sm-12" style="border-bottom:1px solid #ccc">
                        <h5>เลือกข้อมูลที่ต้องการส่งออก</h5>
                         <label class="control-label pull-right" style="text-align: left;">
                            <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#border-emp-filter-field')" > เลือกทั้งหมด
                        </label> 
                    </div>
                    
                    <div class="col-sm-12" style="padding:5px">
                        <div class="nav-tabs-custom tab-filter" >
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#fill_tab_1" data-toggle="tab">
                                    ข้อมูลส่วนตัว
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_tab_2" data-toggle="tab">
                                    ตำแหน่งเงินเดือน
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_tab_3" data-toggle="tab">
                                    เปลี่ยนแปลงเงินเดือน
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_tab_4" data-toggle="tab">
                                    ลงเวลาเข้า-ออกงาน
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_tab_5" data-toggle="tab">
                                    ประวัติการทำงาน
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_tab_6" data-toggle="tab">
                                    ประกันสังคมและเงินสะสม
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_tab_7"  data-have-contend="no" data-toggle="tab">
                                    ข้อมูลเงินค้ำประกัน
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_tab_8" id="title_tab8"  data-have-contend="no" data-toggle="tab">
                                    สัญญาจ้าง
                                    </a>
                                </li>
                            </ul>

                            <div id="border-emp-filter-field"  class="tab-content">
                                <div class="tab-pane active" id="fill_tab_1">
                                    <div class="box-body">
                                        <h2>รอสักครู่..</h2>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_tab_2">
                                    <div class="box-body">
                                        <div class="box-body">
                                            <div class="row col-sm-12">
                                                <label class="control-label" style="text-align: left;">
                                                    <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_tab_2 form')" > เลือกทั้งหมด
                                                </label>    
                                            </div>
                                            <form class="form-horizontal">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="job_type" name="job_type" data-mtitle="ประเภทการจ้างงาน" > ประเภทการจ้างงาน
                                                    </label>
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $EMP_SALARY;?>"  id="EMP_SALARY_CHART" name="EMP_SALARY_CHART" data-mtitle="ผังเงินเดือน" > ผังเงินเดือน
                                                    </label>
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $EMP_SALARY;?>"  id="EMP_SALARY_LEVEL" name="EMP_SALARY_LEVEL" data-mtitle="กระบอก" > กระบอก
                                                    </label>
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $EMP_SALARY;?>"  id="EMP_SALARY_STEP" name="EMP_SALARY_STEP" data-mtitle="ขั้น" > ขั้น
                                                    </label>
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $EMP_SALARY;?>"  id="EMP_SALARY_WAGE" name="EMP_SALARY_WAGE" data-mtitle="เงินเดือน" > เงินเดือน
                                                    </label>

                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="SalaryViaBank" name="SalaryViaBank" data-mtitle="รูปแบบการรับเงินเดือน" > รูปแบบการรับเงินเดือน
                                                    </label>
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Salary_Bank" name="Salary_Bank" data-mtitle="ธนาคาร" > ธนาคาร
                                                    </label>
                                                     <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Salary_BankNo" name="Salary_BankNo" data-mtitle="เลขบัญชี" > เลขบัญชี
                                                    </label>

                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_tab_3">
                                    <div class="row col-sm-12">
                                        <label class="control-label" style="text-align: left;">
                                            <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_tab_3 form')" > เลือกทั้งหมด
                                        </label>    
                                    </div>
                                    <form class="form-horizontal">
                                        <div class="col-sm-6">
                                            <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $EMP_SALARY;?>"  id="EMP_SALARY_UPDATE_DATE" name="EMP_SALARY_UPDATE_DATE" data-mtitle="วันที่เปลี่ยนแปลงเงินเดือน" > วันที่เปลี่ยนแปลงเงินเดือน
                                            </label>
                                             <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_EFFECTIVE_DATE" name="SALARY_EFFECTIVE_DATE" data-mtitle="วันที่เงินเดือนมีผล" > วันที่เงินเดือนมีผล
                                            </label>
                                             <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_TYPE" name="SALARY_CHANGE_TYPE" data-mtitle="ลักษณะการเปลี่ยนแปลง" > ลักษณะการเปลี่ยนแปลง
                                            </label>
                                             <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_CHART_OLD" name="SALARY_CHANGE_CHART_OLD" data-mtitle="ผังเงินเดือนเดิม" > ผังเงินเดือนเดิม
                                            </label>
                                             <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_LEVEL_OLD" name="SALARY_CHANGE_LEVEL_OLD" data-mtitle="กระบอกเดิม" > กระบอกเดิม
                                            </label>
                                             <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_STEP_OLD" name="SALARY_CHANGE_STEP_OLD" data-mtitle="ขั้นเดิม" > ขั้นเดิม
                                            </label>
                                             <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_WAGE_OLD" name="SALARY_CHANGE_WAGE_OLD" data-mtitle="เงินเดือนเดิม" > เงินเดือนเดิม
                                            </label>
                                            

                                        </div>

                                        <div class="col-sm-6">
                                            <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_CHART_NEW" name="SALARY_CHANGE_CHART_NEW" data-mtitle="ผังเงินเดือนใหม่" > ผังเงินเดือนใหม่
                                            </label>
                                            <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_LEVEL_NEW" name="SALARY_CHANGE_LEVEL_NEW" data-mtitle="กระบอกใหม่" > กระบอกใหม่
                                            </label>
                                            <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_ADD_STEP" name="SALARY_CHANGE_ADD_STEP" data-mtitle="ขั้นที่เพิ่ม" > ขั้นที่เพิ่ม
                                            </label>
                                            <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_EMP_REMARK" name="SALARY_CHANGE_EMP_REMARK" data-mtitle="หมายเหตุการเพิ่มขั้น" > หมายเหตุการเพิ่มขั้น
                                            </label>
                                            <label class="control-label col-sm-12" style="text-align: left;">
                                                <input type="checkbox" data-tb="<?php echo $SALARY_CHANGE;?>"  id="SALARY_CHANGE_WAGE_NEW" name="SALARY_CHANGE_WAGE_NEW" data-mtitle="เงินเดือนใหม่" > เงินเดือนใหม่
                                            </label>

                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="tab-pane" id="fill_tab_4">
                                    <div class="box-body">
                                        <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_tab_4 form')" > เลือกทั้งหมด
                                            </label>    
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="col-sm-4">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="DayOff" name="DayOff" data-mtitle="วันหยุดสัปดาห์" > วันหยุดสัปดาห์
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Start_date" name="Start_date" data-mtitle="วันเริ่มงาน" > วันเริ่มงาน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Probation_Date" name="Probation_Date" data-mtitle="วันที่ผ่านทดลองงาน" > วันที่ผ่านทดลองงาน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Prosonnal_Being" name="Prosonnal_Being" data-mtitle="สถานะการทำงาน" > สถานะการทำงาน
                                                </label>

                                               
                                           
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="End_date" name="End_date" data-mtitle="วันที่ทำงานวันสุดท้าย" > วันที่ทำงานวันสุดท้าย
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Main_EndWork_Reason" name="Main_EndWork_Reason" data-mtitle="เหตุผลที่ลาออก" > เหตุผลที่ลาออก
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="EndWork_Reason" name="EndWork_Reason" data-mtitle="อธิบายเหตุผลที่ลาออก" > อธิบายเหตุผลที่ลาออก
                                                </label>
                                               
                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="startWorkTime" name="startWorkTime" data-mtitle="เวลาเข้างานเช้า" > เวลาเข้างานเช้า
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="beginLunch" name="beginLunch" data-mtitle="เวลาพักเที่ยง" > เวลาพักเที่ยง
                                                </label>
                                                  <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="endLunch" name="endLunch" data-mtitle="เวลาเข้างานบ่าย" > เวลาเข้างานบ่าย
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="endWorkTime" name="endWorkTime" data-mtitle="เวลาเลิกงาน" > เวลาเลิกงาน
                                                </label>
                                                  <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="use_check" name="use_check" data-mtitle="การสแกนบัตร" > การสแกนบัตร
                                                </label>
                                                
                                           
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_tab_5">
                                    <div class="box-body">
                                        <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_tab_5 form')" > เลือกทั้งหมด
                                            </label>    
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="col-sm-6">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="old_work_company" name="old_work_company" data-mtitle="บริษัท" > บริษัท
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="old_work_salary" name="old_work_salary" data-mtitle="เงินเดือน" > เงินเดือน
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="company_type" name="company_type" data-mtitle="ประเภทธุรกิจ" > ประเภทธุรกิจ
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="bonus" name="bonus" data-mtitle="โบนัส/คอมมิชั่น" > โบนัส/คอมมิชั่น
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox"  data-special="1" id="" name="age"  data-mtitle="ระยะเวลาในการทำงาน" > ระยะเวลาในการทำงาน
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="work_detail" name="work_detail" data-mtitle="ลักษณะงาน/ความรับผิดชอบ" > ลักษณะงาน/ความรับผิดชอบ
                                                </label>
                                                 
                                                
                                               
                                           
                                            </div>
                                            <div class="col-sm-6">

                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="reason_out_type" name="reason_out_type" data-mtitle="สาเหตุที่ออก" > สาเหตุที่ออก
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="reason_out_detail" name="reason_out_detail" data-mtitle="อธิบายเหตุผลที่ลาออก" > อธิบายเหตุผลที่ลาออก
                                                </label>

                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="amount_uniforms" name="amount_uniforms" data-mtitle="สวัสดิการเครื่องแบบ ชุด/ปี" > สวัสดิการเครื่องแบบ ชุด/ปี
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="amount_food" name="amount_food" data-mtitle="สวัสดิการอาหารฟรี มื้อ/วัน" > สวัสดิการอาหารฟรี มื้อ/วัน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="amount_buses" name="amount_buses" data-mtitle="สวัสดิการรถรับส่งพนักงาน บาท/เดือน" > สวัสดิการรถรับส่งพนักงาน บาท/เดือน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="amount_habitat" name="amount_habitat" data-mtitle="สวัสดิการที่พักฟรีหรือคิดค่าเช่า บาท/เดือน" > สวัสดิการที่พักฟรีหรือคิดค่าเช่า บาท/เดือน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="amount_oil" name="amount_oil" data-mtitle="สวัสดิการค่าน้ำมัน/ค่าพาหนะ บาท/เดือน" > สวัสดิการค่าน้ำมัน/ค่าพาหนะ บาท/เดือน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="amount_insurance" name="amount_insurance" data-mtitle="ประกันสุขภาพหรือค่ารักษาพยาบาล บาท/ปี" > ประกันสุขภาพหรือค่ารักษาพยาบาล บาท/ปี
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $work_history;?>"  id="other" name="other" data-mtitle="สวัสดิการอื่นๆ (ถ้ามี)" > อื่นๆ (ถ้ามี)
                                                </label>
                                                
                                           
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_tab_6">
                                    <div class="box-body">
                                        <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_tab_6 form')" > เลือกทั้งหมด
                                            </label>    
                                            </div>
                                            <form class="form-horizontal">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="socialBenefitStatus" name="socialBenefitStatus" data-mtitle="ประกันสังคม" > ประกันสังคม
                                                    </label>
                                                     <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="socialBenifitPercent" name="socialBenifitPercent" data-mtitle="ร้อยละของการจ่าย" > ร้อยละของการจ่าย
                                                    </label>
                                                     <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="benifitFundStatus" name="benifitFundStatus" data-mtitle="เงินสะสม" > เงินสะสม
                                                    </label>
                                                     <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="benefitFundPercent" name="benefitFundPercent" data-mtitle="ร้อยละของการจ่าย" > ร้อยละของการจ่าย
                                                    </label>
                                                     <label class="control-label col-sm-12" style="text-align: left;">
                                                        <input type="checkbox" data-special="1"  id="" name="sum_benefitfund" data-mtitle="ยอดเงินสะสม" > ยอดเงินสะสม
                                                    </label>

                                                </div>

                                            </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_tab_7">
                                    <div class="box-body">
                                        <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_tab_7 form')" > เลือกทั้งหมด
                                            </label>    
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="col-sm-4">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="start_money_bonds" name="start_money_bonds" data-mtitle="สถานะเงินค้ำ" > สถานะเงินค้ำ
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="start_pay_bonds" name="start_pay_bonds" data-mtitle="การชำระเงินค้ำ" > การชำระเงินค้ำ
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="money_bonds" name="money_bonds" data-mtitle="เงินค้ำประกัน" > เงินค้ำประกัน
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bank_name" name="bank_name" data-mtitle="ธนาคารเงินค้ำ" > ธนาคาร
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bank_no" name="bank_no" data-mtitle="หมายเลขบัญชีเงินค้ำ" > หมายเลขบัญชี
                                                </label>
                                                
                                            </div>
                                             <div class="col-sm-4">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_prefix" name="bondsman_prefix" data-mtitle="คำนำหน้าคนค้ำ" > คำนำหน้า
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_sex" name="bondsman_sex" data-mtitle="เพศคนค้ำ" > เพศ
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_name" name="bondsman_name" data-mtitle="ชื่อคนค้ำ" > ชื่อ
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_surname" name="bondsman_surname" data-mtitle="สกุลคนค้ำ" > สกุล
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_idcard" name="bondsman_idcard" data-mtitle="หมายเลขบัตรประชาชนคนค้ำ" > หมายเลขบัตรประชาชน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_birthday" name="bondsman_birthday" data-mtitle="วัน เดือน ปีเกิด คนค้ำ" > วัน เดือน ปีเกิด
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-special="1" id="" name="bonds_age"   data-mtitle="อายุคนค้ำ" > อายุ
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_business" name="bondsman_business" data-mtitle="อาชีพคนค้ำ" > อาชีพ
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_money_bonds;?>"  id="bondsman_work_place" name="bondsman_work_place" data-mtitle="ที่อยู่ที่ทำงานคนค้ำ" > ที่อยู่ที่ทำงาน
                                                </label>

                                                
                                            </div>
                                             <div class="col-sm-4">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox"  data-special="1" id="" name="bonds_addr1"  data-mtitle="ที่อยู่ตามสถานที่ทำงานคนค้ำ" > ที่อยู่ตามสถานที่ทำงาน
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox"  data-special="1" id="" name="bonds_addr2"  data-mtitle="ที่อยู่ตามทะเบียนบ้านคนค้ำ" > ที่อยู่ตามทะเบียนบ้าน
                                                </label>
                                                
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_tab_8">
                                    <div class="box-body">
                                        <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_tab_8 form')" > เลือกทั้งหมด
                                            </label>    
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="col-sm-12">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_promise;?>"  id="promise_sign_date" name="promise_sign_date" data-mtitle="วัน เดือน ปี เซ็นสัญญาจ้าง" > วัน เดือน ปี เซ็นสัญญาจ้าง
                                                </label>
                                                <!--<label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb=""  id="" name="" data-mtitle="วัน เดือน ปี สิ้นสุดสัญญาจ้าง" > วัน เดือน ปี สิ้นสุดสัญญาจ้าง
                                                </label>-->
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $emp_promise;?>"  id="promise_name" name="promise_name" data-mtitle="ประเภทสัญญาจ้าง" > ประเภทสัญญาจ้าง
                                                </label>
                                                <!--<label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb=""  id="" name="" data-mtitle="อายุสัญญาจ้าง" > อายุสัญญาจ้าง
                                                </label>-->
                                              
                                           
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        
            <div class="panel panel-success">
                <div class="panel-body">
                   
                        <div class="row col-sm-12">
                            <div class="col-sm-offset-4" style="padding-left:60px">
                                <button type="button" id="" class="btn btn-primary emp-btn-main-search" onclick="showTableExportEmp(false,this)" style="margin-left:10px" >
                                    <i class="glyphicon glyphicon-search emp-btn-normal"></i>
                                    <i class="fa fa-spinner fa-pulse fa-1x fa-fw emp-btn-load" style="display:none"></i>
                                    ค้นหา
                                </button>
                                <button type="button" id="" class="btn btn-success  emp-btn-main-search" style="margin-left:10px" onclick="showTableExportEmp(true,this)">
                                    <i class="glyphicon glyphicon-download-alt emp-btn-normal"></i> 
                                    <i class="fa fa-spinner fa-pulse fa-1x fa-fw emp-btn-load" style="display:none"></i>
                                    download
                                </button>
                            </div>
                            

                        </div>
                    
                </div>
            </div>

            
             <div class="panel panel-success">
                <div class="panel-body">
                   <div class="col-sm-12" style="overflow: auto">
                    <table id="showAllExportEmp"  width="100%"></table>
                  </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->

<input type="hidden" value='<?php echo $companyinfo;?>' id="fullcompanyinfo" >