<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Mainaddress;
use yii\helpers\ArrayHelper;

class MainaddressController extends ActiveController
{
    public $modelClass = 'app\modules\wscommon\models\Mainaddress';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}

	public function actionSearchgeocusformobile()
    {
        $cusno      = $_GET['cusno'];
        //$typeadd   = $_GET['typeadd'];

        $dataaddress = new Mainaddress();
        $showdataaddress = $dataaddress->searchgeocusformobile($cusno);
        return $showdataaddress;

    }


}
