<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ot_requestdetail".
 *
 * @property integer $id
 * @property integer $ot_requestmaster_id
 * @property string $id_card
 * @property string $time_start
 * @property string $time_end
 * @property string $time_total
 * @property integer $return_id
 * @property string $return_name
 * @property integer $is_check
 * @property string $check_byuser
 * @property string $check_date
 * @property string $check_time
 * @property integer $is_approved
 * @property string $unapproved_remark
 * @property string $approved_byuser
 * @property string $approved_date
 * @property string $approved_time
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $money_total
 * @property string $remak
 * @property integer $is_hr_approved
 * @property string $hr_approved_date
 * @property string $hr_approved_time
 * @property string $hr_approved_by
 */
class OtRequestdetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ot_requestdetail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ot_requestmaster_id', 'id_card', 'time_start', 'time_end', 'time_total', 'return_id', 'return_name', 'is_check', 'is_approved', 'createby_user', 'create_datetime'], 'required'],
            [['ot_requestmaster_id', 'return_id', 'is_check', 'is_approved', 'is_hr_approved'], 'integer'],
            [['time_start', 'time_end', 'check_date', 'check_time', 'approved_date', 'approved_time', 'create_datetime', 'hr_approved_date', 'hr_approved_time'], 'safe'],
            [['time_total', 'money_total'], 'number'],
            [['remak'], 'string'],
            [['id_card', 'check_byuser', 'approved_byuser', 'createby_user', 'hr_approved_by'], 'string', 'max' => 13],
            [['return_name'], 'string', 'max' => 100],
            [['unapproved_remark'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ot_requestmaster_id' => 'Ot Requestmaster ID',
            'id_card' => 'Id Card',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'time_total' => 'Time Total',
            'return_id' => 'Return ID',
            'return_name' => 'Return Name',
            'is_check' => 'Is Check',
            'check_byuser' => 'Check Byuser',
            'check_date' => 'Check Date',
            'check_time' => 'Check Time',
            'is_approved' => 'Is Approved',
            'unapproved_remark' => 'Unapproved Remark',
            'approved_byuser' => 'Approved Byuser',
            'approved_date' => 'Approved Date',
            'approved_time' => 'Approved Time',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'money_total' => 'Money Total',
            'remak' => 'Remak',
            'is_hr_approved' => 'Is Hr Approved',
            'hr_approved_date' => 'Hr Approved Date',
            'hr_approved_time' => 'Hr Approved Time',
            'hr_approved_by' => 'Hr Approved By',
        ];
    }
}
