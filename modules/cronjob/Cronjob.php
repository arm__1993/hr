<?php

namespace app\modules\cronjob;

/**
 * cronjob module definition class
 */
class Cronjob extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\cronjob\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
