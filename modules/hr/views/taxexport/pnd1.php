<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\modules\hr\apihr\ApiHr;
$company = ApiHr::getWorking_company();
if($error!=''){
    
}
$script = <<< JS
    bootbox.alert({
            //size: 'small',
            message: "<h4 class=\"btalert\">พบข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้</h4>",
            callback: function(){}
        });
JS;
$this->registerJs($script);
?>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">xxxxxx</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <form id='seartcompany'  action="pnd1pdf" method="post" data-toggle="validator">

                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                       <div class='row'>
                             <div class='col-md-2'></div>
                            <div class="form-group">
                                <label for="numberPassportEmp" style='text-align:right' class="col-sm-1 control-label">บริษัท</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="selectworking" id="selectworking" onchange="loadChart(this);"; required>
                                                <option value="">เลือกบริษัท</option>
                                                    <?php 
                                                        foreach ($company  as  $value) {
                                                                echo '<option value="' . $value['id']. '">' . $value['name'] . '</option>';
                                                } ?>
                                        </select>
                                    </div>
                            </div>
                       </div>
                       <br>
                        <div class='row'>
                            <div class='col-md-2'></div>
                            <label for="numberPassportEmp" style='text-align:right' class="col-md-1 control-label">เดือน :</label>                   
                            <div class='col-md-3'>
                                <div  class="form-group">
                                    <select name='month' class="form-control" id='gMonth2'>
                                        <option value=''>--Select Month--</option>
                                        <option selected value='1'>Janaury</option>
                                        <option value='2'>February</option>
                                        <option value='3'>March</option>
                                        <option value='4'>April</option>
                                        <option value='5'>May</option>
                                        <option value='6'>June</option>
                                        <option value='7'>July</option>
                                        <option value='8'>August</option>
                                        <option value='9'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                    </select> 
                                </div>
                            </div>
                            <label for="numberPassportEmp" style='text-align:right' class="col-md-1 control-label">ปี :</label>
                            <div class='col-md-3' >
                                <div id="head2" class="form-group">
                                    <input name='year' class="form-control" type='number' min='<?php echo date('Y')-6;?>' max='<?php echo date('Y')+6;?>' value='<?php echo date('Y');?>' />
                                </div>
                            </div>  
                            <div class='col-md-2'></div>                  
                        </div>
                        <div class='row'>
                            <div class='col-md-3'></div>  
                            <div class='col-md-6'><center><button type="submit" class="btn btn-info ">download pdf</button></center></div>   
                            <div class='col-md-3'></div>   
                        </div>
                    </form>
                </div> 
                
           </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->