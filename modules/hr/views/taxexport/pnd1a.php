<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\modules\hr\apihr\ApiHr;
$company = ApiHr::getWorking_company();



?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">xxxxxx</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <form id='seartcompany'  data-toggle="validator" action="pnd1apdf" method="post">
                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                       <div class='row form-group'>
                            <div class='col-md-2'></div>
                            <div class="form-group">
                                <label for="numberPassportEmp" style='text-align:right' class="col-sm-1 control-label">บริษัท</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="selectworking" id="selectworking"  required>
                                                <option value="">เลือกบริษัท</option>
                                                    <?php 
                                                        foreach ($company  as  $value) {
                                                                echo '<option value="' . $value['id']. '">' . $value['name'] . '</option>';
                                                } ?>
                                        </select>
                                    </div>
                            </div>
                       </div>
                        <div class='row form-group'>
                            <div class='col-md-2'></div>  
                            <label for="numberPassportEmp" style='text-align:right' class="col-md-1 control-label">ปี :</label>
                            <div class='col-md-6' >
                                <div id="head2" class="form-group">
                                    <input class="form-control" name='year' type='number' min='<?php echo date('Y')-6;?>' max='<?php echo date('Y')+6;?>' value='<?php echo date('Y');?>' />
                                </div>
                            </div>  
                            <div class='col-md-2'></div>                 
                        </div>
                        <br>
                        <div class='row'>
                            <div class='col-md-3'></div>  
                            <div class='col-md-6'><center><input  type="submit" class="btn btn-info " value='download pdf'></input></center></div>   
                            <div class='col-md-3'></div>   
                        </div>
                    </form>
                </div> 
           </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->