<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "b_bloodgroup".
 *
 * @property integer $id
 * @property string $bloodgroup
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property integer $updatteby_user
 * @property string $update_datetime
 */
class Bbloodgroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b_bloodgroup';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_active', 'updatteby_user'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['bloodgroup'], 'string', 'max' => 2],
            [['createby_user'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bloodgroup' => 'Bloodgroup',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updatteby_user' => 'Updatteby User',
            'update_datetime' => 'Update Datetime',
        ];
    }
}
