<?php

namespace app\modules\auth;

/**
 * auth module definition class
 */
class Auth extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\auth\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function testA()
    {
        return 'aaa';
    }
}
