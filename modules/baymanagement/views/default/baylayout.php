<?php
/**
 * Created by PhpStorm.
 * User: watcharaphan
 * Date: 22/5/2018 AD
 * Time: 17:37
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\modules\vhc\api\ApiServiceVHC;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/baylayout.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/uploadpreview.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/baymanagement/armbox.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$today = date('d/m/Y');

/*echo '>>>>>>>>>>' . $CG . $i . $o;
echo '<pre>';
print_r($CG);
echo '</pre>';
echo '......' . $d['1'];
echo '<hr>';

foreach ($a as $item) {
    echo '<pre>';
    print_r($item['background_image']);
    echo '</pre>';
}
*/


echo '<pre>';
print_r($c['background_image']);
echo '</pre>';
?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">จัดการแผนที่โรงซ่อม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>


        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-10"></div>


                    <form id="form1" runat="server">
                        <div class="col-md-8"></div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-success" data-dismiss="modal" id="btn_saveme"
                                    onclick="Photo_Storage();">บันทึกรูปภาพ
                            </button>
                        </div>
                        <div class="col-md-2">
                            <input type='file' id="imgInp">

                        </div>

                        <div class="col-md-4"></div>
                        <div class="col-md-7">
                            <div id='img_contain'>

                                <img src="<?php

                                echo Yii::$app->request->baseUrl . '/upload/baymanagement/' . $c['background_image'];


                                ?>" width="200" HEIGHT="200"


                                     id="blah"
                                     align='middle'
                                     title=''/>
                            </div>
                        </div>


                    </form>


                    <!-- <img src="<?php /*echo Yii::$app->request->baseUrl . '/upload/baymanagement/789712482.jpg'; */ ?>"
                         width="200" HEIGHT="200">-->
                </div>
            </div>
        </div>
    </div>
</section>