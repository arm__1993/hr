<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "payroll_config_wage".
 *
 * @property integer $id
 * @property integer $wage_type
 * @property integer $wage_day
 * @property integer $wage_multiply
 * @property string $wage_type_name
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class PayrollConfigWage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_config_wage';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wage_type', 'wage_day', 'wage_multiply', 'status_active'], 'required'],
            [['wage_type', 'wage_day', 'wage_multiply', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['wage_type_name'], 'string', 'max' => 150],
            [['createby_user', 'updateby_user'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wage_type' => 'Wage Type',
            'wage_day' => 'Wage Day',
            'wage_multiply' => 'Wage Multiply',
            'wage_type_name' => 'Wage Type Name',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }
}
