$(document).ready(function () {
    ///ส่วนจ่าย///
    $("#btnSaveActivityservice").on("click", function () {
        var f = frmValidate('frmAddVhcservice');
        if (!f) {
            // console.log('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy');
            showWarningInputForm();
        } else {
            $('.record_status').val('1');
            // console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            servicetype(1);

        }
    });

    $('#btnAddNewVhcservice').on("click", function () {
        initForm('btnSaveActivityservice');

    });

    ///ส่วนหัก///
    $("#btnSaveActivityserviceDeduct").on("click", function () {
        var f = frmValidate('frmDeduct');
        if (!f) {
            showWarningInputForm();
        } else {
            $('.record_status').val('2');
            servicetype(2);
        }
    });

    $('#btnAddNewVhcservice').on("click", function () {
        initForm('btnSaveActivityservice');
        $('#hide_activityedit_servicetype').val('');
        $('#servicetype_name').val('');
        $('#record_status_servicetype').prop('checked', true);
    });
});

function deletevhcservicetype(id, valselect) { //อันนี้ยกเลิกเน้อออออออออออออออออ
    // alert(id);
    $.ajax({
        url: 'deletevhcservicetype',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showDeleteSuccess();
                $.pjax.reload({container: "#pjax_tb_vhcservicetype"});  //Reload GridView

                selecttab(valselect);
            } else {
                showDeleteError();
                console.log(data);

            }
        }
    });
}
function servicetype(idselectform) {
    if (idselectform == 1) {
        var datavar = $('#frmAddVhcservice').serialize();
    } else {
        var datavar = $('#frmDeduct').serialize();
    }
    // console.log(datavar);
    $.ajax({
        url: 'savevhcblame',
        data: datavar,
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showSaveSuccess();
                if (idselectform == 1) {
                    initForm('frmAddVhcservice');
                } else {
                    initForm('frmDeduct');
                }

                selecttab(idselectform);
             //   $.pjax.reload({container: "#pjax_tb_vhcservicetype"});  //Reload GridView
            } else {
                showSaveError();
            }
        }
    });
}

function editservicetype(id) {//แก้ไขเน้อออออออออออ
    // console.log(id);
    initForm('frmAddVhcservice');//idฟอมมมมม
    $.ajax({
        url: 'updatevhcservicetype',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            //  console.log(data);
            //
            if(data.record_status==1){
                $('#record_status_servicetype').prop('checked', true);
            }else {
                $('#record_status_servicetype').prop('checked', false);
            }

            $('#hide_activityedit_servicetype').val(data.id);
            $('#servicetype_name').val(data.servicetype_name);
            $('#modalfrmAddVhcservice').modal();
        }
    });
}

function servicetype() {//อันนี้นี้เซฟเน้ออออออออออออออออ
    var datavar = $('#frmAddVhcservice').serialize();

    console.log(datavar);
    $.ajax({
        url: 'servicetype',
        data: datavar,
        type: 'POST',
        success: function (data) {
            console.log(data);
            if (parseInt(data) == 1) {
                showSaveSuccess();
                initForm('frmAddVhcservice');
                $.pjax.reload({container: "#pjax_tb_vhcservicetype"});  //Reload GridView
            }
            else {
                showSaveError();
            }
        }
    });
}

function selecttab(val) {
    //alert("2222" + val);
    // console.log($("#tabselect" + val));
    $(".tabselect").removeClass("active");
    $("#tabselect" + val).addClass("active");
    $("#tab" + val).addClass("active");

    if (val == 1) {
        $.pjax.reload({container: "#pjax_tb_adddeducttemp"}); //Reload GridView
    } else {
        $.pjax.reload({container: "#pjax_tb_vhcservicetype"}); //Reload GridView
    }


}