-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2017 at 09:38 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_checktime`
--

-- --------------------------------------------------------

--
-- Table structure for table `b_leveleducation`
--

CREATE TABLE `b_leveleducation` (
  `id` int(11) NOT NULL,
  `level_education` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT 'ระดับการศึกษา',
  `status_active` tinyint(3) DEFAULT NULL COMMENT '1=ใช้งาน 0= หยุดการใช้งาน 99=ลบ',
  `createby_user` varchar(13) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `updatteby_user` varchar(13) DEFAULT NULL,
  `update_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `b_leveleducation`
--
ALTER TABLE `b_leveleducation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `b_leveleducation`
--
ALTER TABLE `b_leveleducation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
