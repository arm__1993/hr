<?php

namespace app\modules\hr\controllers;


use yii;
use ZipArchive;
use app\modules\hr\controllers\MasterController;

class SsoexportController extends MasterController
{

    // public $idcardLogin;


    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }



    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionTestzip() 
    { 
        $zip = new ZipArchive; 
        $basePath = Yii::$app->basePath; 
        $path = $basePath.'/upload/file/'; 

        //$path = '/Applications/XAMPP/htdocs/webapp/wseasyerp/upload/file/'; 

            if ($zip->open($path.'test_new.zip', ZipArchive::CREATE) === TRUE) 
            { 
                $zip->addFile($path.'HBSO.xlsx'); 
                $zip->addFile($path.'avatar2.png'); 

                
                $zip->close(); 
            } 
            else { 
                echo 'xxxx'; 
            } 
    }

}
