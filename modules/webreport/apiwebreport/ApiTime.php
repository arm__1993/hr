<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 18:08
 */

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiTime
{
    public static function getAvgTimeReport1($value)
    {
        return $value = (($value/60)/60);
    }
    public static function getDayRangtime($date_start,$date_end)
    {
        $Newdate_start=date_create($date_start);
        $Newdate_end=date_create($date_end);
        $diff=date_diff($Newdate_start,$Newdate_end);
        $countdate = $diff->format("%a");


        $x = 0;
        $j=[];
        do {
            $j[$date_start] = $date_start ;
            $date_start = date('Y-m-d',(strtotime('+1 day',strtotime ($date_start))));

            $x++;
        } while ($x <= $countdate);

        return $j;

    }
    public static function resultDrepairRangTimeAVG($IN_get_IdDrepair,$select_company,$Time_start,$Time_End)
    {
         $sqlresultDrepairRangTimeAVG = "SELECT FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d ') AS time_start,
                        SUM(TIMESTAMPDIFF(SECOND, DATE_FORMAT(FROM_UNIXTIME(erepair_sent_list.time_start), '%Y-%m-%d %H:%i:%s'),
                                                  DATE_FORMAT(FROM_UNIXTIME(erepair_sent_list.time_stop), '%Y-%m-%d %H:%i:%s'))) AS SumDifftime
                        FROM erepair_send 
                        INNER JOIN erepair_sent_list
                        ON erepair_send.id = erepair_sent_list.erepair_send_id
                        WHERE erepair_send.drepair_inform_id IN ($IN_get_IdDrepair)
                        AND erepair_send.branch_id = '$select_company' 
                        AND FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d %H:%i:%s') >= '$Time_start 00:00:00' 
                        AND FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d %H:%i:%s') <= '$Time_End 23:59:59'
                        GROUP BY FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')";

        return $resultDrepairRangTimeAVG = Yii::$app->dbERP_service->createCommand($sqlresultDrepairRangTimeAVG)->queryAll();
    }
    public static function getDataRangListDrepairinformWagefree($IN_IDcvhcmainfree,$Time_start,$Time_End)
    {
         $sql_IDdrepairFree = "SELECT drepair_inform.id AS id_drepair_inform,
                  FROM_UNIXTIME (start_time,'%Y-%m-%d %H:%i:%s') AS Timestart 
                  FROM drepair_inform
                  WHERE drepair_inform.cvhc_main_id IN ($IN_IDcvhcmainfree) 
                  AND FROM_UNIXTIME (start_time,'%Y-%m-%d %H:%i:%s') BETWEEN '$Time_start 00:00:00' 
                  AND '$Time_End 23:59:59'";


        return $List_DrepairinformWagefree = Yii::$app->dbERP_service->createCommand($sql_IDdrepairFree)->queryAll();
    }
    public static function getDataTimeRangCkPriceFree($Time_start,$Time_End)
    {
        ///////   งานฟรี   ///////
         $sql_ckpricefree = "SELECT ytax.id , 
                            ytax.datePay,
                            ytax.ref_id AS id_cvchofytax,
                            ytax.sumall,
                            cvhc_main.id AS id_cvch
                            FROM ytax
                            INNER JOIN cvhc_main ON ytax.ref_id = cvhc_main.id
                            WHERE ytax.sumall = 0.00
                            AND ytax.datePay BETWEEN '$Time_start 00:00:00' 
                            AND '$Time_End 23:59:59'  ";



        return   $datackpricefree = Yii::$app->dbERP_service->createCommand($sql_ckpricefree)->queryAll();
    }
    public static function getDateCkeTimeRangStart($select_company,$Time_start,$Time_End,$technician_TypeCar)
    {
         $sql_datecktime_start = "SELECT drepair_inform.id AS id_drepair_inform,
                cvhc_main.id AS id_cvhc ,
                ahistory.id AS id_ahistory,
                FROM_UNIXTIME (cvhc_main.sa_str_cktime,'%Y-%m-%d %H:%i:%s') AS Timestart 
                FROM drepair_inform 
                INNER JOIN cvhc_main ON cvhc_main.id = drepair_inform.cvhc_main_id 
                INNER JOIN ahistory ON ahistory.id = cvhc_main.ahistory_id 
                WHERE brach_id = '$select_company'
                AND cvhc_main.status <> '99'
                AND FROM_UNIXTIME (sa_str_cktime,'%Y-%m-%d') BETWEEN '$Time_start' 
                AND '$Time_End' 
                AND ahistory.agroup_id IN ($technician_TypeCar)";


        return $datacktime_start = Yii::$app->dbERP_service->createCommand($sql_datecktime_start)->queryAll();
    }
    public static function resultDrepair($IN_get_IdDrepair,$select_company,$select_year)
    {
        $sql_resultDrepair = "SELECT FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m') AS time_start,
                        SUM(IF(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d ')) = '0',1,0)) AS SUMDiffDateResult1,
                        SUM(IF(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d ')) = '1',1,0)) AS SUMDiffDateResult2,
                        SUM(IF(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')) = '2',1,0)) AS SUMDiffDateResult3,
                        SUM(IF(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')) >= '3' &&
                               DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')) < '8',1,0)) AS SUMDiffDateResult47,
                        SUM(IF(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')) >= '8' &&
                               DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')) < '15',1,0)) AS SUMDiffDateResult814,
                        SUM(IF(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')) >= '15' &&
                               DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d')) < '31',1,0)) AS SUMDiffDateResult1530,
                        SUM(IF(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d ')) >= '31',1,0)) AS SUMDiffDateResultmore30,
                        COUNT(DATEDIFF(FROM_UNIXTIME(erepair_sent_list.time_stop,'%Y-%m-%d '),
                                 FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m-%d '))) AS SUMAllResult
                        FROM erepair_send 
                        INNER JOIN erepair_sent_list
                        ON erepair_send.id = erepair_sent_list.erepair_send_id
                        WHERE erepair_send.drepair_inform_id IN ($IN_get_IdDrepair)
                        AND erepair_send.branch_id = '$select_company' 
                        AND FROM_UNIXTIME(erepair_sent_list.time_start,'%Y') LIKE '$select_year%' 
                        GROUP BY FROM_UNIXTIME(erepair_sent_list.time_start,'%Y-%m')";


        return $resultDreparir = Yii::$app->dbERP_service->createCommand($sql_resultDrepair)->queryAll();
    }
    public static function getDateCkeTimeStart($select_company,$select_year,$technician_TypeCar)
    {
        $sql_datecktime_start = "SELECT drepair_inform.id AS id_drepair_inform,
                cvhc_main.id AS id_cvhc ,
                ahistory.id AS id_ahistory,
                FROM_UNIXTIME (cvhc_main.sa_str_cktime,'%Y-%m-%d %H:%i:%s') AS Timestart 
                FROM drepair_inform 
                INNER JOIN cvhc_main ON cvhc_main.id = drepair_inform.cvhc_main_id 
                INNER JOIN ahistory ON ahistory.id = cvhc_main.ahistory_id 
                WHERE brach_id = '$select_company'
                AND cvhc_main.status <> '99'
                AND FROM_UNIXTIME (sa_str_cktime,'%Y') LIKE '$select_year%'
                AND ahistory.agroup_id IN ($technician_TypeCar)";

       return $datacktime_start = Yii::$app->dbERP_service->createCommand($sql_datecktime_start)->queryAll();
    }
    public static function getDataCkPriceFree($select_year)
    {
        ///////   งานฟรี   ///////
        $sql_ckpricefree = "SELECT ytax.id , 
                            ytax.datePay,
                            ytax.ref_id AS id_cvchofytax,
                            ytax.sumall,
                            cvhc_main.id AS id_cvch
                            FROM ytax
                            INNER JOIN cvhc_main ON ytax.ref_id = cvhc_main.id
                            WHERE ytax.sumall = 0.00
                            AND ytax.datePay LIKE '$select_year%'";

     return   $datackpricefree = Yii::$app->dbERP_service->createCommand($sql_ckpricefree)->queryAll();
    }
    public static function getDataListDrepairinformWagefree($IN_IDcvhcmainfree,$select_year)
    {
        $sql_IDdrepairFree = "SELECT drepair_inform.id AS id_drepair_inform,
                  FROM_UNIXTIME (start_time,'%Y-%m-%d %H:%i:%s') AS Timestart 
                  FROM drepair_inform
                  WHERE drepair_inform.cvhc_main_id IN ($IN_IDcvhcmainfree) 
                  AND FROM_UNIXTIME (start_time,'%Y-%m-%d %H:%i:%s') LIKE '$select_year%'";

       return $List_DrepairinformWagefree = Yii::$app->dbERP_service->createCommand($sql_IDdrepairFree)->queryAll();
    }
    public static function getDrepairinformlist($IN_get_IdDrepair)
    {

        $sql_listdrepairinformlist = "SELECT drepair_inform_list.drepair_inform_id, 
                                      drepair_inform_list.crepair_id,
                                      crepair.repair_id AS repair_id_header,
                                      drepair_inform_list.wage
                                      FROM drepair_inform_list 
                                      INNER  JOIN crepair 
                                      ON drepair_inform_list.crepair_id = crepair.id
                                      WHERE drepair_inform_id 
                                      IN ($IN_get_IdDrepair)";

       return $List_Drepairinformlist = Yii::$app->dbERP_service
            ->createCommand($sql_listdrepairinformlist)
            ->queryAll();

    }

}