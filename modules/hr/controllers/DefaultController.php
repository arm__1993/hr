<?php

namespace app\modules\hr\controllers;

use app\modules\hr\apihr\ApiBenefit;
use app\modules\hr\apihr\ApiMenu;
use app\modules\hr\models\AddDeDuctTemplate;
use app\modules\hr\models\Empdata;
use yii\web\Controller;
use yii\web\Session;
use Yii;
use app\modules\hr\apihr\ApiHr;
use app\api\DBConnect;
use app\modules\hr\controllers\MasterController;
use app\modules\hr\apihr\Apilookupposition;

/**
 * Default controller for the `hr` module
 */
class DefaultController extends MasterController
{
    public $layout = 'hrlayout';

    public $idcardLogin;
    public $idcompanyname;

    public function init()
    {
        $session = Yii::$app->session;
        if (!$session->has('fullname') || !$session->has('idcard')) {
            return $this->redirect(array('/auth/default/logout'), 302);
        }
        $this->idcompanyname = $session->get('companyid');
        $this->idcardLogin = $session->get('idcard');

    }
    /**
     * function init() check session active or session login, if not redirect to login page
     * @return \yii\web\Response
     */

    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex()
    {
//        $login = $this->idcardLogin;
//        print_r($login);
//        exit();
//        $path = "./upload/emp_img/Pictures_HyperL/";
//        $path2 = Yii::$app->params;
////
//        echo '<pre>';
//        print_r($path2['emp_pic_path']);
//        echo '</pre>';
//        exit();

        $login = $this->idcardLogin;

        return $this->render('index',
            [
                'login'=>$login,
            ]);


    }

    public function actionUploadcroppie()
    {
        $login = $this->idcardLogin;
        $companyname = $this->idcompanyname;

        $all_company = ApiHr::getWorking_company();

        $short_name = $all_company[$companyname];


        $data = $_POST['image'];

        $path2 = Yii::$app->params;
        $path = $path2['emp_pic_path'];

        $pathnew = 'upload/emp_img/Pictures_HyperL/';

        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);


        $data = base64_decode($data);

        $imageName = $login.'.png';

        $model = Empdata::findOne(['ID_Card' => $login ]);
        $model->image_card = $imageName;

        if($model->save()){
            $session = Yii::$app->session;
            $session->set('photo',$pathnew.$imageName);
        }


            file_put_contents($pathnew.$imageName,$data);


        echo 'done';

    }

}
