<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/payroll_report_pdf.css");

$session = Yii::$app->session;

$arrItemData = $session->get('arrItemData');
$monthselect = $session->get('monthselect');
$modelTemp = $session->get('modelTemp');
$modelWorkingcompany = $session->get('modelWorkingcompany');
$arrDataColumnPDF = $session->get('arrDataColumnPDF');
$total_page = $session->get('total_page');


?>
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun";
        font-size: 16px;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }


</style>

<div class="row container">
    <?php
    $getMonth = Datetime::convertFormatMonthYear($monthselect);
    //print_r($getMonth);
    $monthGetReport2 = $getMonth['monthresult'];
    $resultMonthReport2 = Datetime::mapMonth($monthGetReport2);
    $yearGetReport2 = $getMonth['year'];

    $c = 1;
    foreach ($arrDataColumnPDF as $dataColumn) {

        if($c>1) {
            echo '<div style="page-break-after: always"></div>';
        }


        $tbl = '';
        $tbl .= '<div class="row">';
        $tbl .= '<div class="col-md-4"></div>';
        $tbl .= '<div class="col-md-6">';
        $tbl .= '<h3 style="text-align: center">รายงานสรุปส่วนหัก  รอบเงินเดือน '. $resultMonthReport2.'&nbsp;&nbsp;' . $yearGetReport2 .'</h3>';
        $tbl .= '</div>';
        $tbl .= '</div>';

        $tbl .= '<div class="row">'; //start div row
        $tbl .= '<div class="col-md-12">'; //start div col

        $tbl .= '<table  cellspacing="0" class="table table-striped table-bordered rpt"  align="center">
                <thead>
                <tr role="row" style="font-size: 12px;font-weight: bold">
                    <th aria-sort="ascending" width="200">ชื่อบริษัท/รายการหัก</th>';

        foreach ($dataColumn as $data) {
            $tbl .= '<th width="100" aria-sort="ascending" class="text-center">' . $data['short_name'] . '</th>';
        }

        $tbl .= '<th width="100" aria-sort="ascending" class="text-center">รวม</th>';
        $tbl .= '</tr>
                </thead>
                <tbody>';

        $TotalComp = [];
        $r = 1;
        foreach ($modelTemp as $value) {
            $cls = ($r%2==0) ? 'odd' : 'even';
            $tbl .= '<tr class="'.$cls.'">';
            $tbl .= '<td width="200">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</td>';
            $LineTotal = 0;

            foreach ($dataColumn as $item) {
                $data = (isset($arrItemData[$value['ADD_DEDUCT_TEMPLATE_ID']][$item['id']])) ? $arrItemData[$value['ADD_DEDUCT_TEMPLATE_ID']][$item['id']] : 0;
                $LineTotal += $data;
                $TotalComp[$item['id']][$value['ADD_DEDUCT_TEMPLATE_ID']] = $data;
                $show = ($data > 0) ? Helper::displayDecimal($data) : '';
                $tbl .= '<td width="100" style="text-align: right">' . $show . '</td>';
            }

            $SumLine = ($LineTotal > 0) ? Helper::displayDecimal($LineTotal) : '';
            $tbl .= '<td width="100" style="text-align: right">' . $SumLine . '</td>';
            $tbl .= '</tr>';
            $r++;
        }

        $tbl .= '</tbody>
                        <tfoot>
                        <tr role="row">
                            <th aria-sort="ascending" >รวม</th>';
        $TotalMoney = 0;
        foreach ($dataColumn as $item) {
            $SumComp = (isset($TotalComp[$item['id']])) ? array_sum($TotalComp[$item['id']]) : 0;
            $show = ($SumComp > 0) ? Helper::displayDecimal($SumComp) : '';
            $TotalMoney += $SumComp;
            $tbl .= '<th aria-sort="ascending" style="text-align: right">' . $show . '</th>';
        }
        $tbl .= '<th aria-sort="ascending" style="text-align: right">' . Helper::displayDecimal($TotalMoney) . '</th>';
        $tbl .= '</tr></tfoot>';
        $tbl .= '</table>';

        $tbl .= '</div>';  //end div columns col-12
        $tbl .= '</div>';  //end div row
        echo $tbl;
        $c++;
    }
    ?>
</div>