<?php

namespace app\modules\vhc\models\VHC;

use Yii;

/**
 * This is the model class for table "vhc_save_list".
 *
 * @property integer $id
 * @property integer $vhc_detail_id
 * @property integer $vhc_option_id
 * @property integer $value_option
 * @property string $name_option
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcSaveList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_save_list';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vhc_detail_id', 'vhc_option_id', 'value_option', 'name_option', 'status', 'create_by', 'update_by', 'create_date', 'update_date'], 'required'],
            [['vhc_detail_id', 'vhc_option_id', 'value_option', 'status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['name_option'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vhc_detail_id' => 'Vhc Detail ID',
            'vhc_option_id' => 'Vhc Option ID',
            'value_option' => 'Value Option',
            'name_option' => 'Name Option',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
