<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 20/10/2559
 * Time: 11:33
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiDuringrepair;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/jquery.datetimepicker.full.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

 $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });

});
JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานสถิติระยะเวลาใบงานเกินเวลามาตรฐาน ต่อเดือน</h3>

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="box-body">
            <div class="row">
                <form method="post" action="time_month">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key => $value) {
                                    $sel = ($selected_company == $key) ? ' selected="selected" ' : '';
                                    echo '<option value="' . $key . '" ' . $sel . '>' . $value . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>เลือกปี</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="year" id="year">
                                <?php
                                $get_year = ApiReport::getAppYear();
                                foreach ($get_year as $key=>$value) {
                                    $sel_year = ($selected_year==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel_year.' >'.$value.'</option>';
                                }
                                $countyear = count($years);
                                ?>
                            </select>
                        </div>
                          &nbsp;&nbsp;
                        <label>เลือกประเภทรถ</label>
                        &nbsp;
                        <div class="btn-group">
                        <input type="radio" name="technician" value="3,4" checked> รถเล็ก
                        &nbsp;
                        <input type="radio" name="technician" value="1,2"> รถใหญ่
                        </div>
                        </center>                    

                    <center><br>
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/duringrepair/btn_reportomonth">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                    </center>
                    </div>
                </form>
            </div>
        </div>
        <!-- ./box-body -->
<?php if($query) {?>
<?php if(empty($totoalTime)){ ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เดือน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ยอดเข้าบริการ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        A1(รอรับแจ้งซ่อม)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        A1(คิดเป็น%)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        B1(รอจ่ายงานให้ช่าง)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        B1(คิดเป็น%)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        C1(รอปิดงานซ่อม)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        C1(คิดเป็น%)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        D1(ปิดงานซ่อม)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        D1(คิดเป็น%)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        E1(ชำระเงิน)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        E1(คิดเป็น%)
                                        </th>
                                    </tr>
                                    <?php   for ($i=1; $i <= 12 ; $i++) {
                                        ?>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1"><?php  $arrMonth = DateTime::convertMonth($i) ;
                                            echo $arrMonth;?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php  echo $TimeStatusYear[$i-1]['CountCar'];

                                                if($TimeStatusYear[$i-1]['CountCar'] == 0){
                                                    $countCar = 1 ;
                                                }else
                                                {
                                                    $countCar = $TimeStatusYear[$i-1]['CountCar'];
                                                };
                                            ?>
                                        </td>
                                        <td class="sorting_1" align="center"><?php  echo $TimeStatusYear[$i-1]['Time_DiffA'];?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php   $presentOfTimeStatusYearA = (($TimeStatusYear[$i-1]['Time_DiffA']/$countCar)*100);
                                         print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusYearA)));
                                        ?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php  echo $TimeStatusYear[$i-1]['Time_DiffB'];?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php  $presentOfTimeStatusYearB = (($TimeStatusYear[$i-1]['Time_DiffB']/$countCar)*100);
                                            print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusYearB)));
                                            ?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php  echo $TimeStatusYear[$i-1]['Time_DiffC'];?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php  $presentOfTimeStatusYearC = (($TimeStatusYear[$i-1]['Time_DiffC']/$countCar)*100);
                                            print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusYearC)));?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php  echo $TimeStatusYear[$i-1]['Time_DiffD'];?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php  $presentOfTimeStatusYearD = (($TimeStatusYear[$i-1]['Time_DiffD']/$countCar)*100);
                                            print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusYearD)));?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php echo $TimeStatusYear[$i-1]['Time_DiffE'];?>
                                        </td>
                                        <td class="sorting_1"  align="center"><?php $presentOfTimeStatusYearE = (($TimeStatusYear[$i-1]['Time_DiffE']/$countCar)*100);
                                            print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusYearE)));?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php
                                        $chartTimeA = [];
                                        $chartTimeB = [];
                                        $chartTimeC = [];
                                        $chartTimeD = [];
                                        $chartTimeE = [];
                                        for ($i=1; $i <= 12 ; $i++) {
                                            if($TimeStatusYear[$i-1]['CountCar'] == 0){
                                                $countCar = 1 ;
                                            }else
                                            {
                                                $countCar = $TimeStatusYear[$i-1]['CountCar'];
                                            };
                                            $chartTimeA[] = ($TimeStatusYear[$i-1]['Time_DiffA']/$countCar)* 100;
                                            $chartTimeB[] = ($TimeStatusYear[$i-1]['Time_DiffB']/$countCar)* 100;
                                            $chartTimeC[] = ($TimeStatusYear[$i-1]['Time_DiffC']/$countCar)* 100;
                                            $chartTimeD[] = ($TimeStatusYear[$i-1]['Time_DiffD']/$countCar)* 100;
                                            $chartTimeE[] = ($TimeStatusYear[$i-1]['Time_DiffE']/$countCar)* 100;
                                        }
                                        ?>
                                    <?php  $arrMonth = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'); ?>
                                        <?php
                                           echo Highcharts::widget([
                                                    'options' => [          
                                                        'title' => [
                                                            'text' => ''
                                                        ],
                                                        'xAxis' => [
                                                            'categories' => $arrMonth
                                                        ],
                                                        'labels' => [
                                                            'items' => [
                                                                'html' => 'Total fruit consumption',
                                                                'style' => [
                                                                    'left' => '50px',
                                                                    'top' => '18px',
                                                                    'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                                ]
                                                            ]
                                                        ],
                                                        'credits' => [
                                                            'enabled' => false
                                                        ],                                                        
                                                        'series' => [
                                                            [
                                                                'type' => 'column',
                                                                'name' => 'A1',
                                                                'data' => $chartTimeA,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],
                                                            [
                                                                'type' => 'column',
                                                                'name' => 'B1',
                                                                'data' => $chartTimeB,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],
                                                            [                        
                                                                'type' => 'column',
                                                                'name' => 'C1',
                                                                'data' => $chartTimeC,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],
                                                            [
                                                                'type' => 'column',
                                                                'name' => 'D1',
                                                                'data' => $chartTimeD,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],
                                                            [
                                                                'type' => 'column',
                                                                'name' => 'E1',
                                                                'data' => $chartTimeE,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],

                                                        ]
                                                    ]
                                                ]);
                                            ?>
                                    </div>
                                </div><!--row-->
                        </div>
                    </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA CHART -->
            </div>
        </div>
    </section>
    <?php }else { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เดือน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ยอดเข้าบริการ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        A1(รอรับแจ้งซ่อม)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        A1(คิดเป็น%)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        B1(รอจ่ายงานให้ช่าง)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        B1(คิดเป็น%)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        C1(รอปิดงานซ่อม)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        C1(คิดเป็น%)
                                        </th>
                                    </tr>
                                    <?php
                                    $arrMonth[1] = "มกราคม";
                                    $arrMonth[2] = "กุมภาพันธ์";
                                    $arrMonth[3] = "มีนาคม";
                                    $arrMonth[4] = "เมษายน";
                                    $arrMonth[5] = "พฤษภาคม";
                                    $arrMonth[6] = "มิถุนายน";
                                    $arrMonth[7] = "กรกฎาคม";
                                    $arrMonth[8] = "สิงหาคม";
                                    $arrMonth[9] = "กันยายน";
                                    $arrMonth[10] = "ตุลาคม";
                                    $arrMonth[11] = "พฤศจิกายน";
                                    $arrMonth[12] = "ธันวาคม";
                                    for($i=1;$i<=count($A1);$i++){ ?>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">
                                        <?php print_r($arrMonth[$i]); ?>                   
                                        </td>
                                        <td><?php print_r($totoalTime[$i]); ?></td>
                                        <td><?php print_r($A1[$i]); ?></td>
                                        <td>
                                        <?php
                                        $persenA = ($totoalTime[$i]>0) ? ($A1[$i]*100)/$totoalTime[$i] : 0 ; 
                                        echo Helper::displayDecimal($persenA).'%';  ?>
                                        </td>
                                        <td><?php print_r($B1[$i]); ?></td>
                                        <td>
                                        <?php
                                        $persenB = ($totoalTime[$i]>0) ? ($B1[$i]*100)/$totoalTime[$i] : 0 ; 
                                        echo  Helper::displayDecimal($persenB).'%'; ?>
                                        </td>
                                        <td><?php print_r($C1[$i]); ?></td>
                                        <td>
                                        <?php
                                        $persenC = ($totoalTime[$i]>0) ? ($C1[$i]*100)/$totoalTime[$i] : 0 ; 
                                        echo  Helper::displayDecimal($persenC).'%'; ?>
                                        </td>
                                    </tr>  
                                    <?php 
                                    $persenAA[] = $persenA ;
                                    $persenBB[] = $persenB ;
                                    $persenCC[] = $persenC ;
                                    $month[] =  $arrMonth[$i];
                                    } ?>    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                         <?php
                                           echo Highcharts::widget([
                                                    'options' => [          
                                                        'title' => [
                                                            'text' => ''
                                                        ],
                                                        'xAxis' => [
                                                            'categories' => $month
                                                        ],
                                                        'labels' => [
                                                            'items' => [
                                                                'html' => 'Total fruit consumption',
                                                                'style' => [
                                                                    'left' => '50px',
                                                                    'top' => '18px',
                                                                    'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                                ]
                                                            ]
                                                        ],
                                                        'credits' => [
                                                            'enabled' => false
                                                        ],                                                        
                                                        'series' => [
                                                            [
                                                                'type' => 'column',
                                                                'name' => 'A1(รอรับแจ้งซ่อม)',
                                                                'data' => $persenAA,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],
                                                            [
                                                                'type' => 'column',
                                                                'name' => 'B1(รอจ่ายงานให้ช่าง)',
                                                                'data' => $persenBB,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],
                                                            [                        
                                                                'type' => 'column',
                                                                'name' => 'C1(รอปิดงานซ่อม)',
                                                                'data' => $persenCC,
                                                                'dataLabels' => [
                                                                    'enabled'=> true,
                                                                    'format' => '{point.y:.1f}%'
                                                                ]
                                                            ],
                                                            
                                                        ]
                                                    ]
                                                ]);
                                            ?>
                                    </div>
                                </div><!--row-->
                        </div>
                    </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA CHART -->
            </div>
        </div>
    </section>
    <?php } ?>
               
</div>
<!-- /.box-->
<?php } ?>
