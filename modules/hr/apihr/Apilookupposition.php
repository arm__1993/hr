<?php 

namespace app\modules\hr\apihr;
use yii;
use yii\web\Session;
use yii\data\ActiveDataProvider;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Department;
use app\modules\hr\models\Section;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Position;
use app\modules\hr\models\PositionLevel;
use app\modules\hr\models\OrganicChart;
use app\modules\hr\models\RelationPosition;
use yii\helpers\VarDumper;
class Apilookupposition
{
    //$a = Apilookupposition::Positionsteam('json'); วิธีใช้งาน
    public static function Positionsteam($type='php'){
        $array_position = $res_position = $arr_position =  [];
        $idposition = 1774;//Yii::$app->session->get('positionid');
        $position =  OrganicChart::find()
                    ->select('*,Emp_id as DataNo')
                    ->where(['=','Posision_id',$idposition])
                    ->andWhere(['!=','active_status',99])
                    ->asArray()
                    ->all();
        $array_position[] = $position;
        foreach ($position as $key => $value) {
            $res = self::getTeam($value,$res_position);
            $array_position[] = $res;
        }
        foreach ($array_position as $key => $item) {
            $arr_position[$item[0]['DataNo']] = $item[0];
        }
        if($type=='php'){
            return $arr_position;
        }else if($type=='json'){
             \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $arr_position;
        }
        else {
            return 'no format';
        }
    }
    public static function getTeam($data,$res_position)
    {
        $position =  OrganicChart::find()
                    ->select('*,Emp_id as DataNo')
                    ->where([
                        'parent'=>$data['Posision_id']
                    ])
                    //->where(['=','parent',$data['Posision_id']])
                    ->asArray()
                    ->one();
        
        if(!is_array($position)){
            return $res_position;
        }
        else{
            $res_position[] = $position;
            $position = self::getTeam($position,$res_position);
            
        }
        return $res_position;
    }
}
?>
