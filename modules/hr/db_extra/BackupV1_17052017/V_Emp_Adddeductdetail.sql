-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2017 at 10:04 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Structure for view `V_Emp_Adddeductdetail`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `V_Emp_Adddeductdetail`  AS  select `emp_data`.`DataNo` AS `IdEmp`,`emp_data`.`ID_Card` AS `IdCard`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_ID` AS `Add_Deductdetail_Id`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_ID` AS `Ref_Idtemple`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL` AS `Add_Deductdetail_Detail`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_PAY_DATE` AS `Add_Deductdetail_Paydate`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_START_USE_DATE` AS `Add_Deductdetail_Startdate`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_END_USE_DATE` AS `Add_Deductdetail_Enddate`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_EMP_ID` AS `Add_Deductdetail_Idemp`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_AMOUNT` AS `Add_Deductdetail_Amount`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_TYPE` AS `Add_Deductdetail_Type`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_STATUS` AS `Add_Deductdetail_Status`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_CREATE_DATE` AS `Add_Deductdetail_Createdate`,`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_CREATE_BY` AS `Add_Deductdetail_By`,`ADD_DEDUCT_TEMPLATE`.`ADD_DEDUCT_TEMPLATE_ID` AS `Add_Deducttemplate_Id`,`ADD_DEDUCT_TEMPLATE`.`ADD_DEDUCT_TEMPLATE_NAME` AS `Add_Deducttemplate_Name`,`ADD_DEDUCT_TEMPLATE`.`ADD_DEDUCT_TEMPLATE_TYPE` AS `Add_Deducttemplate_Type` from ((`emp_data` join `ADD_DEDUCT_DETAIL` on((`emp_data`.`ID_Card` = `ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_DETAIL_EMP_ID`))) join `ADD_DEDUCT_TEMPLATE` on((`ADD_DEDUCT_DETAIL`.`ADD_DEDUCT_ID` = `ADD_DEDUCT_TEMPLATE`.`ADD_DEDUCT_TEMPLATE_ID`))) ;

--
-- VIEW  `V_Emp_Adddeductdetail`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
