<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\Utility;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
?>
    <div>
        <div class="col-md-10">    
        </div>
        <div class="col-md-2">
            <a data-toggle="modal" onclick="reset_servicetype()"><img src="<?php echo $imghr; ?>/add.png" class="img-circle">
                                    <span>เพิ่ม</span></a>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
            <?php
                Pjax::begin(['id' => 'pjax_VhcServicetype']);
                echo GridView::widget([
                    'id' => 'VhcServicetype',
                    'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                    'dataProvider' => $VhcServicetypeProvider,
                    'filterModel' => $VhcServicetypeSearch,
                    //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'servicetype_name',
                            'value' => 'servicetype_name',                            
                            'label' => 'ชื่อรายการ',
                            'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                        ],
                        [
                            'attribute' => 'record_status',
                            'label' => 'สถานะ',
                            'format' => 'image',
                            'value' => function ($data) {
                                return Utility::dispActive($data->record_status);
                            },
                            'filter' => false,
                            'contentOptions' => ['style' => 'width: 50px;text-align:center']
                        ],
                        [
                            'attribute' => 'create_by',
                            'value' => 'create_by',
                            'label' => 'บันทึกโดยผู้ใช้',
                            'filter' => false,
                            'contentOptions' => ['style' => 'width: 150px;']
                        ],
                        [
                            'attribute' => 'create_date',
                            'label' => 'บันทึกเมื่อวันที่เวลา',
                            'value' => function ($data) {
                                return DateTime::ThaiDateTime($data->create_date);
                            },
                            'filter' => false,
                            'contentOptions' => ['style' => 'width: 150px;']
                        ],
                        [
                            'headerOptions' => ['width' => '100'],
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}  &nbsp; {delete}',
                            'buttons' => [
                                'update' => function ($url, $data) {
                                    return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                        'title' => 'แก้ไข',
                                        'onclick' => '(function($event) {
                                            reset_servicetype(' . $data->id . ');
                                        })();'
                                    ]);
                                },

                                'delete' => function ($url, $data) {
                                    return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                        'title' => 'ลบ',
                                        'onclick' => '(function($event) {
                                                bootbox.confirm({
                                                    size: "small",
                                                    message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ? </h4>",
                                                    callback: function(result){
                                                        if(result==1) {
                                                            delete_servicetype(' . $data->id . ');
                                                        }
                                                    }
                                                });

                                            })();'
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]);
                Pjax::end();  //end pjax_gridcorclub

                ?>
        <!-- /.error-page -->

    </section>
    <!-- /.content -->
</div>
<style>
    .SumoSelect{
        width:100%
    }
</style>
<div class="modal fade" id="addDialog_servietype" tabindex="-1" role="dialog" aria-labelledby="ConfirmDialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ประเภทการให้บริการ</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                 <!--onsubmit="return getdatesubmit();" -->
                <form id="form_servietype" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="0" type="hidden" id="id_servicetype" name="id_servicetype"/>
                    <div class="row">
                        <div class="col-md-3">ชื่อรายการ</div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="servicetype_name"  name="servicetype_name">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">สถานะ</div>
                        <div class="col-md-6">
                            <input type="checkbox" id="record_status"  name="record_status"> 
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="button" id="Btn_save_list" onclick="save_servicetype()" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
