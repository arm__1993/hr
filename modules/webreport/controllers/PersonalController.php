<?php

namespace app\modules\webreport\controllers;

use app\api\DateTime;
use app\modules\webreport\apiwebreport\ApiPersonal;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

use mPDF;


class PersonalController extends \yii\web\Controller
{
    public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPfm_repairman_day()
    {
        if(Yii::$app->session->hasFlash('warning')) {
            Yii::$app->session->remove('warning');
        }
        return $this->render('pfm_repairman_day');
    }

    public function actionPfm_repairman_month()
    {
        if(Yii::$app->session->hasFlash('warning')) {
            Yii::$app->session->remove('warning');
        }
        return $this->render('pfm_repairman_month');
    }

    public function actionReceive_repairman()
    {

        $postData = Yii::$app->request->post();
        $type_company = $postData['company'];
        $technician_type = $postData['technician'];
        $technician_id = $postData['name'];
        $years = $postData['year'];
        $months = $postData['month'];

        //$arrDateRange = DateTime::FormatDateFromCalendarRange($strDate);
        //$dateStart = $arrDateRange['start_date'];
        //$dateEnd = $arrDateRange['end_date'];
        //$_arrRET = Helper::calculateSunday($dateStart, $dateEnd);



        $selected_company = $type_company;
        $selected_technician = $technician_type;
        $selected_technician_id = $technician_id;
        $selected_years = $years;
        $selected_month = $months;

        if (strlen($months) == 1) {
            $months = "0".$months;
        }

        if ($technician_type == '1') {  //ช่างรถเล็ก
            $technician_car = '3,4';
        } elseif ($technician_type == '2') {  //ช่างรถใหญ่
            $technician_car = '1,2';
        } elseif ($technician_type == '3') {  //ช่างทั้งหมด
            $technician_car = '1,2,3,4';
        }


        //$postName = Yii::$app->request->post();
        //$p_name = $postName['name'];
        //$postMoths = Yii::$app->request->post();
        //$months = ($postMoths['month']);
        //$postYear = Yii::$app->request->post();
        //$year = ($postYear['year']);

        //$postTechnician = Yii::$app->request->post();
        //$technician_car = ($postTechnician['technician']);

        //foreach ($year as $row) {
          //  $yearAD[] = $row;
        //}//$yearAD ปี ค.ศ

       // $str_name = "'" . $p_name . "'"; // = 'name'
        //$str_year = "%" . $years . "-%" . $months . "-%d";
        $StartDate = $years . "-" . $months . "-" . "01";
        $MonthDays = date("t", strtotime(".$StartDate."));//แสดงจำนวนวันทั้งหมด ของเดือนที่รับมา
        $EndDate = $years . "-" . $months . "-" . $MonthDays;


        $_arrRET = Helper::calculateSunday($StartDate, $EndDate);

        $technician_persona = ApiPersonal::getTechnicianPerson($years,$technician_id);
       /* if ($technician_car == 'ช่างรถเล็ก') {
            $technician_car = '3';
        } elseif ($technician_car == 'ช่างรถใหญ่') {
            $technician_car = '1';
        } elseif ($technician_car == 'ช่างทั้งหมด') {
            $technician_car = '1,3';
        }*/
        // 3 = L, 4 = P 1 = B, 2 = M, 
        // LP = เล็ก BM = ใหญ่ 



        $technician = ApiPersonal::getTechnicianName($technician_car); //pass
        $key_wage = ApiPersonal::getTechnicianWageInYear($years); //pass
        $arrAllTechnician = ApiReport::getAllTechnician();
        $chart = ApiReport:: buildArrayWage($arrAllTechnician, $technician, $key_wage);
        $Idcard = array_keys($chart);


        $persona = $technician_persona[0]['technician_name'];
        $novat = $technician_persona[0]['SUM(c.novat_wage)'];
        $time = $technician_persona[0]['SUM(c.time)'];

        $session = Yii::$app->session;
        $session->set('_arrRET', $_arrRET);
        $session->set('persona', $persona);
        $session->set('novat', $novat);
        $session->set('time', $time);
        $session->set('Idcard', $Idcard);
        $session->set('chart', $chart);


        return $this->render('pfm_repairman_month',
            [
                'selected_company' => $selected_company,
                'selected_technician'=>$selected_technician,
                'selected_technician_id'=>$selected_technician_id,
                'selected_years' => $selected_years,
                'selected_month' => $selected_month,
                'persona' => $persona,
                'novat' => $novat,
                'time' => $time,
                '_arrRET' => $_arrRET,
                'Idcard' => $Idcard,
                'chart' => $chart,
            ]
        );

    }

    public function actionReceive_repairman_day()
    {

        $postData = Yii::$app->request->post();
        $type_company = $postData['company'];
        $technician_type = $postData['technician'];
        $technician_id = $postData['name'];
        $strDate = $postData['reservation'];

        $arrDateRange = DateTime::FormatDateFromCalendarRange($strDate);
        $dateStart = $arrDateRange['start_date'];
        $dateEnd = $arrDateRange['end_date'];
        $_arrRET = Helper::calculateSunday($dateStart, $dateEnd);



        $selected_company = $type_company;
        $selected_technician = $technician_type;
        $selected_technician_id = $technician_id;
        $selected_date = $strDate;


        if ($technician_type == '1') {  //ช่างรถเล็ก
            $technician_TypeCar = '3,4';
        } elseif ($technician_type == '2') {  //ช่างรถใหญ่
            $technician_TypeCar = '1,2';
        } elseif ($technician_type == '3') {  //ช่างทั้งหมด
            $technician_TypeCar = '1,2,3,4';
        }


        $NamePersona = ApiPersonal::getPersonalName($type_company,$technician_id,$dateStart,$dateEnd);
        if (empty($NamePersona)) {
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('pfm_repairman_day',['query'=>false,]);
        }
        else
        {


            $time = ApiPersonal::getTime($dateStart,$dateEnd);
            $drepairID_MAX = MAX(array_keys($time));
            $drepairID_MIN = MIN(array_keys($time));

            $wage = ApiPersonal::getAllWage($drepairID_MIN,$drepairID_MAX);

            foreach ($time as $index => $value) {//รวมค่าแรง และ เวลา
                if (array_key_exists($index, $wage)) {
                    $timeAndWage[$value['cvhc_main']] = $wage[$index];
                    $timeAndWage[$value['cvhc_main']]['time'] = $value['time'];
                }
            }

            foreach ($timeAndWage as $index => $value) { //ช้อมูลบุคคลที่ค้นหา ค่าแรงและเวลา
                if (array_key_exists($index, $NamePersona)) {
                    $sumWage = array_sum($value);
                    $totalPersona[$index]['wage'] = $sumWage;
                    $totalPersona[$index]['time'] = $value['time'];
                    $totalPersona[$index]['name'] = $NamePersona[$index]['name'];
                }
            }

            $wagePersona = 0;
            foreach ($totalPersona as $value) {
                $wagePersona += $value['wage'];
                $timePersonas[] = $value['time'];
                $namePersonas['name'] = $value['name'];
            }
            $timePersona = ApiReport::CalMinute($timePersonas);
            //print_r($wagePersona); //ค่าแรง 1 คน
            //print_r($timePersona); //เวลา 1 คน (หน่วยเป็นนาที)
            //print_r($namePersonas); //ชื่อของคนที่ค้นหา
            //exit();

            $typeCar = ApiPersonal::getTypeCar($type_company,$technician_TypeCar);
            $company = ApiPersonal::getCompany($type_company,$dateStart,$dateEnd);

            foreach ($typeCar as $index => $value) { //รวมช่าง ประเภทรถและบริษัท
                if (array_key_exists($index, $company)) {
                    $dataTC[$company[$index]['cvhc_main_id']]['name'] = $value['name'];
                }
            }

            foreach ($timeAndWage as $index => $value) {
                if (array_key_exists($index, $dataTC)) {
                    $sumWage = array_sum($value);
                    $totalTC[$index]['wage'] = $sumWage;
                    $totalTC[$index]['time'] = $value['time'];
                    $totalTC[$index]['name'] = $dataTC[$index]['name'];
                }
            }

        }

        /*echo count($totalTC);
        print_r($_arrRET['total_workday']);
        exit();*/

        $session = Yii::$app->session;

        $session->set('totalTC', $totalTC);
        $session->set('_arrRET', $_arrRET);
        $session->set('namePersonas', $namePersonas);
        $session->set('wagePersona', $wagePersona);
        $session->set('timePersona', $timePersona);

        return $this->render('pfm_repairman_day',
            [
                'selected_company'=>$selected_company,
                'selected_technician'=>$selected_technician,
                'selected_technician_id'=>$selected_technician_id,
                'selected_date'=>$selected_date,
                'company' => $company,
                //'years' => $years,
                //'month' => $month,
                'totalTC' => $totalTC,
                '_arrRET' => $_arrRET,
                'namePersonas' => $namePersonas,
                'wagePersona' => $wagePersona,
                'timePersona' => $timePersona,
            ]
        );
    }


    public function actionBtn_reportday()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        //$mpdf->WriteHTML("This is test");
        $mpdf->WriteHTML($this->renderPartial('_reportday'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportmonth()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportmonth'));
        $mpdf->Output();
        exit;

    }

}
