<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "menu_manage_request".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $working_company_id
 * @property integer $department_id
 * @property integer $section_id
 * @property integer $position_id
 * @property string $id_card
 * @property string $skill
 * @property integer $change_to
 * @property integer $approve_status
 * @property string $date_request
 * @property string $date_approve
 * @property string $reason_request
 * @property string $status
 */
class MenuManageRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_manage_request';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'working_company_id', 'department_id', 'section_id', 'position_id', 'change_to', 'approve_status'], 'integer'],
            [['date_request', 'date_approve'], 'safe'],
            [['id_card'], 'string', 'max' => 13],
            [['skill'], 'string', 'max' => 1],
            [['reason_request'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'working_company_id' => 'Working Company ID',
            'department_id' => 'Department ID',
            'section_id' => 'Section ID',
            'position_id' => 'Position ID',
            'id_card' => 'Id Card',
            'skill' => 'Skill',
            'change_to' => 'Change To',
            'approve_status' => 'Approve Status',
            'date_request' => 'Date Request',
            'date_approve' => 'Date Approve',
            'reason_request' => 'Reason Request',
            'status' => 'Status',
        ];
    }
}
