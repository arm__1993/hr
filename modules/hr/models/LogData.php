<?php

namespace app\modules\hr\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use app\api\Helper;

/**
 * This is the model class for table "log_data".
 *
 * @property string $id
 * @property string $action_type
 * @property string $table_name
 * @property integer $record_id
 * @property string $field_name
 * @property string $old_data
 * @property string $update_data
 * @property string $user_account
 * @property string $controller_id
 * @property string $action_id
 * @property string $host_name
 * @property string $ip_address
 * @property string $log_datetime
 */
class LogData extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hrlog_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id'], 'integer'],
            [['log_datetime'], 'safe'],
            [['action_type'], 'string', 'max' => 10],
            [['table_name', 'field_name', 'controller_id', 'action_id', 'host_name'], 'string', 'max' => 100],
            [['old_data', 'replace_data'], 'string', 'max' => 255],
            [['user_account', 'ip_address'], 'string', 'max' => 50]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['log_datetime'],
                    //ActiveRecord::EVENT_BEFORE_UPDATE => 'update_datetime',
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['user_account'],
                   // ActiveRecord::EVENT_BEFORE_UPDATE => 'update_byuser',
                ],
                'value' => $_SESSION['USER_ACCOUNT'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_type' => 'INSERT, UPDATE, DELETE',
            'table_name' => 'Table Name',
            'record_id' => 'Record ID',
            'field_name' => 'Field Name',
            'old_data' => 'Old Data',
            'replace_data' => 'Replace Data',
            'user_account' => 'User Account',
            'controller_id' => 'Controller ID',
            'action_id' => 'Action ID',
            'host_name' => 'Host Name',
            'ip_address' => 'Ip Address',
            'log_datetime' => 'Log Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return LogDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LogDataQuery(get_called_class());
    }
}
