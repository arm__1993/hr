<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "ADD_DEDUCT_TEMPLATE".
 *
 * @property integer $ADD_DEDUCT_TEMPLATE_ID
 * @property string $ADD_DEDUCT_TEMPLATE_NAME
 * @property string $ADD_DEDUCT_TEMPLATE_AMOUNT
 * @property string $ADD_DEDUCT_TEMPLATE_START_DATE
 * @property string $ADD_DEDUCT_TEMPLATE_END_DATE
 * @property string $ADD_DEDUCT_TEMPLATE_TYPE
 * @property integer $ADD_DEDUCT_TEMPLATE_STATUS
 * @property string $ADD_DEDUCT_TEMPLATE_LOAN_REPORT
 * @property string $ADD_DEDUCT_TEMPLATE_CREATE_DATE
 * @property string $ADD_DEDUCT_TEMPLATE_CREATE_BY
 * @property string $ADD_DEDUCT_TEMPLATE_UPDATE_DATE
 * @property string $ADD_DEDUCT_TEMPLATE_UPDATE_BY
 * @property integer $tax_section_id
 * @property integer $taxincome_type_id
 * @property string $tax_rate
 * @property string $accounting_code_pk
 */
class Adddeducttemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ADD_DEDUCT_TEMPLATE';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ADD_DEDUCT_TEMPLATE_NAME', 'ADD_DEDUCT_TEMPLATE_TYPE', 'ADD_DEDUCT_TEMPLATE_STATUS', 'ADD_DEDUCT_TEMPLATE_CREATE_DATE'], 'required'],
            [['ADD_DEDUCT_TEMPLATE_AMOUNT', 'tax_rate'], 'number'],
            [['ADD_DEDUCT_TEMPLATE_START_DATE', 'ADD_DEDUCT_TEMPLATE_END_DATE', 'ADD_DEDUCT_TEMPLATE_CREATE_DATE', 'ADD_DEDUCT_TEMPLATE_UPDATE_DATE'], 'safe'],
            [['ADD_DEDUCT_TEMPLATE_STATUS', 'tax_section_id', 'taxincome_type_id', 'accounting_code_pk'], 'integer'],
            [['ADD_DEDUCT_TEMPLATE_NAME', 'ADD_DEDUCT_TEMPLATE_CREATE_BY', 'ADD_DEDUCT_TEMPLATE_UPDATE_BY'], 'string', 'max' => 250],
            [['ADD_DEDUCT_TEMPLATE_TYPE'], 'string', 'max' => 2],
            [['accounting_code_pk'], 'string', 'max' => 50],
            [['ADD_DEDUCT_TEMPLATE_LOAN_REPORT'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ADD_DEDUCT_TEMPLATE_ID' => 'Add  Deduct  Template  ID',
            'ADD_DEDUCT_TEMPLATE_NAME' => 'Add  Deduct  Template  Name',
            'ADD_DEDUCT_TEMPLATE_AMOUNT' => 'Add  Deduct  Template  Amount',
            'ADD_DEDUCT_TEMPLATE_START_DATE' => 'Add  Deduct  Template  Start  Date',
            'ADD_DEDUCT_TEMPLATE_END_DATE' => 'Add  Deduct  Template  End  Date',
            'ADD_DEDUCT_TEMPLATE_TYPE' => 'Add  Deduct  Template  Type',
            'ADD_DEDUCT_TEMPLATE_STATUS' => 'Add  Deduct  Template  Status',
            'ADD_DEDUCT_TEMPLATE_LOAN_REPORT' => 'Add  Deduct  Template  Loan  Report',
            'ADD_DEDUCT_TEMPLATE_CREATE_DATE' => 'Add  Deduct  Template  Create  Date',
            'ADD_DEDUCT_TEMPLATE_CREATE_BY' => 'Add  Deduct  Template  Create  By',
            'ADD_DEDUCT_TEMPLATE_UPDATE_DATE' => 'Add  Deduct  Template  Update  Date',
            'ADD_DEDUCT_TEMPLATE_UPDATE_BY' => 'Add  Deduct  Template  Update  By',
            'tax_section_id' => 'Tax Section ID',
            'taxincome_type_id' => 'Taxincome Type ID',
            'tax_rate' => 'Tax Rate',
            'accounting_code_pk' => 'Accounting Code Pk',
        ];
    }

    public function searchaddeduct($params){
 
        $model = Adddeducttemplate::find();
        $dataProvider = new ActiveDataProvider(
                [
                    'query'=>$model,
                    'pagination' => ['pageSize' => 10,],
                ]
            );
        $this->load($params);
        $model->andFilterWhere(['like','ADD_DEDUCT_TEMPLATE_NAME', $this->ADD_DEDUCT_TEMPLATE_NAME]);
        $model->andFilterWhere(['=','ADD_DEDUCT_TEMPLATE_TYPE','1']);
        $model->andFilterWhere(['not like','ADD_DEDUCT_TEMPLATE_STATUS','99']);
        return $dataProvider;
    }

    public function searchdeduct($params){
 
        $model = Adddeducttemplate::find();
        $dataProvider = new ActiveDataProvider(
                [
                    'query'=>$model,
                    'pagination' => ['pageSize' => 10,],
                ]
            );
        $this->load($params);
        $model->andFilterWhere(['like','ADD_DEDUCT_TEMPLATE_NAME', $this->ADD_DEDUCT_TEMPLATE_NAME]);
        $model->andFilterWhere(['not like','ADD_DEDUCT_TEMPLATE_STATUS','99']);
        $model->andFilterWhere(['=','ADD_DEDUCT_TEMPLATE_TYPE','2']);
        return $dataProvider;
    }
}
