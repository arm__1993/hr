<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:32
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/global';
$imgaction = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/makesalary.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);

//$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->baseUrl."/fonts/01thaifontcss.css");


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li class="active">สร้างข้อมูลเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <i class="fa fa-bullhorn"></i>
                            <h3 class="box-title">สร้างเงินเดือนระหว่างเดือน</h3>
                        </div>
                        <div class="box-body" id="btnCreateSalaryฺBetween">
                            <div class="callout callout-default">
                                <ul>
                                    <li>#1.ค่าเป้าต้นเดือน (10/07/2017) <img src="<?php echo $imgaction; ?>/checked.png" data-toggle="tooltip" title="ยืนยันแล้ว"></li>
                                    <li>#2.ค่าเป้ากลางเดือน (10/07/2017) <img src="<?php echo $imgaction; ?>/cancel.png" data-toggle="tooltip" title="ยังไม่ได้ยืนยัน">&nbsp;<img src="<?php echo $imgaction; ?>/trash.png" style="cursor:pointer;" onclick="dropSalaryBetween()" data-toggle="tooltip" title="ลบเงินเดือนระหว่างเดือน"></li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-body" id="btnCreateSalaryWeekly_1">
                            <div class="callout callout-info text-center">
                                <h4 id="weekly_status_text_1">สร้างเงินเดือนเดือนระหว่างเดือน <?php echo date('m-Y'); ?> (1) เรียบร้อยแล้ว</h4>
                                <div id="weekly_loadimage_1" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <input type="text" id="txtWageName" name="txtWageName" class="form-control" placeholder="โปรดระบุชื่อรอบการสร้างเงินเดือน" style="margin-top: 8px; margin-bottom: 8px;">
                                <div class="input-group date col-sm-3">
                                    <input type="text" readonly="readonly" name="salary_between_month" data-required="true"
                                           id="salary_between_month" value="<?php echo date('m-Y'); ?>" class="form-control"
                                           placeholder="mm-yyyy">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_weekly_salary_1" onclick="makeSaralyฺBetweenMonth('make_weekly_salary_1','weekly_status_text_1','delete_weekly_salary_1','weekly_loadimage_1',2,1)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือนระหว่างเดือน</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_weekly_salary_1" onclick="deleteSaraly('make_weekly_salary_1','weekly_status_text_1','delete_weekly_salary_1','weekly_loadimage_1',2,1)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?> (1)</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-bullhorn"></i>
                            <h3 class="box-title">สร้างเงินเดือน ณ สิ้นเดือน</h3>
                        </div>
                        <div class="box-body" id="btnCreateSalary">
                            <div class="callout callout-success text-center">
                                <h4 id="monthly_status_text">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> เรียบร้อยแล้ว</h4>
                                <div id="monthly_loadimage" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_monthly_salary" onclick="makeSaraly('make_monthly_salary','monthly_status_text','delete_monthly_salary','monthly_loadimage',3,1)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_monthly_salary" onclick="deleteSaraly('make_monthly_salary','monthly_status_text','delete_monthly_salary','monthly_loadimage',3,1)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?></button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span id="pay_date" title="<?php echo date('m-Y'); ?>"></span>
    <!-- /.box -->
</section><!-- /.content -->