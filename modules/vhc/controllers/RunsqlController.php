<?php

namespace app\modules\vhc\controllers;
use app\modules\vhc\Vhc;
use Yii;
use app\modules\vhc\controllers\MasterController;
use app\modules\vhc\models\VhcGrouplist;
use app\modules\vhc\models\VhcList;
use app\modules\vhc\models\VhcListOption;
use app\modules\vhc\models\VhcServicetype;
use app\modules\vhc\models\VhcBlame;
use app\modules\vhc\models\VhcModel;
class RunsqlController extends MasterController
{
    public function actionIndexx()
    {
        $postValue[0]['option_name'] = 'ปกติ';
        $postValue[0]['option_value'] = 1;
        $postValue[0]['record_status'] = 1;
        $postValue[0]['comment'] ='yes';
        $postValue[1]['option_name'] = 'ไม่ปกติ';
        $postValue[1]['option_value'] = 0;
        $postValue[1]['record_status'] = 1;
        $postValue[1]['comment'] ='no';
        $list = VhcList::find()->asArray()->all();
        foreach ($list as $value) {
            $option = new VhcListOption();
            foreach($postValue as $item){
                $list_id= $value['id'];
                $option_name= $item['option_name'];
                $option_value= $item['option_value'];
                $record_status= $item['record_status'];
                $conment = $item['comment'];
                $model = new VhcListOption();
                $model->list_id = $list_id;
                $model->option_name = $option_name;
                $model->option_value = $option_value;
                $model->record_status = $record_status;
                $model->comment = $conment;
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }

    public function actionCreatelist()
    {


        $model[1] = 'Spark(หัวเดี่ยว)';
        $model[2] = 'Space cab(แค็ป)';
        $model[3] = 'Cab 4 (4 ประตู)';
        $model[4] = 'SUV (รถนั่งเอนกประสงค์)';

        $group[1] = 'อุปกรณ์ติดตั้งภายในรถ';
        $group[2] = 'สิ่งของอื่นๆภายในรถ';
        $group[3] = 'สภาพภายในตัวรถ';
        $group[4] = 'สภาพภายนอกตัวรถ';
        $group[5] = 'สภาพในห้องเครื่อง';
        $group[6] = 'สภาพใต้ท้องรถ';
        //$group[7] = 'ยางรถยนต์';


        //อุปกรณ์ติดตั้งภายในรถ
        $grouplist[1][1] = 'รีโมทกุญแจ';
        $grouplist[1][2] = 'วิทยุ/CD/DVD/รีโมทคอนโทรล';
        $grouplist[1][3] = 'ที่จุดบุรี่';

        //สิ่งของอื่นๆภายในรถ
        $grouplist[2][4] = 'ม้วนเทป,กระเป๋า CD';
        $grouplist[2][5] = 'กล้องถ่ายรูป';
        $grouplist[2][6] = 'ยางวางรองเท้า';
        $grouplist[2][7] = 'ร่ม,หมวก';
        $grouplist[2][8] = 'กระเป๋า,เอกสาร';
        $grouplist[2][9] = 'เสื้อผ้า,รองเท้า';
        $grouplist[2][10] = 'ที่ชาร์จแบตมือถือ';
        $grouplist[2][11] = 'พระเครื่อง';
        $grouplist[2][12] = 'แว่นตา';
        $grouplist[2][13] = 'ชุดหูฟัง';

        //สภาพภายในตัวรถ
        $grouplist[3][14] = 'ระยะฟรีเบรกมือ';
        $grouplist[3][15] = 'ระยะฟรีขาเบรก/คลัตช์';
        $grouplist[3][16] = 'ระบบส่องสว่างภายในรถ/ระบบไฟฟ้ามาตรวัด';
        $grouplist[3][17] = 'ความเสียหายและความเสื่อมสภาพภายในโดยรวม';
        $grouplist[3][18] = 'การทำงานของกระจก(การเลื่อนขึ้น-ลง)';
        $grouplist[3][19] = 'เข็มขัดนิรภัย ความปลอดภัยและการทำงาน';


        //สภาพภายนอกตัวรถ
        $grouplist[4][20] = 'กันสาด L,R';
        $grouplist[4][21] = 'กันแมลง';
        $grouplist[4][22] = 'กระจกมองข้าง L,R';
        $grouplist[4][23] = 'ไฟเลี้ยว L,R';
        $grouplist[4][24] = 'ไฟท้าย L,R';
        $grouplist[4][25] = 'ไฟหรี่ L,R';
        $grouplist[4][26] = 'ไฟหน้า L,R';
        $grouplist[4][27] = 'ไฟส่องป้าย';
        $grouplist[4][28] = 'ช่องปิดยางอะไหล่';
        $grouplist[4][29] = 'ยางอะไหล่';
        $grouplist[4][30] = 'สภาพยางปัดน้ำฝน L,R';
        $grouplist[4][31] = 'กระจกบังลม F,B';


        //สภาพในห้องเครื่อง
        $grouplist[5][32] = 'ระดับน้ำมันพวงมาลัยพาวเวอร์';
        $grouplist[5][33] = 'สภาพแบตเตอร์รี่ ยี่ห้อ....';
        $grouplist[5][34] = 'ระดับน้ำกลั่น';
        $grouplist[5][35] = 'สายพานหน้าเครื่อง/สายพานแอร์';
        $grouplist[5][36] = 'เครื่องยนต์(การรั่วซึม ความเสียหาย)';
        $grouplist[5][37] = 'ระดับน้ำมันเบรก/คลัช';
        $grouplist[5][38] = 'ระดับของเหลวหล่อเย็น(Coolant)';
        $grouplist[5][39] = 'สภาพท่อยางหม้อน้ำ';
        $grouplist[5][40] = 'ระดับน้ำมันเครื่อง';

        //สภาพใต้ท้องรถ
        $grouplist[6][41] = 'สภาพด้านล่างตัวรถ(รั่วซึม/เสียหาย)';
        $grouplist[6][42] = 'ลูกหมากต่างๆลูกยางปีกนก';
        $grouplist[6][43] = 'ลูกปืนล้อหน้า ซ้าย,ขวา';
        $grouplist[6][44] = 'สภาพจานดิสก์เบรกหน้า/ผ้าเบรก';
        $grouplist[6][45] = 'รอยรั่วซึมเกียร์/เฟืองท้าย';
        $grouplist[6][46] = 'ระยะคลัชล่าง .....​มม.';
        $grouplist[6][47] = 'เพลากลาง/เพลาข้างซ้าย,ขวา/เพลาหลัง';
        $grouplist[6][48] = 'สภาพโช้คอัพ/ลูกยางแหนบ';
        $grouplist[6][49] = 'ระบบท่อไอเสีย';



        $connection = \Yii::$app->dbERP_service;
        $transaction = $connection->beginTransaction();
        try {

            $connection->createCommand()->truncateTable('vhc_grouplist')->execute();
            $connection->createCommand()->truncateTable('vhc_list')->execute();
            $connection->createCommand()->truncateTable('vhc_list_option')->execute();

            foreach ($model as $key => $value) {
                $model_id = $key;
                foreach ($group as $groupID => $groupName) {
                    $connection->createCommand()->insert('vhc_grouplist', [
                        'model_id' => $model_id,
                        'group_name' => $groupName,
                        'record_status' => 1,
                    ])->execute();

                    $vhc_grouplist_id = $connection->getLastInsertID();


                //foreach ($group as $key => $value) {
                    $arrList = $grouplist[$groupID];
                    echo '<pre>';
                    print_r($arrList);
                    echo '</pre>';

                    foreach ($arrList as $list) {
                        $connection->createCommand()->insert('vhc_list', [
                            'group_id' => $vhc_grouplist_id,
                            'list_name' => $list,
                            'record_status' => 1,
                        ])->execute();

                        $list_id = $connection->getLastInsertID();

                        $connection->createCommand()->insert('vhc_list_option', [
                            'list_id' => $list_id,
                            'option_name' => ($groupID==1 || $groupID==2) ? 'มี' : 'ปกติ',
                            'option_value' => 1,
                            'record_status' => 1,
                        ])->execute();

                        $connection->createCommand()->insert('vhc_list_option', [
                            'list_id' => $list_id,
                            'option_name' => ($groupID==1 || $groupID==2) ? 'ไมมี' : 'ไม่ปกติ',
                            'option_value' => 0,
                            'record_status' => 1,
                        ])->execute();

                    }
                }
            }

            $transaction->commit();
        }
        catch (ErrorException $e) {
            $transaction->rollBack();
            return 0;
        }

    }
  
}