<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:27
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

echo $imghr = Yii::$app->request->BaseUrl . '/images/wshr';

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/organizechart.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery.orgchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/jquery.orgchart.css', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li class="active">ผังโครงสร้างองค์กร</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div id="orgChartContainer">
        <div id="orgChart"></div>
        </div>
            <div id="consoleOutput">
        </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->

lt0109/source/htdoc/wseasyerp/images/wshr