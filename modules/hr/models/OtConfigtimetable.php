<?php

namespace app\modules\hr\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "ot_configtimetable".
 *
 * @property integer $id
 * @property string $profile_name
 * @property string $time_start
 * @property string $time_end
 * @property string $budget
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class OtConfigtimetable extends \yii\db\ActiveRecord
{

    protected $_pageSize;

    function __construct() {
        $this->_pageSize = Yii::$app->params['PAGE_SIZE'];
    }


    public function behaviors()
    {
        $session = Yii::$app->session;
        $session->open();
        $_account = $session->get('idcard');
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_datetime'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_datetime'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createby_user'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateby_user'],
                ],
                'value' => $_account, //$_SESSION['USER_ACCOUNT'],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ot_configtimetable';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['profile_name'], 'required'],
            [['time_start', 'time_end', 'create_datetime', 'update_datetime'], 'safe'],
            [['budget'], 'number'],
            [['status_active'], 'integer'],
            [['profile_name'], 'string', 'max' => 200],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_name' => 'Profile Name',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'budget' => 'Budget',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return OtConfigtimetableQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OtConfigtimetableQuery(get_called_class());
    }

    public function search($params)
    {

        $query = OtConfigtimetable::find()->where('status_active != :del', [':del' => Yii::$app->params['DELETE_STATUS']]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_datetime' => $this->create_datetime,
        ]);

        $query->andFilterWhere(['like', 'profile_name', $this->profile_name])
            ->andFilterWhere(['like', 'status_active', $this->status_active])
            ->andFilterWhere(['like', 'create_byuser', $this->createby_user])
            ->andFilterWhere(['like', 'update_byuser', $this->updateby_user]);
        return $dataProvider;
    }
}
