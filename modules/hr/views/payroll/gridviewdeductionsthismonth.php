<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use yii\widgets\Pjax;
use yii\grid\GridView;
use app\modules\hr\models\Adddeductdetail;
$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/urlpathconfig.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/deductionsthismonth.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
// //$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);


$script = <<< JS
$(document).ready(function() {
    setTimeout(function() {
    $('#message').fadeOut('slow');
    }, 3000);


$("#example2").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );

$("#tablelistdata").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );
});
JS;

$this->registerCssFile(Yii::$app->request->BaseUrl."/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->BaseUrl."/fonts/01thaifontcss.css");


$this->registerJs($script);
?>
<style type="text/css">
      .modal-wide .modal-dialog {
               width: 90%; /* or whatever you wish */
        }
</style>
<section class="content">
<?php 
Pjax::begin(['id' => 'pjax_grid_step']);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'Add_Deductdetail_Idemp',
        'Add_Deducttemplate_Type',

    ],
]); 
Pjax::end(); //end pjax_gridcorclub
?>

</section><!-- /.content -->
