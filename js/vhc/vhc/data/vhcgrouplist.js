$(document).ready(function () {
    ///ส่วนจ่าย///
    $("#btnSaveActivitygrouplist").on("click", function () {
        var f = frmValidate('frmAddVhcgrouplist');
        if (!f) {
            // console.log('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy');
            showWarningInputForm();
        } else {
            $('.record_status').val('1');
            // console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            savegrouplist(1);

        }
    });

    $('#btnAddNewVhcgrouplist').on("click", function () {
        initForm('btnSaveActivitygrouplist');

    });

    ///ส่วนหัก///
    $("#btnSaveActivityserviceDeduct").on("click", function () {
        var f = frmValidate('frmDeduct');
        if (!f) {
            showWarningInputForm();
        } else {
            $('.record_status').val('2');
            savegrouplist(2);
        }
    });

    $('#btnAddNewVhcgrouplist').on("click", function () {
        initForm('btnSaveActivitygrouplist');
        $('#hide_activityedit_grouplist').val('');
        $('#group_name').val('');
        $('#record_status_grouplist').prop('checked', true);
    });
});

function deletevhcgrouplist(id, valselect) { //อันนี้ยกเลิกเน้อออออออออออออออออ
    // alert(id);
    $.ajax({
        url: 'deletevhcgrouplist',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showDeleteSuccess();
                $.pjax.reload({container: "#pjax_tb_grouplist"});  //Reload GridView

                selecttab(valselect);
            } else {
                showDeleteError();
                console.log(data);

            }
        }
    });
}
function grouplist(idselectform) {
    if (idselectform == 1) {
        var datavar = $('#frmAddVhcservice').serialize();
    } else {
        var datavar = $('#frmDeduct').serialize();
    }
    // console.log(datavar);
    $.ajax({
        url: 'grouplist',
        data: datavar,
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showSaveSuccess();
                if (idselectform == 1) {
                    initForm('frmAddVhcgrouplist');
                } else {
                    initForm('frmDeduct');
                }

                selecttab(idselectform);
                //   $.pjax.reload({container: "#pjax_tb_vhcservicetype"});  //Reload GridView
            } else {
                showSaveError();
            }
        }
    });
}

function editgrouplist(id) {//แก้ไขเน้อออออออออออ
    // console.log(id);
    initForm('frmAddVhcgrouplist');//idฟอมมมมม
    $.ajax({
        url: 'updatevhcgrouplist',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            //  console.log(data);
            //
            if(data.record_status==1){
                $('#record_status_grouplist').prop('checked', true);
            }else {
                $('#record_status_grouplist').prop('checked', false);
            }

            $('#hide_activityedit_grouplist').val(data.id);
            $('#model_id').val(data.model_id);
            $('#group_name').val(data.group_name);
            $('#modalfrmAddVhcgrouplist').modal();
        }
    });
}

function savegrouplist() {//อันนี้นี้เซฟเน้ออออออออออออออออ
    var datavar = $('#frmAddVhcgrouplist').serialize();

    console.log(datavar);
    $.ajax({
        url: 'grouplist',
        data: datavar,
        type: 'POST',
        success: function (data) {
            console.log(data);
            if (parseInt(data) == 1) {
                showSaveSuccess();
                initForm('frmAddVhcgrouplist');
                $.pjax.reload({container: "#pjax_tb_grouplist"});  //Reload GridView
            }
            else {
                showSaveError();
            }
        }
    });
}

function selecttab(val) {
    //alert("2222" + val);
    // console.log($("#tabselect" + val));
    $(".tabselect").removeClass("active");
    $("#tabselect" + val).addClass("active");
    $("#tab" + val).addClass("active");

    if (val == 1) {
        $.pjax.reload({container: "#pjax_tb_adddeducttemp"}); //Reload GridView
    } else {
        $.pjax.reload({container: "#pjax_tb_grouplist"}); //Reload GridView
    }


}