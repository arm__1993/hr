<?php
/* @var $this yii\web\View */
use app\modules\hr\apihr\ApiHr;


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use app\api\Utility;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

//use app\api\AjaxSubmitButton;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/saveconfigmapsso.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity

?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li>ตั้งค่าข้อมูลองค์กร</li>
                <li class="active">ตั้งการสถานะประกันสังคม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="row">
                <div class="pull-right" style="padding-right: 15px;">
                    <?php
                    Modal::begin([
                        'id' => 'modelProvider',
                        'header' => '<strong>ฟอร์มบันทึกข้อมูลคำนำหน้า</strong>',
                        'toggleButton' => [
                            'id' => 'btnAddNewBtitle',
                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มสถานะการลาออก ',
                            'class' => 'btn btn-success'
                        ],
                        'closeButton' => [
                            'label' => '<i class="fa fa-close"></i>',
                            //'class' => 'close pull-right',
                            'class' => 'btn btn-success btn-sm pull-right'
                        ],
                        'size' => 'modal-sm'
                    ]);
                    ?>
                    <form id="frmConfigmaping" onsubmit="return getdatesubmit1();" data-toggle="validator" method="post" >
                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                        <div class="form-group">
                            <div class="box-body">
                                <label>รหัสสถานะประกันสังคม<span>*</span></label>
                                <input id="id_sso" name="id_sso" data-required="true"
                                       class="form-control" type="text"  required="true" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="box-body">
                                <label>สาเหตุการสิ้นสุดความเป็นผู้ประกันตน<span>*</span></label>
                                <input id="namestatussso" name="namestatussso" data-required="true"
                                       class="form-control" type="text"  required="true" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="box-body">
                                <label>สถานะการลาออก (โปรแกรม HR )<span>*</span></label>
                                <input id="namestatushr" name="namestatushr" data-required="true"
                                       class="form-control" type="text"  required="true"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="box-body">
                                <label>สถานะ</label>
                                <select class="form-control status_active" id="status"
                                        name="status">
                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                        Active
                                    </option>
                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                        In Active
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo Html::hiddenInput('hide_activityconfigmaping', null, ['id' => 'hide_activityconfigmaping']); ?>
                            <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                    <?php
                    Modal::end();
                    ?>
                </div>
                        <div class="col-sm-12">
                            <?php
                            Pjax::begin(['id' => 'pjax_tb_betitel']);
                            echo GridView::widget([
                                'id'=>'G1',
                                'dataProvider' => $modelProvider,
                                'filterModel' => $modelSearch,
                                //'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                'columns' => [

                                    [
                                        'class' =>'yii\grid\SerialColumn',
                                        'headerOptions' => ['width' => '20px'],
                                    ],
                                    [
                                        'attribute' => 'id_sso',
                                        'label' => 'รหัสสถานะประกันสังคม',
                                        'value' => 'id_sso',
                                        'contentOptions' => ['style' => 'width:20px']
                                    ],
                                    [
                                        'attribute' => 'namestatussso',
                                        'label' => 'สาเหตุการสิ้นสุดความเป็นผู้ประกันตน',
                                        'value' => 'namestatussso',
                                        'contentOptions' => ['style' => 'width:20px']
                                    ],
                                    [
                                        'attribute' => 'namestatushr',
                                        'label' => 'สถานะการลาออก (โปรแกรม HR )',
                                        'value' => 'namestatushr',
                                        'contentOptions' => ['style' => 'width:20px']
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'label' => 'สถานะการใช้งาน',
                                        //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                        'format' => 'image',
                                        'value' => function ($data) {
                                            return Utility::dispActive($data->status);
                                        },
                                        'filter' => false,
                                        'contentOptions' => ['style' => 'width:5px']
                                    ],
                                    [
                                        'headerOptions' => ['width' => '100'],
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{update}  &nbsp; {delete}',
                                        'buttons' => [
                                            'update' => function ($url, $data) {
                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                    'title' => 'แก้ไข',
                                                    'onclick' => '(function($event) {
                                                                        updateconfigmapping(' . $data->id. ');
                                                                })();'
                                                ]);
                                            },

                                            'delete' => function ($url, $data) {
                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                    'title' => 'ลบ',
                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deleteconfigmapsso(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                ]);
                                            },
                                        ],
                                    ],

                                ]
                            ]);
                            Pjax::end();
                            ?>
                </div>
            </div>
        </div>
        <!-- /.box -->
</section><!-- /.content -->
