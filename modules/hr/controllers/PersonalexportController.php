<?php

namespace app\modules\hr\controllers;

use app\modules\hr\controllers\PersonalreportController;
use Yii;
use app\modules\hr\controllers\MasterController;
class PersonalexportController extends MasterController
{

    public $layout = 'hrlayout';
    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }



    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEmpexport()
    {

        $data = [];

        $personal = new PersonalreportController($this->id,$this->module);

        $companyinfo = $personal->getObjCompany();

        return $this->render('empexport',[
            'data' => $data,
            'companyinfo' => json_encode($companyinfo),
            'emp_data' => 'emp_data',
            'emp_family' => 'emp_family',
            'children' => 'children',
            'tax_income' => 'tax_income',
            'education' => 'education',
            'work_history' => 'work_history',
            'emp_money_bonds' => 'emp_money_bonds',
            'emp_promise' => 'emp_promise',
            'EMP_SALARY' => 'EMP_SALARY',
            'SALARY_CHANGE' => 'SALARY_CHANGE'
        ]);
    }

   

    public function actionGetpersonalinfo(){

        return Yii::$app->controller->renderPartial('personalInfo',[
            'emp_data' => 'emp_data',
            'emp_family' => 'emp_family',
            'children' => 'children',
            'tax_income' => 'tax_income',
            'education' => 'education'
        ]);
    }

    public function actionExportempfile(){

        ini_set("memory_limit","900M");
        //set_time_limit(500);
        $param    = Yii::$app->request->post();

        //echo "<pre>";print_r($param);echo "</pre>";exit();
        $map_titles= json_decode($param['map_titles'],true);
        $allData = [];
        $allData['status'] = true;
       
        //echo "<pre>";print_r($allData['map_titles']);echo "</pre>";exit();
        try{
            //$time_s = time();
            $allData = $this->getEmpdata(
                    json_decode($param['filter'],true), 
                    json_decode($param['sel_field'],true),
                    json_decode($param['sel_field_sp'],true)
                );

            //echo "time1=>".(time() - $time_s); 
           

            $allData['name'] = $this->createEmpExcel($allData['head'],$allData['data'],$map_titles);
           

        } catch(Exception $e){

            $allData['status'] = false;
            $allData['msg']      = "เกิดข้อผิดพลาด<br>".$e->getMessage();
            $allData['line']     = $e->getLine();
            $allData['file']      = $e->getFile();
           
        }
        return json_encode($allData);
    }

    public function actionDownloadempexcel(){

        $param    = Yii::$app->request->get();

        $name = $param['name'].'.xls';
        $path = 'upload/file/'.$name;
        Yii::$app->response->sendFile($path); 
    }

   

    public function getRangChar($st = 'A', $end = 'ZZ'){

        $ar = [];
        for ($i = $st; $i < $end; $i++) {
             
             $ar[] = $i;
        }

        return $ar;
    }


    public function createEmpExcel($heads, $datas, $titles){

        $excelName = 'emp_data_info_'.date('ymd');
        set_time_limit(0);

        $objPHPExcel = new \PHPExcel();

       // $time_s_1 = time();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $objPHPExcel->getDefaultStyle()->getFont()
            ->setName('tahoma')
            ->setSize(10);

        $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

        $chRang = $this->getRangChar();
        //head
        foreach($heads as $no => $hName){
            
            $objWorkSheet->setCellValue($chRang[$no].'1', $titles[$hName]);

        }
        

        $objWorkSheet->fromArray($datas, null, 'A2');

        //draw image
         $rowIndex = 2;
        foreach($datas as $dno => $data){
         
            $haveImage = false;

            foreach($heads as $hno => $hName){

                $objWorkSheet->getRowDimension($rowIndex)->setRowHeight(80);
 
                if($hName == 'Pictures_HyperL'){

                    $haveImage = true;
                    $objWorkSheet->getColumnDimension($chRang[$hno])->setWidth(20);

                    $basePath = Yii::$app->basePath; 

                    $pathEmp = $basePath.'/upload/personal/'.$data[$hName];

                    if(!file_exists($pathEmp)){
                        $pathEmp = $basePath.'/upload/personal/avatar2.png';
                    }

                    // Add a drawing to the worksheet
                    $objDrawing = new \PHPExcel_Worksheet_Drawing();
                    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                    // Generate an image
   
                    $objDrawing->setName("test name");
                    $objDrawing->setDescription($rowIndex, "test drec");
                    $objDrawing->setPath($pathEmp);
                    $objDrawing->setHeight(130);
                    $objDrawing->setWidth(100);
                    $objDrawing->setOffsetX(7);
                    $objDrawing->setOffsetY(3);
                    $objDrawing->setResizeProportional(true);
                    $objDrawing->setCoordinates($chRang[$hno].($rowIndex));
                    $objWorkSheet->getStyle($chRang[$hno].($rowIndex))->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objDrawing->getShadow()->setVisible(true);

                }

            }

            if($haveImage){
                $objWorkSheet->setCellValue('A'.$rowIndex, "");
            }

            $rowIndex++;

        }

        //--

        $objWorkSheet->setTitle('emp data');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$excelName.".xls");//=ชื่อไฟล์
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        //$test = 'hbso';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //$objWriter->save(str_replace(__FILE__,'upload/exportsalary/HBSO.xlsx',__FILE__));
        $objWriter->save('upload/file/'.$excelName.'.xls');

        return $excelName;
    }

    public function ____createEmpExcel($heads, $datas, $titles){

        $excelName = 'emp_data_info_'.date('ymd');
        set_time_limit(0);

        $objPHPExcel = new \PHPExcel();

       // $time_s_1 = time();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $objPHPExcel->getDefaultStyle()->getFont()
            ->setName('tahoma')
            ->setSize(10);

        $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
    
        $chRang = $this->getRangChar();

        //echo "\n time2=>".(time() - $time_s_1); 
        //$time_s_2 = time();
        //head
        foreach($heads as $no => $hName){
            
            $objWorkSheet->setCellValue($chRang[$no].'1', $titles[$hName]);

        }
        
       //echo "\n time3=>".(time() - $time_s_2); 
       $time_s_3 = time();
     
        
        //data
        $rowIndex = 2;
        foreach($datas as $dno => $data){
         
            foreach($heads as $hno => $hName){

                $objPHPExcel->getActiveSheet()->getRowDimension($rowIndex)->setRowHeight(80);
 
                if($hName == 'Pictures_HyperL'){

                    $objPHPExcel->getActiveSheet()->getColumnDimension($chRang[$hno])->setWidth(20);

                    $pathEmp = '/app/wseasyerp/upload/personal/'.$data[$hName];

                    if(!file_exists($pathEmp)){
                        $pathEmp = '/app/wseasyerp/upload/personal/avatar2.png';
                    }

                    // Add a drawing to the worksheet
                    $objDrawing = new \PHPExcel_Worksheet_Drawing();
                    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                    // Generate an image
                    $gdImage = @imagecreatetruecolor(130, 20) or die('Cannot Initialize new GD image stream');
                    $textColor = imagecolorallocate($gdImage, 255, 255, 255);
                    imagestring($gdImage, 1, 5, 5,  'Created with PHPExcel', $textColor);
                    
                    $objDrawing->setName("test name");
                    $objDrawing->setDescription($rowIndex, "test drec");
                    $objDrawing->setPath($pathEmp);
                    $objDrawing->setHeight(130);
                    $objDrawing->setWidth(100);
                    $objDrawing->setOffsetX(7);
                    $objDrawing->setOffsetY(3);
                    $objDrawing->setResizeProportional(true);
                    $objDrawing->setCoordinates($chRang[$hno].($rowIndex));
                    $objPHPExcel->getActiveSheet()->getStyle($chRang[$hno].($rowIndex))->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objDrawing->getShadow()->setVisible(true);

                }else{
                    $objWorkSheet->setCellValue($chRang[$hno].($rowIndex),$data[$hName]);
                }

                //$objWorkSheet->setCellValue($chRang[$hno].($rowIndex),$data[$hName]);
            }

            $rowIndex++;

        }

    //    echo "\n time4=>".(time() - $time_s_3); 
    //     exit();


        $objPHPExcel->getActiveSheet()->setTitle('emp data');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$excelName.".xls");//=ชื่อไฟล์
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        //$test = 'hbso';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //$objWriter->save(str_replace(__FILE__,'upload/exportsalary/HBSO.xlsx',__FILE__));
        $objWriter->save('upload/file/'.$excelName.'.xls');

        return $excelName;
    }

    public function actionShowtableempdata(){

        ini_set("memory_limit","528M");
        $param    = Yii::$app->request->post();

        //echo "<pre>";print_r($param);echo "</pre>";exit();
        $allData = [];
        $allData['status'] = true;
        try{

            $allData = $this->getEmpdata(
                    json_decode($param['filter'],true), 
                    json_decode($param['sel_field'],true),
                    json_decode($param['sel_field_sp'],true)
                );

        
            $allData['map_titles'] = json_decode($param['map_titles'],true);

        } catch(Exception $e){

            $allData['status'] = false;
            $allData['msg']      = "เกิดข้อผิดพลาด<br>".$e->getMessage();
            $allData['line']     = $e->getLine();
            $allData['file']      = $e->getFile();
           
        }
        return json_encode($allData);
    }


    public function getEmpdata($search, $fields, $fSp){
        $obj = [];
        $obj['head'] = [];
        $obj['data'] = [];
        $obj['status'] = true;
       
        try{
            $sql = $this->createSql($search,$fields, $fSp);

            $obj['data'] = Yii::$app
                    ->dbERP_easyhr_checktime
                    ->createCommand($sql)
                    ->queryAll();

            $first = $obj['data'][0];
            
            if(count($obj['data']) > 0){
                foreach($first as $f => $v){
                
                    $obj['head'][] = $f;
                } 
            }

         } catch(Exception $e){

            $obj['status'] = false;
            $obj['msg']      = "เกิดข้อผิดพลาด<br>".$e->getMessage();
            $obj['line']     = $e->getLine();
            $obj['file']      = $e->getFile();
            $obj['sql'] = $sql;
        }
            

         

        return $obj;
    }

    public function isImage(){

    }


    public function createSql($where, $fields, $fSp){

        

        //echo "<pre>";print_r($fields);echo "</pre>";exit();

       
        $tbKeep = [];

        //gen select
        $sqlSel = " ";
        $resSel = $this->sqlSelect($fields);
        $tbKeep = $resSel['tbKeep'];
        $sqlSel .= $resSel['sql'];

        $sqlSel .= $this->sqlSelectSpecial($fSp);
        //--
        

        //gen where

        //--

         //get table
        $sqlTable = " ";
        //unset($tbKeep['emp_data']);
        //echo  "<pre>";print_r($tbKeep);echo "</pre>";exit();

        if(count($tbKeep) > 0 && !in_array('emp_data',$tbKeep)){
            //echo '1';
            $tbKeep[] = 'emp_data';
            $sqlTable .= $this->createJoinSql($tbKeep);
        }else if(count($tbKeep) > 0){
            //echo '2';
            $sqlTable .= $this->createJoinSql($tbKeep);
        }
      
        // if(count($tbKeep) > 0){
           
        //     $sqlTable .= $this->createJoinSql($tbKeep);
            
        // }else{
        //     //$sqlTable .= " ".$this->fillNameDB($tbKeep[0])." ";
        // }
        //--

        $optWhere = $this->sqlWhere($where);

        $isPosition = (in_array('position',$optWhere['tb'])) ? true:false;

        $sql = "SELECT ".$sqlSel." FROM ".$this->fillNameDB('emp_data')." ".$sqlTable;
        $sql .= (!$isPosition) ? "":"INNER JOIN ERP_easyhr_OU.relation_position  ON(emp_data.ID_Card = relation_position.id_card) ";
        $sql .= (!$isPosition) ? "":" INNER JOIN ERP_easyhr_OU.position  ON(relation_position.position_id = position.id)";
        $sql .= " WHERE 1 ";
        $sql .=  $optWhere['where'];
        //$sql .= " LIMIT 600";

        //echo $sql;exit();

        return $sql;

    }

    public function sqlSelectSpecial($infs){

        $arWords = [
            'age' => [
                'tb'=>'emp_data',
                'sel' => '(DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), emp_data.Birthday)), "%Y")+0)',
                'word' => 'age'
            ],
            'addr1' => [
                'tb'=>'EMP_ADDRESS',
                'sel' => "(SELECT CONCAT(xx.ADDR_VILLAGE,' ', xx.ADDR_NUMBER,' หมู่ ',xx.ADDR_GROUP_NO,' ', xx.ADDR_SUB_DISTRICT,' ', xx.ADDR_DISTRICT,' ', xx.ADDR_PROVINCE) FROM ERP_easyhr_checktime.EMP_ADDRESS AS xx WHERE xx.ADDR_DATA_NO = emp_data.DataNo AND xx.ADDR_TYPE = 1)",
                'word' => 'addr1'
            ],
            'addr2' => [
                'tb'=>'EMP_ADDRESS',
                'sel' => "(SELECT CONCAT(xx.ADDR_VILLAGE,' ', xx.ADDR_NUMBER,' หมู่ ',xx.ADDR_GROUP_NO,' ', xx.ADDR_SUB_DISTRICT,' ', xx.ADDR_DISTRICT,' ', xx.ADDR_PROVINCE) FROM ERP_easyhr_checktime.EMP_ADDRESS AS xx WHERE xx.ADDR_DATA_NO = emp_data.DataNo AND xx.ADDR_TYPE = 2)",
                'word' => 'addr2'
            ],
            'father_age' =>[
                'tb'=>'emp_family',
                'sel' => '(DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), emp_family.father_birthday)), "%Y")+0)',
                'word' => 'father_age'
            ],
            'mother_age' =>[
                'tb'=>'emp_family',
                'sel' => '(DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), emp_family.mother_birthday)), "%Y")+0)',
                'word' => 'mother_age'
            ],
            'spouse_age' =>[
                'tb'=>'emp_family',
                'sel' => '(DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), emp_family.spouse_birthday)), "%Y")+0)',
                'word' => 'spouse_age'
            ],
            'count_child_boy' =>[
                'tb'=>'children',
                'sel' => '(SELECT COUNT(children.id) FROM ERP_easyhr_checktime.children WHERE children.emp_id = emp_data.ID_Card AND sex ="ชาย")',
                'word' => 'count_child_boy'
            ],
            'count_child_girl' =>[
                'tb'=>'children',
                'sel' => '(SELECT COUNT(children.id) FROM ERP_easyhr_checktime.children WHERE children.emp_id = emp_data.ID_Card AND sex ="หญิง")',
                'word' => 'count_child_girl'
            ],
            'sum_benefitfund' =>[
                'tb'=>'SOCIAL_SALARY',
                'sel' => '(SELECT SUM(SOCIAL_SALARY.SOCIAL_SALARY_AMOUNT) AS SOCIAL_SALARY FROM ERP_easyhr_PAYROLL.SOCIAL_SALARY WHERE SOCIAL_SALARY.SOCIAL_SALARY_EMP_ID = emp_data.ID_Card )',
                'word' => 'sum_benefitfund'
            ]

            

        ];

        $sql = "";
        foreach($infs as $no => $fd){

            if(isset($arWords[$fd])){
                $sql .= " ,".$arWords[$fd]['sel'].' AS '.$arWords[$fd]['word']." ";
            }
        }

        return $sql;

    }

    public function sqlWhere($oStr){

        $resOut = [];
        $resOut['tb'] = [];

        $whereA = " ";
        //บริษัท
        if(isset($oStr['company']) && $oStr['company'] != '' && $oStr['company'] != 'null'){
            $whereA .= " AND position.WorkCompany = '".$oStr['company']."' ";
            $resOut['tb']['position'] = 'position';
        }
        //department
        if(isset($oStr['department']) && $oStr['department'] != '' && $oStr['department'] != 'null'){
            $whereA .= " AND position.Department = '".$oStr['department']."' ";
        }
        //section
        if(isset($oStr['section']) && $oStr['section'] != ''  && $oStr['section'] != 'null'){
            $whereA .= " AND position.Section = '".$oStr['section']."' ";
            $resOut['tb']['position'] = 'position';
        }


        if(isset($oStr['work_start_begin']) && $oStr['work_start_begin'] != ''){
            $whereA .= " AND emp_data.Start_date BETWEEN '".$this->toggleToDataPicker($oStr['work_start_begin'])."' AND '".$this->toggleToDataPicker($oStr['work_start_end'])."' ";
        }

        //emp_active
        if(isset($oStr['emp_active']) && $oStr['emp_active'] == '1'){
            $out = (isset($oStr['emp_out']) && $oStr['emp_out'] == '1') ? ",3":"";
            $whereA .= " AND emp_data.Prosonnal_Being IN (1,2".$out.")  ";
        }
        //emp_out
       
       $resOut['where'] = $whereA;

        return $resOut;
    }

    public function sqlSelect($fields){

        $sql = "";
        $res = [];
        $tbKeep = [];

        $cF     = count($fields);
        $cNow   = 0;

        if(!empty($fields) && $cF > 0){

            $arCache = [];
            foreach($fields as $dbName => $fNames){
                
                foreach($fNames as $fName){
                    $arCache[]  = $dbName.'.'.$fName;
                }
                
                $tbKeep[]   = $dbName;
            }

            $sql .= " ".implode(',',$arCache)." ";

        }

        $res['sql'] = $sql;
        $res['tbKeep'] = $tbKeep;

        return $res;

    }

    public function createJoinSql($tables){

        $sql = "";

        $arrJoin = [
            'emp_data' => [
                'EMP_ADDRESS' => ['INNER JOIN','emp_data.DataNo','=', 'EMP_ADDRESS.ADDR_DATA_NO'],
                'emp_family' => ['LEFT JOIN' ,'emp_data.DataNo' ,'=' ,'emp_family.emp_data_id'],
                'children' => ['LEFT JOIN' ,'emp_data.ID_Card' ,'=' ,'children.emp_id'],
                'tax_income' => ['LEFT JOIN' ,'emp_data.DataNo' ,'=' ,'tax_income.emp_id'],
                'education' => ['LEFT JOIN' ,'emp_data.ID_Card' ,'=' ,'education.emp_id'],
                'work_history' => ['LEFT JOIN' ,'emp_data.ID_Card' ,'=' ,'work_history.emp_id'],
                'emp_money_bonds'  => ['LEFT JOIN' ,'emp_data.DataNo' ,'=' ,'emp_money_bonds.emp_data_id'],
                'emp_promise'  => ['LEFT JOIN' ,'emp_data.DataNo' ,'=' ,'emp_promise.emp_data_id'],
                'EMP_SALARY' => ['LEFT JOIN' ,'emp_data.ID_Card' ,'=' ,'EMP_SALARY.EMP_SALARY_ID_CARD'],
                'SALARY_CHANGE' => ['LEFT JOIN' ,'emp_data.ID_Card' ,'=' ,'SALARY_CHANGE.SALARY_CHANGE_EMP_ID']
            ]
        ];

        foreach($tables as $no => $tbR){

            if(count($arrJoin[$tbR]) > 0){

                foreach($tables as $tbL){

                    if($tbR != $tbL){
                        //echo  "<pre>";print_r($arrJoin[$tbR][$tbL]);echo "</pre>";
                        if(count($arrJoin[$tbR][$tbL]) > 0){
                            
                            //$sql .= " ".$this->fillNameDB($tbR).' '.$arrJoin[$tbR][$tbL][0];
                            $sql .= " ".$arrJoin[$tbR][$tbL][0]." ".$this->fillNameDB($tbL)." ON (".$arrJoin[$tbR][$tbL][1]." ".$arrJoin[$tbR][$tbL][2]." ".$arrJoin[$tbR][$tbL][3]." )";
                            
                        }
                    }
                }
            }
        }


        return $sql;

    }

    public function fillNameDB($tbName){

        $arMatchDB = [
            'emp_data' => 'ERP_easyhr_checktime',
            'emp_family' => 'ERP_easyhr_checktime',
            'tax_income' => 'ERP_easyhr_PAYROLL',
            'EMP_ADDRESS' => 'ERP_easyhr_checktime',
            'children' => 'ERP_easyhr_checktime',
            'education' => 'ERP_easyhr_checktime',
            'work_history' => 'ERP_easyhr_checktime',
            'emp_money_bonds' => 'ERP_easyhr_checktime',
            'emp_promise' => 'ERP_easyhr_checktime',
            'EMP_SALARY'  => 'ERP_easyhr_PAYROLL',
            'SALARY_CHANGE' => 'ERP_easyhr_PAYROLL'
        ];

        return $arMatchDB[$tbName].'.'.$tbName;

    }

    public  function toggleToDataPicker($date){

        $format     = ['-','/'];
        $dateOut    = $date;

        $sp = [];
        $inFormate = false;
        foreach( $format as $no => $f){
            $sp = explode($f,$date);
            if(count($sp) > 1){
                $inFormate = true;
                break;
            }
        }

        if($inFormate){
            if(strlen($sp[0]) == 4){
                $dateOut = $sp[2].'/'.$sp[1].'/'.$sp[0];
            }else{
                $dateOut = $sp[2].'-'.$sp[1].'-'.$sp[0];
            }
        }

        return $dateOut;

    }

    



}
