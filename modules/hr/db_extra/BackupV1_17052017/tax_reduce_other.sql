-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2017 at 10:54 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_reduce_other`
--

CREATE TABLE `tax_reduce_other` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `reduce_name` varchar(200) NOT NULL COMMENT 'ชื่อการลดหย่อน',
  `reduce_amount` decimal(15,2) NOT NULL COMMENT 'จำนวนที่ลดหย่อนได้',
  `reduce_amount_max` decimal(15,2) DEFAULT NULL COMMENT 'จำนวนที่ลดหย่อนได้สูงสุด',
  `for_year` int(11) DEFAULT NULL COMMENT 'ปีที่ได้รับการลดหย่อน',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_reduce_other`
--
ALTER TABLE `tax_reduce_other`
  ADD PRIMARY KEY (`id`),
  ADD KEY `for_year` (`for_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_reduce_other`
--
ALTER TABLE `tax_reduce_other`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
