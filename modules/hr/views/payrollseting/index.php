<?php
/* @var $this yii\web\View */

use app\modules\hr\apihr\ApiHr;


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use app\api\Utility;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

//use app\api\AjaxSubmitButton;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/payrollsetting.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/payroll_config_template.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li>ตั้งค่าข้อมูลองค์กร</li>
                <li class="active">ตั้งค่าข้อมูลการจ่ายเงิน/การหักเงิน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active tabselect" id="tabselect1"><a href="#tab1"
                                                                        data-toggle="tab">ข้อมูลการจ่ายเงิน</a></li>
                        <li class="tabselect" id="tabselect2"><a href="#tab2" data-toggle="tab">ข้อมูลการหักเงิน</a>
                        </li>
                        <li class="tabselect" id="tabselect3"><a href="#tab3" data-toggle="tab">รายการที่จัดการโดยโปรแกรม</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active tabselect" id="tab1">
                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmAdddeduct',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลการจ่ายเงิน</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewAdddeduct',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลการจ่ายเงิน ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                    <form role="form" id="frmAdddeduct">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>ชื่อรายการ <span>*</span></label>
                                                <input id="ADD_DEDUCT_TEMPLATE_NAME" name="ADD_DEDUCT_TEMPLATE_NAME"
                                                       data-required="true"
                                                       class="form-control ADD_DEDUCT_TEMPLATE_NAME" type="text"
                                                       placeholder="ชื่อรายการ">
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control ADD_DEDUCT_TEMPLATE_STATUS"
                                                        id="ADD_DEDUCT_TEMPLATE_STATUS"
                                                        name="ADD_DEDUCT_TEMPLATE_STATUS">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="98">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <label>แสดงในรายงานเงินกู้</label>
                                                <br/>
                                                <input type="checkbox" class="ADD_DEDUCT_TEMPLATE_LOAN_REPORT"
                                                       id="ADD_DEDUCT_TEMPLATE_LOAN_REPORT" value="1"
                                                       name="ADD_DEDUCT_TEMPLATE_LOAN_REPORT">แสดง
                                                <input type="hidden" class="ADD_DEDUCT_TEMPLATE_TYPE"
                                                       name="ADD_DEDUCT_TEMPLATE_TYPE" id="ADD_DEDUCT_TEMPLATE_TYPE"
                                                       value="1">
                                            </div>
                                            <div class="box-body">
                                                <label>ประเภทภาษีบุคคลธรรมดา</label>
                                                <select class="form-control tax_section_id" id="tax_section_id"
                                                        name="tax_section_id">
                                                    <option value="0">เลือกประเภทภาษีบุคคลธรรมดา</option>
                                                    <?php
                                                    $modeltaxSection = ApiHr::gettaxincomesection();
                                                    foreach ($modeltaxSection as $key => $value1) { ?>
                                                        <option value="<?php echo $value1['id']; ?>">
                                                            <?php echo $value1['tax_section_name'] ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <label>รายได้ ภาษีมาตรา 40 </label>
                                                <select class="form-control taxincome_type_id" id="taxincome_type_id"
                                                        name="taxincome_type_id">
                                                    <option value="0">ไม่คิดภาษี</option>
                                                    <?php
                                                    $modeltaxType = ApiHr::gettaxincometype();
                                                    foreach ($modeltaxType as $key => $value2) { ?>
                                                        <option value="<?php echo $value2['id']; ?>">
                                                            <?php echo $value2['taxincome_type'] ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <label>อัตราภาษี (%)</label>
                                                <input id="tax_rate" name="tax_rate"
                                                       class="form-control tax_rate" type="text">
                                            </div>
                                            <div class="box-body">
                                                <label>รหัสทางบัญชี</label><span>*</span>

                                                <input id="accounting_code_pk"
                                                       name="accounting_code_pk"
                                                       data-required="true"
                                                       class="form-control accounting_code_pk "
                                                       type="text"
                                                       placeholder="รหัสทางบัญชี"
                                                >

                                            </div>
                                            <div class="box-body">
                                                <?php echo Html::hiddenInput('hide_activityedit', null, ['id' => 'hide_activityedit', 'class' => 'hide_activityedit']); ?>
                                                <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivity']); ?>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    Pjax::begin(['id' => 'pjax_tb_adddeducttemp']);
                                    echo GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $Adddeducttemplate,
                                        'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                        'columns' => [

                                            [
                                                'class' => 'yii\grid\SerialColumn',
                                                'headerOptions' => ['width' => '20px'],
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_ID',
                                                'label' => 'รหัสรายการ',
                                                'value' => 'ADD_DEDUCT_TEMPLATE_ID',
                                                'contentOptions' => ['style' => 'width:20px']
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_NAME',
                                                'label' => 'ชื่อรายการ',
                                                'value' => 'ADD_DEDUCT_TEMPLATE_NAME',
                                                'contentOptions' => ['style' => 'width:20px']
                                            ],
                                            [
                                                'attribute' => 'tax_section_id',
                                                'label' => 'ประเภทภาษีบุคคลธรรมดา',
                                                //'value' => 'tax_section_id',
                                                'value' => function ($data) {
                                                    $modelResultnamesection = ApiHr::gettaxnameincomesection($data->tax_section_id);
                                                    //print_r($modelResultemp);
                                                    return $modelResultnamesection['0']['tax_section_name'];
                                                },
                                                'contentOptions' => ['style' => 'width:20px'],
                                                'filter' => false,
                                            ],
                                            [
                                                'attribute' => 'taxincome_type_id',
                                                'label' => 'รายได้ ภาษีมาตรา 40',
                                                // 'value' => 'taxincome_type_id',
                                                'value' => function ($data) {
                                                    $modelResultnametype = ApiHr::gettaxnameincometype($data->taxincome_type_id);
                                                    //print_r($modelResultemp);
                                                    if ($modelResultnametype) {
                                                        return $modelResultnametype['0']['taxincome_type'];
                                                    } else {
                                                        return "ไม่คิดภาษี";
                                                    }

                                                },
                                                'contentOptions' => ['style' => 'width:20px'],
                                                'filter' => false,
                                            ],
                                            [
                                                'attribute' => 'tax_rate',
                                                'label' => 'อัตราภาษีที่คิด (%)',
                                                'value' => 'tax_rate',
                                                'contentOptions' => ['style' => 'width:20px'],
                                                'filter' => false,
                                            ],
                                            [
                                                'attribute' => 'accounting_code_pk',
                                                'label' => 'รหัสทางบัญชี',
                                                'value' => 'accounting_code_pk',
                                                'contentOptions' => ['style' => 'width:20px'],
                                                'filter' => false,
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                'label' => 'สถานะการใช้งาน',
                                                //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                'format' => 'image',
                                                'value' => function ($data) {
                                                    return Utility::dispActive($data->ADD_DEDUCT_TEMPLATE_STATUS);
                                                },
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width:5px']
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                'value' => function ($data) {
                                                    $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->ADD_DEDUCT_TEMPLATE_CREATE_BY);
                                                    //print_r($modelResultemp);
                                                    return $modelResultemp['0']['Fullname'];
                                                },
                                                'label' => 'บันทึกโดยผู้ใช้',
                                                //'format' => 'raw',
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_CREATE_DATE',
                                                'label' => 'บันทึกเมื่อวันที่เวลา',
                                                //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_DATE',
                                                'filter' => false,
                                                'value' => function ($data) {
                                                    return DateTime::ThaiDateTime($data->ADD_DEDUCT_TEMPLATE_CREATE_DATE);
                                                },

                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'headerOptions' => ['width' => '100'],
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{update}  &nbsp; {delete}',
                                                'buttons' => [
                                                    'update' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                            'title' => 'แก้ไข',
                                                            'onclick' => '(function($event) {
                                                                        editaddedduct(' . $data->ADD_DEDUCT_TEMPLATE_ID . ',1);
                                                                })();'
                                                        ]);
                                                    },

                                                    'delete' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                            'title' => 'ลบ',
                                                            'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->ADD_DEDUCT_TEMPLATE_NAME . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deleteaddeduct(' . $data->ADD_DEDUCT_TEMPLATE_ID . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                        ]);
                                                    },
                                                ],
                                            ],

                                        ]
                                    ]);
                                    Pjax::end();
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmDeduct',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลการหักเงิน</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewDeduct',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลการหักเงิน ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                    <form role="form" id="frmDeduct">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>ชื่อรายการ <span>*</span></label>
                                                <input id="ADD_DEDUCT_TEMPLATE_NAME" name="ADD_DEDUCT_TEMPLATE_NAME"
                                                       data-required="true"
                                                       class="form-control ADD_DEDUCT_TEMPLATE_NAME" type="text"
                                                       placeholder="ชื่อรายการ">
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control ADD_DEDUCT_TEMPLATE_STATUS"
                                                        id="ADD_DEDUCT_TEMPLATE_STATUS"
                                                        name="ADD_DEDUCT_TEMPLATE_STATUS">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="98">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <label>แสดงในรายงานเงินกู้</label>
                                                <br/>
                                                <input type="checkbox" class="ADD_DEDUCT_TEMPLATE_LOAN_REPORT"
                                                       id="ADD_DEDUCT_TEMPLATE_LOAN_REPORT" value="1"
                                                       name="ADD_DEDUCT_TEMPLATE_LOAN_REPORT">แสดง
                                                <input type="hidden" class="ADD_DEDUCT_TEMPLATE_TYPE"
                                                       name="ADD_DEDUCT_TEMPLATE_TYPE" id="ADD_DEDUCT_TEMPLATE_TYPE"
                                                       value="2">
                                            </div>
                                            <div class="box-body">
                                                <?php echo Html::hiddenInput('hide_activityedit', null, ['id' => 'hide_activityedit', 'class' => 'hide_activityedit']); ?>
                                                <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivityDeduct']); ?>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    Pjax::begin(['id' => 'pjax_tb_deducttemp']);
                                    echo GridView::widget([
                                        'dataProvider' => $dataProviderDeduct,
                                        'filterModel' => $Adddeducttemplate,
                                        'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                        'columns' => [

                                            [
                                                'class' => 'yii\grid\SerialColumn',
                                                'headerOptions' => ['width' => '20px'],
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_ID',
                                                'label' => 'รหัสรายการ',
                                                'value' => 'ADD_DEDUCT_TEMPLATE_ID',
                                                'contentOptions' => ['style' => 'width:20px']
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_NAME',
                                                'label' => 'ชื่อรายการ',
                                                'value' => 'ADD_DEDUCT_TEMPLATE_NAME',
                                                'contentOptions' => ['style' => 'width:20px']
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                'label' => 'สถานะการใช้งาน',
                                                //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                'format' => 'image',
                                                'value' => function ($data) {
                                                    return Utility::dispActive($data->ADD_DEDUCT_TEMPLATE_STATUS);
                                                },
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width:5px']
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                'value' => function ($data) {
                                                    $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->ADD_DEDUCT_TEMPLATE_CREATE_BY);
                                                    //print_r($modelResultemp);
                                                    return $modelResultemp['0']['Fullname'];
                                                },
                                                'label' => 'บันทึกโดยผู้ใช้',
                                                //'format' => 'raw',
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'attribute' => 'ADD_DEDUCT_TEMPLATE_CREATE_DATE',
                                                'label' => 'บันทึกเมื่อวันที่เวลา',
                                                //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_DATE',
                                                'filter' => false,
                                                'value' => function ($data) {
                                                    return DateTime::ThaiDateTime($data->ADD_DEDUCT_TEMPLATE_CREATE_DATE);
                                                },
                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'headerOptions' => ['width' => '100'],
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{update}  &nbsp; {delete}',
                                                'buttons' => [
                                                    'update' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                            'title' => 'แก้ไข',
                                                            'onclick' => '(function($event) {
                                                                        editaddedduct(' . $data->ADD_DEDUCT_TEMPLATE_ID . ',2);
                                                                })();'
                                                        ]);
                                                    },

                                                    'delete' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                            'title' => 'ลบ',
                                                            'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->ADD_DEDUCT_TEMPLATE_NAME . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deleteaddeduct(' . $data->ADD_DEDUCT_TEMPLATE_ID . ',2);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                        ]);
                                                    },
                                                ],
                                            ],

                                        ]
                                    ]);
                                    Pjax::end();
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <?php $form = ActiveForm::begin([
                                'id' => 'payrollconfig-form',
                                'layout' => 'horizontal',
                                'options' => ['class' => 'form-verticle'],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-6',
                                        'offset' => 'col-sm-offset-4',
                                        'wrapper' => 'col-sm-6',
                                        'error' => '',
                                        'hint' => '',
                                    ],
                                ],
                                //'action' => 'saveotconfig',
                                //'enableAjaxValidation' => true,
                                //'validationUrl' => 'validateotconfig',
                            ]);
                            ?>
                            <div class="row">
                                <div class="col-sm-10">
                                    <?php echo $form->field($PayrollConfigTemplate, 'ot_template_id')->dropDownList($arrTemplateID); ?>
                                    <?php echo $form->field($PayrollConfigTemplate, 'sso_template_id')->dropDownList($arrTemplateID); ?>
                                    <?php echo $form->field($PayrollConfigTemplate, 'taxincome_template_id')->dropDownList($arrTemplateID); ?>
                                    <?php echo $form->field($PayrollConfigTemplate, 'taxpnd_template_id')->dropDownList($arrTemplateID); ?>
                                    <?php echo $form->field($PayrollConfigTemplate, 'taxtavi_template_id')->dropDownList($arrTemplateID); ?>
                                    <?php echo $form->field($PayrollConfigTemplate, 'guarantee_template_id')->dropDownList($arrTemplateID); ?>
                                    <?php echo $form->field($PayrollConfigTemplate, 'deposit_bnf_id')->dropDownList($arrTemplateID); ?>
                                    <?php echo $form->field($PayrollConfigTemplate, 'withdraw_bnf_id')->dropDownList($arrTemplateID); ?>
                                </div>
                                <div class="col-sm-2">
                                    &nbsp;
                                </div>

                                <div class="form-group">
                                    <div class="text-center">
                                        <?php

                                        echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'name' => 'btnSavePayrollConfig', 'id' => 'btnSavePayrollConfig']);

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
</section><!-- /.content -->
