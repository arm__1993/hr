<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[LogData]].
 *
 * @see LogData
 */
class LogDataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return LogData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LogData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}