<?php

/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 13/10/2559
 * Time: 14:51
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use app\api\ApiReport;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$script = <<< JS
$(document).ready(function() 
{

    
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

   $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });
  
});

JS;
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานยอดรับแจ้งซ่อม SA รายคน(รายวัน)</h3>


    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>        
        
        <div class="box-body">

                <div class="row">

                <form method="post" action="receive_sa_repair">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key=>$value) {
                                $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                            }
                            ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>ช่วงวันที่</label>
                        &nbsp;
                          <div class="btn-group">                            
                            <div class="input-group" style="width: 250px">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" name="reservation" id="reservation" data-provide="datepicker" data-date-language="th" <?php if(!empty($selected_company)){ echo 'value="'.$selected_months.'"';}?>>
                            </div>
                          </div>
                          &nbsp;&nbsp;                       
                        </center>         

                    <center><br>
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/repair/btn_reportsaday">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                    </center>
                    </div>
                    <!--/col-md-12-->
                </form>
            </div>
            <!--/row-->
        </div>
        <!--/box-body-->

<?php if($query) { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-info">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ชื่อ SA
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        จำนวนคันรับแจ้งซ่อม
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เฉลี่ยต่อวัน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รวมเวลารับแจ้งซ่อม (นาที)
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เวลาเฉลี่ยต่อคัน (นาที)
                                        </th>
                                    </tr>
                                </thead>
                                <?php if(!empty($fullname)) { ?>
                                <tbody>                                    
                                    <?php for($i=0;$i<count($fullname);$i++){ ?>                
                                    <tr role="row">
                                        <td><?php echo($fullname[$i]) ? $fullname[$i] : 'ไม่มีข้อมูล';?></td>
                                        <td><?php echo($sum[$i]) ? $sum[$i] : 0; ?></td>
                                        <td>
                                        <?php echo ($avgdate[$i]) ? Helper::displayDecimal($avgdate[$i]) : 0; ?>
                                        </td>
                                        <td><?php echo($time[$i]) ? $time[$i] : 0; ?></td>
                                        <td>
                                        <?php echo ($avgtime[$i]) ? Helper::displayDecimal($avgtime[$i]) : 0; ?>
                                        </td>                                      
                                    </tr>                                        
                                    <?php } ?>                         
                                </tbody>
                                <?php
                                $sumsum = array_sum($sum);
                                $sumtime = array_sum($time);
                                $sumavgtime = array_sum($avgtime);
                                ?>
                                <tfoot>
                                    <tr>
                                        <th>รวม</th>
                                        <td><?php echo($sumsum) ? $sumsum : 0;?></td>
                                        <td>-</td>
                                        <td><?php echo($sumtime) ? $sumtime : 0;?></td>
                                        <td>
                                        <?php echo($sumavgtime) ? Helper::displayDecimal($sumavgtime) : 0;?>
                                        </td>
                                    </tr>
                                </tfoot>
                                <?php } ?>
                            </table>                                                        
                            </div>
                            <!-- col-sm-12 -->
                        </div>
                        <!-- row -->
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA TABLE -->
            </div>
        </div>        
    </section>
<?php } ?>
</div><!--box-->
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>