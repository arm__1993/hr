<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ot_approved_history".
 *
 * @property integer $id
 * @property integer $ot_requestmaster_id
 * @property integer $ot_requestdetail_id
 * @property string $field_name
 * @property string $old_data
 * @property string $new_data
 * @property string $history_datetime
 */
class OtApprovedHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ot_approved_history';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ot_requestmaster_id', 'ot_requestdetail_id', 'field_name', 'history_datetime'], 'required'],
            [['ot_requestmaster_id', 'ot_requestdetail_id'], 'integer'],
            [['history_datetime'], 'safe'],
            [['field_name', 'old_data', 'new_data'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ot_requestmaster_id' => 'Ot Requestmaster ID',
            'ot_requestdetail_id' => 'Ot Requestdetail ID',
            'field_name' => 'Field Name',
            'old_data' => 'Old Data',
            'new_data' => 'New Data',
            'history_datetime' => 'History Datetime',
        ];
    }
}
