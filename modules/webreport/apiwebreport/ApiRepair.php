<?php

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiRepair
{
    public static function getCvhcMainIdForSearchReportYear($GET_Company,$IDcvhcmain)
    {
        $sqlTimeGroupMountWage ="SELECT id,count(id) AS CountCar,
                               DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d') AS StarttimeSA,
                               DATE_FORMAT(FROM_UNIXTIME(sa_stop_cktime), '%Y-%m-%d') AS EndtimeSA,
                              SUM(TIMESTAMPDIFF(SECOND, DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(FROM_UNIXTIME(sa_stop_cktime), '%Y-%m-%d %H:%i:%s'))) As SumDifftimeTime
                        FROM cvhc_main
                        WHERE brach_id = '$GET_Company'
                        AND cvhc_main.id IN($IDcvhcmain)
                        GROUP BY DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m')";

       return $Time_Group_Mount_Wage = Yii::$app->dbERP_service
            ->createCommand($sqlTimeGroupMountWage)
            ->queryAll();
    }
    public static function getListTimeOfYears($GET_Company,$select_year)
    {
        $sql_listcvhc_main = "SELECT drepair_inform.id AS drepair_inform_id, 
                                cvhc_main.sa_id ,
                               DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d %H:%i:%s') AS StarttimeSA,
                               DATE_FORMAT(FROM_UNIXTIME(sa_stop_cktime), '%Y-%m-%d %H:%i:%s') AS EndtimeSA
                        FROM cvhc_main
                        INNER JOIN drepair_inform
                        ON cvhc_main.id = drepair_inform.cvhc_main_id
                        WHERE brach_id = '$GET_Company' AND cvhc_main.status <> '99'
                        AND  (DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d %H:%i:%s') BETWEEN '$select_year-01-01 00:00:00' 
                              AND  '$select_year-12-31 23:59:59') ";




       return $listcvhc_main = Yii::$app->dbERP_service
            ->createCommand($sql_listcvhc_main)
            ->queryAll();

    }
    public static function getDataSeachByCvhcMain($type_company,$IDcvhcmian)
    {
        $sql_Listtime_Sa_wag ="SELECT cvhc_main.sa_id ,
                               ERP_easyhr_checktime.emp_data.Name,
                               ERP_easyhr_checktime.emp_data.Surname,
                               COUNT(cvhc_main.sa_id) AS Count_Car,
                               DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d') AS StarttimeSA,
                               DATE_FORMAT(FROM_UNIXTIME(sa_stop_cktime), '%Y-%m-%d') AS EndtimeSA,
                              SUM(TIMESTAMPDIFF(SECOND, DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(FROM_UNIXTIME(sa_stop_cktime), '%Y-%m-%d %H:%i:%s'))) As SumDifftime
                        FROM ERP_service.cvhc_main
                              INNER JOIN 
                             ERP_easyhr_checktime.emp_data
                              ON cvhc_main.sa_id = ERP_easyhr_checktime.emp_data.ID_Card
                        WHERE brach_id = '$type_company' AND cvhc_main.status <> 99
                          AND cvhc_main.id IN($IDcvhcmian)
                        GROUP BY cvhc_main.guard_id ";


        return $DataSeachByCvhcMain = Yii::$app->dbERP_service
            ->createCommand($sql_Listtime_Sa_wag)
            ->queryAll();
    }
    public static function getCvhcMainIdForSearch($IDdrepair_inform)
    {
        $sql_SearchCvhcMainId = "SELECT id AS drepair_inform_id,cvhc_main_id
                                      FROM drepair_inform
                                      WHERE id
                                      IN ($IDdrepair_inform)";

        return $idcvhc_main_id = Yii::$app->dbERP_service
            ->createCommand($sql_SearchCvhcMainId)
            ->queryAll();
    }
    public static function getListDrepairinformlist($IN_get_IdDrepair)
    {
        $sql_ListDrepairinformlist = "SELECT drepair_inform_list.drepair_inform_id, 
                                      drepair_inform_list.crepair_id,
                                      crepair.repair_id AS repair_id_header,
                                      drepair_inform_list.wage
                                      FROM drepair_inform_list 
                                      INNER  JOIN crepair 
                                      ON drepair_inform_list.crepair_id = crepair.id
                                      WHERE drepair_inform_id 
                                            IN ($IN_get_IdDrepair)";

        return $List_Drepairinformlist = Yii::$app->dbERP_service
            ->createCommand($sql_ListDrepairinformlist)
            ->queryAll();
    }
    public static function getListtimeSa($type_company,$Str_Startmonth,$Str_Endmonth)
    {
        $sql_Listtime_Sa ="SELECT cvhc_main.id AS a, 
                               cvhc_main.sa_id ,
                               DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d %H:%i:%s') AS StarttimeSA,
                               DATE_FORMAT(FROM_UNIXTIME(sa_stop_cktime), '%Y-%m-%d %H:%i:%s') AS EndtimeSA ,
                               TIMESTAMPDIFF(SECOND, DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(FROM_UNIXTIME(sa_stop_cktime), '%Y-%m-%d %H:%i:%s')) As DifftimeSA,
                               drepair_inform.id AS b
                        FROM cvhc_main 
                        INNER JOIN drepair_inform
                              ON cvhc_main.id = drepair_inform.cvhc_main_id
                        WHERE brach_id = '$type_company' AND cvhc_main.status <> '99'
                            AND  (DATE_FORMAT(FROM_UNIXTIME(sa_str_cktime), '%Y-%m-%d %H:%i:%s') BETWEEN '$Str_Startmonth 00:00:00' 
                            AND  '$Str_Endmonth 23:59:59') ";



       return $listtime_Sa = Yii::$app->dbERP_service
            ->createCommand($sql_Listtime_Sa)
            ->queryAll();
    }
    public static function countDayOfMonthStartStop($year_start,$month_start,$year_stop,$month_stop)
    {

        if($month_start==$month_stop)
        {

            $Lastday_stop = cal_days_in_month(CAL_GREGORIAN, $month_start, $year_start);
            $DateStart = 01;	//วันเริ่มต้น
            $MonthStart = $month_start;	//เดือนเริ่มต้น
            $YearStart = $year_start;	//ปีเริ่มต้น

            $DateEnd = $Lastday_stop;	//วันสิ้นสุด
            $MonthEnd = $month_stop;	//เดือนสิ้นสุด
            $YearEnd = $year_stop;	//ปีสิ้นสุด


        }else if($month_start<$month_stop)
        {
            $Lastday_stop= cal_days_in_month(CAL_GREGORIAN, $month_stop, $year_stop);

            $DateStart = 01;	//วันเริ่มต้น
            $MonthStart = $month_start;	//เดือนเริ่มต้น
            $YearStart = $year_start;	//ปีเริ่มต้น

            $DateEnd = $Lastday_stop;	//วันสิ้นสุด
            $MonthEnd = $month_stop;	//เดือนสิ้นสุด
            $YearEnd = $year_stop;	//ปีสิ้นสุด
        }

        $Start = mktime(0,0,0,$MonthStart,01,$YearStart);
        $End = mktime(23,59,59,$MonthEnd,$DateEnd,$YearEnd);

       return $DateNum=ceil(($End -$Start)/86400);
    }
    public static  function getFormatTime($H,$m,$s)
    {
        if(empty($H)){
            $H = "00";
        }elseif(strlen($H) ==1) {
            $H = str_pad($H,2,"0",STR_PAD_LEFT);
        }else{
            $H = $H;
        }

        if(empty($m)){
            $m = "00";
        }elseif(strlen($m) ==1) {
            $m = str_pad($m,2,"0",STR_PAD_LEFT);
        }else{
            $m = $m;
        }
        if(empty($s)){
            $s = "00";
        }elseif(strlen($s) ==1) {
            $s = str_pad($s,2,"0",STR_PAD_LEFT);
        }else{
            $s = $s;
        }

        return $H.":".$m.":".$s;


    }
    public static  function avgTimePerCar($Time,$Countcar)
    {
        if(!empty($Countcar))
        {
            $Countcar = $Countcar;
        }else
        {
            $Countcar = 1;
        }

        return $Time/$Countcar;


    }
    public static  function getDateStartToEnd($year,$m_start,$m_stop)
    {

        $unixtimeStart = mktime(0,0,0,$m_start,1,$year);
        $startDate = date('Y-m-d',$unixtimeStart);
        $unixtimeEnd = mktime(23,59,59,$m_stop,1,$year);;
        $dEnd = date('t',$unixtimeEnd);
        $endDate= "$year-$m_stop-$dEnd";
        return[
            'startDate'=>$startDate,
            'endDate' =>$endDate,
        ];

    }

	public static function getNameUser($str_start,$str_end)
	{
            /* ==================> หาเวลา print_r($_arrRET['total_workday']);*/ 
        //$sql_Sa = id sa, id_derepair     
        $sql_Sa = "SELECT DISTINCT(a.id) as id_derepair, id_card, b.NameUse,cvhc_main_id 
        FROM drepair_inform as a inner join ERP_easyhr_OU.relation_position as b on a.sa_id = b.id_card 
        where a.status != 99 and a.start_time Between '$str_start' and '$str_end' and b.status = 1"; 
        $ID_SaDerepair = Yii::$app->dbERP_service->createCommand($sql_Sa)->queryAll(); 


    return  $ID_SaDerepair;
	}

    public static function getTime()
    {
        //$sql_time หาเวลา 
        $sql_time = "SELECT drepair_inform_id as id_derepair ,sum(time) 
        FROM drepair_inform_list where status !=99 group by drepair_inform_id"; 
        $Time = Yii::$app->dbERP_service->createCommand($sql_time)->queryAll(); 

    return $Time ;
    }

    public static function getIdCompany($type_company)
    {
        //$sql_company หาบริษัท 
        $sql_company = "SELECT id as cvhc_main_id 
        FROM cvhc_main where brach_id = '$type_company' group by ahistory_id"; 
        $ID_Company = Yii::$app->dbERP_service->createCommand($sql_company)->queryAll(); 

    return $ID_Company;
    }

    public static function getIdSaDerepair($str_StarDate,$str_EndDate,$type_company)
    {
        $sql_Sa = "select DISTINCT(a.id) as id_derepair,b.NameUse,id_card,cvhc_main_id ,a.start_time as month 
        FROM drepair_inform as a inner join ERP_easyhr_OU.relation_position as b on a.sa_id = b.id_card 
        inner join ERP_easyhr_OU.position as c on b.position_id = c.id where a.status != 99 
        and a.start_time BETWEEN '$str_StarDate' and '$str_EndDate '
        and b.status = 1 and c.WorkCompany = $type_company"; 
        $ID_SaDerepair = Yii::$app->dbERP_service->createCommand($sql_Sa)->queryAll(); 


    return  $ID_SaDerepair;
    }

    public static function getIDSaDerepairActionMonth($str_year)
    {
        /* ==================> หาเวลา print_r($_arrRET['total_workday']);*/ 

        //$sql_SA = id sa, id_derepair 
        $sql_Sa = "SELECT DISTINCT(a.id) as id_derepair,id_card,b.NameUse,cvhc_main_id 
        FROM drepair_inform as a inner join ERP_easyhr_OU.relation_position as b on a.sa_id = b.id_card where a.status != 99 
        and DATE_FORMAT(FROM_UNIXTIME(a.start_time),'%Y-%m-%d') 
        LIKE DATE_FORMAT(FROM_UNIXTIME(a.start_time),$str_year) and b.status = 1"; 
        $ID_SaDerepair = Yii::$app->dbERP_service->createCommand($sql_Sa)->queryAll(); 


    return $ID_SaDerepair;
    }
    public static function getSumTime()
    {
        //$sql_time หาเวลา 
        $sql_time = "SELECT drepair_inform_id as id_derepair ,sum(time) 
        FROM drepair_inform_list where status !=99 group by drepair_inform_id"; 
        $Time = Yii::$app->dbERP_service->createCommand($sql_time)->queryAll(); 
     $sumTime = [];   
        foreach ($Time as $value) { 
        $sumTime[$value['id_derepair']][] = $value['sum(time)']; 
        } 



    return $sumTime;
    }


}