<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Firstname;
use yii\helpers\ArrayHelper;



class BefirstnameController extends ActiveController
{
 
    public $modelClass = 'app\modules\wscommon\models\Firstname';


    public function behaviors()
    {
        return
            ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                ],
            ],
        ],
        parent::behaviors());
    }

    public function actionSearchbename()
    {   
        //return 1;

        $firstname = $_GET['firstname'];

        //return $firstname;
 
        $bename = new Firstname();
        $benameSearch = $bename->searchbename($firstname);
        return $benameSearch;
    }
    

}
