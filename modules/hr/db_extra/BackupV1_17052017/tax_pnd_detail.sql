-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 17, 2017 at 12:12 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_pnd_detail`
--

CREATE TABLE `tax_pnd_detail` (
  `id` int(11) NOT NULL COMMENT 'รหัส',
  `tax_pnd_head_id` int(11) NOT NULL COMMENT 'รหัสตารางหัว tax_pnd_head',
  `emp_no` int(11) NOT NULL COMMENT 'ลำดับการจ่ายภาษีจะปรากฏใน ทวิ 50 ด้วย',
  `emp_idcard` char(13) NOT NULL COMMENT 'รหัสบัตรประชาชนพนักงานจ่ายภาษี',
  `emp_firstname` varchar(100) NOT NULL COMMENT 'ชื่อพนักงาน',
  `emp_lastname` varchar(100) NOT NULL COMMENT 'นามสกุล',
  `emp_address` varchar(500) NOT NULL COMMENT 'ที่อยู่ มีเฉพาะใน ภงด 1ก',
  `paid_date` date NOT NULL COMMENT 'วันที่จ่ายเงิน',
  `paid_amount` decimal(15,2) NOT NULL COMMENT 'จำนวนที่จ่ายในครั้งนี้',
  `paid_tax` decimal(15,2) NOT NULL COMMENT 'จำนวนภาษีที่หักนำส่งครั้งนี้',
  `createby_user` char(13) NOT NULL COMMENT 'เลขประจำตัวผู้สร้าง',
  `create_datetime` datetime NOT NULL COMMENT 'สร้างรายการเมื่อวันที่เวลา',
  `income_gnr` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax_gnr` decimal(15,2) NOT NULL DEFAULT '0.00',
  `income_auth` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax_auth` decimal(15,2) NOT NULL DEFAULT '0.00',
  `income_onepaid` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax_onepaid` decimal(15,2) NOT NULL DEFAULT '0.00',
  `income_inthai` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax_inthai` decimal(15,2) NOT NULL DEFAULT '0.00',
  `income_notin` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax_notin` decimal(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_pnd_detail`
--
ALTER TABLE `tax_pnd_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_pnd_detail`
--
ALTER TABLE `tax_pnd_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
