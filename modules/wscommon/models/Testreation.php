<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "TestReation".
 *
 * @property integer $id
 * @property integer $TestMainCus_id
 * @property string $tes
 */
class Testreation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TestReation';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TestMainCus_id', 'tes'], 'required'],
            [['TestMainCus_id'], 'integer'],
            [['tes'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'TestMainCus_id' => 'Test Main Cus ID',
            'tes' => 'Tes',
        ];
    }
    public function Insertreationship($Lastid)
    {
 
        $sql = "insert into TestReation  VALUES (NULL,$Lastid,'xxxxxx')";
         $datareation = Yii::$app->dbERP_maincusdata
       ->createCommand($sql)->execute();
 
        return $datareation;
        
    }

}
