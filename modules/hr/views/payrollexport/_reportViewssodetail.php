<?php
use yii\helpers\Html;
use app\api\DateTime;
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$data = $data;
$datacompany= $company;
$item = $dataH;
?>

<!--<meta charset="UTF-8">-->

  <table width="100%" border='1' style="border-collapse: collapse; border: 1px solid #ccc" class='b4' cellpadding='5' cellspacing='0'>
    <tr>
        <td rowspan="2" width="10%">
            <img height="63" width="63" src="<?php echo $imghr; ?>/sps.png" class="img-circle">
        </td >
        <td colspan='3'>
        </td>
        <td colspan='2'>
            สปส.1-10 ( ส่วนที่ 2 )	
        </td>
    </tr>
    <tr>
        <td colspan='5'>
                <b>รายละเอียดการนำส่งเงินสมทบ</b>
        </td>
    </tr>
    <tr>
            <td colspan='4' >
                สำหรับค่าจ้างเดือน     &nbsp;&nbsp;    <?php echo DateTime::mappingMonth($item[0]['MONTHS']); ?>  &nbsp;    พ.ศ.        <?php echo $item[0]['YEARS']+543 ?>	
            </td>

            <td>
                เลขที่บัญชี
            </td>

            <td >
               <?php echo $datacompany[0]['soc_acc_number']; ?>	
            </td>
    </tr>
    <tr>
            <td colspan='4' >
               <?php echo $datacompany[0]['name']; ?>	                
            </td>

            <td>
                สาขา
            </td>

            <td >
                <?php echo '0000'.$datacompany[0]['id']; ?>		
            </td>
    </tr>
    <tr>
            <td>
                ลำดับที่
            </td>

            <td>
                เลขประจำตัวประชาชน
            </td>

            <td>
               คำนำหน้า
            </td>

            <td >
                ชื่อ - สกุล
            </td>
            <td style="text-align: right;">
                เงินเดือน
            </td>
            <td style="text-align: right;">
                เงินสมทบ
            </td>
    </tr>
    <?php
        for($i=1;$i<=count($data);$i++){
            if($data[$i]['PREFIX']=='003'){
                $sex = 'นาย';
            }
            else if($data[$i]['PREFIX']=='004'){
                $sex = 'นางสาว';
            }
            else if($data[$i]['PREFIX']=='005'){
                $sex = 'นาง';
            }


            $datatable = '<tr class="classname">';
            $datatable .= '<td>'.$i.'</td>';
            $datatable .= '<td>'.$data[$i]['SSO_ID'].'</td>';
            $datatable .= '<td><center>'.$sex.'</center></td>';
            $datatable .= '<td >'.$data[$i]['FNAME'].'-'.$data[$i]['LNAME'].'</td>';
            $datatable .= '<td style="text-align: right;">'.number_format((int)substr($data[$i]['WAGES'],0,-2).'.'.substr($data[$i]['WAGES'],-2),2).'</td>';
            $datatable .= '<td style="text-align: right;">'.number_format((int)substr($data[$i]['PAID_AMOUNT'],0,-2).'.'.substr($data[$i]['PAID_AMOUNT'],-2),2).'</td>';
            $datatable .= '</tr>';
            echo $datatable;
        }
        
    ?>
    
  </table>