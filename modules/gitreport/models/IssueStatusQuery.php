<?php

namespace app\modules\gitreport\models;

/**
 * This is the ActiveQuery class for [[IssueStatus]].
 *
 * @see IssueStatus
 */
class IssueStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IssueStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IssueStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
