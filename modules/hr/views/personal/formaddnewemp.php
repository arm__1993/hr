<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;

use app\bundle\AppAsset;

// API กลาง
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;
// API HR

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\controllers\PersonalController;
use app\modules\hr\apihr\ApiPersonal;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/formaddnewemp.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/formEmpData.css?t=" . time(), ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tinymce/js/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/personalsalary/position.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/default/croppie.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/default/croppie.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);


$script = <<< JS
$(document).ready(function() {
    
    
    setTimeout(function() {
        $('#message').fadeOut('slow');
    }, 3000);


$("#example2").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });

$("#tablelistdata").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });

  var txtDate = document.getElementById("birthdayEmp");
  selectBirthdayEmp(txtDate);
  
});
JS;

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");


$this->registerJs($script);
?>
<section class="content">
    <div class="box box-default">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-user "></i>
                    <a href="#">ข้อมูลพนักงาน </a>
                </li>
                <li class="active">
                    <a href="#"><?php echo $data['title_curent']; ?></a>
                </li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1" onclick="selecttab(1)" data-toggle="tab">
                                    ข้อมูลทั่วไป
                                </a>
                            </li>
                            <li>
                                <a href="#tab_2" onclick="selecttab(2)" data-toggle="tab">
                                    ที่อยู่
                                </a>
                            </li>
                            <li>
                                <a href="#tab_3" onclick="selecttab(3)" data-toggle="tab">
                                    ครอบครัว
                                </a>
                            </li>
                            <li>
                                <a href="#tab_4" onclick="selecttab(4)" data-toggle="tab">
                                    ลดหย่อนภาษี
                                </a>
                            </li>
                            <li>
                                <a href="#tab_5" onclick="selecttab(5)" data-toggle="tab">
                                    การศึกษา
                                </a>
                            </li>
                            <li>
                                <a href="#tab_6" onclick="selecttab(6)" data-toggle="tab">
                                    ลงเวลาเข้า-ออกงาน
                                </a>
                            </li>
                            <li>
                                <a href="#tab_7" onclick="loadBondsman(this)" data-have-contend="no" data-toggle="tab">
                                    ข้อมูลเงินค้ำประกัน
                                </a>
                            </li>
                            <li>
                                <a href="#tab_8" id="title_tab8" onclick="loadEmpPromise(this)" data-have-contend="no"
                                   data-toggle="tab">
                                    ข้อมูลสัญญา
                                </a>
                            </li>
                            <li>
                                <a href="#tab_9" id="title_tab9" onclick="loadEmpWorkhistory(this)"
                                   data-have-contend="no" data-toggle="tab">
                                    ประวัติการทำงาน
                                </a>
                            </li>
                            <li>
                                <!--<a href="#tab_10" id="title_tab10" onclick="loadPersonalsarary(this)" data-have-contend="no" data-toggle="tab">-->
                                <a href="#tab_10" id="title_tab10" onclick="loadEmpPosition(this)"
                                   data-have-contend="no" data-toggle="tab">
                                    ตำแหน่งเงินเดือน
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content form-emp">

                            <input type="hidden" value="<?php echo $empModel['ID_Card']; ?>" id="id_card_ref"/>
                            <input type="hidden" value="<?php echo $empModel['DataNo']; ?>" id="emp_id_ref"/>
                            <input type="hidden" value="<?php echo ($optional['viewOnly']) ? 1 : 0; ?>" id="viewOnly"/>

                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- block รูป -->
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"></h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="col-sm-8">
                                                        <div class="form-group <?php echo $optional['displayNone']; ?>">
                                                            <label for="fileEmp" class="col-sm-4 control-label">รูปพนักงาน</label>

                                                            <div class="col-sm-8">
                                                                <input type="file" name="fileEmp" class="form-control"
                                                                       id="fileEmp">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmp" class="col-sm-4 control-label">รหัสพนักงาน</label>

                                                            <div class="col-sm-8">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>"
                                                                       disabled="disabled"
                                                                       id="Code"
                                                                       name="Code"
                                                                       value="<?php echo $empModel['Code']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmpMain" class="col-sm-4 control-label">รหัสพนักงานขายตรีเพชร</label>

                                                            <div class="col-sm-8">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="TIS_id" name="TIS_id"
                                                                       value="<?php echo $empModel['TIS_id']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="img-holder" class="col-sm-4"
                                                         style="border:1px solid #3c8dbc;height: 160px">
                                                        <img src="<?php $picpath = ($empModel['Pictures_HyperL'] == '') ? 'avatar2.png' : $empModel['Pictures_HyperL'];
                                                        echo Yii::$app->request->baseUrl . '/upload/personal/' . $picpath; ?>"
                                                             alt="image img" style="height:158px;">
                                                    </div>

                                                    <div class="col-md-4 text-center">
                                                        <div id="upload-demo" style="width:350px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <!-- block ข้อมูลส่วนตัว -->
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลส่วนตัว</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">เชื้อชาติ</label>

                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="race" name="race"
                                                                   value="<?php echo $empModel['race']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Nationality"
                                                               class="col-sm-4 control-label">สัญชาติ</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="Nationality" name="Nationality"
                                                                   value="<?php echo $empModel['Nationality']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Religion"
                                                               class="col-sm-4 control-label">ศาสนา</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="Religion" name="Religion"
                                                                   value="<?php echo $empModel['Religion']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Weight" class="col-sm-4 control-label">น้ำหนัก
                                                            (Kg.)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="Weight" name="Weight"
                                                                   value="<?php echo $empModel['Weight']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Height" class="col-sm-4 control-label">ส่วนสูง
                                                            (Cm.)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="Height" name="Height"
                                                                   value="<?php echo $empModel['Height']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Blood_G"
                                                               class="col-sm-4 control-label">กลุ่มเลือด</label>
                                                        <div class="col-sm-6">
                                                            <?php
                                                            if ($mode == 'view') echo $empModel['Blood_G'];
                                                            else {
                                                                $cssClass = 'form-control ' . $optional['dontouch'];
                                                                echo Html::dropDownList('Blood_G', $empModel['Blood_G'], \app\modules\hr\apihr\ApiPersonal::bloodGroup(), [
                                                                    'id' => 'Blood_G',
                                                                    'prompt' => 'กลุ่มเลือด',
                                                                    'class' => $cssClass,
                                                                    'readonly' => $optional['readonly'],

                                                                ]);
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="disease" class="col-sm-4 control-label"
                                                               id="diseaseStatus">โรคประจำตัว</label>
                                                        <div class="col-sm-6">
                                                            <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <input type="radio"
                                                                       name="disease" <?php echo ($empModel['disease'] == 1) ? 'checked' : ''; ?>
                                                                       value="1">
                                                                มี
                                                                <input type="radio"
                                                                       name="disease" <?php echo ($empModel['disease'] == 0) ? 'checked' : ''; ?>
                                                                       value="0">
                                                                ไม่มี
                                                            </div>
                                                            <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : '' ?>"
                                                                 style="padding-top: 7px;padding-left: 10px;">
                                                                <?php echo ($empModel['disease'] == 1) ? 'มี' : 'ไม่มี'; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="main"
                                                               class="col-sm-4 control-label">ระบุโรคประจำตัว</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="diseaseDetail" name="diseaseDetail"
                                                                   value="<?php echo $empModel['diseaseDetail']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>


                                    </div>
                                    <div class="col-md-6">
                                        <!-- block ข้อมูลบัตรประชาชน -->
                                        <form id="id_card_form" class="form-horizontal" onsubmit="return false;"
                                              role="form" data-toggle="validator">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลพนักงาน</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="Sex" class="col-sm-4 control-label">เพศ<font
                                                                    color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <input type="radio" name="Sex"
                                                                       onclick="getGenderEmp('1')" <?php echo ($empModel['Sex'] == 'ชาย') ? 'checked' : '' ?>
                                                                       value="ชาย" required="true">
                                                                ชาย
                                                                <input type="radio" name="Sex"
                                                                       onclick="getGenderEmp('2')" <?php echo ($empModel['Sex'] == 'หญิง') ? 'checked' : '' ?>
                                                                       value="หญิง" required="true">
                                                                หญิง
                                                            </div>
                                                            <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : '' ?>"
                                                                 style="padding-top: 7px;padding-left: 10px;">
                                                                <?php echo $empModel['Sex']; ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">คำนำหน้า</label>
                                                        <div class="col-sm-6">
                                                            <?php if (!$optional['viewOnly']) { ?>
                                                                <select class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?>
                                                                        name="Be" id="selectBenameTH">
                                                                    <option value="">เลือกคำนำหน้า</option>
                                                                    <?php echo $optional['option_bename_th']; ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <label class="control-label"
                                                                       style="font-weight:normal;padding-left: 12px">
                                                                    <?php echo ($empModel['Be'] == '') ? "ไม่ระบุ" : $empModel['Be']; ?>
                                                                </label>
                                                            <?php } ?>

                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Name" class="col-sm-4 control-label">ชื่อ<font
                                                                    color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="nameEmpTH" name="Name"
                                                                   value="<?php echo $empModel['Name']; ?>" size="13"
                                                                   data-required="true" required="true">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Surname" class="col-sm-4 control-label">นามสกุล<font
                                                                    color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="lastNameEmpTH" name="Surname"
                                                                   value="<?php echo $empModel['Surname']; ?>" size="13"
                                                                   data-required="true" required="true">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nickNameEmpTH" class="col-sm-4 control-label">ชื่อเล่น</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="nickNameEmpTH" name="Nickname"
                                                                   value="<?php echo $empModel['Nickname']; ?>"
                                                                   size="13">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">คำนำหน้า[Eng]</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?>
                                                                    name="BenameEN" id="selectBenameEN">
                                                                <option value="">เลือกคำนำหน้า</option>
                                                                <?php echo $optional['option_bename_en']; ?>
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nameEmpEN"
                                                               class="col-sm-4 control-label">ชื่อ[Eng]<font
                                                                    color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="nameEmpEN" name="nameEmpEN"
                                                                   value="<?php echo $empModel['nameEmpEN']; ?>"
                                                                   size="12" data-required="true" required="true">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="lastNameEmpEN" class="col-sm-4 control-label">นามสกุล[Eng]<font
                                                                    color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="lastNameEmpEN" name="lastNameEmpEN"
                                                                   value="<?php echo $empModel['lastNameEmpEN']; ?>"
                                                                   size="12" data-required="true" required="true">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nickNameEmpEN" class="col-sm-4 control-label">ชื่อเล่น[Eng]</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="nickNameEmpEN" name="nickNameEmpEN"
                                                                   value="<?php echo $empModel['nickNameEmpEN']; ?>"
                                                                   size="12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="username" class="col-sm-4 control-label">ชื่อล็อกอิน<font
                                                                    color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="nameEmpLogin" name="username"
                                                                   value="<?php echo $empModel['username']; ?>"
                                                                   size="12" data-required="true" required="true"
                                                                   style="background-color: #85ECEF !important; font-weight: bold;">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password" class="col-sm-4 control-label">รหัสผ่าน
                                                            <font color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="passEmp" value="" name="password" size="12"
                                                                   value=" "
                                                                   style="background-color: #85ECEF !important;">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="birthday" class="col-sm-4 control-label">วัน เดือน
                                                            ปีเกิด</label>

                                                        <div class="col-sm-6 ">
                                                            <?php if (!$optional['viewOnly']) { ?>
                                                                <div class="input-group date">
                                                                    <input type="text"
                                                                           readonly="readonly"
                                                                           class="form-control <?php echo $optional['dontouch']; ?>"
                                                                           id="birthdayEmp"
                                                                           name="Birthday"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($empModel['Birthday']) ?>"
                                                                           placeholder="dd/mm/yyyy"
                                                                           onchange="selectBirthdayEmp(this)"/>

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <input type="hidden"
                                                                       id="birthdayEmp"
                                                                       value="<?php echo PersonalController::toggleToDataPicker($empModel['Birthday']) ?>"
                                                                />
                                                                <?php echo PersonalController::toggleToDataPicker($empModel['Birthday']) ?>
                                                            <?php } ?>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="age" class="col-sm-4 control-label">อายุ</label>
                                                        <div class="col-sm-2">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="ageEmp" name="age" size="2" disabled>
                                                        </div>
                                                        <label class="col-sm-1 control-label">ปี</label>
                                                    </div>

                                                </div>
                                            </div>
                                        </form>

                                        <!-- block ข้อมูลบัตรประชาชน(id card) -->
                                        <form class="form-horizontal" id="form_id_card_info" onsubmit="return false;"
                                              data-toggle="validator">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลบัตรประชาชน</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="ID_Card" class="col-sm-4 control-label">หมายเลขบัตรประชาชน<font
                                                                    color="red">*</font></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   id="ID_Card"
                                                                   name="ID_Card"
                                                                   value="<?php echo $empModel['ID_Card']; ?>"
                                                                   data-required="true" required="true"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="districtRelease" class="col-sm-4 control-label">ออกให้โดย
                                                            (เขต/อำเภอ)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   name="districtRelease" id="districtRelease"
                                                                   value="<?php echo $empModel['districtRelease']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="DateOfIssueIdcrad" class="col-sm-4 control-label">วันที่ออกบัตร</label>
                                                        <div class="col-sm-6">

                                                            <?php if (!$optional['viewOnly']) { ?>
                                                                <div class="input-group date">
                                                                    <input type="text" readonly="readonly"
                                                                           class="form-control"
                                                                           id="DateOfIssueIdcrad"
                                                                           name="DateOfIssueIdcrad"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($empModel['DateOfIssueIdcrad']) ?>"
                                                                           placeholder="dd/mm/yyyy"/>

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <?php echo PersonalController::toggleToDataPicker($empModel['DateOfIssueIdcrad']) ?>
                                                            <?php } ?>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="DateOfExpiryIdcrad" class="col-sm-4 control-label">วันที่บัตรหมดอายุ</label>
                                                        <div class="col-sm-6">

                                                            <?php if (!$optional['viewOnly']) { ?>
                                                                <div class="input-group date">
                                                                    <input type="text" readonly="readonly"
                                                                           class="form-control"
                                                                           id="DateOfExpiryIdcrad"
                                                                           name="DateOfExpiryIdcrad"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($empModel['DateOfExpiryIdcrad']) ?>"
                                                                           placeholder="dd/mm/yyyy"/>

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <?php echo PersonalController::toggleToDataPicker($empModel['DateOfExpiryIdcrad']) ?>
                                                            <?php } ?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>


                                        <!-- block ข้อมูลหนังสือเดินทาง -->
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลหนังสือเดินทาง</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="numberPassportEmp" class="col-sm-4 control-label">เลขที่</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   name="numberPassportEmp" id="numberPassportEmp"
                                                                   value="<?php echo $empModel['numberPassportEmp']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="DateOfIssuePassport" class="col-sm-4 control-label">วันที่ออกหนังสือ</label>
                                                        <div class="col-sm-6">

                                                            <?php if (!$optional['viewOnly']) { ?>
                                                                <div class="input-group date">
                                                                    <input type="text" readonly="readonly"
                                                                           class="form-control"
                                                                           id="DateOfIssuePassport"
                                                                           name="DateOfIssuePassport"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($empModel['DateOfIssuePassport']) ?>"
                                                                           placeholder="dd/mm/yyyy"/>

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <?php echo PersonalController::toggleToDataPicker($empModel['DateOfIssuePassport']) ?>
                                                            <?php } ?>


                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="DateOfExpiryPassport"
                                                               class="col-sm-4 control-label">วันที่หมดอายุ</label>
                                                        <div class="col-sm-6">

                                                            <?php if (!$optional['viewOnly']) { ?>
                                                                <div class="input-group date">
                                                                    <input type="text" readonly="readonly"
                                                                           class="form-control"
                                                                           id="DateOfExpiryPassport"
                                                                           name="DateOfExpiryPassport"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($empModel['DateOfExpiryPassport']) ?>"
                                                                           placeholder="dd/mm/yyyy"/>

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <?php echo PersonalController::toggleToDataPicker($empModel['DateOfExpiryPassport']) ?>
                                                            <?php } ?>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane " id="tab_2">
                                <div class="row">

                                    <!--ที่อยู่ตามทะเบียนบ้าน-->
                                    <div id="home_addr" class="col-md-6 outlet-addr-form">
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">ที่อยู่ตามทะเบียนบ้าน</h3>
                                            </div>
                                            <div class="box-body">

                                                <div class="form-group">
                                                    <label for="ADDR_NUMBER"
                                                           class="col-sm-2 control-label">บ้านเลขที่</label>
                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                               name="ADDR_NUMBER"
                                                               value="<?php echo $addrInf['home_addr']['ADDR_NUMBER']; ?>"
                                                               class="form-control ADDR_NUMBER <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="ADDR_NUMBER" size="3">
                                                    </div>
                                                    <label for="ADDR_GROUP_NO"
                                                           class="col-sm-2 control-label">หมู่</label>

                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                               name="ADDR_GROUP_NO"
                                                               value="<?php echo $addrInf['home_addr']['ADDR_GROUP_NO']; ?>"
                                                               class="form-control ADDR_GROUP_NO <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="ADDR_GROUP_NO" size="3">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">หมู่บ้าน</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_VILLAGE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['home_addr']['ADDR_VILLAGE']; ?>"
                                                               id="ADDR_VILLAGE"
                                                               name="ADDR_VILLAGE">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">ตรอก/ซอย</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_LANE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['home_addr']['ADDR_LANE']; ?>"
                                                               id="ADDR_LANE"
                                                               name="ADDR_LANE">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">ถนน</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_ROAD <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['home_addr']['ADDR_ROAD']; ?>"
                                                               id="ADDR_ROAD"
                                                               name="ADDR_ROAD">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">ตำบล</label>
                                                    <div class="col-sm-6">
                                                        <!--<select class="home_addr-select2-ajax form-control"  id="selTumbon" style="width: 100% !important;"></select>-->
                                                        <input type="text" id="ADDR_SUB_DISTRICT"
                                                               name="ADDR_SUB_DISTRICT"
                                                               class="form-control ADDR_SUB_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['home_addr']['ADDR_SUB_DISTRICT']; ?>"
                                                               readonly="readonly">
                                                    </div>
                                                    <span class="input-group-btn">
                                                            <button class="btn btn-default" id="btnTHomeAddr"
                                                                    data-title="ค้นหาตำบล" type="button"
                                                                    data-placement="bottom" data-toggle="popover"
                                                                    data-container="body" data-html="true"><i
                                                                        class="fa fa-search"></i> </button>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">อำเภอ</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['home_addr']['ADDR_DISTRICT']; ?>"
                                                               id="ADDR_DISTRICT"
                                                               name="ADDR_DISTRICT" readonly="readonly">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">จังหวัด</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_PROVINCE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['home_addr']['ADDR_PROVINCE']; ?>"
                                                               id="ADDR_PROVINCE"
                                                               name="ADDR_PROVINCE" readonly="readonly">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">รหัสไปรษณีย์</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_POSTCODE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['home_addr']['ADDR_POSTCODE']; ?>"
                                                               id="ADDR_POSTCODE"
                                                               name="ADDR_POSTCODE"
                                                               maxlength="5" onkeypress="return onlyNumber(event);"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--ที่อยู่ตามบัตรประชาชน-->
                                    <div id="personal_card_addr" class="col-md-6 border-form-add outlet-addr-form">
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">ที่อยู่ตามบัตรประชาชน</h3>
                                            </div>
                                            <div class="box-body">

                                                <div class="form-group"
                                                     style="<?php echo ($optional['viewOnly']) ? 'display:none' : ''; ?>">
                                                    <input type="checkbox"
                                                        <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'checked' : ''; ?>
                                                           id="isCopyRestAddr"
                                                           name="ADDR_REF_MAIN"
                                                           value="1"
                                                           class="isCheckCopy">
                                                    เหมือนที่อยู่ตามทะเบียนบ้าน
                                                </div>
                                                <div class="form-group">
                                                    <label for="ADDR_NUMBER"
                                                           class="col-sm-2 control-label">บ้านเลขที่</label>
                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                               name="ADDR_NUMBER"
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_NUMBER']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               class="form-control ADDR_NUMBER <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="ADDR_NUMBER"
                                                               size="3">
                                                    </div>
                                                    <label for="ADDR_GROUP_NO"
                                                           class="col-sm-2 control-label">หมู่</label>

                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                               name="ADDR_GROUP_NO"
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_GROUP_NO']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               class="form-control ADDR_GROUP_NO <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="ADDR_GROUP_NO"
                                                               size="3">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">หมู่บ้าน</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_VILLAGE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_VILLAGE']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               id="ADDR_VILLAGE" name="ADDR_VILLAGE">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">ตรอก/ซอย</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_LANE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="ADDR_LANE"
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_LANE']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               name="ADDR_LANE">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">ถนน</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_ROAD <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="ADDR_ROAD"
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_ROAD']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               name="ADDR_ROAD">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">ตำบล</label>
                                                    <div class="col-sm-6">
                                                        <!--<select class="home_addr-select2-ajax form-control"  id="selTumbon" style="width: 100% !important;"></select>-->
                                                        <input type="text" id="ADDR_SUB_DISTRICT"
                                                               name="ADDR_SUB_DISTRICT"
                                                               class="form-control ADDR_SUB_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_SUB_DISTRICT']; ?>"
                                                               readonly="readonly">
                                                    </div>
                                                    <span class="input-group-btn">
                                                                <button class="btn btn-default" id="btnTIDcardAddr"
                                                                        data-title="ค้นหาตำบล" type="button"
                                                                        data-placement="bottom" data-toggle="popover"
                                                                        data-container="body" data-html="true"><i
                                                                            class="fa fa-search"></i> </button>
                                                        </span>
                                                </div>

                                                <!--
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">ตำบล</label>
                                                    <div class="col-sm-6">
                                                        <select class="personal_card_addr-select2-ajax form-control"  style="width: 100% !important;"></select>
                                                        <input type="hidden" id="ADDR_SUB_DISTRICT" name="ADDR_SUB_DISTRICT"
                                                               class="form-control ADDR_SUB_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_SUB_DISTRICT']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>>
                                                    </div>
                                                </div>
                                                  -->

                                                <!--
                                                <div class="form-group">
                                                  <label for="" class="col-sm-3 control-label">ตำบล</label>
                                                  <div class="col-sm-6">
                                                    <input type="text"
                                                        class="form-control ADDR_SUB_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                        value="<?php echo $addrInf['personal_card_addr']['ADDR_SUB_DISTRICT']; ?>"
                                                        <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                        id="ADDR_SUB_DISTRICT"
                                                        name="ADDR_SUB_DISTRICT" >
                                                  </div>
                                                </div>
                                            -->

                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">อำเภอ</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_DISTRICT']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               id="ADDR_DISTRICT"
                                                               name="ADDR_DISTRICT" readonly="readonly">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">จังหวัด</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_PROVINCE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_PROVINCE']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               id="ADDR_PROVINCE"
                                                               name="ADDR_PROVINCE" readonly="readonly">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">รหัสไปรษณีย์</label>

                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control ADDR_POSTCODE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               value="<?php echo $addrInf['personal_card_addr']['ADDR_POSTCODE']; ?>"
                                                            <?php echo ($addrInf['personal_card_addr']['ADDR_REF_MAIN'] == 1) ? 'disabled' : ''; ?>
                                                               id="ADDR_POSTCODE"
                                                               name="ADDR_POSTCODE"
                                                               maxlength="5" onkeypress="return onlyNumber(event);"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-info">

                                            <!--ที่อยู่ที่สามารถติดต่อได้-->
                                            <div id="contact_addr" class="col-sm-6 border-form-add outlet-addr-form">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ที่อยู่ที่สามารถติดต่อได้</h3>
                                                </div>
                                                <div class="box-body">

                                                    <div class="form-group"
                                                         style="<?php echo ($optional['viewOnly']) ? 'display:none' : ''; ?>">


                                                        <input type="checkbox"
                                                               id="isCopyPersonalAddr"
                                                               name="isCopyPersonalAddr"
                                                               value="1"
                                                            <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'checked' : ''; ?>
                                                               onclick="toggleCopyAddr(this)"
                                                               class="isCheckCopy"/>
                                                        คัดลอกที่อยู่
                                                        <div class="select_copy"
                                                             style="margin-left:30px;<?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] == 0) ? 'display:none' : ''; ?>">
                                                            <input type="radio"
                                                                   class="except_dis"
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] == 1) ? 'checked' : ''; ?>
                                                                   value="home_addr"
                                                                   name="copy_addr" id="copy_addr1"/>
                                                            ตามทะเบียนบ้าน
                                                            <input type="radio"
                                                                   class="except_dis"
                                                                   value="personal_card_addr"
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] == 2) ? 'checked' : ''; ?>
                                                                   name="copy_addr" id="copy_addr2"/>
                                                            บัตรประชาชน
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ADDR_NUMBER" class="col-sm-2 control-label">บ้านเลขที่</label>
                                                        <div class="col-sm-2">
                                                            <input type="text"
                                                                   name="ADDR_NUMBER"
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_NUMBER']; ?>"
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   class="form-control ADDR_NUMBER bindChange <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="ADDR_NUMBER" size="3">
                                                        </div>
                                                        <label for="ADDR_GROUP_NO"
                                                               class="col-sm-2 control-label">หมู่</label>

                                                        <div class="col-sm-2">
                                                            <input type="text"
                                                                   name="ADDR_GROUP_NO"
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_GROUP_NO']; ?>"
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   class="form-control ADDR_GROUP_NO <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="ADDR_GROUP_NO"
                                                                   size="3">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">หมู่บ้าน</label>

                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control ADDR_VILLAGE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_VILLAGE']; ?>"
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   id="ADDR_VILLAGE"
                                                                   name="ADDR_VILLAGE">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">ตรอก/ซอย</label>

                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control ADDR_LANE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_LANE']; ?>"
                                                                   id="ADDR_LANE"
                                                                   name="ADDR_LANE">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">ถนน</label>

                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control ADDR_ROAD <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_ROAD']; ?>"
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   id="ADDR_ROAD"
                                                                   name="ADDR_ROAD">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">ตำบล</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" id="ADDR_SUB_DISTRICT"
                                                                   name="ADDR_SUB_DISTRICT"
                                                                   class="form-control ADDR_SUB_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_SUB_DISTRICT']; ?>"
                                                                   readonly="readonly">
                                                        </div>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default" id="btnTContactAddr"
                                                                    data-title="ค้นหาตำบล" type="button"
                                                                    data-placement="bottom" data-toggle="popover"
                                                                    data-container="body" data-html="true"><i
                                                                        class="fa fa-search"></i> </button>
                                                    </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">อำเภอ</label>

                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control ADDR_DISTRICT <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_DISTRICT']; ?>"
                                                                   id="ADDR_DISTRICT"
                                                                   name="ADDR_DISTRICT" readonly="readonly">
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/><br/>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">จังหวัด</label>

                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control ADDR_PROVINCE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_PROVINCE']; ?>"
                                                                   id="ADDR_PROVINCE"
                                                                   name="ADDR_PROVINCE" readonly="readonly">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for=""
                                                               class="col-sm-3 control-label">รหัสไปรษณีย์</label>

                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control ADDR_POSTCODE <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                <?php echo ($addrInf['contact_addr']['ADDR_REF_MAIN'] != 0) ? 'disabled' : ''; ?>
                                                                   value="<?php echo $addrInf['contact_addr']['ADDR_POSTCODE']; ?>"
                                                                   id="ADDR_POSTCODE"
                                                                   name="ADDR_POSTCODE"
                                                                   maxlength="5" onkeypress="return onlyNumber(event);"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--รูปแผนที่บ้าน-->
                                            <div class="col-sm-6">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title"></h3>
                                                    <div class="box-body">
                                                        <div class="col-sm-12 form-group"
                                                             style="<?php echo ($optional['viewOnly']) ? 'display:none' : ''; ?>">
                                                            <div class="col-sm-6">
                                                                รูปแผนที่บ้าน
                                                                <input type="file" class="form-control" id="Pic_map"
                                                                       name="Pic_map">

                                                            </div>

                                                        </div>
                                                        <div class="col-sm-12 form-group">

                                                            <div id="img-holder-map" class="col-sm-12"
                                                                 style="border:1px solid #3c8dbc;height: 160px">
                                                                <?php if ($addrInf['contact_addr']['ADDR_MAP_DESC'] != '') { ?>
                                                                    <img src="<?php echo Yii::$app->request->baseUrl . '/upload/map/' . $addrInf['contact_addr']['ADDR_MAP_DESC']; ?>"
                                                                         alt="image img" style="height:158px;">
                                                                <?php } else { ?>
                                                                    <h2 style="color:#555;">ไม่มีแผนที่</h2>
                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12 form-group">
                                                            <label for="" class="col-sm-3 control-label">ละติจูด
                                                                (E)</label>

                                                            <div class="col-sm-6">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="Latitude"
                                                                       name="Latitude"
                                                                       value="<?php echo $empModel['Latitude']; ?>"
                                                                >
                                                            </div>
                                                            <label for="" class="col-sm-3 control-label">องศา</label>
                                                        </div>

                                                        <div class="col-sm-12  form-group">
                                                            <label for="" class="col-sm-3 control-label">ลองติจูด
                                                                (N)</label>

                                                            <div class="col-sm-6">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="Longitude"
                                                                       name="Longitude"
                                                                       value="<?php echo $empModel['Longitude']; ?>">
                                                            </div>
                                                            <label for="" class="col-sm-3 control-label">องศา</label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">การพักอาศัย</h3>
                                            </div>
                                            <div class="box-body" id="liveWithForm">
                                                <div class="form-group">
                                                    <label for="">ปัจจุบันพักอยู่กับ</label>
                                                </div>

                                                <div style="<?php echo ($optional['viewOnly']) ? 'display:none' : ''; ?>">

                                                    <?php foreach ($data['liveWith'] as $k => $lw) { ?>
                                                        <div class="form-group  form-inline col-sm-12">
                                                            <div class="col-sm-1">
                                                                <input type="radio"
                                                                       name="<?php echo $lw['name']; ?>"
                                                                    <?php echo $lw['checked']; ?>
                                                                       value="<?php echo $lw['value']; ?>"/>
                                                            </div>
                                                            <div class="col-sm-10 control-label">
                                                                <strong><?php echo $lw['text'] ?></strong>
                                                                <?php if ($lw['type'] == '2') { ?>
                                                                    <input type="text" style="width:70%"
                                                                           class="form-control"
                                                                           name="liveWithOtherDesc"
                                                                           id="liveWithOtherDesc"
                                                                           value="<?php echo ($lw['checked'] == 'checked') ? $empModel['Live_with'] : ''; ?>">
                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                        <br>
                                                        <br>
                                                    <?php } ?>

                                                </div>
                                                <div style="<?php echo (!$optional['viewOnly']) ? 'display:none' : ''; ?>">
                                                    <?php echo $empModel['Live_with']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">ประเภทที่พัก</h3>
                                            </div>
                                            <div class="box-body" id="residentWithForm">
                                                <div class="form-group">
                                                    <label for="">ประเภทที่พัก</label>
                                                </div>
                                                <div style="<?php echo ($optional['viewOnly']) ? 'display:none' : ''; ?>">
                                                    <?php foreach ($data['residentWith'] as $k => $lw) { ?>
                                                        <div class="form-group  form-inline col-sm-12">
                                                            <div class="col-sm-1">
                                                                <input type="radio"
                                                                       name="<?php echo $lw['name'] ?>" <?php echo $lw['checked']; ?>
                                                                       value="<?php echo $lw['value']; ?>">
                                                            </div>
                                                            <div class="col-sm-10 control-label">
                                                                <strong><?php echo $lw['text'] ?></strong>
                                                                <?php if ($lw['type'] == '2') { ?>
                                                                    <input type="text" style="width:70%"
                                                                           class="form-control"
                                                                           name="residentWithOtherDesc"
                                                                           id="residentWithOtherDesc"
                                                                           value="<?php echo ($lw['checked'] == 'checked') ? $empModel['Resident_type'] : ''; ?>">
                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                        <br>
                                                        <br>
                                                    <?php } ?>
                                                </div>
                                                <div style="<?php echo (!$optional['viewOnly']) ? 'display:none' : ''; ?>">
                                                    <?php echo $empModel['Resident_type']; ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">การติดต่อสื่อสาร</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="Tel_Num"
                                                           class="col-sm-4 control-label">เบอร์โทรบ้าน</label>
                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="Tel_Num"
                                                               name="Tel_Num"
                                                               value="<?php echo $empModel['Tel_Num']; ?>"
                                                        />
                                                    </div>
                                                </div>
                                                <br><br>
                                                <div class="form-group">
                                                    <label for="Mobile_Num"
                                                           class="col-sm-4 control-label">เบอร์มือถือ</label>
                                                    <div class="col-sm-6">
                                                        <input type="text"
                                                               class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                               id="Mobile_Num" name="Mobile_Num"
                                                               value="<?php echo $empModel['Mobile_Num']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane " id="tab_3">
                                <div id="family_relation">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form class="form-horizontal" id="father_tax_use" name="father_tax_use">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">ข้อมูลบิดา</h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="radio"
                                                                           name="father_tax_status"
                                                                           value="1"
                                                                        <?php echo ($family['father_tax_status'] == '1') ? 'checked' : ''; ?>
                                                                           id="father_tax_status_use">
                                                                    ใช้สิทธิ์ลดหย่อน
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="radio"
                                                                           name="father_tax_status"
                                                                           value="0"
                                                                        <?php echo ($family['father_tax_status'] == '0') ? 'checked' : ''; ?>
                                                                           id="father_tax_status_not" checked>
                                                                    ไม่ใช้สิทธิ์ลดหย่อน
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <?php echo ($family['father_tax_status'] == '1') ? 'ใช้สิทธิ์ลดหย่อน' : 'ไม่ใช้สิทธิ์ลดหย่อน'; ?>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">สถานะบิดา-มารดา</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?>
                                                                        id="parent_status" name="parent_status">
                                                                    <option value="">กรุณาเลือกสถานะ</option>
                                                                    <?php echo $rel['option_parent_status']; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">คำนำหน้า</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?>
                                                                        id="father_be" name="father_be">
                                                                    <?php echo $rel['option_father_be']; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">ชื่อ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_name"
                                                                       name="father_name"
                                                                       value="<?php echo $family['father_name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">นามสกุล</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_surname"
                                                                       name="father_surname"
                                                                       value="<?php echo $family['father_surname']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">หมายเลขบัตรประชาชน</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_idcard"
                                                                       name="father_idcard"
                                                                       value="<?php echo $family['father_idcard']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">วัน เดือน
                                                                ปีเกิด</label>
                                                            <div class="col-sm-4">

                                                                <?php if (!$optional['viewOnly']) { ?>
                                                                    <div class="input-group date">
                                                                        <input type="text" readonly="readonly"
                                                                               class="form-control"
                                                                               id="father_birthday"
                                                                               name="father_birthday"
                                                                               value="<?php echo PersonalController::toggleToDataPicker($family['father_birthday']) ?>"
                                                                               placeholder="dd/mm/yyyy"/>

                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <input type="hidden"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($family['father_birthday']) ?>"
                                                                           id="father_birthday"/>
                                                                    <?php echo PersonalController::toggleToDataPicker($family['father_birthday']) ?>
                                                                <?php } ?>
                                                            </div>
                                                            <label for="" class="col-sm-1 control-label">อายุ</label>
                                                            <div class="col-sm-2">
                                                                <input type="text"
                                                                       class="form-control  <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_birthday_day" size="1" maxlength="2"
                                                                       onkeypress="return onlyNumber(event);"
                                                                       readonly="readonly">
                                                            </div>
                                                            <label for="" class="col-sm-1 control-label">ปี</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">เบี้ยประกันสุขภาพ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_insurance"
                                                                       name="father_insurance"
                                                                       value="<?php echo $family['father_insurance']; ?>"
                                                                       maxlength="6">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">อาชีพ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_business"
                                                                       name="father_business"
                                                                       value="<?php echo $family['father_business']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">ที่อยู่ที่ทำงาน</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_place"
                                                                       name="father_place"
                                                                       value="<?php echo $family['father_place']; ?>">
                                                            </div>
                                                            <label for="" class="col-sm-3 control-label">Ex.เทศบาลเชียงราย</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">เบอร์โทร</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="father_phone"
                                                                       name="father_phone"
                                                                       value="<?php echo $family['father_phone']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <form class="form-horizontal" id="mother_tax_use" name="mother_tax_use">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">ข้อมูลมารดา</h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="radio"
                                                                           name="mother_tax_status"
                                                                           value="1"
                                                                        <?php echo ($family['mother_tax_status'] == '1') ? 'checked' : ''; ?>
                                                                           id="mother_tax_status_use">
                                                                    ใช้สิทธิ์ลดหย่อน
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="radio"
                                                                           name="mother_tax_status"
                                                                           value="0"
                                                                        <?php echo ($family['mother_tax_status'] == '0') ? 'checked' : ''; ?>
                                                                           id="mother_tax_status_not_use" checked>
                                                                    ไม่ใช้สิทธิ์ลดหย่อน
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <?php echo ($family['mother_tax_status'] == '1') ? 'ใช้สิทธิ์ลดหย่อน' : 'ไม่ใช้สิทธิ์ลดหย่อน'; ?>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">คำนำหน้า</label>
                                                            <div class="col-sm-4">
                                                                <?php if (!$optional['viewOnly']) { ?>
                                                                    <select class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?>
                                                                            name="mother_be"
                                                                            id="mother_be">
                                                                        <?php echo $rel['option_mother_be']; ?>
                                                                    </select>
                                                                <?php } else { ?>
                                                                    <?php echo $family['mother_be']; ?>
                                                                <?php } ?>


                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">ชื่อ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="mother_name"
                                                                       id="mother_name"
                                                                       value="<?php echo $family['mother_name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">นามสกุล</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="mother_surname"
                                                                       id="mother_surname"
                                                                       value="<?php echo $family['mother_surname']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">หมายเลขบัตรประชาชน</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="mother_idcard"
                                                                       id="mother_idcard"
                                                                       value="<?php echo $family['mother_idcard']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">วัน เดือน
                                                                ปีเกิด</label>
                                                            <div class="col-sm-4">

                                                                <?php if (!$optional['viewOnly']) { ?>
                                                                    <div class="input-group date">
                                                                        <input type="text" readonly="readonly"
                                                                               class="form-control"
                                                                               id="mother_birthday"
                                                                               name="mother_birthday"
                                                                               value="<?php echo PersonalController::toggleToDataPicker($family['mother_birthday']) ?>"
                                                                               placeholder="dd/mm/yyyy"/>

                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <input type="hidden"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($family['mother_birthday']) ?>"
                                                                           id="mother_birthday"/>
                                                                    <?php echo PersonalController::toggleToDataPicker($family['mother_birthday']) ?>
                                                                <?php } ?>
                                                            </div>
                                                            <label for="" class="col-sm-1 control-label">อายุ</label>
                                                            <div class="col-sm-2">
                                                                <input type="text"
                                                                       class="form-control  <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="mother_birthday_day" size="1" maxlength="2"
                                                                       onkeypress="return onlyNumber(event);"
                                                                       readonly="readonly">
                                                            </div>
                                                            <label for="" class="col-sm-1 control-label">ปี</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">เบี้ยประกันสุขภาพ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="mother_insurance"
                                                                       id="mother_insurance"
                                                                       value="<?php echo $family['mother_insurance']; ?>"
                                                                       maxlength="6">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">อาชีพ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="mother_business"
                                                                       id="mother_business"
                                                                       value="<?php echo $family['mother_business']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">ที่อยู่ที่ทำงาน</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="mother_place"
                                                                       id="mother_place"
                                                                       value="<?php echo $family['mother_place']; ?>">
                                                            </div>
                                                            <label for="" class="col-sm-3 control-label">Ex.เทศบาลเชียงราย</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">เบอร์โทร</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="mother_phone"
                                                                       id="mother_phone"
                                                                       value="<?php echo $family['mother_phone']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form class="form-horizontal" id="frmMarrystatus" name="frmMarrystatus">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">ข้อมูลการสมรส</h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">สถานะภาพ</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?>
                                                                        id="spouse_status"
                                                                        name="spouse_status">
                                                                    <option value="">กรุณาเลือกสถานะ</option>
                                                                    <?php echo $rel['option_spouse_status']; ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">เพศ</label>
                                                            <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <div class="col-sm-2">
                                                                    <input type="radio"
                                                                           onclick="relativeGetGender(1,'#spouse_be')"
                                                                           value="ชาย"
                                                                           name="spouse_sex"
                                                                        <?php echo ($family['spouse_sex'] == 'ชาย') ? 'checked' : ''; ?>
                                                                           id="spouse_sex_man">
                                                                    <span>ชาย</span>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="radio"
                                                                           onclick="relativeGetGender(2,'#spouse_be')"
                                                                           value="หญิง"
                                                                           name="spouse_sex"
                                                                        <?php echo ($family['spouse_sex'] == 'หญิง') ? 'checked' : ''; ?>
                                                                           id="spouse_sex_women">
                                                                    <span>หญิง</span>
                                                                </div>
                                                            </div>
                                                            <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <div class="col-sm-4 show-inline-lable">
                                                                    <?php echo ($family['spouse_sex'] == 'ชาย') ? 'ชาย' : 'หญิง'; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">คำนำหน้าคู่สมรส</label>
                                                            <div class="col-sm-4">
                                                                <select
                                                                        class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?>
                                                                        id="spouse_be"
                                                                        name="spouse_be">
                                                                    <option value="">เลือกคำนำหน้า</option>
                                                                    <?php echo $rel['option_spous_be']; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">ชื่อ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="spouse_name"
                                                                       id="spouse_name"
                                                                       value="<?php echo $family['spouse_name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">นามสกุล</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="spouse_surname"
                                                                       id="spouse_surname"
                                                                       value="<?php echo $family['spouse_surname']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">หมายเลขบัตรประชาชน</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="spouse_idcard"
                                                                       id="spouse_idcard"
                                                                       value="<?php echo $family['spouse_idcard']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">วัน เดือน
                                                                ปีเกิด</label>
                                                            <div class="col-sm-4">

                                                                <?php if (!$optional['viewOnly']) { ?>
                                                                    <div class="input-group date">
                                                                        <input type="text" readonly="readonly"
                                                                               class="form-control"
                                                                               id="spouse_birthday"
                                                                               name="spouse_birthday"
                                                                               value="<?php echo PersonalController::toggleToDataPicker($family['spouse_birthday']) ?>"
                                                                               placeholder="dd/mm/yyyy"
                                                                               onchange="calDateToDay(this, '#spouse_birthday_day')"/>

                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <input type="hidden"
                                                                           value="<?php echo PersonalController::toggleToDataPicker($family['spouse_birthday']) ?>"
                                                                           id="spouse_birthday"/>
                                                                    <?php echo PersonalController::toggleToDataPicker($family['spouse_birthday']) ?>
                                                                <?php } ?>
                                                            </div>
                                                            <label for="" class="col-sm-1 control-label">อายุ</label>
                                                            <div class="col-sm-2">
                                                                <input type="text"
                                                                       class="form-control  <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="spouse_birthday_day" size="1"
                                                                       readonly="readonly">
                                                            </div>
                                                            <label for="" class="col-sm-1 control-label">ปี</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">เบี้ยประกันสุขภาพ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="spouse_insurance"
                                                                       id="spouse_insurance"
                                                                       value="<?php echo $family['spouse_insurance']; ?>"
                                                                       maxlength="6"
                                                                       onkeypress="return onlyDecimal(event);">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">อาชีพ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="spouse_business"
                                                                       id="spouse_business"
                                                                       value="<?php echo $family['spouse_business']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">ที่อยู่ที่ทำงาน</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="spouse_place"
                                                                       id="spouse_place"
                                                                       value="<?php echo $family['spouse_place']; ?>">
                                                            </div>
                                                            <label for="" class="col-sm-3 control-label">Ex.เทศบาลเชียงราย</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">เบอร์โทร</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       name="spouse_phone"
                                                                       id="spouse_phone"
                                                                       value="<?php echo $family['spouse_phone']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <form class="form-horizontal">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">ข้อมูลผู้ติดต่อฉุกเฉิน</h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <input type="radio"
                                                                           id="relative_status_1"
                                                                           name="relative_status"
                                                                        <?php echo ($family['relative_status'] == 'บิดา') ? 'checked' : '' ?>
                                                                           value="บิดา">
                                                                    <label>บิดา</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <input type="radio"
                                                                           id="relative_status_2"
                                                                           name="relative_status"
                                                                        <?php echo ($family['relative_status'] == 'มารดา') ? 'checked' : '' ?>
                                                                           value="มารดา">
                                                                    <label>มารดา</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <input type="radio"
                                                                           id="relative_status_4"
                                                                           name="relative_status"
                                                                        <?php echo ($family['relative_status'] == 'คู่สมรส') ? 'checked' : '' ?>
                                                                           value="คู่สมรส">
                                                                    <label>คู่สมรส</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <input type="radio"
                                                                       id="relative_status_3"
                                                                       name="relative_status"
                                                                       value="อื่นๆ"
                                                                    <?php echo ($family['relative_status'] != 'มารดา' && $family['relative_status'] != 'บิดา' && $family['relative_status'] != 'คู่สมรส') ? 'checked' : '' ?>
                                                                       class="col-sm-1">
                                                                <label class="col-sm-4">อื่นๆ (ระบุ)</label>
                                                                <div class="col-sm-5">
                                                                    <input type="text"
                                                                           class="form-control"
                                                                           id="relative_status_desc"
                                                                           name="relative_status_desc"
                                                                           value="<?php echo ($family['relative_status'] != 'มารดา' && $family['relative_status'] != 'บิดา') ? $family['relative_status'] : ''; ?>"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <br>
                                                            <br>
                                                        </div>
                                                        <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">ความสัมพันธ์</label>
                                                                <div class="col-sm-8">
                                                                    <?php echo $family['relative_status']; ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">เพศ</label>
                                                            <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <div class="col-sm-2">
                                                                    <input type="radio"
                                                                           onclick="relativeGetGender(1,'#relative_be')"
                                                                           value="ชาย"
                                                                        <?php echo ($family['relative_sex'] == 'ชาย') ? 'checked' : ''; ?>
                                                                           id="relative_sexe_1"
                                                                           name="relative_sex">
                                                                    <span>ชาย</span>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="radio"
                                                                           onclick="relativeGetGender(2,'#relative_be')"
                                                                           value="หญิง"
                                                                        <?php echo ($family['relative_sex'] == 'หญิง') ? 'checked' : ''; ?>
                                                                           id="relative_sexe_2"
                                                                           name="relative_sex">
                                                                    <span>หญิง</span>
                                                                </div>
                                                            </div>
                                                            <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <div class="col-sm-4 show-inline-lable">
                                                                    <?php echo ($family['relative_sex'] == 'หญิง') ? 'หญิง' : 'ชาย'; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">คำนำหน้า</label>
                                                            <div class="col-sm-4">
                                                                <select
                                                                        class="form-control <?php echo $optional['disabled']; ?>" <?php echo $optional['readonly']; ?>
                                                                        id="relative_be"
                                                                        name="relative_be">
                                                                    <option value="">เลือกคำนำหน้า</option>
                                                                    <?php echo $rel['option_relative_be']; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">ชื่อ</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="relative_name"
                                                                       name="relative_name"
                                                                       value="<?php echo $family['relative_name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">นามสกุล</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="relative_surname"
                                                                       name="relative_surname"
                                                                       value="<?php echo $family['relative_surname']; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for=""
                                                                   class="col-sm-3 control-label">เบอร์โทร</label>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                       id="relative_phone"
                                                                       name="relative_phone"
                                                                       value="<?php echo $family['relative_phone']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลบุตร</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-md-5">
                                                            <label>สถานะบุตร</label>
                                                            <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                &nbsp;
                                                                <input type="radio"
                                                                       id="children_status1" <?php echo ($empModel['Childs'] == 'มี') ? 'checked' : ''; ?>
                                                                       value="มี" name="children_status">
                                                                <label>มี</label>
                                                                &nbsp;
                                                                <input type="radio"
                                                                       id="children_status2" <?php echo ($empModel['Childs'] == 'ไม่มี') ? 'checked' : ''; ?>
                                                                       value="ไม่มี" name="children_status">
                                                                <label>ไม่มี</label>
                                                            </div>
                                                            <span class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                               <b>:</b> <?php echo $empModel['Childs']; ?>
                                                          </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div align="right"
                                                                 class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <button type="button" id="btnAddChildren"
                                                                        name="btnAddChildren" class="btn btn-primary"
                                                                        onclick="showModalChildren(this, 'new')">
                                                                    เพิ่มข้อมูล
                                                                </button>
                                                            </div>
                                                            <div class="modal fade modal-wide" id="modalAddChild"
                                                                 tabindex="-1" role="dialog"
                                                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal"
                                                                                    aria-hidden="true">&times;
                                                                            </button>
                                                                            <h4 id="myModalLabel" class="modal-title">
                                                                                เพิ่มข้อมูลบุตร</h4>
                                                                        </div>
                                                                        <form action="#" method="post">
                                                                            <input type="hidden"
                                                                                   value="<?= Yii::$app->request->csrfToken; ?>"
                                                                                   name="_csrf">
                                                                            <div class="modal-body" width="100%"
                                                                                 class="table table-bordered table-hover dataTable">
                                                                                <div class="form-group">
                                                                                    <label for="genderEmpChild"
                                                                                           class="col-sm-4 control-label">เพศ</label>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio"
                                                                                               onclick="relativeGetGender(1,'#child_prefix')"
                                                                                               value="ชาย"
                                                                                               name="child_sex"
                                                                                               id="child_sex">
                                                                                        <span>ชาย</span>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio"
                                                                                               onclick="relativeGetGender(2,'#child_prefix')"
                                                                                               value="หญิง"
                                                                                               name="child_sex"
                                                                                               id="child_sex">

                                                                                        <span>หญิง</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="beNameChild"
                                                                                           class="col-sm-4 control-label">คำนำหน้า</label>
                                                                                    <div class="col-sm-4">
                                                                                        <select class="form-control"
                                                                                                id="child_prefix"
                                                                                                name="child_prefix">
                                                                                            <option value="">
                                                                                                เลือกคำนำหน้า
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="nameChild"
                                                                                           class="col-sm-4 control-label">ชื่อ</label>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="text"
                                                                                               class="form-control"
                                                                                               id="child_name"
                                                                                               name="child_name"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="LastnameChild"
                                                                                           class="col-sm-4 control-label">นามสกุล</label>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="text"
                                                                                               class="form-control"
                                                                                               id="child_surname"
                                                                                               name="child_surname"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for=""
                                                                                           class="col-sm-4 control-label">สถานะทางการศึกษา</label>
                                                                                    <div class="col-sm-4">
                                                                                        <select class="form-control"
                                                                                                id="child_education_status"
                                                                                                name="child_education_status">
                                                                                            <option value="">
                                                                                                เลือกสถานะการศึกษา
                                                                                            </option>
                                                                                            <option value="1">
                                                                                                กำลังศึกษา
                                                                                            </option>
                                                                                            <option value="2">
                                                                                                จบการศึกษา
                                                                                            </option>
                                                                                            <option value="3">
                                                                                                ไม่ได้รับการศึกษา
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for=""
                                                                                           class="col-sm-4 control-label">ระดับการศึกษา</label>
                                                                                    <div class="col-sm-4">
                                                                                        <!--<select class="form-control" id="child_class" name="child_class">
                                                                                        <option value="">เลือกระดับการศึกษา</option>
                                                                                      </select>
                                                                                      <input type="text" class="form-control"  id="child_class" name="child_class">
                                                                                      -->
                                                                                        <?php
                                                                                        echo Html::dropDownList('child_class', null, $arrEduLevel, [
                                                                                            'id' => 'child_class',
                                                                                            'prompt' => 'ระดับการศึกษา',
                                                                                            'class' => 'form-control',
                                                                                        ]);
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for=""
                                                                                           class="col-sm-4 control-label">สถานศึกษา</label>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="text"
                                                                                               class="form-control"
                                                                                               id="child_school"
                                                                                               name="child_school">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for=""
                                                                                           class="col-sm-4 control-label">ชั้นปีที่กำลังศึกษา</label>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="text"
                                                                                               class="form-control"
                                                                                               id="child_grade"
                                                                                               name="child_grade">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for=""
                                                                                           class="col-sm-4 control-label">วันเกิด</label>
                                                                                    <div class="col-sm-4">

                                                                                        <div class="input-group date">
                                                                                            <input type="text"
                                                                                                   readonly="readonly"
                                                                                                   class="form-control"
                                                                                                   id="child_birthday"
                                                                                                   name="child_birthday"
                                                                                                   placeholder="dd/mm/yyyy"
                                                                                                   onchange="calDateToDay(this, '#child_age')"/>

                                                                                            <div class="input-group-addon">
                                                                                                <i class="fa fa-calendar"></i>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for=""
                                                                                           class="col-sm-4 control-label">อายุ</label>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="text"
                                                                                               readonly="readonly"
                                                                                               class="form-control"
                                                                                               id="child_age"
                                                                                               name="child_age">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <input type="button" id="btnSaveChild"
                                                                                       class="btn btn-primary"
                                                                                       data-reftr=""
                                                                                       onclick="addChildren(this)"
                                                                                       value="บันทึกข้อมูล">
                                                                                <button type="button"
                                                                                        class="btn btn-default"
                                                                                        data-dismiss="modal">Close
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div><!-- /.modal-content -->
                                                                </div><!-- /.modal-dialog -->
                                                            </div><!-- /.modal -->


                                                            <table class="table table-hover" data-delchild="[]"
                                                                   id="children_tbx">
                                                                <thead>
                                                                <tr>
                                                                    <th>ลำดับ</th>
                                                                    <th>ชื่อ - นามสกุล</th>
                                                                    <th>เพศ</th>
                                                                    <th>สถานศึกษา</th>
                                                                    <th>ชั้น</th>
                                                                    <th>วัน/เดือน/ปี เกิด</th>
                                                                    <th>อายุ</th>
                                                                    <th class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                        การจัดการ
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td colspan="8">ไม่มีข้อมูล</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div align="right">

                                        </div>
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">รายละเอียดบุตร</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-2 control-label">จำนวนบุตรชาย</label>
                                                                <div class="col-sm-1">
                                                                    <input type="text" id="amount_child_m" value="0"
                                                                           class="form-control" Disabled>
                                                                </div>
                                                                <label for="" class="col-sm-1 control-label">คน</label>

                                                                <label for="" class="col-sm-2 control-label">จำนวนบุตรหญิง</label>
                                                                <div class="col-sm-1">
                                                                    <input type="text" id="amount_child_w" value="0"
                                                                           class="form-control" Disabled>
                                                                </div>
                                                                <label for="" class="col-sm-1 control-label">คน</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-2 control-label">สถานะทางการศึกษา
                                                                    :</label>
                                                                <label for=""
                                                                       class="col-sm-2 control-label">ไม่ได้ศึกษา</label>
                                                                <div class="col-sm-1">
                                                                    <input type="text" id="amount_edu_no" value="0"
                                                                           class="form-control" Disabled>
                                                                </div>
                                                                <label for="" class="col-sm-1 control-label">คน</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <label for=""
                                                                       class="col-sm-2 control-label">&nbsp;</label>
                                                                <label for=""
                                                                       class="col-sm-2 control-label">กำลังศึกษา</label>
                                                                <div class="col-sm-1">
                                                                    <input type="text" id="amount_edu_do" value="0"
                                                                           class="form-control" Disabled>
                                                                </div>
                                                                <label for="" class="col-sm-1 control-label">คน</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <label for=""
                                                                       class="col-sm-2 control-label">&nbsp;</label>
                                                                <label for=""
                                                                       class="col-sm-2 control-label">จบการศึกษา</label>
                                                                <div class="col-sm-1">
                                                                    <input type="text" id="amount_edu_past" value="0"
                                                                           class="form-control" Disabled>
                                                                </div>
                                                                <label for="" class="col-sm-1 control-label">คน</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane " id="tab_4">
                                <div class="row">
                                    <div class="col-md-7">
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลลดหย่อนภาษีของผู้มีเงินได้</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="tax_personal" class="col-sm-4 control-label">เบี้ยประกันชีวิตตัวเอง</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_personal"
                                                                   name="tax_personal"
                                                                   value="<?php echo $taxIncomeModel['tax_personal']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_keepback" class="col-sm-4 control-label">เงินสะสมกองทุนสำรองเลี้ยงชีพ</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_keepback"
                                                                   name="tax_keepback"
                                                                   value="<?php echo $taxIncomeModel['tax_keepback']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_rating" class="col-sm-4 control-label">เงินสะสม
                                                            กบข.</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_rating"
                                                                   name="tax_rating"
                                                                   value="<?php echo $taxIncomeModel['tax_rating']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_teachers" class="col-sm-4 control-label">เงินสะสมกองทุนสงเคราะห์ครูโรงเรียนเอกชน</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_teachers"
                                                                   name="tax_teachers"
                                                                   value="<?php echo $taxIncomeModel['tax_teachers']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_rmf" class="col-sm-4 control-label">กองทุนเพื่อการเลี้ยงชีพ
                                                            (RMF)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_rmf"
                                                                   name="tax_rmf"
                                                                   value="<?php echo $taxIncomeModel['tax_rmf']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_ltf" class="col-sm-4 control-label">กองทุนรวมหุ้นระยะยาว
                                                            (LTF)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_ltf"
                                                                   name="tax_ltf"
                                                                   value="<?php echo $taxIncomeModel['tax_ltf']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_increase_home" class="col-sm-4 control-label">ดอกเบี้ยเงินกู้เพื่อที่อยู่อาศัย</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_increase_home"
                                                                   name="tax_increase_home"
                                                                   value="<?php echo $taxIncomeModel['tax_increase_home']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_social" class="col-sm-4 control-label">กองทุนประกันสังคม</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_social"
                                                                   name="tax_social"
                                                                   value="<?php echo $taxIncomeModel['tax_social']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_education" class="col-sm-4 control-label">เงินสนับสนุนเพื่อการศึกษา</label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_education"
                                                                   name="tax_education"
                                                                   value="<?php echo $taxIncomeModel['tax_education']; ?>"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-5">
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลลดหย่อนภาษีของผู้มีเงินได้</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">

                                                        <div class="col-sm-12 <?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-5">
                                                                <input type="radio"
                                                                       value="1"
                                                                       name="tax_income_spouse_status"
                                                                       id="tax_income_spouse_status1"
                                                                    <?php echo ($taxIncomeModel['tax_income_spouse_status'] == '1') ? 'checked' : ''; ?>
                                                                />
                                                                คู่สมรสมีเงินได้
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <input type="radio"
                                                                       value="2"
                                                                       name="tax_income_spouse_status"
                                                                       id="tax_income_spouse_status2"
                                                                    <?php echo ($taxIncomeModel['tax_income_spouse_status'] == '2') ? 'checked' : ''; ?>
                                                                />
                                                                คู่สมรสไม่มีเงินได้
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 <?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-5">
                                                                - <?php echo ($taxIncomeModel['tax_income_spouse_status'] == '1') ? 'คู่สมรสมีเงินได้' : 'คู่สมรสไม่มีเงินได้'; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-sm-12 <?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-5">
                                                                <input type="radio"
                                                                       value="1"
                                                                       name="tax_income_with_spouse_status"
                                                                       id="tax_income_with_spouse_status1"
                                                                    <?php echo ($taxIncomeModel['tax_income_with_spouse_status'] == '1') ? 'checked' : ''; ?>
                                                                />
                                                                ยื่นร่วมกับคู่สมรส
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <input type="radio"
                                                                       value="2"
                                                                       name="tax_income_with_spouse_status"
                                                                       id="tax_income_with_spouse_status2"
                                                                    <?php echo ($taxIncomeModel['tax_income_with_spouse_status'] == '2') ? 'checked' : ''; ?>
                                                                />
                                                                แยกยื่นแบบกับคู่สมรส
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12 <?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-5">
                                                                - <?php echo ($taxIncomeModel['tax_income_with_spouse_status'] == '1') ? 'ยื่นร่วมกับคู่สมรส' : 'แยกยื่นแบบกับคู่สมรส'; ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-sm-12 <?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-5">
                                                                <input type="radio"
                                                                       value="1" <?php echo ($taxIncomeModel['tax_income_children_status'] == '1') ? "checked" : ""; ?>
                                                                       name="tax_income_children_status"
                                                                       id="tax_income_children_status1">
                                                                บุตรศึกษา
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <input type="radio"
                                                                       value="2" <?php echo ($taxIncomeModel['tax_income_children_status'] == '2') ? "checked" : ""; ?>
                                                                       name="tax_income_children_status"
                                                                       id="tax_income_children_status2">
                                                                บุตรไม่ศึกษา
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 <?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-5">
                                                                - <?php echo ($taxIncomeModel['tax_income_children_status'] == '1') ? 'บุตรศึกษา' : 'บุตรไม่ศึกษา'; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_income_spouse" class="col-sm-4 control-label">รายได้คู่สมรส</label>
                                                        <div class="col-sm-3">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_income_spouse"
                                                                   name="tax_income_spouse"
                                                                   value="<?php echo $taxIncomeModel['tax_income_spouse']; ?>"
                                                            />
                                                        </div>
                                                        <label class="col-sm-3 control-label">บาท/เดือน</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tax_insurance_spouse"
                                                               class="col-sm-4 control-label">เบี้ยประกันชีวิตคู่สมรส</label>
                                                        <div class="col-sm-3">
                                                            <input type="text"
                                                                   class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                   id="tax_insurance_spouse"
                                                                   name="tax_insurance_spouse"
                                                                   value="<?php echo $taxIncomeModel['tax_insurance_spouse']; ?>"
                                                            />
                                                        </div>
                                                        <label class="col-sm-3 control-label">บาท/เดือน</label>
                                                    </div>


                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane " id="tab_5">
                                <div class="row">
                                    <div class="col-md-12">

                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">การศึกษา</h3>
                                                </div>
                                                <div align="right">
                                                    <button type="button"
                                                            class="btn btn-primary <?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>"
                                                            onclick="showModalEducation(this,'new')">เพิ่มข้อมูล
                                                    </button>
                                                </div>
                                                <div class="modal fade modal-wide" id="modalAddEducate" role="dialog"
                                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                                <h4 id="myModalLabel" class="modal-title">
                                                                    เพิ่มข้อมูลการศึกษา</h4>
                                                            </div>
                                                            <form action="#" method="post">
                                                                <input type="hidden"
                                                                       value="<?= Yii::$app->request->csrfToken; ?>"
                                                                       name="_csrf">
                                                                <div class="modal-body" width="50%"
                                                                     class="table table-bordered table-hover dataTable">
                                                                    <div class="form-group">
                                                                        <label for="" class="col-sm-4 control-label">ระดับการศึกษา</label>
                                                                        <div class="col-sm-4">

                                                                            <!--<input type="text" class="form-control" id="edu_level" name="edu_level" >-->
                                                                            <?php
                                                                            echo Html::dropDownList('edu_level', null, $arrEduLevel, [
                                                                                'id' => 'edu_level',
                                                                                'prompt' => 'ระดับการศึกษา',
                                                                                'class' => 'form-control',
                                                                            ]);
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="" class="col-sm-4 control-label">สถานศึกษา</label>
                                                                        <div class="col-sm-8">
                                                                            <?php
                                                                            $arrUniversity = ApiPersonal::configUniversity();
                                                                            /*echo Html::dropDownList('edu_school', null, $arrUniversity, [
                                                                        'id' => 'edu_school',
                                                                        //'prompt' => 'สถานศึกษา',
                                                                        'class' => 'select2 sel_width',
                                                                        'options'=>[
                                                                                'style'=>['width'=>'200px',]
                                                                        ],
                                                                    ]);
                                                                    */
                                                                            ?>
                                                                            <select class="select2 form-control"
                                                                                    id="edu_school" name="edu_school"
                                                                                    style="width:400px !important;">
                                                                                <?php
                                                                                foreach ($arrUniversity as $u) {
                                                                                    echo '<option>' . $u . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="" class="col-sm-4 control-label">สาขาวิชา</label>
                                                                        <div class="col-sm-4">
                                                                            <input type="text" class="form-control"
                                                                                   id="edu_major" name="edu_major">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="" class="col-sm-4 control-label">จบ
                                                                            พ.ศ.</label>
                                                                        <div class="col-sm-4">
                                                                            <input type="text" class="form-control"
                                                                                   id="edu_finish" name="edu_finish">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="" class="col-sm-4 control-label">เกรดเฉลี่ย</label>
                                                                        <div class="col-sm-4">
                                                                            <input type="text" class="form-control"
                                                                                   id="edu_GPA" name="edu_GPA">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <input type="button" class="btn btn-primary"
                                                                           id="btnSaveEducation" data-reftr="[]"
                                                                           onclick="addEducation(this)"
                                                                           value="บันทึกข้อมูล">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                                <div class="box-body">
                                                    <table class="table table-hover" data-delchild="[]"
                                                           id="emp_education_tb">
                                                        <thead>
                                                        <tr>
                                                            <th>ระดับการศึกษา</th>
                                                            <th>สถานศึกษา</th>
                                                            <th>สาขาวิชา</th>
                                                            <th>จบ พ.ศ.</th>
                                                            <th>เกรดเฉลี่ย</th>
                                                            <th class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                การจัดการ
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>ปริญญาตรี</td>
                                                            <td>มหาวิทยาลัยเชียงใหม่</td>
                                                            <td>รัฐศาสตร์</td>
                                                            <td>2556</td>
                                                            <td>3.50</td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane " id="tab_6">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form class="form-horizontal">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">ข้อมูลการทำงาน</h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label for="imageEmp" class="col-sm-3 control-label">วันหยุดประจำสัปดาห์</label>
                                                            <div class="col-sm-6">
                                                                <select id="DayOff" name="DayOff"
                                                                        class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                    <?php echo $rel['option_dayoff']; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmp" class="col-sm-3 control-label">วันที่เริ่มงาน</label>
                                                            <div class="col-sm-6">

                                                                <?php if (!$optional['viewOnly']) { ?>
                                                                    <div class="input-group date">
                                                                        <input type="text" readonly="readonly"
                                                                               class="form-control"
                                                                               id="Start_date"
                                                                               name="Start_date"
                                                                               value="<?php echo PersonalController::toggleToDataPicker($empModel['Start_date']) ?>"
                                                                               placeholder="dd/mm/yyyy"/>

                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <?php echo PersonalController::toggleToDataPicker($empModel['Start_date']) ?>
                                                                <?php } ?>


                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmpMain" class="col-sm-3 control-label">วันที่ผ่านทดลองงาน</label>
                                                            <div class="col-sm-6">

                                                                <?php if (!$optional['viewOnly']) { ?>
                                                                    <div class="input-group date">
                                                                        <input type="text" readonly="readonly"
                                                                               class="form-control"
                                                                               id="Probation_Date"
                                                                               name="Probation_Date"
                                                                               value="<?php echo PersonalController::toggleToDataPicker($empModel['Probation_Date']) ?>"
                                                                               placeholder="dd/mm/yyyy"/>

                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <?php echo PersonalController::toggleToDataPicker($empModel['Probation_Date']) ?>
                                                                <?php } ?>


                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmpMain" class="col-sm-3 control-label">ประเภทการจ้างงาน</label>
                                                            <div class="col-sm-6">
                                                                <select id="job_type" name="job_type"
                                                                        class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                    <?php echo $rel['job_type']; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmpMain" class="col-sm-3 control-label">สถานะการทำงาน</label>
                                                            <div class="col-sm-6">
                                                                <select id="Prosonnal_Being" name="Prosonnal_Being"
                                                                        onchange="resetWorkOut(this)"
                                                                        class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                    <?php echo $rel['Prosonnal_Being']; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <form class="form-horizontal">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">ข้อมูลการลาออก</h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label for="imageEmp" class="col-sm-3 control-label">วันที่ทำงานวันสุดท้าย</label>
                                                            <div class="col-sm-6">

                                                                <?php if (!$optional['viewOnly']) { ?>
                                                                    <div class="input-group date">
                                                                        <input type="text" readonly="readonly"
                                                                               class="form-control"
                                                                               id="End_date"
                                                                               name="End_date"
                                                                               value="<?php echo PersonalController::toggleToDataPicker($empModel['End_date']) ?>"
                                                                               placeholder="dd/mm/yyyy"/>

                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <?php echo PersonalController::toggleToDataPicker($empModel['End_date']) ?>
                                                                <?php } ?>


                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmp" class="col-sm-3 control-label">เหตุผลที่ลาออก</label>
                                                            <div class="col-sm-6 <?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <select id="Main_EndWork_Reason"
                                                                        name="Main_EndWork_Reason"
                                                                        class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                    <?php echo $rel['Main_EndWork_Reason']; ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-6 <?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                                <?php echo ($empModel['Main_EndWork_Reason'] == '') ? '-' : $empModel['Main_EndWork_Reason']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="codeEmpMain" class="col-sm-3 control-label">อธิบายเหตุผลที่ลาออก</label>
                                                            <div class="col-sm-6">
                                                                <textarea id="EndWork_Reason" name="EndWork_Reason"
                                                                          class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['readonly']; ?>
                                                                          rows="3"
                                                                          placeholder=""><?php echo $empModel['EndWork_Reason']; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row>">
                                        <form class="form-horizontal">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">ข้อมูลเวลาเข้า-ออกงาน</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="imageEmp" class="col-sm-2 control-label">เวลาเข้างานเช้า</label>
                                                        <div class="col-sm-2">
                                                            <select id="startWorkTime_h" name="startWorkTime_h"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['startWorkTime_h']; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <select id="startWorkTime_m" name="startWorkTime_m"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['startWorkTime_m']; ?>
                                                            </select>
                                                        </div>
                                                        <label for="imageEmp"
                                                               class="col-sm-1 control-label">เวลาเลิกงาน</label>
                                                        <div class="col-sm-2">
                                                            <select id="endWorkTime_h" name="endWorkTime_h"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['endWorkTime_h']; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <select id="endWorkTime_m" name="endWorkTime_m"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['endWorkTime_m']; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="imageEmp" class="col-sm-2 control-label">เวลาพักเที่ยง</label>
                                                        <div class="col-sm-2">
                                                            <select id="beginLunch_h" name="beginLunch_h"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['beginLunch_h']; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <select id="beginLunch_m" name="beginLunch_m"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['beginLunch_m']; ?>
                                                            </select>
                                                        </div>
                                                        <label for="imageEmp"
                                                               class="col-sm-1 control-label">ถึงเวลา</label>
                                                        <div class="col-sm-2">
                                                            <select id="endLunch_h" name="endLunch_h"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['endLunch_h']; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <select id="endLunch_m" name="endLunch_m"
                                                                    class="form-control <?php echo $optional['dontouch']; ?>" <?php echo $optional['disabled']; ?> >
                                                                <?php echo $rel['endLunch_m']; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="imageEmp"
                                                               class="col-sm-2 control-label">การสแกนบัตร</label>
                                                        <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-2">
                                                                <input type="radio" <?php echo ($empModel['use_check'] == 'YES') ? 'checked' : '' ?>
                                                                       value="YES" name="use_check" id="use_check_1">
                                                                ใช้
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input type="radio" <?php echo ($empModel['use_check'] == 'NO') ? 'checked' : '' ?>
                                                                       value="NO" name="use_check" id="use_check_2">
                                                                ไม่ใช้
                                                            </div>
                                                        </div>
                                                        <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone' : ''; ?>">
                                                            <div class="col-sm-2">
                                                                <?php echo ($empModel['use_check'] == 'YES') ? 'ใช้' : 'ไม่ใช้'; ?>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_7">
                                <div class="box-body">
                                    <h2>รอสักครู่..</h2>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_8">
                                <div class="box-body">
                                    <h2>รอสักครู่..</h2>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_9">
                                <div class="box-body">
                                    <h2>รอสักครู่..</h2>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_10">

                                <div class="box-body">
                                    <div class="box box-warning">
                                        <div class="box-header with-border">
                                            <div class="row">
                                                <div class="col-sm-6"><h3 class="box-title">รูปแบบการรับเงิน</h3></div>
                                                <div class="col-sm-6">

                                                    <div class="form-group">
                                                        <input type="radio" name="SalaryViaBank" id="SalaryViaBank0"
                                                               value="0" onclick="Cash();">
                                                        <span>เงินสด</span>
                                                        <input type="radio" name="SalaryViaBank" id="SalaryViaBank1"
                                                               value="1" onclick="TransferBank();" checked>
                                                        <span>เงินโอน</span>
                                                        <input type="radio" name="SalaryViaBank" id="SalaryViaBank2"
                                                               value="2" onclick="TransferPromptPay();">
                                                        <span>พร้อมเพย์</span>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label for="selectBank" class="col-sm-4 control-label">ธนาคาร
                                                                <font color="red">*</font></label>
                                                            <div class="col-sm-8">
                                                                <?php

                                                                $arrBankName = ApiPersonal::configBank();
                                                                $_editBank = ($mode == 'edit') ? $empModel['Salary_Bank'] : 'ธนาคารกรุงไทย';
                                                                echo Html::dropDownList('Salary_Bank', $_editBank, $arrBankName, [
                                                                    'id' => 'Salary_Bank',
                                                                    'prompt' => 'เลือกธนาคาร',
                                                                    'class' => 'form-control',
                                                                ]); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" id="Salary_BankNo"
                                                           value="<?php echo $empModel['Salary_BankNo']; ?>"
                                                           data-required="true" required="true"
                                                           placeholder="เลขบัญชีธนาคาร">
                                                </div>


                                                <div class="col-sm-4">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label for="promtpay_type" class="col-sm-4 control-label">พร้อมเพย์</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control" id="promtpay_type"
                                                                        name="promtpay_type">
                                                                    <option value="1">ใช้เบอร์โทร</option>
                                                                    <option value="2" selected>ใช้เลขบัตรประชาชน
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" name="promp_pay_no"
                                                           id="promp_pay_no" placeholder="หมายเลขพร้อมเพย์">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <form class="form-horizontal" id="frmAddEmpSalary" name="frmAddEmpSalary">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">ตำแหน่งเงินเดือน</h3>
                                                    </div>
                                                    <div align="right">
                                                        <button type="button" style="margin: 5px 5px;"
                                                                class="btn btn-primary <?php echo ($optional['viewOnly']) ? 'empDisplayNone' : ''; ?>"
                                                                onclick="showModalEmpPosition(this,'new')">เพิ่มข้อมูล
                                                        </button>
                                                    </div>

                                                    <div class="modal fade modal-wide" id="modalAddEmpPosition"
                                                         tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">


                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal" aria-hidden="true">
                                                                        &times;
                                                                    </button>
                                                                    <h4 id="myModalLabel" class="modal-title">
                                                                        เพิ่มข้อมูลตำแหน่งเงินเดือน</h4>
                                                                </div>
                                                                <input type="hidden"
                                                                       value="<?php echo Yii::$app->request->csrfToken; ?>"
                                                                       name="_csrf">
                                                                <div class="modal-body" width="100%"
                                                                     class="table table-bordered table-hover dataTable">
                                                                    <div class="form-group">
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">บริษัท</label>
                                                                        <div class="col-sm-3">
                                                                            <select class="form-control"
                                                                                    name="selectworking"
                                                                                    id="selectworking"
                                                                                    onchange="getCompanyForDepartment(this);"
                                                                                    ;>

                                                                                <option value="">เลือกบริษัท</option>
                                                                                <?php $working = ApiHr::getWorking_company();
                                                                                foreach ($working as $value) {
                                                                                    echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                                                } ?>
                                                                            </select>
                                                                            <span id="urlGetDepartment"
                                                                                  title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                                                                        </div>
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">ลักษณะการเปลี่ยนแปลง</label>
                                                                        <div class="col-sm-3">
                                                                            <?php
                                                                            $modelResultchange = ApiPersonal::configchangesalary();
                                                                            echo Html::dropDownList('selectsalarychangetype', null, $modelResultchange, [
                                                                                'id' => 'selectsalarychangetype',
                                                                                'prompt' => 'เลือกลักษณะการเปลี่ยนแปลง',
                                                                                'class' => 'form-control',
                                                                            ]);
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">แผนก</label>

                                                                        <div class="col-sm-3">
                                                                            <select class="form-control"
                                                                                    name="selectdepartment"
                                                                                    id="selectdepartment"
                                                                                    onchange="getDepartmentForSection(this);">
                                                                                <option value=""> เลือกแผนก</option>
                                                                            </select>
                                                                            <span id="urlGetSection"
                                                                                  title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                                                                        </div>

                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">ผังเงินเดือน</label>

                                                                        <div class="col-sm-3">
                                                                            <select class="form-control"
                                                                                    id="salarystepchartname"
                                                                                    onchange="selectSalarystepchartname(this)">
                                                                                <option>เลือกผังเงินเดือน</option>
                                                                                <?php $modelStepSalary = ApiPersonal::getsalarychart();
                                                                                foreach ($modelStepSalary as $valueStepSalary) {
                                                                                    ?>
                                                                                    <option value="<?php echo $valueStepSalary['SALARY_STEP_CHART_NAME']; ?>"><?php echo $valueStepSalary['SALARY_STEP_CHART_NAME']; ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">ฝ่าย</label>
                                                                        <div class="col-sm-3">
                                                                            <select class="form-control"
                                                                                    name="selectsection"
                                                                                    id="selectsection"
                                                                                    onchange="selectSection(this)">
                                                                                <option value="">เลือกฝ่าย</option>
                                                                            </select>
                                                                            <span id="urlGetSection"
                                                                                  title="<?php echo \yii\helpers\Url::toRoute('payroll/urlGetSection'); ?>"></span>
                                                                        </div>
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">ลงกระบอก</label>
                                                                        <div class="col-sm-3">
                                                                            <select class="form-control"
                                                                                    id="salarysteplevel"
                                                                                    onchange="selectSalarysteplevel(this)">
                                                                                <option>เลือกกระบอก</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">ตำแหน่งงาน</label>
                                                                        <div class="col-sm-3">
                                                                            <select class="form-control"
                                                                                    id="selectposition"
                                                                                    name="selectposition">

                                                                                <option value="">เลือกตำแหน่งงาน</option>


                                                                            </select>
                                                                        </div>
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">ขั้น</label>
                                                                        <div class="col-sm-3">
                                                                            <select class="form-control"
                                                                                    id="empsalarystep"
                                                                                    onchange="selectStep(this)">
                                                                                <option>เลือกขั้น</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">วันที่มีผล</label>
                                                                        <div class="col-sm-3">
                                                                            <input type="text" class="form-control"
                                                                                   id="start_effectdate" readonly>
                                                                        </div>
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">ขั้นที่เพิ่ม</label>
                                                                        <div class="col-sm-3">
                                                                            <input type="text" id='stapupnew'
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">วันที่สิ้นสุด</label>
                                                                        <div class="col-sm-3">
                                                                            <input type="text" class="form-control"
                                                                                   id="end_effectdate"
                                                                                   name="end_effectdate" readonly>
                                                                        </div>
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label">เงินเดือน</label>
                                                                        <div class="col-sm-3">
                                                                            <input type="text" class="form-control"
                                                                                   id="salarystepstartsalary" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label"></label>
                                                                        <div class="col-sm-3">

                                                                        </div>
                                                                        <label for="genderEmp"
                                                                               class="col-sm-2 control-label"></label>
                                                                        <div class="col-sm-3">

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <?php echo Html::hiddenInput('hide_activityedit_counter', null, ['id' => 'hide_activityedit_counter', 'class' => 'hide_activityedit_counter']); ?>
                                                                    <input type="button" class="btn btn-primary"
                                                                           id="btnSaveEmpPosition" data-reftr="[]"
                                                                           onclick="addEmpPosition(this)"
                                                                           value="บันทึกข้อมูล">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                </div>

                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->
                                                    <div class="box-body">
                                                        <table class="table table-hover" data-delchild="[]"
                                                               id="emp_position_tb">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 5%;">ลำดับ</th>
                                                                <th style="width: 5%;">จ่ายประกันสังคม</th>
                                                                <th style="width: 5%;">ตำแหน่งหลัก</th>
                                                                <th style="width: 5%;">จ่ายเงินเดือน</th>
                                                                <th style="width: 10%;">บริษัทสังกัด</th>
                                                                <th style="width: 10%;">รหัสตำแหน่ง</th>
                                                                <th style="width: 10%;">ตำแหน่ง</th>
                                                                <th style="width: 5%;">เริ่มใช้</th>
                                                                <th style="width: 5%;">สิ้นสุด</th>
                                                                <th style="width: 5%;">ผังเงินเดือน</th>
                                                                <th style="width: 5%;">กระบอก</th>
                                                                <th style="width: 5%;">ขั้น</th>
                                                                <th style="width: 5%;">เงินเดือน</th>
                                                                <th style="width: 5%;">สถานะ</th>
                                                                <th style="width: 5%;">การจัดการ</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="pull-right" style="padding-right: 15px;">
                    <?php
                    echo Html::button('<i class="fa fa-reply"></i> ยกเลิก',
                        [
                            'style' => (($optional['viewOnly'] == true) ? 'display:none' : ''),
                            'class' => 'btn btn-danger',
                            'id' => 'btnCancelClient',
                            'onclick' => 'window.history.back();',
                        ]);
                    echo '&nbsp;';
                    echo Html::button('<i class="fa fa-save"></i> บันทึก',
                        [
                            'style' => (($optional['viewOnly'] == true) ? 'display:none' : ''),
                            'class' => 'btn btn-primary',
                            'id' => 'btnSaveClient',
                            'onclick' => 'saveNewEmp(this, "' . $dataNo . '")'
                        ]);
                    echo Html::button('<i class="fa fa-save"></i> แก้ไข',
                        [
                            'style' => (($optional['viewOnly'] == false) ? 'display:none' : ''),
                            'class' => 'btn btn-primary',
                            'id' => 'btnEditClient',
                            'onclick' => 'editEmp("' . $dataNo . '","")'
                        ]);
                    ?>
                </div>
            </div>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row"></div>

        </div>
    </div>


    <!--     <div class="row">
        <div class="col-md-12">
           <div class="box box-info">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                              title="Collapse">
                          <i class="fa fa-minus"></i>
                      </button>
                    </div>
                </div>
            </div>
        </div>
    </div>   -->
    <span id="mode" title="<?php echo $mode; ?>"></span>
    <span id="dataNo" title="<?php echo ($empModel) ? $empModel['DataNo'] : ''; ?>"></span>
    <span id="IdCard"
          title="<?php echo ($empModel) ? \app\api\Utility::removeDashIDcard($empModel['ID_Card']) : ''; ?>"></span>
</section>

