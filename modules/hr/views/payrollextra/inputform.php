<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/6/2017 AD
 * Time: 11:56
 */


//AppAsset::register($this);
use app\bundle\AppAsset;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payrollextra/inputform.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/ot_manageot.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css


?>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li><a href="#">บันทึกจ่ายเงินระหว่างเดือน</a></li>
                <li class="active">บันทึกจ่ายเงินระหว่างเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2 col-md-offset-2">
                        <?php echo Html::dropDownList('for_month', null, [], [
                            'id' => 'activity_id',
                            'prompt' => 'เลือกเดือนที่จ่าย',
                            'class' => 'form-control',
                            'data-required' => 'true',
                        ]); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo Html::dropDownList('for_year', null, [], [
                            'id' => 'activity_id',
                            'prompt' => 'เลือกปี',
                            'class' => 'form-control',
                            'data-required' => 'true',
                        ]); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo Html::dropDownList('template_id', null, [], [
                            'id' => 'activity_id',
                            'prompt' => 'รายการจ่าย',
                            'class' => 'form-control',
                            'data-required' => 'true',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">บันทึกรายชื่อพนักงานที่ได้รับค่าตอบแทน</h3>
                    <div class="pull-right" style="margin-bottom: 10px;">
                        <?php
                        echo Html::button('<i class="fa fa-plus-circle"></i> เพิ่มแถว',
                            [
                                'class' => 'btn btn-success',
                                'id' => 'btnAddNewRow',
                            ]);
                        ?>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive" id="dvEmpList">

                                <?php
                                Pjax::begin(['id' => 'pjax_tbemployeeot']);

                                ?>
                                <table id="tbemployeeot" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 2%">ลำดับ</th>
                                        <th style="width: 20%">ชื่อ-สกุล</th>
                                        <th style="width: 15%">บริษัท</th>
                                        <th style="width: 10%">แผนก</th>
                                        <th style="width: 8%">จำนวน</th>
                                        <th style="width: 10%">หมายเหตุ</th>
                                        <th style="width: 2%">ลบ</th>
                                    </tr>
                                    <thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>
                                            <select class="form-control select2" style="width: 100%;" id="otEmployee"
                                                    name="otEmployee[]">
                                                <option>อดิเทพ ไชยสาร</option>
                                            </select>
                                        </td>
                                        <td><input type="text" id="txtCompany" name="txtCompany[]"
                                                   readonly="readonly" class="form-control" value="บริษัท นกเงือก โซลูชั่น จำกัด"></td>
                                        <td><input type="text" id="txtDepartment" name="txtDepartment[]"
                                                   readonly="readonly" class="form-control" value="Production"></td>
                                        <td><input type="text" id="txtMoney" name="txtMoney[]" class="form-control text-right" value="5,000"></td>
                                        <td><input type="text" id="txtRemark" name="txtRemark[]" class="form-control" value="ค่าทะลุเป้า"></td>
                                        <td><button type="button" id="del" onclick="deleteMe($(this));" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>
                                            <select class="form-control select2" style="width: 100%;" id="otEmployee"
                                                    name="otEmployee[]">
                                                <option> ณัฐวุฒิ จุมปา</option>
                                            </select>
                                        </td>
                                        <td><input type="text" id="txtCompany" name="txtCompany[]"
                                                   readonly="readonly" class="form-control" value="บริษัท นกเงือก โซลูชั่น จำกัด"></td>
                                        <td><input type="text" id="txtDepartment" name="txtDepartment[]"
                                                   readonly="readonly" class="form-control" value="Production"></td>
                                        <td><input type="text" id="txtMoney" name="txtMoney[]" class="form-control text-right" value="5,000"></td>
                                        <td><input type="text" id="txtRemark" name="txtRemark[]" class="form-control" value="ค่าทะลุเป้า"></td>
                                        <td><button type="button" id="del" onclick="deleteMe($(this));" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>
                                            <select class="form-control select2" style="width: 100%;" id="otEmployee"
                                                    name="otEmployee[]">
                                                <option>บัญญัติ ปัญญาโกน</option>
                                            </select>
                                        </td>
                                        <td><input type="text" id="txtCompany" name="txtCompany[]"
                                                   readonly="readonly" class="form-control" value="บริษัท นกเงือก โซลูชั่น จำกัด"></td>
                                        <td><input type="text" id="txtDepartment" name="txtDepartment[]"
                                                   readonly="readonly" class="form-control" value="Production"></td>
                                        <td><input type="text" id="txtMoney" name="txtMoney[]" class="form-control text-right" value="5,000"></td>
                                        <td><input type="text" id="txtRemark" name="txtRemark[]" class="form-control" value="ค่าทะลุเป้า"></td>
                                        <td><button type="button" id="del" onclick="deleteMe($(this));" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <?php
                                Pjax::end();  //end pjax_tbemployeeot

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center" style="margin-bottom: 40px !important;">
                    <?php
                    echo Html::button('<i class="fa fa-save"></i> บันทึกข้อมูล',
                        [
                            'class' => 'btn btn-primary',
                            'id' => 'btnSaveManageOT',
                        ]);
                    ?>
                </div>
                <div style="height: 20px;"></div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->