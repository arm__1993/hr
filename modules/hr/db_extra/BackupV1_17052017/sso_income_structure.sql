-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 07, 2017 at 08:22 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `sso_income_structure`
--

CREATE TABLE `sso_income_structure` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'รหัส',
  `income_floor` decimal(10,2) DEFAULT NULL COMMENT 'ช่วงเงินได้ เริ่มต้น',
  `income_ceil` decimal(10,2) DEFAULT NULL COMMENT 'ช่วงเงินได้ สิ้นสุด',
  `sso_rate` decimal(4,2) DEFAULT NULL COMMENT 'อัตราเงินประกันสังคม',
  `from_year` int(11) DEFAULT NULL COMMENT 'เริ่มใช้ตั้งแต่ปี',
  `to_year` int(11) DEFAULT NULL COMMENT 'จนถึงปี',
  `is_calculate_percent` tinyint(3) NOT NULL COMMENT 'ลักษณะการคำนวณ 1= เป็น%, 2=เป็นบาท',
  `remark` varchar(200) DEFAULT NULL COMMENT 'หมายเหตุ',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sso_income_structure`
--

INSERT INTO `sso_income_structure` (`id`, `income_floor`, `income_ceil`, `sso_rate`, `from_year`, `to_year`, `is_calculate_percent`, `remark`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, '1.00', '1650.00', '5.00', 2017, NULL, 1, '', 1, NULL, '2017-04-07 16:51:06', NULL, NULL),
(2, '1.00', '1650.00', '5.00', 2017, NULL, 1, '', 99, NULL, '2017-04-07 16:51:24', NULL, '2017-04-07 19:59:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sso_income_structure`
--
ALTER TABLE `sso_income_structure`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sso_income_structure`
--
ALTER TABLE `sso_income_structure`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส', AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
