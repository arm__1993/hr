<?php

namespace app\modules\wssale\models;

use Yii;

/**
 * This is the model class for table "CusInt_CusRecommend".
 *
 * @property integer $CusRec_TrcNo
 * @property string $IntNo_Ref
 * @property string $ConditionNo_Ref
 * @property string $Rec_Date
 * @property string $CusNo_Ref_Rec
 * @property string $Rec_CusNo_Ref
 * @property string $CusRec_Relation
 * @property string $RecType
 * @property string $Rec_Com
 * @property string $Remark
 * @property string $TrancDate
 */
class Cusintcusrecommend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CusInt_CusRecommend';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Remark'], 'string'],
            [['IntNo_Ref', 'ConditionNo_Ref', 'Rec_Date', 'Rec_Com', 'TrancDate'], 'string', 'max' => 50],
            [['CusNo_Ref_Rec', 'Rec_CusNo_Ref', 'RecType'], 'string', 'max' => 255],
            [['CusRec_Relation'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CusRec_TrcNo' => 'วิ่ง',
            'IntNo_Ref' => 'หมายเลขอ้างอิงการสนใจ',
            'ConditionNo_Ref' => 'เลขอ้างอิงเงื่อนไข',
            'Rec_Date' => 'วันที่แนะนำ',
            'CusNo_Ref_Rec' => 'ผู้แนะนำ',
            'Rec_CusNo_Ref' => 'ผู้ถูกแนะนำ',
            'CusRec_Relation' => 'ความสัมพันธ์',
            'RecType' => 'ลักษณะการแนะนำ EX แนะนำเฉยๆ แนะนำและค่าคอม FGF  FGFRE',
            'Rec_Com' => 'ค่าคอมแนะนำ',
            'Remark' => 'หมายเหตุ',
            'TrancDate' => 'วันที่เกิดรายการ',
        ];
    }

    public function searchcusintcusrecommendformobile($cusno)
    {

        $sql = "select CusInt_CusRecommend.CusRec_TrcNo,
                       CusInt_CusRecommend.Rec_Date , 
                       CusInt_CusRecommend.IntNo_Ref , 
                       CusInt_CusRecommend.Rec_CusNo_Ref ,
                       Rec_CusNo_Ref AS cusNoOfRec_CusNo_Ref,
                       (select ERP_new_cusdata.MAIN_CUS_GINFO.Cus_Name 
                            FROM ERP_new_cusdata.MAIN_CUS_GINFO 
                            WHERE ERP_new_cusdata.MAIN_CUS_GINFO.CusNo 
                                IN (cusNoOfRec_CusNo_Ref)) AS NameRec_CusNo_Ref ,
                       CusInt_CusRecommend.RecType ,
                       CusInt_CusRecommend.CusRec_Relation,
                       CusInt_CusRecommend.Remark
                FROM ERP_Easysale_icmba.CusInt_CusRecommend 
                    WHERE CusInt_CusRecommend.CusNo_Ref_Rec = :cusno";
        //echo ;
        $showdataeducusCusintcusrecommend = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
        return $showdataeducusCusintcusrecommend;
 
    }
}
