<?php

namespace app\modules\hr\controllers;
use app\modules\hr\apihr\ApiOT;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\OtConfigformular;
use app\modules\hr\models\OtConfigtimetable;
use Yii;
use app\modules\hr\models\OtActivity;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

use yii\helpers\VarDumper;
use app\modules\hr\controllers\MasterController;

//TODO : add data log
//TODO : add permission
//TODO : add check session with permission

class PayrollmasterController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;


    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }



    public function behaviors()
    {
        return [
            'verbs' => [
                'class' =>VerbFilter::className(),
                'actions' => [
                    'saveotactivity' =>['post'],
                    'editotactivity'=>['post'],
                    'deleteotactivity'=>['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {


        $OtActivitySearch = new OtActivity();
        $OtActivityProvider = $OtActivitySearch->search(Yii::$app->request->queryParams);

        $OtConfigTimeTableSearch = new OtConfigtimetable();
        $OtConfigTimeTableProvider = $OtConfigTimeTableSearch->search(Yii::$app->request->queryParams);


        $OtConfigFormular = OtConfigformular::findOne(1);
        if($OtConfigFormular===null) {
            $OtConfigFormular = new OtConfigformular();
        }

        $arrTemplateID = ApiOT::getArrayDeductTemplate("1");

        return $this->render('index',[
            'OtActivitySearch'=>$OtActivitySearch,
            'OtActivityProvider'=>$OtActivityProvider,
            'OtConfigFormular'=>$OtConfigFormular,
            'OtConfigTimeTableSearch'=>$OtConfigTimeTableSearch,
            'OtConfigTimeTableProvider'=>$OtConfigTimeTableProvider,
            'arrTemplateID'=>$arrTemplateID,
        ]);
    }




    public function actionSaveotactivity()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            if ($post['hide_activityedit'] != '')  //update data
            {
                $model = OtActivity::findOne($post['hide_activityedit']);
                $model->activity_name = $post['activity_name'];
                $model->status_active = $post['activity_status'];
                echo $model->save();
            }
            else
            {
                $model = new OtActivity();
                $model->activity_name = $post['activity_name'];
                $model->status_active = $post['activity_status'];
                echo $model->save();
            }
        }
    }

    public function actionEditotactivity()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = OtActivity::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeleteotactivity()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = OtActivity::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            echo  $model->save();
        }
    }



    public function actionSaveprofileroute()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            if ($post['hide_timetableedit'] != '')  //update data
            {
                $model = OtConfigtimetable::findOne($post['hide_timetableedit']);
                $model->profile_name = $post['profile_name'];
                $model->time_start = $post['time_start'];
                $model->time_end = $post['time_end'];
                $model->budget = $post['budget'];
                $model->status_active = $post['timetable_status'];
                echo $model->save();
            }
            else
            {
                $model = new OtConfigtimetable();
                $model->profile_name = $post['profile_name'];
                $model->time_start = $post['time_start'];
                $model->time_end = $post['time_end'];
                $model->budget = $post['budget'];
                $model->status_active = $post['timetable_status'];
                echo $model->save();
            }
        }
    }

    public function actionEditprofileroute()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = OtConfigtimetable::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeleteprofileroute()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = OtConfigtimetable::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            echo  $model->save();
        }
    }



    public function actionSaveotconfig()
    {

        $model = OtConfigformular::findOne(1);
        if($model===null) {
            $model = new OtConfigformular();
        }

        //VarDumper::dump($model->attributeLabels());
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            echo 1;
            //Yii::$app->response->format = trim(Response::FORMAT_JSON);
            //return $result;
        }else{
            $error = \yii\widgets\ActiveForm::validate($model);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            print_r($error);
        }
    }


}
