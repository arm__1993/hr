<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiRepair;
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$groupby = $session->get('groupby');
$select_company = $session->get('selected_company');
$select_startmonths = $session->get('selected_months');
$select_endmonths = $session->get('selected_year');
$select_year = $session->get('select_year');


?>

    <div class="box">
        <h3><i class="fa fa-fw fa-file-text"></i>รายงานเวลารับแจ้งซ่อมเฉลี่ยต่อคัน รายเดือน</h3>
        <h3> <?php  $namecompany = ApiReport::getNameCompanyPDF($select_company) ;
            echo  $namecompany['0']['name']; ?>
        </h3>
        <h3>เดือน <?php echo DateTime::convertMonth($select_startmonths);?>
            ถึง <?php  echo DateTime::convertMonth($select_endmonths);?>
            ปี <?php echo $select_year['0'];?>
        </h3>
    </div>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- AREA TABLE -->
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table width="100%" bgcolor="#777" ALIGN="CENTER" id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                        <thead>
                                        <tr role="row">
                                            <th bgcolor="#eee" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                เดือน
                                            </th>
                                            <th bgcolor="#eee" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                เวลาเฉลี่ยต่อคัน (นาที)
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php foreach ($groupby as $index => $value) {
                                            if($index == 1){
                                                $index = "มกราคม";
                                            }elseif ($index == 2) {
                                                $index = "กุมภาพันธ์";
                                            }elseif ($index == 3) {
                                                $index = "มีนาคม";
                                            }elseif ($index == 4) {
                                                $index = "เมษายน";
                                            }elseif ($index == 5) {
                                                $index = "พฤษภาคม";
                                            }elseif ($index == 6) {
                                                $index = "มิถุนายน";
                                            }elseif ($index == 7) {
                                                $index = "กรกฎาคม";
                                            }elseif ($index == 8) {
                                                $index = "สิงหาคม";
                                            }elseif ($index == 9) {
                                                $index = "กันยายน";
                                            }elseif ($index == 10) {
                                                $index = "ตุลาคม";
                                            }elseif ($index == 11) {
                                                $index = "พฤศจิกายน";
                                            }elseif ($index == 12) {
                                                $index = "ธันวาคม";
                                            }
                                            ?>
                                            <tr role="row">
                                                <td bgcolor="#f0f8ff"><?php echo $index; ?></td>
                                                <td bgcolor="#f0f8ff">
                                                    <?php echo (count($value)>0) ? Helper::displayDecimal(array_sum($value)/count($value)) : 0;?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- col-sm-12 -->
                            </div>
                            <!-- row -->
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-warning-->
            <!-- AREA TABLE -->
        </div>
        </div>
    </section>
<?php
session_start();
session_destroy();
?>