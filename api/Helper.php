<?php

/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 9/12/2016 AD
 * Time: 11:56 AM
 */
namespace app\api;
class Helper
{

    public static function calculateSunday($StartDate, $EndDate)
    {
        // $StartDate = "2016-09-01";
        //$EndDate = "2016-09-30";

        $WorkDay = $Sunday = 0;
        $_starttime = strtotime($StartDate);
        $_endtime = strtotime($EndDate);
        $_arrSunday = $_arrWorkday = [];
        while ($_starttime <= $_endtime) {
            $Day = date("w", $_starttime);
            if ($Day == 0) { // 0 = Sunday
                $Sunday++;
                array_push($_arrSunday, date('Y-m-d', $_starttime));
            } else {
                $WorkDay++;
                array_push($_arrWorkday, date('Y-m-d', $_starttime));
            }
            $_starttime += 86400;
        }

        return [
            'total_sunday' => $Sunday,
            'total_workday' => $WorkDay,
            'array_sunday' => $_arrSunday,
            'array_workday' => $_arrWorkday,
        ];
    }

    public static function displayDecimal($number, $float = 2)
    {
        return number_format($number, $float);
    }


    public static function printImg()
    {

        for($i=1;$i<=10;$i++) {
            $front = imagecreatefrompng('images/wshr/template_empcard_front.png');
            $back = imagecreatefrompng('images/wshr/template_empcard_front.png');
            $emp = imagecreatefromjpeg('images/wshr/3560500575223_m.jpg');
            imagecopy($front, $emp, 200, 400, 0, 0, 200, 200);
            header('Content-type: image/png');
            imagepng($front, 'upload/personal/aeedy'.$i.'.png');
            imagedestroy($front);
            imagedestroy($emp);
        }
    }
}