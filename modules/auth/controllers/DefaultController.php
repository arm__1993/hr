<?php

namespace app\modules\auth\controllers;

//session_start();

use yii\web\Controller;
use Yii;
use yii\web\Session;
use app\modules\hr\models\RelationPosition;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Position;
use app\modules\hr\apihr\ApiHr;
use app\api\Pepper;
use app\api\Login;
use app\api\DBConnect;
use yii\bootstrap\Alert;
use yii\db\Command;

/**
* Default controller for the `auth` module
*/
class DefaultController extends Controller
{
    
    public $layout = 'authlayout';
    public $errCode = [
    '205' => 'Session Timeout and Reset content',
    '403' => 'Username OR Password incorrect',
    //'404'=>'Username not found.',
    ];
    
    /**
    * Renders the index view for the module
    * @return string
    */
    
    public function actionIndex()
    {
        $msg = Yii::$app->request->get();
        if ($msg) {
            //print_r($msg);
        }
        return $this->render('index', ['msg' => $msg[1]['msg']]);
        
    }
    
    
    //TODO  : check user & login and store session in this function
    public function actionAuthenticate()
    {
        //    $session = Yii::$app->session;
        //    $idcardlogin = $_SESSION['USER_ID'];----
        //    $session->set('USER_ID', $idcardlogin);
        $func = Yii::$app->request->get();
        // echo "<pre>";
        // print_r($func);
        // echo "</pre>";
        // exit();
        switch ($func['func']) {
            case 'submit':
                
                //print_r($_POST);
                $postValue = Yii::$app->request->post();
                $login = new Login();
                $checklogin = $login->checklogin($postValue);
                if($checklogin==1){
                    return $this->redirect(array('/auth/dashboad/index'), 302);
                }else{
                    $MSG = $checklogin;
                    $link = "index.php";
                    return $this->redirect(array('/auth/default/index', ['msg' => $MSG]), 302);
                }
            break;
    }
    exit();
    
    // return $this->redirect(array('/hr/default'), 302);
}



public function actionCheckposition()
{
    $myusername = Yii::$app->request->get();
    // print_r($myusername);
    //        print_r($myusername);
    //        exit;
    $modelIdcard = ApiHr::getdatabyusername($myusername['username']);
    $model = RelationPosition::find()
    ->select('relation_position.position_id,
    relation_position.status,
    position.PositionCode,
    position.Name')
    ->from('relation_position')
    ->InnerJoin('position', 'position.id = relation_position.position_id')
    ->where(['relation_position.id_card' => $modelIdcard['ID_Card']])
    ->andWhere('relation_position.status != "99" AND position.status != "99"')
    ->asArray()
    ->all();
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return $model;
}


public function redirectToLogin($code)
{
    Yii::$app->getSession()->setFlash('loginfail', [
    'body' => $this->errCode[$code],
    'options' => ['class' => 'alert-error']
    ]);
    return $this->redirect(array('default/index'), 302);
}

public function actionLogout()
{
    $session = Yii::$app->session;
    $session->open();
    $session->destroy();
    return $this->redirect(array('index'), 302);
}

}