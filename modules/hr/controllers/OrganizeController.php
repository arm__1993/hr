<?php

namespace app\modules\hr\controllers;

use yii;
use app\api\Utility;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\models\Department;
use app\modules\hr\models\Section;
use app\modules\hr\models\Position;
use app\modules\hr\models\PositionLevel;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelpers;
use yii\helpers\Json;
use app\modules\hr\models\OrganicChart;
use app\modules\hr\models\Organizechart;
use app\modules\hr\apihr\ApiOrganizeChart;
use app\modules\hr\apihr\ApiOrganize;
use app\modules\hr\models\Command;
use app\modules\hr\models\RelationPosition;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\Empdata;
use app\modules\hr\controllers\MasterController;
use app\modules\hr\models\WorkType;
use yii\db\Expression;
use kartik\mpdf\Pdf;

class OrganizeController extends MasterController
{
    public $layout = 'hrlayout';
    public $CommendNode = [];
    public $idcardLogin;
    // public $idcardLogin;

    // public function init()
    // {
    //     $session = Yii::$app->session;
    //      $session->open();
    //      if ($session->isActive){
    //          if (!$session->has('USER_ACCOUNT')) {
    //              return $this->redirect(array('/login/index'),302);
    //          }
    //      }
    // }


    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' =>VerbFilter::className(),
    //             'actions' => [
    //                 'companyinsert' =>['post'],
    //                 //'saveprovince'=>['post'],
    //                 //'editprovince'=>['post'],
    //             ]
    //         ]
    //     ]
    // }


    /**
     * function init() check session active or session login, if not redirect to login page
     * @return \yii\web\Response
     */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function actionIndex()
    {
        $objSearch = Yii::$app->request->get();
        $objSearchi = Json_decode($objSearch[objSearch], true);
        $modelPosition = new Position();
        $dataProvider = ApiOrganize::searchGridpositioncount($objSearchi['company'], $objSearchi['department'], $objSearchi['section']);

        // Push data to navigate
        $arrSelector = [];
        $arrSelector[] = ($objSearchi['company']) ? $objSearchi['company'] : 0;
        $arrSelector[] = ($objSearchi['department']) ? $objSearchi['department'] : 0;
        $arrSelector[] = ($objSearchi['section']) ? $objSearchi['section'] : 0;

        $arrExistPosition = ApiOrganize::getExistPosition();  //get Exist Position

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelPosition,
            'arrExistPosition' => $arrExistPosition,
            'strSelector' => join(',', $arrSelector),
            'mode' => 'search',
            'commpay'=> $objSearchi['company'],
            'department'=>$objSearchi['department'],
            'section'=>$objSearchi['section'],
        ]);
    }

    public function actionIndexpdf()
    {
        $objSearch = Yii::$app->request->get();
        $objSearchi = Json_decode($objSearch[objSearch], true);
        $modelPosition = new Position();
        $dataProvider = ApiOrganize::searchGridpositioncountpdf($objSearchi['company'], $objSearchi['department'], $objSearchi['section']);

        // Push data to navigate
        $arrSelector = [];
        $arrSelector[] = ($objSearchi['company']) ? $objSearchi['company'] : 0;
        $arrSelector[] = ($objSearchi['department']) ? $objSearchi['department'] : 0;
        $arrSelector[] = ($objSearchi['section']) ? $objSearchi['section'] : 0;

        $arrExistPosition = ApiOrganize::getExistPosition();  //get Exist Position



        $mpdf = new \Mpdf\Mpdf();
        $mpdf->AddPage('L', '', '', '', '',10,10,10,10, 10, 10);
       // $mpdf->AddPage('P', '', '', '', '', 10, 10, 10, 10, 10, 10);
        $mpdf->WriteHTML($this->renderPartial('index_pdf', [
            'dataProvider' => $dataProvider,

        ]));
        $mpdf->Output('รายงานตำแหน่งงานว่าง.pdf', 'I');
        exit;
    }

    public function actionCheckdepartment()
    {
        $postValue = Yii::$app->request->post();
        $data = Department::find()
            ->where(['=', 'code_name', $postValue['data']])
            ->andWhere(['=', 'company', $postValue['company_id']])
            ->asArray()
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }

    public function actionCheckworkingcompany()
    {
        $postValue = Yii::$app->request->post();
        $data = Workingcompany::find()
            ->where(['=', 'code_name', $postValue['data']])
            ->asArray()
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }

    public function actionChecksection()
    {
        $postValue = Yii::$app->request->post();
        $data = Section::find()
            ->where(['=', 'code_name', $postValue['data']])
            ->andWhere(['=', 'department', $postValue['department']])
            ->asArray()
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }

    public function actionCompany()
    {
        // $data = Workingcompany::find()
        //     ->where('status <> 99 ');
        //   $dataProvider = new ActiveDataProvider([
        //     'query' => $data,
        //     'pagination' => [
        //         'pageSize' => 10,
        //     ]
        // ]);

        $modelSearch = new Workingcompany();
        $dataProvider = $modelSearch->companySearch(Yii::$app->request->queryparams);
        $data = ApiOrganize::CheckEditCompany();
        return $this->render('company', [
            'companyUse' => $data,
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
        ]);
    }

    public function actionCompanyinsert()
    {
        $workingcompany = new Workingcompany();
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['companyInsert'], true);

            $obj['Tel'] = Utility::removeDashIDcard($obj['Tel']);
            $obj['Fax'] = Utility::removeDashIDcard($obj['Fax']);
            $obj['inv_number'] = Utility::removeDashIDcard($obj['inv_number']);
            $obj['soc_acc_number'] = Utility::removeDashIDcard($obj['soc_acc_number']);
            $workingcompany->code_name = $obj['code_name'];
            $workingcompany->codeName_tax = ($obj['codeName_tax']) ? $obj['codeName_tax'] : '';  //อักษรนำหน้าใบกำกับภาษี
            $workingcompany->short_name = $obj['short_name'];
            $workingcompany->name = $obj['name'];
            $workingcompany->name_eng = $obj['name_eng'];
            $workingcompany->address = $obj['address'];
            $workingcompany->Tel = $obj['Tel'];
            $workingcompany->Fax = $obj['Fax'];
            $workingcompany->soc_acc_number = $obj['soc_acc_number'];
            $workingcompany->status = 1;
            $workingcompany->inv_number = $obj['inv_number'];
            $workingcompany->business_number = ($obj['business_number']) ? $obj['business_number'] : '';// หมายเลขทะเบียนการค้า
            $workingcompany->numberbranch = ($obj['numberbranch']) ? $obj['numberbranch'] : ''; // เลขที่สาขา(จากระบบiosของตรีเพชร)
            $workingcompany->dealercode = $obj['dealercode'];
            $workingcompany->Code = ($obj['Code']) ? $obj['Code'] : ''; //รหัสสาขาที่ใช้กับservice
            $workingcompany->company_type = $obj['company_type'];
            $workingcompany->branch_number = ($obj['branch_number']) ? $obj['branch_number'] : ''; // รหัสสาขาที่จะเอาไปโชว์ต่อท้าย
            $workingcompany->branch_comment = ($obj['branch_comment']) ? $obj['branch_comment'] : '';// คอมเม้นชื่อสาขาที่จะเอาไปโชว์ต่อท้าย
            $workingcompany->branch_salesman_cd = ($obj['branch_salesman_cd']) ? $obj['branch_salesman_cd'] : '';  //รหัสพนักงานขายประจำสาขา
            $workingcompany->BranchName_TIS = ($obj['BranchName_TIS']) ? $obj['BranchName_TIS'] : ''; // ชื่อสาขาจากทางตรีเพชร
            $workingcompany->show_in_report = ($obj['show_in_report']) ? $obj['show_in_report'] : '';
            $workingcompany->images_pdf_home = ($obj['images_pdf_home']) ? $obj['images_pdf_home'] : '';
            $workingcompany->images_pdf_isuzu = ($obj['images_pdf_isuzu']) ? $obj['images_pdf_isuzu'] : '';
            $statusinsert = $workingcompany->save();
            return $statusinsert;
        }
    }

    public function actionOrgchart()
    {
        $arrCompany = ApiHr::getWorking_company();
        $arrEmp = ApiHr::getempdataall();
        return $this->render('orgchart', [

            'arrCompany' => $arrCompany,
            'arrEmp' => $arrEmp,
        ]);
    }

    public function actionCompaneditbyid()
    {
        $workingcompany = new Workingcompany();
        if (Yii::$app->request->isAjax) {
            $idCompany = Yii::$app->request->get('idCompany');
            //print_r($getValueById);
            $data = Workingcompany::findOne($idCompany);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $data;

        }
    }

    public function actionCompanupdetbyid()
    {

        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['companyInsert'], true);
            $workingcompany = Workingcompany::findOne((int)$obj['id']);
            $obj['Tel'] = Utility::removeDashIDcard($obj['Tel']);
            $obj['Fax'] = Utility::removeDashIDcard($obj['Fax']);
            $obj['inv_number'] = Utility::removeDashIDcard($obj['inv_number']);
            $obj['soc_acc_number'] = Utility::removeDashIDcard($obj['soc_acc_number']);
            $workingcompany->code_name = $obj['code_name'];
            $workingcompany->codeName_tax = $obj['codeName_tax'];
            $workingcompany->short_name = $obj['short_name'];
            $workingcompany->name = $obj['name'];
            $workingcompany->name_eng = $obj['name_eng'];
            $workingcompany->address = $obj['address'];
            $workingcompany->Tel = $obj['Tel'];
            $workingcompany->Fax = $obj['Fax'];
            $workingcompany->soc_acc_number = $obj['soc_acc_number'];
            // $workingcompany->status = $obj['status'];
            $workingcompany->inv_number = $obj['inv_number'];
            $workingcompany->business_number = $obj['business_number'];
            $workingcompany->numberbranch = $obj['numberbranch'];
            $workingcompany->dealercode = $obj['dealercode'];
            $workingcompany->Code = $obj['Code'];
            $workingcompany->company_type = $obj['company_type'];
            $workingcompany->branch_number = $obj['branch_number'];
            $workingcompany->branch_comment = $obj['branch_comment'];
            $workingcompany->branch_salesman_cd = $obj['branch_salesman_cd'];
            $workingcompany->BranchName_TIS = $obj['BranchName_TIS'];
            $workingcompany->show_in_report = $obj['show_in_report'];
            $workingcompany->images_pdf_home = $obj['images_pdf_home'];
            $workingcompany->images_pdf_isuzu = $obj['images_pdf_isuzu'];

            if ($workingcompany->validate()) {
                $statusinsert = $workingcompany->update();
            } else {
                $errors = $workingcompany->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }

            return $statusinsert;
        }
    }

    public function actionCompandeletebyid()
    {
        $postValue = Yii::$app->request->post();
        $obj = null;
        $obj = $postValue['id'];
        $workingcompany = Department::findOne((int)$obj);
        $statusinsert = $workingcompany->delete();
        return $statusinsert;
    }


    public function actionCompandeletebyid1()
    {
        $postValue = Yii::$app->request->post();
        $obj = null;
        $obj = $postValue['id'];
        $workingcompany = Workingcompany::findOne((int)$obj);
        $statusinsert = $workingcompany->delete();
        return $statusinsert;
    }

    public function actionDepartment()
    {
        $postValue = Yii::$app->request->get();
        if (isset($postValue['id']) && $postValue['id'] > 0) {
            $datacompany = Workingcompany::find()
                ->where('status <> 99 ')
                ->andWhere(['id' => $postValue['id']])
                ->asArray()
                ->all();

            $modelSearch = new Department();
            $dataProvider = $modelSearch->SearchModel($postValue, Yii::$app->request->queryparams);
            $datacheck = ApiOrganize::CheckEditDepartment($postValue['id']);
            return $this->render('department', [
                'datacheck' => $datacheck,
                'dataProvider_department' => $dataProvider,
                'modelSearch' => $modelSearch,
                'datacompany' => $datacompany
            ]);
        } else {
            echo 'Invalid Input !!!';
            return '';
        }
    }

    public function actionAdddepartment()
    {
        $department = new Department();
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['deparmentInsert'], true);
            $department->code_name = $obj['code_name'];
            $department->company = $obj['company'];
            $department->name = $obj['name'];
            $department->status = '1';

            if ($department->validate()) {
                $statusinsert = $department->save();
            } else {
                $errors = $department->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }
            return $statusinsert;
        }
    }

    public function actionDepartmentbyid()
    {
        $department = new Department();
        if (Yii::$app->request->isAjax) {
            $iddepartment = Yii::$app->request->get('iddepartment');
            $data = Department::findOne($iddepartment);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $data;

        }
    }

    public function actionDepartmentbyidupdate()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['deparmentInsert'], true);

            $department = Department::findOne((int)$obj['id']);
            $department->code_name = $obj['code_name'];
            $department->company = $obj['company'];
            $department->name = $obj['name'];
            $department->status = '1';
            if ($department->validate()) {
                $statusinsert = $department->update();
            } else {
                $errors = $department->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }
            return $statusinsert;
        }
    }

    public function actionSectionbyiddepartment()
    {
        $getValue = Yii::$app->request->get();
        $company = $getValue['obj']['company'];
        $iddepartment = $getValue['obj']['iddepartment'];
        return $this->redirect(['organize/section', 'company' => $company, 'iddepartment' => $iddepartment]);
    }

    public function actionSection()
    {
        $Value = Yii::$app->request->get();
        $datacompany = Workingcompany::find()
            ->where('status <> 99 ')
            ->andWhere(['id' => $Value['company']])
            ->asArray()
            ->all();
        $datadepartmant = Department::find()
            ->where('status <> 99 ')
            ->andWhere(['id' => $Value['iddepartment']])
            ->asArray()
            ->all();
        $data = Section::find()
            ->select([
                    'section.id as sectionid',
                    'section.code_name as sectioncode_name',
                    'section.department as sectiondepartment',
                    'section.name as sectionname',
                    'section.status as sectionstatus'
                ]
            )
            ->from('section')
            ->where('section.department = ' . $Value['iddepartment'] . ' ')
            ->andWhere('status <> 99');
        $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        return $this->render('section', [
            'dataProvider' => $dataProvider,
            'datacompany' => $datacompany,
            'datadepartmant' => $datadepartmant,
            'iddepartment' => $Value['iddepartment']
        ]);
    }

    public function actionSectionbyid()
    {
        if (Yii::$app->request->isAjax) {
            $idsection = Yii::$app->request->get('idsection');
            $data = Section::findOne($idsection);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $data;

        }
    }

    public function actionSectionbyidupdate()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['sectionInsert'], true);

            $department = Section::findOne((int)$obj['id']);
            $department->attributes = $obj;
            $statusinsert = $department->update();
            return $statusinsert;
        }
    }

    public function actionSectioninsert()
    {
        $workingcompany = new Section();
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['sectionInsert'], true);
            $workingcompany->attributes = $obj;
            $statusinsert = $workingcompany->save();
            return $statusinsert;
        }
    }

    public function actionSectiondeletebyid()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $obj = null;
            $obj = json_decode($postValue['id'], true);
            $department = Section::findOne((int)$obj);
            $department->status = '99';
            $statusinsert = $department->update();
            return $statusinsert;
        }
    }

    public function actionDivision()
    {
        return $this->render('division');
    }


    public function actionWorktype()
    {
        $data = WorkType::find()
            ->where('status <> 99 ');
        $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $arrExistWorktype = ApiOrganize::getExistWorktype(); //get exist worktype;
        return $this->render('worktype', [
            'dataProvider' => $dataProvider,
            'arrExistWorktype' => $arrExistWorktype,
        ]);
    }

    public function actionDeleteposition()
    {
        $Value = Yii::$app->request->post('data');
        $model = Position::findOne((int)$Value);
        $model->Status = 99;
        $statussave = $model->save();
        if (!$statussave) {
            echo 'error';
        }
        return $statussave;
    }
    // public function actionPositoncode()
    // {
    //     $Value = Yii::$app->request->post('data');
    //     $model = Position::find()->where('PositionCode')->asArray()->one();

    // }

    public function actionLevel()
    {
        $data = PositionLevel::find()
            ->where('level_status <> 99 ');
        $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $arrExistLevel = ApiOrganize::getExistLevel(); //get exist level
        return $this->render('level', [
            'dataProvider' => $dataProvider,
            'arrExistLevel' => $arrExistLevel,
        ]);
    }

    public function actionPosition()
    {
        $modelPosition = Position::find()->where('id<0');
        $dataProvider = new ActiveDataProvider([
            'query' => $modelPosition,
            'sort' => ['defaultOrder' => 'PositionCode asc'],
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        return $this->render('position', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelPosition,
        ]);
    }


    public function actionCompanychart()
    {
        return $this->render('companychart');
    }

    public function actionOrgchartview()
    {
        $arrCompany = ApiHr::getWorking_company();
        $arrEmp = ApiHr::getempdataall();

        return $this->render('orgchartview', [

            'arrCompany' => $arrCompany,
            'arrEmp' => $arrEmp,
        ]);
    }

    public function actionGetchart()
    {
        $Value = Yii::$app->request->get();
        $obj_Chart = new ApiOrganizeChart();
        $modelChart = new Organizechart();
        $arrayChart = [];
        $arrayIDpodition = [];
        $companyID;
        $count = 1;
        $countConpany = $countDepartment = $countPasition = $countSection = 0;
        $modelPosition = ApiOrganizeChart::getPosition($Value['company']);
        foreach ($modelPosition as $item) {
            foreach ($item as $items) {
                array_push($arrayIDpodition, $items);
            }
        }
        //$idCommendH = ApiOrganizeChart::getCommand($arrayIDpodition);
        //$modelCommandH = ApiOrganizeChart::getCommandbyid($idCommendH);
        $CommendNodeFun = ApiOrganizeChart::getCommandnode($arrayIDpodition);
        // $this->CommendNode = $CommendNodeFun;
        // $this->setarrar($modelCommandH);
        // for($i = 0 ;$i<count($this->CommendNode);$i++){
        //     $id = $obj_Chart->formatJson($this->CommendNode[$idCommendH]);
        //     $idCommendH = $id;
        // }
        // $arrayChart = $obj_Chart->getchart();
        $arrJson = $obj_Chart->formatJson($CommendNodeFun);
        $arrayChart = $obj_Chart->getchart();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $arrayChart;
    }

    public function actionGetchartnode()
    {
        $Value = Yii::$app->request->get();
        $obj_Chart = new ApiOrganizeChart();
        $arrOrg = ApiOrganizeChart::getOrgNode($Value['company']);
        $arrayChart = $obj_Chart->formatJsonNode($arrOrg);
        $data = [];
        $arraydatanode = [];
        $datanode = [];


        foreach ($arrOrg as $key => $value) {
            $arraydatanode[$value['Posision_id']] = $value;
            $arraydatanode[$value['Posision_id']]['name'] = $value['Name'];
            $arraydatanode[$value['Posision_id']]['title'] = $value['Emp_name'];
        }
        foreach ($arraydatanode as $key => $value) {

            $headnode[] = ApiOrganizeChart::graphHead($arraydatanode, $value['parent'], $data);
        }
       $headnode[0]['parent'] = 0;


        array_push($datanode, $headnode[0]);
        $nodegrahp = ApiOrganizeChart::graphHeadbyNode($datanode, $arraydatanode, $data);

        foreach ($nodegrahp as $key => $value) {
            array_push($datanode, $value);
        }


        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;




        return $datanode;
    }

    public function actionClicknodeedit()
    {
        $Value = Yii::$app->request->post('node');
        echo "<pre>";
        print_r($Value);
        echo "</pre>";
        exit();
    }

    public function actionGetchartnodeview()
    {
        $arraydatanode = [];
        $Value = Yii::$app->request->get();
        $obj_Chart = new ApiOrganizeChart();
        $arrOrg = ApiOrganizeChart::getOrgNode($Value['company']);




        foreach ($arrOrg as $key => $value) {
            $arraydatanode[$value['Posision_id']] = $value;
            $arraydatanode[$value['Posision_id']]['name'] = $value['Name'];
            $arraydatanode[$value['Posision_id']]['title'] = $value['Emp_name'];
            $arraydatanode[$value['Posision_id']]['className'] = 'recruit-enablednotnull';
            $arraydatanode[$value['Posision_id']]['classfa'] = '';


//            if(empty($value['Name'])&&(empty($value['Emp_name']))&&(empty($value['positionCode']))){
//                $arraydatanode[$value['className']] = 'recruit-enablednot';
//            }

            // print_r($value);
        }


        // exit();
        $data = [];
        foreach ($arraydatanode as $key => $value) {
            $headnode[] = ApiOrganizeChart::graphHead($arraydatanode, $value['parent'], $data);
        }
        foreach ($headnode as $value) {
            if (is_array($value)) {
                $data[$value['Posision_id']] = $value;
            }
        }

        foreach ($data as $key => $item) {
            $node[] = ApiOrganizeChart::graphNode($arraydatanode, $key, $item);
        }


        $datanode = [
            'name' => 'Hornbill Group',
            'title' => 'ประธานกรรมการ',
            'className' => 'product-dept',
            'children' => $node,


        ];




        // $arrayChart = $obj_Chart->formatJsonNode($arrOrg);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $datanode;
    }

    public static function GetchartnodeviewsetSESSION($id_emp)
    {
        $arraydatanode = [];


        $obj_Chart = new ApiOrganizeChart();
        $arrOrg = ApiOrganizeChart::getOrgNodeSESSION($id_emp);


        foreach ($arrOrg as $key => $value) {
            $arraydatanode[$value['Posision_id']] = $value;
            $arraydatanode[$value['Posision_id']]['position_name'] = $value['Name'];
            $arraydatanode[$value['Posision_id']]['emp_name'] = $value['Emp_name'];
            $arraydatanode[$value['Posision_id']]['position_id'] = $value['Posision_id'];
            $arraydatanode[$value['Posision_id']]['id_card'] = $value['Emp_IDCard'];
            $arraydatanode[$value['Posision_id']]['Leader'] = $value['parent'];





        }



        $data = [];
        foreach ($arraydatanode as $key => $value) {
            $headnode[] = ApiOrganizeChart::graphHeadSESSION($arraydatanode, $value['Leader'], $data);
        }
        foreach ($headnode as $value) {
            if (is_array($value)) {
                $data[$value['position_id']] = $value;
            }
        }

        foreach ($data as $key => $item) {
            $node[] = ApiOrganizeChart::graphNodeSESSION($arraydatanode, $key, $item);
        }


        $datanode = [
            'name' => 'Hornbill Group',
            'title' => 'ประธานกรรมการ',
            'team' => $node,
        ];


//        echo "<pre>";
//        print_r($datanode);
//        echo "</pre>";

        // $arrayChart = $obj_Chart->formatJsonNode($arrOrg);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $datanode;
    }

    public function actionRecruitingcon()
    {
        $postValue = Yii::$app->request->post();
        $model = Position::findOne(['id' => $postValue['recruitings']]);
        $model->recruit_status = $postValue['reffser'];
        $model->position_Edit_date = new Expression('NOW()');
        $model->save();

    }

    public function actionRemovecheckemp()
    {
        $Value = Yii::$app->request->get('data');
        $emp = Empdata::find()->select('ID_Card')
            ->where('DataNo =' . $Value['Emp_id'])
            ->asArray()
            ->all();
        if (count($emp) == 0) {
            return true;
        } else {
            $SALARY = \app\modules\hr\models\Empsalary::find()->where("EMP_SALARY_ID_CARD = " . $emp[0]['ID_Card'] . " AND status != '99'")->asArray()->all();
            echo "<pre>";
            print_r($SALARY);
            echo "</pre>";
            exit();
        }
        return false;
    }

    public function actionGetautoposition()
    {
        $Value = Yii::$app->request->get();
        $obj_Chart = new ApiOrganizeChart();
        $arraydata = ApiOrganizeChart::getPositionName($Value['data']);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $arraydata;
    }

    public function actionAddnode()
    {
        $Value = Yii::$app->request->post('data');
        //$obj_Chart = new ApiOrganizeChart();

        $idorglast = ApiOrganizeChart::addOrgNodeNew($Value);




        return $idorglast;
    }

    public function actionDeletenodenew()
    {
        $Value = Yii::$app->request->post('id');
        $obj_Chart = new ApiOrganizeChart();
        $idorglast = ApiOrganizeChart::deletenodeNew($Value);


        return $idorglast;
    }

    public function actionDeleteemp()
    {
        $Value = Yii::$app->request->post('id');
        $obj_Chart = new ApiOrganizeChart();
        $arrOrg = ApiOrganizeChart::removeOrgNode($Value);
    }

    public function setarrar($param)
    {
        foreach ($param as $key => $value) {
            $this->CommendNode[$key] = $value;
            if ($value[0]['Leader'] != '') {
                $arr = $this->getparenthight($value[0]['Leader']);
                $this->setarrar($arr);
            }
        }
    }

    public function getparenthight($value)
    {
        return ApiOrganizeChart::getCommandbyHead($value);
    }

    public function actionSavenode()
    {
        $postValue = Yii::$app->request->post('obj');
        $obj = Json::decode($postValue, true);
        return ApiOrganizeChart::SaveNode($obj);
    }

    public function actionSavenodeorg()
    {
        $postValue = Yii::$app->request->post('obj');
        $company = Yii::$app->request->post('compay');
        $obj = Json::decode($postValue, true);
        $satusd = ApiOrganizeChart::DeleteNodeOrg($company);
        ApiOrganizeChart::SaveNodeOrg($obj['nodeDataArray'], $company);
    }

    public function actionEmptyposition()
    {
        return $this->render('emptyposition');
    }

    public function actionEmptypositiondata()
    {
        $data = ApiOrganize::PositionsVacant();
        return $this->render('emptypositiondata',
            ['data' => $data]
        );;
    }


    public function actionSaveworktype()
    {
        $worktype = new WorkType();
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['worktype'], true);
            if ($obj['id'] == 0 && $obj['check'] == 0) {
                $worktype->attributes = $obj;
                if ($worktype->validate()) {
                    $statusSave = $worktype->save();
                    return $statusSave;
                } else {
                    throw new \Exception("model can't validator");
                }

            } else {
                $worktype = WorkType::findOne((int)$obj['id']);
                $worktype->attributes = $obj;
                if ($worktype->validate()) {
                    $statusSave = $worktype->update();
                    return $statusSave;
                } else {
                    //echo "model can't validater";
                    throw new \Exception("model can't validator");
                }
                //return  $statusSave;
            }

        }
    }


    public function actionSavelevel()
    {
        $positionLevel = new PositionLevel();
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['level'], true);
            if ($obj['id'] == 0 && $obj['check'] == 0) {
                $positionLevel->attributes = $obj;
                if ($positionLevel->validate()) {
                    $statusSave = $positionLevel->save();
                } else {
                    echo "model can't validater";
                }
                return $statusSave;
            } else {
                $section = PositionLevel::findOne((int)$obj['id']);
                $section->attributes = $obj;
                if ($section->validate()) {
                    $statusSave = $section->update();
                } else {
                    echo "model can't validater";
                }
                return $statusSave;
            }

        }
    }

    public function actionSaveposition()
    {
        $position = new Position();
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = $postValue['positionInpus'];



            if ($obj['id'] == 0 && $obj['check'] == 0) {
                $obj['addToCommand'] = '0';
                $obj['position_Edit_date'] = date('Y-m-d H:i:s');
                $obj['Status'] = 1;
                $obj['recruit_status'] =1;
                $position->attributes = $obj;
                if ($position->validate()) {
                    $statusSave = $position->save();
                } else {
                    $errors = $position->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
                return $statusSave;
            } else {
                $section = Position::findOne((int)$obj['id']);
                $section->attributes = $obj;
                if ($section->validate()) {
                    $statusSave = $section->update();
                } else {
                    $errors = $position->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
                return $statusSave;
            }

        }
    }

    public function actionLeveldeletebyid()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            //$obj = null;
            //$obj = json_decode($postValue['id'], true);

            $ID = $postValue['id'];
            $level = PositionLevel::findOne($ID);
            //$level = PositionLevel::find()->where(['level_id' => $ID])->one();
            $level->level_status = 99;
            if ($level->update()) {
                return 1;
            } else {
                $errors = $level->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }
        }
    }


    public function actionWorktypedeletebyid()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $obj = null;
            $obj = json_decode($postValue['id'], true);
            $model = WorkType::findOne((int)$obj);
            $model->status = 99;
            $statusSave = $model->update();
            return $statusSave;
        }
    }


    public function actionEditworktype()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = WorkType::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }


    public function actionEditlevel()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = PositionLevel::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionEditposition()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = Position::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionGetdepartmentbyid()
    {
        $id = Yii::$app->request->get('id');
        $department = ApiOrganize::getDepartment($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $department;
    }

    public function actionGetsectionbyid()
    {
        $id = Yii::$app->request->get('id');
        $section = ApiOrganize::getSection($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $section;
    }

    public function actionGetlevel()
    {
        $Level = ApiOrganize::getPositionLevel();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $Level;
    }

    public function actionGeteditlevelbyid()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            if (($model = PositionLevel::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionSearchposition()
    {
        $objSearch = Yii::$app->request->get();
        $objSearchi = Json_decode($objSearch[objSearch], true);
        $modelPosition = new Position();
        $dataProvider = ApiOrganize::searchGridposition($objSearchi['company'], $objSearchi['department'], $objSearchi['section']);

        // Push data to navigate
        $arrSelector = [];
        $arrSelector[] = ($objSearchi['company']) ? $objSearchi['company'] : 0;
        $arrSelector[] = ($objSearchi['department']) ? $objSearchi['department'] : 0;
        $arrSelector[] = ($objSearchi['section']) ? $objSearchi['section'] : 0;

        $arrExistPosition = ApiOrganize::getExistPosition();  //get Exist Position
        return $this->render('position', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelPosition,
            'arrExistPosition' => $arrExistPosition,
            'strSelector' => join(',', $arrSelector),
            'mode' => 'search',
        ]);
    }

    public function actionGetruning()
    {
        $code = Yii::$app->request->get('data');
        $run = ApiOrganize::getruning($code);
        //$strrun = (strlen($run['runNumber']) == '' || is_null(strlen($run['runNumber']))) ? '01' : str_pad((int)$run['runNumber'] + 1, 2, "0", STR_PAD_LEFT);


        return $run;
    }

    public function actionGetworktype()
    {
        $dataWorkType = ApiOrganize::getWorkType();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $dataWorkType;
    }


    public function actionTesta()
    {
        $a = ApiOrganize::getExistPosition();
        echo count($a);
        echo '<pre>';
        print_r($a);
        echo '</pre>';
    }

    public function actionRecursive()
    {


        return $this->render('recursive');
    }

}