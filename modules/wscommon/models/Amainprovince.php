<?php

namespace app\modules\wscommon\models;

use Yii;

class Amainprovince extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
       /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }
    public static function tableName()
    {
        return 'aMain_province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['province_code', 'province_name'], 'required'],
            [['geo_id'], 'integer'],
            [['province_code'], 'string', 'max' => 2],
            [['province_name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'province_id' => 'Province ID',
            'province_code' => 'Province Code',
            'province_name' => 'Province Name',
            'geo_id' => 'Geo ID',
        ];
    }
    public function searchprovincebygeoid($keyword) // param
    {
        $province = Yii::$app->dbERP_maincusdata
       ->createCommand("select * from aMain_province  where geo_id = :keyword ORDER BY province_name ASC")
       ->bindParam(':keyword',$keyword)->queryAll();
        return $province;

    }

}
