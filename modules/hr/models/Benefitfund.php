<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "BENEFIT_FUND".
 *
 * @property integer $BENEFIT_ID
 * @property string $BENEFIT_EMP_ID
 * @property string $BENEFIT_SAVING_DATE
 * @property string $BENEFIT_DETAIL
 * @property string $BENEFIT_AMOUNT
 * @property string $BENEFIT_TOTAL_AMOUNT
 * @property string $BENEFIT_TRANSACTION_BY
 * @property string $BENEFIT_TRANSACTION_DATE
 * @property string $BENEFIT_FUND_STATUS
 * @property string $BENEFIT_FUND_REMARK
 */
class Benefitfund extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BENEFIT_FUND';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BENEFIT_EMP_ID', 'BENEFIT_SAVING_DATE', 'BENEFIT_DETAIL', 'BENEFIT_AMOUNT', 'BENEFIT_TOTAL_AMOUNT', 'BENEFIT_TRANSACTION_BY', 'BENEFIT_FUND_STATUS', 'BENEFIT_FUND_REMARK'], 'required'],
            [['BENEFIT_AMOUNT', 'BENEFIT_TOTAL_AMOUNT'], 'number'],
            [['BENEFIT_TRANSACTION_DATE'], 'safe'],
            [['BENEFIT_EMP_ID'], 'string', 'max' => 13],
            [['BENEFIT_SAVING_DATE', 'BENEFIT_DETAIL'], 'string', 'max' => 100],
            [['BENEFIT_TRANSACTION_BY'], 'string', 'max' => 200],
            [['BENEFIT_FUND_STATUS'], 'string', 'max' => 2],
            [['BENEFIT_FUND_REMARK'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'BENEFIT_ID' => 'Benefit  ID',
            'BENEFIT_EMP_ID' => 'Benefit  Emp  ID',
            'BENEFIT_SAVING_DATE' => 'Benefit  Saving  Date',
            'BENEFIT_DETAIL' => 'Benefit  Detail',
            'BENEFIT_AMOUNT' => 'Benefit  Amount',
            'BENEFIT_TOTAL_AMOUNT' => 'Benefit  Total  Amount',
            'BENEFIT_TRANSACTION_BY' => 'Benefit  Transaction  By',
            'BENEFIT_TRANSACTION_DATE' => 'Benefit  Transaction  Date',
            'BENEFIT_FUND_STATUS' => 'Benefit  Fund  Status',
            'BENEFIT_FUND_REMARK' => 'Benefit  Fund  Remark',
        ];
    }
}
