<?php

namespace app\modules\hr\controllers;

use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\Btitle;
use app\modules\hr\models\Bbloodgroup;
use app\modules\hr\models\Bstatuspersonal;
use app\modules\hr\models\BStatuseducation;
use app\modules\hr\models\BLeveleducation;
use app\modules\hr\models\Bdegreeeducation;
use app\modules\hr\models\BInstitutioneducation;

// use app\modules\hr\models\OtConfigformular;
// use app\modules\hr\models\OtConfigtimetable;
// use app\modules\hr\models\OtActivity;

use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use Yii;
use app\modules\hr\controllers\MasterController;

class ConfigpersonalgeneralController extends MasterController
{
    public $layout = 'hrlayout';

    public $idcardLogin;

    //  public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' =>VerbFilter::className(),
    //             'actions' => [
    //                 'saveadddeductactivity' =>['post'],
    //                 'updateadddeduct'=>['post'],
    //                 'deleteaddeduct'=>['post'],
    //             ],
    //         ],
    //     ];
    // }


    /**
     * function init() check session active or session login, if not redirect to login page
     * @return \yii\web\Response
     */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }




    
    public function actionIndex()
    {
    //    $param = Yii::$app->request->queryParams;

    
        $BtitleSearch = new Btitle();
        $BtitleProvider = $BtitleSearch->search(Yii::$app->request->queryParams);

        $queryBbloodgroup = Bbloodgroup::find()->where('status_active != :del', [':del' => Yii::$app->params['DELETE_STATUS']]);
        $BbloodgroupProvider = new ActiveDataProvider(
                [
                    'query'=>$queryBbloodgroup,
                    'pagination' => ['pageSize' => 10,],
                ]);

        $BstatuspersonalSearch = new Bstatuspersonal();
        $BstatuspersonalProvider = $BstatuspersonalSearch->search(Yii::$app->request->queryParams);

        $BStatuseducationSearch = new BStatuseducation();
        $BStatuseducationProvider = $BStatuseducationSearch->search(Yii::$app->request->queryParams);

        $BLeveleducationSearch = new BLeveleducation();
        $BLeveleducationProvider = $BLeveleducationSearch->search(Yii::$app->request->queryParams);

        $BdegreeeducationSearch = new Bdegreeeducation();
        $BdegreeeducationProvider = $BdegreeeducationSearch->search(Yii::$app->request->queryParams);
        
        $BInstitutioneducationSearch = new BInstitutioneducation();
        $BInstitutionProvider = $BInstitutioneducationSearch->search(Yii::$app->request->queryParams);
        

        return $this->render('index',[
            'BtitleProvider'=>$BtitleProvider,
            'BtitleSearch' => $BtitleSearch,
            'BbloodgroupProvider'=>$BbloodgroupProvider,
            'BstatuspersonalProvider'=>$BstatuspersonalProvider,
            'BstatuspersonalSearch' => $BstatuspersonalSearch,
            'BStatuseducationProvider' => $BStatuseducationProvider,
            'BStatuseducationSearch' => $BStatuseducationSearch,
            'BLeveleducationProvider' => $BLeveleducationProvider,
            'BLeveleducationSearch' => $BLeveleducationSearch,
            'BdegreeeducationProvider' => $BdegreeeducationProvider,
            'BdegreeeducationSearch' => $BdegreeeducationSearch,
            'BInstitutionProvider' => $BInstitutionProvider,
            'BInstitutioneducationSearch' => $BInstitutioneducationSearch,
            ]);
    }

        ///===== คำนำหน้า  =====///
    public function actionDeletebtitel()
    {
         if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = Btitle::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

     public function actionSavebtitel()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login= $this->idcardLogin;
            if ($post['hide_activitybtitel'] != '') {
                    $model = Btitle::findOne($post['hide_activitybtitel']);
                    $model->title_name_th=$post['title_name_th'];
                    $model->title_name_en=$post['title_name_en'];
                    $model->status_active=$post['status_active'];
                    $model->gender=$post['gender'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                    }
            }else{
                    $model = new Btitle();
                    $model->title_name_th=$post['title_name_th'];
                    $model->title_name_en=$post['title_name_en'];
                    $model->status_active=$post['status_active'];
                    $model->gender=$post['gender'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    //$model->tax_section_id=$post['tax_section_id'];
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                       
                    }
            }
        }

    }

    public function actionUpdatebtitel()
    {
         if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = Btitle::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
                ///===== เลือด  =====///
    public function actionSavebloodgroup(){

          if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login=$this->idcardLogin;

            if ($post['hide_activitybtitel'] != '') {
                    $model = Bbloodgroup::findOne($post['hide_activitybtitel']);
                    $model->bloodgroup=$post['bloodgroup'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                    }
            }else{
                    $model = new Bbloodgroup();
                    $model->bloodgroup=$post['bloodgroup'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    //$model->tax_section_id=$post['tax_section_id'];
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                       
                    }
            }

           
        }

    }

    public function actionDeletebloodgroup()
    {
         if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = Bbloodgroup::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

    public function actionUpdatebloodgroup()
    {
         if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = Bbloodgroup::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

                ///===== สถานะสมรส  =====///
    public function actionSavestatuspersonal(){

          if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login=$this->idcardLogin;

            if ($post['hide_activitybtitel'] != '') {
                    $model = Bstatuspersonal::findOne($post['hide_activitybtitel']);
                    $model->status_personal=$post['status_personal'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                    }
            }else{
                    $model = new Bstatuspersonal();
                    $model->status_personal=$post['status_personal'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    //$model->tax_section_id=$post['tax_section_id'];
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                       
                    }
            }

           
        }

    }

    public function actionDeletestatuspersonal()
    {
         if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = Bstatuspersonal::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

    public function actionUpdatestatuspersonal()
    {
         if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = Bstatuspersonal::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }


                    ///===== สถานะการศึกษา  =====///
    public function actionSavestatuseducation(){

          if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login=$this->idcardLogin;

            if ($post['hide_activitybtitel'] != '') {
                    $model = BStatuseducation::findOne($post['hide_activitybtitel']);
                    $model->status_education=$post['status_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                    }
            }else{
                    $model = new BStatuseducation();
                    $model->status_education=$post['status_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    //$model->tax_section_id=$post['tax_section_id'];
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                       
                    }
            }

           
        }

    }

    public function actionDeletestatuseducation()
    {
         if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = BStatuseducation::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

    public function actionUpdatestatuseducation()
    {
         if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = BStatuseducation::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

             ///===== ระดับการศึกษา  =====///
    public function actionSaveleveleducation(){

          if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login=$this->idcardLogin;

            if ($post['hide_activitybtitel'] != '') {
                    $model = BLeveleducation::findOne($post['hide_activitybtitel']);
                    $model->level_education=$post['level_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                    }
            }else{
                    $model = new BLeveleducation();
                    $model->level_education=$post['level_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    //$model->tax_section_id=$post['tax_section_id'];
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                       
                    }
            }

           
        }

    }

    public function actionDeleteleveleducation()
    {
         if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = BLeveleducation::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

    public function actionUpdateleveleducation()
    {
         if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = BLeveleducation::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }


                ///===== วุฒิการศึกษา  =====///
    public function actionSavedegreeeducation(){

          if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login=$this->idcardLogin;

            if ($post['hide_activitybtitel'] != '') {
                    $model = Bdegreeeducation::findOne($post['hide_activitybtitel']);
                    $model->degree_education=$post['degree_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                    }
            }else{
                    $model = new Bdegreeeducation();
                    $model->degree_education=$post['degree_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    //$model->tax_section_id=$post['tax_section_id'];
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                       
                    }
            }

           
        }

    }

    public function actionDeletedegreeeeducation()
    {
         if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = Bdegreeeducation::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

    public function actionUpdatedegreeeducation()
    {
         if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = Bdegreeeducation::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }



                ///===== วุฒิการศึกษา  =====///
    public function actionSaveinstitutioneducation(){

          if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            //print_r($post);
            $login=$this->idcardLogin;

            if ($post['hide_activitybtitel'] != '') {
                    $model = BInstitutioneducation::findOne($post['hide_activitybtitel']);
                    $model->institution_education=$post['institution_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                    }
            }else{
                    $model = new BInstitutioneducation();
                    $model->institution_education=$post['institution_education'];
                    $model->status_active=$post['status_active'];
                    $model->createby_user=$login;
                    $model->create_datetime=new Expression('NOW()');
                    $model->updatteby_user=$login;
                    $model->update_datetime=new Expression('NOW()');
                    //$model->tax_section_id=$post['tax_section_id'];
                    $model->save();
                    if($model->save() !== false){
                        echo true;
                    }else{
                        $errors = $model->errors;
                        echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                       
                    }
            }

           
        }

    }

    public function actionDeleteinstitutioneducation()
    {
         if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = BInstitutioneducation::findOne(['id'=>$post['id']]);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

    public function actionUpdateinstitutioneducation()
    {
         if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = BInstitutioneducation::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

}
