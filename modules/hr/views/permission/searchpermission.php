<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:47
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPermission;
$this->registerCssFile(Yii::$app->request->BaseUrl . "/js/hr/bootstrap-treeview/dist/bootstrap-treeview.min.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/bootstrap-treeview/dist/bootstrap-treeview.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/permission.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/permissionSearch.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/js/jquery.sumoselect.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/permission/sumoselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$selectProgram = ApiHr::selectProgram();

?>
<section class="content" id='sectionstap1'>
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดการสิทธิ์</a>
                </li>
                <li class="active">ค้นหาสิทธ์การใช้โปรแกรม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <form  onsubmit="return SearchStap1()" data-toggle="validator" id='seartFrom1'>
                <div class='row' style="text-align:center"> 
                    <div class="form-group">
                            <label for="numberPassportEmp"  class="col-sm-5 control-label " style="text-align:right"> ประเภทการค้นหา</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="typesearch" id="programid" required>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1">โปรแกรม</option>
                                            <option value="2">บุคคล</option>
                                    </select>
                                </div>
                        </div>
                </div>
                <br>
                <div class="row" style="text-align:center">
                    <input type="submit" class="btn btn-primary btn-md" value="ค้นหา">
                    <input type="reset" class="btn btn-warning btn-md" value="ล้าง">
                </div>
            </form>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
<section class="content" id='sectionstap2' >
    <div class="box box-danger">
     <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">การจัดการสิทธิ์</a>
            </li>
            <li class="active">ค้นหาสิทธ์การใช้โปรแกรม</li>
        </ul><!-- /.breadcrumb -->
        <!-- /section:basics/content.searchbox -->
    </div>
     <div class="box-body">
        <form  onsubmit="return searchCompany()" data-toggle="validator" id='seartFrom'>
            <br>
            <center>
                <div class="row container" id='row'>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp"  class="col-sm-4 control-label">โปรแกม</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="selectworking" id="selectworking" onchange="getProgramforMenu(this);" required>
                                            <option value="">เลือกบริษัทโปรแกม</option>
                                                <?php 
                                                    foreach ($selectProgram as  $value) {
                                                            echo '<option value="' . $value['id']. '">' . $value['name'] . '</option>';
                                            } ?>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp"  class="col-sm-3 control-label">เมนู</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="menu" id="menu" onchange="$('#submenu').html('');getSubMenu(this);" required>
                                        <option value=""> เลือกเมนู </option>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <div id='submenu'></div>
                </div>
            </center>
            <br>
            <table id="example" class="display" width="100%"></table>
            <div class="row" style="text-align:center">
                <input type="submit" class="btn btn-primary btn-md" value="ค้นหา">
                <input type="reset" class="btn btn-warning btn-md" value="ล้าง">
            </div>
        </form>
    </div>
</section>

<section class="content" id='sectionstap3'>
    <div class="box box-danger">
     <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">การจัดการสิทธิ์</a>
            </li>
            <li class="active">ค้นหาสิทธ์การใช้โปรแกรม</li>
        </ul><!-- /.breadcrumb -->
        <!-- /section:basics/content.searchbox -->
    </div>
     <div class="box-body">
        <form  onsubmit="return searchEmp()" data-toggle="validator" id='seartFrom'>
            <div class="col-md-3">
            
            </div>
            <br>
            <div class="row" style="text-align:center">
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    &nbsp;
                </div>
                 <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp"  class="col-sm-3 control-label">ชื่อ</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="empName" id="empName" onchange='getPositionByEmp(this)' required>
                                   <option value="">เลือกรายชื่อพนักงาน</option>
                                    <?php 
                                        $empdata=ApiHr::getempdataall();
                                        foreach ($empdata as  $value) {
                                                echo '<option value="' . $value['value']. '">' . $value['label'] . '</option>';
                                } ?>
                                </select>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp"  class="col-sm-3 control-label">ตำแหน่ง</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="selectPosition" id="selectPosition" onchange="getDepartmentForSection(this);" required>
                                    <option value=""> เลือกตำแหน่ง </option>
                                </select>
                            </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row" style="text-align:center">
                <input type="submit" class="btn btn-primary btn-md" value="ค้นหา">
                <input type="reset" class="btn btn-warning btn-md" value="ล้าง">
            </div>
        </form>
    </div>
    <div id='dataEmp'></div>
</section>