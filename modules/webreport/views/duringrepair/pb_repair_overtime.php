<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 20/10/2559
 * รายงานระยะเวลาช่วงสถานะใบงานซ่อมที่เกินเวลา
 * Time: 11:16 6.1
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/jquery.datetimepicker.full.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

 $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });

});
JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);
?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

    <h3><i class="fa fa-fw fa-file-text"></i>รายงานสถานะใบงานซ่อมที่เกินเวลานัดหมายส่งมอบ</h3>

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- ./box-header -->

        <div class="box-body">
            <div class="row">
                <form method="post" action="overtime">
                    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12 text-center">

                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key => $value) {
                                    $sel = ($selected_company == $key) ? ' selected="selected" ' : '';
                                    echo '<option value="' . $key . '" ' . $sel . '>' . $value . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>ช่วงวันที่</label>
                        &nbsp;
                        <div class="btn-group">
                            <div class="input-group" style="width: 250px">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" name="reservation" id="reservation" value="<?php echo $selected_daterange;?>"
                                       data-provide="datepicker" data-date-language="th">
                            </div>
                        </div>
                        &nbsp;&nbsp;
                        <label>เลือกประเภทรถ</label>
                        &nbsp;
                        <div class="btn-group">
                            <input type="radio" name="technician" id="technician1"
                                   value="1" <?php echo ($selected_technician != 2) ? 'checked' : ''; ?>>
                            ช่างรถเล็ก

                            <input type="radio" name="technician" id="technician2"
                                   value="2" <?php echo ($selected_technician == 2) ? 'checked' : ''; ?>> ช่างรถใหญ่&nbsp;
                        </div>


                        <br>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-fw fa-search"></i>ค้นหา
                            </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                            <a href="<?php echo SITE_URL; ?>/duringrepair/btn_reportpb">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                                </button>
                            </a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <!-- ./box-body -->

        <?php
        if (count($workOverTime) > 0) {
            ?>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- AREA TABLE -->
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title"></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                            data-toggle="tooltip"
                                            title="Collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                    <div class="row">
                                        <div class="col-sm-6"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="example1" class="table table-bordered table-hover" role="grid"
                                                   aria-describedby="example2_info">
                                                <thead>
                                                <tr role="row" class="odd">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ลำดับ
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        เลขที่ใบงาน
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        วันที่เข้ารับบริการ
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ทะเบียน
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        สถานะใบงาน
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        เวลาที่เกิน (นาที)
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        SA
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ช่างซ่อม
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ประเภทใบงานซ่อม
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $item = 1;
                                                foreach ($workOverTime as $value) {
                                                    ?>
                                                    <tr role="row" class="odd">
                                                        <td class="sorting_1"><?php echo $item; ?></td>
                                                        <td><?php echo $value['id']; ?></td>
                                                        <td><?php echo DateTime::CalendarDate($value['guard_time']); ?></td>
                                                        <td><?php echo $value['register']; ?></td>
                                                        <td><?php echo $value['status']; ?></td>
                                                        <td><?php $TotalMoreTime = $value['TotalMoreTime'];
                                                            $arr_Time_total = DateTime::calculateTimeElapsed($TotalMoreTime);
                                                    echo   DateTime::getFormatTime($arr_Time_total['H'],$arr_Time_total['m'],$arr_Time_total['s']);
                                                         ?>
                                                        </td>
                                                        <td><?php echo $value['sa_name']; ?></td>
                                                        <td><?php echo $value['technician_name']; ?></td>
                                                        <td><?php echo $value['typeRepair']['0']; ?></td>
                                                    </tr>
                                                    <?php $item++;
                                                } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div><!--example2_wrapper-->
                            </div><!--box-body-->
                        </div><!--box box-success-->
                        <!-- AREA TABLE -->
                    </div>
                </div>
            </section>
            <?php
        }
        ?>

    </div>
    <!-- /.box-->

<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>