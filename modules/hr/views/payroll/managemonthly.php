<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.config.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hr-config.js?t='.time());
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/MonthPicker.min.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/managemonthly.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);




//$this->registerCssFile(Yii::$app->request->baseUrl."/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->baseUrl."/css/hr/MonthPicker.min.css");
//$this->registerCssFile(Yii::$app->request->BaseUrl."/fonts/01thaifontcss.css");

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>ค้นหาและแก้ไขข้อมูลการจ่าย</li>
                <li class="active">ค้นหาและแก้ไขรายบุคคล</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
       <div class="box-body">
              <form class="form-horizontal">
                <div class="box box-info">
                            <div id="showseach">
                                <div class="box-header with-border">
                                    <h3 class="box-title">ข้อมูลรายการ</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                            <div class="col-md-4">

                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="id_emp"  class="col-sm-3 control-label">ชื่อ</label>
                                                        <div class="col-sm-9">
                                                        <!--<input type="text" name="seachnameemp" id="seachnameemp" class="form-control">-->
                                                        <input type="hidden" id="id_emp">
                                                            <select class="form-control select2" name="seachnameemp" id="seachnameemp" required >
                                                                <?php $empdata=ApiHr::getempdataall();
                                                                foreach($empdata as $value){
                                                                    ?>
                                                                    <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>

                                            <button type="button" class="btn  btn-primary btn-sm" id="seachdatalist"><i class="fa fa-search"></i> ค้นหา</button>
                                            <button type="reset" class="btn  btn-danger btn-sm"><i class="fa fa-reply"></i> ล้างข้อมูล</button>
                                        </div>
                                    </div>
                                </div>
                                <div id="hidedetail">

                                </div>
                            </div>
                            <div id="showMaster"></div>
                            <div id="showtable">
                                <!--<img src="../../images/global/ajax-loader.gif" id="img_loader" style="display: none;">-->
                                <!--<div class="box box-info">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">แสดงรายการ</h3>
                                    </div>
                                        <div class="box-body">
                                    
                                            <table class="table table-bordered table-hover dataTable" >
                                                <thead>
                                                    <tr>
                                                        <th width="5%"><center>ลำดับ</center></th>
                                                        <th width="15%"><center>รูปแบบการจ่าย</center></th>
                                                        <th width="20%"><center>ชื่อรายการ</center></th>
                                                        <th width="15%"><center>เดือนที่จ่าย</center></th>
                                                        <th width="15%"><center>เดือนที่สิ้นสุด</center></th>
                                                        <th width="15%"><center>จำนวน</center></th>
                                                        <th width="15%"><center>จัดการ</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tblticket">
                                                </tbody>
                                            </table>
                                        </div>
                                </div>   -->
                            </div>
                </div>
            </form>



        </div>
       </div>
    <!-- /.box -->
</section><!-- /.content -->