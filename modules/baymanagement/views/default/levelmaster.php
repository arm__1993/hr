<?php
/**
 * Created by PhpStorm.
 * User: watcharaphan
 * Date: 1/6/2018 AD
 * Time: 14:06
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use yii\helpers\Url;
use app\modules\baymanagement\models\EholeTake;
use app\modules\baymanagement\models\EholeTakeLevel;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use app\api\Utility;
use app\modules\baymanagement\models\ErepairGroup;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);



$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/baylayout.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/levelmaster.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/baymanagement/armbox.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$today = date('d/m/Y');


/*echo '<pre>';
print_r($showeholetakeindex);
echo '</pre>';*/


?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">จัดการแผนที่โรงซ่อม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">


                    <!--modal start-->
                    <div class="row">
                        <div class="col-md-11"></div>

                        <div class="col-md-1">
                            <?php
                            Modal::begin([
                                'id' => 'modalfrmAddjobmaster',
                                'header' => '<strong>จัดการlevel </strong>',
                                'toggleButton' => [
                                    'id' => 'btnAddNewjobmaster',
                                    'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                    'class' => 'btn btn-success'
                                ],
                                'closeButton' => [
                                    'label' => '<i class="fa fa-close"></i>',
                                    //'class' => 'close pull-right',
                                    'class' => 'btn btn-success btn-sm pull-right'
                                ],
                                'size' => 'modal-md',
                            ]);
                            ?>
                            <form role="form" id="frmAddjobmaster">
                                <div class="form-group">
                                    <div class="box-body">
                                        <div class="col-md-12">

                                        </div>
                                    </div>

                                    <div class="box-body">
                                        <label>ชื่อlevel <span>*</span></label>
                                        <input id="technician_level_name" name="technician_level_name"
                                               data-required="true"
                                               class="form-control model_name" type="text"
                                               placeholder="ชื่อlevel">
                                    </div>
                                    <!-- /.box-body -->
                                   <!-- <div class="box-body">
                                        <label>ชื่อlevel <span>*</span></label>
                                        <input id="level_code" name="level_code"
                                               data-required="true"
                                               class="form-control model_name" type="text"
                                               placeholder="ชื่อlevel">
                                    </div>-->

                                    <div class="box-body">
                                        <label>สถานะ</label>
                                        <br/>
                                        <input type="hidden" class="status_active"
                                               id="status_active" value="1"
                                               name="status_active">แสดง

                                    </div>


                                    <div class="box-body">
                                        <?php echo Html::hiddenInput('hide_activityedit_jobmaster', null, ['id' => 'hide_activityedit_jobmaster', 'class' => 'hide_activityedit_jobmaster']); ?>
                                        <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivityjobmaster']); ?>
                                    </div>
                                </div>
                            </form>
                            <?php
                            Modal::end();
                            ?>
                        </div>

                    </div>
                    <!--end start-->

                    <!--table start-->
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            Pjax::begin(['id' => 'pjax_tb_jobmaster']);
                            echo GridView::widget([
                                'dataProvider' =>  $showelevelmasterindex,
                                'filterModel' => $showelevelmaster,
                                'columns' => [
                                    [
                                        'header' => 'ที่',
                                        'class' => 'yii\grid\SerialColumn',
                                        'headerOptions' => ['width' => '23'],
                                    ],
                                    [
                                        'attribute' => 'technician_level_name',
                                        'header' => 'ชื่อlevel',
                                        'value' => 'technician_level_name',
                                        'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                    ],
                                    [

                                        //'attribute' => 'create_by',
                                        'label' => 'บันทึกโดยผู้ใช้',
                                        // 'value' => 'emp_idcard',
                                        'value' => function ($data) {
                                            $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                            return $modelResultemp['0']['Fullname'];
                                        },
                                        'label' => 'บันทึกโดยผู้ใช้',
                                        //'format' => 'raw',
                                        'filter' => false,
                                        'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                    ],
                                    [

                                        'label' => 'สถานะ',
                                        // 'value' => 'record_status',
                                        'format' => 'image',
                                        'value' => function ($data) {
                                            return Utility::dispActive($data->status_active);
                                        },
                                        'filter' => false,
                                        'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                    ],




                                    [

                                        'class' => 'yii\grid\ActionColumn',
                                        'header' => 'จัดการข้อมูลบริษัท',
                                        'template' => '{update}  &nbsp;&nbsp; {delete}',
                                        'buttons' => [
                                            'update' => function ($url, $data) {
                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                    'title' => 'แก้ไข',
                                                    'onclick' => '(function($event) {
                                                                        updatelevelmaster(' . $data->id . ',1);
                                                                })();'
                                                ]);
                                            },

                                            'delete' => function ($url, $data) {
                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                    'title' => 'ลบ',
                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->technician_level_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletejobmaster(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                ]);
                                            },
                                        ],
                                        'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                    ],


                                ],

                            ]);
                            Pjax::end();
                            ?>
                        </div>
                    </div>
                    <!--table end-->


                </div>
            </div>
        </div>


</section>