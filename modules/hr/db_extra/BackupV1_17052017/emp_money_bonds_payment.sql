-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 27, 2017 at 10:10 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.27-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_money_bonds_payment`
--

CREATE TABLE `emp_money_bonds_payment` (
  `id` int(11) NOT NULL,
  `money_bonds_id` int(11) DEFAULT NULL,
  `emp_data_id` int(11) DEFAULT NULL,
  `emp_idcard` varchar(13) DEFAULT NULL COMMENT 'เลขบัตรประชาชน',
  `bonds_date` date DEFAULT NULL COMMENT 'วันที่หักเงินสะสม',
  `bonds_list` varchar(250) DEFAULT NULL COMMENT 'รายละเอียด',
  `bonds_bring_forward` float DEFAULT NULL COMMENT 'ยอดยกมา',
  `bonds_amount` float DEFAULT NULL COMMENT 'จำนวนเงินที่จ่าย',
  `bonds_balance` float DEFAULT NULL COMMENT 'ยอดคงเหลือ',
  `create_date` datetime DEFAULT NULL COMMENT 'วันที่ทำรายการ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_money_bonds_payment`
--
ALTER TABLE `emp_money_bonds_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `money_bonds_id` (`money_bonds_id`),
  ADD KEY `emp_data_id` (`emp_data_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_money_bonds_payment`
--
ALTER TABLE `emp_money_bonds_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
