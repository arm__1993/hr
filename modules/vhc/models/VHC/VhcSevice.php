<?php

namespace app\modules\vhc\models\VHC;

use Yii;

/**
 * This is the model class for table "vhc_sevice".
 *
 * @property integer $id
 * @property integer $ahistory_id
 * @property integer $vhc_model_id
 * @property string $listNmae
 * @property string $comment
 * @property string $sumprice
 * @property string $sumvat
 * @property string $suminvatprice
 * @property integer $vhc_master_id
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcSevice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_sevice';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ahistory_id', 'vhc_model_id', 'listNmae', 'comment', 'sumprice', 'sumvat', 'suminvatprice', 'vhc_master_id', 'status', 'create_by', 'update_by', 'create_date', 'update_date'], 'required'],
            [['ahistory_id', 'vhc_model_id', 'vhc_master_id', 'status'], 'integer'],
            [['comment'], 'string'],
            [['sumprice', 'sumvat', 'suminvatprice'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['listNmae'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ahistory_id' => 'Ahistory ID',
            'vhc_model_id' => 'Vhc Model ID',
            'listNmae' => 'List Nmae',
            'comment' => 'Comment',
            'sumprice' => 'Sumprice',
            'sumvat' => 'Sumvat',
            'suminvatprice' => 'Suminvatprice',
            'vhc_master_id' => 'Vhc Master ID',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
