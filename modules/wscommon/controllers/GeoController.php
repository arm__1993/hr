<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Amaingeography;
use yii\helpers\ArrayHelper; //vendor/yiisoft/yii2/helpers/ArrayHelper


class GeoController extends ActiveController
{

    // adjust the model class to match your model
    public $modelClass = 'app\modules\wscommon\models\Amaingeography';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}


}



