<?php
namespace app\modules\hr\apihr;
use yii\db\Expression;
use yii\db\Transaction;
use yii;

use app\modules\hr\models\Benefitfund;

use app\api\Helper;
use app\api\DateTime;
use app\api\Utility;
use app\api\DBConnect;


class ApiBenefit
{


    public static function CalculateBenefit($arrIDCard,$MonthYear)
    {

                $ID_CRAD_TRANSACTION_BY = Yii::$app->session->get('idcard');
                $resultYear = substr($MonthYear,0,2);
//                print_r($resultYear);
//                exit;

                $monthDetail = DateTime::mapMonth($resultYear);
//                print_r($monthDetail);
               
                $resultCalculateBenefit = [];
                foreach($arrIDCard as $key => $value)
                {
                    $valueresultBenefitthismonth = $value['EMP_SALARY_WAGE']*($value['benefitFundPercent']/100);
                    $valueDecimalResult = Helper::displayDecimal($valueresultBenefitthismonth);
                    $valueRoundDecimalResult = Utility::MakeRound($valueDecimalResult);

                     $id_card = $value['empIdCard'];
                    // echo "<br>";
                    $modelGetlastbenefitMaxid = Benefitfund::find()
                                          ->select(['BENEFIT_EMP_ID' , 'max(BENEFIT_ID) as maxidbenefit'])
                                          ->where('BENEFIT_EMP_ID = :id_card')
                                          ->addParams([':id_card' => $id_card,])
                                          ->asArray()
                                          ->all();
                    //print_r($modelGetlastbenefitMaxid);

                    $modelGetlastbenefit =Benefitfund::find()
                                          ->select('BENEFIT_EMP_ID,BENEFIT_TOTAL_AMOUNT')
                                          ->where('BENEFIT_ID = :BENEFIT_ID')
                                          ->addParams([':BENEFIT_ID' => $modelGetlastbenefitMaxid['0']['maxidbenefit'],])
                                          ->asArray()
                                          ->all();
                    //print_r($modelGetlastbenefit);

                        if($value['benifitFundStatus']==1){

                            $resultBenefitThismonth = ($value['EMP_SALARY_WAGE']*($value['benefitFundPercent']/100)) + $modelGetlastbenefit['0']['BENEFIT_TOTAL_AMOUNT'];


                        $resultCalculateBenefit[$key]['BENEFIT_EMP_ID'] = $value['empIdCard'];
                        $resultCalculateBenefit[$key]['BENEFIT_SAVING_DATE'] = $MonthYear;
                        $resultCalculateBenefit[$key]['BENEFIT_DETAIL'] = "เดือน ".$monthDetail;
                        $resultCalculateBenefit[$key]['BENEFIT_AMOUNT'] = $valueRoundDecimalResult;
                        $resultCalculateBenefit[$key]['BENEFIT_TOTAL_AMOUNT'] = $resultBenefitThismonth;
                        $resultCalculateBenefit[$key]['BENEFIT_TRANSACTION_BY'] = $ID_CRAD_TRANSACTION_BY;
                        $resultCalculateBenefit[$key]['BENEFIT_TRANSACTION_DATE'] = new Expression('NOW()');
                        $resultCalculateBenefit[$key]['BENEFIT_FUND_STATUS'] = 1;
                        $resultCalculateBenefit[$key]['BENEFIT_FUND_REMARK'] = '';
                        }else
                        {
                        $resultCalculateBenefit[$key]['BENEFIT_EMP_ID'] = $value['empIdCard'];
                        $resultCalculateBenefit[$key]['BENEFIT_SAVING_DATE'] = $MonthYear;
                        $resultCalculateBenefit[$key]['BENEFIT_DETAIL'] = "เดือน".$monthDetail;
                        $resultCalculateBenefit[$key]['BENEFIT_AMOUNT'] = 0;
                        $resultCalculateBenefit[$key]['BENEFIT_TOTAL_AMOUNT'] = 0;
                        $resultCalculateBenefit[$key]['BENEFIT_TRANSACTION_BY'] = $ID_CRAD_TRANSACTION_BY;
                        $resultCalculateBenefit[$key]['BENEFIT_TRANSACTION_DATE'] = new Expression('NOW()');
                        $resultCalculateBenefit[$key]['BENEFIT_FUND_STATUS'] = 1;
                        $resultCalculateBenefit[$key]['BENEFIT_FUND_REMARK'] = '';
                        }
                }
          
                
                 Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
                            ->batchInsert('BENEFIT_FUND', ['BENEFIT_EMP_ID',
                                'BENEFIT_SAVING_DATE',
                                'BENEFIT_DETAIL',
                                'BENEFIT_AMOUNT',
                                'BENEFIT_TOTAL_AMOUNT',
                                'BENEFIT_TRANSACTION_BY',
                                'BENEFIT_TRANSACTION_DATE',
                                'BENEFIT_FUND_STATUS',
                                'BENEFIT_FUND_REMARK'],
                                $resultCalculateBenefit)
                                ->execute();
    }

     public static function ListYearFUNIDCrad($idcard)
    {
        $model  = Benefitfund::find()
                            ->select(['BENEFIT_SAVING_DATE'])
                            ->where('BENEFIT_EMP_ID = :BENEFIT_EMP_ID')
                            ->addParams([':BENEFIT_EMP_ID' => $idcard])
                            ->asArray()
                            ->all();
        //print_r($model);
        return $model;
    }

    public static function ListDataBenefit($id_card,$year)
    {
        $year = '%'.$year;
        $model  = Benefitfund::find()
                            ->where('BENEFIT_EMP_ID = :BENEFIT_EMP_ID
                                 AND BENEFIT_SAVING_DATE  like :year')
                            ->addParams([':BENEFIT_EMP_ID' => $id_card,
                                         ':year' => $year,
                            ])
                            ->asArray()
                            ->all();
        //print_r($model);
        return $model;
    }

    public static function SumListDataBenefitThisYear($id_card,$year)
    {
        $year = '%'.$year;
        $modelMax  = Benefitfund::find()
                            ->select(['max(BENEFIT_ID) as maxidbenefit'])
                            ->where('BENEFIT_EMP_ID = :BENEFIT_EMP_ID
                                 AND BENEFIT_SAVING_DATE  like :year')
                            ->addParams([':BENEFIT_EMP_ID' => $id_card,
                                         ':year' => $year,
                            ])
                            ->asArray()
                            ->all();

        $model =Benefitfund::find()
                            ->select('BENEFIT_EMP_ID,BENEFIT_TOTAL_AMOUNT')
                            ->where('BENEFIT_ID = :BENEFIT_ID')
                            ->addParams([':BENEFIT_ID' => $modelMax['0']['maxidbenefit'],])
                            ->asArray()
                            ->all();
        //print_r($model);
        return $model;
    }

    public static function SumListDataBenefitThisAll($id_card)
    {
        $modelMax  = Benefitfund::find()
                            ->select(['max(BENEFIT_ID) as maxidbenefit'])
                            ->where('BENEFIT_EMP_ID = :BENEFIT_EMP_ID')
                            ->addParams([':BENEFIT_EMP_ID' => $id_card,])
                            ->asArray()
                            ->all();

        $model =Benefitfund::find()
                            ->select('BENEFIT_EMP_ID,BENEFIT_TOTAL_AMOUNT')
                            ->where('BENEFIT_ID = :BENEFIT_ID')
                            ->addParams([':BENEFIT_ID' => $modelMax['0']['maxidbenefit'],])
                            ->asArray()
                            ->all();
        //print_r($model);
        return $model;
    }
}

?>