<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_promise_type".
 *
 * @property integer $id
 * @property string $permise_name
 * @property integer $promise_type
 * @property string $promise_detail
 * @property string $promise_bond
 * @property string $promise_attach
 */
class EmpPromiseType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_promise_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promise_type'], 'integer'],
            [['promise_detail', 'promise_bond', 'promise_attach'], 'string'],
            [['permise_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permise_name' => 'Permise Name',
            'promise_type' => 'Promise Type',
            'promise_detail' => 'Promise Detail',
            'promise_bond' => 'Promise Bond',
            'promise_attach' => 'Promise Attach',
        ];
    }
}
