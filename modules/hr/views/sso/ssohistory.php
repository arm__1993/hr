<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:12
 */



use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;

use app\bundle\AppAsset;
// API กลาง
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;
// API HR


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/sso/ssohistory.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
// //$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);


$script = <<< JS
$(document).ready(function() {
    setTimeout(function() {
    $('#message').fadeOut('slow');
    }, 3000);


$("#example2").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );

$("#tablelistdata").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );
});
JS;

$this->registerCssFile(Yii::$app->request->BaseUrl."/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->BaseUrl."/fonts/01thaifontcss.css");


$this->registerJs($script);
?>
<style type="text/css">
      .modal-wide .modal-dialog {
               width: 90%; /* or whatever you wish */
        }
</style>
<section class="content">
    <!-- Default box -->
    <!--<div class="box box-danger">-->
        <!--<div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li>เพิ่มข้อมูลพนักงาน</li>
                <li class="active">ประกันสังคมและเงินสะสม</li>
            </ul>
        </div>-->
        <div class="box-body">
        <input type="hidden" id="idcardset" value="1509901325106">
             <form class="form-horizontal">
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title">ประวัติประกันสังคม</h3>
                    </div> -->
                    <div class="box-body">
                        <div class="row">
                        <div class="modal-body" width="100%" class="table table-bordered table-hover dataTable">
                            <div class="form-group">
                                <div class="col-sm-3">
                                &nbsp;
                                </div>
                                <label for="genderEmp" class="col-sm-2 control-label">เงินประกันสังคมประจำปี</label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="selectyearsso" onchange="selectyearvalsso(this)">
                                    
                                    </select>
                                </div> 
                                <br>
                                <br>
                                 <div class="content" style="display:none" id="showtbcontentsso">
                                    <table class="table table-bordered  table-hover dataTable" width="50%" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>วันที่จ่าย</th>
                                                <th>คิดกี่%</th>
                                                <th>จำนวน/บาท</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tblticketsso">
                                        </tbody>
                                    </table>
                                    <div class="row" style="display:none" id="showSumThisSso">
                                        <label class="col-sm-8 control-label" ><div id="sumThisYearSso"></div></label>
                                    </div>
                                    <div class="row" style="display:none" id="showSumAllSso">
                                        <label class="col-sm-8 control-label"><div id="sumThisAllSso"></div></label>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                    </div>
            </form>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
                                                                    <!-- benefit -->
                                            <!-- SSO -->
<div class="modal fade modal-wide" id="modalSSO" tabindex="1"  aria-labelledby="myModalLabel" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="myModalLabel" class="modal-title">ประวัติการจ่ายเงินประกันสังคม</h4>
                </div>
                    
               
            </div><!-- /.modal-content -->
        <!--</div> /.modal-dialog -->
</div><!-- /.modal -->      
