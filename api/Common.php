<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 9/13/2016 AD
 * Time: 9:55 AM
 */

namespace app\api;
use Yii;

class Common
{

    /**
     * function siteURL เป็น  ฟังก์ชั่นสำหรับการหา protocal/hostname  สำหรับการ fixed full  path  ให้กับระบบ
     */
	public static function siteURL()
	{
	    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $domainName = $_SERVER['HTTP_HOST'];
	    return $protocol.$domainName;
	}

	/**
     * function getCurrentVat เป็นฟังก์ชั่นสำหรับการเรียก VAT ปัจจุบันที่กำหนดไว้
     */
	public static function getCurrentVat()
	{

		$sql = "SELECT tax_shortname,tax_profilename,tax_value FROM taxprofile WHERE status_active=1";
		//$model = $connection->createCommand($sql)->queryAll();
		$model = Yii::$app->db->createCommand($sql)->queryOne();
		if($model===null) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		else {
			return $model;
		}
	}

    /**
     * function CalculateTAX เป็นฟังก์ชั่นการคำนวณหาภาษี ใช้ได้ทั้งภาษา
     *  INPUT ราคา, อัตราภาษี
     *  OUTPUT Tax Amount from Formula
     */
    public static function CalculateTax($values,$taxvalue)
    {
        return $values*($taxvalue/100);
    }

    /**
     * function CalculateInclTax เป็นฟงก์ชั่นคำนวณราคารวมกับภาษี
     * INPUT ราคา, อัตราภาษี
     * OUTPUT ราคารวมภาษี
     */
    public static function CalculateInclTax($values,$taxvalue)
    {
        return $values + ($values*($taxvalue/100));
    }

    /**
     * function DecodeTax เป็นฟังก์ชั่นถอดภาษีออกจากราคาสินค้า
     * INPUT ราคารวมภาษี, อัตราภาษี
     * OUTPUT ราคารวม,ราคาไม่รวมภาษี,จำนวนภาษี
     */
    public static function DecodeTax($values,$taxvalue)
    {
        $tax = $values * ($taxvalue /((100+$taxvalue)));
        $notax = $values-$tax;
        return array(
            'total'=>$values,
            'notax'=>$notax,
            'tax'=>$tax,
        );
    }


    public static function getAllEmployee()
    {
        $sql = "SELECT ID_Card, Name, Surname, CONCAT(Name,' ',Surname) as fullname, Prosonnal_Being 
                FROM emp_data
                WHERE Prosonnal_Being <> 3 ORDER BY Name ASC";
        $model = Yii::$app->dbERP_easyhr_checktime->createCommand($sql)->queryAll();
        $arrEmployee=[];
        foreach ($model as $item) {
            $arrEmployee[$item['ID_Card']] = $item['fullname'];
        }
        return $arrEmployee;

    }

}