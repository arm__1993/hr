<?php

namespace app\modules\webreport\controllers;

use app\modules\webreport\apiwebreport\ApiNumber;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

use mPDF;
use PHPExcel;

class NumberController extends \yii\web\Controller
{
    public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionNumber_of_car_repair()
    {

        return $this->render('number_of_car_repair',['query'=>false,]);
    }

    public function actionCar_repairman()
    {

        $postValue = Yii::$app->request->post();
        $type_company = ($postValue['company']);
        $months = ($postValue['reservation']);
        $select_Time = DateTime::FormatDateFromCalendarRange($months);

        $Time_start = $select_Time['start_date'];
        $Time_End = $select_Time['end_date'];
        $technician_car = $postValue['technician'];
        if ($technician_car == '1') {
            $technician_TypeCar = '3,4';
        } elseif ($technician_car == '2') {
            $technician_TypeCar = '1,2';
        }
   

        $CarRepairman = ApiNumber::getCarRepairman($type_company,$technician_TypeCar,$Time_start,$Time_End);
        $cvhcmain_id = ApiNumber::getCarRepairmanCvhcmain($type_company,$technician_TypeCar,$Time_start,$Time_End);
    if(empty($CarRepairman)) {
            // $message = "ไม่มีข้อมูลบริษัท";
            // echo "<script type='text/javascript'>alert('$message');</script>";
            // return $this->render('pfm_mechanic');
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('number_of_car_repair', [
                'result_total' => $array_result,
                'selected_company'=>$type_company,
                'selected_daterange'=>$months,
                'query'=>false,
            ]);
        }else{
        $arr_technician = $arr_countcar = $arr_cvhcmain_technician = [];

        foreach ($cvhcmain_id as $value) {
            $arr_cvhcmain_technician[] = $value['cvhc_main'];
        }
        
        $Technician = ApiNumber::getTechnician($arr_cvhcmain_technician);

        foreach ($CarRepairman as $value) {
            $arr_countcar[$value['date_end']] = $value['sum_car'];
        }
         foreach ($Technician as $value) {
            $arr_technician[$value['date_end']] = $value['countTechnician'];
        }
        


        $array_result = (array_merge_recursive($arr_countcar,$arr_technician));

            $session = Yii::$app->session;

            $session->set('result_total', $array_result);
            $session->set('selected_company', $type_company);
            $session->set('selected_daterange', $months);
            $session->set('technician_car', $technician_car);

            return $this->render('number_of_car_repair', [
                'result_total' => $array_result,
                'selected_company'=>$type_company,
                'selected_daterange'=>$months,
                'technician_car' => $technician_car,
                'query'=>true,
            ]);

        }


    }

    public function actionCarrepairofmonth()
    {
        $postCompany = Yii::$app->request->post();
        $type_company = $postCompany['company'];

        $postReservation = Yii::$app->request->post();
        $months = ($postReservation['reservation2']);

        $dayEnd = substr($months, 13, 2);
        $MonthEnd = substr($months, 16, -5);
        $yearEnd = substr($months, 19);
        $dayStart = substr($months, 0, -21);
        $MonthStart = substr($months, 3, -18);
        $yearStart = substr($months, 6, -13);
        $str_start = (strtotime("$dayStart-$MonthStart-$yearStart"));
        $str_end = (strtotime("$dayEnd-$MonthEnd-$yearEnd"));


        $carRepairman = ApiNumber::getCarRepairManInMonth($type_company,$str_start,$str_end);
        if (empty($carRepairman)) {
           Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
              return $this->render('number_of_car_repair', [
                'carRepairman' => $carRepairman,
                'selected_company'=>$type_company,
                'selected_daterange'=>$months,
                'query'=>false,
            ]);
        } else {
            /*print_r($carRepairman);
            exit(); */

            $session = Yii::$app->session;

            $session->set('carRepairman', $carRepairman);

            return $this->render('number_of_car_repair', [
                'carRepairman' => $carRepairman,
                'selected_company'=>$type_company,
                'selected_daterange'=>$months,
            ]);
        }
    }


    public function actionBtn_reportnum()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportnum'));
        $mpdf->Output();
        exit;

    }

    public function actionExportnum()
    {
        return $this->render('_exportnum');
    }


}
