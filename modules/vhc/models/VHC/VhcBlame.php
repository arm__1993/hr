<?php

namespace app\modules\vhc\models\VHC;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\vhc\models\MasterModel;

/**
 * This is the model class for table "vhc_blame".
 *
 * @property integer $id
 * @property string $blame_name
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcBlame extends MasterModel
{
    /**
     * @inheritdoc
     */
    protected $_pageSize;
    public static function tableName()
    {
        return 'vhc_blame';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blame_name'], 'required'],
            [['record_status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['blame_name'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blame_name' => 'Blame Name',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
   /* public function search($params)
    {

        $query = VhcBlame::find()->where(['!=','record_status','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(
                                ['like', 'blame_name', $this->blame_name]
                                );

        return $dataProvider;
    }*/

    public function search($params)
    {
        $data = VhcBlame::find() ->where('record_status <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','blame_name',$this->blame_name]); //	ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
