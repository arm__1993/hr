<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/6/2017 AD
 * Time: 11:57
 */


use app\bundle\AppAsset;

AppAsset::register($this);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payrollextra/reportdeduction.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/ot_manageot.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

?>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li><a href="#">ส่งออกไฟล์</a></li>
                <li class="active">รายงานบัญชี/การเงิน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active tabselect" id="tabselect1"><a href="#tab1" data-toggle="tab">รายงานสรุปผลเงินเดือน</a></li>
                    <li id="tabselect2" class="tabselect"><a href="#tab2" data-toggle="tab">สรุปส่วนเพิ่ม</a></li>
                    <li id="tabselect3" class="tabselect"><a href="#tab3" data-toggle="tab">รายละเอียดส่วนเพิ่ม</a></li>
                    <li id="tabselect8" class="tabselect"><a href="#tab4" data-toggle="tab">รายงานภาษี</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active tabselect" id="tab1">aa</div>
                    <div class="tab-pane tabselect" id="tab2">bb</div>
                    <div class="tab-pane tabselect" id="tab3">cc</div>
                    <div class="tab-pane tabselect" id="tab4">dd</div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
