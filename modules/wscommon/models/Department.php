<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $code_name
 * @property integer $company
 * @property string $name
 * @property integer $status
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_name', 'company', 'name'], 'required'],
            [['company', 'status'], 'integer'],
            [['code_name'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_name' => 'Code Name',
            'company' => 'Company',
            'name' => 'Name',
            'status' => '99 ไม่ใช้ 1 ใช้',
        ];
    }
}
