<?php

namespace app\modules\wssale\models;

use Yii;

class Booking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Booking';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PO_doc', 'Booking_Num', 'Finance', 'B_cusBuy', 'Booking_Date', 'B_Model_Class', 'B_Model_GName', 'B_Model_C_M', 'web_order', 'Sell_Type_Detail_E', 'SaleBooking_posCode', 'branch_booking', 'Sell_Type_Other', 'WhoInput', 'status', 'Status_Remark', 'SaleNameFull', 'delivery_place', 'delivery_responsible', 'deliver_posCode', 'Log_Remark', 'Bstatus_tis', 'BDate_upstatus', 'pre_respond', 'pre_res_posCode', 'edit_info', 'Status_insuranceActDetailStatus', 'insuranceAct_Status', 'statusFinace', 'FinaceRemark', 'FinaceRemarkCredit', 'statut_cancel_booking', 'statut_cancel_booking_money', 'pre_money_cancel', 'status_report_cusInt', 'status_report'], 'required'],
            [['Booking_Date', 'Est_Delivery'], 'safe'],
            [['branch_booking', 'status', 'Status_insuranceActDetailStatus', 'insuranceAct_Status', 'statusFinace', 'statut_cancel_booking', 'statut_cancel_booking_money'], 'integer'],
            [['Remark', 'B_Input_Time', 'WhoInput', 'Log_Remark', 'FinaceRemark', 'FinaceRemarkCredit'], 'string'],
            [['PO_doc', 'B_Int_Num', 'CusNo', 'B_CusNo', 'B_Unit_Type', 'B_Unit_Model', 'Eng_No_B', 'Price_E', 'Down_E', 'Int_E', 'Time_E', 'Install_Month', 'Reg_Free', 'Tran_Free', 'Ins_Free', 'Booking_Paid', 'delivery_place'], 'string', 'max' => 50],
            [['Booking_Num', 'Finance', 'Chassi_No_B', 'SaleNameFull'], 'string', 'max' => 255],
            [['B_cusBuy'], 'string', 'max' => 60],
            [['B_Model_Class', 'B_Model_GName', 'B_Model_C_M', 'web_order', 'Status_Remark'], 'string', 'max' => 100],
            [['B_Unit_Color'], 'string', 'max' => 3],
            [['Sell_Type_E'], 'string', 'max' => 4],
            [['Sell_Type_Detail_E', 'Sell_Type_Other', 'pre_money_cancel'], 'string', 'max' => 250],
            [['SaleBookingNo', 'delivery_responsible'], 'string', 'max' => 13],
            [['SaleBooking_posCode', 'deliver_posCode', 'pre_res_posCode'], 'string', 'max' => 30],
            [['Bstatus_tis'], 'string', 'max' => 5],
            [['BDate_upstatus'], 'string', 'max' => 20],
            [['pre_respond'], 'string', 'max' => 15],
            [['edit_info'], 'string', 'max' => 10],
            [['status_report_cusInt', 'status_report'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Booking_No' => 'หมายเลขจอง',
            'PO_doc' => 'PO เลขเอกสารอ้างอิง',
            'Booking_Num' => 'หมายเลขใบจอง',
            'B_Int_Num' => 'หมายเลขอ้างอิงการสนใจ',
            'Finance' => 'Finance',
            'CusNo' => 'crm_ref_cusno',
            'B_CusNo' => 'รหัสคน (หมายเลขลูกค้าจอง)',
            'B_cusBuy' => 'รหัสลูกค้าผู้ออกรถ',
            'Booking_Date' => 'วันที่จอง',
            'B_Unit_Type' => 'ชนิดรถ',
            'B_Model_Class' => 'ชื่อระดับรุ่น เช่น Dragon Power ,D-Max',
            'B_Model_GName' => 'แบบรถทั่วไป',
            'B_Unit_Model' => 'เลขแบบรถ',
            'B_Model_C_M' => 'รหัสรุ่นรถ',
            'B_Unit_Color' => 'สี',
            'Eng_No_B' => 'เลขเครื่อง',
            'Chassi_No_B' => 'เลขแชสสี crm_ref_vehicle',
            'web_order' => 'เลข OD',
            'Price_E' => 'ราคา',
            'Sell_Type_E' => 'ซื้อโดย',
            'Sell_Type_Detail_E' => 'ผ่อนกับบริษัทไหน',
            'Down_E' => 'เงินดาวน์',
            'Int_E' => 'ดอกเบี้ย',
            'Time_E' => 'ระยะเวลา',
            'Install_Month' => 'ส่งต่อเดือน',
            'Reg_Free' => 'ค่าทะเบียน (ต้นทุน)',
            'Tran_Free' => 'ค่าขนส่ง (ต้นทุน)',
            'Ins_Free' => 'ค่าพรบ. (ต้นทุน)',
            'Booking_Paid' => 'เงินจอง',
            'Est_Delivery' => 'ประมาณวันออกรถ ',
            'SaleBookingNo' => 'พนง.ขายที่รับจอง  ',
            'SaleBooking_posCode' => 'รหัสตำแหน่ง พนง.ที่รับจอง',
            'branch_booking' => 'สาขาที่นับยอดจอง',
            'Sell_Type_Other' => 'ไม่ได้ใช้งาน',
            'Remark' => 'หมายเหตุ',
            'B_Input_Time' => 'เวลาบันทึกข้อมูล',
            'WhoInput' => 'ผู้บันทึกข้อมูล',
            'status' => '0=รอการแต่ง 1=กำลังแต่งรถ 2=รอส่งมอบ 9=เรียบร้อย 99=ถูกยกเลิกเพราะเปลี่ยนรถ',
            'Status_Remark' => 'Status  Remark',
            'SaleNameFull' => 'Sale Name Full',
            'delivery_place' => 'สถานที่ส่งมอบรถ',
            'delivery_responsible' => 'ผู้รับผิดชอบส่งมอบรถ',
            'deliver_posCode' => 'รหัสตำแหน่งผู้รับผิดชอบส่งมอบรถ',
            'Log_Remark' => 'refer ZLogEditAllTable ',
            'Bstatus_tis' => 'Bstatus Tis',
            'BDate_upstatus' => 'Bdate Upstatus',
            'pre_respond' => 'ชื่อผู้ดูแลหลังการขาย',
            'pre_res_posCode' => 'รหัสตำแหน่งผู้ดูแลหลังการขาย',
            'edit_info' => 'ถ้ามีการแก้ไขข้อมูลหลักจะอัพเดทfieldนี้ = 1',
            'Status_insuranceActDetailStatus' => 'สถานะ พรบ 0 = รอออกพรบ  1 = รอยิงbarcode  2 =เสร็จมบูรณ์',
            'insuranceAct_Status' => 'สถานะ ประกันภัย 0 = รอออกประกันภัย  1 = รอยิงbarcode  2 =เสร็จมบูรณ์',
            'statusFinace' => '1 กำลังหรือทำไฟแนนซ์แล้ว 0 ไม่ได้ทำไฟแนนซ์',
            'FinaceRemark' => 'หมายเหตุไฟแนนซ์',
            'FinaceRemarkCredit' => 'หมายเหตุไฟแนนซ์กรณีเครดิต',
            'statut_cancel_booking' => '0:ปกติ 1:รอหัวหน้าอนุมัติ 2:รอการเงินอนุมัติ',
            'statut_cancel_booking_money' => '1:คืนเงินจอง 2:ไม่คืนเงินจอง ',
            'pre_money_cancel' => 'sell ที่รับเงินจองจากการเงิน',
            'status_report_cusInt' => '1:คำนวณลูกค้าสนใจจองเรียบร้อยแล้ว',
            'status_report' => '1:คำนวณรายงานเรียบร้อยแล้ว,9:คำนวณยกเลิกแล้ว',
        ];
    }

    public function searchbookingformobile($cusno)
    {

        $sql = "select Booking.Booking_No, 
        (SELECT ERP_new_cusdata.MAIN_CUS_GINFO.Cus_Name FROM ERP_new_cusdata.MAIN_CUS_GINFO INNER JOIN ERP_Easysale_icmba.Booking ON ERP_new_cusdata.MAIN_CUS_GINFO.CusNo = ERP_Easysale_icmba.Booking.B_CusNo WHERE Booking.CusNo = :cusno
        ) AS NameB_Cusno,
        (SELECT ERP_new_cusdata.MAIN_CUS_GINFO.Cus_Name FROM ERP_new_cusdata.MAIN_CUS_GINFO INNER JOIN ERP_Easysale_icmba.Booking ON ERP_new_cusdata.MAIN_CUS_GINFO.CusNo = ERP_Easysale_icmba.Booking.B_cusBuy WHERE Booking.CusNo = :cusno
        ) AS NameB_cusBuy,
        Booking.B_Unit_Model,
        Booking.statut_cancel_booking_money
                FROM Booking INNER JOIN CusInt ON Booking.B_Int_Num = CusInt.Int_Num 
                    WHERE Booking.CusNo = (SELECT ERP_new_cusdata.MAIN_CUS_GINFO.CusNo 
                    FROM ERP_new_cusdata.MAIN_CUS_GINFO WHERE CusNo = :cusno )
        ORDER BY Booking.Booking_No ASC";
        //echo ;
        $showdataeducusBooking = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
        return $showdataeducusBooking;
 
    }
}
