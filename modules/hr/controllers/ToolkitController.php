<?php

namespace app\modules\hr\controllers;
use app\api\Utility;
use app\modules\hr\models\Position;
use Yii;

class ToolkitController extends \yii\web\Controller
{

    public $layout = 'hrlayout';
    public $exec = true;
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionUpdateempsalarytoempdata()
    {
        //Get All Position
        $modelPosition = Position::find()->where([
            'Status'=>1,
        ])->orderBy('id ASC')->asArray()->all();
        $arrAllPosition = [];
        foreach ($modelPosition as $item) {
            $arrAllPosition[$item['PositionCode']] = $item;
        }


        $sql = "SELECT * FROM emp_data WHERE Prosonnal_Being IS NULL";
        $emp = Yii::$app->dbERP_easyhr_checktime->createCommand($sql)->queryAll();

        $arrID = [];
        foreach ($emp as $item) {
            array_push($arrID,$item['ID_Card']);
        }

        $strID = Utility::JoinArrayToString($arrID);
        $sql = "SELECT * FROM EMP_SALARY WHERE EMP_SALARY_ID_CARD IN($strID) ";
        $pos = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
        foreach ($pos as $item) {
            //echo $item['EMP_SALARY_ID_CARD'];
            //echo '<br>';
            $arrPosition = $arrAllPosition[trim($item['EMP_SALARY_POSITION_CODE'])];
            $_idcard = $item['EMP_SALARY_ID_CARD'];
            $_pro = 2;
            $_Code = $arrPosition['PositionCode'];
            $_Position = $arrPosition['Name'];
            $_Working_Company = $item['EMP_SALARY_WORKING_COMPANY'];
            $_Department = $item['EMP_SALARY_DEPARTMENT'];
            $_Section = $item['EMP_SALARY_SECTION'];
            $_start_date = $item['start_effective_date'];

            $sql = "UPDATE  emp_data SET Code='$_Code',  Prosonnal_Being='$_pro', 
                    Position='$_Position', Working_Company ='$_Working_Company', Department='$_Department', 
                    Section='$_Section', Start_date='$_start_date'
                    WHERE ID_Card='$_idcard' ";
            $s = ($this->exec) ? Yii::$app->dbERP_easyhr_checktime->createCommand($sql)->execute() : null;
            if($s) {
                echo "update emp_data ===> $_idcard";
                echo '<br>';
            }
        }
    }

}
