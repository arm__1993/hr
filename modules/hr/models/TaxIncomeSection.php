<?php

namespace app\modules\hr\models;

use Yii;

use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tax_income_section".
 *
 * @property integer $id
 * @property integer $tax_income_type_id
 * @property string $tax_section_name
 * @property string $expenses_percent
 * @property string $expenses_notover
 * @property string $comment
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class TaxIncomeSection extends \yii\db\ActiveRecord
{
    protected $_pageSize;

    function __construct() {
        $this->_pageSize = Yii::$app->params['PAGE_SIZE'];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_income_section';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_income_type_id', 'tax_section_name'], 'required'],
            [['tax_income_type_id', 'status_active'], 'integer'],
            [['expenses_percent', 'expenses_notover'], 'number'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['tax_section_name'], 'string', 'max' => 255],
            [['comment'], 'string', 'max' => 1000],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tax_income_type_id' => 'Tax Income Type ID',
            'tax_section_name' => 'Tax Section Name',
            'expenses_percent' => 'Expenses Percent',
            'expenses_notover' => 'Expenses Notover',
            'comment' => 'Comment',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    public function search($params)
    {

        $query = TaxIncomeSection::find()
        ->select('tax_income_section.*,taxincome_type as tax_income_type_name')
        ->join('INNER JOIN', 
				'tax_income_type',
				'tax_income_section.tax_income_type_id = tax_income_type.id'
			)
            ->where(['!=','tax_income_section.status_active','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'tax_income_type_id', $this->tax_income_type_id])
            ->andFilterWhere(['like', 'tax_section_name', $this->tax_section_name]);

        return $dataProvider;
    }
     public function attributes()
    {
        return array_merge(parent::attributes(), ['tax_income_type_name']);
    }

}
