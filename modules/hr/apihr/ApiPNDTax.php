<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/14/2018 AD
 * Time: 13:54
 */



namespace app\modules\hr\apihr;

use app\api\Helper;
use app\api\Utility;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\models\DISCOUNTINCOMETAX;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Empfamily;
use app\modules\hr\models\PayrollConfigWage;
use app\modules\hr\models\TaxCalculate;
use app\modules\hr\models\TaxCalculateStep;
use app\modules\hr\models\TaxIncomeStructure;
use app\modules\hr\models\Wagethismonth;
use yii\helpers\ArrayHelper;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\TaxIncomeSection;
use app\modules\hr\models\TaxIncome;
use Yii;
use yii\db\Expression;


class ApiPNDTax
{

    public static function print_object($obj)
    {
        echo '<pre>';
        print_r($obj);
        echo '</pre>';
    }

    public static function TaxPNDInCome($connection,$pay_date,$arrSalaryAll,$ACTION,$PND1_Template,$arrMoneyAddPND)
    {
        /** ---------------------------------------------------------------------------
         *  โหลดข้อมูลภาษีที่เคยนำส่งไปแล้วในปีนี้
         *----------------------------------------------------------------------------*/


        /** ---------------------------------------------------------------------------
         *  โหลดข้อมูลลดหย่อนส่วนตัวในปีนี้
         *----------------------------------------------------------------------------*/
        $arrPersonTaxDeduct = self::getEmpTaxDeduction();
        $fix_individual_reduce = 60000;

        /** ---------------------------------------------------------------------------
         *  โหลดค่าใช้จ่ายส่วนตัวส่วนตัวในปีนี้
         *----------------------------------------------------------------------------*/
        $fix_individual_cost = 100000;



        //$arrPersonTaxDeduct['3560500575223'] = $fix_individual_cost+$fix_individual_reduce+30000;
        //$arrPersonTaxDeduct['1570400099474'] = 169000;

        //$_reduce_individual = $fix_individual_cost + $fix_individual_reduce;

        $wage_type = 3; //รายเดือน
        $arrWageConfig = self::getPayrollConfigWage();
        //$_daycal = $_arrWageConfig['wage_day'];
        $_multiply = $arrWageConfig[$wage_type]['wage_multiply'];


        $arrPersonNetIncome = $arrPersonDetail =  [];
        foreach ($arrSalaryAll as $item) {

            $_discount_sso = $item['cal_sso_amount'] * 12; //SSO FUND x 12

            $_addMoney = (isset($arrMoneyAddPND[$item['ID_Card']])) ? $arrMoneyAddPND[$item['ID_Card']] : 0 ;
            $_cal_total_income = $item['EMP_SALARY_WAGE'] + $_addMoney;
            $_yearly_amount = $_cal_total_income * $_multiply;

            //$_total_discount = $_reduce_individual;
            $_total_discount = (isset($arrPersonTaxDeduct[$item['ID_Card']])) ? $arrPersonTaxDeduct[$item['ID_Card']] : 0 ;
            //$_total_discount += $_discount_sso;  //discount plus SSO fund x 12;
            $arrPersonNetIncome[$item['MainWorking_Company']][$item['ID_Card']] = ($_yearly_amount - $_total_discount);

            $arrPersonDetail[$item['ID_Card']] = [
                'cal_add_money'=>$_addMoney, //$item['cal_add_money'],
                'emp_salary_wage'=>$item['EMP_SALARY_WAGE'],
                'cal_total_income' => $_cal_total_income,//$item['cal_total_income'],
                'position_code'=>$item['MainCode'],
                'position_name'=>$item['MainPosition'],
                'total_yearly' => $_yearly_amount,
                'total_discount' => $_total_discount,
            ];

           // $arrPersonTaxDeduct[$item['ID_Card']] = 0;
        }



        $arrIncomeStructureRule = self::getArrayIncomeStructure();


        /** @var
         * $arrPersonIncomeRange  ==> จัดเก็บรายละเอียดการคำนวณ ของภาษีแต่ละขั้น
         * $arrPersonGrossStepTax ==> จัดเก็บภาษี ที่คำนวณได้แต่ละขั้น
         * $arrPersonIncomeRate  ==> จัดเก็บอัตราภาษีแต่ละขั้น
         * $arrPersonAccumulateTax  ==> จัดเก็บอัตราสะสมภาษี ในแต่ละขั้น
         * */

        //$arrPersonNetIncome = $arrWageData['TOTAL'];

        $arrPersonGrossStepTax = $arrPersonIncomeRange = $arrPersonIncomeStep = $arrPersonIncomeRate = $arrPersonAccumulateTax = [];


        foreach ($arrPersonNetIncome as $CompanyID => $Person) {
            foreach ($Person as $IDCard => $IncomeStep) {

                //Check income if = 0 OR lessthan 0
                //if($IncomeStep <= 0) { echo 'Income incorrect, verify income ID CARD '.$IDCard; exit; }

                $r = 1;
                $startIncome = $IncomeStep;
                $RemainIncome = $TotalBreakIncome = 0;

                foreach ($arrIncomeStructureRule as $rule) {

                    $ceil = $rule['income_ceil'];
                    $floor = $rule['income_floor'];
                    $TaxRate = $rule['tax_rate'] / 100;
                    $IncomeRuleCeil = ($ceil - $floor) + 1;

                    $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = 0;

                    $InconeBeforeCal = $RemainIncome; //เงินได้ก่อน หักคำนวณตามสูตร
                    $IncomeStep -= $IncomeRuleCeil;
                    $RemainIncome = $IncomeStep;

                    //กรณีเกินขอบเขตที่กำหนด เป็นประเภทเหมาจ่าย
                    if ($ceil == 0) {
                        $balloon = ($startIncome - $TotalBreakIncome);  //นำเงินได้ที่ สะสมหักในอัตราปกติ ลบออกจากเงินได้ทั้งหมด จะได้เงินก้อน (balloon) เพื่อหาภาษีขั้นสุดท้าย
                        $StepTax = $balloon * $TaxRate;
                        $arrPersonIncomeStep[$CompanyID][$IDCard][$rule['id']] = $balloon;
                        $arrPersonIncomeRange[$CompanyID][$IDCard][$rule['id']] = 'Income Over Floor/Upper bound (Balloon Amount) ' . Helper::displayDecimal($balloon) . ' Rate ' . ($TaxRate * 100) . '% Tax:' . Helper::displayDecimal(($TaxRate * $balloon));;
                        $arrPersonGrossStepTax[$CompanyID][$IDCard][$rule['id']] = $StepTax;
                        $arrPersonIncomeRate[$CompanyID][$IDCard][$rule['id']] = $TaxRate;
                        $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = array_sum($arrPersonGrossStepTax[$CompanyID][$IDCard]);

                    } else {
                        //กรณีหักเงินได้ตามอัตรา และมีเงินคงเหลือ ให้นำเงินได้ ที่หักคำนวณได้ในขั้น มาคูณอัตราภาษี
                        $TotalBreakIncome += $IncomeRuleCeil;  //รวมจำนวนเงินได้ที่หัก ไปแล้ว ในอัตราปกติ เพื่อใช้ในการคำนวน ต่อเงินได้ เกินเพดาน

                        if ($RemainIncome >= 0) {
                            $StepTax =  $IncomeRuleCeil * $TaxRate;

                            $IncomeRange = 'C:' . Helper::displayDecimal($ceil) . ' - F:' . Helper::displayDecimal($floor, 2) . ' = Rule:' . Helper::displayDecimal($IncomeRuleCeil);
                            $IncomeRange .= ' Remain:' . Helper::displayDecimal($RemainIncome) . ' Rate:' . ($TaxRate * 100) . '%' . ', Tax:' . Helper::displayDecimal($StepTax);

                            $arrPersonIncomeStep[$CompanyID][$IDCard][$rule['id']] = $IncomeRuleCeil;
                            $arrPersonIncomeRange[$CompanyID][$IDCard][$rule['id']] = $IncomeRange;
                            $arrPersonGrossStepTax[$CompanyID][$IDCard][$rule['id']] = $StepTax;
                            $arrPersonIncomeRate[$CompanyID][$IDCard][$rule['id']] = $TaxRate;
                            $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = array_sum( $arrPersonGrossStepTax[$CompanyID][$IDCard]);

                        }
                        else {
                            //กรณีหักเงินได้ตามอัตรา แต่ไม่พอ ให้นำเงินได้ ก่อนหักคำนวณ มา คูณอัตราภาษี
                            $StepTax =  $InconeBeforeCal * $TaxRate;
                            $_r = ($RemainIncome < 0) ? 0 : $RemainIncome;
                            $IncomeRange = 'C:' . Helper::displayDecimal($ceil) . ' - F:' . Helper::displayDecimal($floor, 2) . ' = Rule:' . Helper::displayDecimal($IncomeRuleCeil);
                            $IncomeRange .= ' Remain:' . Helper::displayDecimal($_r) . ' Rate:' . ($TaxRate * 100) . '%' . ', Tax:' . Helper::displayDecimal($StepTax);

                            $arrPersonIncomeStep[$CompanyID][$IDCard][$rule['id']] = ($r==1) ? $startIncome :  $InconeBeforeCal;
                            $arrPersonIncomeRange[$CompanyID][$IDCard][$rule['id']] = $IncomeRange;
                            $arrPersonGrossStepTax[$CompanyID][$IDCard][$rule['id']] = $StepTax;
                            $arrPersonIncomeRate[$CompanyID][$IDCard][$rule['id']] = $TaxRate;
                            $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = array_sum($arrPersonGrossStepTax[$CompanyID][$IDCard]);
                            break;
                        }
                    }
                    $r++;
                }



            }
        } //end for main loop

        $PND_Record = self::batchInsertIncomeTax($connection,$arrPersonTaxDeduct,$arrPersonDetail,$arrWageConfig,$arrPersonNetIncome,$wage_type,$arrPersonIncomeRange,$arrPersonGrossStepTax,$arrPersonIncomeRate,$arrPersonAccumulateTax,$arrPersonIncomeStep,$pay_date,$PND1_Template);
        return $PND_Record;
    }


    public static function batchInsertIncomeTax($connection,$arrPersonTaxDeduct,$arrPersonDetail,$arrWageConfig,$arrPersonNetIncome,$wage_type,$arrPersonIncomeRange,$arrPersonGrossStepTax,$arrPersonIncomeRate,$arrPersonAccumulateTax,$arrPersonIncomeStep,$datemakesalary,$PND1_Template)
    {


        $arrCompany = ApiOT::getArrayCompany();

        $wageCfg = $arrWageConfig[$wage_type];
        $wage_type = $wageCfg['wage_type'];
        $wage_multiply = $wageCfg['wage_multiply'];

        $my = explode('-',$datemakesalary);
        $this_month = $my[0];
        $this_year = $my[1];

        //$connection = Yii::$app->dbERP_easyhr_PAYROLL;
        $PND_Record = [];
        foreach ($arrPersonNetIncome as $CompanyID => $Emps) {
                $company = $arrCompany[$CompanyID];

                foreach ($Emps as $empIDCard => $emp) {

                    $emp_detail = $arrPersonDetail[$empIDCard];
                    $company_name = $company['name'];
                    $company_tax_code = $company['inv_number'];

                    $position_code = $emp_detail['position_code'];
                    $position_name = $emp_detail['position_name'];

                    $totalPND = 0;
                    $totalPND = array_sum($arrPersonGrossStepTax[$CompanyID][$empIDCard]);
                    $monthPND = $totalPND/12;

                    $salary_wage_monthly = $emp_detail['emp_salary_wage'];
                    $salary_wage_other = $emp_detail['cal_add_money'];
                    $total_monthly = $emp_detail['cal_total_income'];
                    $total_yearly = $emp_detail['total_yearly'];
                    $total_deduction = (isset($arrPersonTaxDeduct[$empIDCard])) ? $arrPersonTaxDeduct[$empIDCard] : 0;
                    $total_income = ($emp < 0) ? 0 : $emp;
                    $total_tax_year = Utility::wage_round_money($totalPND);
                    $total_tax_month = Utility::wage_round_money($monthPND);

                    $PND_Record[$empIDCard] = $total_tax_month;


                    if($total_tax_month > 0) {
                        $arrDeductDetail[] = [
                            'idcard' => $empIDCard,
                            'item_id' => $PND1_Template['ADD_DEDUCT_TEMPLATE_ID'],
                            'item_amount_beforetax'=>$total_tax_year,
                            'item_name' => $PND1_Template['ADD_DEDUCT_TEMPLATE_NAME']. ' ('.$PND1_Template['ADD_DEDUCT_TEMPLATE_NAME'] . ')',
                            'item_amount' => $total_tax_month,
                            'item_detail_extra' => '',
                        ];
                    }


                    $createby_user = Yii::$app->session->get('idcard');
                    $create_datetime = date('Y-m-d H:i:s');

                    $sql_master = "INSERT INTO `tax_calculate` (`id`, `emp_idcard`, `months`, `years`, `pay_date`, `wage_type`, `wage_multiply`,
                    `company_id`, `company_name`, `company_tax_code`, `position_code`, `position_name`,
                    `salary_wage_monthly`, `salary_wage_other`, `total_monthly`, `total_yearly`, `total_deduction`,
                    `total_income`, `total_tax_year`, `total_tax_month`, `createby_user`, `create_datetime`) VALUES
                    (NULL, '$empIDCard', '$this_month', '$this_year', '$datemakesalary', '$wage_type', '$wage_multiply',
                    '$CompanyID', '$company_name', '$company_tax_code', '$position_code', '$position_name' ,'$salary_wage_monthly' , '$salary_wage_other',
                    '$total_monthly' , '$total_yearly', '$total_deduction', '$total_income', '$total_tax_year', '$total_tax_month', '$createby_user', '$create_datetime');";

                    $eff = $connection->createCommand($sql_master)->execute();

                    $tax_calculate_id = $connection->getLastInsertID();

                    $_table_detail = 'tax_calculate_step';
                    $_arrDetailCol = TaxCalculateStep::getTableSchema()->getColumnNames(); //['id', 'tax_calculate_id', 'tax_income_structure_id', 'income_range', 'tax_rate', 'income_step', 'gross_step_tax', 'total_accumulate_tax'];

                    $eff = $idx = 0;
                    $_arr_data = null;

                    $arrEmpIncomeRange = $arrPersonIncomeRange[$CompanyID][$empIDCard];
                    $arrEmpIncomeStep = $arrPersonIncomeStep[$CompanyID][$empIDCard];
                    $arrEmpGrossStepTax = $arrPersonGrossStepTax[$CompanyID][$empIDCard];
                    $arrEmpIncomeRate = $arrPersonIncomeRate[$CompanyID][$empIDCard];
                    $arrEmpAccumulateTax = $arrPersonAccumulateTax[$CompanyID][$empIDCard];

                    foreach ($arrEmpIncomeRange as $ruleID => $value) {
                        $_arr_data[] = [
                            $_arrDetailCol[0] => NULL, //id
                            $_arrDetailCol[1] => $empIDCard,//emp_idcard
                            $_arrDetailCol[2] => $datemakesalary,//pay_date
                            $_arrDetailCol[3] => $tax_calculate_id,  //tax_calculate_id
                            $_arrDetailCol[4] => $ruleID, //tax_income_structure_id
                            $_arrDetailCol[5] => $value,   //income_range
                            $_arrDetailCol[6] => $arrEmpIncomeRate[$ruleID],   //tax_rate
                            $_arrDetailCol[7] => $arrEmpIncomeStep[$ruleID],   //income_step
                            $_arrDetailCol[8] => $arrEmpGrossStepTax[$ruleID],   //gross_step_tax
                            $_arrDetailCol[9] => $arrEmpAccumulateTax[$ruleID],   //total_accumulate_tax
                        ];

                    }

                    $connection->createCommand()->batchInsert($_table_detail, $_arrDetailCol, $_arr_data)->execute();

                } //for emps
            }// main


        /** ---------------------------------------------------------------------------
         *  บันทึกรายการลงตาราง  ADD_DEDUCT_THIS_MONTH  เป็นรายการหักใน slip เงินเดือน
         *----------------------------------------------------------------------------*/
        if(count($arrDeductDetail)  > 0 )
            ApiSalary::AdjustAddDeductDetail($connection, $datemakesalary, $arrDeductDetail, $PND1_Template);

        return $PND_Record;
    }


    public static function getArrayIncomeStructure()
    {
        $model = TaxIncomeStructure::find()->where([
            'status_active' => 1
        ])->asArray()->orderBy('income_floor ASC')->all();

        $arrIncomeStructure = [];
        if ($model != null) {
            foreach ($model as $item) {
                $arrIncomeStructure[] = $item;
            }
        }

        return $arrIncomeStructure;
    }


    public static function getPayrollConfigWage()
    {
        $models = PayrollConfigWage::find()->where([
            'status_active'=>1,
        ])->orderBy('wage_type ASC')->asArray()->all();

        $arrWage = [];
        foreach ($models as $item) {
            $arrWage[$item['wage_type']] = $item;
        }

        return $arrWage;
    }



    //config deduction
    public static function configDeduction()
    {


        $arrDeductLimit =[
            'father'    => [
                'amt'=>1,
                'deduct'=>30000,
                'limit'=>30000
            ],
            'mother'    => [
                'amt'=>1,
                'deduct'=>30000,
                'limit'=>30000
            ],
            'child'     => [
                'amt'=>4,
                'deduct'=>30000,
                'limit'=>30000
            ],
            'spouse'    => [
                'amt'=>1,
                'deduct'=>60000,
                'limit'=>60000
            ],
            'tax_personal'   => [
                'amt'=>1,
                'deduct'=>100000,
                'limit'=>100000
            ],
            'tax_keepback'=> [
                'amt'=>1,
                'deduct'=>100000,
                'limit'=>100000
            ],
            'tax_rating' => [],
            'tax_teachers' => [],
            'tax_rmf' => [],
            'tax_ltf' => [],
            'tax_increase_home'=> [],
            'tax_social'=>[],
            'tax_education'=>[],

        ];
    }


    //รายการลดหย่อนส่วนตัว
    public static function getEmpTaxDeduction()
    {
        $model = TaxIncome::find()->orderBy('emp_id ASC')->asArray()->all();
        $arrEmpDeduct = [];
        foreach ($model as $item) {
            $arrEmpDeduct[$item['emp_id']] = $item;
        }

        $emps = Empdata::find()->select('DataNo,ID_Card')->orderBy('DataNo ASC')->asArray()->all();
        $arrEmp = [];
        foreach ($emps as $item) {
            $dataNo = $item['DataNo'];
            $arrEmp[$item['ID_Card']] = (isset($arrEmpDeduct[$dataNo])) ? $arrEmpDeduct[$dataNo] : 0;
        }


        $models = DISCOUNTINCOMETAX::find()->select('idcard,total_discount')->asArray()->all();
        $arrEmp = [];
        foreach ($models as $item) {
            $arrEmp[$item['idcard']] = $item['total_discount'];
        }

        return $arrEmp;
    }

    //รายการลดหย่อนบิดา มารดา
    public static function getEmpFamilyDeduction()
    {
        $model = Empfamily::find()->orderBy('emp_id ASC')->asArray()->all();
        $arrEmpDeduct = [];
        foreach ($model as $item) {
            $arrEmpDeduct[$item['emp_data_id']] = $item;
        }
        return $arrEmpDeduct;
    }


    //

}

?>