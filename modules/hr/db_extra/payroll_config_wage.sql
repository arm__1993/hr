-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 17, 2017 at 11:20 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `payroll_config_wage`
--

CREATE TABLE `payroll_config_wage` (
  `id` tinyint(3) NOT NULL COMMENT 'รหัสลำดับ',
  `wage_type` tinyint(3) NOT NULL COMMENT 'ประเภทการจ่ายเงินเดือน 1=ราย7วัน,2=ราย 15วัน,3=รายเดือน',
  `wage_day` tinyint(3) NOT NULL COMMENT 'รอบจำนวนเงินที่จ่ายเงิน 7=ราย7วัน, 15=ราย15วัน,30=ราย30วัน',
  `wage_multiply` tinyint(3) NOT NULL COMMENT 'ตัวคูณเพื่อคำนวณภาษีตามประเภทการจ่าย 52=ราย 7วัน,24=ราย15วัน,12= รายเดือน',
  `wage_type_name` varchar(150) DEFAULT NULL COMMENT 'ประเภทการจ่ายเงินเดือน ',
  `is_endofmonth` tinyint(3) NOT NULL COMMENT 'flag บอกว่ารายการนี้คำนวณภาษี ณ สิ้นเดือน, 1=ใช่, 0=ไม่ใช่',
  `status_active` tinyint(3) NOT NULL COMMENT 'รหัสประเภทการหักเงินค้ำประกัน',
  `createby_user` char(13) DEFAULT NULL COMMENT 'เลขประจำตัวผู้สร้าง',
  `create_datetime` datetime DEFAULT NULL COMMENT 'สร้างรายการเมื่อวันที่เวลา',
  `updateby_user` char(13) DEFAULT NULL COMMENT 'เลขประจำตัวผู้อัพเดท',
  `update_datetime` datetime DEFAULT NULL COMMENT 'อัพเดทรายการเมื่อวันที่เวลา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payroll_config_wage`
--

INSERT INTO `payroll_config_wage` (`id`, `wage_type`, `wage_day`, `wage_multiply`, `wage_type_name`, `is_endofmonth`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, 1, 7, 52, 'รอบ 7 วัน', 0, 1, '3560500575223', '2017-06-24 00:00:00', NULL, NULL),
(2, 2, 15, 24, 'รอบ 15 วัน', 0, 1, '3560500575223', '2017-06-24 00:00:00', NULL, NULL),
(3, 3, 30, 12, 'รอบ 30 วัน', 1, 1, '3560500575223', '2017-06-24 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payroll_config_wage`
--
ALTER TABLE `payroll_config_wage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_active` (`status_active`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payroll_config_wage`
--
ALTER TABLE `payroll_config_wage`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'รหัสลำดับ', AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
