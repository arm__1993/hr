<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host='.$DB_HOST.';dbname=ERP_service',
    'username' => $DB_USER,
    'password' => $DB_PWD,
    'charset' => 'utf8',
];
