<?php

namespace app\modules\webreport\controllers;

// use app\api\ApiReport;
use app\api\Common;
use app\api\DateTime;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use mPDF;


use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiReceivewage;

class ReceivewageController extends Controller
{
    public $layout = 'webreportlayouts';

    public function actionWage()
    {
        //return $this->render('wage');
        return $this->render('wage', ['query' => false,]);
    }

    public function actionPfm_mechanic()
    {
        //return $this->render('pfm_mechanic');
        return $this->render('pfm_mechanic', ['query' => false,]);
    }


    public function actionReceivewage()
    {
        $postYear = Yii::$app->request->post();
        $CheckYearPost = $_POST['year'];
        //print_r($CheckYearPost);
        if (!isset($CheckYearPost)) {
            // $messagenodate = "กรุณาเลือกปีที่จะค้นหา";
            // echo "<script type='text/javascript'>alert('$messagenodate');</script>";
            // return $this->render('wage');
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('wage', ['query' => false,]);
        }

        $GET_year = $postYear['year'];
        $maxYear = max($postYear['year']);
        $checkyear = date("Y");//เช็คปีปัจจุบัน

        $postCompany = Yii::$app->request->post();
        $GET_company = $postCompany['company'];

        $type_company = $GET_company;
        $selected_company = $type_company;
        foreach ($GET_year as $row) {
            $verify[] = $row;
        }

        array_push($verify, $checkyear);
        $verify = array_unique($verify); // array_unique รวมตัวแปรที่มีเหมือนกัน
        $arr_checkyear[] = $checkyear;


        $checkyear_forpricewage = "%$checkyear%"; // "%2016%"


        $sumpricewage_yearnow = ApiReceivewage::getSumPriceWageYearNow($checkyear_forpricewage, $type_company);


        if (empty($sumpricewage_yearnow)) {
            // $message = "ไม่มีข้อมูล";
            // echo "<script type='text/javascript'>alert('$message');</script>";
            return $this->render('wage');
        } else {
            for ($i = 0; $i < count($sumpricewage_yearnow); $i++) {
                $pw_yearnow[] = ($sumpricewage_yearnow[$i]['pricewage']);
            }


            $arr_sumtarget = ApiReceivewage::getArrSumTarget($checkyear, $type_company);

            //$checkarray = array();
            foreach ($verify as $val) {
                $parts[] = "" . ($val) . "";
                $stringyear = implode(',', $parts);
                $stringyear = '(' . $stringyear . ')'; //เปลี่ยนปีให้อยู่ในรูปแบบ (2015,2016,)
            }


            $checkarray = ApiReceivewage::getCheckArray($stringyear, $type_company);


            $arrKeyYear = array_keys($checkarray);
            foreach ($checkarray as $value) {
                $arr_resultYear[] = ($value);
            }

            $yearnow = array_sum($pw_yearnow);//ผลรวม pricewage ปีล่าสุด (นำไปหาค่าแรงเทียบเป้าสะสม)
            $sumtarget = array_sum($arr_sumtarget);//ผลรวมของเป้าหมายทั้งหมด

            $numberMax = count($arr_resultYear) - 1;
            $numberLasr = ($numberMax - 1);


            $arrDataAVG = ApiReceivewage::getArrDataAVG($checkyear, $type_company);


            if (count($arr_resultYear) == 1) {// กรณีมีปีเดียว
                $Wages = 0; //ปีเดียวรายได้ค่าแรงเทียบกับปีก่อนได้ = 0
                $sumwTatget = 0; //ยอดเป้าสะสม
                $sumwageNow = (array_sum($arr_resultYear[$numberMax]));//รายได้ปีล่าสุด
                for ($i = 0; $i < count($arr_resultYear[$numberMax]); $i++) {//
                    $sumwTatget += ($arrDataAVG[$i]);
                }

                $calTarget = (($sumwageNow / $sumwTatget) * 100);
                $Targets = (number_format($calTarget, 2, '.', ','));

            } else {
                $sumwageNow = (array_sum($arr_resultYear[$numberMax]));//รายได้ปีล่าสุด
                $sumwageLast = 0; //รายได้ปีก่อน
                $sumwTatget = 0; //ยอดเป้าสะสม
                for ($i = 1; $i <= count($arr_resultYear[$numberMax]); $i++) {//
                    $sumwageLast += ($arr_resultYear[$numberLasr][$i]);
                }
                $calWage = ((($sumwageLast - $sumwageNow) / $sumwageNow) * 100);
                $Wages = (number_format($calWage, 2, '.', ','));

                for ($i = 0; $i < count($arr_resultYear[$numberMax]); $i++) {//
                    $sumwTatget += ($arrDataAVG[$i]);
                }

                if($sumwTatget==0)
                {
                    $sumwTatget = 1;
                }else
                {
                    $sumwTatget = $sumwTatget ;
                }
                $calTarget = (($sumwageNow / $sumwTatget) * 100);
                $Targets = (number_format($calTarget, 2, '.', ','));
            }

            //print_r($Wages); //รายได้สะสม
            //print_r($Targets); //ยอดเป้าสะสม

            $session = Yii::$app->session;

            $session->set('maxYear', $maxYear);
            $session->set('pw_yearnow', $pw_yearnow);
            $session->set('arrDataAVG', $arrDataAVG);
            $session->set('checkarray', $checkarray);
            $session->set('Wages', $Wages);
            $session->set('Targets', $Targets);


            return $this->render('wage',
                ['maxYear' => $maxYear,
                    'pw_yearnow' => $pw_yearnow, //sum pricewage yearnow
                    'arrDataAVG' => $arrDataAVG,
                    'checkarray' => $checkarray, // ข้อมูลปี เดือน ของปีที่รับมา
                    'Wages' => $Wages,
                    'Targets' => $Targets,
                    'query' => true,
                    'selected_company' => $selected_company,
                    'selected_year' => $GET_year,
                ]);
        }
    }

    public function actionReceivepfm()
    {
        $postyear = Yii::$app->request->post();
        $year = ($postyear['year']);

        $post_technician = Yii::$app->request->post();
        $technician = ($post_technician['technician']);

        $postCompany = Yii::$app->request->post();
        $GET_company = $postCompany['company'];
        //$type_company = ApiReport::TypeCompany($GET_company);
        $type_company = $GET_company;
        $postMoths = Yii::$app->request->post();
        $months = ($postMoths['month']);

        if (strlen($months) == 1) {
            $months = "0" . $months;
        } else {
            $months;
        }

        $yearAD = $year[0]; //เปลี่ยน พ.ศ => ค.ศ
        $stringyear = $yearAD . "-" . $months . "-" . "1";

        $allday = date("t", strtotime(".$stringyear."));//แสดงจำนวนวันทั้งหมด ของเดือนที่รับมา
        $checkyear = "'" . $yearAD . "-" . $months . "%'";// ปี - เดือน %
        $technician = "'%" . $technician . "%'"; //ช่างรถใหญ่ , ช่างรถเล็ก

        $GET_company . "<br>";
        $selected_months = $postMoths['month'] . "<br>";
        $selected_year = $year["0"];
        $selected_technician = $post_technician['technician'];
        // exit();

        // หาจำนวนช่างต่อวัน........

        $mechanic = ApiReceivewage::getMechanic($checkyear, $technician, $type_company);

        if (empty($mechanic)) {
            // $message = "ไม่มีข้อมูลบริษัท";
            // echo "<script type='text/javascript'>alert('$message');</script>";
            // return $this->render('pfm_mechanic');
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('pfm_mechanic',
                ['selected_company' => $GET_company,
                    'selected_months' => $selected_months,
                    'selected_year' => $selected_year,
                    'selected_technician' => $selected_technician,
                    'query' => false,]
            );
        } else {

            $technician_ofday = count($mechanic);//ช่างทั้งหมดในเดือนนั้น
            $StartDate = "$yearAD-$months-01"; //ใช้ในAPI helper
            $EndDate = "$yearAD-$months-$allday"; //ใช้ในAPI helper
            $_arrRET = Helper::calculateSunday($StartDate, $EndDate);//จำนวนวันที่ทำงาน และ หยุดงาน

            //array_intersect เอาตัวที่ซ้ำกัน
            //array_diff เอาตัวที่ไม่ซ้ำกัน


            $sumwage = ApiReceivewage::getSumWage($checkyear, $type_company);


            $technician_Workday = ApiReceivewage::getTechnicianWorkday($checkyear, $technician, $type_company);


            $arry = $arr_workday = [];
            foreach ($technician_Workday as $row) {
                $arry[] = $arr_workday[$row['EMP']][] = $row['workdate'];

            }

            $MonthDays = date("t", strtotime($StartDate)); //แสดงเจำนวนวันดือนที่รับมา

            $firstDay = $startDay = strtotime($StartDate);
            $lastDay = strtotime($EndDate);

            for ($i = 1; $i <= $MonthDays; $i++) {
                $Day[$i][] = date('Y-m-d', $firstDay); //เป็นรูปแบบวันเป็น Y-m-d

                if ($firstDay) {
                    $a_workday[] = count(array_intersect($arry, $Day[$i]));
                }
                $firstDay += 86400;
            }

            $arrTechnicianInDay = [];
            for ($day = $startDay; $day <= $lastDay; $day += 86400) {
                $checkDate = date("Y-m-d", $day);
                $arrTechnicianInDay[$checkDate] = 0;
                foreach ($arr_workday as $key => $value) {
                    $arrTotalDay = $value;
                    foreach ($arrTotalDay as $date) {
                        if ($checkDate == $date) {
                            $arrTechnicianInDay[$checkDate] += 1;
                        } else {
                            $arrTechnicianInDay[$checkDate] += 0;
                        }
                    }
                }

            }

            foreach ($_arrRET['array_sunday'] as $sunday) {
                $weekend[] = ($arrTechnicianInDay[$sunday]); //มาทำงานวันอาทิตย์


            }
            foreach ($_arrRET['array_workday'] as $workday) {
                $workingday[] = ($arrTechnicianInDay[$workday]); //มาทำงานวันปกติ

            }


            $year = ApiReceivewage::getYear();


            $session = Yii::$app->session;

            $session->set('weekend', $weekend);
            $session->set('workingday', $workingday);
            $session->set('_arrRET', $_arrRET);
            $session->set('sumwage', $sumwage);

            return $this->render('pfm_mechanic', [
                '_arrRET' => $_arrRET,
                'year' => $year,
                'technician_ofday' => $technician_ofday, //ช่างทั้งหมด
                'sumwage' => $sumwage,
                'a_workday' => $a_workday, //ช่างที่มาทำงานในแต่ละวันของเดือน
                'weekend' => $weekend,
                'workingday' => $workingday,
                'query' => true,
                'selected_company' => $GET_company,
                'selected_months' => $selected_months,
                'selected_year' => $selected_year,
                'selected_technician' => $selected_technician,

            ]);

        }

    }

    public function actionBtn_report()/*05-10-2559 th mpdf*/
    {

        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_report'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportpfm()/*05-10-2559 th mpdf*/
    {


        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();

        $mpdf->WriteHTML($this->renderPartial('_reportpfm'));
        $mpdf->Output();
        exit;

    }


}
