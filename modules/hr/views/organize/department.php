<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:26
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\modules\hr\apihr\ApiHr;

$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/department.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/filter_input.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);

?>
<section class="content">
    <!-- Default box -->
    <input type='hidden' id='companyid' value='<?php echo $datacompany[0]['id']; ?>'/>
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li>
                    <a href="company"><?php echo 'ข้อมูลบริษัท' ?></a>
                </li>
                <li>
                    <a href="department?id=<?php echo $datacompany[0]['id']; ?>"><?php echo $datacompany[0]['name']; ?></a>
                </li>
                <li class="active">แผนก</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box box-warning">
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">บริษัท</div>
                    <div class="col-md-9"><?php echo $datacompany[0]['name']; ?></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">ชื่อย่อบริษัท</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['short_name']; ?></u></div>
                    <div class="col-md-2">รหัสบริษัท</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['code_name']; ?></u></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">ที่อยู่บริษัท</div>
                    <div class="col-md-9"><u><?php echo $datacompany[0]['address']; ?></u></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">เบอร์โทรศัพท์</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['Tel']; ?></u></div>
                    <div class="col-md-2">Fax.</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['Fax']; ?></u></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">เลขประจำตัวผู้เสียภาษี</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['inv_number']; ?></u></div>
                    <div class="col-md-2">เลขบัญชีประกันสังคม</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['soc_acc_number']; ?></u></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">ประเภทบริษัท</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['branch_comment']; ?></u></div>
                    <div class="col-md-2">Dealercode</div>
                    <div class="col-md-2"><u><?php echo $datacompany[0]['Code']; ?></u></div>
                </div>
                <br>
                <div class="box box-warning"></div>
            </div>
            <div class="row" id="gridDepartment">
                <div class="box-body">
                    <div class="row">
                        <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-10">

                                </div>
                                <div class="col-md-2">
                                    <a data-toggle="modal" onclick="reset()"><img src="<?php echo $imghr; ?>/add.png"
                                                                                  class="img-circle">
                                        <span>เพิ่มแผนก</span></a>
                                </div>
                            </div>

                            <?php {
                                Pjax::begin(['id' => 'pjax_grid_department']);
                                echo GridView::widget([
                                    'dataProvider' => $dataProvider_department,
                                    'filterModel' => $modelSearch,
                                    'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn',
                                            'headerOptions' => ['width' => '20'],
                                        ],
                                        [
                                            'attribute' => 'departmentcode_name',
                                            'label' => 'รหัสแผนก',
                                            'value' => 'departmentcode_name',
                                            //'format' => 'raw', 
                                            //'filter' => true, 
                                            'contentOptions' => ['style' => 'width: 120px;', 'align=center']
                                        ],
                                        [
                                            'attribute' => 'departmentname',
                                            'label' => 'ชื่อแผนก',
                                            'value' => 'departmentname',
                                            //'format' => 'raw', 
                                            //'filter' => true, 
                                            'contentOptions' => ['style' => 'width: 120px;', 'align=center']
                                        ],
                                        [
                                            'headerOptions' => ['width' => '100'],
                                            'class' => 'yii\grid\ActionColumn',
                                            'header' => 'จัดการแผนก',
                                            'template' => '{update}{delete}',
                                            'buttons' => [
                                                'update' => function ($url, $data) use ($datacheck) {
                                                    if (!in_array($data->departmentid, $datacheck)) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                            'title' => 'แก้ไข',
                                                            'onclick' => '(function($event) {
                                                                        editDepartmentByid_get(' . $data->departmentid . ');
                                                                })();'
                                                        ]);
                                                    }
                                                },

                                                'delete' => function ($url, $data) use ($datacheck) {
                                                    if (!in_array($data->departmentid, $datacheck)) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                            'title' => 'ลบ',
                                                            'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->departmentname . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deleteDeparmentByid(' . $data->departmentid . ');
                                                                                }
                                                                            }
                                                                        });
                                                                
                                                                
                                                                })();'
                                                        ]);
                                                    }
                                                },
                                            ],
                                        ],
                                        [
                                            'headerOptions' => ['width' => '100'],
                                            'class' => 'yii\grid\ActionColumn',
                                            'header' => 'เพิ่มฝ่าย',
                                            'template' => '{update}',
                                            'buttons' => [
                                                'update' => function ($url, $data) {
                                                    $urls = Url::to([
                                                        'organize/department',
                                                        'id' => $data->id
                                                    ]);
                                                    return Html::button('ข้อมูลฝ่าย', $options = [
                                                        'onclick' => '(function($event) {
                                                                showFormsection(' . $data->departmentid . ',' . $data->companyid . ');
                                                            })();'

                                                    ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]);
                                Pjax::end(); //end pjax_gridcorclub
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
<div class="modal fade" id="addDialog" tabindex="-1" role="dialog" aria-labelledby="ConfirmDialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ยืนยันการบันทึก</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="AddDeparmentForm" onsubmit="return getdatesubmit();" data-toggle="validator" method="post"
                      class="form-horizontal">
                    <input class="form-control" id="company" value="<?php echo $datacompany[0]['id']; ?>" type="hidden"
                           name="company"/>
                    <input value="0" type="hidden" id="check" name="check"/>
                    <input value="0" type="hidden" id="id" name="id"/>
                    <div class="row">
                        <div class="col-md-3">ชื่อแผนก</div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">รหัสแผนก</div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="code_name" maxlength="2"  onkeypress="return KeyCode(event);" name="code_name">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6"><u>ตัวอย่าง</u>HR ใส่เป็นตัวอังกฤษเท่านั้น</div>
                    </div>


                    <br>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="button" id="Btn_save" class="btn btn-primary">บันทึก</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
