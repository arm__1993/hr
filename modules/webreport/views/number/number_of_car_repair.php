
<?php

/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 6/10/2559
 * Time: 14:35
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );
    $("#example2").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    }
    );
  });

  $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });
    $('#reservation2').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });

});
JS;


$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานจำนวนรถที่ซ่อมต่อช่าง</h3>


    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>        
        
        <div class="box-body">

                <div class="row">

                <form method="post" action="car_repairman">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key=>$value) {
                                $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                            }
                            ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                         <label>ช่วงวันที่</label>
                        &nbsp;
                          <div class="btn-group">                            
                            <div class="input-group" style="width: 250px">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" name="reservation" value="<?php print_r($selected_daterange); ?>" id="reservation" data-provide="datepicker" data-date-language="th">
                            </div>
                          </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;

                    <label>เลือกช่าง</label>
                    &nbsp;
                    <div class="btn-group">
                    <input type="radio" name="technician" value="1" checked> ช่างรถเล็ก
                    &nbsp;
                    <input type="radio" name="technician" value="2"> ช่างรถใหญ่
                    </div>
                    </center>
                    &nbsp;&nbsp;&nbsp;&nbsp;                       
                        <center><br>
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/number/btn_reportnum">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                        </center>
                    </div>
                </form>
            </div>
        </div><!--/box-body-->

    <?php if(!empty($result_total)){ ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            วันที่
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending">
                                                จำนวนรถซ่อม <?php if($technician_car == 1){
                                                    echo "รถเล็ก";
                                                }else
                                                {
                                                    echo "รถใหญ่";
                                                }

                                                 ?>   
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                จำนวนช่าง
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                จำนวนคันที่ซ่อมต่อช่าง 1 คน
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php 
                                        foreach ($result_total as $key => $value) {
                                        ?>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">
                                        <?php echo $key ;?>
                                        </td>                                        
                                        <td>
                                         <?php echo $value['0'] ;?>
                                        </td>
                                        <td>
                                        <?php echo $value['1'] ;?>
                                        </td>
                                        <td>
                                        <?php $sum_result =  $value['0']/$value['1'] ;
                                            echo Helper::displayDecimal($sum_result);
                                        ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
 



    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                    <?php
                                   
                                        $arr_date = [] ; 
                                        $arr_sumtotal = [] ;
                                        foreach ($result_total as $key => $value) {
                                        $arr_date [] = $key; 
                                        $sum_result =  $value['0']/$value['1'] ;
                                        $arr_sumtotal [] =$sum_result;    
                                       
                                  } 
                                 
                                            echo Highcharts::widget([
                                                'options' => [
                                                    'title' => [
                                                        'text' => ''
                                                    ],
                                                    'xAxis' => [
                                                        'categories' => $arr_date,
                                                    ],
                                                    'labels' => [
                                                        'items' => [
                                                            'html' => 'Total fruit consumption',
                                                            'style' => [
                                                                'left' => '50px',
                                                                'top' => '18px',
                                                                'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                            ]
                                                        ]
                                                    ],
                                                    'credits' => [
                                                        'enabled' => false
                                                    ],

                                                    'series' => [
                                                        [
                                                            'type' => 'column',
                                                            'name' => 'จำนวนคันที่ซ่อมต่อช่าง 1 คน',
                                                            'data' => $arr_sumtotal,
                                                        ],

                                                    ]
                                                ]
                                            ]);
                                            ?>
                                    </div>
                                </div><!--row-->
                        </div>
                    </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA CHART -->
            </div>
        </div>
    </section>

</div><!--box-->
   <?php } ?>

<!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>        
        
        <div class="box-body">

                <div class="row">

                <form method="post" action="carrepairofmonth">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                    <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                             <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key=>$value) {
                                $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                            }
                            ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                         <label>ช่วงวันที่</label>
                        &nbsp;
                          <div class="btn-group">                            
                            <div class="input-group" style="width: 250px">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" value="<?php print_r($selected_daterange); ?>" name="reservation2" id="reservation2" data-provide="datepicker" data-date-language="th">
                            </div>
                          </div>
                        &nbsp;&nbsp;                                     

                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        &nbsp;
                        <a href="<?php echo SITE_URL;?>/number/exportnum">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-file-excel-o"></i> ออกรายงาน Excel
                        </button>
                        </a>
                        </center>
                    </div>
                </form>
            </div>
        </div><!--/box-body-->
<?php if(!empty($carRepairman)){ ?>
<section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-warning">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            ชื่อ ช่าง
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending">
                                                จำนวนคันที่ซ่อม
                                            </th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($carRepairman as $value) {
                                        ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                            <?php
                                            echo ($value['tc_name']) ? $value['tc_name'] : 0;
                                            ?>
                                            </td>
                                            <td>
                                            <?php
                                            echo ($value['amount_car']) ? $value['amount_car'] : 0;
                                            ?>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-warning-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php } ?>
</div><!--/box-->
 <?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>