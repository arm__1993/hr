-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 01, 2017 at 09:01 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-10+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_checktime`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_manage_request`
--

CREATE TABLE `menu_manage_request` (
  `id` int(11) NOT NULL,
  `menu_id` int(20) DEFAULT NULL COMMENT 'id จาก menu',
  `working_company_id` int(20) DEFAULT NULL COMMENT 'id บริษัท จาก working_company',
  `department_id` int(20) DEFAULT NULL COMMENT 'id จาก department',
  `section_id` int(20) DEFAULT NULL COMMENT 'id จาก section',
  `position_id` int(20) DEFAULT NULL COMMENT 'id position',
  `id_card` varchar(13) DEFAULT NULL COMMENT 'ระบุรายบุคคล มีเฉพาะฟิลด์นี้ ฟิลด์ company จะว่าง',
  `skill` varchar(1) DEFAULT NULL COMMENT 'r: อ่านอย่างเดียว w: เขียนได้ด้วย',
  `change_to` int(1) DEFAULT NULL COMMENT '1 ขอเปิด 2 ขอปิด',
  `approve_status` int(1) DEFAULT NULL COMMENT '0:รอ, 1:อนุมัติ, 2:ไม่อนุมัติ',
  `date_request` datetime DEFAULT NULL COMMENT 'วันที่สร้างคำขอ',
  `date_approve` datetime DEFAULT NULL COMMENT 'วันที่อนุมัติ',
  `reason_request` varchar(250) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL COMMENT '99 คือ ไม่เช็ค'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_manage_request`
--
ALTER TABLE `menu_manage_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `working_company_id` (`working_company_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `id_card` (`id_card`),
  ADD KEY `position_id` (`position_id`),
  ADD KEY `menu_id_index` (`menu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_manage_request`
--
ALTER TABLE `menu_manage_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
