<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "config_mapping_sso".
 *
 * @property integer $id
 * @property integer $id_sso
 * @property string $namestatussso
 * @property string $namestatushr
 * @property integer $status
 * @property string $createby
 * @property string $createdate
 * @property string $updateby
 * @property string $updatedate
 */
class ConfigMappingSso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config_mapping_sso';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sso', 'status'], 'integer'],
            [['status'], 'required'],
            [['createdate', 'updatedate'], 'safe'],
            [['namestatussso', 'namestatushr'], 'string', 'max' => 250],
            [['createby', 'updateby'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sso' => 'Id Sso',
            'namestatussso' => 'Namestatussso',
            'namestatushr' => 'Namestatushr',
            'status' => 'Status',
            'createby' => 'Createby',
            'createdate' => 'Createdate',
            'updateby' => 'Updateby',
            'updatedate' => 'Updatedate',
        ];
    }


    public function search($params)
    {


        $query = ConfigMappingSso::find()->where('status != :del', [':del' => Yii::$app->params['DELETE_STATUS']]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>['pageSize' => 10,],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        $query->andFilterWhere(['like', 'namestatussso', $this->namestatussso])
                ->andFilterWhere(['like', 'namestatushr', $this->namestatushr]);
        return $dataProvider;
    }



}
