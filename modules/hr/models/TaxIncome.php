<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "tax_income".
 *
 * @property integer $id
 * @property integer $emp_id
 * @property string $tax_personal
 * @property string $tax_keepback
 * @property string $tax_rating
 * @property string $tax_teachers
 * @property string $tax_rmf
 * @property string $tax_ltf
 * @property string $tax_increase_home
 * @property string $tax_social
 * @property string $tax_education
 * @property integer $tax_income_spouse_status
 * @property integer $tax_income_with_spouse_status
 * @property string $tax_income_spouse
 * @property string $tax_insurance_spouse
 * @property integer $tax_income_children_status
 */
class TaxIncome extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_income';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emp_id', 'tax_income_spouse_status', 'tax_income_with_spouse_status', 'tax_income_children_status'], 'integer'],
            [['tax_personal', 'tax_keepback', 'tax_rating', 'tax_teachers', 'tax_rmf', 'tax_ltf', 'tax_increase_home', 'tax_social', 'tax_education', 'tax_income_spouse', 'tax_insurance_spouse'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_id' => 'Emp ID',
            'tax_personal' => 'Tax Personal',
            'tax_keepback' => 'Tax Keepback',
            'tax_rating' => 'Tax Rating',
            'tax_teachers' => 'Tax Teachers',
            'tax_rmf' => 'Tax Rmf',
            'tax_ltf' => 'Tax Ltf',
            'tax_increase_home' => 'Tax Increase Home',
            'tax_social' => 'Tax Social',
            'tax_education' => 'Tax Education',
            'tax_income_spouse_status' => 'Tax Income Spouse Status',
            'tax_income_with_spouse_status' => 'Tax Income With Spouse Status',
            'tax_income_spouse' => 'Tax Income Spouse',
            'tax_insurance_spouse' => 'Tax Insurance Spouse',
            'tax_income_children_status' => 'Tax Income Children Status',
        ];
    }
}
