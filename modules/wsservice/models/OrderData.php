<?php

namespace app\modules\wsservice\models;

use Yii;
use yii\rest\ActiveController;

/**
 * This is the model class for table "order_data".
 *
 * @property integer $id
 * @property integer $order_type
 * @property string $order_date
 * @property string $order_amount
 */
class OrderData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_type', 'order_date', 'order_amount'], 'required'],
            [['order_type'], 'integer'],
            [['order_date'], 'safe'],
            [['order_amount'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_type' => 'Order Type',
            'order_date' => 'Order Date',
            'order_amount' => 'Order Amount',
        ];
    }

    /**
     * @inheritdoc
     * @return OrderDataQuery the active query used by this AR class.
     */
     public function searchbytitle($keyword)
    {
        $model = OrderData::find()
        ->where(['like', 'id', $keyword])
        ->all();
        return $model;
        
    }
}
