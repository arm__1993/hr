-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 18, 2017 at 10:12 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.27-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `EMP_ADDRESS`
--

CREATE TABLE `EMP_ADDRESS` (
  `id` int(11) NOT NULL COMMENT 'auto run',
  `ADDR_DATA_NO` int(11) DEFAULT NULL COMMENT 'หมายเลขพนักงาน',
  `ADDR_VILLAGE` varchar(100) DEFAULT NULL COMMENT 'ชื่ออาคาร/หมู่บ้าน',
  `ADDR_ROOM_NO` varchar(50) DEFAULT NULL COMMENT 'ห้องเลขที่',
  `ADDR_FLOOR_NO` varchar(50) DEFAULT NULL COMMENT 'ชั้นที่',
  `ADDR_NUMBER` varchar(50) DEFAULT NULL COMMENT 'เลขที่',
  `ADDR_GROUP_NO` varchar(50) DEFAULT NULL COMMENT 'หมู่ที่ 7',
  `ADDR_LANE` varchar(50) DEFAULT NULL COMMENT 'ตรอก/ซอย',
  `ADDR_ROAD` varchar(150) DEFAULT NULL COMMENT 'ถนน',
  `ADDR_SUB_DISTRICT` varchar(100) DEFAULT NULL COMMENT 'ตำบล/แขวง',
  `ADDR_DISTRICT` varchar(100) DEFAULT NULL COMMENT 'อำเภอ/เขต',
  `ADDR_PROVINCE` varchar(100) DEFAULT NULL COMMENT 'จังหวัด',
  `ADDR_POSTCODE` varchar(100) DEFAULT NULL COMMENT 'รหัสไปรษณีย์',
  `ADDR_GEOGRAPHY` varchar(10) DEFAULT NULL COMMENT 'ภาค ',
  `ADDR_GPS_LATITUDE` varchar(255) DEFAULT NULL COMMENT 'GPS ละติจูด ',
  `ADDR_GPS_LONGTITUDE` varchar(255) DEFAULT NULL COMMENT 'GPS ลองติจูด ',
  `ADDR_MAP_DESC` text COMMENT 'คำอธิบายแผนที่ ',
  `ADDR_REMARK` text COMMENT 'หมายเหตุ',
  `ADDR_TYPE` int(2) DEFAULT NULL COMMENT '1.ที่อยู่ตามทะเบียนบ้าน,2.ที่อยู่ตามบัตรประชาชน, 3.ที่อยู่ที่สามารถติดต่อได้',
  `ADDR_MAIN_ACTIVE` int(2) DEFAULT NULL COMMENT 'ต้องระบุที่อยู่หลัก',
  `ADDR_REF_MAIN` tinyint(1) DEFAULT NULL COMMENT 'คัดลอกมาจากที่อยู่หลักม 1=copy'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ข้อมูลที่อยู่หลัก';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `EMP_ADDRESS`
--
ALTER TABLE `EMP_ADDRESS`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ADDR_PROVINCE` (`ADDR_PROVINCE`,`ADDR_TYPE`,`ADDR_MAIN_ACTIVE`),
  ADD KEY `ADDR_CUS_NO` (`ADDR_DATA_NO`),
  ADD KEY `ADDR_VILLAGE` (`ADDR_VILLAGE`),
  ADD KEY `ADDR_NUMBER` (`ADDR_NUMBER`),
  ADD KEY `ADDR_SUB_DISTRICT` (`ADDR_SUB_DISTRICT`),
  ADD KEY `ADDR_DISTRICT` (`ADDR_DISTRICT`),
  ADD KEY `ADDR_PROVINCE_2` (`ADDR_PROVINCE`),
  ADD KEY `ADDR_POSTCODE` (`ADDR_POSTCODE`),
  ADD KEY `ADDR_GEOGRAPHY` (`ADDR_GEOGRAPHY`),
  ADD KEY `ADDR_TYPE` (`ADDR_TYPE`),
  ADD KEY `ADDR_MAIN_ACTIVE` (`ADDR_MAIN_ACTIVE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `EMP_ADDRESS`
--
ALTER TABLE `EMP_ADDRESS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto run';