<?php

namespace app\modules\hr\controllers;

use app\modules\hr\apihr\ApiTax;
use app\modules\hr\apihr\ApiSalary;
use app\modules\hr\models\TaxConfigDeducttion;
use app\modules\hr\models\TaxIncomeMapping;
use app\modules\hr\models\TaxIncomeSection;
use app\modules\hr\models\TaxIncomeStructure;
use app\modules\hr\models\TaxIncomeType;
use app\modules\hr\models\TaxReduceOther;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Adddeducttemplate;
use yii\db\Expression;
use app\modules\hr\models\Command;
use app\modules\hr\models\RelationPosition;
use yii\helpers\ArrayHelper;
use Yii;
use app\modules\hr\controllers\MasterController;
class TaxController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;


    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }



    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $TaxIcomeStructureSearch = new TaxIncomeStructure();
        $TaxIcomeStructureProvider = $TaxIcomeStructureSearch->search(Yii::$app->request->queryParams);

        $TaxIncomeMappSearch = new TaxConfigDeducttion();
        $TaxIncomeMappProvider = $TaxIncomeMappSearch->search(Yii::$app->request->queryParams);

        $TaxIncomeMappingSearch = new TaxIncomeMapping();
        $TaxIncomeMappingProvider = $TaxIncomeMappingSearch->search(Yii::$app->request->queryParams);

        $TaxIncomeTypeSearch = new TaxIncomeType();
        $TaxIncomeTypeProvider = $TaxIncomeTypeSearch->search(Yii::$app->request->queryParams);

        $TaxIncomeSectionSearch = new TaxIncomeSection();
        $TaxIncomeSectionProvider = $TaxIncomeSectionSearch->search(Yii::$app->request->queryParams);

        $TaxReduceOtherSearch = new TaxReduceOther();
        $TaxReduceOtherProvider = $TaxReduceOtherSearch->search(Yii::$app->request->queryParams);
        /*$TaxIncomeType      = new TaxIncomeType() ;
        $TaxIncomeMapping   = new TaxIncomeMapping();
        $TaxIncomeMapp      = new TaxConfigDeducttion();
        $TaxIncomeSection   = new TaxIncomeSection();
        $TaxReduceOther     = new TaxReduceOther();*/

        $arrAddDeduct = ApiTax::getAddDeduct();
        $arrTaxIncomeSection =  ApiTax::getTaxIncomeSection();

        return $this->render('index', [
            'TaxIcomeStructureSearch' => $TaxIcomeStructureSearch,
            'TaxIcomeStructureProvider' => $TaxIcomeStructureProvider,

            'TaxIncomeMappSearch' => $TaxIncomeMappSearch,
            'TaxIncomeMappProvider' => $TaxIncomeMappProvider,

            'TaxIncomeMappingSearch' => $TaxIncomeMappingSearch,
            'TaxIncomeMappingProvider' => $TaxIncomeMappingProvider,

            'TaxIncomeTypeSearch' => $TaxIncomeTypeSearch,
            'TaxIncomeTypeProvider' => $TaxIncomeTypeProvider,

            'TaxIncomeSectionSearch' => $TaxIncomeSectionSearch,
            'TaxIncomeSectionProvider' => $TaxIncomeSectionProvider,

            'TaxReduceOtherSearch' => $TaxReduceOtherSearch,
            'TaxReduceOtherProvider' => $TaxReduceOtherProvider,

            'arrAddDeduct'=>$arrAddDeduct,
            'arrTaxIncomeSection'=>$arrTaxIncomeSection,

            /*'TaxIncomeType'=>$TaxIncomeType,
            'TaxIncomeMapping' =>$TaxIncomeMapping,
            'TaxIncomeSection'=>$TaxIncomeSection,
            'TaxReductOther'=>$TaxReduceOther,*/
        ]);
    }

    public function actionListtemplate()
    {
        $model = Adddeducttemplate::find()->where(['=', 'ADD_DEDUCT_TEMPLATE_TYPE', '1'])->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionListtemplatededuct()
    {
        $model = Adddeducttemplate::find()->where(['=', 'ADD_DEDUCT_TEMPLATE_TYPE', '2'])->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionListsection()
    {
        $model = TaxIncomeSection::find()->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionListtype()
    {
        $model = TaxIncomeType::find()->where(['!=', 'status_active', '99'])->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionSaveconfigtemp()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            //$add_deduct_template_id = $postValue['data',true];
            $add_deduct_template_id = $postValue['id_temp'];
            $detail_add_deduct_template = $postValue['detail_temp'];
           // $tax_rate = $postValue['tax_rate'];

            $model = new TaxConfigDeducttion();
            if ($postValue['id'] == '') {
                $model->add_deduct_template_id = $add_deduct_template_id;
                $model->detail_add_deduct_template = $detail_add_deduct_template;
                //$model->tax_rate = $tax_rate;
                $model->createby_user = $id_card;
                $model->create_datetime = new Expression('NOW()');
                $model->updateby_user = '';
                $model->update_datetime = '';
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = TaxConfigDeducttion::findOne($postValue['id']);
                $model->add_deduct_template_id = $add_deduct_template_id;
                $model->detail_add_deduct_template = $detail_add_deduct_template;
                $model->updateby_user = $id_card;
                $model->update_datetime = new Expression('NOW()');
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }
    }

    public function actionSaveconfigmapping()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;
            $postValue = Yii::$app->request->post();
            $arrTaxIncome = $postValue['taxincomesection_id'];
            $arrTaxRate = $postValue['taxrate'];


            $arrTaxSection = ApiTax::getTaxSection();


            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $transaction = $connection->beginTransaction();
            $eff = 0;
            try {

                foreach ($arrTaxIncome as $key => $value) {
                    $_rate = $arrTaxRate[$key];
                    $_id = $key;
                    if($value >=1)
                    {
                        $_tax_section_id = $value;
                        $_taxincome_type_id = $arrTaxSection[$_tax_section_id]['tax_income_type_id'];
                    }
                    else {
                        $_tax_section_id = null;
                        $_taxincome_type_id = null;
                    }

                    $sql = "UPDATE ADD_DEDUCT_TEMPLATE SET tax_section_id='$_tax_section_id' , taxincome_type_id='$_taxincome_type_id' , tax_rate='$_rate', ADD_DEDUCT_TEMPLATE_UPDATE_DATE=NOW() WHERE ADD_DEDUCT_TEMPLATE_ID='$_id' ";
                    $eff += $connection->createCommand($sql)->execute();

                    //echo $sql;
                }

               $transaction->commit();

            } catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception('ERROR');
            }

            return $eff;

        }
    }

    public function actionSaveconfigmapping_old()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            $id = $postValue['id'];
            $tax_income_section_id = $postValue['tax_income_section_id'];
            $add_deduct_template_id = $postValue['add_deduct_template_id'];
            $activity_status = $postValue['activity_status'];
            $model = new TaxIncomeMapping();
            if ($postValue['id'] == '') {
                $model->tax_income_section_id = $tax_income_section_id;
                $model->add_deduct_template_id = $add_deduct_template_id;
                $model->status_active = $activity_status;
                $model->createby_user = $id_card;
                $model->create_datetime = new Expression('NOW()');
                $model->updateby_user = '';
                $model->update_datetime = '';
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = TaxIncomeMapping::findOne($postValue['id']);
                $model->tax_income_section_id = $tax_income_section_id;
                $model->add_deduct_template_id = $add_deduct_template_id;
                $model->status_active = $activity_status;
                $model->createby_user = $id_card;
                $model->create_datetime = new Expression('NOW()');
                $model->updateby_user = '';
                $model->update_datetime = '';
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
            return $statusSave;
        }
    }

    public function actionDeletededuction()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post('id');
            $model = TaxConfigDeducttion::findOne($postValue);
            $statusModel = $model->delete();
            return $statusModel;
        }
    }

    public function actionGetupdateconfigmapping()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = "1509901325106";
            $getValue = Yii::$app->request->get('id');
            // $model = TaxIncomeMapping::findOne($getValue)->asArray();
            $model = TaxIncomeMapping::find()->where(['id' => $getValue])->asArray()->one();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionGetupdateconfigtemp()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = "1509901325106";
            $getValue = Yii::$app->request->get('id');
            $model = TaxConfigDeducttion::find()->where(['id' => $getValue])->asArray()->one();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeleteconfigmapping()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->get('id');
            $model = TaxIncomeMapping::findOne($postValue);
            $model->status_active = 99;
            $statusModel = $model->update();
        }
        return $statusModel;
    }

    public function actionSaveconfigtype()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            $taxincome_type = $postValue['taxincome_type'];
            $activity_status = $postValue['activity_status'];
            $model = new TaxIncomeType();
            if ($postValue['id'] == '') {
                $model->taxincome_type = $taxincome_type;
                $model->status_active = $activity_status;
                $model->createby_user = $id_card;
                $model->create_datetime = new Expression('NOW()');
                $model->updateby_user = '';
                $model->update_datetime = '';
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = TaxIncomeType::findOne($postValue['id']);
                $model->taxincome_type = $taxincome_type;
                $model->status_active = $activity_status;
                $model->updateby_user = $id_card;
                $model->update_datetime = new Expression('NOW()');
                if ($model->validate()) {
                    $statusSave = $model->update();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }

            return $statusSave;
        }
    }

    public function actionGetupdateconfigtype()
    {
        if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get('id'); //->where(['id'=>$getValue])->
            $model = TaxIncomeType::findOne($getValue);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeleteconfigtype()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->get('id');
            $model = TaxIncomeType::findOne($postValue);
            $model->status_active = Yii::$app->params['DELETE_STATUS'];;
            $statusModel = $model->update();
        }
        return $statusModel;
    }

    public function actionSaveconfigsection()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            //$add_deduct_template_id = $postValue['data',true];
            $tax_section_name = $postValue['tax_section_name'];
            $tax_income_type_id = $postValue['tax_income_type_id'];
            $expenses_percent = $postValue['expenses_percent'];
            $expenses_notover = $postValue['expenses_notover'];
            $comment = $postValue['comment'];
            $activity_status = $postValue['activity_status'];
            $model = new TaxIncomeSection();
            if ($postValue['id'] == '') {

                $model->tax_section_name = $tax_section_name;
                $model->tax_income_type_id = $tax_income_type_id;
                $model->expenses_percent = $expenses_percent;
                $model->expenses_notover = $expenses_notover;
                $model->comment = $comment;
                $model->status_active = $activity_status;
                $model->createby_user = $id_card;
                $model->create_datetime = new Expression('NOW()');
                $model->updateby_user = '';
                $model->update_datetime = '';
                $statusSave = $model->save();
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = TaxIncomeSection::findOne($postValue['id']);
                $model->tax_section_name = $tax_section_name;
                $model->tax_income_type_id = $tax_income_type_id;
                $model->expenses_percent = $expenses_percent;
                $model->expenses_notover = $expenses_notover;
                $model->status_active = $activity_status;
                $model->updateby_user = $id_card;
                $model->update_datetime = new Expression('NOW()');
                if ($model->validate()) {
                    $statusSave = $model->update();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
            return $statusSave;
        }
    }

    public function actionGetupdateconfigsection()
    {
        if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get('id');
            $model = TaxIncomeSection::find()->where(['id' => $getValue])->asArray()->one();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeleteconfigsection()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->get('id');
            $model = TaxIncomeSection::findOne($postValue);
            $model->status_active = 99;
            $statusModel = $model->update();
        }
        return $statusModel;
    }


    public function actionSaveconfigreduceother()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            //$add_deduct_template_id = $postValue['data',true];
            $reduce_name = $postValue['reduce_name'];
            $reduce_amount = $postValue['reduce_amount'];
            $reduce_amount_max = $postValue['reduce_amount_max'];
            $for_year = $postValue['for_year'];
            $activity_status = $postValue['activity_status'];
            $model = new TaxReduceOther();
            if ($postValue['id'] == '') {

                $model->reduce_name = $reduce_name;
                $model->reduce_amount = $reduce_amount;
                $model->reduce_amount_max = $reduce_amount_max;
                $model->for_year = $for_year;
                $model->status_active = $activity_status;
                $model->createby_user = $id_card;
                $model->create_datetime = new Expression('NOW()');
                $model->updateby_user = '';
                $model->update_datetime = '';
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }
            } else {
                $model = TaxReduceOther::findOne($postValue['id']);
                $model->reduce_name = $reduce_name;
                $model->reduce_amount = $reduce_amount;
                $model->reduce_amount_max = $reduce_amount_max;
                $model->for_year = $for_year;
                $model->status_active = $activity_status;
                $model->updateby_user = $id_card;
                $model->update_datetime = new Expression('NOW()');
                if ($model->validate()) {
                    $statusSave = $model->update();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }

            }
            return $statusSave;
        }
    }

    public function actionGetupdateconfigreduceother()
    {
        if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get('id');
            $model = TaxReduceOther::find()->where(['id' => $getValue])->asArray()->one();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeleteconfigreduceother()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->get('id');
            $model = TaxReduceOther::findOne($postValue);
            $model->status_active = 99;
            $statusModel = $model->update();
        }
        return $statusModel;
    }

    public function actionSaveconfigstructure()
    {
        if (Yii::$app->request->isAjax) {
            $id_card = $this->idcardLogin;

            $postValue = Yii::$app->request->post();
            $income_floor = $postValue['income_floor'];
            $income_ceil = $postValue['income_ceil'];
            $tax_rate = $postValue['tax_rate'];
            $gross_step = $postValue['gross_step'];
            $max_accumulate = $postValue['max_accumulate'];
            $from_year = $postValue['from_year'];
            $to_year = $postValue['to_year'];
            $remark = $postValue['remark'];
            $activity_status = $postValue['status_active'];
            $model = new TaxIncomeStructure();
            if ($postValue['id'] == '') {

                $model->income_floor = $income_floor;
                $model->income_ceil = $income_ceil;
                $model->tax_rate = $tax_rate;
                $model->gross_step = $gross_step;
                $model->max_accumulate = $max_accumulate;
                $model->from_year = $from_year;
                $model->to_year = $to_year;
                $model->remark = $remark;
                $model->status_active = $activity_status;
                $model->createby_user = $id_card;
                $model->create_datetime = new Expression('NOW()');
                $model->updateby_user = '';
                $model->update_datetime = '';
                if ($model->validate()) {
                    $statusSave = $model->save();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }

            } else {
                $model = TaxIncomeStructure::findOne($postValue['id']);
                $model->income_floor = $income_floor;
                $model->income_ceil = $income_ceil;
                $model->tax_rate = $tax_rate;
                $model->gross_step = $gross_step;
                $model->max_accumulate = $max_accumulate;
                $model->from_year = $from_year;
                $model->to_year = $to_year;
                $model->remark = $remark;
                $model->status_active = $activity_status;
                $model->updateby_user = $id_card;
                $model->update_datetime = new Expression('NOW()');
                if ($model->validate()) {
                    $statusSave = $model->update();
                } else {
                    $errors = $model->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                }

            }
            return $statusSave;
        }
    }

    public function actionGetupdateconfigstructure()
    {
        if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get('id');
            $model = TaxIncomeStructure::find()->where(['id' => $getValue])->asArray()->one();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeleteconfigstructure()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->get('id');
            $model = TaxIncomeStructure::findOne($postValue);
            $model->status_active = 99;
            $statusModel = $model->update();
        }
        return $statusModel;
    }

    public function actionTesttax()
    {

        $datemakesalary = '06-2017';

        /** $arrSalaryCalType
         *  ประเภทการคำนวณเงินเดือนและภาษี
         *  1=> รายสัปดาห์ (7) วัน  ตัวคูณภาษี 52
         *  2=> ราย week (15) วัน ตัวคูณภาษี 24
         *  3=> รายเดือน (30) วัน ตัวคูณภาษี 12
         **/

        $_wage_type = 3;//
        ApiTax::TaxInCome($datemakesalary,$_wage_type);


        //$id_cradmakesalary = $this->idcardLogin;
        //$datemakesalary = '2017-06';
       // $datemakesalary = '06-2017';
        //$k = explode('-',$datemakesalary);
        //$empdate = (strlen($k[0])==2) ? $k[1].'-'.$k[0] : $datemakesalary;

        //collect employee wage this month
        //$arrEmpWageThisMonth = ApiTax::getEmpWageThisMonth($datemakesalary);

        //collect employee wage add deduct this month
        //$arrEmpWageThisMonthDetail = ApiTax::getEmpWageArray($datemakesalary);

        //collect employee wage this month
        //$arrEmpTaxDeduction = ApiTax::getEmpTaxDeduction($arrEmpWageThisMonth);



        //echo "<pre>";
        //print_r($arrEmpWageThisMonth);
        //echo "</pre>";
        //exit;

        //$modelListEmpSalaryNormal = ApiSalary::listDataSalaryEmpNormal($empdate); //พนักงานปกติ
        //$arrEmpSalary = ApiTax::makeEmpSalaryArray($modelListEmpSalaryNormal);
       // $arrEmpWage = ApiTax::getEmpWageArray($datemakesalary);

/*        $arrPersonNetIncome[56] = [
            '3560500575223'=>800000,
            '3560500575244'=>500000,
            '3560500575240'=>510000,
            '3560500575243'=>149999,
            '3560500575288'=>130000,
            '3560500575250'=>7000000,
            '3560500575110'=>75000,
            '3560500575880'=>290000,
        ];

        $arrPersonNetIncome[43] = [
            '3420500575223'=>800000,
            '3577500575244'=>500000,
            '3578500575240'=>510000,
            '3560500575243'=>450000,
            '3560542575288'=>130000,
            '3560430575250'=>12000000,
            '3560599575110'=>75000,
            '3560780575880'=>170000,
        ];*/


        //$arrIncomeStructureRule = ApiTax::getArrayIncomeStructure();
        //$arrPersonIncomeTax = ApiTax::calculateIncomeTax($arrPersonNetIncome,$arrIncomeStructureRule);
       // $b = ApiTax::getArrayTemplateTax();
        //echo "<pre>";
        //print_r($arrPersonNetIncome);
        //echo "</pre>";

        //$income = 150000;
        //$step1 = 150000;
        //echo ($income/$step1);
        //echo 0.9 >= 1;



  /*      echo "<pre>";
        print_r($arrPersonAccumulateTax);
        echo "</pre>";
        echo '<hr>';
        echo "<pre>";
        // print_r($arrPersonGrossStepTax);
        echo "</pre>";
        echo '<hr>';*/

    }



}