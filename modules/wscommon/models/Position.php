<?php

namespace app\modules\wscommon\models;

use Yii;

class Position extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position';
    }


    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PositionCode', 'WorkCompany', 'Department', 'Section', 'WorkType', 'Level', 'runNumber', 'Name', 'Note', 'addToCommand', 'Status', 'position_Edit_date'], 'required'],
            [['Note'], 'string'],
            [['Status'], 'integer'],
            [['position_Edit_date'], 'safe'],
            [['PositionCode'], 'string', 'max' => 20],
            [['WorkCompany', 'Department', 'Section', 'WorkType'], 'string', 'max' => 50],
            [['Level'], 'string', 'max' => 2],
            [['runNumber'], 'string', 'max' => 4],
            [['Name'], 'string', 'max' => 250],
            [['addToCommand'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'PositionCode' => 'รหัสตำแหน่ง แบบเต็ม',
            'WorkCompany' => 'บริษัท',
            'Department' => 'แผนก',
            'Section' => 'ฝ่าย',
            'WorkType' => 'ลักษณะงาน',
            'Level' => 'ระดับ',
            'runNumber' => 'run number  ของตำแหน่ง',
            'Name' => 'Name',
            'Note' => 'Note',
            'addToCommand' => 'เพิ่มใน table command หรือยัง , 0:ยัง 1:เพิ่มแล้ว',
            'Status' => '99 ไม่ใช้ 1 ใช้',
            'position_Edit_date' => 'วันที่เริ่มใช้หรือวันที่แก้ไขข้อมูลล่าสุด',
        ];
    }

    public function searchrlistposition($id_postion_search) // param
    {
       

    // print_r($id_postion_search);



       $data=[];

       for($i=0;$i<=(count($id_postion_search)-1);$i++)
       {
        $data[]=$id_postion_search[$i]["position_id"];


       }
      // print_r($data);

        $ids = implode(",",$data);   

        $sql = "select position.PositionCode ,(position.Name) AS position , (department.name) AS department from 
        position INNER JOIN department ON 
        position.Department=department.id 
        where position.id IN($ids) AND position.status = 1 ";
       //$sql = "select PositionCode , Name  from position where id IN($ids) AND status = 1 ";

      //echo $sql;
       
        $show_postion_id = Yii::$app->dbERP_easyhr_OU
       ->createCommand($sql)
       ->bindParam(':seach_id',$seach_id)
       ->queryAll();
        return  $show_postion_id;


    }
}

