-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2017 at 03:51 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `dept_deduct_detail`
--

CREATE TABLE `dept_deduct_detail` (
  `id` int(11) UNSIGNED NOT NULL,
  `dept_deduct_id` int(11) DEFAULT NULL COMMENT 'รหัส dept_deduct FK. dept_deduct.id',
  `id_card` char(13) DEFAULT NULL COMMENT 'เลขบัตรประชาชนพนักงานที่จะหักเงิน',
  `is_record_approved` tinyint(3) DEFAULT NULL COMMENT 'สถานะการ approved 0=not approved, 1= approved',
  `record_approved_datetime` datetime DEFAULT NULL COMMENT ' วันเวลาที่ approved',
  `record_approved_by` char(13) DEFAULT NULL COMMENT 'approved โดย',
  `add_deduct_template_id` int(11) DEFAULT NULL COMMENT 'รหัสของ deduct_template id FK add_deduct_template.add_deduct_template_id',
  `add_deduct_template_name` varchar(250) DEFAULT NULL COMMENT 'รายละเอียดประเภทการหักเงิน',
  `dept_deduct_amount` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนเงินที่ต้องหัก',
  `doc_notice_id` int(11) DEFAULT NULL COMMENT 'รหัส pk ของรายการเอกสาร',
  `doc_notice_code` varchar(15) DEFAULT NULL COMMENT 'รหัสเอกสาร หากมีข้อมูลมาเชื่อม',
  `record_comment` varchar(200) DEFAULT NULL COMMENT 'หมายเหตุของแต่ละรายการ',
  `create_by` char(13) DEFAULT NULL COMMENT 'สร้างรายการโดย',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันเวลาที่สร้างรายการ',
  `update_by` char(13) DEFAULT NULL COMMENT 'อัพเดทโดย',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่อัพเดท'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dept_deduct_detail`
--
ALTER TABLE `dept_deduct_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dept_deduct_id` (`dept_deduct_id`),
  ADD KEY `id_card` (`id_card`),
  ADD KEY `doc_notice_id` (`doc_notice_id`),
  ADD KEY `doc_notice_code` (`doc_notice_code`),
  ADD KEY `create_by` (`create_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dept_deduct_detail`
--
ALTER TABLE `dept_deduct_detail`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
