<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 5/19/2017 AD
 * Time: 14:00
 */

return [
    'adminEmail' => 'admin@example.com',
    'example' => 'Test',
    'PAY_THISMONTH'=>'2',
    'PAY_EVERYMONTH'=>'1',
    'TYPE_ADD'=>'1',
    'TYPE_DEDUCT'=>'2',
    'PAY_THISMONTH_LABEL'=>'เฉพาะเดือน',
    'PAY_EVERYMONTH_LABEL'=>'ประจำทุกเดือน',
    'PAY_ACTIVE_LABEL'=>'ใช้งาน',
    'PAY_NOTACTIVE_LABEL'=>'ไม่ใช้งาน',
    'PAY_SSO_PERC'=>'1',
    'PAY_SSO_BAHT'=>'2',
    'PAY_SSO_PERC_LABEL'=>'คิดเป็น %',
    'PAY_SSO_BAHT_LABEL'=>'คิดเป็น บาท',
    'MIN_PND_CAL'=>1000,  //จำนวนเงินขั้นต่ำในการคิด อัตราภาษี
    'OT_EVENTING_MARKER'=>'18:00:01', //เวลาทำโอทีหลัง 6 โมงเย็น
];