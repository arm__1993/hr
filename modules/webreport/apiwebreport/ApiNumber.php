<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 18:06
 */

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiNumber
{

    public static function getCarRepairman($type_company,$technician_TypeCar,$str_start,$str_end)
    {
        //$sql_repairman = หารถที่แจ้งซ่อม
         $sql_repairman = "SELECT count(a.id) as sum_car,
         DATE_FORMAT(FROM_UNIXTIME(a.start_time),'%Y-%m-%d') as date_start,  
         DATE_FORMAT(FROM_UNIXTIME(a.end_time),'%Y-%m-%d') as date_end
        from drepair_inform as a 
        inner join cvhc_main as b on b.id = a.cvhc_main_id 
        inner join ahistory as c on b.ahistory_id = c.id
        inner join amodel as d on c.amodel_id = d.id
        where b.status != 99 
        and b.status != 97 
        and b.status != 94 
        and a.status <> 99 
        and b.brach_id = '$type_company' 
        and DATE_FORMAT(FROM_UNIXTIME(a.end_time),'%Y-%m-%d') between '$str_start' and '$str_end' 
        and d.agroup_id in ($technician_TypeCar)
        group by DATE_FORMAT(FROM_UNIXTIME(a.end_time),'%Y-%m-%d')";

      // exit;  
       return $total_CarRepairman = Yii::$app->dbERP_service->createCommand($sql_repairman)->queryAll();
    }
    public static function getCarRepairmanCvhcmain($type_company,$technician_TypeCar,$str_start,$str_end)
    {
        //$sql_repairman = หารถที่แจ้งซ่อม
         $sql_cvhc  = "SELECT b.id as cvhc_main,
         DATE_FORMAT(FROM_UNIXTIME(a.start_time),'%Y-%m-%d') as date_start,  
         DATE_FORMAT(FROM_UNIXTIME(a.end_time),'%Y-%m-%d') as date_end
        from drepair_inform as a 
        inner join cvhc_main as b on b.id = a.cvhc_main_id 
        inner join ahistory as c on b.ahistory_id = c.id
        inner join amodel as d on c.amodel_id = d.id
        where b.status != 99 
        and b.status != 97 
        and b.status != 94 
        and a.status <> 99 
        and b.brach_id = '$type_company' 
        and DATE_FORMAT(FROM_UNIXTIME(a.end_time),'%Y-%m-%d') between '$str_start' and '$str_end' 
        and d.agroup_id in ($technician_TypeCar)";
        
       return $cvhcmain = Yii::$app->dbERP_service->createCommand($sql_cvhc)->queryAll();
    }

    public static function getTechnician($arr_cvhcmain_technician)
    {
        $cvhcmain_id = join(",",$arr_cvhcmain_technician);
        $sql_countTechnician = "SELECT 
        count(erepair_send_all_mechanic.technician_id) AS countTechnician,
        DATE_FORMAT(FROM_UNIXTIME(drepair_inform.start_time),'%Y-%m-%d') as date_start,  
        DATE_FORMAT(FROM_UNIXTIME(drepair_inform.end_time),'%Y-%m-%d') as date_end
        FROM erepair_send_all_mechanic 
        inner join cvhc_main on cvhc_main.id = erepair_send_all_mechanic.cvhc_main_id 
        inner join drepair_inform on drepair_inform.cvhc_main_id = cvhc_main.id 
        WHERE erepair_send_all_mechanic.cvhc_main_id IN($cvhcmain_id)
        AND erepair_send_all_mechanic.status = '3'
        AND DATE_FORMAT(FROM_UNIXTIME(drepair_inform.end_time),'%Y-%m-%d') <> '1970-01-01'
        group by DATE_FORMAT(FROM_UNIXTIME(drepair_inform.end_time),'%Y-%m-%d')";

        $countTechnician = Yii::$app->dbERP_service->createCommand($sql_countTechnician)->queryAll();

        // $Technician=[];
        // foreach ($countTechnician as $value) {
        //     $Technician[$value['CHECKTIME_DATE']][] = count($value['id_card']);
        // }

        return $countTechnician;

    }


    public static function getCarRepairManInMonth($type_company,$str_start,$str_end)
    {
        $sql_CarRepairOfMonth = "SELECT erepair_send_all_mechanic.technician_name as tc_name, 
        count(distinct(cvhc_main.ahistory_id)) as amount_car ,erepair_send_all_mechanic.stop_time_job 
        FROM erepair_send_all_mechanic 
        inner join cvhc_main on cvhc_main.id = erepair_send_all_mechanic.cvhc_main_id 
        where erepair_send_all_mechanic.stop_time_job 
        between '$str_start' and '$str_end' 
        and erepair_send_all_mechanic.branch_id = $type_company 
        group by erepair_send_all_mechanic.technician_id";
        $carRepairman = Yii::$app->dbERP_service->createCommand($sql_CarRepairOfMonth)->queryAll();
        return $carRepairman;
    }


}