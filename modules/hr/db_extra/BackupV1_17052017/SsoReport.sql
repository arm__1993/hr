-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 19, 2017 at 02:48 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `sso_paid_detail`
--

CREATE TABLE `sso_paid_detail` (
  `ID_Detail` int(11) NOT NULL,
  `ID_Header` int(11) NOT NULL,
  `RECORD_TYPE` char(1) CHARACTER SET utf8 NOT NULL,
  `SSO_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `PREFIX` char(3) CHARACTER SET utf8 NOT NULL,
  `FNAME` char(30) CHARACTER SET utf8 NOT NULL,
  `LNAME` char(35) CHARACTER SET utf8 NOT NULL,
  `WAGES` char(14) CHARACTER SET utf8 NOT NULL,
  `PAID_AMOUNT` char(12) CHARACTER SET utf8 NOT NULL,
  `BLANK` char(27) CHARACTER SET utf8 NOT NULL,
  `COMPANY_ID` char(3) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `sso_paid_head`
--

CREATE TABLE `sso_paid_head` (
  `id` int(11) NOT NULL,
  `sso_paid_head` tinyint(3) NOT NULL,
  `for_month` tinyint(3) NOT NULL,
  `for_year` int(11) NOT NULL,
  `RECORD_TYPE` char(1) CHARACTER SET utf8 NOT NULL,
  `ACC_NO` char(10) CHARACTER SET utf8 NOT NULL,
  `BRANCH_NO` char(6) CHARACTER SET utf8 NOT NULL,
  `PAID_DATE` char(6) CHARACTER SET utf8 DEFAULT NULL,
  `PAID_PERIOD` char(4) CHARACTER SET utf8 NOT NULL,
  `COMPANY_NAME` char(45) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `RATE` char(4) CHARACTER SET utf8 NOT NULL,
  `TOTAL_EMPLOYEE` char(6) CHARACTER SET utf8 NOT NULL,
  `TOTAL_WAGES` char(15) CHARACTER SET utf8 NOT NULL,
  `TOTAL_PAID` char(14) CHARACTER SET utf8 NOT NULL,
  `TOTAL_PAID_BY_EMPLOYEE` char(12) CHARACTER SET utf8 NOT NULL,
  `TOTAL_PAID_BY_EMPLOYER` char(12) CHARACTER SET utf8 NOT NULL,
  `YEARS` int(11) NOT NULL,
  `MONTHS` int(11) NOT NULL,
  `DEPARTMENT_ID` int(11) NOT NULL,
  `COMPANY_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sso_paid_detail`
--
ALTER TABLE `sso_paid_detail`
  ADD PRIMARY KEY (`ID_Detail`);

--
-- Indexes for table `sso_paid_head`
--
ALTER TABLE `sso_paid_head`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sso_paid_detail`
--
ALTER TABLE `sso_paid_detail`
  MODIFY `ID_Detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sso_paid_head`
--
ALTER TABLE `sso_paid_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
