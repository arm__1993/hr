<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "ytax_list".
 *
 * @property integer $id
 * @property string $ytax_id
 * @property string $cvhc_main_id
 * @property string $ref_id
 * @property string $drepair_inform_id
 * @property string $drepair_inform_list_id
 * @property string $drepair_inform_listpart_id
 * @property string $abrand_id
 * @property string $agroup_id
 * @property string $amodel_id
 * @property string $ref_status
 * @property string $crepair_id
 * @property string $crepair_name
 * @property string $crepair_list_id
 * @property string $crepair_list_name
 * @property string $pw_id
 * @property string $pw_code
 * @property string $kind
 * @property string $pw_name
 * @property string $price_u
 * @property integer $unit
 * @property integer $unitw
 * @property string $vat_price
 * @property string $invat_price
 * @property string $novat_price
 * @property string $novat_cap
 * @property string $vat_cap
 * @property string $invat_cap
 * @property string $cam_id
 * @property string $id_cam
 * @property string $cam_name
 * @property string $discount
 * @property integer $sale_code_id
 * @property string $salecode_name
 * @property string $salecode_dis
 * @property string $dis_pay
 * @property string $dis_u
 * @property string $receive
 * @property string $emp_id
 * @property string $time
 * @property string $uses
 * @property string $status
 * @property string $warranty
 * @property string $type_list
 * @property integer $status_claim
 * @property integer $test
 * @property integer $test2
 * @property string $tis_price_p_part
 * @property string $tis_unit_new
 * @property string $tis_sum_price
 * @property string $tis_dis
 * @property string $tis_pw_code
 * @property string $tis_pw_name
 * @property integer $status_PDS
 * @property string $DEL_FROM
 * @property integer $sent_view_ytax_list
 */
class YtaxList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ytax_list';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ytax_id', 'cvhc_main_id', 'ref_id', 'drepair_inform_id', 'drepair_inform_list_id', 'drepair_inform_listpart_id', 'abrand_id', 'agroup_id', 'amodel_id', 'ref_status', 'crepair_id', 'crepair_name', 'crepair_list_id', 'crepair_list_name', 'pw_id', 'pw_code', 'kind', 'pw_name', 'price_u', 'unit', 'unitw', 'vat_price', 'invat_price', 'novat_price', 'novat_cap', 'vat_cap', 'invat_cap', 'cam_id', 'id_cam', 'cam_name', 'discount', 'sale_code_id', 'salecode_name', 'salecode_dis', 'dis_pay', 'dis_u', 'receive', 'emp_id', 'time', 'uses', 'status', 'warranty', 'type_list', 'status_claim', 'test', 'test2', 'tis_price_p_part', 'tis_unit_new', 'tis_sum_price', 'tis_dis', 'tis_pw_code', 'tis_pw_name', 'status_PDS', 'DEL_FROM', 'sent_view_ytax_list'], 'required'],
            [['crepair_name', 'crepair_list_name', 'pw_name', 'cam_name'], 'string'],
            [['price_u', 'vat_price', 'invat_price', 'novat_price', 'novat_cap', 'vat_cap', 'invat_cap', 'discount', 'salecode_dis', 'dis_pay', 'dis_u', 'receive', 'tis_price_p_part', 'tis_unit_new', 'tis_sum_price', 'tis_dis'], 'number'],
            [['unit', 'unitw', 'sale_code_id', 'status_claim', 'test', 'test2', 'status_PDS', 'sent_view_ytax_list'], 'integer'],
            [['ytax_id', 'cvhc_main_id', 'ref_id', 'drepair_inform_id', 'drepair_inform_list_id', 'drepair_inform_listpart_id', 'crepair_id', 'crepair_list_id', 'pw_id', 'pw_code', 'cam_id', 'id_cam', 'salecode_name', 'time', 'tis_pw_code'], 'string', 'max' => 20],
            [['abrand_id', 'agroup_id', 'amodel_id'], 'string', 'max' => 200],
            [['ref_status', 'uses', 'type_list'], 'string', 'max' => 1],
            [['kind', 'status', 'warranty', 'DEL_FROM'], 'string', 'max' => 2],
            [['emp_id'], 'string', 'max' => 13],
            [['tis_pw_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ytax_id' => 'Ytax ID',
            'cvhc_main_id' => 'Cvhc Main ID',
            'ref_id' => 'Ref ID',
            'drepair_inform_id' => 'Drepair Inform ID',
            'drepair_inform_list_id' => 'Drepair Inform List ID',
            'drepair_inform_listpart_id' => 'Drepair Inform Listpart ID',
            'abrand_id' => 'Abrand ID',
            'agroup_id' => 'Agroup ID',
            'amodel_id' => 'Amodel ID',
            'ref_status' => 'Ref Status',
            'crepair_id' => 'Crepair ID',
            'crepair_name' => 'Crepair Name',
            'crepair_list_id' => 'Crepair List ID',
            'crepair_list_name' => 'Crepair List Name',
            'pw_id' => 'Pw ID',
            'pw_code' => 'Pw Code',
            'kind' => 'Kind',
            'pw_name' => 'Pw Name',
            'price_u' => 'Price U',
            'unit' => 'Unit',
            'unitw' => 'Unitw',
            'vat_price' => 'Vat Price',
            'invat_price' => 'Invat Price',
            'novat_price' => 'Novat Price',
            'novat_cap' => 'Novat Cap',
            'vat_cap' => 'Vat Cap',
            'invat_cap' => 'Invat Cap',
            'cam_id' => 'Cam ID',
            'id_cam' => 'Id Cam',
            'cam_name' => 'Cam Name',
            'discount' => 'Discount',
            'sale_code_id' => 'Sale Code ID',
            'salecode_name' => 'Salecode Name',
            'salecode_dis' => 'Salecode Dis',
            'dis_pay' => 'Dis Pay',
            'dis_u' => 'Dis U',
            'receive' => 'Receive',
            'emp_id' => 'Emp ID',
            'time' => 'Time',
            'uses' => 'Uses',
            'status' => 'Status',
            'warranty' => 'Warranty',
            'type_list' => 'Type List',
            'status_claim' => 'Status Claim',
            'test' => 'Test',
            'test2' => 'Test2',
            'tis_price_p_part' => 'Tis Price P Part',
            'tis_unit_new' => 'Tis Unit New',
            'tis_sum_price' => 'Tis Sum Price',
            'tis_dis' => 'Tis Dis',
            'tis_pw_code' => 'Tis Pw Code',
            'tis_pw_name' => 'Tis Pw Name',
            'status_PDS' => 'Status  Pds',
            'DEL_FROM' => 'Del  From',
            'sent_view_ytax_list' => 'Sent View Ytax List',
        ];
    }
}
