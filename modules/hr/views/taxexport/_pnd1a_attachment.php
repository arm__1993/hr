<?php
use yii\helpers\Html;
use app\api\DateTime;
use app\api\Utility;
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$dataemp_idcard = [];
$sumpaidmount = 0;
$sumpaidtax =0;
$company_idcard = str_split($datahead[0]['tax_regis_code']);
$branchNumber = str_split((strlen($data[0][branch_no])<5)?str_pad($data[0][branch_no],5,"0",STR_PAD_LEFT):$data[0][branch_no]);

?>
  <table width='120%'>
    <tr>
      <td width='20%'>
        <table>
          <tr>
            <td style='font-size:15pt'>
              ใบแนบ
            </td>
            <td style='font-size:20pt'>
              ภ.ง.ด.1
            </td>
          </tr>
        </table>
      </td>
      <td width='80%'>
        <table>
          <tr>
            <td style='font-size:9pt;'><b>เลขประจำตัวผู้เสียภาษีอากร </b>(ของผู้มีหน้าที่หักภาษี ณ ที่จ่าย)</td>
            <td>
              <table border='1' style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                <tr>
                  <td>
                    <?php echo $company_idcard[0]; ?>
                  </td>
                  <td style="border:0px;">-</td>
                  <td>
                    <?php echo $company_idcard[1]; ?>
                  </td>
                  <td style="border-left:0px;">
                    <?php echo $company_idcard[2]; ?>
                  </td>
                  <td style="border-left:0px;">
                    <?php echo $company_idcard[3]; ?>
                  </td>
                  <td style="border-left:0px;">
                    <?php echo $company_idcard[4]; ?>
                  </td>
                  <td style="border:0px;">-</td>
                  <td>
                    <?php echo $company_idcard[5]; ?>
                  </td>
                  <td style="border-left:0px;">
                    <?php echo $company_idcard[6]; ?>
                  </td>
                  <td style="border-left:0px;">
                    <?php echo $company_idcard[7]; ?>
                  </td>
                  <td style="border-left:0px;">
                    <?php echo $company_idcard[8]; ?>
                  </td>
                  <td>
                    <?php echo $company_idcard[9]; ?>
                  </td>
                  <td style="border:0px;">-</td>
                  <td>
                    <?php echo $company_idcard[10]; ?>
                  </td>
                  <td style="border-left:0px;">
                    <?php echo $company_idcard[11]; ?>
                  </td>
                  <td style="border:0px;">-</td>
                  <td>
                    <?php echo $company_idcard[12]; ?>
                  </td>

                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!--<div style='width:70% background-color:#cdd1dd; border: 1px solid #ccc;border-top-left-radius: 8px;border-top-right-radius: 8px;border-bottom-left-radius: 8px;border-bottom-right-radius: 8px;'>
<table style='font-size:7pt;'>
<tr>
<td>
(ให้แยกกรอกรายการในใบแนบนี้ตามเงินได้แต่ละประเภท โดยใส่เครื่องหมาย “” ลงใน “ ” หน้าข้อความแล้วแต่กรณี เพียงข้อเดียว)
</td>
</tr>
<tr>
<td>
<table style='width:100%;' style='font-size:7pt;'>
<tr>
<td>
<table>
<tr>
<td valign="top"><b>ประเภทเงินได้</b></td>
<td valign="top">
<input type="checkbox">
</td>
<td valign="top">(1)</td>
<td valign="top"> เงินได้ตาม <b>มาตรา 40 (1)</b> เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป</td>
</tr>
<tr>
<td valign="top"></td>
<td valign="top">
<input type="checkbox">
</td>
<td valign="top">(2)</td>
<td valign="top"> เงินได้ตาม <b>มาตรา 40 (1)</b> เงินเดือน ค่าจ้าง ฯลฯ
<br> กรณีได้รับอนุมัติจากกรมสรรพากรให้หักอัตรา
<b>ร้อยละ 3</b>
</td>
</tr>

</table>
</td>
<td>
<table>
<tr>
<td valign="top">
<input type="checkbox">
</td>
<td valign="top">(3)</td>
<td valign="top">เงินได้ตาม <b>มาตรา 40 (1) (2)</b> กรณีนายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงาน</td>
</tr>
<tr>
<td valign="top">
<input type="checkbox">
</td>
<td valign="top">(4)</td>
<td valign="top"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย</b>
</td>
</tr>
<tr>
<td valign="top">
<input type="checkbox">
</td>
<td valign="top">(5)</td>
<td valign="top"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย</b>
</td>
</tr>

</table>
</td>
</tr>
</table>
</td>

</tr>
</table>
</div>-->
  <div style='width:100%' border='1'>
    <div style='float:left;width:75% background-color:#cdd1dd; border: 1px solid #000;border-top-left-radius: 8px;border-top-right-radius: 8px;border-bottom-left-radius: 8px;border-bottom-right-radius: 8px;'>
      <table style='font-size:7pt;'>
        <tr>
          <td>
            (ให้แยกกรอกรายการในใบแนบนี้ตามเงินได้แต่ละประเภท โดยใส่เครื่องหมาย “” ลงใน “ ” หน้าข้อความแล้วแต่กรณี เพียงข้อเดียว)
          </td>
        </tr>
        <tr>
          <td>
            <table style='width:100%;' style='font-size:7pt;'>
              <tr>
                <td>
                  <table>
                    <tr>
                      <td valign="top"><b>ประเภทเงินได้</b></td>
                      <td valign="top">
                        <input type="checkbox">
                      </td>
                      <td valign="top">(1)</td>
                      <td valign="top"> เงินได้ตาม <b>มาตรา 40 (1)</b> เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป</td>
                    </tr>
                    <tr>
                      <td valign="top"></td>
                      <td valign="top">
                        <input type="checkbox">
                      </td>
                      <td valign="top">(2)</td>
                      <td valign="top"> เงินได้ตาม <b>มาตรา 40 (1)</b> เงินเดือน ค่าจ้าง ฯลฯ
                        <br> กรณีได้รับอนุมัติจากกรมสรรพากรให้หักอัตรา
                        <b>ร้อยละ 3</b>
                      </td>
                    </tr>

                  </table>
                </td>
                <td>
                  <table>
                    <tr>
                      <td valign="top">
                        <input type="checkbox">
                      </td>
                      <td valign="top">(3)</td>
                      <td valign="top">เงินได้ตาม <b>มาตรา 40 (1) (2)</b> กรณีนายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงาน</td>
                    </tr>
                    <tr>
                      <td valign="top">
                        <input type="checkbox">
                      </td>
                      <td valign="top">(4)</td>
                      <td valign="top"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย</b>
                      </td>
                    </tr>
                    <tr>
                      <td valign="top">
                        <input type="checkbox">
                      </td>
                      <td valign="top">(5)</td>
                      <td valign="top"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย</b>
                      </td>
                    </tr>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
    <div style='width:23%;float:right; border: 0px solid #000;'>
      <div align='right'>
        <table width='100%'>
          <tr>
            <td style='font-size:7pt;text-align:right'><b>สาขาที่</b></td>
            <td valign='right' style='width:30%'>
              <table style='border: 1px solid #ccc;' cellpadding='4' rowpadding='2' cellspacing='0'>
                <tr>
                  <?php foreach($branchNumber as $item){
                        echo "<td style='font-size:7pt'>".$item."</td>";
                    }
                    ?>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
      <br>
      <table style='valign:bottom' height='100%'>
        <tr>
          <td style='font-size:8pt;padding-bottom:-15px;'>
            <center>
              <?php echo $count_page; ?>
            </center>
          </td>
          <td style='font-size:8pt;padding-bottom:-15px'>
            <center>
              <?php echo $all_page; ?>
            </center>
          </td>
        </tr>
        <tr>
          <td style='font-size:8pt'>
            แผ่นที่.....................
          </td>
          <td style='font-size:8pt'>
            ในจำนวน..................แผ่น
          </td>
        </tr>
      </table>
    </div>
  </div>
  <table style='border:1px solid #ccc;width:100%;font-size:9pt;border-collapse: collapse;margin-top:3px;' cellpadding='1' rowpadding='2' cellspacing='1'>
    <tr>
      <td rowspan='2' align='center'>
        <b>ลำดับที่</b>
      </td>
      <td style='width:25%' align='center' rowspan='2'>
        <b>เลขประจำ ตัวผู้เสียภาษีอากร</b><span style='font-size:7pt'>(ของผู้มีเงินได้)</span>
      </td>
      <td align='center' style='width:33%'>
        <b>ชื่อผู้มีเงินได้</b><span style='font-size:7pt'>(ให้ระบุชัดเจนว่าเป็น นาย นาง นางสาว หรือยศ)</span>
      </td>
      <td style='width:15%' align='center'>
        <b>จำนวนงานได้ที่จ่ายทั้งปี</b>
      </td>
      <td style='width:15%' align='center'>
        <b>จำนวนงานได้ที่หัก<br>และนำส่งทั้งปี</b>
      </td>
      <td rowspan='2' align='center'>
        <svg width='20' height="40">
          <text style='font-size:9pt;font-color:#15205b' transform='rotate(90,0,0)'>เงื่อนไข *</text>
        </svg>
      </td>
    </tr>
    <tr align='center'>

      <td align='center'>
        <b>ที่อยู่ของผู้มีเงินได้</b><span style='font-size:7pt'>(ให่ระบุ ตรอก/ซอย ถนน ตำบล/แขวง อำเถอ/เขต จังหวัด)</span>
      </td>
    </tr>
    <?php
$totle_paid_amount=0;
$totle_paid_tax=0;
for($i=1;$i<=8;$i++){
    $totle_paid_amount += $arrayDetail[$i]['paid_amount'];
    $totle_paid_tax += $arrayDetail[$i]['paid_tax'];
    $dataemp_idcard  = str_split($arrayDetail[$i]['emp_idcard']);
    ?>
      <tr>
        <td valign='top'>
          <center>
            <?php print($i); ?>
          </center>
        </td>
        <td valign='top'>
          <table border='1' style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc;" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='1' width='80%'>
            <tr>
              <td>
                <?php echo $dataemp_idcard[0];  ?>
              </td>
              <td border='0' style="border:0px;">-</td>
              <td>
                <?php echo $dataemp_idcard[1];  ?>
              </td>
              <td style="border-left:0px;">
                <?php echo $dataemp_idcard[2];  ?>
              </td>
              <td style="border-left:0px;">
                <?php echo $dataemp_idcard[3];  ?>
              </td>
              <td style="border-left:0px;">
                <?php echo $dataemp_idcard[4];  ?>
              </td>
              <td border='0' style="border:0px;">-</td>
              <td>
                <?php echo $dataemp_idcard[5];  ?>
              </td>
              <td style="border-left:0px;">
                <?php echo $dataemp_idcard[6];  ?>
              </td>
              <td style="border-left:0px;">
                <?php echo $dataemp_idcard[7];  ?>
              </td>
              <td style="border-left:0px;">
                <?php echo $dataemp_idcard[8];  ?>
              </td>
              <td>
                <?php echo $dataemp_idcard[9];  ?>
              </td>
              <td border='0' style="border:0px;">-</td>
              <td>
                <?php echo $dataemp_idcard[10];  ?>
              </td>
              <td style="border-left:0px;">
                <?php echo $dataemp_idcard[11];  ?>
              </td>
              <td border='0' style="border:0px;">-</td>
              <td>
                <?php echo $dataemp_idcard[12];  ?>
              </td>
            </tr>

          </table>

        </td>
        <td>
          <table style='border:1px'>
            <tr>
              <td width='50%' style='padding-bottom:-15px' align='center'>
                <?php echo $arrayDetail[$i]['emp_firstname']; ?>
              </td>
              <td width='50%' style='padding-bottom:-15px;margin-left:-15px' align='center'>
                <?php echo $arrayDetail[$i]['emp_lastname']; ?>
              </td>
            </tr>
            <tr>
              <td width='50%' sytle='valign:left'> ชื่อ...............................................</td>
              <td width='50%' sytle='valign:left;margin-left:-15px'>ชื่อสกุล.......................................</td>
            </tr>
            <tr>
              <td colspan='2' style='padding-bottom:-15px' align='center'>
                <?php echo $arrayDetail[$i]['emp_address'];  ?>
              </td>
            </tr>
            <tr>
              <td colspan='2' style=''><b>ที่่อยู่</b>...............................................................................................</td>
            </tr>
          </table>
        </td>
        <td>
          <br>
          <center style='padding-bottom:-15px'>
            <?php echo $arrayDetail[$i]['paid_amount']; ?>
          </center>
          ............................................
        </td>
        <td>
          <br>
          <center style='padding-bottom:-15px'>
            <?php echo $arrayDetail[$i]['paid_tax']; ?>
          </center>
          ........................................
        </td>
        <td>
          <br> .........
        </td>
      </tr>
      <?php }?>
        <tr>
          <td colspan='3' align='right' style='padding-bottom:-15px;border-bottom:0px'>
          </td>
          <td style='padding-bottom:-15px;border-bottom:0px' align='center'>
            <?php echo $totle_paid_amount?>
          </td>
          <td style='padding-bottom:-15px;border-bottom:0px' align='center'>
            <?php echo $totle_paid_tax?>
          </td>
        </tr>
        <tr>
          <td colspan='3' align='right' style='border-top:0px'>
            รวมยอดเงินได้และภาษีที่นำส่ง (นำ ไปรวมกับใบแนบ ภ.ง.ด.1 แผ่นอื่น (ถ้ามี))
          </td>
          <td style='border-top:0px'>...........................................</td>
          <td style='border-top:0px'>...........................................
            <td>
        </tr>
        <tr>
          <td colspan='6'>
            <table>
              <tr>
                <td colspan='3' align='left' style='padding-left:10px' width='60%'>
                  <table>
                    <tr>
                      <td colspan='2'>(ให้กรอกลำดับที่ต่อเนื่องกันไปทุกแผ่นตามเงินได้แต่ละประเภท)</td>
                    </tr>
                    <tr>
                      <td><b>หมายเหตุ * </b></td>
                      <td> เงื่อนไขการหักภาษีให้กรอกดังนี้ </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td> หัก ณ ที่จ่าย กรอก 1 </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td> ออกให้ตลอดไป กรอก 2 </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td> ออกให้ครั้งเดียว กรอก 3 </td>
                    </tr>
                  </table>
                </td>
                <td colspan='3' align='center'>
                  <table style='border: 1px solid #fff;'>
                    <tr>
                      <td rowspan='4'><img height="60" width="60" src="<?php echo $imghr; ?>/stamp-icon.png" class="img-circle"></td>
                      <td> ลงชื่อ..................................................................................ผู้จ่ายเงิน </td>
                    </tr>
                    <tr>
                      <td> (................................................................................) </td>
                    </tr>
                    <tr>
                      <td>ตำแหน่ง.............................................................................</td>
                    </tr>
                    <tr>
                      <td>ยื่นวันที่...........เดือน..................................พ.ศ. ................... </td>
                    </tr>
                  </table>
                </td>

              </tr>
            </table>
          </td>
        </tr>

  </table>
  <hr>
  <div style='width:25%;float:right;text-align:right;font-size:9pt;margin-top:-5px'>
    <p>พิมพ์
      <?php echo DateTime::mappingMonthContraction(date('m')).(date('Y')+543); ?>
    </p>
  </div>
  <div style='width:45%;float:left;text-align:lefts;font-size:9pt;margin-top:-5px'>
    <p>สอบถามข้อมูลเพิ่มเติมได้ที่ศูนย์สารนิเทศสรรพากร โทร.<img style='padding-bottom:-5px' height='20' src="<?php echo $imghr; ?>/210360_WHT1_kor.pdf.png" class="img-circle"> 1161</p>
  </div>