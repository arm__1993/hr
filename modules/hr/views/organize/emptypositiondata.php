<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:27
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;


$selectworking = ApiHr::getWorking_company();
$this->registerCssFile(Yii::$app->request->BaseUrl . '/css/hr/OU/cssDatatable/buttons.dataTables.min.css', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->BaseUrl . '/css/hr/OU/cssDatatable/jquery.dataTables.min.css', ['depends' =>[\yii\bootstrap\BootstrapPluginAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/dataTables.buttons.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/buttons.flash.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/jszip.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/pdfmake.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/vfs_fonts.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/buttons.html5.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsDatatable/buttons.print.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//s$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/pdf-make-font.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/emptypositiondata.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<meta charset="UTF-8">
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li class="active">รายงานตำแหน่งงานว่าง</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-1">
                    &nbsp;
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp"  class="col-sm-4 control-label">บริษัท</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="selectworking" id="selectworking" onchange="getCompanyForDepartment(this);";>
                                        <option value="">เลือกบริษัท</option>
                                            <?php 
                                                foreach ($selectworking as  $value) {
                                                        echo '<option value="' . $value['id']. '">' . $value['name'] . '</option>';
                                        } ?>
                                </select>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp"  class="col-sm-3 control-label">แผนก</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="selectdepartment" id="selectdepartment" onchange="getDepartmentForSection(this);">
                                    <option value=""> เลือกแผนก </option>
                                </select>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                <div class="form-group">
                    <label for="numberPassportEmp"  class="col-sm-3 control-label">ฝ่าย</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="selectsection" id="selectsection" >
                                <option value=""> เลือกฝ่าย </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row" style="text-align:center">
                <input type="button" onclick="Search();" class="btn btn-primary btn-md" value="ค้นหา">
                <input type="submit" class="btn btn-warning btn-md" value="ล้าง">
            </div>
        </div>
    </div>
    <div class="box-body">
        <table id='datatable' width='100%'>
            <thead>
                <tr>
                    <th>บริษัท</th>
                    <th>แผนก</th>
                    <th>ฝ่าย</th>
                    <th>ตำแหน่ง</th>
                    <th>ผังโครงสร้าง</th>
                    <th>บรรจุ</th>
                    <th>ทดลองงาน</th>
                    <th>สรรหา</th>
                    <th>ปิดสรรหา</th>
                    <th>ปิดตำแหน่ง</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($data as $item){
                        echo '<tr>';
                        echo '<td> '.$item['companyName'].' </td>';
                        echo '<td> '.$item['departmentName'].' </td>';
                        echo '<td> '.$item['sectionName'].' </td>';
                        echo '<td> '.$item['Position_Name'].' </td>';
                        echo '<td> '.$item['Position_typeAll'].' </td>';
                        echo '<td> '.$item['Position_typepersonnel'].' </td>';
                        echo '<td> '.$item['Position_typeapprentice'].' </td>';
                        echo '<td> '.$item['Position_typeFree'].' </td>';
                        echo '<td> '.$item['Position_typeclose'].' </td>';
                        echo '<td> '.$item['Position_typecloseposition'].' </td>';
                        echo '</tr>';
                    }
                ?>
            </tbody>
        </table>
        
    </div>
    <!-- /.box -->
</section><!-- /.content -->