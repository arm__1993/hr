<?php

namespace app\modules\wssale\controllers;
use app\modules\wssale\models\Sell;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;

class SellController extends ActiveController
{
   public $modelClass = 'app\modules\wssale\models\Sell';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	
	public function actionSearchsellformobile()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $datacussell = new Sell();
        $showdatasell = $datacussell->searchsellformobile($cusno);
        return $showdatasell;
    }

}
