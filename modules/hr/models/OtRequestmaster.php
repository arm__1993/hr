<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ot_requestmaster".
 *
 * @property integer $id
 * @property string $request_no
 * @property string $wage_pay_date
 * @property integer $company_id
 * @property integer $division_id
 * @property integer $section_id
 * @property integer $activity_id
 * @property string $activity_name
 * @property integer $ot_calculate_type
 * @property string $activity_date
 * @property integer $has_profile_route
 * @property integer $profile_route_id
 * @property string $distance_amount
 * @property integer $has_motel_profile
 * @property string $motel_price
 * @property string $request_byuser
 * @property string $request_date
 * @property string $request_time
 * @property integer $is_approved
 * @property string $approved_byuser
 * @property string $approved_date
 * @property string $approved_time
 * @property integer $status_active
 * @property string $reject_byuser
 * @property string $reject_date
 * @property string $reject_time
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 * @property integer $is_hr_approved
 * @property string $hr_approved_date
 * @property string $hr_approved_time
 * @property string $hr_approved_by
 */
class OtRequestmaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ot_requestmaster';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_no', 'wage_pay_date', 'company_id', 'division_id', 'section_id', 'activity_id', 'activity_name', 'ot_calculate_type', 'activity_date', 'request_byuser', 'request_date', 'request_time', 'is_approved', 'status_active', 'createby_user', 'create_datetime'], 'required'],
            [['company_id', 'division_id', 'section_id', 'activity_id', 'ot_calculate_type', 'has_profile_route', 'profile_route_id', 'has_motel_profile', 'is_approved', 'status_active', 'is_hr_approved'], 'integer'],
            [['activity_date', 'request_date', 'request_time', 'approved_date', 'approved_time', 'reject_date', 'reject_time', 'create_datetime', 'update_datetime', 'hr_approved_date', 'hr_approved_time'], 'safe'],
            [['distance_amount', 'motel_price'], 'number'],
            [['request_no'], 'string', 'max' => 20],
            [['wage_pay_date'], 'string', 'max' => 7],
            [['activity_name'], 'string', 'max' => 250],
            [['request_byuser', 'approved_byuser', 'reject_byuser', 'hr_approved_by'], 'string', 'max' => 13],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
            [['request_no'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_no' => 'Request No',
            'wage_pay_date' => 'Wage Pay Date',
            'company_id' => 'Company ID',
            'division_id' => 'Division ID',
            'section_id' => 'Section ID',
            'activity_id' => 'Activity ID',
            'activity_name' => 'Activity Name',
            'ot_calculate_type' => 'Ot Calculate Type',
            'activity_date' => 'Activity Date',
            'has_profile_route' => 'Has Profile Route',
            'profile_route_id' => 'Profile Route ID',
            'distance_amount' => 'Distance Amount',
            'has_motel_profile' => 'Has Motel Profile',
            'motel_price' => 'Motel Price',
            'request_byuser' => 'Request Byuser',
            'request_date' => 'Request Date',
            'request_time' => 'Request Time',
            'is_approved' => 'Is Approved',
            'approved_byuser' => 'Approved Byuser',
            'approved_date' => 'Approved Date',
            'approved_time' => 'Approved Time',
            'status_active' => 'Status Active',
            'reject_byuser' => 'Reject Byuser',
            'reject_date' => 'Reject Date',
            'reject_time' => 'Reject Time',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
            'is_hr_approved' => 'Is Hr Approved',
            'hr_approved_date' => 'Hr Approved Date',
            'hr_approved_time' => 'Hr Approved Time',
            'hr_approved_by' => 'Hr Approved By',
        ];
    }
}
