<?php


namespace app\modules\hr\apihr;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;


use yii;
use yii\web\JsExpression;
use yii\db\Expression;
use yii\base\InvalidConfigException;

use yii\web\Response;

use app\modules\hr\models\Wagethismonth;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\apihr\PHPExcel_Cell_MyValueBinder;

/**
 * Class ApiExportbank
 * @package app\modules\hr\apihr
 */
class ApiExportbank
{
    public $idcardLogin;

    public function init()
    {
        $session = Yii::$app->session;
        if (!$session->has('fullname') || !$session->has('idcard')) {
            return $this->redirect(array('/auth/default/logout'), 302);
        }

        $this->idcardLogin = $session->get('idcard');
    }


    public static function exportExceltobank($arrCompany, $dateExport)
    {

        /* ------------------------------------------------------------- */
        /* ตรวจสอบข้อมูล company and date export   */
        /* ------------------------------------------------------------- */

        if(count($arrCompany) < 1) {
            throw new \Exception('ERROR : ไม่พบรายชื่อบริษัทที่ส่งออก');
            exit;
        }

        if($dateExport == '' || $dateExport == null) {
            throw new \Exception('ERROR : ไม่พบวันที่ที่ทำรายการ');
            exit;
        }


        /* ------------------------------------------------------------- */
        /*  Load data company and wage this month   */
        /* ------------------------------------------------------------- */
        $modelWK = Workingcompany::find()
            ->select('short_name,name,id')
            ->where('status <> 99')
            ->andWhere([
                'IN', 'id', $arrCompany
            ])
            ->orderBy('id DESC')
            ->asArray()
            ->all();


        $modelSalary = Wagethismonth::find()
            ->where(' WAGE_NET_SALARY != 0.00')
            ->andWhere([
                'IN', 'WAGE_WORKING_COMPANY', $arrCompany
            ])
            ->orderBy('WAGE_WORKING_COMPANY,WAGE_DEPARTMENT_ID,WAGE_SECTION_ID ASC')
            ->asArray()
            ->all();


        $arrWageData = [];
        foreach ($modelSalary AS $data) {
            $arrWageData[$data['WAGE_WORKING_COMPANY']][] = $data;
        }


        /** สร้างไฟล์เงินเดือน  excel แยกตามบริษัท */
        /* ------------------------------------------------------------- */
        /*  วนลูปสร้างไฟล์ excel เงินเดือนตามชื่อบริษัทที่ส่งมา  */
        /* ------------------------------------------------------------- */

        foreach ($modelWK as $wk) {

            $objPHPExcel = new Spreadsheet();
            $objPHPExcel->getProperties()->setCreator("HBSO Team")
                ->setLastModifiedBy("HBSO Team")
                ->setTitle("รายชื่อส่งออกเงินเดือน")
                ->setSubject("รายชื่อส่งออกเงินเดือน")
                ->setDescription("รายชื่อส่งออกเงินเดือน")
                ->setKeywords("รายชื่อส่งออกเงินเดือน")
                ->setCategory("รายชื่อส่งออกเงินเดือน");
            $objPHPExcel->getDefaultStyle()->getFont()
                ->setName('AngsanaUPC')
                ->setSize(16);

            $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);


            $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
            $objWorkSheet->setCellValue('A1', 'URef');
            $objWorkSheet->setCellValue('B1', 'Salary_BankNo');
            $objWorkSheet->setCellValue('C1', 'Total_Receive');
            $objWorkSheet->setCellValue('D1', 'Date');

            $_srow = 2; // เริ่มใส่ข้อมูลบรรทัดที่ 2
            $i = 0;
            if (isset($arrWageData[$wk['id']])) {
                $arrDataItem = $arrWageData[$wk['id']];

                foreach ($arrDataItem AS $data) {
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $_srow, ($data['WAGE_EMP_ID']), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $_srow, ($data['WAGE_ACCOUNT_NUMBER']), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    //$objWorkSheet->setCellValue('B' . $_srow, "'".($modelSalary[$i]['WAGE_ACCOUNT_NUMBER']));
                    $objWorkSheet->setCellValue('C' . $_srow, ($data['WAGE_NET_SALARY']));
                    $objWorkSheet->setCellValue('D' . $_srow, ($dateExport));
                    $i++;
                    $_srow++;
                }
            }

            $objPHPExcel->getActiveSheet()->setTitle('"' . $wk['short_name'] . '"');
            $objPHPExcel->setActiveSheetIndex(0);

            //old for PHPExcel
            //$objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Excel5');
            //$objWriter->save(str_replace(__FILE__,'upload/exportsalary/HBSO.xlsx',__FILE__));
            //$objWriter->save('upload/exportsalary/' . $wk['short_name'] . '.xls');

            //New for Phpspreadsheet
            $writer = new Xls($objPHPExcel);
            $writer->save('upload/exportsalary/' . $wk['short_name'] . '.xls');

        }


        /** สร้างไฟล์เซ็นต์ชื่อของพนักงาน */
        /* ------------------------------------------------------------- */
        /*  วนลูปสร้างไฟล์ excel เซ็นต์ชื่อของพนักงานตามบริษัทที่ส่งมา  */
        /* ------------------------------------------------------------- */
        $d = str_replace(['/', '-'], '', $dateExport);
        $fileName = "upload/exportsalary/Employee-Sign-$d.xls";

        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getProperties()->setCreator("HBSO Team")
            ->setLastModifiedBy("HBSO Team")
            ->setTitle("รายชื่อส่งออกเงินเดือน")
            ->setSubject("รายชื่อส่งออกเงินเดือน")
            ->setDescription("รายชื่อส่งออกเงินเดือน")
            ->setKeywords("รายชื่อส่งออกเงินเดือน")
            ->setCategory("รายชื่อส่งออกเงินเดือน");
        $objPHPExcel->getDefaultStyle()->getFont()
            ->setName('AngsanaUPC')
            ->setSize(16);

        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);


        $start_row = 2;
        foreach ($modelWK as $wk) {

            $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
            $objWorkSheet->setCellValue('A' . ($start_row - 1), $wk['name']);
            $objWorkSheet->setCellValue('A' . $start_row, 'ลำดับ');
            $objWorkSheet->setCellValue('B' . $start_row, 'ชื่อ-สกุล');
            $objWorkSheet->setCellValue('C' . $start_row, 'ตำแหน่งงาน');
            $objWorkSheet->setCellValue('D' . $start_row, 'ฝ่าย');
            $objWorkSheet->setCellValue('E' . $start_row, 'ลงชื่อ');
            $objWorkSheet->setCellValue('F' . $start_row, 'หมายเหตุ');

            $_srow = $start_row + 1; // เริ่มใส่ข้อมูลบรรทัดที่ 2
            $i = 1;
            if (isset($arrWageData[$wk['id']])) {
                $arrDataItem = $arrWageData[$wk['id']];
                foreach ($arrDataItem AS $data) {
                    $full_name = $data['WAGE_FIRST_NAME'] . ' ' . $data['WAGE_LAST_NAME'];
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $_srow, ($i), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $_srow, ($full_name), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $_srow, ($data['WAGE_POSITION_NAME']), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $_srow, ($data['SECTION_NAME']), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $_srow, '', \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $i++;
                    $_srow++;
                    $start_row++;
                }
            }

            $start_row = $start_row + 4;
        }

        $objPHPExcel->getActiveSheet()->setTitle('รายชื่อพนักงานลงชื่อ');
        $objPHPExcel->setActiveSheetIndex(0);
        $writer = new Xls($objPHPExcel);
        $writer->save($fileName);
        return true;
    }




    public static function sendzipfilesalary($zipNamefile, $dateExport)
    {
        $basePath = Yii::$app->basePath;
        $destination = $basePath . "/upload/exportsalaryzip/" . $zipNamefile . ".zip";
        return Yii::$app->response->sendFile($destination);
    }



    public static function getreportwagethismonth($Workingreport, $monthselectreport)
    {
        $model = Wagethismonth::find()
            ->select('WAGE_EMP_ID,
                                          WAGE_WORKING_COMPANY,
                                          WAGE_PAY_DATE,
                                          WAGE_SALARY')
            ->where('WAGE_WORKING_COMPANY = ' . $Workingreport . '
                                    AND  WAGE_THIS_MONTH_CONFIRM = "1"
                                    AND  WAGE_THIS_MONTH_BANK_CONFIRM_STATUS = "1"
                                    AND  WAGE_PAY_DATE = "' . $monthselectreport . '"')
            ->asArray()
            ->all();
        return $model;
    }



    public static function gettempthismonth($arrIdtemp)
    {
        $model = Adddeducttemplate::find()
            ->select('ADD_DEDUCT_TEMPLATE_ID,ADD_DEDUCT_TEMPLATE_NAME')
            ->where(['IN', 'ADD_DEDUCT_TEMPLATE_ID', $arrIdtemp])
            ->asArray()
            ->all();
        return $model;
    }



    public static function getreportadddeductthismonth($arrIdcard, $arrIdtemp)
    {
        $model = Adddeductthismonth::find()
            ->select('ADD_DEDUCT_THIS_MONTH_EMP_ID,
                                          ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                          ADD_DEDUCT_THIS_MONTH_TMP_NAME,
                                          ADD_DEDUCT_THIS_MONTH_AMOUNT')
            ->where(['IN', 'ADD_DEDUCT_THIS_MONTH_EMP_ID', $arrIdcard])
            ->andWhere(['IN', 'ADD_DEDUCT_THIS_MONTH_TMP_ID', $arrIdtemp])
            ->asArray()
            ->all();
        return $model;
    }

//
//    public static function getconfrimexportexcel($date, $arrCom)
//    {
//
//        $session = Yii::$app->session;
//        $idcard = $session->get('idcard');
//
//        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
//        $transaction = $connection->beginTransaction();
//        try {
//            $arrValue = [];
//            foreach ($arrCom as $key => $value) {
//                $arrValue[$key]['dateconfirm'] = $date;
//                $arrValue[$key]['id_company'] = $value;
//                $arrValue[$key]['statusconfirm'] = 1;
//                $arrValue[$key]['createby'] = $idcard;
//                $arrValue[$key]['datecreate'] = new Expression('NOW()');;
//                $arrValue[$key]['updateby'] = $idcard;
//                $arrValue[$key]['updatedate'] = new Expression('NOW()');;
//            }
//
//            $connection->createCommand()
//                ->batchInsert('wage_confirm_head', ['dateconfirm',
//                    'id_company',
//                    'statusconfirm',
//                    'createby',
//                    'datecreate',
//                    'updateby',
//                    'updatedate',],
//                    $arrValue)
//                ->execute();
//
//            $transaction->commit();
//
//
//        } catch (ErrorException $e) {
//            $transaction->rollBack();
//            // Yii::warning("Division by zero.");
//            throw new \Exception('ERROR');
//        }
//
//    }

//    public static function movetoadddeducthistory($date, $arrCom)
//    {
//        foreach ($arrCom as $key => $value) {
//            $model = Wagethismonth::find()
//                ->where("WAGE_THIS_MONTH_CONFIRM = '1'")
//                ->andWhere("WAGE_WORKING_COMPANY = '$value'")
//                ->asArray()
//                ->all();
//            //  echo $value;
//            print_r($model);
//        }
//
//    }


}

?>