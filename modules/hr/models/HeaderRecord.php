<?php

namespace app\modules\hr\models;

use Yii;
 
/**
 * This is the model class for table "Header_Record".
 *
 * @property integer $ID_Header
 * @property string $RECORD_TYPE
 * @property string $ACC_NO
 * @property string $BRANCH_NO
 * @property string $PAID_DATE
 * @property string $PAID_PERIOD
 * @property string $COMPANY_NAME
 * @property string $RATE
 * @property string $TOTAL_EMPLOYEE
 * @property string $TOTAL_WAGES
 * @property string $TOTAL_PAID
 * @property string $TOTAL_PAID_BY_EMPLOYEE
 * @property string $TOTAL_PAID_BY_EMPLOYER
 * @property integer $YEARS
 * @property integer $MONTHS
 * @property integer $DEPARTMENT_ID
 * @property integer $COMPANY_ID
 * @property string $pay_date
 */
class HeaderRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Header_Record';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RECORD_TYPE', 'ACC_NO', 'BRANCH_NO', 'PAID_DATE', 'PAID_PERIOD', 'RATE', 'TOTAL_EMPLOYEE', 'TOTAL_WAGES', 'TOTAL_PAID', 'TOTAL_PAID_BY_EMPLOYEE', 'TOTAL_PAID_BY_EMPLOYER', 'YEARS', 'MONTHS', 'DEPARTMENT_ID', 'COMPANY_ID','pay_date'], 'required'],
            [['YEARS', 'MONTHS', 'DEPARTMENT_ID', 'COMPANY_ID'], 'integer'],
            [['RECORD_TYPE'], 'string', 'max' => 1],
            [['ACC_NO'], 'string', 'max' => 10],
            [['BRANCH_NO', 'PAID_DATE', 'TOTAL_EMPLOYEE'], 'string', 'max' => 6],
            [['PAID_PERIOD', 'RATE'], 'string', 'max' => 4],
            [['COMPANY_NAME'], 'string', 'max' => 45],
            [['TOTAL_WAGES'], 'string', 'max' => 15],
            [['TOTAL_PAID'], 'string', 'max' => 14],
            [['pay_date'], 'string', 'max' => 100],
            [['TOTAL_PAID_BY_EMPLOYEE', 'TOTAL_PAID_BY_EMPLOYER'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_Header' => 'Id  Header',
            'RECORD_TYPE' => 'Record  Type',
            'ACC_NO' => 'Acc  No',
            'BRANCH_NO' => 'Branch  No',
            'PAID_DATE' => 'Paid  Date',
            'PAID_PERIOD' => 'Paid  Period',
            'COMPANY_NAME' => 'Company  Name',
            'RATE' => 'Rate',
            'TOTAL_EMPLOYEE' => 'Total  Employee',
            'TOTAL_WAGES' => 'Total  Wages',
            'TOTAL_PAID' => 'Total  Paid',
            'TOTAL_PAID_BY_EMPLOYEE' => 'Total  Paid  By  Employee',
            'TOTAL_PAID_BY_EMPLOYER' => 'Total  Paid  By  Employer',
            'YEARS' => 'Years',
            'MONTHS' => 'Months',
            'DEPARTMENT_ID' => 'Department  ID',
            'COMPANY_ID' => 'Company  ID',
            'pay_date' => 'pay date',
        ];
    }
}
