<?php

namespace app\modules\baymanagement\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/**
 * This is the model class for table "ehole_take_level".
 *
 * @property integer $id
 * @property integer $branch_id
 * @property integer $data_no
 * @property string $emp_idcard
 * @property string $emp_code
 * @property integer $technician_level_id
 * @property string $technician_level_name
 * @property integer $level_code
 * @property integer $erepair_group_id
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class EholeTakeLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ehole_take_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_id', 'data_no', 'emp_idcard', 'emp_code', 'technician_level_id', 'technician_level_name', 'level_code', 'erepair_group_id', 'status_active', 'createby_user', 'create_datetime'], 'required'],
            [['branch_id', 'data_no', 'technician_level_id', 'level_code', 'erepair_group_id', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['emp_idcard', 'createby_user', 'updateby_user'], 'string', 'max' => 13],
            [['emp_code'], 'string', 'max' => 20],
            [['technician_level_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_id' => 'Branch ID',
            'data_no' => 'Data No',
            'emp_idcard' => 'Emp Idcard',
            'emp_code' => 'Emp Code',
            'technician_level_id' => 'Technician Level ID',
            'technician_level_name' => 'Technician Level Name',
            'level_code' => 'Level Code',
            'erepair_group_id' => 'Erepair Group ID',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    public function search($params)
    {
        $data = EholeTakeLevel::find() ->where('status_active <> 99  GROUP BY data_no');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','technician_level_name',$this->technician_level_name]); // ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
