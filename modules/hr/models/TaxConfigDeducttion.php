<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "tax_config_deducttion".
 *
 * @property integer $id
 * @property integer $add_deduct_template_id
 * @property string $detail_add_deduct_template
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class TaxConfigDeducttion extends \yii\db\ActiveRecord
{
    protected $_pageSize;

    function __construct() {
        $this->_pageSize = Yii::$app->params['PAGE_SIZE'];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_config_deducttion';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_deduct_template_id', 'detail_add_deduct_template'], 'required'],
            [['add_deduct_template_id'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['detail_add_deduct_template'], 'string', 'max' => 250],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'add_deduct_template_id' => 'Add Deduct Template ID',
            'detail_add_deduct_template' => 'Detail Add Deduct Template',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    public function search($params)
    {

        $query = TaxConfigDeducttion::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'add_deduct_template_id', $this->add_deduct_template_id],
                               ['like', 'detail_add_deduct_template', $this->detail_add_deduct_template]
                                );

        return $dataProvider;
    }

}
