<?php

namespace app\modules\hr\controllers;


// use yii\rest\ActiveController;
// use yii\filters\Cors;
use app\modules\hr\apihr\AddDeductDetailData;
use app\modules\hr\apihr\ApiPNDTax;
use app\modules\hr\apihr\ApiSSOExport;
use app\modules\hr\apihr\ApiTax;
use app\modules\hr\apihr\ApiWithodingTax;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\models\AdddeductPermanent;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\models\Benefitfund;
use app\modules\hr\models\Empsalary;
use app\modules\hr\models\RelationPosition;
use app\modules\hr\models\Salarychange;
use app\modules\hr\models\SummaryMoneyWage;
use app\modules\hr\models\Wagehistory;
use app\modules\hr\models\Wagethismonth;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Vempadddeductdetail;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\apihr\ApiSalary;
use app\modules\hr\apihr\ApiBenefit;
use app\modules\hr\models\Deptdeduct;
use app\modules\hr\models\Deptdeductdetail;
use app\modules\hr\models\EmpMoneyBonds;
use app\modules\hr\models\OrganicChart;
use app\api\DBConnect;
use app\modules\hr\apihr\ApiPDF;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;
use app\api\Utility;
use app\modules\hr\apihr\ApiOrganize;

use app\modules\vhc\models\serviceold\Wage;
use Interop\Container\Exception\NotFoundException;
use yii\db\Exception;
use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\Session;
use Yii;
use app\modules\hr\controllers\MasterController;

class PayrollController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;

    /**
     * function init() check session active or session login, if not redirect to login page
     * @return \yii\web\Response
     */

    //TODO : check fixed status  to PARAMS is 2

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGettaxdeduct() {
        //testing

        $model = ApiPNDTax::getEmpTaxDeduction();
        ApiPNDTax::print_object($model);
    }

    public function actionExpensesthismonth()
    {

        return $this->render('expensesthismonth');
    }


    public function actionBenefitwithdraw()
    {
        return $this->render('benefitwithdraw');
    }


    public function actionManagededuct()
    {

        return $this->render('managededuct');
    }

    public function actionSearchandedit()
    {
        $data = Adddeducttemplate::find()
            ->where('ADD_DEDUCT_TEMPLATE_STATUS <> 99 ')
            ->asArray()
            ->all();

        $arrAdd = $arrDe = [];
        foreach ($data as $item) {
            if($item['ADD_DEDUCT_TEMPLATE_TYPE']==1) {
                $arrAdd[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item['ADD_DEDUCT_TEMPLATE_NAME'];
            }
            else {
                $arrDe[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item['ADD_DEDUCT_TEMPLATE_NAME'];
            }
        }

        return $this->render('searchandedit',[
            'arrAdd'=>$arrAdd,
            'arrDe'=>$arrDe,
        ]);
    }

    public function actionManagemonthly()
    {

        return $this->render('managemonthly');
    }

    public function actionDeductionsthismonth()
    {


        return $this->render('deductionsthismonth');
    }

    public function actionCheckexpense()
    {

        return $this->render('checkexpense');
    }

    public function actionCheckdeduct()
    {

        return $this->render('checkdeduct');
    }

    public function actionVerifyadddeduct()
    {

        return $this->render('verifyadddeduct');
    }

    public function actionExpensesdeducthismonth()
    {
        return $this->render('expensesdeducthismonth');
    }

    public function actionExpensesdeductallmonth()
    {
        return $this->render('expensesdeductallmonth');
    }

    public function actionPersonalslip()
    {
        $getValue = Yii::$app->request->get();
        $session = Yii::$app->session;
        $session->open();  //open session
        $USER_ID = $session->get('idcard');

        $db['pl'] = DBConnect::getDBConn()['pl'];

        $sql = "SELECT SUBSTRING($db[pl].WAGE_HISTORY.WAGE_PAY_DATE,4) AS Year 
                FROM $db[pl].WAGE_HISTORY WHERE $db[pl].WAGE_HISTORY.WAGE_EMP_ID = '$USER_ID' 
                GROUP BY SUBSTRING($db[pl].WAGE_HISTORY.WAGE_PAY_DATE,4)";
        $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)
            ->queryAll();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return Yii::$app->controller->renderPartial('personalslip',['data'=>$model]);
    }
    public function actionGetdataviewbyyear(Type $var = null)
    {
        $getValue = Yii::$app->request->get();
        $session = Yii::$app->session;
        $USER_ID = $session->get('idcard');
        $db['pl'] = DBConnect::getDBConn()['pl'];
        $sql = "SELECT SUBSTRING($db[pl].WAGE_HISTORY.WAGE_PAY_DATE,1,2) AS mo 
                FROM $db[pl].WAGE_HISTORY WHERE $db[pl].WAGE_HISTORY.WAGE_EMP_ID = '$USER_ID' AND  $db[pl].WAGE_HISTORY.WAGE_PAY_DATE LIKE '%".$getValue['data']."'
                GROUP BY SUBSTRING($db[pl].WAGE_HISTORY.WAGE_PAY_DATE,1,2)";
        $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
        $arr_stringmonth = [];
        foreach($model as $item){
            $arr_stringmonth[$item['mo']] = DateTime::mappingMonth($item['mo']);
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return Yii::$app->controller->renderPartial('_slipPrev',['data'=>$arr_stringmonth,'year'=>$getValue['data']]);
    }

    public function actionPersonaldeduct()
    {
        return Yii::$app->controller->renderPartial('personaldeduct');
    }

    public function actionSubmitdeductdetail()
    {
        $postValue = Yii::$app->request->post();

        //  echo "<pre>";
        // print_r($postValue);
        // //exit;

        $ADD_DEDUCT_DETAIL_CREATE_BY = $this->idcardLogin;
        $ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
        $ADD_DEDUCT_DETAIL_RESP_PERSON = $this->idcardLogin;

        $arrValue = [];
        foreach ($postValue['paytypededuct'] as $key => $value) {
            $payTypePay = $postValue['paytypededuct'][$key];

            if ($payTypePay == '1') {
                $datePayNow = date('m-Y');
                if ($postValue['usedate'][$key]) {
                    $postusedate = $postValue['usedate'][$key];
                    $dateUsePay = date($postusedate . '-t');
                } else {
                    $postusedate = "0000-00-00";
                    $dateUsePay = $postusedate;
                }

                if ($postValue['enddate'][$key]) {
                    $postenddate = $postValue['enddate'][$key];
                    $dateEndPay = date($postenddate . '-t');
                } else {
                    $postenddate = "0000-00-00";
                    $dateEndPay = $postenddate;
                }


            } elseif ($payTypePay == '2') {
                $valueYearPay = substr($postValue['paydate'][$key], 0, 4);
                $valueMonthPay = substr($postValue['paydate'][$key], 5, 2);
                if ($postValue['paydate'][$key]) {
                    $datePayNow = $valueMonthPay . '-' . $valueYearPay;
                } else {
                    $datePayNow = date('m-Y');
                }
                $dateUsePay = date('Y-m-t');
                $dateEndPay = date('Y-m-t');
            }
            $arrValue[] = array(
                'ADD_DEDUCT_ID' => $postValue['id_deduct'][$key],
                'ADD_DEDUCT_DETAIL' => $postValue['detail'][$key],
                'ADD_DEDUCT_DETAIL_PAY_DATE' => $datePayNow,
                'ADD_DEDUCT_DETAIL_START_USE_DATE' => $dateUsePay,
                'ADD_DEDUCT_DETAIL_END_USE_DATE' => $dateEndPay,
                'INSTALLMENT_START' => '',
                'INSTALLMENT_END' => '',
                'INSTALLMENT_CURRENT' => '',
                'ADD_DEDUCT_DETAIL_EMP_ID' => $postValue['Id_Card'],
                'ADD_DEDUCT_DETAIL_AMOUNT' => $postValue['amount'][$key],
                'ADD_DEDUCT_DETAIL_TYPE' => $payTypePay,
                'ADD_DEDUCT_DETAIL_STATUS' => '1',
                'ADD_DEDUCT_DETAIL_HISTORY_STATUS' => '',
                'ADD_DEDUCT_DETAIL_GROUP_STATUS' => '',
                'ADD_DEDUCT_DETAIL_CREATE_DATE' => date('Y-m-t'),
                'ADD_DEDUCT_DETAIL_CREATE_BY' => $ADD_DEDUCT_DETAIL_CREATE_BY,
                'ADD_DEDUCT_DETAIL_UPDATE_BY' => $ADD_DEDUCT_DETAIL_UPDATE_BY,
                'ADD_DEDUCT_DETAIL_UPDATE_DATE' => date('Y-m-t'),
                'ADD_DEDUCT_DETAIL_RESP_PERSON' => $ADD_DEDUCT_DETAIL_RESP_PERSON
            );

        }

        // print_r($arrValue);
        // exit;

        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {

            $connection->createCommand()
                ->batchInsert('ADD_DEDUCT_DETAIL', ['ADD_DEDUCT_ID',
                    'ADD_DEDUCT_DETAIL',
                    'ADD_DEDUCT_DETAIL_PAY_DATE',
                    'ADD_DEDUCT_DETAIL_START_USE_DATE',
                    'ADD_DEDUCT_DETAIL_END_USE_DATE',
                    'INSTALLMENT_START',
                    'INSTALLMENT_END',
                    'INSTALLMENT_CURRENT',
                    'ADD_DEDUCT_DETAIL_EMP_ID',
                    'ADD_DEDUCT_DETAIL_AMOUNT',
                    'ADD_DEDUCT_DETAIL_TYPE',
                    'ADD_DEDUCT_DETAIL_STATUS',
                    'ADD_DEDUCT_DETAIL_HISTORY_STATUS',
                    'ADD_DEDUCT_DETAIL_GROUP_STATUS',
                    'ADD_DEDUCT_DETAIL_CREATE_DATE',
                    'ADD_DEDUCT_DETAIL_CREATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_DATE',
                    'ADD_DEDUCT_DETAIL_RESP_PERSON',],
                    $arrValue)
                ->execute();

            $transaction->commit();

            $id_card = $postValue['Id_Card'];
            $dataEmpPersanal = ApiHr::getSeachDataPersonalByIdcard($id_card);

            return $this->redirect(array('/hr/payroll/gridviewducuctdetail', 'id_card' => $id_card), 302);


        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }


    }

    public function actionExpensesallmonth()
    {
        return $this->render('expensesallmonth');
    }


    //action for department input overtime by owner
    public function actionDepartmentovertime()
    {
        return $this->render('departmentovertime');
    }


    public function actionDepartmentdeduct()
    {
        return $this->render('departmentdeduct');
    }


    public function actionDeptdeductlist()
    {
        $id_cardlogin = $this->idcardLogin;
        $listworkbyidcard = ApiHr::getCompanyByIdcard($id_cardlogin);
        // print_r($listworkbyidcard);
        return $this->render('deptdeductlist', ['selectworking' => $listworkbyidcard]);

    }

    public function actionAdddeductbydept()
    {
        $id_cardlogin = $this->idcardLogin;
        $listworkbyidcard = ApiHr::getCompanyByIdcard($id_cardlogin);
        // print_r($listworkbyidcard);
        return $this->render('adddeductbydept', ['selectworking' => $listworkbyidcard]);
    }

    public function actionApproveddeductbydept()
    {
        return $this->render('approveddeductbydept');
    }

    public function actionHistorydeductbydept()
    {
        return $this->render('historydeductbydept');
    }


    public function actionAutocomp()
    {
        $model = ApiPayroll::getEmpdata();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;

    }

    public function actionGetlistdeducttem()
    {
        $model = ApiPayroll::getDataDeductTemp();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;
    }

    public static function actionGetdepartment()
    {
        $getValue = Yii::$app->request->get();

        $id_company = $getValue['id_company'];

        $model = ApiHr::getDepartment($id_company);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;
    }

    public static function actionGetsection()
    {
        $getValue = Yii::$app->request->get();

        $id_department = $getValue['id_department'];

        $model = ApiHr::getSection($id_department);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;
    }

    public function actionGetdatadeductdetail()
    {
        $getValue = Yii::$app->request->get();

        $id_card = $getValue['id_card'];

        $model = ApiPayroll::getDatadeductdetail($id_card);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;
    }

    public function actionGetdataempjson()
    {
        $getValue = Yii::$app->request->get();
        $id_card = $getValue['id_card'];

        $model = ApiHr::getSeachDataPersonalByIdcard($id_card);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;
    }

    public function actionGridviewducuctdetail()
    {
        $getValue = Yii::$app->request->get();
        $id_card = $getValue['id_card'];

        $dataEmpPersanal = ApiHr::getSeachDataPersonalByIdcard($id_card);


        $query = Vempadddeductdetail::find()
            ->where('Add_Deductdetail_Status <> 99')
            ->andWhere(['=', 'Add_Deductdetail_Idemp', $id_card])
            ->andWhere(['<>', 'Add_Deducttemplate_Type', 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        return $this->render('deductionsthismonth', [
            'dataProvider' => $dataProvider,
            'dataEmpPersanal' => $dataEmpPersanal,
        ]);

    }

    public function actionSaveexpensesthismonth()
    {
        $postValue = Yii::$app->request->post();

        $add_deduct_template_id = $postValue['add_deduct_template_id'];
        $id_emp = $postValue ['id_emp'];
        $ADD_DEDUCT_DETAIL_AMOUNT = $postValue ['total'];
        $detail = $postValue ['detail'];

        $date_pay = $postValue['datepay'];
        $dateRang = DateTime::makeDayFromMonthPicker($date_pay);
        //$start_date = $dateRang['start_date'];
        $end_date = $dateRang['end_date'];

        $ADD_DEDUCT_DETAIL_CREATE_BY = $this->idcardLogin;
        $ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
        $ADD_DEDUCT_DETAIL_RESP_PERSON = $this->idcardLogin;

       // $id_card = array_combine($id_emp, $id_emp);
       // $ADD_DEDUCT_DETAIL_AMOUNT = array_combine($id_emp, $ADD_DEDUCT_DETAIL_AMOUNT);
        //$detail = array_combine($id_emp, $detail);

        $addDeductdetail = [];

        $key = 0;
        foreach ($id_emp as $item) {
            $totalAmount = Utility::removeCommar($ADD_DEDUCT_DETAIL_AMOUNT[$key]);
            $addDeductdetail[$key]['ADD_DEDUCT_ID'] = $add_deduct_template_id;
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL'] = $detail[$key];
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_PAY_DATE'] = $date_pay;
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_START_USE_DATE'] = $end_date;
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_END_USE_DATE'] = $end_date ;
            $addDeductdetail[$key]['INSTALLMENT_START'] = '';
            $addDeductdetail[$key]['INSTALLMENT_END'] = '';
            $addDeductdetail[$key]['INSTALLMENT_CURRENT'] = '';
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_EMP_ID'] = $item;
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_AMOUNT'] = $totalAmount;
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_TYPE'] = '2';
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_STATUS'] = '2'; //รายการรอคำนวณในเดือนนี้
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_HISTORY_STATUS'] = '';
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_GROUP_STATUS'] = '';
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_CREATE_DATE'] = new Expression('NOW()');
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_CREATE_BY'] = $ADD_DEDUCT_DETAIL_CREATE_BY;
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_UPDATE_BY'] = $ADD_DEDUCT_DETAIL_UPDATE_BY;
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_UPDATE_DATE'] = new Expression('NOW()');
            $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_RESP_PERSON'] = $ADD_DEDUCT_DETAIL_RESP_PERSON;
            $addDeductdetail[$key]['dept_deduct_id'] = 0;
            $addDeductdetail[$key]['dept_deduct_detail_id'] = 0;

            $key++;
        }


        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {

            $connection->createCommand()
                ->batchInsert('ADD_DEDUCT_DETAIL', ['ADD_DEDUCT_ID',
                    'ADD_DEDUCT_DETAIL',
                    'ADD_DEDUCT_DETAIL_PAY_DATE',
                    'ADD_DEDUCT_DETAIL_START_USE_DATE',
                    'ADD_DEDUCT_DETAIL_END_USE_DATE',
                    'INSTALLMENT_START',
                    'INSTALLMENT_END',
                    'INSTALLMENT_CURRENT',
                    'ADD_DEDUCT_DETAIL_EMP_ID',
                    'ADD_DEDUCT_DETAIL_AMOUNT',
                    'ADD_DEDUCT_DETAIL_TYPE',
                    'ADD_DEDUCT_DETAIL_STATUS',
                    'ADD_DEDUCT_DETAIL_HISTORY_STATUS',
                    'ADD_DEDUCT_DETAIL_GROUP_STATUS',
                    'ADD_DEDUCT_DETAIL_CREATE_DATE',
                    'ADD_DEDUCT_DETAIL_CREATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_DATE',
                    'ADD_DEDUCT_DETAIL_RESP_PERSON','dept_deduct_id','dept_deduct_detail_id',],
                    $addDeductdetail)
                ->execute();

            $transaction->commit();
            $rowInsert = count($addDeductdetail);

            return $rowInsert;

        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }

    }

    public function actionSeachempdeductions()
    {
        $postValue = Yii::$app->request->post();
        $id_company = $postValue['selectworking'];
        $id_department = $postValue['selectdepartment'];
        $id_section = $postValue['selectsection'];

        $dataEmp = ApiPayroll::seachEmpforDeductions($id_company, $id_department, $id_section);

        return $this->render('deductionsthismonth', ['dataEmp' => $dataEmp,
            'selectworking' => $id_company,
            'selectdepartment' => $id_department,
            'selectsection' => $id_section,
            'query' => true]);

    }

    public function actionSavedeductionsthismonth()
    {
        $postValue = Yii::$app->request->post();
        $selectmonth = $postValue['selectmonth'];
        $selectyear = $postValue['selectyear'];
        $add_deduct_template_id = $postValue['add_deduct_template_id'];
        $id_emp = $postValue ['id_emp'];
        $ADD_DEDUCT_DETAIL_AMOUNT = $postValue ['total'];
        $detail = $postValue ['detail'];

        $ADD_DEDUCT_DETAIL_CREATE_BY = $this->idcardLogin;
        $ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
        $ADD_DEDUCT_DETAIL_RESP_PERSON = $this->idcardLogin;

        $id_card = array_combine($id_emp, $id_emp);
        $ADD_DEDUCT_DETAIL_AMOUNT = array_combine($id_emp, $ADD_DEDUCT_DETAIL_AMOUNT);
        $detail = array_combine($id_emp, $detail);

        $addDeductdetail = [];
        foreach ($ADD_DEDUCT_DETAIL_AMOUNT as $key => $VALUE_ADD_DEDUCT_DETAIL_AMOUNT) {
            if (array_key_exists($key, $id_card)) {
                foreach ($detail as $key => $valuedetail) {
                    if (array_key_exists($key, $id_card)) {

                        $addDeductdetail[$key]['ADD_DEDUCT_ID'] = $add_deduct_template_id;
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL'] = $detail[$key];
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_PAY_DATE'] = date('m-Y');
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_START_USE_DATE'] = date('Y-m-t');
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_END_USE_DATE'] = date('Y-m-t');
                        $addDeductdetail[$key]['INSTALLMENT_START'] = '';
                        $addDeductdetail[$key]['INSTALLMENT_END'] = '';
                        $addDeductdetail[$key]['INSTALLMENT_CURRENT'] = '';
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_EMP_ID'] = $key;
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_AMOUNT'] = $ADD_DEDUCT_DETAIL_AMOUNT[$key];
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_TYPE'] = '2';
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_STATUS'] = '2';  //รายการรอคำนวณในเดือนนี้
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_HISTORY_STATUS'] = '';
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_GROUP_STATUS'] = '';
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_CREATE_DATE'] = new Expression('NOW()');
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_CREATE_BY'] = $ADD_DEDUCT_DETAIL_CREATE_BY;
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_UPDATE_BY'] = $ADD_DEDUCT_DETAIL_UPDATE_BY;
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_UPDATE_DATE'] = new Expression('NOW()');
                        $addDeductdetail[$key]['ADD_DEDUCT_DETAIL_RESP_PERSON'] = $ADD_DEDUCT_DETAIL_RESP_PERSON;
                    }
                }
            }
        }

        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {

            $connection->createCommand()
                ->batchInsert('ADD_DEDUCT_DETAIL', ['ADD_DEDUCT_ID',
                    'ADD_DEDUCT_DETAIL',
                    'ADD_DEDUCT_DETAIL_PAY_DATE',
                    'ADD_DEDUCT_DETAIL_START_USE_DATE',
                    'ADD_DEDUCT_DETAIL_END_USE_DATE',
                    'INSTALLMENT_START',
                    'INSTALLMENT_END',
                    'INSTALLMENT_CURRENT',
                    'ADD_DEDUCT_DETAIL_EMP_ID',
                    'ADD_DEDUCT_DETAIL_AMOUNT',
                    'ADD_DEDUCT_DETAIL_TYPE',
                    'ADD_DEDUCT_DETAIL_STATUS',
                    'ADD_DEDUCT_DETAIL_HISTORY_STATUS',
                    'ADD_DEDUCT_DETAIL_GROUP_STATUS',
                    'ADD_DEDUCT_DETAIL_CREATE_DATE',
                    'ADD_DEDUCT_DETAIL_CREATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_DATE',
                    'ADD_DEDUCT_DETAIL_RESP_PERSON',],
                    $addDeductdetail)
                ->execute();

            $transaction->commit();

            $rowInsert = count($addDeductdetail);

            return $rowInsert;
        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }
        Yii::$app->session->setFlash('success', "บันทึกข้อมูลสำเร็จ");
        return $this->render('deductionsthismonth');

    }




    public function actionSaveexpensesallmonth()
    {

        $postValue = Yii::$app->request->post();
        $arrAllTemplate = ApiPayroll::getArrayAdddeductTemplate();

        $ADD_DEDUCT_DETAIL_CREATE_BY = $this->idcardLogin;
        $ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
        //$ADD_DEDUCT_DETAIL_RESP_PERSON = $this->idcardLogin;

        $arrValue = [];
        foreach ($postValue['id_emp'] as $key => $value) {

            //start date use
            $arrDateRange = DateTime::makeDateRangeDeduct($postValue['start_month_add'][$key],$postValue['start_year_add'][$key],$postValue['end_month_add'][$key],$postValue['end_year_add'][$key]);

            $useStartDate = $arrDateRange['start_date'];
            $useEndDate = $arrDateRange['end_date'];
            $arrTemplate = $arrAllTemplate[$postValue['id_deduct_template']];
            $arrValue[] = array(
                'ADD_DEDUCT_TEMPLATE_ID' => $postValue['id_deduct_template'],
                'TEMPLATE_TYPE_ID'=> $arrTemplate['ADD_DEDUCT_TEMPLATE_TYPE'],
                'ADD_DEDUCT_DETAIL' => $postValue['detail'][$key],
                'ADD_DEDUCT_DETAIL_START_USE_DATE' => $useStartDate,
                'ADD_DEDUCT_DETAIL_END_USE_DATE' => $useEndDate,
                'ADD_DEDUCT_DETAIL_EMP_ID' => $postValue['id_emp'][$key],
                'ADD_DEDUCT_DETAIL_AMOUNT' => Utility::removeCommar($postValue['total'][$key]),
                'ADD_DEDUCT_DETAIL_TYPE' => 1, //permanent
                'ADD_DEDUCT_DETAIL_STATUS' => 2, //active รายการรอคำนวณในเดือนนี้
                'ADD_DEDUCT_DETAIL_CREATE_DATE' => new Expression('NOW()'),
                'ADD_DEDUCT_DETAIL_CREATE_BY' => $ADD_DEDUCT_DETAIL_CREATE_BY,
                'ADD_DEDUCT_DETAIL_UPDATE_BY' => $ADD_DEDUCT_DETAIL_UPDATE_BY,
                'ADD_DEDUCT_DETAIL_UPDATE_DATE' => new Expression('NOW()'),
            );

        }

        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {

            $connection->createCommand()
                ->batchInsert('ADD_DEDUCT_PERMANENT', [
                    'ADD_DEDUCT_TEMPLATE_ID',
                    'TEMPLATE_TYPE_ID',
                    'ADD_DEDUCT_DETAIL',
                    'ADD_DEDUCT_DETAIL_START_USE_DATE',
                    'ADD_DEDUCT_DETAIL_END_USE_DATE',
                    'ADD_DEDUCT_DETAIL_EMP_ID',
                    'ADD_DEDUCT_DETAIL_AMOUNT',
                    'ADD_DEDUCT_DETAIL_TYPE',
                    'ADD_DEDUCT_DETAIL_STATUS',
                    'ADD_DEDUCT_DETAIL_CREATE_DATE',
                    'ADD_DEDUCT_DETAIL_CREATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_BY',
                    'ADD_DEDUCT_DETAIL_UPDATE_DATE',],
                    $arrValue)
                ->execute();

            $transaction->commit();

            $rowInsert = count($arrValue);

            return $rowInsert;

            // Yii::$app->session->setFlash('success', "บันทึกข้อมูลสำเร็จ");
            // return $this->redirect(array('/hr/payroll/expensesallmonth'), 302);

        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }

    }




    public function actionMakesalary()
    {
        return $this->render('makesalary');
    }

    public function actionValidatesalary()
    {
        //cheet code for FORCE INSERT
        $sso = \Yii::$app->request->get('sso',0) ;
        $pnd = \Yii::$app->request->get('pnd',0) ;
        $wht = \Yii::$app->request->get('wht',0) ;
        return $this->render('validatesalary', [
                'sso'=>$sso,
                'pnd'=>$pnd,
                'wht'=>$wht
            ]);
    }

    public function actionApprovedsalary()
    {
        return $this->render('approvedsalary');
    }

    public function actionAdjustsalary()
    {
        return $this->render('adjustsalary');
    }

    public function actionGetdeductlisttempall()
    {
        $getValue = Yii::$app->request->get();

        $selecttypepay = $getValue['selecttypepay'];
        if ($selecttypepay == "1") {
            $model = ApiPayroll::listdata_add_deduct_template();
        } else {
            $model = ApiPayroll::getDataDeductTemp();
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;
    }

    public function actionLoaddeducttemplate()
    {
        $model = ApiPayroll::getDataDeductAll();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }



    public function actionSeachempdeductall()
    {
        //if (Yii::$app->request->isAjax) {
        $postValue = Yii::$app->request->post();

        $selectworking = $postValue['selectworking'];
        $selectdepartment = $postValue['selectdepartment'];
        $selectsection = $postValue['selectsection'];
        $monthselect = $postValue['monthselect'];

        $selectdeduct = $postValue['selectdeduct'];
        $selectadddeduct = $postValue['selectadddeduct'];

        //$con_deduction = $selectdeduct;
        $con_deduction = ($selectdeduct) ?  $selectdeduct : $selectadddeduct;
        $model = ApiPayroll::getDataDeductDetailByIdcard($selectworking, $selectdepartment, $selectsection, $con_deduction, $monthselect);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // print_r($model);
        return $model;
        // }
    }

    public function actionSeachempdeforadddeductbydept()
    {
        //if (Yii::$app->request->isAjax) {
        $postValue = Yii::$app->request->post();
        $monthselect = $postValue['monthselect'];
        $template_type_id = $postValue['template_type_id'];
        $loadofme = $postValue['loadofme'];


/*
        if(empty($monthselect)) {
            throw new \Exception('ไม่ได้เลือกรอบเงินเดือน');
        }*/

        //default condition load
        $arrCond = [
            'is_record_approved'=>0
        ];


        if(!empty($monthselect) && $monthselect != '') {
            $arrCond['pay_date'] = $monthselect;
        }


        //load only me
        if(!empty($loadofme) && $loadofme==1) {
            $arrCond['create_by'] = $this->idcardLogin;
        }


        //IF post select template
        if(!empty($template_type_id)) {
            $arrCond['add_deduct_template_id'] = $template_type_id;
        }

        $arrAllEmp = ApiHr::getAllEmpinDB();


        $model = Deptdeductdetail::find()
            ->where($arrCond)->orderBy('id ASC')->asArray()->all();
        $arrList = [];
        if(is_array($model) || is_object($model)) {
            foreach ($model as $item) {
                $arrItem = [
                    'id'=>$item['id'],
                    'dept_deduct_id' => $item['dept_deduct_id'],
                    'pay_date' => $item['pay_date'],
                    'id_card' => $item['id_card'],
                    'emp_name' => $arrAllEmp[$item['id_card']],
                    'add_deduct_template_id' => $item['add_deduct_template_id'],
                    'add_deduct_template_name' => $item['add_deduct_template_name'],
                    'dept_deduct_amount' => $item['dept_deduct_amount'],
                    'record_comment' => $item['record_comment'],
                    'create_datetime' => $item['create_datetime'],
                    'create_by' => $item['create_by'],
                    'create_empname'=> $arrAllEmp[$item['create_by']]
                ];
                $arrList[] = $arrItem;
            }
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $arrList;

    }

    public function actionAutocompforadddeductbydept()
    {
        $postValue = Yii::$app->request->post();

        $selectworking = $postValue['selectworking'];
        $selectdepartment = $postValue['selectdepartment'];
        $selectsection = $postValue['selectsection'];

        $model = ApiHr::getEmpdataByCompany($selectworking, $selectdepartment, $selectsection);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;

    }

    public function actionDeleteadddeductdetail()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $add_deductdetail_id = $postValue['add_deductdetail_id'];
            $modeldelete = ApiPayroll::deleteDataDeductdetailByAddDeductdetailId($add_deductdetail_id);
            return ($modeldelete) ? 1 : 0;
        }
    }

    public function actionDeleteadddeductbydept()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $id = $postValue['id'];

            if(empty($id)) {
                throw new Exception('ค่าส่งมาไม่ครบ');
            }

            $model = Deptdeductdetail::findOne($id);
            if($model !== null) {
                return $model->delete();
            }
            else {
                return 0;
            }
        }
    }

    public function actionSaveadddeductdetail()
    {
        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {
            if (Yii::$app->request->isAjax) {
                $postValue = Yii::$app->request->post();

                $add_deductdetail_id = $postValue['add_deductdetail_id'];
                $amounttotal = $postValue['amounttotal'];
                $detaildeduct = $postValue['detaildeduct'];

                if(empty($add_deductdetail_id)) {
                    throw  new  Exception('ส่งค่ามาไม่ครบ');
                }

                $model = Adddeductdetail::findOne($add_deductdetail_id);
                if($model !== null) {
                    $model->ADD_DEDUCT_DETAIL = $detaildeduct;
                    $model->ADD_DEDUCT_DETAIL_AMOUNT = Utility::removeCommar($amounttotal);
                    $model->ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
                    $model->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
                    return $model->save();
                }
                else {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }

               // $modeledit = ApiPayroll::saveEditDeductdetailByAddDeductdetailId($add_deductdetail_id, $id_emp, $selecttypeadddeduc, $amounttotal, $detaildeduct);
               // return ($modeledit) ? 1 : 0;
            }

        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }
    }

    public function actionSeachembyidcard()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $id_card = $postValue['id_card'];

            $model = ApiHr::getSeachDataPersonalByIdcard($id_card);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $model;

        }
    }

    public function actionDeletepermanent(){
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $id = $postValue['id'];
            if ($id == '') {
                throw  new  Exception('ส่งค่ามาไม่ครบ');
            }

            if (($model = AdddeductPermanent::findOne($id)) !== null) {
                $model->ADD_DEDUCT_DETAIL_STATUS = 99;
                $model->ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
                $model->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
                return $model->save();
            }

        }
    }


    public function actionSavepermanent()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $id = $postValue['id'];
            if($id == '') {
                throw  new  Exception('ส่งค่ามาไม่ครบ');
            }

            if(empty($postValue['date_use_start'])) {
                throw  new  Exception('ส่งค่ามาไม่ครบ');
            }

            if (($model = AdddeductPermanent::findOne($id)) !== null) {
                $model->ADD_DEDUCT_TEMPLATE_ID = $postValue['template_id'];
                $model->ADD_DEDUCT_DETAIL_START_USE_DATE = DateTime::DateToMysqlDB($postValue['date_use_start']);
                $model->ADD_DEDUCT_DETAIL_END_USE_DATE = DateTime::DateToMysqlDB($postValue['date_use_end']);
                $model->ADD_DEDUCT_DETAIL_AMOUNT = Utility::removeCommar($postValue['master_amount']);
                $model->ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
                $model->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
                return $model->save();
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

        }
    }

    public function actionSeachpermanentmonthly()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $id_card = $postValue['id_card'];
            $valueselecttypepay = $postValue['valueselecttypepay'];
            if($id_card == '') {
                throw  new  Exception('ส่งค่ามาไม่ครบ');
            }
            $model = ApiPayroll::getAddDeductPermanent($id_card,$valueselecttypepay);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }



    public function actionSeachdeducdetailofmanagemonthly()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $id_card = $postValue['id_card'];
            $valueselecttypepay = $postValue['valueselecttypepay'];

            if($id_card == '' || $valueselecttypepay=='') {
                throw  new  Exception('ส่งค่ามาไม่ครบ');
            }
            $model = ApiPayroll::getDatadeductdetailByIdcardAndTypepay($id_card, $valueselecttypepay);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionDeletedeductdetailformanagemonthly()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $add_deductdetail_id = $postValue['add_deductdetail_id'];
            $id_card = $postValue['id_card'];
            if($id_card == null || $id_card =='') {
                throw  new  Exception('ส่งค่ามาไม่ครบ');
            }

            $valueselecttypepay = $postValue['valueselecttypepay'];
            $modeldelete = ApiPayroll::deleteDataDeductdetailByAddDeductdetailId($add_deductdetail_id);
            if ($modeldelete) {
                $model = ApiPayroll::getDatadeductdetailByIdcardAndTypepay($id_card, $valueselecttypepay);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            }

        }
    }

    public function actionSavedeductdetailformanagemonthly()
    {
        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {
            if (Yii::$app->request->isAjax) {
                $postValue = Yii::$app->request->post();

                $id_emp = $postValue['id_emp'];
                //$selecttypepaylist = $postValue['selecttypepaylist'];
                //$selecttypeadddeduc = $postValue['selecttypeadddeduc'];
                //$monthpaydate = $postValue['monthpaydate'];
                //$monthusedate = $postValue['monthusedate'];
                $amounttotal = Utility::removeCommar($postValue['amounttotal']);
                $remarks = $postValue['remarks'];
                $add_deductdetail_id = $postValue['add_deductdetail_id'];
                $detaildeduct = $postValue['$detaildeduct'];
                $valueselecttypepay = $postValue['valueselecttypepay'];

                $idupdate = $this->idcardLogin;


                if(empty($id_emp)){
                    throw  new Exception('ID Card not found');
                }

                //echo $add_deductdetail_id;
                $model = Adddeductdetail::findOne($add_deductdetail_id);

                //$model->ADD_DEDUCT_ID = $selecttypeadddeduc;
                //$model->ADD_DEDUCT_DETAIL_PAY_DATE = $monthpaydate;

                //$model->ADD_DEDUCT_DETAIL_END_USE_DATE = DateTime::DateToMysqlDB($monthusedate);
                //$model->ADD_DEDUCT_DETAIL_EMP_ID = $id_emp;
                //$model->ADD_DEDUCT_DETAIL_TYPE = $selecttypepaylist;
                $model->ADD_DEDUCT_DETAIL_AMOUNT = $amounttotal;
                $model->ADD_DEDUCT_DETAIL = $remarks;
                //$model->ADD_DEDUCT_DETAIL_STATUS = Yii::$app->params['INACTIVE_STATUS'];
                $model->ADD_DEDUCT_DETAIL_UPDATE_BY = $idupdate;
                $model->ADD_DEDUCT_DETAIL_RESP_PERSON = $idupdate;
                $model->ADD_DEDUCT_DETAIL_UPDATE_DATE = date('Y-m-d H:i:s');
                $model->save();

                $modelresult = ApiPayroll::getDatadeductdetailByIdcardAndTypepay($id_emp, $valueselecttypepay);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $modelresult;


                //$modelADD_DEDUCT_DETAIL_START_USE_DATE = $model['ADD_DEDUCT_DETAIL_START_USE_DATE'];
                //$modelADD_DEDUCT_DETAIL_END_USE_DATE = $model['ADD_DEDUCT_DETAIL_END_USE_DATE'];

            }
        } catch (ErrorException $e) {
            $transaction->rollBack();
            // Yii::warning("Division by zero.");
            throw new \Exception('ERROR');
        }
    }

    public function actionSaveeditadddeductbydept()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $id = $postValue['id'];
            $amounttotal = $postValue['amounttotal'];
            $record_comment= $postValue['record_comment'];

            if(empty($id)) {
                throw new Exception('ส่งข้อมูลมาไม่ครบ');
            }

            $idupdate = Yii::$app->session->get('idcard');
            $model = Deptdeductdetail::findOne($id);
            if($model !== null) {
                $model->dept_deduct_amount = Utility::removeCommar($amounttotal);
                $model->record_comment = $record_comment;
                $model->update_by = $idupdate;
                $model->update_datetime = new Expression('NOW()');
                return $model->save();
            }
            else {
                return 0;
            }

        }
    }

    public function actionUpdateapproveddeduct()
    {

            $postValue = Yii::$app->request->post();
            $selectworking = $postValue['selectworking'];
            $selectdepartment = $postValue['selectdepartment'];
            $selectsection = $postValue['selectsection'];
            $monthselect = $postValue['monthselect'];

            $monthresult = DateTime::selectMonthNotZero($monthselect);
            $for_month = $monthresult['monthresult'];
            $for_year = $monthresult['year'];
            $postgetidapprove = $postValue['getidapprove'];
            //print_r($postgetidapprove);

            $modelsave = ApiPayroll::saveUpdateapproveddeduct($postgetidapprove);
    }


    public function actionSaveadddeductbydept()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $arrPostdata = [];
            $id_create_by = $this->idcardLogin;

            $arrEmpID = $postValue['id_emp'];
            $arrTotal = $postValue['total'];
            $arrDetail = $postValue['detail'];
            $pay_date = $postValue['datepay'];
            $template_id = $postValue['add_deduct_template_id'];

            $my = explode('-',$pay_date);



            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $transaction = $connection->beginTransaction();
            try {

                $headmodel = new Deptdeduct();
                $headmodel->id = null;
                $headmodel->division_id = 0;
                $headmodel->dept_id = 0;
                $headmodel->company_id = 0;
                $headmodel->for_year = $my[1];
                $headmodel->for_month = $my[0];
                $headmodel->pay_date = $pay_date;
                $headmodel->subject = null;
                $headmodel->description = null;
                $headmodel->is_approved = 0;
                $headmodel->approved_datetime = null;
                $headmodel->approved_by = null;
                $headmodel->dept_deduct_status = 1;
                $headmodel->create_by = $id_create_by;
                $headmodel->create_datetime = new Expression('NOW()');
                $headmodel->update_by = null;
                $headmodel->update_datetime = null;
                $headmodel->by_dept_id = Yii::$app->session->get('companyid');
                $headmodel->by_dept_name = Yii::$app->session->get('companyname');
                $headmodel->save();


                $masterID = $connection->getLastInsertID();
                $arrTemplate = ApiPayroll::getArrayAdddeductTemplate();


                foreach ($arrEmpID as $key => $value) {
                    $arrPostdata[$key]['id'] = null;
                    $arrPostdata[$key]['dept_deduct_id'] = $masterID;
                    $arrPostdata[$key]['pay_date'] = $pay_date ;
                    $arrPostdata[$key]['id_card'] = $arrEmpID[$key];
                    $arrPostdata[$key]['is_record_approved'] = '0';
                    $arrPostdata[$key]['record_approved_datetime'] = null;
                    $arrPostdata[$key]['record_approved_by'] = null;
                    $arrPostdata[$key]['add_deduct_template_id'] = $template_id;
                    $arrPostdata[$key]['add_deduct_template_name'] = $arrTemplate[$template_id]['ADD_DEDUCT_TEMPLATE_NAME'];
                    $arrPostdata[$key]['dept_deduct_amount'] = Utility::removeCommar($arrTotal[$key]);
                    $arrPostdata[$key]['doc_notice_id'] = null;
                    $arrPostdata[$key]['doc_notice_code'] = null;
                    $arrPostdata[$key]['record_comment'] = $arrDetail[$key];
                    $arrPostdata[$key]['create_by'] = $id_create_by;
                    $arrPostdata[$key]['create_datetime'] = new Expression('NOW()');
                    $arrPostdata[$key]['update_by'] = null;
                    $arrPostdata[$key]['update_datetime'] = null;

                }

                $attrDept_deduct_detail = Deptdeductdetail::getTableSchema()->getColumnNames();
                $connection->createCommand()->batchInsert('dept_deduct_detail',$attrDept_deduct_detail,$arrPostdata)->execute();
                $transaction->commit();

            } catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception('ERROR');
            }


        }
    }


/*    public function actionRecalsalary()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            if(isset($postValue['mode']) && $postValue['mode']=='recal') {
                $arrEmpIDCard = array_unique($postValue['id_emp']);
                return ApiSalary::resetSalary($arrEmpIDCard);
            }
        }
    }*/


    public function actionRecalmoney ()
    {

        if (Yii::$app->request->isAjax) {

            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;

            $transaction = $connection->beginTransaction();
            try {


                $postVar = Yii::$app->request->post();
                $arrAddDeduct = $postVar['adddeduct'];
                $recal_idcard = $postVar['idcard'];
                $wage_id = $postVar['wage_id'];
                $force_sso = $postVar['force_sso']; //cheet code IF 1, FORCE ADD INSERT RECORD SSO
                $force_pnd = $postVar['force_pnd']; //cheet code IF 1, FORCE ADD INSERT RECORD PND
                $force_wht = $postVar['force_wht']; //cheet code IF 1, FORCE ADD INSERT RECORD WHT

                $wage_salary = ($postVar['wage_salary']) ? Utility::removeCommar($postVar['wage_salary']) : 0;
                $new_salary = floatval($wage_salary);

                $arrKey = $arrAddDeduct['data_id'];
                $arrValue = $arrAddDeduct['data_value'];
                $arrAddDeductList = (count($arrKey) > 0 && count($arrValue) > 0) ? array_combine($arrKey, $arrValue) : [];


                //$sudo_add = ()

                //check validate data
                if (empty($wage_id) || $wage_id == 0) {
                    throw new Exception('wage id not found');
                }

                if (empty($recal_idcard) || $recal_idcard == '') {
                    throw new Exception('ID Card not found');
                }


                //setup action
                $arrRecalIDCard = array($recal_idcard);
                $ACTION = [];
                $ACTION['recal_idcard'] = $arrRecalIDCard;  //mode=recal NEED array ID CARD
                $ACTION['mode'] =  'recal';  //setup mode


                /** LOAD EMP DATA */
                $EmpData = ApiHr::getEmpDataByIDCard($recal_idcard);
                $socialBenefitStatus = $EmpData['socialBenefitStatus'];
                $socialBenifitPercent = $EmpData['socialBenifitPercent'];
                $benifitFundStatus = $EmpData['benifitFundStatus'];
                $benefitFundPercent = $EmpData['benefitFundPercent'];

                $AddDeductTemplate = ApiSalary::get_all_adddeduct_template();


                /** UPDATE WAGE SALARY , IF Change UPDATE and RECAL SSO and BNF */
                $new_WHT = $new_PND = 0;
                $WageThisMonth = Wagethismonth::findOne($wage_id);
                $pay_date = $WageThisMonth->WAGE_PAY_DATE;
                $dateRang = DateTime::makeDayFromMonthPicker($pay_date);


                /** Query date config : หาค่าวันเวลา เริ่มต้น สิ้นสุดรอบเงินเดือน */
                $arrConfig = ApiSalary::buildSalaryDateCondition($pay_date);
                $new_SSO = $WageThisMonth->SSO_AMOUNT;
                $new_BNF = $WageThisMonth->BNF_AMOUNT;
                if ($new_salary != $WageThisMonth->WAGE_SALARY) {
                    $WageThisMonth->WAGE_SALARY = $new_salary;
                    $new_SSO = ($socialBenefitStatus == 1) ? ApiSalary::cal_sso_money($new_salary, $arrConfig) : 0;
                    $new_BNF = ApiSalary::cal_benefit_fund($new_salary, $benifitFundStatus, $arrConfig);
                    $WageThisMonth->BNF_AMOUNT = $new_BNF;
                    $WageThisMonth->SSO_AMOUNT = $new_SSO;
                    $WageThisMonth->WAGE_CREATE_DATE = new Expression('NOW()');
                    $WageThisMonth->WAGE_CREATE_BY = $this->idcardLogin;
                    $WageThisMonth->update(false);
                }

                /** Load config template */
                $CfgTempate = ApiHr::getPayrollConfigTemplate();
                $SSO_TemplateID = $CfgTempate->getSsoTemplateID();          //ประกันสังคม
                $IncomeTAX_TemplateID = $CfgTempate->getTaxIncomeTemplateID();  //ภาษีบุคคลธรรมดา 90,91
                $PND1_TemplateID = $CfgTempate->getTaxPndTemplateID();         //ภาษี ภงด.1
                $WhdTax_TemplateID = $CfgTempate->getTaxTaviTemplateID();       //ภาษีหัก ณ ที่จ่าย
                $Guarantee_TemplateID = $CfgTempate->getGuaranteeTemplateID();  //เงินค้ำประกัน


                /** UPDATE ADD_DEDUCT_THISMONTH : Line by Line Only line was change */
                foreach ($arrAddDeductList as $ADD_DEDUCT_DETAIL_ID => $ADD_DEDUCT_DETAIL_AMOUNT) {
                    $this_month = Adddeductthismonth::findOne($ADD_DEDUCT_DETAIL_ID);
                    $old_obj = clone $this_month;

                    $AMT = Utility::removeCommar($ADD_DEDUCT_DETAIL_AMOUNT);
                    if ($AMT == 0 || $AMT < 0) {
                        $this_month->delete();
                    } else {
                        if ($old_obj->ADD_DEDUCT_DETAIL_AMOUNT != $AMT) {
                            $this_month->ADD_DEDUCT_DETAIL_AMOUNT = $AMT;
                            $this_month->ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
                            $this_month->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
                            $this_month->update(false);
                        } else if (($this_month->ADD_DEDUCT_ID == $SSO_TemplateID) && ($new_SSO != $AMT)) { //update ประกันสังคม
                            $this_month->ADD_DEDUCT_DETAIL_AMOUNT = $new_SSO;
                            $this_month->ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
                            $this_month->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
                            $this_month->update(false);
                        }
                    }
                }

                //calculate sso / IF user delete,  force INSERT NEW one
                   if($wage_salary >0 )
                    {
                        $ObjSSO = Adddeductthismonth::find()->where([
                            'ADD_DEDUCT_ID' => $SSO_TemplateID,
                            'ADD_DEDUCT_DETAIL_EMP_ID' => $recal_idcard,
                            'ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date,
                        ])->one();

                        if((!$ObjSSO) && ($force_sso==1)) {

                            $SSO_template = $AddDeductTemplate[$SSO_TemplateID];
                            ApiSalary::RecalInsertThisMonth($pay_date,$SSO_template,$recal_idcard,$new_SSO);
                        }


                    }


                $data_adddeduct = ApiSalary::loadall_recal_thismonth($recal_idcard);
                $arrAddDeDuctEmp = ApiSalary::buildAddDeductEmployee($data_adddeduct);
                $arrAddDuctSummary = ApiSalary::buildAddDeductSummary($data_adddeduct);

                $arrMoneyAddPND = ApiSalary::buildAddMoneyPND($arrAddDeDuctEmp);

                $new_total_add = (count($arrAddDuctSummary[$recal_idcard][1])) ? array_sum($arrAddDuctSummary[$recal_idcard][1]) : 0;
                $new_total_deduct = (count($arrAddDuctSummary[$recal_idcard][2])) ? array_sum($arrAddDuctSummary[$recal_idcard][2]) : 0;

                $new_subtotal_deduct = ($new_total_deduct + $new_SSO);
                $new_subtotal_income = ($new_salary + $new_total_add);
                $new_subtotal_net = ($new_subtotal_income - $new_subtotal_deduct);


                ApiSalary::resetWitholdingTax($pay_date,$arrRecalIDCard);
                ApiSalary::resetPNDTax($pay_date,$arrRecalIDCard);
                ApiSalary::resetBenefitFund($pay_date,$arrRecalIDCard);

                $WHT_template = $AddDeductTemplate[$WhdTax_TemplateID];
                $WHT_Record = ApiWithodingTax::CalWitholdingTax($connection,$pay_date,$arrAddDeDuctEmp,$ACTION,$WHT_template,$AddDeductTemplate);


                $arrEmpSalaryNormal = ApiSalary::getEmployeeSalary($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Active */
                $arrEmpSalaryStartBetweenMonth = ApiSalary::getEmployeeSalaryStartBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Start work between month, calculate by day */
                $arrEmpSalaryOffBetweenMonth = ApiSalary::getEmployeeSalaryOffBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Out between month, calculate by day */
                $arrEmpSalaryOffThisMonth = ApiSalary::getEmployeeSalaryOffThisMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Out between month, calculate by day */
                $arrSalaryAll = array_merge($arrEmpSalaryNormal, $arrEmpSalaryStartBetweenMonth, $arrEmpSalaryOffBetweenMonth, $arrEmpSalaryOffThisMonth);


                /** ---------------------------------------------------------------------------
                 *  คำนวณ ภาษีเงินได้บุคคลธรรมดา / step การคำนวน
                 *  เพิ่มข้อมูลลงใน ADD_DEDUCT_THIS_MONTH / tax_calculate / tax_calculate_step
                 *----------------------------------------------------------------------------*/
                $PND1_Template = $AddDeductTemplate[$PND1_TemplateID];
                $PND_Record = ApiPNDTax::TaxPNDInCome($connection,$pay_date,$arrSalaryAll,$ACTION,$PND1_Template,$arrMoneyAddPND);

                /** ---------------------------------------------------------------------------
                 *  คำนวณ รายการจ่ายรวมโดย วนลูป array แล้วปรับปรุงข้อมูลตาม index, ID Card
                 *  Loop through $arrSalaryAll
                 *----------------------------------------------------------------------------*/
                $arrAllStaffWage = [];
                foreach ($arrSalaryAll as $item) {

                    //Apply WHT and Recal summary
                    $_wht_amt = 0;
                    if(isset($WHT_Record[$item['ID_Card']])) {
                        $_wht_amt = $WHT_Record[$item['ID_Card']]['total_wht'];

                        $WHT_ThisMonth = Adddeductthismonth::find()
                            ->where([
                                'ADD_DEDUCT_DETAIL_EMP_ID' => $recal_idcard,
                                'ADD_DEDUCT_ID' => $WhdTax_TemplateID,
                            ])
                            ->one();
                        if($WHT_ThisMonth) {
                            $WHT_ThisMonth->ADD_DEDUCT_DETAIL_AMOUNT = $_wht_amt;
                            $WHT_ThisMonth->ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
                            $WHT_ThisMonth->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
                            $WHT_ThisMonth->update(false);
                        }
                        else {
                            if($force_wht==1) {  //FORCE INSERT NEW RECORD WHT
                                ApiSalary::RecalInsertThisMonth($pay_date,$WHT_template,$recal_idcard,$_wht_amt);
                            }
                        }
                    }

                    //Apply PND and Recal summary
                    $_pnd_amt = 0;
                    if(isset($PND_Record[$item['ID_Card']])) {
                        $_pnd_amt = $PND_Record[$item['ID_Card']];

                        $WHT_ThisMonth = Adddeductthismonth::find()
                            ->where([
                                'ADD_DEDUCT_DETAIL_EMP_ID' => $recal_idcard,
                                'ADD_DEDUCT_ID' => $PND1_TemplateID,
                            ])
                            ->one();
                        if($WHT_ThisMonth) {
                            $WHT_ThisMonth->ADD_DEDUCT_DETAIL_AMOUNT = $_pnd_amt;
                            $WHT_ThisMonth->ADD_DEDUCT_DETAIL_UPDATE_BY = $this->idcardLogin;
                            $WHT_ThisMonth->ADD_DEDUCT_DETAIL_UPDATE_DATE = new Expression('NOW()');
                            $WHT_ThisMonth->update(false);
                        }
                        else {
                            if($force_pnd==1) {  //FORCE INSERT NEW RECORD PND
                                ApiSalary::RecalInsertThisMonth($pay_date,$PND1_Template,$recal_idcard,$_pnd_amt);
                            }
                        }
                    }

                    //WHT + PND Tax
                    $new_deduct_tax = ($_wht_amt+$_pnd_amt);

                    $item['EMP_SALARY_WAGE'] = $new_salary;
                    $item['socialBenifitPercent'] = $socialBenifitPercent;
                    $item['socialBenefitStatus'] = $socialBenefitStatus;
                    $item['benefitFundPercent'] = $benefitFundPercent;
                    $item['benifitFundStatus'] = $benifitFundStatus;
                    $item['cal_wht_amount'] = $_wht_amt;
                    $item['cal_pnd_amount'] = $_pnd_amt;
                    $item['cal_bnf_amount'] = $new_BNF;
                    $item['cal_sso_amount'] = $new_SSO;
                    $item['cal_add_money']      = $new_total_add;
                    $item['cal_deduct_money']   = ($new_subtotal_deduct + $new_deduct_tax);
                    $item['cal_net_amount']     = ($new_subtotal_net - $new_deduct_tax);
                    $item['cal_total_income']   = $new_subtotal_income;
                    $arrAllStaffWage[$item['ID_Card']] = $item;
                }

                //Recal BNF
                ApiSalary::buildBenefitFund($connection,$pay_date,$arrAllStaffWage);

                $WageThisMonth = Wagethismonth::findOne($wage_id);
                $WageThisMonth->WAGE_SALARY = $arrAllStaffWage[$recal_idcard]['EMP_SALARY_WAGE'];
                $WageThisMonth->WAGE_TOTAL_ADDITION = $arrAllStaffWage[$recal_idcard]['cal_add_money'];
                $WageThisMonth->WAGE_EARN_PLUS_ADD = $arrAllStaffWage[$recal_idcard]['cal_total_income'];
                $WageThisMonth->WAGE_TOTAL_DEDUCTION = $arrAllStaffWage[$recal_idcard]['cal_deduct_money'];

                $WageThisMonth->WAGE_EARN_MINUS_DEDUCT = $arrAllStaffWage[$recal_idcard]['cal_net_amount'];
                $WageThisMonth->WAGE_NET_SALARY = $arrAllStaffWage[$recal_idcard]['cal_net_amount'];

                $WageThisMonth->WTH_AMOUNT = (isset($WHT_Record[$recal_idcard])) ? $WHT_Record[$recal_idcard]['total_deduct'] : 0 ;
                $WageThisMonth->WTH_AMOUNT_TAX = (isset($WHT_Record[$recal_idcard])) ? $WHT_Record[$recal_idcard]['total_wht'] : 0 ;

                $WageThisMonth->BNF_AMOUNT = $new_BNF;
                $WageThisMonth->PND_TOTAL_INCOME = $arrAllStaffWage[$recal_idcard]['cal_total_income'];
                $WageThisMonth->PND_AMOUNT_REDUCE = 0;
                $WageThisMonth->PND_AMOUNT_TAX = $arrAllStaffWage[$recal_idcard]['cal_pnd_amount'];

                $WageThisMonth->SSO_AMOUNT = $new_SSO;
                $WageThisMonth->update(false);


                $transaction->commit();
                return 1;
            }
            catch (ErrorException $e) {
                $transaction->rollBack();
                throw new \Exception($e->getMessage());
            }


        }
    }


    /** function AdjustSalary when user Validate or Approved Salary call here, by own ID Card */
    public function AdjustSalary($postValue)
    {

    }

    public function actionTestaa()
    {

        $ACTION = [];
        $ACTION['recal_idcard'] = ['1570500134506'];  //array id card if mode=recal
        $ACTION['mode'] = 'recal';//(isset($postValue['mode']) && $postValue['mode'] === 'recal') ? 'recal' : 'normal';

        $pay_date = '06-2018'; //($postValue['pay_date']) ? $postValue['pay_date'] : date('m-Y');
        if ($pay_date == '') {
            throw new \Exception('ERROR : Not found date');
        }

        /** ---------------------------------------------------------------------------
         *  โหลด ข้อมูล Template / master
         *----------------------------------------------------------------------------*/
        $arrAddDeductTemplate = ApiPayroll::getArrayAdddeductTemplate();

        //** Load Auto Calculate Template ID */
        $arrAutoTemplateID = [];
        $CfgTempate = ApiHr::getPayrollConfigTemplate();

        /** ---------------------------------------------------------------------------
         *  โหลด config auto จาก  user กำหนด
         *----------------------------------------------------------------------------*/
        $SSO_TemplateID = $CfgTempate->getSsoTemplateID();          //ประกันสังคม
        $IncomeTAX_TemplateID = $CfgTempate->getTaxIncomeTemplateID();  //ภาษีบุคคลธรรมดา 90,91
        $PND1_TemplateID = $CfgTempate->getTaxPndTemplateID();         //ภาษี ภงด.1
        $WhdTax_TemplateID = $CfgTempate->getTaxTaviTemplateID();       //ภาษีหัก ณ ที่จ่าย
        $Guarantee_TemplateID = $CfgTempate->getGuaranteeTemplateID();  //เงินค้ำประกัน

        $arrAutoTemplateID[0] = $SSO_TemplateID;
        $arrAutoTemplateID[1] = $IncomeTAX_TemplateID;
        $arrAutoTemplateID[2] = $PND1_TemplateID;
        $arrAutoTemplateID[3] = $WhdTax_TemplateID;



        /** Query date config : หาค่าวันเวลา เริ่มต้น สิ้นสุดรอบcreatesalaryเงินเดือน */
        $arrConfig = ApiSalary::buildSalaryDateCondition($pay_date);


        /** ---------------------------------------------------------------------------
         *  โหลดข้อมูล จาก add_deduct_detail จากรายการได้ที่สร้างไว้
         *  แปลงเป็น array
         *----------------------------------------------------------------------------*/
        $data_adddeduct = ApiSalary::loadall_adddeduct($pay_date,$ACTION);
        $arrAddDeDuctEmp = ApiSalary::buildAddDeductEmployee($data_adddeduct);
        $arrAddDuctSummary = ApiSalary::buildAddDeductSummary($data_adddeduct);

        /** ---------------------------------------------------------------------------
         *  โหลดข้อมูล จาก $data_adddeduct หารายได้ที่ไม่ใช่ 40(2) 40(8)
         *  แปลงเป็น array
         *----------------------------------------------------------------------------*/
        $arrMoneyAddPND = ApiSalary::buildAddMoneyPND($arrAddDeDuctEmp);



        /** ------------------------------------------------------------------------------------------------------------
         *  โหลดข้อมูลพนักงาน ทั้งหมด มี 4 เคส
         *  1.พนักงานปกติ 2.พนักงานเริ่มงานระหว่างเดือน 3.พนักงานลาออกระหว่างเดือน 4.พนักงานลาออก ณ สิ้นรอบเงินเดือน
         *  เอาทั้งหมดมา merge array ด้วยกัน
         *-------------------------------------------------------------------------------------------------------------*/
        $arrEmpSalaryNormal = ApiSalary::getEmployeeSalary($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Active */
        $arrEmpSalaryStartBetweenMonth = ApiSalary::getEmployeeSalaryStartBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Start work between month, calculate by day */
        $arrEmpSalaryOffBetweenMonth = ApiSalary::getEmployeeSalaryOffBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Out between month, calculate by day */
        $arrEmpSalaryOffThisMonth = ApiSalary::getEmployeeSalaryOffThisMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Out between month, calculate by day */
        $arrSalaryAll = array_merge($arrEmpSalaryNormal, $arrEmpSalaryStartBetweenMonth, $arrEmpSalaryOffBetweenMonth, $arrEmpSalaryOffThisMonth);


        echo '<pre>';
        print_r($arrEmpSalaryOffBetweenMonth);
        echo '</pre>';

        exit;
        //$arrAddDuctSummary = ApiSalary::buildAddDeductSummary($data_adddeduct);
    }


    /**
     *  CREATE SALARY Add/Edit 4 DEC 2017, by AEEDY
     */
    public function actionCreatesalary()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
//            echo 'postValue==>';
//            echo '<pre>';
//            print_r($postValue);
//            echo '<pre>';
            echo $this->CalculateSalary(false,$postValue);
        }
    }

    public function CalculateSalary($isAdjust=false,$postValue=null)
    {

        /** ---------------------------------------------------------------------------
         *  ประกาศ transaction ใช้ได้เฉพาะ type InnoDB เท่านั้น
         *----------------------------------------------------------------------------*/


        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;

        $transaction = $connection->beginTransaction();
        try {


            $ACTION = [];
            $ACTION['recal_idcard'] = null;  //array id card if mode=recal
            $ACTION['mode'] = (isset($postValue['mode']) && $postValue['mode'] === 'recal') ? 'recal' : 'normal';

            $pay_date = ($postValue['pay_date']) ? $postValue['pay_date'] : date('m-Y');
            if ($pay_date == '') {
                throw new \Exception('ERROR : Not found date');
            }


            //update all add_deduct_detail for PRE-PROCESS
            Adddeductdetail::updateAll(['ADD_DEDUCT_DETAIL_STATUS' => 2], ['ADD_DEDUCT_DETAIL_STATUS' => 1, 'ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date]);
            Adddeductdetail::updateAll(['ADD_DEDUCT_DETAIL_STATUS' => 99], ['ADD_DEDUCT_ID' => [34,41,43,54], 'ADD_DEDUCT_DETAIL_PAY_DATE' => $pay_date]);



            /** ---------------------------------------------------------------------------
             *  โหลด ข้อมูล Template / master
             *----------------------------------------------------------------------------*/
            $arrAddDeductTemplate = ApiPayroll::getArrayAdddeductTemplate();

            //** Load Auto Calculate Template ID */
            $arrAutoTemplateID = [];
            $CfgTempate = ApiHr::getPayrollConfigTemplate();

            /** ---------------------------------------------------------------------------
             *  โหลด config auto จาก  user กำหนด
             *----------------------------------------------------------------------------*/
            $SSO_TemplateID = $CfgTempate->getSsoTemplateID();          //ประกันสังคม
            $IncomeTAX_TemplateID = $CfgTempate->getTaxIncomeTemplateID();  //ภาษีบุคคลธรรมดา 90,91
            $PND1_TemplateID = $CfgTempate->getTaxPndTemplateID();         //ภาษี ภงด.1
            $WhdTax_TemplateID = $CfgTempate->getTaxTaviTemplateID();       //ภาษีหัก ณ ที่จ่าย
            $Guarantee_TemplateID = $CfgTempate->getGuaranteeTemplateID();  //เงินค้ำประกัน


            $arrAutoTemplateID[0] = $SSO_TemplateID;
            $arrAutoTemplateID[1] = $IncomeTAX_TemplateID;
            $arrAutoTemplateID[2] = $PND1_TemplateID;
            $arrAutoTemplateID[3] = $WhdTax_TemplateID;


            /** ---------------------------------------------------------------------------
             *  โหมด Recal // IF MODE = RECAL ** reset salary ***
             *  โดยประเภทรายได้เดียวกันให้จับมัดรวมกัน
             *----------------------------------------------------------------------------*/
            if ($ACTION['mode'] === 'recal') {
                $_arrReCalIDCard = (!is_array($postValue['id_emp'])) ? array($postValue['id_emp']) : $postValue['id_emp'];
                $arrRecalEmpIDCard = array_unique($_arrReCalIDCard);
                $recal_record_total = ApiSalary::resetSalary($pay_date,$arrRecalEmpIDCard,$arrAutoTemplateID,$connection);
                $ACTION['recal_idcard'] = $arrRecalEmpIDCard;
            }



            /** Query date config : หาค่าวันเวลา เริ่มต้น สิ้นสุดรอบเงินเดือน */
            $arrConfig = ApiSalary::buildSalaryDateCondition($pay_date);


            /** ---------------------------------------------------------------------------
             *  โหลดข้อมูล จาก add_deduct_detail จากรายการได้ที่สร้างไว้
             *  แปลงเป็น array
             *----------------------------------------------------------------------------*/
            $data_adddeduct = ApiSalary::loadall_adddeduct($pay_date,$ACTION);
            $arrAddDeDuctEmp = ApiSalary::buildAddDeductEmployee($data_adddeduct);
            $arrAddDuctSummary = ApiSalary::buildAddDeductSummary($data_adddeduct);


            $output = print_r($arrAddDuctSummary, true);
            file_put_contents('upload/add_deduct_summary.txt', $output);

            /** ---------------------------------------------------------------------------
             *  โหลดข้อมูล จาก $data_adddeduct หารายได้ที่ไม่ใช่ 40(2) 40(8)
             *  แปลงเป็น array
             *----------------------------------------------------------------------------*/
            $arrMoneyAddPND = ApiSalary::buildAddMoneyPND($arrAddDeDuctEmp);


            /** ---------------------------------------------------------------------------
             *  คำนวณ ภงด 3 / หัก ณ ที่จ่าย / ทวิ 50
             *  เพิ่มข้อมูลลงใน ADD_DEDUCT_THIS_MONTH
             *----------------------------------------------------------------------------*/
            $WHT_template = $arrAddDeductTemplate[$WhdTax_TemplateID];
            $WHT_Record = ApiWithodingTax::CalWitholdingTax($connection,$pay_date,$arrAddDeDuctEmp,$ACTION,$WHT_template,$arrAddDeductTemplate);


            /** ------------------------------------------------------------------------------------------------------------
             *  โหลดข้อมูลพนักงาน ทั้งหมด มี 4 เคส
             *  1.พนักงานปกติ 2.พนักงานเริ่มงานระหว่างเดือน 3.พนักงานลาออกระหว่างเดือน 4.พนักงานลาออก ณ สิ้นรอบเงินเดือน
             *  เอาทั้งหมดมา merge array ด้วยกัน
             *-------------------------------------------------------------------------------------------------------------*/
            $arrEmpSalaryNormal = ApiSalary::getEmployeeSalary($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Active */
            $arrEmpSalaryStartBetweenMonth = ApiSalary::getEmployeeSalaryStartBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Start work between month, calculate by day */
            $arrEmpSalaryOffBetweenMonth = ApiSalary::getEmployeeSalaryOffBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Out between month, calculate by day */
            $arrEmpSalaryOffThisMonth = ApiSalary::getEmployeeSalaryOffThisMonth($arrConfig, $arrAddDuctSummary, $ACTION); /*  Find Employee Salaray Out between month, calculate by day */
            $arrSalaryAll = array_merge($arrEmpSalaryNormal, $arrEmpSalaryStartBetweenMonth, $arrEmpSalaryOffBetweenMonth, $arrEmpSalaryOffThisMonth);


            
            $output = print_r($arrEmpSalaryStartBetweenMonth, true);
            file_put_contents('upload/output.txt', $output);

            /** ---------------------------------------------------------------------------
             *  คำนวณ เงินสะสมพนักงาน / ทำสรุปยอดแต่ละเดือน
             *  เพิ่มข้อมูลลงใน BENEFIT_FUND
             *----------------------------------------------------------------------------*/
            ApiSalary::buildBenefitFund($connection,$pay_date,$arrSalaryAll);


            /** ---------------------------------------------------------------------------
             *  คำนวณ เงินประกันสังคม / ทำสรุปยอดแต่ละเดือน
             *  เพิ่มข้อมูลลงใน ADD_DEDUCT_THIS_MONTH
             *----------------------------------------------------------------------------*/
            $SSO_template = $arrAddDeductTemplate[$SSO_TemplateID];
            $SSO_Record = ApiSalary::buildSSOdata($connection,$pay_date,$arrSalaryAll,$SSO_template);


            /** ---------------------------------------------------------------------------
             *  คำนวณ ภาษีเงินได้บุคคลธรรมดา / step การคำนวน
             *  เพิ่มข้อมูลลงใน ADD_DEDUCT_THIS_MONTH / tax_calculate / tax_calculate_step
             *----------------------------------------------------------------------------*/
            $PND1_Template = $arrAddDeductTemplate[$PND1_TemplateID];
            $PND_Record = ApiPNDTax::TaxPNDInCome($connection,$pay_date,$arrSalaryAll,$ACTION,$PND1_Template,$arrMoneyAddPND);


            /** ---------------------------------------------------------------------------
             *  คำนวณ รายการจ่ายรวมโดย วนลูป array แล้วปรับปรุงข้อมูลตาม index, ID Card
             *  Loop through $arrSalaryAll
             *----------------------------------------------------------------------------*/
            $arrAllStaffWage = [];
            foreach ($arrSalaryAll as $item) {

                $_old_cal_deduct_money  = $item['cal_deduct_money'];
                $_old_cal_net_amount    = $item['cal_net_amount'];
                $_old_cal_total_income  = $item['cal_total_income'];

                //Apply WHT and Recal summary
                $_wht_amt = 0;
                if(isset($WHT_Record[$item['ID_Card']])) {
                    $_wht_amt = $WHT_Record[$item['ID_Card']]['total_wht'];
                    $item['cal_wht_amount'] = $_wht_amt;
                }

                //Apply PND and Recal summary
                $_pnd_amt = 0;
                if(isset($PND_Record[$item['ID_Card']])) {
                    $_pnd_amt = $PND_Record[$item['ID_Card']];
                    $item['cal_pnd_amount'] = $_pnd_amt;
                }

                $item['cal_deduct_money'] = ($_old_cal_deduct_money+$_wht_amt+$_pnd_amt);
                $item['cal_net_amount'] = $_old_cal_net_amount-($_wht_amt+$_pnd_amt);
                $item['cal_total_income'] = $_old_cal_total_income - ($_wht_amt+$_pnd_amt);
                $arrAllStaffWage[$item['ID_Card']] = $item;

            }


            $output = print_r($arrAllStaffWage, true);
            file_put_contents('upload/output2.txt', $output);
            //exit;

            /** ---------------------------------------------------------------------------
             *  สรุปรายการทั้งหมดลงตาราง
             *  เพิ่มข้อมูลลงใน WAGE_THIS_MONTH / summary_money_wage
             *----------------------------------------------------------------------------*/
            $totalrec = ApiSalary::buildInsertThisMonth($connection,$arrAllStaffWage,$data_adddeduct);
            ApiSalary::buildWageSummary($connection,$pay_date,$arrAllStaffWage);

            $transaction->commit();
            return $totalrec;
        }
        catch (ErrorException $e) {
            $transaction->rollBack();
            throw new \Exception($e->getMessage());
        }
    }


    public function actionTest()
    {


        $ACTION = [];
        $ACTION['recal_idcard'] = null;  //array id card if mode=recal
        $ACTION['mode'] = (isset($postValue['mode']) && $postValue['mode'] === 'recal') ? 'recal' : 'normal';

        $pay_date = date('m-Y');
        if ($pay_date == '') {
            throw new \Exception('ERROR : Not found date');
        }

        //Load Add deduct Template
        $arrAddDeductTemplate = ApiPayroll::getArrayAdddeductTemplate();



        $arrAutoTemplateID = [];
        $CfgTempate = ApiHr::getPayrollConfigTemplate();

        $SSO_TemplateID = $CfgTempate->getSsoTemplateID();          //ประกันสังคม
        $IncomeTAX_TemplateID = $CfgTempate->getTaxIncomeTemplateID();  //ภาษีบุคคลธรรมดา 90,91
        $PND1_TemplateID = $CfgTempate->getTaxPndTemplateID();         //ภาษี ภงด.1
        $WhdTax_TemplateID = $CfgTempate->getTaxTaviTemplateID();       //ภาษีหัก ณ ที่จ่าย
        $Guarantee_TemplateID = $CfgTempate->getGuaranteeTemplateID();  //เงินค้ำประกัน


        $arrAutoTemplateID[0] = $SSO_TemplateID;
        $arrAutoTemplateID[1] = $IncomeTAX_TemplateID;
        $arrAutoTemplateID[2] = $PND1_TemplateID;
        $arrAutoTemplateID[3] = $WhdTax_TemplateID;

        // IF MODE = RECAL ** reset salary ***
        if ($ACTION['mode'] === 'recal') {

            $_arrReCalIDCard = (!is_array($postValue['id_emp'])) ? array($postValue['id_emp']) : $postValue['id_emp'];

            $arrRecalEmpIDCard = array_unique($_arrReCalIDCard);
            $recal_record_total = ApiSalary::resetSalary($pay_date,$arrRecalEmpIDCard,$arrAutoTemplateID);
            $ACTION['recal_idcard'] = $arrRecalEmpIDCard;
        }


        //Query date config
        $arrConfig = ApiSalary::buildSalaryDateCondition($pay_date);

        //Load add deduct all
        $data_adddeduct = ApiSalary::loadall_adddeduct($pay_date);
        $arrAddDeDuctEmp = ApiSalary::buildAddDeductEmployee($data_adddeduct);
        $arrAddDuctSummary = ApiSalary::buildAddDeductSummary($data_adddeduct);



       // $a = ApiSalary::getAllEmpSalaryInfo();
        $arrEmpSalaryNormal = ApiSalary::getEmployeeSalary($arrConfig, $arrAddDuctSummary, $ACTION);
        $arrEmpSalaryStartBetweenMonth = ApiSalary::getEmployeeSalaryStartBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION);
        $arrEmpSalaryOffBetweenMonth = ApiSalary::getEmployeeSalaryOffBetweenMonth($arrConfig, $arrAddDuctSummary, $ACTION);
        $arrEmpSalaryOffThisMonth = ApiSalary::getEmployeeSalaryOffThisMonth($arrConfig, $arrAddDuctSummary, $ACTION);

        $arrSalaryAll = array_merge($arrEmpSalaryNormal, $arrEmpSalaryStartBetweenMonth, $arrEmpSalaryOffBetweenMonth, $arrEmpSalaryOffThisMonth);



        //echo '<pre>';
        //print_r($arrSalaryAll);
        //echo '</pre>';

        //Find Witholding Tax
        $arrStaffWHT = [
            '3560500575223'=>250,
            '1100900447050'=>90,
            '3401600666494'=>60,
        ];

        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        ApiPNDTax::TaxPNDInCome($connection,$pay_date,$arrSalaryAll,$ACTION,$PND1_TemplateID);

        exit;
        //Find PND Tax
        $arrStaffPND = [
            '3560500575223'=>700,
            '1100900447050'=>550,
            '3401600666494'=>450,
        ];



        $arrAllStaffWage = [];
        foreach ($arrSalaryAll as $item) {
            $_add_money     = $item['cal_add_money'];
            $_total_income  = $item['EMP_SALARY_WAGE'];
            $_grt_amount    = $item['cal_grt_amount'];
            $_sso_amount    = $item['cal_sso_amount'];
            $_total_deduct = $_grt_amount + $_sso_amount;

            //Apply WHT and Recal summary
            if(isset($arrStaffWHT[$item['ID_Card']])) {
                $_wht_amt = $arrStaffWHT[$item['ID_Card']];
                $item['cal_wht_amount'] = $_wht_amt;
                $_total_deduct = $_total_deduct + $_wht_amt;
            }

            //Apply PND and Recal summary
            if(isset($arrStaffPND[$item['ID_Card']])) {
                $_pnd_amt = $arrStaffPND[$item['ID_Card']];
                $item['cal_pnd_amount'] = $_pnd_amt;
                $_total_deduct = $_total_deduct + $_pnd_amt;
            }

            $_net_income = ($_total_income + $_add_money) - $_total_deduct;
            $item['cal_deduct_money'] = $_total_deduct;
            $item['cal_net_amount'] = $_net_income;
            $item['cal_total_income'] = ($_total_income + $_add_money);
            $arrAllStaffWage[$item['ID_Card']] = $item;
        }


        //$arrAllStaffWage['3560500575223']['cal_wht_amount'] = 100;
        //$arrAllStaffWage['3560500575223']['cal_pnd_amount'] = 200;

        echo '<pre>';
        print_r($arrAllStaffWage);
        echo '</pre>';



        //test calculate PND


    }



    public function actionHrdeletesalary()
    {
        $postValue = Yii::$app->request->post();

        //$pay_date = date('m-Y');
        $pay_date = ($postValue['pay_date']) ? $postValue['pay_date'] : date('m-Y');
        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {


            //TRUNCATE TABLE WAGE THIS MONTH
            $connection->createCommand()->truncateTable('ADD_DEDUCT_THIS_MONTH')->execute();
            $connection->createCommand()->truncateTable('WAGE_THIS_MONTH')->execute();


            $arrAutoTemplateID = [];
            $CfgTempate = ApiHr::getPayrollConfigTemplate();
            $SSO_TemplateID = $CfgTempate->getSsoTemplateID();          //ประกันสังคม
            $IncomeTAX_TemplateID = $CfgTempate->getTaxIncomeTemplateID();  //ภาษีบุคคลธรรมดา 90,91
            $PND1_TemplateID = $CfgTempate->getTaxPndTemplateID();         //ภาษี ภงด.1
            $WhdTax_TemplateID = $CfgTempate->getTaxTaviTemplateID();       //ภาษีหัก ณ ที่จ่าย
            $Guarantee_TemplateID = $CfgTempate->getGuaranteeTemplateID();  //เงินค้ำประกัน

            $arrAutoTemplateID[0] = $SSO_TemplateID;
            $arrAutoTemplateID[1] = $IncomeTAX_TemplateID;
            $arrAutoTemplateID[2] = $PND1_TemplateID;
            $arrAutoTemplateID[3] = $WhdTax_TemplateID;


           // print_r($pay_date);

            ApiSalary::resetSummaryWage($pay_date,[],true);
            ApiSalary::resetBenefitFund($pay_date,[],true);
            ApiSalary::resetWitholdingTax($pay_date,[],true);
            ApiSalary::resetPNDTax($pay_date,[],true);
            ApiSalary::Header_Record($pay_date);
            ApiSalary::Detail_Record($pay_date);


            $transaction->commit();
            return 1;
        }
         catch (ErrorException $e) {
             $transaction->rollBack();
             return 0;
         }
    }


    public function actionBuindingsalary()
    {

        /** $arrSalaryCalType
         *  ประเภทการคำนวณเงินเดือนและภาษี
         *  1=> รายสัปดาห์ (7) วัน  ตัวคูณภาษี 52
         *  2=> ราย week (15) วัน ตัวคูณภาษี 24
         *  3=> รายเดือน (30) วัน ตัวคูณภาษี 12
         **/
        $_wage_type = 3;//pass params from user

        $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();
        try {
            $postValue = Yii::$app->request->post();

            //$datemakesalary formate mm-YYYY;

            $id_cradmakesalary = $postValue['id_cradmaker'];
            $datemakesalary = "26-" . $postValue['datemakesalary'];

            $dateFormateNew = DateTime::convertdmyToymd($datemakesalary);

            $modelListEmpSalaryNormal = ApiSalary::listDataSalaryEmpNormal($dateFormateNew); //พนักงานปกติ
            $modelSocialNormal = ApiSalary::CalculateSSO($modelListEmpSalaryNormal, $postValue['datemakesalary']);
            $modelBenefit = ApiBenefit::CalculateBenefit($modelListEmpSalaryNormal, $postValue['datemakesalary']);

            $modelCalculateSalary = ApiSalary::CalculateSalary($modelListEmpSalaryNormal, $postValue['datemakesalary']);


            //  คำนวณภาษี  ////
            //$modelTax = ApiTax::TaxInCome($datemakesalary,$_wage_type);


            // print_r($modelListEmpSalaryNormal);

            $transaction->commit();
        } catch (ErrorException $e) {
            $transaction->rollBack();
            print_r($e->getMessage());
            //  echo "xx";
            // Yii::warning("Division by zero.");
            //  throw new \Exception('ERROR');

        }


    }


    public function actionCheckmakesalary()
    {
        if (Yii::$app->request->isAjax) {
            $_pay_date = Yii::$app->request->get('pay_date');
            $date_pay = ($_pay_date !='' && $_pay_date != null) ? $_pay_date : date('m-Y');
            $model = ApiSalary::GetlistWagesSalary($date_pay);
            return $model;
        }
    }


    public function actionSeachempcheckexpense()
    {
        $postValue = Yii::$app->request->post();

        $id_company = $postValue['selectworking'];
        $id_department = $postValue['selectdepartment'];
        $id_section = $postValue['selectsection'];

        $dataEmp = ApiPayroll::seachEmpforDeductions($id_company, $id_department, $id_section);
        // echo "<pre>";
        // print_r($dataEmp);
        // exit();
        return $this->render('checkexpense', ['dataEmp' => $dataEmp,
            'selectworking' => $id_company,
            'selectdepartment' => $id_department,
            'selectsection' => $id_section,
            'query' => true]);

    }

    public function actionSeachempcheckexpensehistory()
    {
        $postValue = Yii::$app->request->post();

        $id_company = $postValue['selectworking'];
        $id_department = $postValue['selectdepartment'];
        $id_section = $postValue['selectsection'];
        $id_emp= $postValue['id_emp'];

        $dataEmp = ApiPayroll::seachEmpforDeductions($id_company, $id_department, $id_section,$id_emp);
        return $this->render('histexpense', [
            'dataEmp' => $dataEmp,
            'selectworking' => $id_company,
            'selectdepartment' => $id_department,
            'selectsection' => $id_section,
            'id_emp'=>$id_emp,
            'query' => true
        ]);

    }

    public function actionSeachempcheckdeducthistory()
    {
        $postValue = Yii::$app->request->post();

        $id_company = $postValue['selectworking'];
        $id_department = $postValue['selectdepartment'];
        $id_section = $postValue['selectsection'];
        $id_emp= $postValue['id_emp'];

        $dataEmp = ApiPayroll::seachEmpforDeductions($id_company, $id_department, $id_section,$id_emp);
        return $this->render('histdeduct', ['dataEmp' => $dataEmp,
            'selectworking' => $id_company,
            'selectdepartment' => $id_department,
            'selectsection' => $id_section,
            'id_emp'=>$id_emp,
            'query' => true]);

    }


    public function actionGridviewaddducuctdetail()
    {
        $getValue = Yii::$app->request->get();
        $id_card = $getValue['id_card'];

        $dataEmpPersanal = ApiHr::getSeachDataPersonalByIdcard($id_card);


        $query = Vempadddeductdetail::find()
            ->where('Add_Deductdetail_Status = "1"')
            ->andWhere(['=', 'Add_Deductdetail_Idemp', $id_card])
            ->andWhere(['<>', 'Add_Deducttemplate_Type', 2])
            ->orderBy(['Add_Deductdetail_Id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $count = $dataProvider->getTotalCount();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ]
        ]);
        return $this->render('checkexpense', [
            'dataProvider' => $dataProvider,
            'dataEmpPersanal' => $dataEmpPersanal,
        ]);

    }

    private function queryAddDeductHistory($idcard,$type)
    {
        return Adddeducthistory::find()
            ->where([
                'ADD_DEDUCT_THIS_MONTH_TYPE' => $type,
                'ADD_DEDUCT_THIS_MONTH_EMP_ID' => $idcard,
                'ADD_DEDUCT_THIS_MONTH_STATUS' => 1,
            ])->orderBy(['ADD_DEDUCT_THIS_MONTH_CREATE_DATE' => SORT_DESC]);
    }

    public function actionGridviewaddducuctdetailhistedit()
    {
        $template_type = 1;
        $getValue = Yii::$app->request->get();
        $id_card = $getValue['id_card'];

        $dataEmpPersanal = ApiHr::getSeachDataPersonalByIdcard($id_card);

        $query2 = $this->queryAddDeductHistory($id_card,$template_type);
        $dataProvider = new ActiveDataProvider([
            'query' => $query2,
        ]);


        $count = $dataProvider->getTotalCount();
        $dataProvider = new ActiveDataProvider([
            'query' => $query2,
            'pagination' => [
                'pageSize' => $count,
            ]
        ]);


        $arrEmp = ApiPayroll::getArrayEmpdata();
        return $this->render('histexpense', [
            'dataProvider' => $dataProvider,
            'dataEmpPersanal' => $dataEmpPersanal,
            'arrEmp'=>$arrEmp,
        ]);

    }


    public function actionSeachempchededuction()
    {
        $postValue = Yii::$app->request->post();

        $id_company = $postValue['selectworking'];
        $id_department = $postValue['selectdepartment'];
        $id_section = $postValue['selectsection'];

        $dataEmp = ApiPayroll::seachEmpforDeductions($id_company, $id_department, $id_section);
        return $this->render('checkdeduct', ['dataEmp' => $dataEmp,
            'selectworking' => $id_company,
            'selectdepartment' => $id_department,
            'selectsection' => $id_section,
            'query' => true]);

    }


    public function actionGridviewminusducuctdetail()
    {
        $getValue = Yii::$app->request->get();
        $id_card = $getValue['id_card'];

        $dataEmpPersanal = ApiHr::getSeachDataPersonalByIdcard($id_card);


        $query = Vempadddeductdetail::find()
            ->where('Add_Deductdetail_Status = 1')
            ->andWhere(['=', 'Add_Deductdetail_Idemp', $id_card])
            ->andWhere(['<>', 'Add_Deducttemplate_Type', 1])
            ->orderBy(['Add_Deductdetail_Id' => SORT_DESC]);;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $count = $dataProvider->getTotalCount();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ]
        ]);


        return $this->render('checkdeduct', [
            'dataProvider' => $dataProvider,
            'dataEmpPersanal' => $dataEmpPersanal,
        ]);

    }

    public function actionGridviewminusducuctdetailhistory()
    {
        $template_type = 2;
        $getValue = Yii::$app->request->get();
        $id_card = $getValue['id_card'];

        $dataEmpPersanal = ApiHr::getSeachDataPersonalByIdcard($id_card);


        $query = $this->queryAddDeductHistory($id_card,$template_type);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $count = $dataProvider->getTotalCount();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ]
        ]);


        $arrEmp = ApiPayroll::getArrayEmpdata();
        return $this->render('histdeduct', [
            'dataProvider' => $dataProvider,
            'dataEmpPersanal' => $dataEmpPersanal,
            'arrEmp'=>$arrEmp,
        ]);

    }

    public function actionSearchempdata()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();

            $selectworking = $postValue['selectworking'];
            $selectdepartment = $postValue['selectdepartment'];
            $selectsection = $postValue['selectsection'];
            $id_emp = $postValue['id_emp'];

            $arrCond = [];
            if($selectworking != '')  $arrCond['WAGE_WORKING_COMPANY'] = $selectworking;
            if($selectdepartment != '')  $arrCond['WAGE_DEPARTMENT_ID'] = $selectdepartment;
            if($selectsection != '')  $arrCond['WAGE_SECTION_ID'] = $selectsection;
            if($id_emp != '')  $arrCond['WAGE_EMP_ID'] = $id_emp;

            $data = Wagethismonth::find()
                ->select([
                    'WAGE_ID as WAGE_ID',
                    'WAGE_EMP_ID as ID_Card',
                    'CONCAT(WAGE_FIRST_NAME," ",WAGE_LAST_NAME) as Fullname',
                    'COMPANY_NAME as CompanyName',
                    'DEPARTMENT_NAME as DepartmentName',
                    'SECTION_NAME as SectionName',
                    'WAGE_POSITION_CODE as Position',
                    'WAGE_THIS_MONTH_CONFIRM as WAGE_THIS_MONTH_CONFIRM',
                    'WAGE_THIS_MONTH_EMPLOYEE_LOCK as WAGE_THIS_MONTH_EMPLOYEE_LOCK',
                    'WAGE_THIS_MONTH_DIRECTOR_LOCK as WAGE_THIS_MONTH_DIRECTOR_LOCK',
                    'WAGE_THIS_MONTH_CONFIRM_BY as WAGE_THIS_MONTH_CONFIRM_BY',
                    'WAGE_THIS_MONTH_CONFIRM_DATE as WAGE_THIS_MONTH_CONFIRM_DATE',
                    'DIRECTOR_LOCK_COMFIRM_BY as DIRECTOR_LOCK_COMFIRM_BY',
                    'DIRECTOR_LOCK_COMFIRM_DATE as DIRECTOR_LOCK_COMFIRM_DATE'
                ])
                ->where($arrCond)
                ->orderBy('CompanyName, DepartmentName, SectionName ,Fullname ASC')
                ->asArray()
                ->all();

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $data;

        }

    }

    public function actionGetadddeductbyidcard()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $id_card = $postValue['id_card'];
            $date_pay = $postValue['date_pay'];
            $model = ApiSalary::loadall_adddeductthismonth_idcard($date_pay,$id_card);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionSearchsalaryidcard()
    {
        if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get();
            $wage_id = $getValue['wage_id'];
            $model = Wagethismonth::findOne($wage_id);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionGetsummarywage()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $date_pay = $postValue['date_pay'];
            $id_card = $postValue['id_card'];
            $model = SummaryMoneyWage::find()->where([
                'emp_idcard'=>$id_card,
                'pay_date'=>$date_pay,
            ])->asArray()->one();

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }


    public function actionGetbenefitdata()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $id_card = $postValue['id_card'];
            $date_pay = $postValue['date_pay'];

            $model = Benefitfund::find()->where([
                'BENEFIT_EMP_ID'=>$id_card,
                'BENEFIT_SAVING_DATE'=>$date_pay,
            ])->asArray()->one();

            if($model !== null) {
                return $model['BENEFIT_TOTAL_AMOUNT'];
            }
            else {
                return '0.00';
            }
        }
    }

    public function actionSaveconfirmsalary()
    {
        if (Yii::$app->request->isAjax && $postValue = Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();


            $empfullname = ApiHr::getEmpConfirm($this->idcardLogin);
            $id_wage_confirm = $postValue['id_wage_confirm'];
            $page_mode = $postValue['page_mode'];
            $wage_remark = $postValue['wage_remark'];
            $model = Wagethismonth::findOne($id_wage_confirm);


            //mode validate
            if($page_mode=='validate'){
                $model->WAGE_THIS_MONTH_CONFIRM = 1;
                $model->WAGE_THIS_MONTH_EMPLOYEE_LOCK = 1;
                $model->WAGE_THIS_MONTH_CONFIRM_BY = $empfullname;
                $model->WAGE_REMARK = $wage_remark;
                $model->WAGE_THIS_MONTH_CONFIRM_DATE = new Expression('NOW()');

            }


            //mode approved
            if($page_mode=='approved'){
                $model->WAGE_THIS_MONTH_DIRECTOR_LOCK = 1;
                $model->DIRECTOR_LOCK_COMFIRM_BY = $empfullname;
                $model->WAGE_REMARK = $wage_remark;
                $model->DIRECTOR_LOCK_COMFIRM_DATE = new Expression('NOW()');
            }


            if(!$model->validate()) {
                print_r($model->errors);
            }
            else {
                return $model->save();
            }

        }
    }

    public function actionSaveconfirmapprovesalary()
    {
/*        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            // print_r($postValue);
            $id_wage_confirm = $postValue['id_wage_confirm'];
            $id_card_confirm = $postValue['id_card_confirm'];
            $selectworking = $postValue['selectworking'];
            $selectdepartment = $postValue['selectdepartment'];
            $selectsection = $postValue['selectsection'];

            $modelupdate = ApiSalary::Confirmlast($id_wage_confirm, $id_card_confirm);
            $model = ApiSalary::seachEmpAndCompanyGetlistWagesSalary($selectworking, $selectdepartment, $selectsection);

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $model;
        }*/
    }


    public function actionHistexpense()
    {
        return $this->render('histexpense');
    }

    public function actionHistdeduct()
    {
        return $this->render('histdeduct');
    }

    public function actionRemovesalary()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            if ($postValue['submitremove'] == 1) {
                $dateTime = $postValue['datetimeremove'];
                $model = ApiSalary::deleteEmpSalary($dateTime);
                return $model;
            }

        }
    }

    public function actionSavesalarypersonal()
    {
        $transaction = Yii::$app->dbERP_easyhr_PAYROLL->beginTransaction();
        try {
            if (Yii::$app->request->isAjax) {
                $postValue = Yii::$app->request->post();

                $session = Yii::$app->session;
                $USER_ID = $session->get('idcard');

                $getdatanoEmp = ApiHr::getempdataInidcard($postValue['IdCardEmp']);
                $emp_data_id = $getdatanoEmp['0']['DataNo'];
                $EMP_SALARY_GUARANTEE_MONEY = EmpMoneyBonds::find()
                    ->select('money_bonds')
                    ->where('emp_data_id = "' . $emp_data_id . '"')
                    ->asArray()
                    ->one();
                if (isset($EMP_SALARY_GUARANTEE_MONEY)) {
                    $EMP_SALARY_GUARANTEE_MONEY = $EMP_SALARY_GUARANTEE_MONEY['money_bonds'];
                } else {
                    $EMP_SALARY_GUARANTEE_MONEY = 0;
                }

                $dateStart = DateTime::convertdmyToymd($postValue['datapicker']);
                $modelSalary = new Empsalary;
                $modelSalary->EMP_SALARY_ID_CARD = $postValue['IdCardEmp'];
                $modelSalary->EMP_SALARY_POSITION_CODE = $postValue['selectposition'];
                $modelSalary->EMP_SALARY_POSITION_LEVEL = $postValue['selectlevel'];
                $modelSalary->EMP_SALARY_WORKING_COMPANY = $postValue['selectworking'];
                $modelSalary->EMP_SALARY_DEPARTMENT = $postValue['selectdepartment'];
                $modelSalary->EMP_SALARY_SECTION = $postValue['selectsection'];
                $modelSalary->EMP_SALARY_CHART = $postValue['selectsalarystepchartname'];
                $modelSalary->EMP_SALARY_STEP = $postValue['selectempsalarystep'];
                $modelSalary->EMP_SALARY_LEVEL = $postValue['selectsalarysteplevel'];
                $modelSalary->EMP_SALARY_WAGE = $postValue['selectsalarystepstartsalary'];
                $modelSalary->EMP_SALARY_CHART_ID = $postValue['salarystepstartid'];
                $modelSalary->EMP_SALARY_GUARANTEE_MONEY = $EMP_SALARY_GUARANTEE_MONEY;
                $modelSalary->status = '1';
                $modelSalary->statusCalculate = '0';
                $modelSalary->statusMainJob = '0';
                $modelSalary->dateusestart = $dateStart;
                $modelSalary->EMP_SALARY_CREATE_DATE = new Expression('NOW()');
                $modelSalary->EMP_SALARY_CREATE_BY = $USER_ID;
                $modelSalary->EMP_SALARY_UPDATE_DATE = new Expression('NOW()');
                $modelSalary->EMP_SALARY_UPDATE_BY = $USER_ID;
                if ($modelSalary->save() !== false) {
                    $getPositionName = ApiOrganize::getPositionByCodeposition($postValue['selectposition']);

                    $modelChangeSalary = new Salarychange;
                    $modelChangeSalary->SALARY_CHANGE_EMP_ID = $postValue['IdCardEmp'];
                    $modelChangeSalary->SALARY_CHANGE_POSITION_NAME = $getPositionName['Name'];
                    $modelChangeSalary->SALARY_CHANGE_POSITION_CODE = $postValue['selectposition'];
                    $modelChangeSalary->SALARY_CHANGE_DATE = $dateStart;
                    $modelChangeSalary->SALARY_CHANGE_ROUND = "1";
                    $modelChangeSalary->EVALUATION_RESULT_CHART = "";
                    $modelChangeSalary->SALARY_CHANGE_TYPE = $postValue['selectsalarychangetype'];
                    $modelChangeSalary->SALARY_CHANGE_CHART_OLD = "";
                    $modelChangeSalary->SALARY_CHANGE_LEVEL_OLD = "";
                    $modelChangeSalary->SALARY_CHANGE_STEP_OLD = "";
                    $modelChangeSalary->SALARY_CHANGE_WAGE_OLD = "";
                    $modelChangeSalary->SALARY_CHANGE_CHART_NEW = $postValue['selectsalarystepchartname'];
                    $modelChangeSalary->SALARY_CHANGE_LEVEL_NEW = $postValue['selectsalarysteplevel'];
                    $modelChangeSalary->SALARY_CHANGE_STEP_NEW = $postValue['selectempsalarystep'];
                    $modelChangeSalary->SALARY_CHANGE_ADD_STEP = "";
                    $modelChangeSalary->SALARY_CHANGE_WAGE_NEW = $postValue['selectsalarystepstartsalary'];
                    $modelChangeSalary->SALARY_CHANGE_EMP_REMARK = "";
                    $modelChangeSalary->SALARY_CHANGE_REMARK = "";
                    $modelChangeSalary->SALARY_EFFECTIVE_DATE = $dateStart;
                    $modelChangeSalary->SALARY_CHANGE_STATUS = "1";
                    $modelChangeSalary->SALARY_CHANGE_IN_SLIP = "";
                    $modelChangeSalary->SALARY_CHANGE_CREATE_DATE = new Expression('NOW()');
                    $modelChangeSalary->SALARY_CHANGE_CREATE_BY = $USER_ID;
                    $modelChangeSalary->SALARY_CHANGE_UPDATE_DATE = "";
                    $modelChangeSalary->SALARY_CHANGE_UPDATE_BY = "";
                    if ($modelChangeSalary->save() !== false) {
                        $getmodelLastid = RelationPosition::find()
                            ->select('max(id) as Lastid')
                            ->asArray()
                            ->one();
                        $modelPositionRelation = new RelationPosition;
                        $modelPositionRelation->id = $getmodelLastid['Lastid'] + 1;
                        $modelPositionRelation->position_id = $getPositionName['id'];
                        $modelPositionRelation->id_card = $postValue['IdCardEmp'];
                        $modelPositionRelation->NameUse = $getdatanoEmp['0']['Fullname'];
                        $modelPositionRelation->Note = "";
                        $modelPositionRelation->status = "1";
                        $modelPositionRelation->relation_position_Start_date = new Expression('NOW()');
                        $result = $modelPositionRelation->save();
                        if ($result !== false) {
                            $emp = ApiPayroll::getEmpdata($postValue['IdCardEmp']);
                            $org = OrganicChart::findOne($postValue['org_id']);
                            $org->Emp_id =$emp[0]['DataNo'];
                            $org->Emp_name =$emp[0]['label'];
                            $org->active_status = 1;
                            if($org->save() !== false){
                                $transaction->commit();
                                echo true;
                            }else{
                                $errors = $org->errors;
                                echo "model can't validater <pre>";
                                print_r($errors);
                                echo "</pre>";
                                $transaction->rollBack();
                            }
                        } else {
                            $errors = $modelPositionRelation->errors;
                            echo "model can't validater <pre>";
                            print_r($errors);
                            echo "</pre>";
                            $transaction->rollBack();
                        }
                    } else {
                        $errors = $modelChangeSalary->errors;
                        echo "model can't validater <pre>";
                        print_r($errors);
                        echo "</pre>";
                        $transaction->rollBack();
                    }
                } else {
                    $errors = $modelSalary->errors;
                    echo "model can't validater <pre>";
                    print_r($errors);
                    echo "</pre>";
                    $transaction->rollBack();
                }


            }

        } catch (Exception $e) {

            $transaction->rollBack();

        }


    }

    public function actionListyearhistdeduct()
    {
        if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get();


            $model = Adddeducthistory::find()
                ->select('ADD_DEDUCT_THIS_MONTH_PAY_DATE')
                ->where('ADD_DEDUCT_THIS_MONTH_TYPE = 2')
                ->andWhere('ADD_DEDUCT_THIS_MONTH_EMP_ID = "' . $getValue['idcard'] . '"')
                ->andWhere('ADD_DEDUCT_THIS_MONTH_STATUS != 99')
                ->groupBy('ADD_DEDUCT_THIS_MONTH_PAY_DATE')
                ->asArray()
                ->all();
            $arrYear = [];
            foreach ($model as $key => $value) {
                $arrYear[] = substr($value['ADD_DEDUCT_THIS_MONTH_PAY_DATE'], 3, 4);
            }

            $resultYear = array_unique($arrYear);

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $resultYear;

        }
    }


    public function actionListgetdeductpersonal()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $montselect = "%$postValue[selectmonth]-$postValue[selectyear]%";

            $model = Adddeducthistory::find()
                ->select('ADD_DEDUCT_THIS_MONTH_TMP_NAME,
                                  ADD_DEDUCT_THIS_MONTH_AMOUNT,
                                  ADD_DEDUCT_THIS_MONTH_DETAIL,
                                  ADD_DEDUCT_THIS_MONTH_DETAIL_ID')
                ->where('ADD_DEDUCT_THIS_MONTH_TYPE = 2')
                ->andWhere('ADD_DEDUCT_THIS_MONTH_EMP_ID = "' . $postValue['idcardset'] . '"')
                ->andWhere('ADD_DEDUCT_THIS_MONTH_STATUS != 99')
                ->andWhere('ADD_DEDUCT_THIS_MONTH_PAY_DATE LIKE "' . $montselect . '"')
                ->asArray()
                ->all();

//           print_r($model);
//           exit;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $model;

        }
    }

    public function actionTestaction()
    {
        /*$pay_date = '12-2017';
        $arrEmpIDCard = '3560500575223';
        $arrCircleYear = DateTime::findCircleMonth($pay_date);
        $arrWitholdingTax = ApiWithodingTax::LookupWitholdingTax($arrCircleYear, $arrEmpIDCard);
        echo '<pre>';
        print_r($arrWitholdingTax);
        echo '</pre>';*/

        //$pay_date = '01-2018';
        //$arrCirCleMonth = DateTime::findCircleMonth($pay_date);
        //echo '<pre>';
        //print_r($arrCirCleMonth);
        //echo '</pre>';


        $a = ApiPayroll::getTemplateTax();
        echo '<pre>';
        print_r($a);
        echo '</pre>';

    }

    public function actionMakesalarydaliy()
    {
        return $this->render('makesalarydaliy');
    }

    public function actionGetsalarybyidcard()
    {
        $session = Yii::$app->session;
        $session->open();  //open session
        $USER_ID = $session->get('idcard');

        $db['pl'] = DBConnect::getDBConn()['pl'];

        $sql = "SELECT SUBSTRING($db[pl].WAGE_HISTORY.WAGE_PAY_DATE,4) AS Year 
                FROM $db[pl].WAGE_HISTORY WHERE $db[pl].WAGE_HISTORY.WAGE_EMP_ID = '$USER_ID' 
                GROUP BY SUBSTRING($db[pl].WAGE_HISTORY.WAGE_PAY_DATE,4)";
        $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)
            ->queryAll();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;
    }

    public function actionSearchslippersonal()
    {

        //echo "11111";

        $postValue = Yii::$app->request->post();
        //echo "<pre>";
        // print_r($postValue);
        //exit;
        $db['pl'] = DBConnect::getDBConn()['pl'];
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $idcard = $postValue['idcard'];
        $selectDate = $postValue['monthselect'] . "-" . $postValue['yearselect'];
        $sql = "SELECT $db[pl].WAGE_HISTORY.* ,
                        $db[pl].ADD_DEDUCT_HISTORY.* ,
                        $db[ou].working_company.name as Workingname,
                        $db[ou].department.name as departmentname
                FROM $db[pl].WAGE_HISTORY 
                INNER JOIN $db[pl].ADD_DEDUCT_HISTORY ON $db[pl].ADD_DEDUCT_HISTORY.WAGE_THIS_MONTH_ID  = $db[pl].WAGE_HISTORY.WAGE_ID
                INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY
                INNER JOIN $db[ou].department ON $db[ou].department.id = $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID
                WHERE $db[pl].WAGE_HISTORY.WAGE_EMP_ID = '$idcard'
                AND $db[pl].WAGE_HISTORY.WAGE_PAY_DATE LIKE '%$selectDate%'
                AND $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE LIKE '%$selectDate%'";

        $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //       return $this->redirect(['../hr/default',['data'=>$model]],302);


    }

    public function actionWorkingcompany()
    {
        $getValue = Yii::$app->request->get();
        $db['pl'] = DBConnect::getDBConn()['pl'];
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $sql = "SELECT  $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID,
                        $db[pl].ADD_DEDUCT_DETAIL.dept_deduct_id,
                        $db[pl].dept_deduct.company_id,
                        $db[ou].working_company.name as Workingname
                FROM $db[pl].ADD_DEDUCT_DETAIL 
                INNER JOIN $db[pl].dept_deduct ON $db[pl].ADD_DEDUCT_DETAIL.dept_deduct_id  = $db[pl].dept_deduct.id
                INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].dept_deduct.company_id
                WHERE $db[pl].ADD_DEDUCT_DETAIL.ADD_DEDUCT_DETAIL_ID = '$getValue[id_deductdetail]'";

        $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

}

    