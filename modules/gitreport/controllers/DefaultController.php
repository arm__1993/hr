<?php

namespace app\modules\gitreport\controllers;

use app\modules\gitreport\models\HbsoDev;
use app\modules\gitreport\models\HbsoIssue;
use app\modules\gitreport\models\HbsoMilestone;
use app\modules\gitreport\models\HbsoProject;
use app\modules\gitreport\models\IssueStatus;
use yii\web\Controller;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `modules` module
 */
class DefaultController extends Controller
{
    public $layout = 'gitreportlayouts';
    /**
     * Renders the index view for the module
     * @return string
     */



   /* public function init()
    {
        $session = Yii::$app->session;
        $session->open();
        if ($session->isActive){
            if (!$session->has('USER_ACCOUNT')) {
                //return $this->redirect(array('/login/index'),302);
                //$a = new \app\modules\auth\controllers\DefaultController(1,'aaa');
                //$a->redirectToLogin(205);
            }
        }
    }*/


    public function actionIndex()
    {

        $mProject = HbsoProject::find()->where([
            'status_active'=>Yii::$app->params['GIT_REPORT']['ACTIVE_STATUS'],
        ])->all();

        $mDev = HbsoDev::find()->all();
        $mIssueStatus = IssueStatus::find()->all();

        $this_year = date('Y');
        $this_month = date('m');

        $mIssue = HbsoIssue::find()->where([
            'for_year'=>$this_year,
            'for_month'=>$this_month,
        ])->all();

        /*
        $mMileStone = HbsoMilestone::find()
            ->andFilterWhere(['between', 'milestone_start_date', '2017-02-01', '2017-02-28'])
            ->all();
        */
        $mMileStone = HbsoMilestone::find()
            //->andFilterWhere(['between', 'milestone_start_date', '2017-02-01', '2017-02-28'])
            ->all();

        return $this->render('index',[
            'allProject'=>$mProject,
            'allDev'    =>$mDev,
            'allStatus' =>$mIssueStatus,
            'allIssue'  =>$mIssue,
            'allMileStone' =>$mMileStone,
        ]);

    }

    public function actionUpdateissue()
    {
        return $this->render('updateissue');
    }


}
