-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 29, 2017 at 11:41 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `config_mapping_sso`
--

CREATE TABLE `config_mapping_sso` (
  `id` int(11) NOT NULL,
  `id_sso` int(11) DEFAULT NULL COMMENT 'รหัสประกันสังคม',
  `namestatussso` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'สถานะทางประกันสังคม',
  `namestatushr` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'สถานะทางโปรแกรมHR',
  `status` tinyint(4) NOT NULL COMMENT '1=ใช้งาน 99=ยกเลิก',
  `createby` varchar(13) DEFAULT NULL COMMENT 'แก้ไขโดย',
  `createdate` date DEFAULT NULL COMMENT 'วันที่สร้าง',
  `updateby` varchar(13) DEFAULT NULL COMMENT 'แก้ไขโดย',
  `updatedate` date DEFAULT NULL COMMENT 'วันที่แก้ไข'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_mapping_sso`
--

INSERT INTO `config_mapping_sso` (`id`, `id_sso`, `namestatussso`, `namestatushr`, `status`, `createby`, `createdate`, `updateby`, `updatedate`) VALUES
(1, 1, 'ลาออก/ละทิ้งหน้าที่โดยมีการติดต่อนายจ้างภายใน 6 วันทำงานติดต่อกัน', 'ลาออก', 1, NULL, NULL, NULL, NULL),
(2, 2, 'สิ้นสุดระยะเวลาการจ้าง', 'ไม่ผ่านทดลองงาน', 1, NULL, NULL, NULL, NULL),
(3, 2, 'สิ้นสุดระยะเวลาการจ้าง', 'สิ้นสุดสัญญาจ้าง', 1, NULL, NULL, NULL, NULL),
(4, NULL, NULL, 'หมดระยะเวลาฝึกงาน', 1, NULL, NULL, NULL, NULL),
(5, 3, 'เลิกจ้าง', 'เลิกจ้าง', 1, NULL, NULL, NULL, NULL),
(6, 5, 'ไล่ออก/ปลดออก/ให้ออกเนื่องจากกระทำความผิด/ละทิ้งหน้าที่โดยไม่มีการติดต่อนายจ้างภายใน 7 วันทำงานติดต่อกัน', 'ให้ออก', 1, NULL, '2017-06-29', NULL, '2017-06-29'),
(7, 4, 'เกษียณอายุ', 'เกษียณอายุ', 1, NULL, '2017-06-29', NULL, '2017-06-29'),
(8, 6, 'ตาย', 'ถึงแก่กรรม', 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `config_mapping_sso`
--
ALTER TABLE `config_mapping_sso`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `config_mapping_sso`
--
ALTER TABLE `config_mapping_sso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
