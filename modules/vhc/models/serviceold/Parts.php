<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "parts".
 *
 * @property integer $id
 * @property string $bar_code
 * @property string $bar_code_bp
 * @property string $fixnumber
 * @property string $numberpart
 * @property string $parts_group_id
 * @property string $modelparts
 * @property string $parts_name
 * @property string $parts_type
 * @property string $parts_kind
 * @property string $price_
 * @property string $price
 * @property string $pricevat
 * @property string $priceincludevat
 * @property string $parts_pic
 * @property string $parts_comment
 * @property string $packet
 * @property string $Unit_number
 * @property string $Unit
 * @property string $MUnit_number
 * @property string $MUnit
 * @property string $status_unit
 * @property double $amountPerUnit
 * @property string $Caltype
 * @property string $supplier_id
 * @property string $status_chang
 * @property string $parts_id_emp
 * @property string $part_date
 * @property string $up_parts
 * @property string $status
 * @property string $data_date
 * @property string $Valid_From
 * @property string $Valid_To
 * @property string $Daily_Monthly_Flag
 */
class Parts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parts';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bar_code', 'bar_code_bp', 'fixnumber', 'numberpart', 'parts_group_id', 'modelparts', 'parts_name', 'parts_type', 'parts_kind', 'price_', 'price', 'pricevat', 'priceincludevat', 'parts_pic', 'parts_comment', 'packet', 'Unit_number', 'Unit', 'MUnit_number', 'MUnit', 'status_unit', 'amountPerUnit', 'Caltype', 'supplier_id', 'status_chang', 'parts_id_emp', 'part_date', 'up_parts', 'status', 'data_date', 'Valid_From', 'Valid_To', 'Daily_Monthly_Flag'], 'required'],
            [['price_', 'price', 'pricevat', 'priceincludevat', 'amountPerUnit'], 'number'],
            [['parts_comment'], 'string'],
            [['part_date', 'data_date', 'Valid_From', 'Valid_To'], 'safe'],
            [['bar_code', 'bar_code_bp'], 'string', 'max' => 30],
            [['fixnumber', 'parts_group_id', 'supplier_id', 'status_chang', 'parts_id_emp', 'up_parts'], 'string', 'max' => 20],
            [['numberpart', 'parts_pic'], 'string', 'max' => 50],
            [['modelparts'], 'string', 'max' => 200],
            [['parts_name'], 'string', 'max' => 500],
            [['parts_type', 'parts_kind', 'status_unit', 'Caltype', 'status'], 'string', 'max' => 2],
            [['packet'], 'string', 'max' => 5],
            [['Unit_number', 'Unit', 'MUnit_number', 'MUnit'], 'string', 'max' => 100],
            [['Daily_Monthly_Flag'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bar_code' => 'Bar Code',
            'bar_code_bp' => 'Bar Code Bp',
            'fixnumber' => 'Fixnumber',
            'numberpart' => 'Numberpart',
            'parts_group_id' => 'Parts Group ID',
            'modelparts' => 'Modelparts',
            'parts_name' => 'Parts Name',
            'parts_type' => 'Parts Type',
            'parts_kind' => 'Parts Kind',
            'price_' => 'Price',
            'price' => 'Price',
            'pricevat' => 'Pricevat',
            'priceincludevat' => 'Priceincludevat',
            'parts_pic' => 'Parts Pic',
            'parts_comment' => 'Parts Comment',
            'packet' => 'Packet',
            'Unit_number' => 'Unit Number',
            'Unit' => 'Unit',
            'MUnit_number' => 'Munit Number',
            'MUnit' => 'Munit',
            'status_unit' => 'Status Unit',
            'amountPerUnit' => 'Amount Per Unit',
            'Caltype' => 'Caltype',
            'supplier_id' => 'Supplier ID',
            'status_chang' => 'Status Chang',
            'parts_id_emp' => 'Parts Id Emp',
            'part_date' => 'Part Date',
            'up_parts' => 'Up Parts',
            'status' => 'Status',
            'data_date' => 'Data Date',
            'Valid_From' => 'Valid  From',
            'Valid_To' => 'Valid  To',
            'Daily_Monthly_Flag' => 'Daily  Monthly  Flag',
        ];
    }
}
