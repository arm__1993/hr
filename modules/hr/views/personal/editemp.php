<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:13
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/formaddnewemp.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
// //$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);



?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li class="active">
					<a href="#">ปรับปรุงข้อมูลพนักงาน</a>
				</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <?php 
           	Pjax::begin(['id' => 'pjax_tb_emp']);
	           echo GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $Empdata,
				'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
				'columns' => [
					
					[
						'class' =>'yii\grid\SerialColumn',
                        //'headerOptions' => ['width' => '20px'],
					],
                    [
						'attribute' => 'Code',
						'label' => 'รหัสพน้กงาน',
						'value' => 'Code',
						//'contentOptions' => ['style' => 'width:10px']
					],
					[
						'attribute' => 'Name',
						'label' => 'ชื่อ',
						'value' => 'Name',
						//'contentOptions' => ['style' => 'width:10px']
					],
					[
						'attribute' => 'Surname',
						'label' => 'นามสกุล',
						'value' => 'Surname',
						//'filter' => false,
						//'contentOptions' => ['style' => 'width:90px']
					],
                    [
						'attribute' => 'Nickname',
						'label' => 'ชื่อเล่น',
						'value' => 'Nickname',
						//'filter' => false,
						//'contentOptions' => ['style' => 'width:90px']
					],
                    [
						'attribute' => 'ID_Card',
						'label' => 'หมายเลขบัตรประชาชน',
						'value' => 'ID_Card',
						//'filter' => false,
						//'contentOptions' => ['style' => 'width:90px']
					],
					[
						//'headerOptions' =>['width' => '100'],
						'header' => 'การจัดการ',
						'class' => 'yii\grid\ActionColumn',
						'template' => '{view}&nbsp;&nbsp;&nbsp;{update} ',
						'buttons' => [
							'update' => function ($url, $data) {
							
								//return '<a onclick="editEmp(' . $data->DataNo . ')"><i class="fa fa-pencil-square-o fa-1x" style="color:#3c8dbc" aria-hidden="true"></i></a>';
								return Html::a('<i class="fa fa-pencil-square-o fa-2x" style="color:#3c8dbc" aria-hidden="true"></i>', 'javascript:;', [
										//'title' => 'แก้ไข',
										'onclick' => '(function($event) {
											editEmp(' . $data->DataNo . ',"");
									})();'
								]);
							},
							'view' => function ($url, $data) {
							
								//return '<a><i class="fa fa-eye fa-1x" style="color:#3c8dbc" aria-hidden="true"></i></a>';
								return Html::a('<i class="fa fa-eye fa-2x" style="color:#3c8dbc" aria-hidden="true"></i>', 'javascript:;', [
										//'title' => 'แก้ไข',
										'onclick' => '(function($event) {
											editEmp(' . $data->DataNo . ',"view");
									})();'
								]);
							}
						],
						
					]

				]
			]);
	           Pjax::end();
           ?>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->