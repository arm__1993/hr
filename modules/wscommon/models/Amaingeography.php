<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "aMain_geography".
 *
 * @property integer $geo_id
 * @property string $geo_name
 */
class Amaingeography extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
        /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }
    public static function tableName()
    {
        return 'aMain_geography';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['geo_name'], 'required'],
            [['geo_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'geo_id' => 'Geo ID',
            'geo_name' => 'Geo Name',
        ];
    }



}
