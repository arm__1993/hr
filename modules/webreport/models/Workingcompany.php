<?php

namespace app\modules\webreport\models;

use Yii;

class Workingcompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'working_company';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_name', 'codeName_tax', 'short_name', 'name', 'address', 'Tel', 'Fax', 'soc_acc_number', 'inv_number', 'business_number', 'numberbranch', 'dealercode', 'Code', 'company_type', 'branch_number', 'branch_comment', 'branch_salesman_cd', 'BranchName_TIS', 'show_in_report', 'images_pdf_home', 'images_pdf_isuzu'], 'required'],
            [['address', 'images_pdf_home', 'images_pdf_isuzu'], 'string'],
            [['status'], 'integer'],
            [['code_name', 'codeName_tax'], 'string', 'max' => 2],
            [['short_name', 'Tel', 'Fax', 'inv_number', 'dealercode', 'Code'], 'string', 'max' => 20],
            [['name', 'branch_number', 'branch_comment'], 'string', 'max' => 250],
            [['soc_acc_number'], 'string', 'max' => 13],
            [['business_number'], 'string', 'max' => 50],
            [['numberbranch', 'branch_salesman_cd'], 'string', 'max' => 10],
            [['company_type', 'show_in_report'], 'string', 'max' => 1],
            [['BranchName_TIS'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_name' => 'Code Name',
            'codeName_tax' => 'Code Name Tax',
            'short_name' => 'Short Name',
            'name' => 'Name',
            'address' => 'Address',
            'Tel' => 'Tel',
            'Fax' => 'Fax',
            'soc_acc_number' => 'Soc Acc Number',
            'status' => 'Status',
            'inv_number' => 'Inv Number',
            'business_number' => 'Business Number',
            'numberbranch' => 'Numberbranch',
            'dealercode' => 'Dealercode',
            'Code' => 'Code',
            'company_type' => 'Company Type',
            'branch_number' => 'Branch Number',
            'branch_comment' => 'Branch Comment',
            'branch_salesman_cd' => 'Branch Salesman Cd',
            'BranchName_TIS' => 'Branch Name  Tis',
            'show_in_report' => 'Show In Report',
            'images_pdf_home' => 'Images Pdf Home',
            'images_pdf_isuzu' => 'Images Pdf Isuzu',
        ];
    }
}
