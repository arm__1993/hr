<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 09:51
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;

use app\modules\hr\apihr\ApiOT;


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/employee-lookup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/ot_manageot.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/ot_manageot.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

$arrHour = DateTime::makeHour();
$arrMinute = DateTime::makeMinute();

$today = date('d/m/Y');

/*$arrOTReturn = [
    '1' => 'เงิน',
    '2' => 'วันหยุด'
];*/

//$arrOTReturn = $this->ReturnOTBenefit;
//echo $OtConfig['pay_motel'];


//TODO : 1. Javascript Validate, 2. Save Success show popup
//TODO : Session for user create , approved

?>

<form id="frmListOTRequest" name="frmListOTRequest" action="#" method="post">
    <section class="content">
        <!-- Default box -->
        <div class="box box-danger">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#"> ข้อมูลเงินเดือน</a>
                    </li>
                    <li>บันทึกข้อมูลการทำงานล่วงเวลา</li>
                    <li class="active">แก้ไขข้อมูลการทำงานล่วงเวลา</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">


                <div class="box-body">
                    <div class="row">
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>บริษัท <span>*</span></label>
                            <select class="form-control" name="xcompany" id="xcompany">
                                <option value="">เลือกบริษัท</option>
                                <?php
                                $working = ApiHr::getWorking_company();
                                foreach ($working as $value) {
                                    $sel = ($OtMaster['company_id']==$value['id']) ? ' selected="selected"' : '';
                                    echo '<option value="' . $value['id'] . '" '.$sel.'>' . $value['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label>แผนก <span>*</span></label>
                            <select class="form-control" name="xdepartment" id="xdepartment">
                                <option value=""> เลือกแผนก</option>
                                <?php
                                foreach ($arrDepartment as $item) {
                                    $sel = ($OtMaster['division_id']==$item['id']) ? ' selected="selected"' : '';
                                    echo '<option value="' . $item['id'] . '" '.$sel.'>' . $item['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label>ฝ่าย <span>*</span></label>
                            <select class="form-control" name="xsection" id="xsection">
                                <option value=""> เลือกฝ่าย</option>
                                <?php
                                foreach ($arrSection as $item) {
                                    $sel = ($OtMaster['section_id']==$item['id']) ? ' selected="selected"' : '';
                                    echo '<option value="' . $item['id'] . '" '.$sel.'>' . $item['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-1">&nbsp;</div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-sm-1">
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <label>กิจกรรมโอที <span>*</span></label>
                            <?php echo Html::dropDownList('activity_id', $OtMaster['activity_id'], $arrOTActivity, [
                                'id' => 'activity_id',
                                'prompt' => 'กรุณาเลือกกิจกรรม',
                                'class' => 'form-control',
                                'data-required' => 'true',
                            ]);
                            ?>
                        </div>

                        <div class="col-sm-4">
                            <label>ประเภทการคิดโอที </label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_calculate_type" value="1" <?php if($OtMaster['ot_calculate_type']==1) echo 'checked'; ?>>
                                    <span>ปกติ</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_calculate_type" value="2" <?php if($OtMaster['ot_calculate_type']==2) echo 'checked'; ?>>
                                    <span>วันหยุด</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_calculate_type" value="3" <?php if($OtMaster['ot_calculate_type']==3) echo 'checked'; ?>>
                                    <span>นอกเวลาวันหยุด</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>วันที่ทำกิจกรรมโอที <span>*</span></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" readonly="readonly" name="ot_date" data-required="true" id="ot_date"
                                       value="<?php echo DateTime::CalendarDate($OtMaster['activity_date']); ?>" class="form-control pull-right"
                                       placeholder="dd/mm/yyyy">
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>มีค่าเดินทาง <span>*</span></label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_route" value="1" id="openrouteprofile" <?php if($OtMaster['has_profile_route']==1) echo 'checked'; ?>>
                                    <span>มี</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_route" value="0" id="closerouteprofile" <?php if($OtMaster['has_profile_route']==0) echo 'checked'; ?>>
                                    <span>ไม่มี</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>คิดค่าเดินทาง <span>*</span></label>
                            <?php
                            echo Html::dropDownList('profileroute_id', $OtMaster['profile_route_id'], $arrOTProfileRoute, [
                                'id' => 'profileroute_id',
                                //'prompt' => 'กรุณาเลือกค่าเดินทาง',
                                'class' => 'form-control',
                                'data-required' => 'true',
                            ]); ?>
                        </div>
                        <div class="col-sm-2">
                            <label>จำนวนระยะทาง (กม.) <span>*</span></label>
                            <input type="text" class="form-control" id="ot_distance" name="ot_distance" value="<?php echo $OtMaster['distance_amount'];?>">
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3">
                            <label>มีค่าที่พัก <span>*</span></label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_motel" value="1" id="openmotel" <?php if($OtMaster['has_motel_profile']==1) echo 'checked'; ?>>
                                    <span>มี</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="ot_motel" value="0" id="closemotel" <?php if($OtMaster['has_motel_profile']==0) echo 'checked'; ?>>
                                    <span>ไม่มี</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>จ่ายตามจริง (บาท) <span>*</span></label>
                            <input type="text" class="form-control"
                                   value="<?php echo ($OtMaster['motel_price']) ? $OtMaster['motel_price'] : null; ?>"
                                   readonly="readonly" id="motel_price" name="motel_price">
                        </div>
                        <div class="col-sm-2">
                            <label>รอบเงินเดือน <span>*</span></label>
                            <input type="text" class="form-control" data-required="true" id="month_pay" name="month_pay" value="<?php echo $OtMaster['wage_pay_date'];?>">
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                </div>


                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">รายชื่อพนักงานทำกิจกรรมโอที</h3>
                        <div class="pull-right" style="margin-bottom: 10px;">
                            <?php
                            echo Html::button('<i class="fa fa-plus-circle"></i> เพิ่มแถว',
                                [
                                    'class' => 'btn btn-success',
                                    'id' => 'btnAddNewRow',
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive" id="dvEmpList">

                                    <?php
                                    Pjax::begin(['id' => 'pjax_tbemployeeot']);

                                    ?>
                                    <table id="tbemployeeot" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 2%">ลำดับ</th>
                                            <th style="width: 18%">ชื่อ-สกุล</th>
                                            <th style="width: 15%">แผนก</th>
                                            <th style="width: 12%">เวลาเข้า OT</th>
                                            <th style="width: 12%">เวลาออก OT</th>
                                            <th style="width: 5%">รวมเวลา</th>
                                            <th style="width: 5%">รวมเงิน</th>
                                            <th style="width: 10%">เลือกค่าตอบแทน</th>
                                            <th style="width: 2%">ลบ</th>
                                        </tr>
                                        <thead>
                                        <tbody>
                                        <?php

                                        $idx = 1;
                                        foreach ($OtDetail as $item)
                                        {
                                        ?>
                                        <tr>
                                            <td><?php echo $idx;?></td>
                                            <td><?php echo $arrEmpData[$item->id_card];?></td>
                                            <td><?php echo $arrDepartment[$OtMaster['division_id']]['name']; ?></td>
                                            <td style="width: 10%;text-align: center;"><?php echo ApiOT::showTime($item->time_start); ?></td>
                                            <td style="width: 10%;text-align: center;"><?php echo ApiOT::showTime($item->time_end); ?></td>
                                            <td style="width: 10%;text-align: right;"><?php echo ApiOT::showTime($item->time_total); ?></td>
                                            <td style="width: 10%;text-align: right;"><?php echo Helper::displayDecimal($item->money_total); ?></td>
                                            <td><?php echo $item->return_name; ?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" id="del" onclick="deleteMewithEmp($(this),<?php echo $item['id'];?>);" class="btn btn-danger"><i class="fa fa-trash"></i> </button>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                            $idx++;
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $idx;?>.</td>
                                            <td>
                                                <select class="form-control select2" style="width: 100%;"
                                                        id="otEmployee" name="otEmployee[]">
                                                </select>
                                            </td>
                                            <td><input type="text" id="txtDepartment" name="txtDepartment[]" readonly="readonly" class="form-control"></td>
                                            <td>
                                                <?php
                                                echo Html::dropDownList('start_hour[]', null, $arrHour, [
                                                    'id' => 'start_hour',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?> : <?php
                                                echo Html::dropDownList('start_minute[]', null, $arrMinute, [
                                                    'id' => 'start_minute',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?></td>
                                            <td>
                                                <?php
                                                echo Html::dropDownList('end_hour[]', null, $arrHour, [
                                                    'id' => 'end_hour',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?> : <?php
                                                echo Html::dropDownList('end_minute[]', null, $arrMinute, [
                                                    'id' => 'end_minute',
                                                    'prompt' => '',
                                                    'class' => 'time_width',
                                                    'data-required' => 'true',
                                                ]);
                                                ?></td>
                                            <td><input type="text" id="totaltime" name="totaltime[]" readonly="readonly" class="form-control"></td>
                                            <td><input type="text" id="totalmoney" name="totalmoney[]" readonly="readonly" class="form-control"></td>
                                            <td>
                                                <?php

                                                echo Html::dropDownList('otreturn_id[]', null, $arrOTReturn, [
                                                    'id' => 'otreturn_id',
                                                    'prompt' => 'ค่าตอบแทน',
                                                    'class' => 'form-control',
                                                    'data-required' => 'true',
                                                ]); ?>
                                                <input type="hidden" name="remark[]">
                                                <input type="hidden" name="wage[]">
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" id="del" onclick="deleteMe($(this));" class="btn btn-danger"><i class="fa fa-trash"></i> </button>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <?php
                                    Pjax::end();  //end pjax_tbemployeeot

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center" style="margin-bottom: 40px !important;">
                        <?php
                        echo Html::button('<i class="fa fa-save"></i> บันทึกข้อมูล',
                            [
                                'class' => 'btn btn-primary',
                                'id' => 'btnSaveManageOT',
                            ]);
                        ?>
                    </div>
                    <div style="height: 20px;"></div>
                </div>

            </div>
        </div>
        <!-- /.box -->
    </section><!-- /.content -->
    <input type="hidden" id="hide_edit" name="hide_edit" value="<?php echo $OtMaster['id']; ?>">
    <span id="mode" title="edit"></span>
</form>


