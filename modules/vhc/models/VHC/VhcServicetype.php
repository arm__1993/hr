<?php

namespace app\modules\vhc\models\VHC;


use Yii;
use yii\data\ActiveDataProvider;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_servicetype".
 *
 * @property integer $id
 * @property string $servicetype_name
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcServicetype extends MasterModel
{
    /**
     * @inheritdoc
     */
    protected $_pageSize;
    public static function tableName()
    {
        return 'vhc_servicetype';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['servicetype_name', 'record_status'], 'required'],
            [['record_status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['servicetype_name'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servicetype_name' => 'Servicetype Name',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
    /*public function search($params)
    {

        $query = VhcServicetype::find()->where(['!=','record_status','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(
                                ['like', 'servicetype_name', $this->servicetype_name]
                                );

        return $dataProvider;
    }*/

    public function search($params)
    {
        $data = VhcServicetype::find() ->where('record_status <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','servicetype_name',$this->servicetype_name]); //	ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
