<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/4/2016 AD
 * Time: 11:02 AM
 */


namespace app\modules\wssale\controllers;
use app\modules\wssale\models\Book;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;


class BookController extends ActiveController
{

    // adjust the model class to match your model
    public $modelClass = 'app\modules\wssale\models\Book';
	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}


	public function actionSearchme()
    {
        $f = \Yii::$app->request->get('f');
        $book = new Book();
        $bookSearch = $book->searchbytitle($f);
        return $bookSearch;
    }





}