<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Helper;
use app\api\DateTime;
use app\api\Common;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;


HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
});
JS;

/*$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);*/
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);

?>


<h3><i class="fa fa-fw fa-file-text"></i>รายงานประสิทธิภาพงานโดยรวม</h3>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">
        <div class="row">
            <form method="post" action="receivepfm">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
            
            <div class="col-md-12">
             <label>บริษัท</label>
             &nbsp;
             <?php //print_r($selected_company) ?>
                    <div class="btn-group">
                        <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key=>$value) {
                                    $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    &nbsp;&nbsp;               
                     <label>เลือกเดือน</label>
                     &nbsp;
                    <div class="btn-group">
                        <select class="form-control" name="month" id="month" style="width: 150px">
                            <option value="">เลือกเดือน</option>
                                <?php
                                for ($i=1 ; $i<=12 ; $i++ ){
                                $sel = ($i==$selected_months) ? 'selected="selected"': '';
                                echo '<option value="'.$i.'" '.$sel.'>'.\app\api\DateTime::convertMonth($i).'</option>';
                                }
                                ?>
                        </select>
                        </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <label>เลือกปี</label>
                &nbsp;
                <?php //print_r($selected_year[0]) ?>
                    <div class="btn-group">
                        <select class="form-control" name="year[]" id="year">
                                <?php
                                $get_year = ApiReport::getAppYear();
                                foreach ($get_year as $key=>$value) {
                                    $sel_year = ($selected_year==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel_year.' >'.$value.'</option>';
                                }
                                $countyear = count($years);
                                ?>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;

                    <label>เลือกช่าง</label>
                    &nbsp;
                    <div class="btn-group">
                    <input type="radio" name="technician" value="ช่างรถเล็ก" <?php echo ($selected_technician=='ช่างรถเล็ก') ? 'checked' : '';?> checked> ช่างรถเล็ก
                    &nbsp;
                    <input type="radio" name="technician" value="ช่างรถใหญ่" <?php echo ($selected_technician=='ช่างรถใหญ่') ? 'checked' : '';?> > ช่างรถใหญ่
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;

                    <center><br>
                    <div class="btn-group">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-fw fa-search"></i>ค้นหา
                    </button>
                    </div>
                    &nbsp;&nbsp;
                    <div class="btn-group">
                    <a href="<?php echo SITE_URL;?>/receivewage/btn_reportpfm">
                    <button type="button" class="btn btn-success ">
                        <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                    </button>
                    </a>
                    </div>
                    </center>
                </div>
            </form>
        </div>
    </div>
    <!-- /.box-body -->

    <?php
    if(empty($weekend))
        {
            
        }
    else{
        $sumweekend = array_sum($weekend); //มาทำงานวันอาทิตย์
        $sumworkingday = array_sum($workingday); //มาทำงานวันปกติ

        $Avg_workday = ($_arrRET['total_workday'] > 0) ?  ($sumworkingday/$_arrRET['total_workday']): 0;
        $Avg_sunday = ($_arrRET['total_sunday'] > 0) ?  ($sumweekend/$_arrRET['total_sunday']): 0;

        $date_work = $_arrRET['total_workday']*8*$Avg_workday;
        $date_sun = $_arrRET['total_sunday']*8*$Avg_sunday;

        $resultsum = ($date_work+$date_sun)-(250+80);
    }
    ?>
    <?php if($query){?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- AREA TABLE -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">ตารางแสดงชั่วโมงการทำงานของช่าง</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6"></div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <table id="example2" class="table table-bordered table-hover dataTable"
                               role="grid" aria-describedby="example2_info">
                            <thead>

                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                    rowspan="1" colspan="1" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">
                                    เดือน
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    จันทร์-เสาร์
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    อาทิตย์
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    ลา(ชม.)
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    OT(ชม.)
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    รวมชั่วโมงการทำงาน
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    จำนวนวันทำงาน (เดือน)
                                </th>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    <?php
                                    echo ($_arrRET['total_workday']) ? $_arrRET['total_workday'] : 0;
                                        ?>
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    <?php
                                    echo ($_arrRET['total_sunday']) ? $_arrRET['total_sunday'] : 0;
                                        ?>
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   250
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   80
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    <?php
                                    echo ($resultsum) ? $resultsum : 0;
                                    ?>
                                </td>                              
                            </tr>
                            <tr>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    จำนวนช่างต่อวัน (เฉลี่ย)
                                </th>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    <?php
                                    echo ($Avg_workday) ? (int)$Avg_workday : 0;
                                        ?>
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    <?php
                                    echo ($Avg_sunday) ? (int)($Avg_sunday) : 0;
                                        ?>
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                            </tr>
                            <tr>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    ชั่วโมงทำงานต่อวัน
                                </th>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   8
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   8
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                            </tr>
                            <tr>
                                <th class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    จำนวนชั่วโมงทำงาน (เดือน)
                                </th>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                     <?php
                                     echo ($date_work) ? $date_work : 0;
                                        ?>
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                    <?php
                                    echo ($date_sun) ? $date_sun : 0;
                                        ?>
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="example2"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="Browser: activate to sort column ascending">
                                   -
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box-success -->
    </div>
    </div>
    </section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- AREA TABLE -->
    <div class="box box-info">
        <div class="box-header">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6"></div>
                </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        <?php
        if(!empty($_arrRET))
        {
        ?>

            <?php
            $arrData = [];
            $arrMonth = [];
        
        /*<!--------------------chartsX--------------------->*/
            $workday = $_arrRET['total_workday'];
            $sunday = $_arrRET['total_sunday'];
            $nummonth = $workday+$sunday;
            $arrAbsent = []; // ขาดงาน
            $arrWorkIn = []; // คนมาทำงาน
            $allTechnichian = $technician_ofday;
            for($i=1;$i<=$nummonth;$i++)
            {
            
                $arrMonth[] = (int)$i;

                for($j=0;$j<$nummonth;$j++)
                {
                    $tmp = $a_workday[$i-1];
                }

                //$tmp = rand(1,5); 

                $arrAbsent[] = $allTechnichian-$tmp;
                $arrWorkIn[] = $tmp;

            }


            // print_r($arrMonth);
        /*<!--------------------series--------------------->*/
        

        $arrData[0] = [
                'dataLabels' =>[
                    'enabled' => true,
                    'color' => "(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'",
                    ],
                'stacking' => 'normal',
                'name' => 'ไม่มาทำงาน',
                'data' => $arrAbsent,
                'color'=> '#F1948A',
            ];


        $arrData[1] = [
                'dataLabels' =>[
                    'enabled' => true,
                    'color' => "(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'",
                    ],
                'stacking' => 'normal',
                'name' => 'มาทำงาน',
                'data' => $arrWorkIn,
                'color'=> '#1ABC9C'
            ];
            ?>

            <?php
            echo Highcharts::widget([
                'options' => [
                            'chart' => [
                                'type' => 'column'
                            ],
                            'title' => [
                                'text' => 'กราฟแสดงจำนวนช่างต่อวัน'
                            ],
                           'column' => [                                
                                'dataLabels' => [
                                    'enabled' => true,
                                    'color' => "(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'",
                                    'style' => [
                                        'textShadow' => "'0 0 3px black'"
                                    ]
                                ]
                            ],
                            'xAxis' => [
                            'categories' => $arrMonth,
                            ],
                            'yAxis' => [
                                'min' => 0,
                                'title' => [
                                    'text' => 'จำนวนช่าง (คน)'
                                ],
                                'stackLabels' => [
                                    'enabled' => true,
                                    'style' => [
                                        'fontWeight' => 'bold',
                                        'color' => "(Highcharts.theme && Highcharts.theme.textColor) || '#FFFFFF'"
                                    ]
                                ]
                            ],
                            'labels' => [
                                'items' => [
                                    'html' => 'Total fruit consumption',
                                    'style' => [
                                        'left' => '50px',
                                        'top' => '18px',
                                        'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                    ]
                                ]
                            ],
                            'tooltip' => [
                                'headerFormat' => '<b>{point.x}</b><br/>',
                                'pointFormat' => '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                            ],
                            'credits' => [
                                'enabled' => false
                            ],
                                                  
                'series' => [    
                        $arrData[0],
                        $arrData[1], 
                    ]
                ]
                
            ]);  
            ?>

        <?php } else { ?>
                <?php
            echo Highcharts::widget([
                'options' => [
                            'chart' => [
                                'type' => 'column'
                            ],
                            'title' => [
                                'text' => 'กราฟแสดงจำนวนช่างต่อวัน'
                            ],
                           'column' => [                                
                                'dataLabels' => [
                                    'enabled' => true,
                                    'color' => "(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'",
                                    'style' => [
                                        'textShadow' => "'0 0 3px black'"
                                    ]
                                ]
                            ],
                            'xAxis' => [
                            'categories' => ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                            ],
                            'yAxis' => [
                                'min' => 0,
                                'title' => [
                                    'text' => 'จำนวนช่าง (คน)'
                                ],
                                'stackLabels' => [
                                    'enabled' => true,
                                    'style' => [
                                        'fontWeight' => 'bold',
                                        'color' => "(Highcharts.theme && Highcharts.theme.textColor) || '#FFFFFF'"
                                    ]
                                ]
                            ],
                            'labels' => [
                                'items' => [
                                    'html' => 'Total fruit consumption',
                                    'style' => [
                                        'left' => '50px',
                                        'top' => '18px',
                                        'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                    ]
                                ]
                            ],
                            'tooltip' => [
                                'headerFormat' => '<b>{point.x}</b><br/>',
                                'pointFormat' => '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                            ],
                            'credits' => [
                            'enabled' => false
                            ],
                                                  
                'series' => [    
                        [
                            'dataLabels' =>[
                                'enabled' => true,
                                'color' => "(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'",
                                ],
                            'stacking' => 'normal',
                            'name' => 'ไม่มาทำงาน',
                            'data' => [0,5,3,2,4,6,1,2,5,3,1,4],
                            'color'=> '#F1948A',
                        ],
                        [
                            'dataLabels' =>[
                                'enabled' => true,
                                'color' => "(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'",
                                ],
                            'stacking' => 'normal',
                            'name' => 'มาทำงาน',
                            'data' => [50,45,47,48,46,44,49,48,45,47,49,46],
                            'color'=> '#1ABC9C'
                        ],
                    ]
                ]
                
            ]);  
            ?>
        <?php } ?> 

       </div>
    </div>
    </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box-info -->
    </div>
    </div>
    </section>


    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        <?php
        $sumwage = $sumwage[0]['pricewage'];
        $divwage = $sumwage/470;
        $percentwage = ($resultsum > 0) ?  ($divwage/$resultsum)*100 : 0;
        ?>

            <h4>รายได้ค่าแรงทั้งหมด          <?php echo Helper::displayDecimal($sumwage); ?></h4><br>
            <h4>ค่าแรงต่อชั่วโมงที่ขาย           470</h4><br>
            <h4>ค่าแรงต่อชั่วโมงที่ขายได้   <?php echo Helper::displayDecimal($divwage); ?></h4><br>
            <h4>ประสิทธิภาพงานซ่อมโดยรวม      <?php echo Helper::displayDecimal($percentwage); ?>%</h4>
        </div>
    </div>

</div>
<!-- /.box -->
    <?php } ?>
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>