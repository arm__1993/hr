<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:48
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

AppAsset::register($this);
//?t='.time(
//dist
$this->registerCssFile(Yii::$app->request->BaseUrl . "/js/hr/bootstrap-treeview/dist/bootstrap-treeview.min.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/bootstrap-treeview/dist/bootstrap-treeview.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/menulink.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/js/jquery.sumoselect.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/vhc/service/servicemaping.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/permission/sumoselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); 
$this->registerCss("
.panel-body {
    padding: 0px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}
.setautomagin {
    margin-left:  auto;
    margin-right:  auto;
}
");
?>
<section class="content">
    <!-- Default box -->
    <div class="box-body"  id="dvloadot"style="width:  100%;height: 370%;position: absolute;background: #b5b1b112;z-index:  2;" >
        <div class="table-responsive"  style="margin: 0 auto;margin-top: 300px;margin-left: 0;margin-right: 0; text-align: center;">
            <img class="loading-image" src="../../images/global/ajax-loader.gif" alt="loading..">
        </div>
    </div>
    <div class="box box-danger" disable>
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดการสิทธิ์</a>
                </li>
                <li class="active">เมนูและลิงค์</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class='row'>
                <div class='panel panel-success'>
                    <div class="panel-body">
                        <div class='col-md-1'>
                        </div>
                        <div class='col-md-4' sytle='height:100%'>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">model :</label>
                                <div class="col-sm-8">
                                    <select class="form-control" style='width:100%' name="selectmodel" id="selectmodel" required>
                                        <option value=""> เลือก model รถ </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-2'>
                        </div>
                        <div class='col-md-4'>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">vhc model :</label>
                                <div class="col-sm-8">
                                    <select class="form-control" style='width:100%' name="selectvhcmodel" id="selectvhcmodel" required>
                                        <option value=""> เลือกประเภทรถ</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-1'>
                        </div>
                    </div>
                </div>
            </div>
            <div style='overflow: scroll;'>
                <div  style='width: 150%;'>
                    <div class='row' >
                        <!--programs name-->
                        <div class="col-sm-2" style='height: 876px;overflow: scroll;'>
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <div class="row col-sm-12 setautomagin">
                                        <!--<h4><b>รายชื่อโปรแกรม</b></h4>-->
                                        <label class="txt-head-1">
                                            รายชื่อระบบ
                                        </label>
                                        <!-- <div class="pull-right action-buttons">
                                            <button type="button" id="" class="btn btn-success btn-xs" 
                                                onclick="showModelFormPlace(this,'new',event)"  />
                                                <i class="glyphicon glyphicon-plus"></i>
                                                เพิ่ม
                                            </button>
                                        </div> -->
                                    </div>
                                    <div class="row col-sm-12 setautomagin" style="margin-top:10px">
                                        
                                        <input type="hidden" value="" id="place_id_sel">
                                    
                                        <ul class="list-group crepair-lists-ul crepair">

                                            <?php 
                                            foreach($crepair AS $no => $crepair){ 
                                            
                                                echo Yii::$app->controller->renderPartial('_crepair',[
                                                    'item' => $crepair
                                                ]);
                                        
                                            }
                                            ?>
                                        
                                        
                                        </ul>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--manage-->
                        <div class="col-sm-2" style='height: 876px;overflow: scroll;'>
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <div class="row col-sm-12 setautomagin">
                                        <!--<h4><b>รายชื่อโปรแกรม</b></h4>-->
                                        <label class="txt-head-1">
                                            รายชื่ออาการ
                                        </label>
                                        <!-- <div class="pull-right action-buttons">
                                            <button type="button" id="" class="btn btn-success btn-xs" 
                                                onclick="showModelFormPlace(this,'new',event)"  />
                                                <i class="glyphicon glyphicon-plus"></i>
                                                เพิ่ม
                                            </button>
                                        </div> -->
                                    </div>
                                    <div class="row col-sm-12 setautomagin" style="margin-top:10px">
                                        <div id="form-move-crepairlist" >
                                            <--- รอสักครู่..
                                        </div>  
                                        <input type="hidden" value="" id="place_id_sel">
                                        <ul class="list-group program-lists-ul" id='testww'>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--manage-->
                        <div class="col-sm-2" style='height: 876px;overflow: scroll;'>
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <div class="row col-sm-12 setautomagin">
                                        <!--<h4><b>รายชื่อโปรแกรม</b></h4>-->
                                        <label class="txt-head-1">
                                            รายชื่อค่าแรง
                                        </label>
                                        <!-- <div class="pull-right action-buttons">
                                            <button type="button" id="" class="btn btn-success btn-xs" 
                                                onclick="showModelFormPlace(this,'new',event)"  />
                                                <i class="glyphicon glyphicon-plus"></i>
                                                เพิ่ม
                                            </button>
                                        </div> -->
                                    </div>
                                    <div class="row col-sm-12 setautomagin" style="margin-top:10px">
                                        <div id="form-move-laber">
                                            <--- รอสักครู่..
                                        </div>  
                                        <input type="hidden" value="" id="place_id_sel">
                                        <ul class="list-group program-lists-ul" id='testleber'>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--manage  part-->
                        <div class="col-sm-2" style='height: 876px;overflow: scroll;'>
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <div class="row col-sm-12 setautomagin">
                                        <!--<h4><b>รายชื่อโปรแกรม</b></h4>-->
                                        <label class="txt-head-1">
                                            รายชื่ออะไหล่
                                        </label>
                                        <!-- <div class="pull-right action-buttons">
                                            <button type="button" id="" class="btn btn-success btn-xs" 
                                                onclick="showModelFormPlace(this,'new',event)"  />
                                                <i class="glyphicon glyphicon-plus"></i>
                                                เพิ่ม
                                            </button>
                                        </div>
                                        -->
                                    </div>
                                    <div class="row col-sm-12 setautomagin" style="margin-top:10px">
                                        <div id="form-move-part">
                                            <--- รอสักครู่..
                                        </div>  
                                        <input type="hidden" value="" id="place_id_sel">
                                        <div class="row form-group">
                                            <div class="col-sm-12 hidden" id='divseartpart'>
                                                <select class="form-control" style='width:100%' name="seartpart" id="seartpart" required>
                                                    <option value="">  </option>
                                                </select>
                                            </div>
                                        </div>
                                        <ul class="list-group program-lists-ul" id='partstest'>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <input type="button" name="select" id='select' value=">>">
                            <br>
                            <input type="button" name="unselect" id='unselect' value="<<">
                        </div>
                        <div class="col-sm-3" style='height: 876px;overflow: scroll;'>
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <div class="row col-sm-12 setautomagin">
                                        <!--<h4><b>รายชื่อโปรแกรม</b></h4>-->
                                        <label class="txt-head-1">
                                            รายชื่ออะไหล่
                                        </label>
                                        <!-- <div class="pull-right action-buttons">
                                            <button type="button" id="" class="btn btn-success btn-xs" 
                                                onclick="showModelFormPlace(this,'new',event)"  />
                                                <i class="glyphicon glyphicon-plus"></i>
                                                เพิ่ม
                                            </button>
                                        </div>
                                        -->
                                    </div>
                                    <div class="row col-sm-12 setautomagin" style="margin-top:10px">
                                        <div id="form-move-partselect">
                                            <--- รอสักครู่..
                                        </div>  
                                        <input type="hidden" value="" id="place_id_sel">
                                        <ul class="list-group program-lists-ul" id='selectpart'>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                
        </div>
    </div>
    <!-- /.box -->
    <div class='row'>
        <button class='btn btn-md-3 btn-success'id='savebtn'> save </button>
    </div>
</section><!-- /.content -->


<div class="modal fade modal-wide" 
    id="modal-form-add-place" 
    tabindex="-1" role="dialog" 
    aria-labelledby="myModalLabel" 
    aria-hidden="true"
    data-backdrop="static" data-keyboard="false" >

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">เพิ่มโปรแกรม</h4>
                </div>
                <div class="modal-body" width="100%" >
                    
                    <div id="form-add-place">
                        <form role="form" class="form-horizontal" data-sel-now=""  >

                                <input type="hidden" name="id" value="" />
                            
                                <div class="row">
                                    <div class="col-sm-12 form-group">
                                        <label class="control-label col-sm-3" for="name">ชื่อโปรแกรม:</label>
                                        <div class="col-sm-9">
                                            <input type="text"  class="form-control" 
                                                    id="name" 
                                                    name="name"   />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <label class="control-label col-sm-3" for="path">path:</label>
                                        <div class="col-sm-9">
                                            <input type="text"   class="form-control" 
                                                    id="path" 
                                                    name="path"  />
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>    
                    
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" onclick="savePlaceList()" value="บันทึกข้อมูล">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  


<div class="modal fade modal-wide" 
    id="modal-form-add-menu" 
    tabindex="-1" role="dialog" 
    aria-labelledby="myModalLabel" 
    aria-hidden="true"
    data-backdrop="static" data-keyboard="false" >

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">เพิ่มเมนู</h4>
                </div>
                <div class="modal-body" width="100%" >

                    <div id="form-add-menu">
                        <form role="form" class="form-horizontal" data-sel-now="" data-state-form="" data-save-runno="0"  >

                                <input type="hidden" name="id" value="" />
                                <input type="hidden" name="ref_id" id="ref_id" value="" />
                            
                                <div class="row">
                                    <div class="col-sm-12 form-group">
                                        <label class="control-label col-sm-3" for="name">ชื่อเมนู:</label>
                                        <div class="col-sm-9">
                                            <input type="text"  class="form-control" 
                                                    id="name" 
                                                    name="name"   />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <label class="control-label col-sm-3" for="path">กระบวณการ:</label>
                                        <div class="col-sm-9">
                                            <select id="process_id" name="process_id" class="form-control">
                                                <?php echo $addition['optionMenuProcess'];?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <label class="control-label col-sm-3" for="name">link:</label>
                                        <div class="col-sm-9">
                                            <input type="text"  class="form-control" 
                                                    id="link" 
                                                    name="link"   />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <label class="control-label col-sm-3" for="name">คำอธิบายเมนู:</label>
                                        <div class="col-sm-9">
                                            <input type="text"  class="form-control" 
                                                    id="menu_desc" 
                                                    name="menu_desc"   />
                                        </div>
                                    </div>
                                    

                                </div>
                            </form>
                    </div>    
                    
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" onclick="addMenuToObj()" value="บันทึกข้อมูล">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade modal-wide" 
    id="modal-form-move-menu" 
    tabindex="-1" role="dialog" 
    aria-labelledby="myModalLabel" 
    aria-hidden="true"
   
    data-backdrop="static" data-keyboard="false" >

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ย้ายเมนู</h4>
                </div>
                <div class="modal-body" width="100%" >

                    <div id="form-move-menu">
                        รอสักครู่..
                    </div>    
                    
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" onclick="moveMenu()" value="ย้ายเมนู">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  