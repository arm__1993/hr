<?php

namespace app\modules\gitreport\models;

/**
 * This is the ActiveQuery class for [[HbsoDev]].
 *
 * @see HbsoDev
 */
class HbsoDevQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return HbsoDev[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HbsoDev|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
