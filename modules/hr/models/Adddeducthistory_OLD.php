<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ADD_DEDUCT_HISTORY".
 *
 * @property integer $ADD_DEDUCT_THIS_MONTH_ID
 * @property string $WAGE_THIS_MONTH_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_EMP_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_PAY_DATE
 * @property string $ADD_DEDUCT_THIS_MONTH_TMP_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_TMP_NAME
 * @property string $ADD_DEDUCT_THIS_MONTH_DETAIL_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_DETAIL
 * @property string $ADD_DEDUCT_THIS_MONTH_AMOUNT
 * @property string $ADD_DEDUCT_THIS_MONTH_TYPE
 * @property string $ADD_DEDUCT_THIS_MONTH_STATUS
 * @property string $ADD_DEDUCT_THIS_MONTH_SLIP_STATUS
 * @property string $ADD_DEDUCT_THIS_MONTH_CREATE_DATE
 * @property string $ADD_DEDUCT_THIS_MONTH_CREATE_BY
 * @property string $ADD_DEDUCT_THIS_MONTH_UPDATE_DATE
 * @property string $ADD_DEDUCT_THIS_MONTH_UPDATE_BY
 * @property integer $is_endofmonth
 * @property integer $cal_times
 * @property string $cal_date
 * @property integer $wage_calculate_id
 */
class Adddeducthistory_OLD extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ADD_DEDUCT_HISTORY';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WAGE_THIS_MONTH_ID', 'ADD_DEDUCT_THIS_MONTH_EMP_ID', 'ADD_DEDUCT_THIS_MONTH_PAY_DATE', 'ADD_DEDUCT_THIS_MONTH_TMP_ID', 'ADD_DEDUCT_THIS_MONTH_TMP_NAME', 'ADD_DEDUCT_THIS_MONTH_DETAIL_ID', 'ADD_DEDUCT_THIS_MONTH_DETAIL', 'ADD_DEDUCT_THIS_MONTH_AMOUNT', 'ADD_DEDUCT_THIS_MONTH_TYPE', 'ADD_DEDUCT_THIS_MONTH_STATUS', 'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS', 'ADD_DEDUCT_THIS_MONTH_CREATE_DATE', 'ADD_DEDUCT_THIS_MONTH_CREATE_BY', 'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE', 'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'], 'required'],
            [['ADD_DEDUCT_THIS_MONTH_AMOUNT'], 'number'],
            [['ADD_DEDUCT_THIS_MONTH_CREATE_DATE', 'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE', 'cal_date'], 'safe'],
            [['is_endofmonth', 'cal_times','wage_calculate_id'], 'integer'],
            [['WAGE_THIS_MONTH_ID', 'ADD_DEDUCT_THIS_MONTH_EMP_ID', 'ADD_DEDUCT_THIS_MONTH_PAY_DATE', 'ADD_DEDUCT_THIS_MONTH_TMP_ID', 'ADD_DEDUCT_THIS_MONTH_DETAIL_ID'], 'string', 'max' => 100],
            [['ADD_DEDUCT_THIS_MONTH_TMP_NAME', 'ADD_DEDUCT_THIS_MONTH_DETAIL', 'ADD_DEDUCT_THIS_MONTH_CREATE_BY', 'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'], 'string', 'max' => 250],
            [['ADD_DEDUCT_THIS_MONTH_TYPE', 'ADD_DEDUCT_THIS_MONTH_STATUS', 'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ADD_DEDUCT_THIS_MONTH_ID' => 'Add  Deduct  This  Month  ID',
            'WAGE_THIS_MONTH_ID' => 'Wage  This  Month  ID',
            'ADD_DEDUCT_THIS_MONTH_EMP_ID' => 'Add  Deduct  This  Month  Emp  ID',
            'ADD_DEDUCT_THIS_MONTH_PAY_DATE' => 'Add  Deduct  This  Month  Pay  Date',
            'ADD_DEDUCT_THIS_MONTH_TMP_ID' => 'Add  Deduct  This  Month  Tmp  ID',
            'ADD_DEDUCT_THIS_MONTH_TMP_NAME' => 'Add  Deduct  This  Month  Tmp  Name',
            'ADD_DEDUCT_THIS_MONTH_DETAIL_ID' => 'Add  Deduct  This  Month  Detail  ID',
            'ADD_DEDUCT_THIS_MONTH_DETAIL' => 'Add  Deduct  This  Month  Detail',
            'ADD_DEDUCT_THIS_MONTH_AMOUNT' => 'Add  Deduct  This  Month  Amount',
            'ADD_DEDUCT_THIS_MONTH_TYPE' => 'Add  Deduct  This  Month  Type',
            'ADD_DEDUCT_THIS_MONTH_STATUS' => 'Add  Deduct  This  Month  Status',
            'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS' => 'Add  Deduct  This  Month  Slip  Status',
            'ADD_DEDUCT_THIS_MONTH_CREATE_DATE' => 'Add  Deduct  This  Month  Create  Date',
            'ADD_DEDUCT_THIS_MONTH_CREATE_BY' => 'Add  Deduct  This  Month  Create  By',
            'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE' => 'Add  Deduct  This  Month  Update  Date',
            'ADD_DEDUCT_THIS_MONTH_UPDATE_BY' => 'Add  Deduct  This  Month  Update  By',
            'is_endofmonth' => 'Is Endofmonth',
            'cal_times' => 'Cal Times',
            'cal_date' => 'Cal Date',
            'wage_calculate_id'=> 'Wage Calculate Id',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'total_employee']);
    }
}
