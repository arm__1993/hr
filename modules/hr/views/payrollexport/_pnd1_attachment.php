<?php
use yii\helpers\Html;
use app\api\DateTime;
use app\api\Utility;
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$dataitem = $data;
$sumpaidmount = 0;
$sumpaidtax =0;
$company_idcard = str_split($datahead[0]['tax_regis_code']);
?>
    <table width='120%'>
        <tr>
            <td width='20%'>
                <table>
                    <tr>
                        <td style='font-size:9pt'>
                            ใบแนบ
                        </td>
                        <td style='font-size:15pt'>
                            ภ.ง.ด.1
                        </td>
                    </tr>
                </table>
            </td>
            <td width='80%'>
                <table>
                    <tr>
                        <td style='font-size:7pt;'><b>เลขประจำตัวผู้เสียภาษีอากร </b>(ของผู้มีหน้าที่หักภาษี ณ ที่จ่าย)</td>
                        <td>
                            <table border='1' style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                                <tr>
                                    <td><?php echo $company_idcard[0]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $company_idcard[1]; ?></td>
                                    <td style="border-left:0px;"><?php echo $company_idcard[2]; ?></td>
                                    <td style="border-left:0px;"><?php echo $company_idcard[3]; ?></td>
                                    <td style="border-left:0px;"><?php echo $company_idcard[4]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $company_idcard[5]; ?></td>
                                    <td style="border-left:0px;"><?php echo $company_idcard[6]; ?></td>
                                    <td style="border-left:0px;"><?php echo $company_idcard[7]; ?></td>
                                    <td style="border-left:0px;"><?php echo $company_idcard[8]; ?></td>
                                    <td><?php echo $company_idcard[9]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $company_idcard[10]; ?></td>
                                    <td style="border-left:0px;"><?php echo $company_idcard[11]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $company_idcard[12]; ?></td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style='width:100%' border='1'>
        <div style='float:left;width:75% background-color:#cdd1dd; border: 1px solid #000;border-top-left-radius: 8px;border-top-right-radius: 8px;border-bottom-left-radius: 8px;border-bottom-right-radius: 8px;'>
            <table style='font-size:8pt;'>
                <tr>
                    <td>
                        (ให้แยกกรอกรายการในใบแนบนี้ตามเงินได้แต่ละประเภท โดยใส่เครื่องหมาย “” ลงใน “ ” หน้าข้อความแล้วแต่กรณี เพียงข้อเดียว)
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style='width:100%;'>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top"><b>ประเภทเงินได้</b></td>
                                            <td valign="top">
                                                <input type="checkbox">
                                            </td>
                                            <td valign="top">(1)</td>
                                            <td valign="top"> เงินได้ตาม <b>มาตรา 40 (1)</b> เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"></td>
                                            <td valign="top">
                                                <input type="checkbox">
                                            </td>
                                            <td valign="top">(2)</td>
                                            <td valign="top"> เงินได้ตาม <b>มาตรา 40 (1)</b> เงินเดือน ค่าจ้าง ฯลฯ
                                                <br> กรณีได้รับอนุมัติจากกรมสรรพากรให้หักอัตรา
                                                <b>ร้อยละ 3</b>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <input type="checkbox">
                                            </td>
                                            <td valign="top">(3)</td>
                                            <td valign="top">เงินได้ตาม <b>มาตรา 40 (1) (2)</b> กรณีนายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงาน</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <input type="checkbox">
                                            </td>
                                            <td valign="top">(4)</td>
                                            <td valign="top"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <input type="checkbox">
                                            </td>
                                            <td valign="top">(5)</td>
                                            <td valign="top"> เงินได้ตาม <b>มาตรา 40 (2)</b>กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย</b>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
        </div>
        <div style='width:23%;float:right; border: 0px solid #000;'>
            <div align='right'>
                <table width='100%' >
                    <tr>
                        <td style='font-size:7pt;text-align:right'><b>สาขาที่</b></td>
                        <td valign='right' style='width:30%'>
                            <table style='border: 1px solid #ccc;'cellpadding='4' rowpadding='2' cellspacing='0'>
                                <tr>
                                    <td style='font-size:7pt'>0</td>
                                    <td style='font-size:7pt'>0</td>
                                    <td style='font-size:7pt'>0</td>
                                    <td style='font-size:7pt'>5</td>
                                    <td style='font-size:7pt'>6</td>
                                </tr>
                            </table>
                        </td>                        
                    </tr>
                </table>
            </div>
            <br>
            <table style='valign:bottom' height='100%'>
                <tr>
                    <td style='font-size:8pt;padding-bottom:-15px;'>
                        <center><?php echo $count_page; ?></center>
                    </td>
                    <td style='font-size:8pt;padding-bottom:-15px'>
                        <center><?php echo $all_page; ?></center>                        
                    </td>
                </tr>
                <tr>
                    <td style='font-size:8pt'>
                       แผ่นที่.....................
                    </td>
                    <td style='font-size:8pt'>
                        ในจำนวน..................แผ่น                    
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table border='1' style='margin-top:3px;width:100%;font-size:9pt;border-collapse: collapse;margin-top:3px;' cellpadding='1' rowpadding='2' cellspacing='0'>
        <tr>
            <td rowspan='2' align='center'>
                ลำดับที่
            </td>
            <td align='center'>
                เลขประจำ ตัวผู้เสียภาษีอากร (ของผู้มีเงินได้)
            </td>
            <td colspan='2' style='width:15%' align='center'>
                รายละเอียดเกี่ยวกับการจ่ายเงิน
            </td>
            <td rowspan='2' align='center'>
                จำนวนเงินภาษีที่หัก และนำส่งในครั้งนี้
            </td>
            <td rowspan='2' align='center'>
                <svg width='20' height="40">
          <text style='font-size:9pt;font-color:#15205b' transform='rotate(90,0,0)'>เงื่อนไข *</text>
        </svg>
            </td>
        </tr>
        <tr align='center'>
            <td align='center'>
                ชื่อผู้มีเงินได้(ให้ระบุชัดเจนว่าเป็น นาย นาง นางสาว หรือยศ)
            </td>
            <td align='center'>
                วัน เดือน ปี ที่จ่าย
            </td>
            <td align='center'>
                จำนวนเงินได้ที่จ่ายในครั้งนี้
            </td>
        </tr>
        <?php for($i=0;$i<count($dataitem);$i++){ 
            $post_code = str_split($dataitem[$i][emp_idcard])
        ?>

        <tr>
            <td>
                <?php print($i); ?>
            </td>
            <td>
                <table border='0' width='100%' style='font-size:9pt'>
                    <tr>
                        <td colspan='2'>
                            <table border='1' style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='80%'>
                                <tr>
                                    <td><?php echo $post_code[0]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $post_code[1]; ?></td>
                                    <td style="border-left:0px;"><?php echo $post_code[2]; ?></td>
                                    <td style="border-left:0px;"><?php echo $post_code[3]; ?></td>
                                    <td style="border-left:0px;"><?php echo $post_code[4]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $post_code[5]; ?></td>
                                    <td style="border-left:0px;"><?php echo $post_code[6]; ?></td>
                                    <td style="border-left:0px;"><?php echo $post_code[7]; ?></td>
                                    <td style="border-left:0px;"><?php echo $post_code[8]; ?></td>
                                    <td><?php echo $post_code[9]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $post_code[10]; ?></td>
                                    <td style="border-left:0px;"><?php echo $post_code[1]; ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $post_code[12]; ?></td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width='50%' style='padding-bottom:-15px' align='center'><?php echo $dataitem[$i]['emp_firstname']; ?></td>
                        <td width='50%' style='padding-bottom:-15px' align='center'><?php echo $dataitem[$i]['emp_lastname']; ?></td>
                    </tr>
                    <tr>
                        <td width='50%' sytle='valign:left'> ชื่อ.............................................</td>
                        <td width='50%' sytle='valign:left'>ชื่อสกุล...........................................</td>
                    </tr>
                </table>

            </td>
            <td>
                <br>
                <center style='padding-bottom:-15px;fint-size:9pt;'><?php echo $date; ?></center>
                ............................................
            </td>
            <td style='text-align:right;fint-size:9pt;'>
                <br>
                <p style='padding-bottom:-15px;'><?php echo number_format($dataitem[$i]['paid_amount'],2,".",","); ?></p>
                ....................................................
            </td>
            <td style='text-align:right;fint-size:9pt;'>
                <br><p style='padding-bottom:-15px;'><?php echo number_format($dataitem[$i]['paid_tax'],2,".",","); ?></p>
                .......................................................................
            </td>
            <td>
                <br>
                <br> .........
            </td>
        </tr>
        <?php  $sumpaidmount += $dataitem[$i]['paid_amount'];
                $sumpaidtax += $dataitem[$i]['paid_tax'];
        }?>
        <tr>
            <td colspan='3' align='right' style='padding-bottom:-15px;border-bottom:0px'>
            </td>
            <td style='padding-bottom:-15px;border-bottom:0px' align='center'><?php echo $sumpaidmount; ?></td>
            <td style='padding-bottom:-15px;border-bottom:0px' align='center'><?php echo $sumpaidtax; ?></td>
        </tr>
        <tr>
            <td colspan='3' align='right' style='border-top:0px'>
                รวมยอดเงินได้และภาษีที่นำส่ง (นำ ไปรวมกับใบแนบ ภ.ง.ด.1 แผ่นอื่น (ถ้ามี))
            </td>
            <td style='border-top:0px'>...........................................</td>
            <td style='border-top:0px'>.......................................................................
                <td>
        </tr>
        <tr>
            <td colspan='3' align='left'>
                <table>
                    <tr>
                        <td colspan='2'>(ให้กรอกลำดับที่ต่อเนื่องกันไปทุกแผ่นตามเงินได้แต่ละประเภท)</td>
                    </tr>
                    <tr>
                        <td><b>หมายเหตุ * </b></td>
                        <td> เงื่อนไขการหักภาษีให้กรอกดังนี้ </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td> หัก ณ ที่จ่าย กรอก 1 </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td> ออกให้ตลอดไป กรอก 2 </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td> ออกให้ครั้งเดียว กรอก 3 </td>
                    </tr>
                </table>
            </td>
            <td colspan='3' align='center'>
                <table style='border: 1px solid #fff'>
                    <tr>
                        <td rowspan='4'><img height="60" width="60" src="<?php echo $imghr; ?>/stamp-icon.png" class="img-circle"></td>
                        <td> ลงชื่อ..................................................................................ผู้จ่ายเงิน </td>
                    </tr>
                    <tr>
                        <td> (................................................................................) </td>
                    </tr>
                    <tr>
                        <td>ตำแหน่ง.............................................................................</td>
                    </tr>
                    <tr>
                        <td>ยื่นวันที่...........เดือน..................................พ.ศ. ................... </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
    <hr>
<div style='width:45%;float:right;text-align:right;font-size:9pt;'><p>พิมพ์ <?php echo DateTime::mappingMonthContraction(date('m')).(date('Y')+543); ?></p></div>
<div style='width:45%;float:left;text-align:lefts;font-size:9pt;margin-top:-5px'><p>สอบถามข้อมูลเพิ่มเติมได้ที่ศูนย์สารนิเทศสรรพากร โทร.<img style='padding-bottom:-5px' height='20' src="<?php echo $imghr; ?>/210360_WHT1_kor.pdf.png" class="img-circle"> 1161</p></div>