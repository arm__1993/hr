<?php

namespace app\modules\gitreport\models;

use Yii;

/**
 * This is the model class for table "hbso_milestone".
 *
 * @property integer $id
 * @property string $milestone_name
 * @property integer $project_id
 * @property string $milestone_start_date
 * @property string $milestone_end_date
 * @property integer $status_active
 * @property string $create_by
 * @property string $create_datetime
 */
class HbsoMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hbso_milestone';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbGit_Report');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'status_active'], 'integer'],
            [['milestone_start_date', 'milestone_end_date', 'create_datetime'], 'safe'],
            [['milestone_name'], 'string', 'max' => 250],
            [['create_by'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'milestone_name' => 'Milestone Name',
            'project_id' => 'Project ID',
            'milestone_start_date' => 'Milestone Start Date',
            'milestone_end_date' => 'Milestone End Date',
            'status_active' => 'Status Active',
            'create_by' => 'Create By',
            'create_datetime' => 'Create Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return HbsoMilestoneQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HbsoMilestoneQuery(get_called_class());
    }



}
