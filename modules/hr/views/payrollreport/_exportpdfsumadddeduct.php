<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;
$modelCommpaneyAdddeduct = $session->get('modelCommpaneyAdddeduct');
$monthselect = $session->get('monthselect');
$arrIdtemp = $session->get('arrIdtemp');
$modelWorkingcompany = $session->get('modelWorkingcompany');


?>
<?php
    $toTalrecord =  count($modelWorkingcompany);
    $toTalpage = ceil($toTalrecord / 8);
    echo $toTalpage;
     print_r($modelWorkingcompany);
?>
<style>
    .container{
        font-family: "THSarabun";
        font-size: 16px;
    }
    p {
        font-family: "THSarabun";
        font-size: 16px;
    }
    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    table {
        border-collapse: collapse;
    }
    th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }
    td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;
    }
    tr.odd { background: #f3f3f3; }
    tr.even { background: #FFF; }


</style>
    <h3 style="text-align:center;">รายละเอียดรายงานเงินเดือน</h3>

<div class="row">
    <div class="row">
            </div>
        <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-6">
        <h3><?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                //print_r($getMonth);
                $monthGetReport1 = $getMonth['monthresult'];
                $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
                $yearGetReport1 = $getMonth['year'];
        ?>รายงานสรุปผลเงินเดือน (รายการเพิ่ม)  <?php echo $resultMonthReport1; echo '&nbsp;&nbsp;'.$yearGetReport1; ?></h3>
        </div>
    </div>
    <?php //if($querySalaryTotalThisMonth){
    //  echo "<pre>";
    //   print_r($reportSalaryTotal);?>
   
    
        <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_wrapper" width="80%">
            <thead>
                <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" width="30%">
                            รายการเพิ่ม
                        </th>
                        <?php //foreach($modelWorkingcompany as $key=>$value) {
                           for($i=0;$i<8;$i++)
                           {
                        ?>
                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" width="10%">
                            <?php echo $modelWorkingcompany[$i]['short_name'];?>
                        </th>
                        <?php }?>
                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" width="10%">
                            รวม
                        </th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($arrIdtemp as $key1=>$value){
                    $somrow=[];
            ?>
                <tr>
                    <td><?php echo $value['ADD_DEDUCT_TEMPLATE_NAME'];?></td>
                    <?php //foreach($modelWorkingcompany as $key2=>$values){
                        for($j=0;$j<8;$j++)
                        {
                    ?>
                    <td align="right"><?php 

                    echo $i.','.$j;
                           /* $setZero="0.00";
                            foreach($modelCommpaneyAdddeduct AS $key => $valueResult){
                                switch ($valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] AND $valueResult['WAGE_WORKING_COMPANY']) {
                                    case $valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID']==$value['ADD_DEDUCT_TEMPLATE_ID']&&$valueResult['WAGE_WORKING_COMPANY']==$modelWorkingcompany[$j]['id'];
                                        // echo "//////".$valueResult['SUM_ADD_DEDUCT_HIS']."//////";
                                        $setZero=$valueResult['SUM_ADD_DEDUCT_HIS'];
                                        break;
                                }
                            
                            }
                                echo  Helper::displayDecimal($setZero);
                                $somrow[]=$setZero;

                                */

                    ?></td>
                    <?php } ?>
                    <td><?php echo  Helper::displayDecimal(array_sum($somrow)) ;?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
</div>
