<?php

namespace app\modules\hr\controllers;

use app\modules\hr\models\ConfigMappingSso;

use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Session;
use app\modules\hr\controllers\MasterController;

class ConfigssomapingController extends MasterController
{
    public $layout = 'hrlayout';

    // public $idcardLogin;

    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function actionIndex()
    {

        $modelSearch = new ConfigMappingSso();
        $modelProvider = $modelSearch->search(Yii::$app->request->queryParams);


        return $this->render('index',[
            'modelProvider'=>$modelProvider,
            'modelSearch' => $modelSearch
        ]);
    }

    public function actionSaveconfigmapping(){
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $login= $this->idcardLogin;
            if ($post['hide_activityconfigmaping'] != '') {
                $model = ConfigMappingSso::findOne($post['hide_activityconfigmaping']);
                $model->id_sso=$post['id_sso'];
                $model->namestatussso=$post['namestatussso'];
                $model->namestatushr=$post['namestatushr'];
                $model->status=$post['status'];
                $model->createby=$login;
                $model->createdate=new Expression('NOW()');
                $model->updateby=$login;
                $model->updatedate=new Expression('NOW()');
                $model->save();
                if($model->save() !== false){
                    echo true;
                }else{
                    $errors = $model->errors;
                    echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
                }
            }else{
                $model = new ConfigMappingSso();
                $model->id_sso=$post['id_sso'];
                $model->namestatussso=$post['namestatussso'];
                $model->namestatushr=$post['namestatushr'];
                $model->status=$post['status'];
                $model->createby=$login;
                $model->createdate=new Expression('NOW()');
                $model->updateby=$login;
                $model->updatedate=new Expression('NOW()');
                //$model->tax_section_id=$post['tax_section_id'];
                $model->save();
                if($model->save() !== false){
                    echo true;
                }else{
                    $errors = $model->errors;
                    echo "model can't validater <pre>"; print_r($errors); echo "</pre>";

                }
            }
        }
    }


    public function actionUpdateconfigmapping(){
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            if (($model = ConfigMappingSso::findOne($id)) !== null) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    public function actionDeleteconfigmapsso(){
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = ConfigMappingSso::findOne(['id'=>$post['id']]);
            $model->status = Yii::$app->params['DELETE_STATUS'];
            $model->save();
            if($model->save() !== false){
                echo true;
            }else{
                $errors = $model->errors;
                echo "model can't validater <pre>"; print_r($errors); echo "</pre>";
            }
        }
    }

}
