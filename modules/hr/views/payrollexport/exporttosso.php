<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiSSOExport;
use yii\data\ActiveDataProvider;
$company = ApiSSOExport::select_company();
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/sso/payrollecport.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                 <li>ส่งออกข้อมูล</li>
                <li class="active">ข้อมูลส่งประกันสังคม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
        <div class="box-header with-border">
                 <h3 class="box-title">ส่งออกข้อมูลประกันสังคม</h3>
                </div>
                <div>
                    <h3></h3>
                </div>
                <!--onsubmit="return exportText();"-->
            <form  id='exportcompany'  data-toggle="validator" method="post">     
                <div class="row">
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-4 control-label " align="right"><h4>เลือกรูปแบบข้อมูล :</h4></label>
                        <div class="col-sm-4 control-label" >
                            <select class="form-control" name="type_data" id="selecttype" required>
                                <option value="">เลือกรูปแบบข้อมูล</option>
                                <option value="1">แบบที่ 1 (135 ตัวอักษร)</option>
                                <!--<option value="2">แบบที่ 2 (126 ตัวอักษร)</option>-->
                            </select>
                        </div>
                    </div>
                </div> 
                               
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                <div class="row">  
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-4 control-label " align="right"><h4>เลือกบริษัท :</h4></label>
                        <div class="col-sm-4 control-label">
                                <select class="form-control" name="selectworking" id="selectworking" required>
                                 <option value="">เลือกบริษัท</option>
                                <?php 

                                    $working = ApiHr::getWorking_company();
                                    foreach ($working as  $value) {
                                            echo '<option value="' . $value['id']. '">' . $value['name'] . '</option>';
                            } ?>
                                  <!-- <option value="">เลือกบริษัท</option>
                                  <option value="1">บริษัท อีซูซุเชียงรายบริการ(2002)จำกัด</option>
                                  <option value="2">บริษัท อีซูซุเชียงราย(2002)จำกัด</option>
                                  <option value="3">บริษัท ไอซีลิสชิ่ง </option> -->
                                </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-4 control-label " align="right"><h4>เลือกเดือน :</h4></label>
                   
                        <div  class="col-sm-2 control-label">
                            <select class="form-control" name="selectmonth" value="<?php date('m'); ?>" id="selectmonth" required>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                                $sel = ($i == date('m')) ? 'selected="selected"' : '';
                                echo '<option value="' . $i . '" >' . \app\api\DateTime::convertMonth($i) . '</option>';
                            }
                            ?>
                            </select>
                        </div> 
                        <div class="col-sm-4 control-label">                            
                            <div class="btn-group">
                                <select class="form-control" name="selectyear" id="selectyear" value="<?php date(''); ?>" required>
                                <?php
                                $dateSelect = ApiSSOExport::selectMinus5Year();
                                foreach ($dateSelect as $key => $value) {
                                    echo '<option value="' . $value. '">' . $value . '</option>';
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                </div>
                <div class='row'></div>
                <div class="row">  
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-4 control-label " align="right"><h4>วันที่จ่ายประกันสังคม :</h4></label>
                        <div class="col-sm-4 control-label">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" readonly="readonly" name="sso_date" data-required="true" id="sso_date" value="<?php echo $today;?>" class="form-control" placeholder="dd/mm/yyyy">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-sm-11 control-label form-group"> 
                    
                        <div class="form-group" align="center">
                            <button type='button' id='btnexport' class="btn btn-primary">Download</button>

                            <!-- <button type="reset" class="btn btn-danger">Cancle</button>   --> 
                       </div>
                       
                    </div> 
                </div>
            </form>
        </div>
        
    </div>
    <!-- /.box -->
</section><!-- /.content -->