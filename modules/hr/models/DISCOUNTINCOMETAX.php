<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "DISCOUNT_INCOME_TAX".
 *
 * @property string $no
 * @property string $idcard
 * @property string $ชื่อ-สกุล
 * @property string $บริษัท
 * @property string $ใช้ลดหย่อนได้
 * @property string $ลดหย่อนส่วนตัว
 * @property string $ลดหย่อนคู่สมรส
 * @property string $บิดามารดา
 * @property string $เบี้ยประกันชีวิตบิดามารดา
 * @property string $ลดหย่อนบุตร
 * @property string $เบี้ยประกันชีวิต
 * @property string $ดอกเบี้ยกู้ยืม
 * @property string $ประกันสังคม
 * @property string $ประกันสังคมต่อเดือน
 * @property string $total_discount
 * @property string $หักจ่าย  ม.ค.
 * @property string $Jan
 * @property string $หักจ่าย ก.พ.
 * @property string $Feb
 */
class DISCOUNTINCOMETAX extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'DISCOUNT_INCOME_TAX';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no', 'idcard', 'ชื่อ-สกุล', 'บริษัท', 'ลดหย่อนส่วนตัว', 'ลดหย่อนคู่สมรส', 'บิดามารดา', 'เบี้ยประกันชีวิตบิดามารดา', 'ลดหย่อนบุตร', 'เบี้ยประกันชีวิต', 'ดอกเบี้ยกู้ยืม', 'ประกันสังคม', 'ประกันสังคมต่อเดือน', 'total_discount'], 'string', 'max' => 255],
            [['ใช้ลดหย่อนได้', 'หักจ่าย  ม.ค.', 'Jan', 'หักจ่าย ก.พ.', 'Feb'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'idcard' => 'Idcard',
            'ชื่อ-สกุล' => 'ชื่อ สกุล',
            'บริษัท' => 'บริษัท',
            'ใช้ลดหย่อนได้' => 'ใช้ลดหย่อนได้',
            'ลดหย่อนส่วนตัว' => 'ลดหย่อนส่วนตัว',
            'ลดหย่อนคู่สมรส' => 'ลดหย่อนคู่สมรส',
            'บิดามารดา' => 'บิดามารดา',
            'เบี้ยประกันชีวิตบิดามารดา' => 'เบี้ยประกันชีวิตบิดามารดา',
            'ลดหย่อนบุตร' => 'ลดหย่อนบุตร',
            'เบี้ยประกันชีวิต' => 'เบี้ยประกันชีวิต',
            'ดอกเบี้ยกู้ยืม' => 'ดอกเบี้ยกู้ยืม',
            'ประกันสังคม' => 'ประกันสังคม',
            'ประกันสังคมต่อเดือน' => 'ประกันสังคมต่อเดือน',
            'total_discount' => 'Total Discount',
            'หักจ่าย  ม.ค.' => 'หักจ่าย  ม ค',
            'Jan' => 'Jan',
            'หักจ่าย ก.พ.' => 'หักจ่าย ก พ',
            'Feb' => 'Feb',
        ];
    }
}
