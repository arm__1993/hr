-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 20, 2017 at 11:46 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `payroll_config_template`
--

CREATE TABLE `payroll_config_template` (
  `id` tinyint(3) NOT NULL COMMENT 'รหัสลำดับ',
  `ot_template_id` int(11) NOT NULL COMMENT 'รหัสประเภทการจ่ายค่าล่วงเวลา',
  `sso_template_id` int(11) NOT NULL COMMENT 'รหัสประเภทการหักเงินประกันสังคม',
  `taxincome_template_id` int(11) NOT NULL COMMENT 'รหัสประเภทการหักเงินภาษี ภงด. 91',
  `taxpnd_template_id` int(11) NOT NULL COMMENT 'รหัสประเภทการหักเงินภาษี ภงด. 1',
  `taxtavi_template_id` int(11) NOT NULL COMMENT 'รหัสประเภทการหักเงินภาษี หัก ณ ที่จ่าย',
  `guarantee_template_id` int(11) NOT NULL COMMENT 'รหัสประเภทการหักเงินค้ำประกัน',
  `createby_user` char(13) DEFAULT NULL COMMENT 'เลขประจำตัวผู้สร้าง',
  `create_datetime` datetime DEFAULT NULL COMMENT 'สร้างรายการเมื่อวันที่เวลา',
  `updateby_user` char(13) DEFAULT NULL COMMENT 'เลขประจำตัวผู้อัพเดท',
  `update_datetime` datetime DEFAULT NULL COMMENT 'อัพเดทรายการเมื่อวันที่เวลา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payroll_config_template`
--

INSERT INTO `payroll_config_template` (`id`, `ot_template_id`, `sso_template_id`, `taxincome_template_id`, `taxpnd_template_id`, `taxtavi_template_id`, `guarantee_template_id`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, 23, 22, 34, 41, 43, 21, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payroll_config_template`
--
ALTER TABLE `payroll_config_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ot_template_id` (`ot_template_id`),
  ADD KEY `sso_template_id` (`sso_template_id`),
  ADD KEY `tax_template_id` (`taxincome_template_id`),
  ADD KEY `taxpnd_template_id` (`taxpnd_template_id`),
  ADD KEY `guarantee_template_id` (`guarantee_template_id`),
  ADD KEY `taxtavi_template_id` (`taxtavi_template_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payroll_config_template`
--
ALTER TABLE `payroll_config_template`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'รหัสลำดับ', AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
