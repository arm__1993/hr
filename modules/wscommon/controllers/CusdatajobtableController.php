<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Maincusdatajobtable;
use yii\helpers\ArrayHelper;


class CusdatajobtableController extends ActiveController
{


   	public $modelClass = 'app\modules\wscommon\models\Maincusdatajobtable';

	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}

	public function actionSelectjobbytypeofcus()
    {
        //return 1;
        $typecus = \Yii::$app->request->get('typecus');
        $datajob = new Maincusdatajobtable();
        $listJob = $datajob->selectjobbytypeofcus($typecus);
        return $listJob;
    }
	public function actionSearchjobcusformobile()
    {
        //return 1;
        $Cusno = \Yii::$app->request->get('Cusno');
        $datajob = new Maincusdatajobtable();
        $listJob = $datajob->searchjobcusformobile($Cusno);
        return $listJob;
    }


}
