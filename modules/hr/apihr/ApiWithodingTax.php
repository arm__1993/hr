<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 12/27/2017 AD
 * Time: 09:46
 */

namespace app\modules\hr\apihr;


use app\api\DateTime;
use app\api\Helper;
use app\api\Utility;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\models\TaxWitholding;
use app\modules\hr\models\TaxWitholdingDetail;
use app\modules\hr\models\Wagethismonth;
use yii\helpers\ArrayHelper;
use Yii;
use yii\db\Expression;


class ApiWithodingTax
{


    public static function CalWitholdingTax($connection, $pay_date, $arrAddDeDuctEmp, $ACTION, $WHT_template,$arrAddDeductTemplate)
    {

        $arrItemMatch = null;
        $tax40_2 = 2;
        $tax40_8 = 9;
        $min_amount = 1000;

        $arrRecalIDCard = $ACTION['recal_idcard'];
        $arrGroupingWHT = $arrAllListWHTbyID = [];

        $arrItemRecal = $arrItemNormal = [];
        $arrAllListWHTbyID_Recal = $arrAllListWHTbyID_Normal = [];


        /** ---------------------------------------------------------------------------
         *  หารายการที่เป็น 40(2) และ 40(8)
         *  โดยประเภทรายได้เดียวกันให้จับมัดรวมกัน
         *----------------------------------------------------------------------------*/

        foreach ($arrAddDeDuctEmp as $idcard => $items) {
            if (isset($items[1])) {
                foreach ($items[1] as $item) {
                    if ($item['tax_section_id'] == $tax40_2 || $item['tax_section_id'] == $tax40_8) {  //หารายการที่เป็น 40(2) 40(8)
                        $arrGroupingWHT[$idcard][$item['ADD_DEDUCT_ID']] += $item['ADD_DEDUCT_DETAIL_AMOUNT'];

                        if ($ACTION['mode'] == 'recal' && in_array($idcard, $arrRecalIDCard)) {
                            $arrItemRecal[$idcard][] = $item;
                        } else {
                            $arrItemNormal[$idcard][] = $item;
                        }

                    }
                }
            }
        }


        /** ---------------------------------------------------------------------------
         *  เมื่อมัดรวมกันได้แล้วให้หาจำนวน ตามรายการที่มากกว่า 1000
         *  โดยประเภทรายได้เดียวกันให้จับมัดรวมกัน
         *----------------------------------------------------------------------------*/
        $arrDeductDetail = $arrSummaryWHTByIDCard = [];
        $_total_wht = $_total_deduct =  [];
        foreach ($arrGroupingWHT as $idcard => $templates) {
            foreach ($templates as $template_id => $t) {
                if ($t >= $min_amount) {
                    if ($ACTION['mode'] == 'recal' && in_array($idcard, $arrRecalIDCard)) {
                        $arrAllListWHTbyID_Recal[$idcard][$template_id] = $t;
                    } else {
                        $arrAllListWHTbyID_Normal[$idcard][$template_id] = $t;
                    }

                    $arrTemplate = $arrAddDeductTemplate[$template_id];
                    $tax_wht = Utility::CalculateWitholdingTax($arrTemplate['tax_rate'], $t);
                    $arrDeductDetail[] = [
                        'idcard' => $idcard,
                        'item_id' => $template_id,
                        'item_amount_beforetax'=>$t,
                        'item_name' => $WHT_template['ADD_DEDUCT_TEMPLATE_NAME']. ' ('.$arrTemplate['ADD_DEDUCT_TEMPLATE_NAME'] . ')',
                        'item_amount' => $tax_wht,
                        'item_detail_extra' => $arrTemplate['ADD_DEDUCT_TEMPLATE_NAME'],
                        'item_deduct_amt' => $t,
                    ];

                    $_total_wht[$idcard] += $tax_wht;
                    $_total_deduct[$idcard] += $t;
                    $arrSummaryWHTByIDCard[$idcard]  = [
                        'total_deduct' => $_total_deduct[$idcard],
                        'total_wht' => $_total_wht[$idcard],
                    ];


                }
            }
        }


        /** ---------------------------------------------------------------------------
         *  บันทึกรายการลงตาราง  tax_witholding เฉพาะรายการ หัก ณ ที่จ่าย
         *----------------------------------------------------------------------------*/
        $rec = self::InsertWithodingTax($connection, $arrDeductDetail, $pay_date, $WHT_template,$arrSummaryWHTByIDCard);


        /** ---------------------------------------------------------------------------
         *  บันทึกรายการลงตาราง  ADD_DEDUCT_THIS_MONTH  เป็นรายการหักใน slip เงินเดือน
         *----------------------------------------------------------------------------*/
        ApiSalary::AdjustAddDeductDetail($connection, $pay_date, $arrDeductDetail, $WHT_template);

        return $arrSummaryWHTByIDCard;
    }


    public static function InsertWithodingTax($connection, $arrDeductDetail, $pay_date, $WHT_template,$arrSummaryWHTByIDCard)
    {

        $arrEmpIDCard = ArrayHelper::getColumn($arrDeductDetail, 'idcard');
        $arrCircleYear = DateTime::findCircleMonth($pay_date);
        $years = $arrCircleYear['cur_year'];
        $arrWitholdingTax = self::LookupWitholdingTax($years, $arrEmpIDCard);
        $arrTemplateTax = ApiPayroll::getTemplateTax();
        $arrEmpSalaryData = ApiSalary::getAllEmpSalaryInfo();

        /** ---------------------------------------------------------------------------
         *  บันทึกรายการลงตาราง  tax_witholding เฉพาะรายการ หัก ณ ที่จ่าย ในตารางหัว
         *----------------------------------------------------------------------------*/

        $eff = 0; $arrData = null;
        $_table = 'tax_witholding';
        $detailColumns = TaxWitholding::getTableSchema()->getColumnNames();
        foreach ($arrEmpIDCard as $idcard) {
            $_cumulativeWhtAmt = $arrWitholdingTax[$idcard];
            $_thismonth_wht = 0;
            $_thismonth_amt = 0;
            $_arrSalaryData = $arrEmpSalaryData[$idcard];
            $company_id = $company_name = $position_code = $emp_salary_wage = null;

            if (isset($arrEmpSalaryData[$idcard])) {
                $_keys = array_keys($_arrSalaryData);
                $_poscode = $_keys[0];
                $_salarydata = $arrEmpSalaryData[$idcard][$_poscode];
                $company_id = $_salarydata['company_id'];
                $company_name = $_salarydata['company_name'];
                $position_code = $_salarydata['position_code'];
                $position_name = $_salarydata['position_name'];
                $emp_salary_wage = $_salarydata['emp_salary_wage'];
            }


            $arrData[] = [
                $detailColumns[0] => null,
                $detailColumns[1] => $idcard,
                $detailColumns[2] => $years,
                $detailColumns[3] => $pay_date,
                $detailColumns[4] => $company_id,
                $detailColumns[5] => $company_name,
                $detailColumns[6] => $position_code,
                $detailColumns[7] => $position_name,
                $detailColumns[8] => $emp_salary_wage,
                $detailColumns[9] => $arrSummaryWHTByIDCard[$idcard]['total_deduct'],
                $detailColumns[10] => ($arrSummaryWHTByIDCard[$idcard]['total_deduct'] + $emp_salary_wage),
                $detailColumns[11] => $arrSummaryWHTByIDCard[$idcard]['total_wht'],
                $detailColumns[12] => ($arrSummaryWHTByIDCard[$idcard]['total_wht'] + $_cumulativeWhtAmt),
                $detailColumns[13] => Yii::$app->session->get('idcard'),
                $detailColumns[14] => new Expression('NOW()'),
            ];
        }

        $eff += $connection->createCommand()->batchInsert($_table, $detailColumns, $arrData)->execute();


        /** ---------------------------------------------------------------------------
         *  บันทึกรายการลงตาราง  tax_witholding_detail เฉพาะรายการ หัก ณ ที่จ่าย ในตารางลูก
         *----------------------------------------------------------------------------*/
        //Query previous Insert
        $model = TaxWitholding::find()->where([
            'IN','emp_idcard',$arrEmpIDCard
        ])->andWhere([
            'years'=>$years,
            'date_pay'=>$pay_date
        ])->asArray()->all();
        $arrTaxWHT = ArrayHelper::index($model, 'emp_idcard');


        $arrData = null;
        $_table = 'tax_witholding_detail';
        $detailColumns = TaxWitholdingDetail::getTableSchema()->getColumnNames();
        $arrWHT_Head = [];

        foreach ($arrDeductDetail as $item) {

            $idcard = $item['idcard'];
            $WHT_data = $arrTaxWHT[$idcard];
            $tax_witholding_id = $WHT_data['id'];

            $item_id = $item['item_id'];
            $template = $arrTemplateTax[$item_id];

            $item_amt = $item['item_deduct_amt'];
            $item_rate = $template['tax_rate'];
            $item_id = $template['ADD_DEDUCT_TEMPLATE_ID'];
            $item_name = $template['ADD_DEDUCT_TEMPLATE_NAME'];
            $wht_amount =  $item['item_amount']; //Utility::CalculateWitholdingTax($item_rate, $item_amt);

            $tax_section_id = $template['tax_section_id'];
            $tax_section_name = $template['tax_section_name'];
            $accounting_code = $template['accounting_code_pk'];

            $_thismonth_wht += $wht_amount;
            $_thismonth_amt += $item_amt;

            $arrData[] = [
                $detailColumns[0] => null,
                $detailColumns[1] => $tax_witholding_id,
                $detailColumns[2] => $item_id,
                $detailColumns[3] => $item_name,
                $detailColumns[4] => $item_rate,
                $detailColumns[5] => $pay_date,
                $detailColumns[6] => $idcard,
                $detailColumns[7] => $item_amt,
                $detailColumns[8] => $wht_amount,
                $detailColumns[9] => $tax_section_id,
                $detailColumns[10] => $tax_section_name,
                $detailColumns[11] => $accounting_code,
            ];

        }


        $connection->createCommand()->batchInsert($_table, $detailColumns, $arrData)->execute();
        //TODO : calsummary and update
//        $_totalcumulativeWhtAmt = $_cumulativeWhtAmt + $_thismonth_wht; //total old wht and plus this month;
//
//        $modelHead = TaxWitholding::findOne($tax_witholding_id);
//        $modelHead->wht_month_amount = $_thismonth_wht;
//        $modelHead->wht_cumulative_amount = $_totalcumulativeWhtAmt;
//        $modelHead->total_adddeduct = $_thismonth_amt;
//        $modelHead->total_income = $emp_salary_wage + $_thismonth_amt;
//        $modelHead->save(false);




        return $eff;
    }




    //never use

    public static function LookupWitholdingTax($years, $idcard = null)
    {
        $arrList = [];
        if ($idcard == null) { //query all given year
            $data = TaxWitholding::find()->where([
                'years' => $years
            ])->orderBy(' id ASC')->asArray()->all();
        } else {

            $emp_idcard = (is_array($idcard)) ? $idcard : [$idcard];
            $strEmpID = Utility::JoinArrayToString($emp_idcard);
            $data = TaxWitholding::find()->where([
                'years' => $years
            ])->andWhere(" emp_idcard IN ($strEmpID)")->orderBy(' id ASC')->asArray()->all();

        }

        if (is_array($data) || is_object($data)) {
            foreach ($data as $item) {
                $arrList[$item['emp_idcard']] = $item['wht_cumulative_amount'];
            }
        }

        return $arrList;
    }


    public static function InsertWithodingTax2($connection, $arrItemMatch, $pay_date, $WHT_template,$arrRet)
    {

        $arrAllListWHTbyID = $arrRet['arrAllListWHTbyID'];
        $arrDeductDetail = [];
        if (count($arrItemMatch) > 0) {
            $arrEmpIDCard = array_keys($arrItemMatch);
            if (count($arrEmpIDCard) > 0) {
                $arrListRet = $arrWHTData = [];
                $arrCircleYear = DateTime::findCircleMonth($pay_date);
                $years = $arrCircleYear['cur_year'];
                $arrWitholdingTax = self::LookupWitholdingTax($years, $arrEmpIDCard);
                //$arrWageThisMonth = self::LookupWageThisMonth($arrEmpIDCard);
                $arrTemplateTax = ApiPayroll::getTemplateTax();
                $arrEmpSalaryData = ApiSalary::getAllEmpSalaryInfo();


                $eff = 0;
                foreach ($arrItemMatch as $idcard => $items) {

                    $_cumulativeWhtAmt = $arrWitholdingTax[$idcard];

                    $_thismonth_wht = 0;
                    $_thismonth_amt = 0;
                    $_arrSalaryData = $arrEmpSalaryData[$idcard];

                    $company_id = $company_name = $position_code = $emp_salary_wage = null;
                    if (isset($arrEmpSalaryData[$idcard])) {
                        $_keys = array_keys($_arrSalaryData);
                        $_poscode = $_keys[0];
                        $_salarydata = $arrEmpSalaryData[$idcard][$_poscode];
                        $company_id = $_salarydata['company_id'];
                        $company_name = $_salarydata['company_name'];
                        $position_code = $_salarydata['position_code'];
                        $position_name = $_salarydata['position_name'];
                        $emp_salary_wage = $_salarydata['emp_salary_wage'];
                    }

                    /** @var Withoding Tax Head */
                    $WhtHead = new TaxWitholding();
                    $WhtHead->id = null;
                    $WhtHead->emp_idcard = $idcard;
                    $WhtHead->years = $years;
                    $WhtHead->date_pay = $pay_date;
                    $WhtHead->company_id = $company_id;//$itemWageThisMonth['WAGE_WORKING_COMPANY'];
                    $WhtHead->company_name = $company_name;//$itemWageThisMonth['COMPANY_NAME'];
                    $WhtHead->position_code = $position_code;//$itemWageThisMonth['WAGE_POSITION_CODE'];
                    $WhtHead->position_name = $position_name;//$itemWageThisMonth['WAGE_POSITION_NAME'];
                    $WhtHead->salary_wage_monthly = $emp_salary_wage;//$itemWageThisMonth['WAGE_SALARY'];
                    //$WhtHead->total_adddeduct =0  ;//$itemWageThisMonth['WAGE_TOTAL_ADDITION'];
                    //$WhtHead->total_income = 0;//$itemWageThisMonth['WAGE_EARN_PLUS_ADD'];
                    //$WhtHead->wht_month_amount ='';
                    //$WhtHead->wht_cumulative_amount ='';
                    $WhtHead->createby_user = Yii::$app->session->get('idcard');
                    $WhtHead->create_datetime = new Expression('NOW()');
                    $WhtHead->isNewRecord = true;
                    $WhtHead->save(false);

                    //$tax_witholding_id = TaxWitholding::getDb()->lastInsertID();
                    $tax_witholding_id = $connection->getLastInsertID();

                    /** @var Withoding Tax detail */
                    $arrData = null;
                    $_table = 'tax_witholding_detail';
                    $detailColumns = TaxWitholdingDetail::getTableSchema()->getColumnNames();

                    foreach ($items as $item) {
                        $item_amt = $item['ADD_DEDUCT_DETAIL_AMOUNT'];
                        $item_rate = $item['tax_rate'];
                        $item_id = $item['ADD_DEDUCT_TEMPLATE_ID'];
                        $item_name = $item['ADD_DEDUCT_TEMPLATE_NAME'];
                        $wht_amount = Utility::CalculateWitholdingTax($item_rate, $item_amt);

                        $template = $arrTemplateTax[$item_id];
                        $tax_section_id = $template['tax_section_id'];
                        $tax_section_name = $template['tax_section_name'];
                        $accounting_code = $item['accounting_code_pk'];

                        $_thismonth_wht += $wht_amount;
                        $_thismonth_amt += $item_amt;


                        $arrData[] = [
                            $detailColumns[0] => null,
                            $detailColumns[1] => $tax_witholding_id,
                            $detailColumns[2] => $item_id,
                            $detailColumns[3] => $item_name,
                            $detailColumns[4] => $item_rate,
                            $detailColumns[5] => $pay_date,
                            $detailColumns[6] => $idcard,
                            $detailColumns[7] => $item_amt,
                            $detailColumns[8] => $wht_amount,
                            $detailColumns[9] => $tax_section_id,
                            $detailColumns[10] => $tax_section_name,
                            $detailColumns[11] => $accounting_code,
                        ];

                        //collect data of WHT/tax by idcard
                        $arrListRet[$idcard] += $wht_amount;

                        $arrDeductDetail[] = [
                            'idcard' => $idcard,
                            'item_id' => $item_id,
                            'item_name' => $item_name . ' (' . $item['ADD_DEDUCT_DETAIL'] . ')',
                            'item_amount' => $wht_amount,
                            'item_detail_extra' => $item['ADD_DEDUCT_DETAIL'],
                        ];
                    }

                    $arrWHTData[$idcard] = [
                        'wht_amt' => $_thismonth_amt,
                        'wht_tax' => $_thismonth_wht,
                    ];

                    $eff += $connection->createCommand()->batchInsert($_table, $detailColumns, $arrData)->execute();

                    $_totalcumulativeWhtAmt = $_cumulativeWhtAmt + $_thismonth_wht; //total old wht and plus this month;

                    $modelHead = TaxWitholding::findOne($tax_witholding_id);
                    $modelHead->wht_month_amount = $_thismonth_wht;
                    $modelHead->wht_cumulative_amount = $_totalcumulativeWhtAmt;
                    $modelHead->total_adddeduct = $_thismonth_amt;
                    $modelHead->total_income = $emp_salary_wage + $_thismonth_amt;
                    $modelHead->save(false);

                } //end foreach

                //call adjust add deduct detail;
                $total_wht = ApiSalary::AdjustAddDeductDetail($connection, $pay_date, $arrDeductDetail, $WHT_template);
                return $arrDeductDetail;

            } else {
                return [];
            }
        }
    }

    public static function FindWHTthisMonth($connection, $pay_date)
    {
        $arrWTH = $arrIDCard = [];
        $model_wht_detail = TaxWitholdingDetail::find()->where([
            'date_pay' => $pay_date
        ])->asArray()->all();

        if (is_array($model_wht_detail) || is_object($model_wht_detail)) {
            foreach ($model_wht_detail as $item) {
                $itemData = [
                    'template_id' => $item['template_id'],
                    'template_name' => $item['template_name'],
                    'wht_amount' => $item['wht_amount'],
                ];
                $arrWTH[$item['emp_idcard']][$item['template_id']] = $itemData;
                $arrIDCard[] = $item['emp_idcard'];
            }
        }
    }


    public static function Generate50TaviPDF($pay_date)
    {

    }

}