-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 23, 2017 at 02:42 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-9+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_money_bonds`
--

CREATE TABLE `emp_money_bonds` (
  `id` int(11) NOT NULL,
  `emp_data_id` int(11) DEFAULT NULL,
  `start_money_bonds` tinyint(1) DEFAULT NULL COMMENT 'สถานะเงินค้ำ 1=มี,0=ไม่มี',
  `start_pay_bonds` tinyint(1) DEFAULT NULL COMMENT 'สถานะเงินค้ำประกัน 1=ครบ,0=ผ่อน',
  `money_bonds` float DEFAULT NULL COMMENT 'เงินค้ำประกัน',
  `bank_name` varchar(100) DEFAULT NULL COMMENT 'ชื่อธนาคาร',
  `bank_no` varchar(20) DEFAULT NULL COMMENT 'หมายเลขธนาคาร',
  `first_money` float DEFAULT NULL COMMENT 'เงินค้ำประกันจ่ายครั้งแรก',
  `pay_money` float DEFAULT NULL COMMENT 'ผ่อนชำระต่อเดือน',
  `start_date_pay` date DEFAULT NULL COMMENT 'เดือนที่เริ่มจ่าย',
  `end_date_pay` date DEFAULT NULL COMMENT 'เดือนที่จ่ายสิ้นสุด',
  `bondsman_prefix` varchar(100) DEFAULT NULL COMMENT 'คำนำหน้า ',
  `bondsman_sex` tinyint(1) DEFAULT NULL COMMENT 'เพศ 1= ชาย, 2= หญิง ผู้ค้ำประกัน',
  `bondsman_name` varchar(100) DEFAULT NULL COMMENT 'ชื่อผู้ค้ำประกัน',
  `bondsman_surname` varchar(100) DEFAULT NULL COMMENT 'นามสกุล ผู้ค้ำประกัน',
  `bondsman_idcard` varchar(13) DEFAULT NULL COMMENT 'หมายเลขบัตรประชาชน ผู้ค้ำประกัน',
  `bondsman_birthday` date DEFAULT NULL COMMENT 'วันเดือนปีที่เกิด ผู้ค้ำประกัน',
  `bondsman_business` varchar(250) DEFAULT NULL COMMENT 'อาชีพ ผู้ค้ำประกัน',
  `bondsman_work_place` varchar(250) DEFAULT NULL COMMENT 'สถานที่ทำงาน ผู้ค้ำประกัน',
  `bondsman_month` varchar(2) DEFAULT NULL COMMENT 'จำนวนเดือนที่ทำงาน',
  `bondsman_year` varchar(2) DEFAULT NULL COMMENT 'จำนวนปีที่ทำงาน',
  `addr_idcard_address` varchar(50) DEFAULT NULL COMMENT 'ที่อยู่ เลขที่',
  `addr_idcard_moo` varchar(10) DEFAULT NULL,
  `addr_idcard_lane` varchar(10) DEFAULT NULL COMMENT 'ซอย',
  `addr_idcard_road` varchar(100) DEFAULT NULL COMMENT 'ถนน',
  `addr_idcard_village` varchar(100) DEFAULT NULL COMMENT 'หมู่บ้าน',
  `addr_idcard_district` varchar(100) DEFAULT NULL COMMENT 'ตำบล',
  `addr_idcard_amphur` varchar(100) DEFAULT NULL,
  `addr_idcard_province` varchar(100) DEFAULT NULL,
  `addr_idcard_postcode` varchar(50) DEFAULT NULL,
  `addr_plance_address` varchar(50) DEFAULT NULL COMMENT 'ที่อยู่ เลขที่',
  `addr_plance_moo` varchar(10) DEFAULT NULL,
  `addr_plance_lane` varchar(10) DEFAULT NULL COMMENT 'ซอย',
  `addr_plance_road` varchar(100) DEFAULT NULL COMMENT 'ถนน',
  `addr_plance_village` varchar(100) DEFAULT NULL COMMENT 'หมู่บ้าน',
  `addr_plance_district` varchar(100) DEFAULT NULL COMMENT 'ตำบล',
  `addr_plance_amphur` varchar(100) DEFAULT NULL,
  `addr_plance_province` varchar(100) DEFAULT NULL,
  `addr_plance_postcode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_money_bonds`
--
ALTER TABLE `emp_money_bonds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_data_id` (`emp_data_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_money_bonds`
--
ALTER TABLE `emp_money_bonds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
