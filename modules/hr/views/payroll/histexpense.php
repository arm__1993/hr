<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;

use app\bundle\AppAsset;
// API กลาง
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;
// API HR
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\Adddeductdetail;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/histexpense.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");

$this->registerCSS("
.select2-container {
    width: 250px;
}

#grid_all_deduct > table > tbody > tr > td {
    font-size : 12px;
}



");


?>

<section class="content">
    <!-- Default box -->
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-success"></i> Alert!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
    <form method="post" action="seachempcheckexpensehistory">
        <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
        <div class="box box-danger">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">การจัดการเงินเดือน</a>
                    </li>
                    <li>บันทึกเพิ่มเงิน</li>
                    <li class="active"> ประวัติรายการเพิ่ม</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h4 class="box-title">ข้อมูลรายการ </h4>
                    </div>
                    <div class="box-body">

                        <div class="row">

                            <div class="col-md-12">
                                <center>
                                    <label>บริษัท</label>
                                    &nbsp;
                                    <div class="btn-group">
                                        <select class="form-control" name="selectworking" id="selectworking"
                                                onchange="getCompanyForDepartment(this);" ;>
                                            <option value="">เลือกบริษัท</option>
                                            <?php $working = ApiHr::getWorking_company();
                                            foreach ($working as $value) {
                                                echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                            } ?>
                                        </select>
                                        <span id="urlGetDepartment"
                                              title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                                    </div>
                                    &nbsp;&nbsp;
                                    <label>แผนก</label>
                                    <div class="btn-group">
                                        <select class="form-control" name="selectdepartment" id="selectdepartment"
                                                onchange="getDepartmentForSection(this);">
                                            <option value=""> เลือกแผนก</option>
                                        </select>
                                        <span id="urlGetSection"
                                              title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                                    </div>
                                    &nbsp;&nbsp;
                                    <label>ฝ่าย</label>
                                    <div class="btn-group">
                                        <select class="form-control" name="selectsection" id="selectsection">
                                            <option value=""> เลือกฝ่าย</option>
                                        </select>
                                    </div>&nbsp;

                                    <label> ชื่อพนักงาน </label>
                                    <div class="btn-group">
                                        <select class="form-control select2" id="id_emp" name="id_emp">
                                            <?php
                                            $empdata = ApiHr::getempdataall();
                                            foreach ($empdata as $value) {
                                                ?>
                                                <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>&nbsp;

                                    <button type="submit" class="btn  btn-primary btn-sm"><i class="fa fa-search"></i>
                                        ค้นหา
                                    </button>

                                </center>
                            </div>

                        </div>
                    </div><!--/box-body-->
                </div>
            </div>
    </form>
    <br>

    <?php if ($query) { ?>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA TABLE -->
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                        data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example2" class="table table-bordered table-hover dataTable"
                                               role="grid" aria-describedby="example2_wrapper">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    NO.
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    ชื่อ - สกุล
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    บริษัท
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    ตำแหน่ง
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    ฝ่าย
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    ประวัติการเพิ่ม
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i = 1;
                                            if ($dataEmp) {
                                                foreach ($dataEmp as $value) { ?>

                                                    <tr role="row">
                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $value['Fullname']; ?></td>
                                                        <td><?php echo $value['CompanyName']; ?></td>
                                                        <td><?php echo $value['DepartmentName']; ?></td>
                                                        <td><?php echo $value['SectionName']; ?></td>
                                                        <td align='center' width="10%"><a
                                                                    href="gridviewaddducuctdetailhistedit?id_card=<?php echo $value['ID_Card']; ?>"
                                                                    target="_blank"><i
                                                                        class="fa fa-eye"></i>ดูข้อมูล</a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++;
                                                }
                                            }
                                            ?>
                                            </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!--example2_wrapper-->
                        </div><!--box-body-->
                    </div><!--box box-success-->
                    <!-- AREA TABLE -->
                </div>
            </div>


        </section>

    <?php } ?>
    </div>
    <?php if ($dataProvider) { ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">ประวัติรายการเพิ่มของพนักงาน</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

                <div class="row">
                    <div class="col-sm-12" style="padding: 20px;">


                        <table class="table table-condensed">
                            <tbody>
                            <tr>
                                <th>ชื่อ : <?php echo $dataEmpPersanal['Name']; ?>
                                    &nbsp;&nbsp;<?php echo $dataEmpPersanal['Surname']; ?>
                                    (<?php echo $dataEmpPersanal['Nickname']; ?>)
                                </th>
                                <th>ตำแหน่ง:<?php echo $dataEmpPersanal['Position']; ?></th>
                                <th>ฝ่าย:<?php echo $dataEmpPersanal['NameDepartment']; ?></th>
                                <th>แผนก:<?php echo $dataEmpPersanal['NameSection']; ?></th>
                            </tr>
                            </tbody>
                        </table>
                        <?php {
                            Pjax::begin(['id' => 'pjax_grid_step']);
                            echo GridView::widget([
                                'id' => 'grid_all_deduct',
                                'summary' => '<div class = "text-right"><strong>แสดง {begin}-{end} จากทั้งหมด {totalCount} รายการ จำนวน {pageCount} หน้า </strong></div>',
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn',
                                        'headerOptions' => ['width' => '5%'],
                                    ],
                                    [
                                        'attribute' => 'ADD_DEDUCT_THIS_MONTH_ID',
                                        'label' => 'รูปแบบการเพิ่ม',
                                        'value' => function ($data) {
                                            return ApiPayroll::mapPaytype($data->ADD_DEDUCT_THIS_MONTH_ID);
                                        },
                                        //'format' => 'raw',
                                        //'filter' => true,
                                        'contentOptions' => ['style' => 'width: 10%;']
                                    ],
                                    [
                                        'attribute' => 'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                                        'label' => 'รายการ',
                                        'value' => 'ADD_DEDUCT_THIS_MONTH_TMP_NAME',
                                        //'format' => 'raw',
                                        //'filter' => true,
                                        'contentOptions' => ['style' => 'width: 30%;']
                                    ],
                                    [
                                        'attribute' => 'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                                        'label' => 'รอบเงินเดือน',
                                        'value' => 'ADD_DEDUCT_THIS_MONTH_PAY_DATE',
                                        //'format' => 'raw',
                                        //'filter' => true,
                                        'contentOptions' => ['style' => 'width: 15%;']
                                    ],

                                    [
                                        'attribute' => 'ADD_DEDUCT_THIS_MONTH_AMOUNT',
                                        'label' => 'จำนวนเงิน',
                                        //'value' => 'Add_Deductdetail_Amount',
                                        'value' => function ($data) {
                                            return Helper::displayDecimal($data->ADD_DEDUCT_THIS_MONTH_AMOUNT);
                                        },
                                        //'format' => 'raw',
                                        //'filter' => true,
                                        'contentOptions' => ['style' => 'width: 10%;text-align:right;']
                                    ],

                                    [
                                        'attribute' => 'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                                        'label' => 'วันที่แก้ไขล่าสุด',
                                        'format' => ['date', 'php:d/m/Y H:i:s'],
                                        'value' => 'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE',
                                        /*  'value' => function ($data) {
                                              return DateTime::CalendarDate($data->ADD_DEDUCT_THIS_MONTH_UPDATE_DATE);
                                          },*/
                                        //'format' => 'raw',
                                        //'filter' => true,
                                        'contentOptions' => ['style' => 'width: 20%;']
                                    ],
                                    [
                                        'attribute' => 'ADD_DEDUCT_THIS_MONTH_UPDATE_BY',
                                        'label' => 'ผู้แก้ไขล่าสุด',
                                        'value' => function ($data) use ($arrEmp) {
                                            return $arrEmp[$data->ADD_DEDUCT_THIS_MONTH_UPDATE_BY];
                                        },
                                        //'value' => 'Add_Deductdetail_By',
                                        //'format' => 'raw',
                                        //'filter' => true,
                                        'contentOptions' => ['style' => 'width: 20%;']
                                    ],

                                ],
                            ]);
                            Pjax::end(); //end pjax_gridcorclub
                        } ?>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
    <?php } ?>


    <!-- /.box -->
</section><!-- /.content -->
