<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 18:07
 */

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiService
{

	public static function getPersonal($str_start,$str_end,$type_company,$technician_car)
	{
 
        $sql_personal = "SELECT ahistory.cus_id,
            max(cvhc_main.guard_time) as Date,
            cvhc_main.mile,
            ahistory.model_name,
            ahistory.engin_id,
            ahistory.register,
            ahistory.chassis            
            FROM cvhc_main
            inner join ahistory on ahistory.id = cvhc_main.ahistory_id            
            where cvhc_main.status in (25,26,27)
            and cvhc_main.guard_time between '$str_start' and '$str_end'
            and cvhc_main.brach_id = $type_company
            and ahistory.agroup_id in ($technician_car)
            and ahistory.status_new != 99
            and ahistory.cus_id != ''
            group by cvhc_main.ahistory_id";

       $personal = Yii::$app->dbERP_service->createCommand($sql_personal)->queryAll();

    return $personal;   

	}

	public static function getRageTime($str_rageStart,$str_rageEnd,$type_company,$technician_car)
	{
 
        $sql_rageTime = "SELECT ahistory.cus_id FROM cvhc_main inner join ahistory on ahistory.id = cvhc_main.ahistory_id where cvhc_main.status in (25,26,27) and cvhc_main.guard_time between '$str_rageStart' and '$str_rageEnd' and cvhc_main.brach_id = $type_company and ahistory.agroup_id in ($technician_car) and ahistory.status_new != 99 and ahistory.cus_id != '' group by cvhc_main.ahistory_id";
        $rageTime = Yii::$app->dbERP_service->createCommand($sql_rageTime)->queryAll();

    return $rageTime;   

	}

	public static function getName($dateStart,$dateEnd)
	{
 
            //$sql_name = name address
        $sql_name = "SELECT cus_id,ytax.name, ytax.address, ytax.tel FROM ytax WHERE datePay BETWEEN '$dateStart 00:00:00' AND '$dateEnd 23:59:59' ";
        $name = Yii::$app->dbERP_service->createCommand($sql_name)->queryAll();

    return $name;   

	}	
}