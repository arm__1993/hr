<?php

namespace app\modules\hr\controllers;

use app\modules\hr\apihr\ApiHr;

use app\modules\hr\models\Empdata;
use app\modules\hr\models\Position;
use app\modules\hr\models\Salarystep;
use app\modules\hr\models\EmpMoneyBonds;
use app\modules\hr\models\Empsalary;
use app\modules\hr\models\Salarychange;
use app\modules\hr\models\OrganicChart;
use app\modules\hr\models\RelationPosition;
use app\modules\hr\models\BankThailand;
use app\api\Utility;
use app\api\Helper;


use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\db\Expression;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\api\DBConnect;
use app\modules\hr\controllers\MasterController;
use Yii;

class PersonalsalaryController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;

    // /**
    // * function init() check session active or session login, if not redirect to login page
    // * @return \yii\web\Response
    // */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPosition()
    {
        //return $this->render('position');
        return Yii::$app->controller->renderPartial('position');
    }

    public function actionGetcontantbank()
    {
        $postValue = Yii::$app->request->post();
        $DataNo = $postValue['selectemp'];

        $model = Empdata::find()
            ->select('DataNo ,ID_Card , Salary_Bank , Salary_BankNo , SalaryViaBank')
            ->where(['DataNo' => $DataNo])
            ->asArray()
            ->one();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;

    }


    public function actionSavecontantbank()
    {
        $postValue = Yii::$app->request->post();
        $DataNo = $postValue['selectemp'];
        $Salary_Bank = $postValue['Salary_Bank'];
        $Salary_BankNo = Utility::removeDashIDcard($postValue['Salary_BankNo']);
        $SalaryViaBank = $postValue['SalaryViaBank'];

        $model = Empdata::findOne($DataNo);
        $model->Salary_Bank = $Salary_Bank;
        $model->Salary_BankNo = $Salary_BankNo;
        $model->SalaryViaBank = $SalaryViaBank;
        $model->save();

        if ($model) {
            $model = Empdata::find()
                ->select('DataNo , Salary_Bank , Salary_BankNo , SalaryViaBank')
                ->where(['DataNo' => $DataNo])
                ->asArray()
                ->one();

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        } else {
            $errors = $model->errors;
            echo "model can't validater <pre>";
            print_r($errors);
            echo "</pre>";
        }
    }

    public function actionGetcompany()
    {
        $getValue = Yii::$app->request->get();
        $model = ApiHr::getWorking_company();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionGetworktype()
    {
        $getValue = Yii::$app->request->get();
        $id_working = $getValue['id_working'];
        $id_department = $getValue['id_department'];
        $id_section = $getValue['id_section'];

        //            $model=Position::find()
        //                            ->select('position.WorkType , work_type.name')
        //                            ->from('position')
        //                            ->innerJoin('work_type','work_type.id = position.WorkType')
        //                            ->where('WorkCompany = '.$id_working.'
        //                                 AND Department = '.$id_department.'
        //                                 AND Section = '.$id_section.'')
        //                            ->distinct()
        //                            ->asArray()
        //                            ->all();
        //            print_r($model);

        // $sql = "SELECT distinct position.WorkType , work_type.name
        // FROM position
        // INNER JOIN work_type ON work_type.id = position.WorkType
        // WHERE WorkCompany = '$id_working'
        // AND Department = '$id_department'
        // AND Section = '$id_section'";
        $sql = "SELECT * FROM `work_type` where status != 99";
        //echo $sql;
        $connection = \Yii::$app->dbERP_easyhr_OU;
        $model = $connection->createCommand($sql)->queryAll();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionGetlevel()
    {
        $getValue = Yii::$app->request->get();
        $id_working = $getValue['id_working'];
        $id_department = $getValue['id_department'];
        $id_section = $getValue['id_section'];
        $id_workingtype = $getValue['id_workingtype'];

        // print_r($getValue);
        // exit;

        // $model=Position::find()
        // ->select('Level')
        // ->where('WorkCompany = '.$id_working.'
        // AND Department = '.$id_department.'
        // AND Section = '.$id_section.'
        // AND WorkType = '.$id_workingtype.'')
        // ->distinct()
        // ->asArray()
        // ->all();

        $model = \app\modules\hr\models\PositionLevel::find()
            ->asArray()
            ->all();

        // print_r($model);
        //  exit;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionGetposition()
    {
        $postValue = Yii::$app->request->post();
        $id_working = $postValue['company']; //บริษัท
        $id_department = $postValue['department']; //แผนก
        $id_section = $postValue['section']; //ฝ่าย
        $id_workingtype = $postValue['workingtype'];
        $id_level = $postValue['level'];
        $positionCode = $postValue['position'];


        $condition = "";
        $condition .= 'position.Status = 1 AND Organic_chart.active_status = 1';


        if (isset($positionCode)) {
            $condition .= " and position.PositionCode = '$positionCode' ";
        } else {
            if (isset($id_working) && isset($id_department) && isset($id_section)) {
                $condition .= " and WorkCompany ='$id_working' and  Department = '$id_department' and Section = '$id_section'";
            } else if (isset($id_working) && isset($id_department)) {
                $condition .= " and WorkCompany ='$id_working' and  Department = '$id_department'";
            } else if (isset($id_working)) {
                $condition .= " and WorkCompany ='$id_working' ";
            }
        }


        $model = Position::find()

            ->select('position.*,Organic_chart.id as org_id')
            ->innerJoin('Organic_chart', 'position.id=Organic_chart.Posision_id')
            ->where($condition)
            ->asArray()
            ->all();

//        if (isset($id_working)&& isset($id_department)&&isset($id_section)) {
//            $model = Position::find()
//                ->select('*')
//                ->from('position')
//                ->innerJoin('Organic_chart', 'position.id = Organic_chart.Posision_id')
//                ->where([
//                    'position.WorkCompany' => $id_working,
//                    'position.Department'=> $id_department,
//                    'position.Section' => $id_section,
//                   // 'position.addToCommand' => $seroo
//                ])
//                ->andWhere('position.Status = 1 ')
//                ->andWhere('Organic_chart.active_status=1')
//                ->asArray()
//                ->all();
//        }

         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionGetsalarysteplevel()
    {
        $getValue = Yii::$app->request->get();
        $stepchartname = $getValue['stepchartname'];
        $model = Salarystep::find()
            ->select('SALARY_STEP_LEVEL,SALARY_STEP_START_SALARY')
            ->where('SALARY_STEP_STATUS = "1"')
            ->andWhere('SALARY_STEP_CHART_NAME LIKE "%' . $stepchartname . '%"')
            ->asArray()
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionGetempsalarystep()
    {
        $getValue = Yii::$app->request->get();
        $stepchartname = $getValue['stepchartname'];
        $level = $getValue['level'];//,SALARY_STEP_VALUE
        $model = Salarystep::find()
            ->select('SALARY_STEP_LIMIT,SALARY_STEP_VALUE,SALARY_STEP_START_SALARY')
            ->where('SALARY_STEP_STATUS = "1"')
            ->andWhere('SALARY_STEP_CHART_NAME LIKE "%' . $stepchartname . '%"')
            ->andWhere('SALARY_STEP_LEVEL = ' . $level . '')
            ->asArray()
            ->all();
        $contStep = $model['0']['SALARY_STEP_LIMIT'];
        $resulStep = [];
        for ($i = 1; $i <= $contStep; $i++) {
            $resulStep[$i]['count'] = Helper::displayDecimal($i, 4);
            $resulStep[$i]['SALARY_STEP_VALUE'] = $model[0]['SALARY_STEP_VALUE'];
            // $resulStep[$i]['EMP_SALARY_STEP']= $model[0]['EMP_SALARY_STEP'];
            $resulStep[$i]['SALARY_STEP_START_SALARY'] = $model[0]['SALARY_STEP_START_SALARY'];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $resulStep;
    }

    public function actionGetsalarystepid()
    {
        $getValue = Yii::$app->request->get();
        $stepchartname = $getValue['stepchartname'];
        $level = $getValue['level'];
        $model = Salarystep::find()
            ->select('SALARY_STEP_ID,SALARY_STEP_START_SALARY')
            ->where('SALARY_STEP_STATUS = "1"')
            ->andWhere('SALARY_STEP_CHART_NAME LIKE "%' . $stepchartname . '%"')
            ->andWhere('SALARY_STEP_LEVEL = ' . $level . '')
            ->asArray()
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model;
    }

    public function actionEditpositionsaraly()
    {
        $getValue = Yii::$app->request->get();
        echo "<pre>";
        print_r($getValue);
        echo "</pre>";
        exit();
        $modeEmppositionsaraly = Empsalary::find()
            ->select('money_bonds')
            ->where(['emp_data_id' => $selectemp])
            ->asArray()
            ->one();
    }

    public function actionSavechangesalary()
    {
        $id_create = $this->idcardLogin;
        $postValue = Yii::$app->request->post();
        $ID_Card = $postValue['IdCardEmp'];
        $selectemp = $postValue['selectemp'];
        $modeEmpMoneyBonds = EmpMoneyBonds::find()
            ->select('money_bonds')
            ->where(['emp_data_id' => $selectemp])
            ->asArray()
            ->one();

        $GUARANTEE = $modeEmpMoneyBonds['money_bonds'];


        $modelPosition = Position::find()
            ->select('Name')
            ->where('PositionCode LIKE "%' . $postValue['selectposition'] . '%"')//แผนก
            ->asArray()
            ->one();


        $model = new Empsalary();
        $model->EMP_SALARY_ID_CARD = $ID_Card;
        $model->EMP_SALARY_POSITION_CODE = $postValue['selectposition']; //แผนก
        $model->EMP_SALARY_POSITION_LEVEL = $postValue['selectlevel'];
        $model->EMP_SALARY_WORKING_COMPANY = $postValue['selectworking']; //บริษัท
        $model->EMP_SALARY_DEPARTMENT = $postValue['selectdepartment'];
        $model->EMP_SALARY_SECTION = $postValue['selectsection']; //ฝ่าย
        $model->EMP_SALARY_CHART = $postValue['selectsalarystepchartname'];
        $model->EMP_SALARY_STEP = $postValue['selectempsalarystep'];
        $model->EMP_SALARY_LEVEL = $postValue['selectsalarysteplevel'];
        $model->EMP_SALARY_WAGE = $postValue['selectsalarystepstartsalary'];
        $model->EMP_SALARY_CHART_ID = $postValue['salarystepstartid'];
        $model->EMP_SALARY_GUARANTEE_MONEY = $GUARANTEE;
        $model->EMP_SALARY_CREATE_DATE = new Expression('NOW()');
        $model->EMP_SALARY_CREATE_BY = $id_create;
        $model->EMP_SALARY_UPDATE_DATE = new Expression('NOW()');
        $model->EMP_SALARY_UPDATE_BY = '';
        $model->socialBenefitStatus = '0';
        $model->socialBenifitPercent = '5';
        $model->status = '1';
        $model->dateusestart = "";
        $model->save();

        $modelSalarychange = new Salarychange();
        $modelSalarychange->EVALUATION_ID = '';
        $modelSalarychange->SALARY_CHANGE_EMP_ID = $ID_Card;
        $modelSalarychange->SALARY_CHANGE_POSITION_NAME = '';
        $modelSalarychange->SALARY_CHANGE_POSITION_CODE = $postValue['selectposition']; //แผนก
        $modelSalarychange->SALARY_CHANGE_DATE = '';
        $modelSalarychange->SALARY_CHANGE_ROUND = '1';
        $modelSalarychange->EVALUATION_RESULT_CHART = '';
        $modelSalarychange->SALARY_CHANGE_TYPE = $postValue['selectsalarychangetype'];
        $modelSalarychange->SALARY_CHANGE_CHART_OLD = '';
        $modelSalarychange->SALARY_CHANGE_LEVEL_OLD = '';
        $modelSalarychange->SALARY_CHANGE_STEP_OLD = '';
        $modelSalarychange->SALARY_CHANGE_WAGE_OLD = '0.00';
        $modelSalarychange->SALARY_CHANGE_CHART_NEW = $postValue['selectsalarystepchartname'];
        $modelSalarychange->SALARY_CHANGE_LEVEL_NEW = $postValue['selectsalarysteplevel'];
        $modelSalarychange->SALARY_CHANGE_STEP_NEW = $postValue['selectempsalarystep'];
        $modelSalarychange->SALARY_CHANGE_ADD_STEP = '';
        $modelSalarychange->SALARY_CHANGE_WAGE_NEW = $postValue['selectsalarystepstartsalary'];
        $modelSalarychange->SALARY_CHANGE_EMP_REMARK = '';
        $modelSalarychange->SALARY_CHANGE_REMARK = '';
        $modelSalarychange->SALARY_EFFECTIVE_DATE = '';
        $modelSalarychange->SALARY_CHANGE_STATUS = '1';
        $modelSalarychange->SALARY_CHANGE_IN_SLIP = '';
        $modelSalarychange->SALARY_CHANGE_CREATE_DATE = new Expression('NOW()');
        $modelSalarychange->SALARY_CHANGE_CREATE_BY = $id_create;
        $modelSalarychange->SALARY_CHANGE_UPDATE_DATE = new Expression('NOW()');
        $modelSalarychange->SALARY_CHANGE_UPDATE_BY = '';
        $modelSalarychange->save();

    }

    public function actionGetsalaryidcard()
    {
        $getValue = Yii::$app->request->get();
        $IdCard = $getValue['Idcard'];

        //        $model=Empsalary::find()
        //                        ->select('EMP_SALARY.* , working_company.name as workingname,position.Name as positionname')
        //                        ->from('EMP_SALARY')
        //                        ->innerJoin('working_company','working_company.id = EMP_SALARY.EMP_SALARY_WORKING_COMPANY')
        //                        ->innerJoin('position','position.PositionCode = EMP_SALARY.EMP_SALARY_POSITION_CODE')
        //                        ->where('EMP_SALARY_ID_CARD ='.$IdCard.'')
        //                        ->andWhere('EMP_SALARY.status != "99"')
        //                        ->asArray()
        //                        ->all();

        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];

        $sql = "SELECT $db[pl].EMP_SALARY.* ,
        $db[ou].working_company.name as workingname,
        $db[ou].position.Name as positionname,
        $db[ct].emp_data.socialBenefitStatus
        FROM $db[pl].EMP_SALARY
        INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY
        INNER JOIN $db[ou].position ON $db[ou].position.PositionCode = $db[pl].EMP_SALARY.EMP_SALARY_POSITION_CODE
        LEFT JOIN $db[ct].emp_data ON $db[ct].emp_data.ID_Card = $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD
        WHERE $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD = '$IdCard'
        AND $db[pl].EMP_SALARY.status !='99' ORDER BY $db[pl].EMP_SALARY.EMP_SALARY_CREATE_DATE DESC, $db[pl].EMP_SALARY.EMP_SALARY_POSITION_CODE ASC
        ";


        $model = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $model;

    }


    public function actionDeletepersonalsalary()
    {
        $transactionPAYROLL = Yii::$app->dbERP_easyhr_PAYROLL->beginTransaction();
        $transactionOU = Yii::$app->dbERP_easyhr_OU->beginTransaction();
        try {
            $postValue = Yii::$app->request->post();
            $model = Empsalary::find()
                ->where(['EMP_SALARY_ID' => $postValue['EMP_SALARY_ID']])
                ->andWhere(['EMP_SALARY_POSITION_CODE' => $postValue['code_position']])
                ->one();
            $model->status = Yii::$app->params['DELETE_STATUS'];
            $saveStatus = $model->save();
            if ($saveStatus) {
                $position = Position::find()->where(['PositionCode' => $postValue['code_position']])->asArray()->all();
                if (count($position) != 0) {
                    $relation_position = RelationPosition::find()->where(['=', 'position_id', $position[0]['id']])->andWhere(['=', 'status', '99']);
                    $relation_position->status = '99';
                    $relation_position_statusSave = $relation_position->save();
                    if ($relation_position_statusSave == false) {
                        $transactionPAYROLL->rollBack();
                        $transactionOU->rollBack();
                    }
                    $org = OrganicChart::find()->where(['Posision_id' => $position[0]['id']])->one();
                    $org->active_status = '2';
                    $org->Emp_id = 0;
                    $org->Emp_name = 'ว่าง';
                    $org_save_status = $org->save();
                    if ($org_save_status) {
                        $transactionPAYROLL->commit();
                        $transactionOU->commit();
                    }
                }
            }
        } catch (Exception $e) {
            $transactionPAYROLL->rollBack();
            $transactionOU->rollBack();
        }
        return true;

        //print_r($model);

    }
}