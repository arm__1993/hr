<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\models\Position;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java

$selectworking = ApiHr::getWorking_company();
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jobreport.js?t=' . time(), ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);



?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li class="active">รายงานตำแหน่งงานว่าง</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <form id="reset" name="set">
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-3">
                            <input id="oppcommpay_object" type="hidden"  value="<? echo $commpay?>" >
                            <input id="department_object" type="hidden"  value="<? echo $department?>" >
                            <input id="section_object" type="hidden" value="<? echo $section?>" >
                            <div class="form-group">
                                <label for="numberPassportEmp" class="col-sm-4 control-label">บริษัท</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="selectworking" id="selectworking"
                                            onchange="getCompanyForDepartment(this,0,0);">
                                        <option value="">ทั้งหมด</option>

                                        <?php
                                        foreach ($selectworking as $value) {

                                                echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';

                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="numberPassportEmp" class="col-sm-3 control-label">แผนก</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="selectdepartment" id="selectdepartment"
                                            onchange="getDepartmentForSection(this,0,0);">
                                        <option value=""> ทั้งหมด</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="numberPassportEmp" class="col-sm-3 control-label">ฝ่าย</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="selectsection" id="selectsection">
                                        <option value=""> ทั้งหมด</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row" style="text-align:center">
                        <input type="button" onclick="Search();" class="btn btn-primary btn-md" value="ค้นหา">
                        <input type="reset" class="btn btn-warning btn-md" value="ล้าง">
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-md-10">

                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary btn-md" onclick="Searchmpdf();" ><i class="fa fa-file-pdf-o"> ออกรายงาน</i></button>
                </div>
            </div>

            <div class="box-body">

                <?php
                {
                    Pjax::begin(['id' => 'pjax_grid_position']);
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $modelSearch,
                        'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                        'showFooter' => true,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn',
                                'headerOptions' => ['width' => '20'],
                            ],

                            [
                                'attribute' => 'companyname',
                                'label' => 'บริษัท',
                                'value' => 'companyname',
                                'footer'=> '<center><B>ผลรวม</B></center>',

                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 200px;', 'align=center']
                            ],
                            [
                                'attribute' => 'departmentname',
                                'label' => 'แผนก',
                                'value' => 'departmentname',



                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 100px;', 'align=center']
                            ],
                            [
                                'attribute' => 'sectionname',
                                'label' => 'ฝ่าย',
                                'value' => 'sectionname',
                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 100px;', 'align=center']
                            ],
                            [
                                'attribute' => 'Name',
                                'label' => 'ชื่อตำแหน่ง',
                                'value' => 'Name',
                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 100px;', 'align=center']
                            ],
                            [
                                'attribute' => 'STATUS_19',
                                'label' => 'ผังโครงสร้าง',
                                'value' => 'STATUS_19',
                                'footer'=> Position::getTotal($dataProvider->models,'STATUS_19'),
                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                            ],
                            [
                                'attribute' => 'pass',
                                'label' => 'บรรจุ',
                                'value' => 'pass',
                                'footer'=> Position::getTotal($dataProvider->models, 'pass'),
                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                            ],
                            [
                                'attribute' => 'notpass',
                                'label' => 'ทดลองงาน',
                                'value' => 'notpass',
                                'footer'=> Position::getTotal($dataProvider->models, 'notpass'),
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                            ],
                            [
                                'attribute' => 'STATUS1',
                                'label' => 'สรรหา',
                                'value' => 'STATUS1',
                                'footer'=> Position::getTotal($dataProvider->models, 'STATUS1'),
                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                            ],
                            [
                                'attribute' => 'STATUS_2',
                                'label' => 'ปิดสรรหา',
                                'value' => 'STATUS_2',
                                'footer'=> Position::getTotal($dataProvider->models, 'STATUS_2'),
                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                            ],
                            [
                                'attribute' => 'STATUS_99',
                                'label' => 'ปิดตำแหน่ง',
                                'value' => 'STATUS_99',
                                'footer' => Position::getTotal($dataProvider->models, 'STATUS_99'),
                                //'format' => 'raw',
                                //'filter' => true,
                                'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                            ],


                        ],

                    ]);
                    Pjax::end(); //end pjax_gridcorclub
                }
                ?>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->