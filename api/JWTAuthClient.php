<?php 
namespace app\api;
use Firebase\JWT\JWT;
class JWTAuthClient{
    private $secretKey;
    private $alg;
    private $expire;
    public $immediate;
    public function __construct(){
        $this->secretKey = "awtxxx";
        $this->alg       = "HS512";
        $this->immediate = true;
	}
    public function setSecretKey($key){
        $this->secretKey = $key;//awtxxx
    }
    public function verifyToken($inToken = ''){
        
        $resToken           = array();
        $resToken['status'] = true;
        $resToken['res']    = array();
        $tokenIn = ($inToken == '') ? $this->getToken():$inToken;//
        try{
            $resToken['res'] = JWT::decode($tokenIn, $this->secretKey, [$this->alg]);
        } catch(Exception $e) {
            $resToken['status'] = false;
            $resToken['msg'] = $e->getMessage();
        }
        if($this->immediate){
            //redirect
           
            if(!$resToken['status']){
                echo '<pre>';print_r($resToken);echo "</pre>";
                exit();
            }
        }
        return $resToken;
    }
    private function getToken(){
        $token = "";
        if(isset($_COOKIE['token']) && $_COOKIE['token'] != ""){
            $token = $_COOKIE['token'];
        }
        return $token;
    }
}
?>