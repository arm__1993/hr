<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 6/6/2017 AD
 * Time: 10:40 AM
 */

namespace app\modules\hr\apihr;

use yii\db\Expression;
use yii\db\Transaction;
use yii;

use app\modules\hr\models\Empdata;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Salarystep;
use app\modules\hr\models\Wagethismonth;
use app\modules\hr\models\Vempsalarydetail;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\models\Socialsalary;
use app\modules\hr\models\Benefitfund;
use app\modules\hr\models\Wagehistory;
use app\modules\hr\models\Adddeducthistory;


use app\api\Helper;
use app\api\DateTime;
use app\api\Utility;
use app\modules\hr\apihr\ApiSSO;
use app\modules\hr\apihr\ApiPayroll;

class ApiSlipsalary
{
    public static function getSlipSalary($idworkingcompany,$iddepartment,$idsection,$idcard,$month,$year,$typeselect){
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];
        $SelectYear = '%'.$month.'-'.$year.'%';

         if($typeselect==1){

             if(isset($idworkingcompany) && isset($iddepartment) && isset($idsection)){
                 $sql = "SELECT $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_EMP_ID as WAGE_EMP_ID,
                                $db[pl].WAGE_HISTORY.WAGE_PAY_DATE as WAGE_PAY_DATE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_CODE as WAGE_POSITION_CODE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_NAME as WAGE_POSITION_NAME,
                                $db[pl].WAGE_HISTORY.WAGE_SECTION_ID as WAGE_SECTION_ID,
                                $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID as WAGE_DEPARTMENT_ID,
                                $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY as WAGE_WORKING_COMPANY,
                                $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_THIS_MONTH_STATUS as WAGE_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID as ADD_DEDUCT_THIS_MONTH_EMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE as ADD_DEDUCT_THIS_MONTH_PAY_DATE ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID as ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME as ADD_DEDUCT_THIS_MONTH_TMP_NAME ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL as ADD_DEDUCT_THIS_MONTH_DETAIL,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT as ADD_DEDUCT_THIS_MONTH_AMOUNT,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE as ADD_DEDUCT_THIS_MONTH_TYPE,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS as ADD_DEDUCT_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS as ADD_DEDUCT_THIS_MONTH_SLIP_STATUS
                          FROM $db[pl].WAGE_HISTORY INNER JOIN $db[pl].ADD_DEDUCT_HISTORY ON $db[pl].WAGE_HISTORY.WAGE_EMP_ID = $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                          WHERE $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS = '1'
                          AND   $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS = '1'
                          AND   $db[pl].WAGE_HISTORY.WAGE_PAY_DATE LIKE $SelectYear
                          AND   $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY = $idworkingcompany
                          AND   $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID = $iddepartment
                          AND   $db[pl].WAGE_HISTORY.WAGE_SECTION_ID = $idsection";

                 $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
                 $data = $connection->createCommand($sql)->queryOne();
                 return $data;

             }else if(isset($idworkingcompany) && isset($iddepartment)){

                 $sql = "SELECT $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_EMP_ID as WAGE_EMP_ID,
                                $db[pl].WAGE_HISTORY.WAGE_PAY_DATE as WAGE_PAY_DATE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_CODE as WAGE_POSITION_CODE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_NAME as WAGE_POSITION_NAME,
                                $db[pl].WAGE_HISTORY.WAGE_SECTION_ID as WAGE_SECTION_ID,
                                $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID as WAGE_DEPARTMENT_ID,
                                $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY as WAGE_WORKING_COMPANY,
                                $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_THIS_MONTH_STATUS as WAGE_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID as ADD_DEDUCT_THIS_MONTH_EMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE as ADD_DEDUCT_THIS_MONTH_PAY_DATE ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID as ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME as ADD_DEDUCT_THIS_MONTH_TMP_NAME ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL as ADD_DEDUCT_THIS_MONTH_DETAIL,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT as ADD_DEDUCT_THIS_MONTH_AMOUNT,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE as ADD_DEDUCT_THIS_MONTH_TYPE,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS as ADD_DEDUCT_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS as ADD_DEDUCT_THIS_MONTH_SLIP_STATUS
                          FROM $db[pl].WAGE_HISTORY INNER JOIN $db[pl].ADD_DEDUCT_HISTORY ON $db[pl].WAGE_HISTORY.WAGE_EMP_ID = $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                          WHERE $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS = '1'
                          AND   $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS = '1'
                          AND   $db[pl].WAGE_HISTORY.WAGE_PAY_DATE LIKE $SelectYear
                          AND   $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY = $idworkingcompany
                          AND   $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID = $iddepartment";

                 $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
                 $data = $connection->createCommand($sql)->queryOne();
                 return $data;

             }else if(isset($idworkingcompany)){
                 $sql = "SELECT $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_EMP_ID as WAGE_EMP_ID,
                                $db[pl].WAGE_HISTORY.WAGE_PAY_DATE as WAGE_PAY_DATE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_CODE as WAGE_POSITION_CODE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_NAME as WAGE_POSITION_NAME,
                                $db[pl].WAGE_HISTORY.WAGE_SECTION_ID as WAGE_SECTION_ID,
                                $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID as WAGE_DEPARTMENT_ID,
                                $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY as WAGE_WORKING_COMPANY,
                                $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_THIS_MONTH_STATUS as WAGE_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID as ADD_DEDUCT_THIS_MONTH_EMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE as ADD_DEDUCT_THIS_MONTH_PAY_DATE ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID as ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME as ADD_DEDUCT_THIS_MONTH_TMP_NAME ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL as ADD_DEDUCT_THIS_MONTH_DETAIL,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT as ADD_DEDUCT_THIS_MONTH_AMOUNT,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE as ADD_DEDUCT_THIS_MONTH_TYPE,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS as ADD_DEDUCT_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS as ADD_DEDUCT_THIS_MONTH_SLIP_STATUS
                          FROM $db[pl].WAGE_HISTORY INNER JOIN $db[pl].ADD_DEDUCT_HISTORY ON $db[pl].WAGE_HISTORY.WAGE_EMP_ID = $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                          WHERE $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS = '1'
                          AND   $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS = '1'
                          AND   $db[pl].WAGE_HISTORY.WAGE_PAY_DATE LIKE $SelectYear
                          AND   $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY = $idworkingcompany";

                 $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
                 $data = $connection->createCommand($sql)->queryOne();
                 return $data;
             }else{
                 $sql = "SELECT $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_EMP_ID as WAGE_EMP_ID,
                                $db[pl].WAGE_HISTORY.WAGE_PAY_DATE as WAGE_PAY_DATE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_CODE as WAGE_POSITION_CODE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_NAME as WAGE_POSITION_NAME,
                                $db[pl].WAGE_HISTORY.WAGE_SECTION_ID as WAGE_SECTION_ID,
                                $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID as WAGE_DEPARTMENT_ID,
                                $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY as WAGE_WORKING_COMPANY,
                                $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_THIS_MONTH_STATUS as WAGE_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID as ADD_DEDUCT_THIS_MONTH_EMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE as ADD_DEDUCT_THIS_MONTH_PAY_DATE ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID as ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME as ADD_DEDUCT_THIS_MONTH_TMP_NAME ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL as ADD_DEDUCT_THIS_MONTH_DETAIL,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT as ADD_DEDUCT_THIS_MONTH_AMOUNT,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE as ADD_DEDUCT_THIS_MONTH_TYPE,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS as ADD_DEDUCT_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS as ADD_DEDUCT_THIS_MONTH_SLIP_STATUS
                          FROM $db[pl].WAGE_HISTORY INNER JOIN $db[pl].ADD_DEDUCT_HISTORY ON $db[pl].WAGE_HISTORY.WAGE_EMP_ID = $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                          WHERE $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS = '1'
                          AND   $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS = '1'
                          AND   $db[pl].WAGE_HISTORY.WAGE_PAY_DATE LIKE $SelectYear";

                 $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
                 $data = $connection->createCommand($sql)->queryOne();
                 return $data;
             }

         }else if($typeselect==2){
             $sql = "SELECT $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_EMP_ID as WAGE_EMP_ID,
                                $db[pl].WAGE_HISTORY.WAGE_PAY_DATE as WAGE_PAY_DATE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_CODE as WAGE_POSITION_CODE,
                                $db[pl].WAGE_HISTORY.WAGE_POSITION_NAME as WAGE_POSITION_NAME,
                                $db[pl].WAGE_HISTORY.WAGE_SECTION_ID as WAGE_SECTION_ID,
                                $db[pl].WAGE_HISTORY.WAGE_DEPARTMENT_ID as WAGE_DEPARTMENT_ID,
                                $db[pl].WAGE_HISTORY.WAGE_WORKING_COMPANY as WAGE_WORKING_COMPANY,
                                $db[pl].WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY,
                                $db[pl].WAGE_HISTORY.WAGE_THIS_MONTH_STATUS as WAGE_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID as ADD_DEDUCT_THIS_MONTH_EMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE as ADD_DEDUCT_THIS_MONTH_PAY_DATE ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID as ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME as ADD_DEDUCT_THIS_MONTH_TMP_NAME ,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL as ADD_DEDUCT_THIS_MONTH_DETAIL,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT as ADD_DEDUCT_THIS_MONTH_AMOUNT,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE as ADD_DEDUCT_THIS_MONTH_TYPE,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS as ADD_DEDUCT_THIS_MONTH_STATUS,
                                $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS as ADD_DEDUCT_THIS_MONTH_SLIP_STATUS
                          FROM $db[pl].WAGE_HISTORY INNER JOIN $db[pl].ADD_DEDUCT_HISTORY ON $db[pl].WAGE_HISTORY.WAGE_EMP_ID = $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                          WHERE $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_STATUS = '1'
                          AND   $db[pl].ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_SLIP_STATUS = '1'
                          AND   $db[pl].WAGE_HISTORY.WAGE_PAY_DATE LIKE $SelectYear
                          AND   $db[pl].WAGE_HISTORY.WAGE_EMP_ID = $idcard";

             $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
             $data = $connection->createCommand($sql)->queryOne();
             return $data;
         }

    }
}