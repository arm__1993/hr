<?php

namespace app\modules\baymanagement\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;


/**
 * This is the model class for table "ehole_take".
 *
 * @property integer $id
 * @property string $branch_id
 * @property string $checker_id
 * @property string $technician
 * @property string $techician_name_sername
 * @property string $erepair_group_id
 * @property string $erepair_group_name
 * @property string $whoadd
 * @property string $time_add
 * @property integer $status_available
 * @property integer $erepair_p_code_id
 * @property string $status
 */
class EholeTake extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    protected $_pageSize;
    public static function tableName()
    {
        return 'ehole_take';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_id', 'checker_id', 'technician', 'techician_name_sername', 'erepair_group_id', 'erepair_group_name', 'whoadd', 'time_add', 'status_available', 'erepair_p_code_id', 'status'], 'required'],
            [['erepair_group_id', 'erepair_group_name'], 'string'],
            [['status_available', 'erepair_p_code_id'], 'integer'],
            [['branch_id'], 'string', 'max' => 10],
            [['checker_id', 'technician', 'whoadd'], 'string', 'max' => 13],
            [['techician_name_sername'], 'string', 'max' => 250],
            [['time_add'], 'string', 'max' => 25],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_id' => 'Branch ID',
            'checker_id' => 'Checker ID',
            'technician' => 'Technician',
            'techician_name_sername' => 'Techician Name Sername',
            'erepair_group_id' => 'Erepair Group ID',
            'erepair_group_name' => 'Erepair Group Name',
            'whoadd' => 'Whoadd',
            'time_add' => 'Time Add',
            'status_available' => 'Status Available',
            'erepair_p_code_id' => 'Erepair P Code ID',
            'status' => 'Status',
        ];
    }


    public function search($params)
    {
        $data = EholeTake::find() ->where('status_available <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','techician_name_sername',$this->techician_name_sername]); //	ชื่อเต็มบริษัท
        $data->andFilterWhere(['like','erepair_group_name',$this->erepair_group_name]); //	ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
