<?php

namespace app\modules\vhc\models\service;

use Yii;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_mapping".
 *
 * @property integer $id
 * @property integer $amodel_id
 * @property integer $vhc_model_id
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcMapping extends MasterModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_mapping';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amodel_id', 'vhc_model_id'], 'required'],
            [['amodel_id', 'vhc_model_id', 'record_status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amodel_id' => 'Amodel ID',
            'vhc_model_id' => 'Vhc Model ID',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'savestatus']);
    }
}
