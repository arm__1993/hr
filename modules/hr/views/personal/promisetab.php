<?php

?>

<div class="row">

    <!--ข้อมูลสัญญา -->
    <div class="col-md-12">
 
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">ข้อมูลสัญญา</h3>
            </div>
            <div class="box-body">
                <div class="pull-right" style="margin-bottom:3px"> 
                    <button type="button" style="<?php echo ($empModel['DataNo'] == '') ? 'display:none':''; ?>" class="btn btn-sm  <?php echo ($optional['viewOnly'] == true) ? 'empDisplayNone':''; ?>" onclick="showAddPromise()">เพิ่มสัญญา</button>
                </div>
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>ลำดับ</th>
                        <th>เลขที่สัญญา</th>
                        <th>ประเภทสัญญาจ้างงาน</th>
                        <th>วันที่เซ็นสัญญา</th>
                        <th>วันที่เริ่มงาน</th>
                        <th>วันที่สิ้นสุด</th>
                        <th>สถานะสัญญา</th>
                        <th>การจัดการ</th>
                    </tr>
                    <?php if(empty($empProms)){ ?>
                    <tr>
                        <td style="text-align:center" colspan="8">
                            <span> <?php echo ($empModel['DataNo'] == '') ? 'กรุณาบันทึกข้อมูลพนักงานก่อน':'ไม่มีข้อมูล'; ?></span>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php foreach($empProms as $no => $eProm){?>
                        <tr class="promise_list" 
                            data-promisetypeid="<?php echo $eProm->emp_promise_type_id;?>"
                            data-promiseprint = '{"promise_detail":<?php echo ($eProm->promise_detail != "")?1:0;?>,"promise_bond":<?php echo ($eProm->promise_bond != "")?1:0;?>,"promise_attach":<?php echo ($eProm->promise_attach != "")?1:0;?>}'
                            >
                            <td><?php echo ($no + 1); ?></td>
                            <td><?php echo $eProm->promise_code; ?></td>
                            <td><?php echo $eProm->promise_name; ?></td>
                            <td><?php echo $eProm->promise_sign_date; ?></td>
                            <td><?php echo $empModel->Start_date; ?></td>
                            <td><?php echo $empModel->End_date; ?></td>
                            <td><?php echo ($eProm->promise_sign_status == 0) ? 'ยังไม่เซ็นสัญญา':'เซ็นสัญญาแล้ว'; ?></td>
                            <td>
                                <button type="button" style="<?php echo ($eProm->promise_sign_status == 1) ? 'display:none':'';?>" class="btn btn-success btn-xs <?php echo ($optional['viewOnly'] == true) ? 'empDisplayNone':''; ?>" onclick="signPromise(<?php echo $eProm->id;?>)"><i class="glyphicon glyphicon-ok-sign"></i> เซ็นสัญญา</button>
                                <button type="button" class="btn btn-primary btn-xs  <?php echo ($optional['viewOnly'] == true) ? 'empDisplayNone':''; ?>" onclick="showFormEditContentPromise(this,<?php echo $eProm->id;?>)"><i class="glyphicon glyphicon-edit"></i> แก้ไข</button>
                                <button type="button" class="btn btn-info btn-xs" onclick="printPromise(<?php echo $eProm->id;?>,this)"><i class="glyphicon glyphicon-print"></i> ปรินต์</button>
                                <button type="button" style="<?php echo ($eProm->promise_sign_status == 1) ? 'display:none':'';?>" class="btn btn-danger btn-xs  <?php echo ($optional['viewOnly'] == true) ? 'empDisplayNone':''; ?>" onclick="delPromise(<?php echo $eProm->id;?>)"><i class="glyphicon glyphicon-remove"></i> ลบ</button>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
 
    </div>
</div>


<div class="modal fade modal-wide" id="modalAddPromise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มสัญญา</h4>
            </div>
            <div class="modal-body" width="100%" >
                <div id="addPromiseForm" >
                    <center><h2>รอสักครู่..</h2></center>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addPromise()" >เพิ่ม</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  

<div class="modal fade modal-wide" id="modalEditContentPromise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">แก้ไข</h4>
            </div>
            <div class="modal-body" width="100%" >
                <div >
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#promise_tab_1" data-toggle="tab">
                                รายละเอียดสัญญาจ้าง
                                </a>
                            </li>
                            <li>
                                <a href="#promise_tab_2" data-toggle="tab">
                                รายละเอียดสัญญาค้ำประกัน
                                </a>
                            </li>
                             <li>
                                <a href="#promise_tab_3" data-toggle="tab">
                                รายละเอียดบันทึกแนบ
                                </a>
                            </li>
                         
                        </ul>
                        <div class="tab-content">

                              <div class="tab-pane fade in active" id="promise_tab_1">
                                <div class="box-body">
                                    <textarea id="text_promise_detail" style="height:800px"></textarea>
                                </div>
                              </div>
                              <div class="tab-pane" id="promise_tab_2">
                                <div class="box-body">
                                    <textarea id="text_promise_bond"  style="height:800px"></textarea>
                                </div>
                              </div>
                              <div class="tab-pane" id="promise_tab_3">
                                <div class="box-body">
                                    <textarea id="text_promise_attach"  style="height:800px"></textarea>
                                </div>
                              </div>
                      </div>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="submitEditpromiseContent" data-promiseid onclick="editPromiseContent()" >แก้ไข</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  
