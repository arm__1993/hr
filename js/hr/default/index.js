
$(document).ready(function () {








        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            viewport: {
                width: 329,
                height: 329,
                // width: 325,
                // height: 325,
                type: 'circle'
            },
            boundary: {
                width: 400,
                height: 400
            }
        });
   // });


    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
                console.log('jQuery bind complete');
            });

        }
        reader.readAsDataURL(this.files[0]);
    });

    $('.upload-result').on('click', function (ev) {

        var objForm = {};
        objForm.inputName = $('#inputName').val();
        objForm.Last = $('#Last').val();


        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {


            $.ajax({
                url: "uploadcroppie",
                type: "POST",
                data: {"image": resp},
                success: function (data) {
                    console.log(objForm.Last);
                    //  console.log({"image": resp});

                    html = '<img src="' + resp + '" />';
                    h2ml = objForm.inputName;
                    h3ml = objForm.Last;
                    $("#upload-demo-i").html(html);
                    $("#inputNameview").html(h2ml);
                    $("#Lastview").html(h3ml);



                    bootbox.confirm({
                        size: "small",
                        message:"<h4 class=\"btalert\">ต้องการจะRefreshหน้าโปรไฟล์เพื่อดูรูปคุณหรือไม่?</h4>",
                        callback: function(result){
                            if(result==1) {
                                window.location.href='../../hr/default/index';
                            }
                        }
                    });
                }
            });
        });
    });
});