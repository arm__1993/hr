<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;

use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\apihr\ApiHr;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/employee-lookup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/urlpathconfig.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/adddeductbydept.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hr-config.js?t='.time());
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/MonthPicker.min.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl."/css/hr/MonthPicker.min.css");


$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);


$script = <<< JS
$(document).ready(function() {
    setTimeout(function() {
    $('#message').fadeOut('slow');
    }, 3000);

});
JS;

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");
$this->registerCssFile(Yii::$app->request->baseUrl . "/fonts/01thaifontcss.css");


$this->registerJs($script);
?>
    <section class="content">
        <!-- Default box -->
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-success"></i> Alert!</h4>
                <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>

        <form class="form-horizontal" id="AddForm">
            <div class="box box-danger">
                <div class="breadcrumbs" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">ข้อมูลเงินเดือน</a>
                        </li>
                        <li>รายการหักจากหน่วยงาน</li>
                        <li class="active">บันทึกรายการหัก</li>
                    </ul><!-- /.breadcrumb -->
                    <!-- /section:basics/content.searchbox -->
                </div>
                <div class="box-body">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h4 class="box-title">ข้อมูลรายการ </h4>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="monthselect"  class="col-sm-4 control-label">รอบเงินเดือน</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="date_pay" id="date_pay" value="<?php echo date('m-Y');?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label>ประเภทรายการ</label>&nbsp;
                                    <div class="btn-group">
                                        <select class="form-control" name="add_deduct_template_id"
                                                id="add_deduct_template_id">
                                            <option value="0">กรุณาเลือก</option>
                                            <?php
                                            $arrList = ApiPayroll::getDataDeductTemp();
                                            foreach ($arrList as $key => $value) {
                                                echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                </div>

                            </div>
                        </div><!--/box-body-->
                    </div>
                </div>
                <br>
                <div align="right">
                    <a href="javascript:;" id="btnAddFile">
                        <img src="<?php echo $imghr; ?>/add.png" class="img-circle">
                        <span>เพิ่มแถว</span>
                        &nbsp;
                    </a>
                </div>

                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h4 class="box-title">รายชื่อพนักงาน </h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <center>
                                <table id="tblticket" style="width: 80% !important;">
                                    <tbody>
                                    <tr>
                                        <td width="5%">1.</td>
                                        <td width="35%">
                                            <select class="form-control select2" name="id_emp[]" required>
                                                <?php $empdata = ApiHr::getempdataall();
                                                foreach ($empdata as $value) {
                                                    ?>
                                                    <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td width="5%" style="padding-left: 10px;"><label>จำนวน</label></td>
                                        <td width="20%">
                                            <input type="text" class="form-control numberformat" name="total[]"
                                                   data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true"
                                                   data-mask required>
                                        </td>
                                        <td width="5%">
                                            <a href="javascript:;" onclick="showhidedetail($(this))">
                                                <img src="<?php echo $imghr; ?>/detail_icon.png"
                                                     style="width: 24px;height: 24px;">
                                            </a>
                                        </td>
                                        <td width="25%">
                                            <input type="text" class="form-control" name="detail[]" id="detail"
                                                   style="display:none" required>
                                        </td>
                                        <td width="5%" style="padding-left: 10px;">

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </center>
                            <br>
                            <div>
                                <center>
                                    <div class="btn-group">
                                        <button type="button" id="saveAdddeduct"
                                                class="btn btn-block btn-success btn-sm"><i class="fa fa-save"></i>
                                            บันทึก
                                        </button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="reset" class="btn btn-block btn-danger btn-sm"><i
                                                    class="fa fa-reply"></i> ล้างข้อมูล
                                        </button>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- /.box -->
    </section><!-- /.content -->
<?php if (Yii::$app->session->hasFlash('loginfail')): ?>
    <div id="message">
        <br/><br/><br/>
        <?php echo \yii\bootstrap\Alert::widget([
            'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('loginfail'), 'body'),
            'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('loginfail'), 'options'),
        ]); ?>
    </div>

<?php endif; ?>