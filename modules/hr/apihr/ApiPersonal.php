<?php


/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/1/2017 AD
 * Time: 10:50
 */

namespace app\modules\hr\apihr;

use app\modules\hr\models\BLeveleducation;
use app\modules\hr\models\Btitle;
use app\modules\hr\models\Salarystep;
use yii\helpers\ArrayHelper;
use Yii;

class ApiPersonal
{

    public static function getArrayEducationLevel()
    {
        $eduLevel = BLeveleducation::find()->where([
            'status_active' => 1,
        ])->all();
        return ArrayHelper::map($eduLevel, 'level_education', 'level_education');
    }

    public static function getBenameth($typeGender)
    {
        $data = Btitle::find()
            ->where('gender = :typeGender
                     AND status_active != 99
                     AND title_name_th != ""
                ')
            ->addParams([':typeGender' => $typeGender,
            ])
            ->asArray()
            ->all();

        return $data;
    }

    public static function getBenameen($typeGender)
    {
        $data = Btitle::find()
            ->where('gender = :typeGender
                     AND status_active != 99
                     AND title_name_en != ""
                ')
            ->addParams([':typeGender' => $typeGender,
            ])
            ->asArray()
            ->all();

        return $data;
    }

    public static function configchangesalary()
    {
        $data = [
            '1' => "ปรับกระบอกเงินเดือน",
            '2' => "ปรับตำแหน่ง",
            '3' => "ปรับผังเงินเดือน",
            '4' => "ผ่านทดลองงานดี",
            '5' => "ปรับย้ายสายงาน",
            '6' => "ปรับขั้นเงินเดือน"
        ];
        return $data;
    }

    public static function configUniversity()
    {
        /*
        $data = ['1' => "จุฬาลงกรณ์มหาวิทยาลัย",
            '2' => "มหาวิทยาลัยเชียงใหม่",
            '3' => "มหาวิทยาลัยเกษตรศาสตร์",
            '4' => "มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี",
            '5' => "มหาวิทยาลัยธรรมศาสตร์",
            '6' => "มหาวิทยาลัยมหิดล"];
           */
        $data = ['จุฬาลงกรณ์มหาวิทยาลัย' => "จุฬาลงกรณ์มหาวิทยาลัย",
            'มหาวิทยาลัยเชียงใหม่' => "มหาวิทยาลัยเชียงใหม่",
            'มหาวิทยาลัยเกษตรศาสตร์' => "มหาวิทยาลัยเกษตรศาสตร์",
            'มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี' => "มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี",
            'มหาวิทยาลัยธรรมศาสตร์' => "มหาวิทยาลัยธรรมศาสตร์",
            'มหาวิทยาลัยมหิดล' => "มหาวิทยาลัยมหิดล"];

        return $data;
    }

    public static function configBank()
    {
        $data = ['ธนาคารกรุงไทย' => "ธนาคารกรุงไทย",
            'ธนาคารไทยพาณิชย์' => "ธนาคารไทยพาณิชย์",
            'ธนาคารกรุงเทพ' => "ธนาคารกรุงเทพ",
            'ธนาคารกสิกรไทย' => "ธนาคารกสิกรไทย",
            'ธนาคารกรุงศรีอยุธยา' => "ธนาคารกรุงศรีอยุธยา",
            'ธนาคารทหารไทย' => "ธนาคารทหารไทย",
            'ธนาคารเกียรตินาคิน' => "ธนาคารเกียรตินาคิน",
            'ธนาคารซีไอเอ็มบีไทย' => "ธนาคารซีไอเอ็มบีไทย",
            'ธนาคารทิสโก้' => "ธนาคารทิสโก้",
            'ธนาคารธนชาต' => "ธนาคารธนชาต",
        ];
        return $data;
    }

    public static function bloodGroup()
    {
        $data = [
            'A' => "A",
            'B' => "B",
            'AB' => "AB",
            'O' => "O",
        ];
        return $data;
    }

    public static function getsalarychart()
    {
        $model = Salarystep::find()
            ->select('SALARY_STEP_CHART_NAME')
            ->where('SALARY_STEP_STATUS = "1"')
            ->groupby('SALARY_STEP_CHART_NAME')
            ->asArray()
            ->all();
        return $model;
    }


}

?>

