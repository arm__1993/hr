<?php

namespace app\modules\wscommon\models;

use Yii;
class Maincusdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
      /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }
    public static function tableName()
    {
        return 'MainCusData';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Date_Receive', 'UpdateT', 'respon_date'], 'safe'],
            [['CusNo_Old', 'BeType', 'company_type', 'cus_branch_id', 'cus_branch_txt', 'Cus_Big_Picture', 'Cus_Small_Picture', 'mary_id', 'C_copy', 'C_GEO', 'Cus_Coppy', 'Cus_GEO', 'JobID', 'JobSegmentGroup', 'W_Coppy', 'W_GEO', 'Resperson', 'Resperson_posCode', 'respon_date', 'Time_for_talk', 'Pleasure_value', 'rcheck', 'cleansing_status', 'cleansing_date', 'status_up'], 'required'],
            [['Picture', 'C_Add', 'Cus_Add', 'Job_Des', 'W_Add', 'Remark'], 'string'],
            [['CusNo_Old', 'Be', 'mary_id', 'C_AddGPSLatitude', 'C_AddGPSLongtitude', 'Cus_AddGPSLatitude', 'Cus_AddGPSLongtitude', 'Cus_WGPSLatitude', 'Cus_WGPSLongtitude', 'Email', 'Time_for_talk'], 'string', 'max' => 100],
            [['BeType', 'C_Code', 'Cus_Code', 'W_Code'], 'string', 'max' => 5],
            [['company_type', 'rcheck'], 'string', 'max' => 1],
            [['Cus_Name'], 'string', 'max' => 200],
            [['Cus_Surename', 'JobID', 'Title', 'W_Email', '1stSourceData'], 'string', 'max' => 50],
            [['Cus_Nickname', 'Status', 'W_Fax', 'Resperson'], 'string', 'max' => 15],
            [['cus_branch_id', 'cus_branch_txt', 'C_AddMapDescribe', 'Cus_AddMapDescribe', 'Cus_WMapDescribe'], 'string', 'max' => 250],
            [['Cus_Big_Picture', 'Cus_Small_Picture'], 'string', 'max' => 255],
            [['Sex', 'Cus_Tum', 'Cus_Type', 'Pleasure_value'], 'string', 'max' => 20],
            [['DateOfBirth', 'C_copy', 'Cus_Coppy', 'W_Coppy'], 'string', 'max' => 10],
            [['Cus_IDNo', 'Data_Received_Num', 'UpdaterNO'], 'string', 'max' => 13],
            [['Primary_Edu', 'Secondary_Edu', 'Highschool_Edu', 'Bachelor', 'Master', 'Phd', 'C_Vill', 'C_Tum', 'C_Amp', 'C_Pro', 'Cus_Vil', 'Cus_Amp', 'Job', 'W_Vill', 'W_Tum', 'W_Amp', 'W_Pro', 'Resperson_posCode', 'cleansing_status', 'cleansing_date', 'status_up'], 'string', 'max' => 30],
            [['C_GEO', 'Cus_Pro', 'Cus_GEO', 'W_GEO'], 'string', 'max' => 25],
            [['NotMail'], 'string', 'max' => 3],
            [['H_Tel', 'JobSegmentGroup', 'W_Tel', 'Pagger', 'M_Tel'], 'string', 'max' => 150],
            [['W_Name'], 'string', 'max' => 80],
            [['Data_Received', 'Updater'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Date_Receive' => ' วันที่รับข้อมูล',
            'CusNo' => 'Cus No',
            'CusNo_Old' => '๏ฟฝ๏ฟฝ๏ฟฝ๏ฟฝ๏ฟฝลข๏ฟฝูก๏ฟฝ๏ฟฝ๏ฟฝ',
            'Picture' => 'รูปภาพลูกค้า',
            'Be' => 'คำนำหน้า',
            'BeType' => '1= บุคคล , 2 = ชมรม / สมาคม / นิติบุคล',
            'company_type' => '1=สำนักงานใหญ่,2=สาขา',
            'Cus_Name' => 'ชื่อ',
            'Cus_Surename' => 'นามสกุล',
            'Cus_Nickname' => 'ชื่อเล่น',
            'cus_branch_id' => 'เลขสาขาลูกค้า',
            'cus_branch_txt' => 'ชื่อสาขาลูกค้า',
            'Cus_Big_Picture' => 'ภาพขนาดใหญ่',
            'Cus_Small_Picture' => 'ภาพขนาดย่อ',
            'Sex' => 'เพศ',
            'DateOfBirth' => 'วันเกิด',
            'Cus_IDNo' => 'เลขบัตรประชาชน',
            'Primary_Edu' => 'ประถมศึกษา',
            'Secondary_Edu' => 'ม.ต้น',
            'Highschool_Edu' => 'ม.ปลาย / ปวช.',
            'Bachelor' => 'ปริญญาตรี',
            'Master' => 'ปริญญาโท',
            'Phd' => 'ปริญญาเอก',
            'Status' => 'สถานะ',
            'mary_id' => 'รหัสคู่สมรส',
            'C_copy' => 'C Copy',
            'C_Add' => 'ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Vill' => 'หมู่บ้านที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Tum' => 'ตำบล ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Amp' => 'อำเภอ ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_Pro' => 'จังหวัด ที่อยู่ตามบัตรปชช/ทะเบียนบ้าน/ทะเบียนพาณิชย์',
            'C_GEO' => 'ภาค',
            'C_Code' => 'รหัสไปรษณีย์',
            'C_AddGPSLatitude' => 'ที่อยู่ทางการ GPS ละติจูด',
            'C_AddGPSLongtitude' => 'ที่อยู่ทางการ GPS ลองติจูด',
            'C_AddMapDescribe' => 'คำอธิบายแผนที่',
            'Cus_Coppy' => 'Cus  Coppy',
            'Cus_Add' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Vil' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Tum' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Amp' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Pro' => 'ที่อยู่ส่งเอกสาร',
            'Cus_GEO' => 'ที่อยู่ส่งเอกสาร',
            'Cus_Code' => 'ที่อยู่ส่งเอกสาร',
            'Cus_AddGPSLatitude' => 'ที่อยู่ส่งเอกสาร GPS ละติจูด',
            'Cus_AddGPSLongtitude' => 'ที่อยู่ส่งเอกสาร GPS ลองติจูด',
            'Cus_AddMapDescribe' => 'คำอธิบายแผนที่',
            'NotMail' => 'ไม่ส่งจดหมาย 1=Yes/No',
            'H_Tel' => 'โทรศัพท์บ้าน',
            'JobID' => 'refer job id',
            'Job' => 'อาชีพ',
            'JobSegmentGroup' => 'กลุ่มอาชีพ',
            'Job_Des' => 'ส่วนขยายอาชีพ',
            'W_Name' => 'ชื่อที่ทำงาน',
            'Title' => 'ตำแหน่ง',
            'W_Coppy' => 'W  Coppy',
            'W_Add' => 'ที่ทำงาน',
            'W_Vill' => 'ที่ทำงาน',
            'W_Tum' => 'ที่ทำงาน',
            'W_Amp' => 'ที่ทำงาน',
            'W_Pro' => 'ที่ทำงาน',
            'W_GEO' => 'ที่ทำงาน',
            'W_Code' => 'ที่ทำงาน',
            'W_Tel' => 'ที่ทำงาน',
            'W_Fax' => 'ที่ทำงาน',
            'W_Email' => 'ที่ทำงาน',
            'Cus_WGPSLatitude' => 'ที่อยู่ส่งเอกสารที่ทำงาน GPS ละติจูด',
            'Cus_WGPSLongtitude' => 'ที่อยู่ส่งเอกสาร GPS ลองติจูด',
            'Cus_WMapDescribe' => 'คำอธิบายแผนที่',
            'Pagger' => 'เพจเจอร์',
            'M_Tel' => 'มือถือ',
            'Email' => 'Email',
            'Cus_Type' => 'ชนิดลูกค้า',
            'Data_Received' => 'ผู้รับข้อมูล',
            'Data_Received_Num' => 'ผู้รับข้อมูลNo',
            '1stSourceData' => 'แหล่งข้อมูลครั้งแรก',
            'Remark' => 'หมายเหตุ',
            'Updater' => 'ผู้ป้อนข้อมูล',
            'UpdaterNO' => 'ผู้ป้อนข้อมูล ID',
            'UpdateT' => 'วันที่Update',
            'Resperson' => 'ผู้รับผิดชอบลูกค้า',
            'Resperson_posCode' => 'รหัสตำแหน่งผู้รับผิดชอบลูกค้า',
            'respon_date' => 'วันที่เริ่มดูแลลูกค้า',
            'Time_for_talk' => 'เวลาที่ลูกค้าสะดวกคุย',
            'Pleasure_value' => 'ค่าความพึงพอใจ',
            'rcheck' => 'Rcheck',
            'cleansing_status' => 'Cleansing Status',
            'cleansing_date' => 'Cleansing Date',
            'status_up' => 'Status Up',
        ];
    }


       public function searchcustomerdataforsalemobile($Cus_Name,$Cus_Surename,$Cus_IDNo) // param
    {   
        $Cus_Name = "%".$Cus_Name."%";
        $Cus_Surename = "%".$Cus_Surename."%";
        $Cus_IDNo = "%".$Cus_IDNo."%";
        $datacustomer = Yii::$app->dbERP_maincusdata
       ->createCommand("select * from MainCusData where Cus_Name LIKE :Cus_Name 
        AND Cus_Surename LIKE :Cus_Surename AND Cus_IDNo LIKE :Cus_IDNo
        ")
       ->bindParam(':Cus_Name',$Cus_Name)
       ->bindParam(':Cus_Surename',$Cus_Surename)
       ->bindParam(':Cus_IDNo',$Cus_IDNo)
       ->queryAll();
        return $datacustomer;

    }
}
