<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:11
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

?>
<style type="text/css">
      .modal-wide .modal-dialog {
               width: 80%; /* or whatever you wish */
        }
</style>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li>เพิ่มข้อมูลพนักงาน</li>
                <li class="active">ตำแหน่งเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
        <form class="form-horizontal">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">จัดการตำแหน่งและเงินเดือน</h3>
                </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                            &nbsp;
                            </div>
                            <div class="form-group">
                                <label for="genderEmp" class="col-sm-2 control-label">รูปแบบการรับเงิน</label>
                                <div class="col-sm-6">
                                <input type="radio" name="genderEmp" id="genderEmp"  >
                                <span>เงินโอน</span>
                                <input type="radio" name="genderEmp" id="genderEmp"  >
                                <span>เงินสด</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="nameEmp" class="col-sm-3 control-label">ธนาคาร:</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="nameEmp" size="12">
                                </div>
                                <label for="nameEmp" class="col-sm-1 control-label">หมายเลขบัญชี:</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="nameEmp" size="12">
                                </div>
                            </div>
                        </div>
                        <div align="right"> 
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">เพิ่มข้อมูล</button>
                        </div>
                            <div class="modal fade modal-wide" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 id="myModalLabel" class="modal-title">จัดการตำแหน่งและเงินเดือน</h4>
                                        </div>
                                        <form action="#" method="post">
                                        <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                        <div class="modal-body" width="100%" class="table table-bordered table-hover dataTable">
                                            <div class="form-group">
                                            <label for="genderEmp" class="col-sm-2 control-label">บริษัท</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            <label for="genderEmp" class="col-sm-1 control-label">วันที่มีผล</label>
                                                <div class="col-sm-3">
                                                    <input type="date">
                                                </div> 
                                            </div>
                                            <div class="form-group">
                                            <label for="genderEmp" class="col-sm-2 control-label">แผนก</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            <label for="genderEmp" class="col-sm-1 control-label">ลักษณะการเปลี่ยนแปลง</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            </div>
                                            <div class="form-group">
                                            <label for="genderEmp" class="col-sm-2 control-label">ฝ่าย</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            <label for="genderEmp" class="col-sm-1 control-label">ผังเงินเดือน</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            </div>
                                            <div class="form-group">
                                            <label for="genderEmp" class="col-sm-2 control-label">ระดับ</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            <label for="genderEmp" class="col-sm-1 control-label">ลกระบอก</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            </div>
                                            <div class="form-group">
                                            <label for="genderEmp" class="col-sm-2 control-label">ตำแหน่งงาน</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            <label for="genderEmp" class="col-sm-1 control-label">ขั้น</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control">
                                                    <option>HBSO</option>
                                                    </select>
                                                </div> 
                                            </div>
                                            <div class="form-group">
                                            <label for="genderEmp" class="col-sm-2 control-label"></label>
                                                <div class="col-sm-3">
                                                    
                                                </div> 
                                            <label for="genderEmp" class="col-sm-1 control-label">ขั้นที่เพิ่ม</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" Disabled>
                                                </div> 
                                            </div>
                                            <div class="form-group">
                                            <label for="genderEmp" class="col-sm-2 control-label"></label>
                                                <div class="col-sm-3">

                                                </div> 
                                            <label for="genderEmp" class="col-sm-1 control-label">เงินเดือน</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" Disabled>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        <input type="submit" class="btn btn-primary" value="บันทึกข้อมูล">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                        </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->  
                        <br>
                       <table class="table table-bordered table-hover dataTable">
                            <tbody>
                                <tr>

                                    <th>ลำดับ</th>
                                    <th>ตำแหน่งสังกัด</th>
                                    <th>รหัสตำแหน่ง</th>
                                    <th>ตำแหน่ง</th>
                                    <th>ระยะเวลาสิ้นสุดการใช้งาน</th>
                                    <th>การจ่ายเงินเดือน</th>
                                    <th>ผังเงินเดือน</th>
                                    <th>กระบอก</th>
                                    <th>ขั้น</th>
                                    <th>เงินเดือน</th>
                                    <th>การจัดการ</th>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content --> 