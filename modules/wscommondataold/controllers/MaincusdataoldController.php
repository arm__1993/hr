<?php

namespace app\modules\wscommondataold\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommondataold\models\Maincusdataold;
use yii\helpers\ArrayHelper;

class MaincusdataoldController extends ActiveController
{
 
   	public $modelClass = 'app\modules\wscommondataold\models\Maincusdataold';
	//public $modelClass = 'app\modules\wscommon\models\Testmaincus';

	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}


    public function actionInsertmaincusdataold()
    {

        $Be = $_POST['Be'];
        $BeType = $_POST['BeType'];
        $Cus_Name = $_POST['Cus_Name'];
        $Cus_Surename = $_POST['Cus_Surename'];
        $Cus_Nickname = $_POST['Cus_Nickname'];
        $Sex = $_POST['Sex'];
        $DateOfBirth = $_POST['DateOfBirth'];
        $Cus_IDNo_Type = $_POST['Cus_IDNo_Type'];
        $Cus_IDNo = $_POST['Cus_IDNo'];
        $Married_Status = $_POST['Married_Status'];
        $Date_Receive = $_POST['Date_Receive'];
        $Data_Received = $_POST['Data_Received'];
        $Data_Received_Num = $_POST['Data_Received_Num'];
        $stSourceData = $_POST['1stSourceData'];
        $Remark = $_POST['Remark'];
        $Updater = $_POST['Updater'];
        $UpdaterNO = $_POST['UpdaterNO'];
        $UpdateT = $_POST['UpdateT'];
        $company_type = $_POST['company_type'];
        $cus_branch_id = $_POST['cus_branch_id'];
        $cus_branch_txt = $_POST['cus_branch_txt'];
 
        //return 1;
        // $Be = \Yii::$app->request->get('Be');
        // $Cus_Name = \Yii::$app->request->get('Cus_Name');
        // $Cus_Surename = \Yii::$app->request->get('Cus_Surename');
        $datanew = new Maincusdataold();
        $LastidInsert = $datanew->insertmaincusdataold($Be,
        												$BeType,
        												$Cus_Name,
        												$Cus_Surename,
        												$Cus_Nickname,
        												$Sex,
        												$DateOfBirth,
        												$Cus_IDNo_Type,
        												$Cus_IDNo,
        												$Married_Status,
        												$Date_Receive,
        												$Data_Received,
        												$Data_Received_Num,
        												$stSourceData,
        												$Remark,
        												$Updater,
        												$UpdaterNO,
        												$UpdateT,
        												$company_type,
        												$cus_branch_id,
        												$cus_branch_txt);
        // $dataInsert = new Testreation();
        // $Inserreation = $dataInsert->insertreationship($LastidInsert);

        echo "LastidInsert=".$LastidInsert;
        //return $Lastid;
    }

}
