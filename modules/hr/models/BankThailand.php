<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "bank_thailand".
 *
 * @property integer $id
 * @property string $bank_name
 * @property string $bank_icon
 * @property integer $set_default
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class BankThailand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_thailand';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['set_default', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['bank_name'], 'string', 'max' => 250],
            [['bank_icon'], 'string', 'max' => 50],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_name' => 'Bank Name',
            'bank_icon' => 'Bank Icon',
            'set_default' => 'Set Default',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }
}
