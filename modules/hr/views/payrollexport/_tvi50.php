
<?php


$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/payroll_report_pdf.css");


?>
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun";
        font-size: 16px;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }


</style>

<div class="row container">
    ทดสอบบบบบบบ

    <h3 style="text-align: center">รายงานสรุปส่วนเพิ่ม  รอบเงินเดือน </h3>
</div>
<!--

<div class="container" style="padding-top:0px" >
    ทดสอบบบฟหก ดฟหกดฟหกด ฟหกด
<p>ฉบับที่1 (สำหรับผู้ถูกหักภาษี ณ ที่จ่าย ใช้แนบพร้อมกับแบบแสดงรายการภาษี) </p><br>
<font size='1'>ฉบับที่2 (สำหรับผู้ถูกหักภาษี ณ ที่จ่าย เก็บไว้เป็นหลักฐาน)</font>
  <table width="100%" heigth='100%' border='1' style="border-collapse: collapse; border: 1px solid #ccc padding-top:0px" class='borderunset' cellpadding='15' rowpadding='15' cellspacing='0'>
    <tr>
        <td>
            <center><h3>หนังสือรับรองการหักภาษี ณ ที่จ่าย</h3></center>
            <center><h6>ตามมาตรา 50 ทวิ แห่งประมวลรัษฎากร</h6></center>
        </td>
        <td width="20%" align="right">
            
                <p><font size='1'>เล่มที......................</font></p>
                <p><font size='1'>เลขที่......................</font></p>
            
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <table width="100%" class="roundedCorners  padding">
                <tr>
                    <td colspan='3'>
                        <font >ผู้ถูกหักภาษี ณ ที่จ่าย :</font>
                    </td>

                    <td>
                    </td>

                    <td colspan='2'>
                        <font size='3'>เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)*</font>
                    </td>

                    <td colspan='3' width='30%'>

                        <table  border='1' style="border-collapse: collapse; border: 1px solid #ccc padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                            <tr >
                                <td>xx</td>
                                <td style="border:0px;">-</td>
                                <td>rrr</td>
                                <td style="border-left:0px;">cc</td>
                                <td style="border-left:0px;">zxx</td>
                                <td style="border-left:0px;">vv</td>
                                <td style="border:0px;">-</td>
                                <td>xc</td>
                                <td style="border-left:0px;">dd</td>
                                <td style="border-left:0px;">dd</td>
                                <td style="border-left:0px;">d</td>
                                <td>ddd</td>
                                <td style="border:0px;">-</td>
                                <td>ddd</td>
                                <td style="border-left:0px;">ss</td>
                                <td style="border:0px;">-</td>
                                <td>dd</td>
                                
                            </tr>
                        </table>
                       
                    </td>

                </tr>
                <tr>
                    <td colspan='4' width="45%">
                        <table width="100%">
                            <tr>
                                <td border='0'></td>

                                </td>
                                <td style='padding-left: -25px;padding-bottom: -15px;'>
                                    <font size='6'>dd</font>
                                </td>
                            </tr>
                            <tr>
                                <td border='0' colspan='2'>
                                   <font size='3'>ชื่อ ......................................................................................................................</font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan='2' width="30%">
                        <font size='3'>เลขประจำตัวผู้เสียภาษีอากร</font>
                    </td>

                    <td colspan='3'  >
                        
                    </td>

                </tr>
                <tr>
                    <td colspan='4'>
                        <center><font size='1'>(ให้ระบุว่าเป็น บุคคล นิติบุคคล บริษัท สมาคม หรือคณะบุคคล)</font></center>
                    </td>
                    <td colspan='5' >
                    </td>

                </tr>
                <tr>
                    <td colspan='9' border='1'>
                        <tabel  border='1'>
                            <tr>
                                <td width="10%">
                                </td>
                                <td colspan='8' align="left" style='padding-left: -25px;padding-bottom: -15px;'>
                                    <font size='5'>sss</font>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='9'>
                                    <font size='3'>ที่อยู่ ............................................................................................................................................................................................................................................................................. </font>                            
                                </td>
                            </tr>
                        </tabel>
                    </td>
                </tr>

                <tr>
                    <td colspan='6'>
                        <center><font size='1'>(ให้ระบุชื่ออาคาร/หมู่บ้าน ห้องเลขที่ชั้นที่ เลขที่ ตรอก/ซอย หมู่ที่ถนน ตำบล/แขวง อำเภอ/เขต จังหวัด)</font></center>                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <table width="100%">
                <tr>
                    <td colspan='3'>
                        <font >ผู้ถูกหักภาษี ณ ที่จ่าย :</font>
                    </td>

                    <td>
                    </td>

                    <td colspan='2'>
                        <font size='3'>เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)*</font>
                    </td>

                    <td colspan='3' >
                        <table  border='1' style="border-collapse: collapse; border: 1px solid #ccc padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                            <tr >
                                <td>ddd</td>
                                <td style="border:0px;">-</td>
                                <td>adsf</td>
                                <td style="border-left:0px;">xxx</td>
                                <td style="border-left:0px;">yyy</td>
                                <td style="border-left:0px;">zzz</td>
                                <td style="border:0px;">-</td>
                                <td>xxx</td>
                                <td style="border-left:0px;">eee</td>
                                <td style="border-left:0px;">ewww</td>
                                <td style="border-left:0px;">sss</td>
                                <td>mm</td>
                                <td style="border:0px;">-</td>
                                <td>xxx</td>
                                <td style="border-left:0px;">xxxx</td>
                                <td style="border:0px;">-</td>
                                <td>aaa</td>
                                
                            </tr>
                        </table>
                       
                    </td>

                </tr>
                <tr>
                    <td colspan='4' width="45%">
                        <table width="100%">
                            <tr>
                                <td border='0'></td>

                                </td>
                                <td style='padding-left: -25px;padding-bottom: -15px;'>
                                    <font size='6'>dddd</font><br>
                                </td>
                            </tr>
                            <tr>
                                <td border='0' colspan='2'>
                                   <font size='3'>ชื่อ .........................................................................................................................</font>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                    <td colspan='2' width="30%">
                        <font size='3'>เลขประจำตัวผู้เสียภาษีอากร</font>
                    </td>

                    <td colspan='3' width="25%" >
                    </td>

                </tr>
                <tr>
                    <td colspan='4'>
                        <center><font size='1'>(ให้ระบุว่าเป็น บุคคล นิติบุคคล บริษัท สมาคม หรือคณะบุคคล)</font></center>
                    </td>
                    <td colspan='5' >
                    </td>

                </tr>
                <tr>
                    <td colspan='9'>
                        <tabel  border='1'>
                            <tr>
                                <td width="10%">
                                </td>
                                <td colspan='8' align="left" style='padding-left: -25px;padding-bottom: -15px;'>
                                    <font size='5'>xxxx <?php
                                        /*$address = $emp_data[0]['Address'];
                                          $address .= ' หมู่ที่ '.$emp_data[0]["Moo"];
                                          $address .= ' ถนน '.$emp_data[0]['Road'];
                                          $address .= ' ตำบล'.$emp_data[0]['SubDistrict'];
                                          $address .= ' อำเภอ'.$emp_data[0]['District'];
                                          $address .= ' จังหวัด'.$emp_data[0]['Province'];
                                          $address .= ' รหัสไปษณี '.$emp_data[0]['Postcode']; 
                                          echo $address;  */
                                    ?>
                                    </font>   
                                </td>
                            </tr>
                            <tr>
                                <td colspan='9'>
                                    <font size='3'>ที่อยู่ ............................................................................................................................................................................................................................................... </font>                            
                                </td>
                            </tr>
                        </tabel>
                    </td>
                </tr>

                <tr>
                    <td colspan='6'>
                        <center><font size='1'>(ให้ระบุชื่ออาคาร/หมู่บ้าน ห้องเลขที่ชั้นที่ เลขที่ ตรอก/ซอย หมู่ที่ถนน ตำบล/แขวง อำเภอ/เขต จังหวัด)</font></center>                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <table width="100%">
                <tr style="border-top:0px;">
                    <td colspan='2'>
                        ลำดับที่ <input tyoe='text'> ในแบบ
                    </td>
                    <td align="left" width="17%" >
                        <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (1) ภ.ง.ด.1ก</font>
                    </td>
                    <td align="left" width="17%">
                        <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (2) ภ.ง.ด.1ก พเศษ</font>
                    </td>
                    <td align="left" width="17%">
                        <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (3) ภ.ง.ด.2</font>
                    </td>
                    <td align="left" width="17%">    
                        <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (4) ภ.ง.ด.3</font>
                    </td>
                </tr>
                <tr style="border-top:0px;">
                    <td colspan='2'>
                        <font size='1'>(ให้สามารถอ้างอิงหรือสอบยันกันได้ระหว่างลำดับที่ตาม หนังสือรับรองฯ กับแบบยื่นรายการภาษีหักที่จ่าย)</font>
                    </td>
                    <td align="left" width="17%">
                        <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (5) ภ.ง.ด.2ก</font>
                    </td>
                    <td align="left">
                        <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (6) ภ.ง.ด.3ก</font>
                    </td>
                    <td align="left">
                        <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (7) ภ.ง.ด.53</font>
                    </td>
                    <td align="left">    
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <th colspan='2'>
            <table width="100%" border='1' cellpadding='15' cellspacing='-1'>
                <tr>
                    <td colspan='3' width="45%" style="border-left:0px;border-top:0px;">
                        <h6>ประเภทเงินได้พึงประเมินจ่าย</h6>
                    </td>
                    <td width="10%" style="border-left:0px;border-top:0px;">
                        <h6>วัน ดือน หรือปีภาษีที่จ่าย</h6>
                    </td>
                    <td width="15%" style="border-left:0px;border-top:0px;">
                        <h6>จำนวนเงินที่จ่าย</h6>
                    </td>
                    <td width="10%" style="border-left:0px;border-top:0px;">
                        <h6>ภาษีท่ี่หักและนำส่งไว้</h6>
                    </td>
                </tr>
                <tr>
                    <td colspan='3' align="left" style="border-left:0px;border-top:0px;">
                        <font size='3'>1. เงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัส ฯลฯ ตามมาตรา 40 (1)</font>
                    </td >
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan='3' align="left" style="border-left:0px;border-top:0px;">
                        <font size='3'>2. ค่าธรรมเนียม ค่านายหน้า ฯลฯ ตามมาตรา 40 (2)</font>
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan='3' align="left" style="border-left:0px;border-top:0px;">
                        <font size='3'>3. ค่าแห่งลิขสิทธิ์ ฯลฯ ตามมาตรา 40 (3)</font>
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan='3' align="left" style="border-left:0px;border-top:0px;">
                        <ul class="list-group">
                            <li class="list-group-item"><font size='3'>4. (ก) ค่าดอกเบี้ย ฯลฯ ตามมาตรา 40(4) (ก)</font></li>
                            <li class="list-group-item">
                                <font size='3'>(ข) เงินปันผล เงินส่วนแบ่งกำไร ฯลฯ ตามมาตรา 40 (4) (ข)</font>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <font size='3'>(1) กิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราต่อไปนี้</font>
                                        <ul>
                                            <li>
                                               <font size='3'>(1.1) อัตราร้อยละ 30 ของกำไรสุทธิ	</font>
                                            </li>
                                            <li>
                                               <font size='3'>(1.2) อัตราร้อยละ 25 ของกำไรสุทธิ</font>
                                            </li>
                                            <li>
                                               <font size='3'>(1.3) อัตราร้อยละ 20 ของกำไรสุทธิ	</font>
                                            </li>
                                            <li>
                                                <font size='3'>(1.4) อัตราอื่น ๆ ระบุ _____________ ของกำไรสุทธิ</font>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <font size='3'>(2) กรณีผู้รับเงินปันผลไม่ได้รับเครดิตภาษี เนื่องจากจ่ายจาก</font>
                                        <ul>
                                            <li>
                                                <font size='3'>(2.1) กำไรสุทธิของกิจการที่ได้รับยกเว้นภาษีเงินได้นิติบุคคล	</font>
                                            </li>
                                            <li>
                                                <font size='3'>(2.2) เงินปันผลหรือเงินส่วนแบ่งของกำไรที่ได้รับยกเว้นไม่ต้องนำมารวมคำนวณเป็นรายได้เพื่อเสียภาษีเงินได้นิติบุคคล</font>
                                            </li>
                                            <li>
                                                <font size='3'>(2.3) กำไรสุทธิส่วนที่ได้หักผลขาดทุนสุทธิยกมาไม่เกิน 5 ปี ก่อนรอบระยะเวลาบัญชีปีปัจจุบัน</font>
                                            </li>
                                            <li>
                                                <font size='3'>(2.4) กำไรที่รับรู้ทางบัญชีโดยวิธีส่วนได้เสีย (equity method)</font>
                                            </li>
                                            <li>
                                                <font size='3'>(2.5) อื่นๆ (ระบุ)..................................................</font>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan='3' align="left" style="border-left:0px;border-top:0px;">
                        <font size='3'>5. การชำระเงินได้ที่ต้องหักภาษี ณ ที่จ่าย ตามคำสั่งกรมสรรพากรที่ออกตามมาตรา								
                        3 เตรส เช่น รางวัล ส่วนลดหรือประโยชน์ใด ๆ เนื่องจากการส่งเสริมการขาย								
                        รางวัลในการประกวด การแข่งขัน การชิงโชค คำแสดงของนักแสดงสาธารณะ 								
                        ค่าจ้างทำของ ค่าโฆษณา ค่าเช่า ค่าขนส่ง ค่าบริการ ค่าเบี้ยประกันวินาศภัย ฯลฯ</font>
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan='3' align="left" style='padding-top:5px;' margin-bottom='-2' margin-top='20'>
                        <p><center ><font size='3' ><b>เขียนโปรแกรม</b></font></center></p>
                        <p style='padding-top:-5px;'><font size='3' >6. อื่น ๆระบุ ................................................................................................................</font></p>
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan='4' align='right' style="border-left:0px;border-top:0px;">
                        รวมเงินที่จ่ายและภาษีที่หักนำส่ง
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                    <td style="border-left:0px;border-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan='6' align='left' style="border-left:0px;border-top:0px;">
                       รวมเงินภาษีที่หักนำส่ง ................................................................................................................
                    </td>
                </tr>
                <tr>
                    <td colspan='6'>
                        <table width='100%'>
                            <tr>
                                <td align='left'>
                                    ผู้จ่ายเงิน
                                </td>
                                <td align='left'>
                                    <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (1) หัก ณ ที่จ่าย</font>
                                </td>
                                <td align='left'>
                                    <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (2) ออกให้ตลอดไป</font>
                                </td>
                                <td align='left'>
                                    <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (3) ออกให้ครั้งเดียว</font>
                                </td>
                                <td align='left' >
                                    <input type="checkbox" name="vehicle" value="Bike"><font size='3'> (4) อื่น ๆ (ระบุ) ..................................</font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr >
                    <td colspan='2' valign="top">
                        <table  hight='100%' >
                            <tr rowspan='4' >
                                <td width='22%' valign="top" >
                                    คำเตือน
                                </td>
                                <td align='left'>
                                    <P align='left'>
                                        <font size='3' align='left' >
                                            ผู้มีหน้าที่ออกหนังสือรับรองการหักภาษี ณ ที่จ่าย
                                            ฝ่าฝืนไม่ปฏิบัติตามมาตรา 50 ทวิ แห่งประมวล 
                                            รัษฏากร ต้องรับโทษทางอาญาตามมาตรา 35 
                                            แห่งประมวณรัษฏากร
                                          </font>  
                                    </P>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan='4'>
                         <table>
                            <tr>
                                <td>
                                    ขอรับรองว่าข้อความและตัวเลขดังกล่าวข้างต้นถูกต้องตรงกับความจริงทุกประการ
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ลงชื่อ ............................. ผู้มีหน้าที่หักภาษี ณ ที่จ่าย
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ............./............................./...............
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    วัน เดือน ปี ที่ออกหนังสือรับรอง
                                </td>
                            </tr>
                         </table>       
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td valign="top">
            <font size='1' >หมายเหตุ เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)* หมายถึง</font>
        </td>
        <td>
            <font size='1' >
            1. กรณีบคคลธรรมดาไทย ให้ใช้ เลขประจำตัวประชาชนของกรมการปกครอง<br>
            2. กรณนีติบิคคล ให้ใช้เลขทะเบียนนิติบุคคลของกรมพัฒนาธุรกิจการค้า<br>
            3. กรณอื่นๆ นอกเหนื่อจาก 1. และ 2. ให้ใช้ เลขประจำตัวผู้เสียภาษี อากร (13 หลัก) ของกรมสรรพากร
            </font>
        </td>
    </tr>
</table>
</div>
-->