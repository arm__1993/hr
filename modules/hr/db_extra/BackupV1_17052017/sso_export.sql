-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 18, 2017 at 03:31 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `TEST_INTERN`
--

-- --------------------------------------------------------

--
-- Table structure for table `Detail_Record`
--

CREATE TABLE `Detail_Record` (
  `ID_Detail` int(11) NOT NULL,
  `ID_Header` int(11) NOT NULL,
  `RECORD_TYPE` char(1) CHARACTER SET utf8 NOT NULL,
  `SSO_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `PREFIX` char(3) CHARACTER SET utf8 NOT NULL,
  `FNAME` char(30) CHARACTER SET utf8 NOT NULL,
  `LNAME` char(35) CHARACTER SET utf8 NOT NULL,
  `WAGES` char(14) CHARACTER SET utf8 NOT NULL,
  `PAID_AMOUNT` char(12) CHARACTER SET utf8 NOT NULL,
  `BLANK` char(27) CHARACTER SET utf8 NOT NULL,
  `COMPANY_ID` char(3) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Detail_Record`
--

INSERT INTO `Detail_Record` (`ID_Detail`, `ID_Header`, `RECORD_TYPE`, `SSO_ID`, `PREFIX`, `FNAME`, `LNAME`, `WAGES`, `PAID_AMOUNT`, `BLANK`, `COMPANY_ID`) VALUES
(1, 1, '2', '2512345678901', '003', 'วราวุธ', 'ฟูแก้ว', '00000000015000', '000000075000', '', '10'),
(2, 1, '2', '3257481369584', '004', 'พัชรนันท์', 'โมราพิพัฒน์กุล', '00000000014000', '000000075000', '', '10'),
(3, 2, '2', '1579321456987', '003', 'มานะ', 'ประสพธรรม', '00000000014000', '000000075000', '', '11'),
(4, 1, '2', '2369854785125', '003', 'เฉิน', 'shen', '00000000025000', '000000100000', '', '12'),
(5, 3, '2', '2332547964147', '003', 'สิทธิพงศ์', 'อนันตภูมิ', '00000000020000', '000000100000', '', '12'),
(6, 1, '2', '5214785214558', '003', 'จักรพันธ์', 'บุญนุ่ม', '00000000030000', '000000150000', '', '11'),
(7, 1, '2', '4725566983140', '004', 'ธนาภรณ์', 'ทองอันชา', '00000000030000', '000000150000', '', '11');

-- --------------------------------------------------------

--
-- Table structure for table `Header_Record`
--

CREATE TABLE `Header_Record` (
  `ID_Header` int(11) NOT NULL,
  `RECORD_TYPE` char(1) CHARACTER SET utf8 NOT NULL,
  `ACC_NO` char(10) CHARACTER SET utf8 NOT NULL,
  `BRANCH_NO` char(6) CHARACTER SET utf8 NOT NULL,
  `PAID_DATE` char(6) CHARACTER SET utf8 NOT NULL,
  `PAID_PERIOD` char(4) CHARACTER SET utf8 NOT NULL,
  `COMPANY_NAME` char(45) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `RATE` char(4) CHARACTER SET utf8 NOT NULL,
  `TOTAL_EMPLOYEE` char(6) CHARACTER SET utf8 NOT NULL,
  `TOTAL_WAGES` char(15) CHARACTER SET utf8 NOT NULL,
  `TOTAL_PAID` char(14) CHARACTER SET utf8 NOT NULL,
  `TOTAL_PAID_BY_EMPLOYEE` char(12) CHARACTER SET utf8 NOT NULL,
  `TOTAL_PAID_BY_EMPLOYER` char(12) CHARACTER SET utf8 NOT NULL,
  `YEARS` int(11) NOT NULL,
  `MONTHS` int(11) NOT NULL,
  `DEPARTMENT_ID` int(11) NOT NULL,
  `COMPANY_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Header_Record`
--

INSERT INTO `Header_Record` (`ID_Header`, `RECORD_TYPE`, `ACC_NO`, `BRANCH_NO`, `PAID_DATE`, `PAID_PERIOD`, `COMPANY_NAME`, `RATE`, `TOTAL_EMPLOYEE`, `TOTAL_WAGES`, `TOTAL_PAID`, `TOTAL_PAID_BY_EMPLOYEE`, `TOTAL_PAID_BY_EMPLOYER`, `YEARS`, `MONTHS`, `DEPARTMENT_ID`, `COMPANY_ID`) VALUES
(1, '1', '1090002301', '000000', '150447', '0347', 'ผู้บริหาร กลุ่ม อีซูซุเชียงราย', '0300', '000100', '000000150000000', '00000009000000', '000004500000', '000004500000', 2017, 5, 9, 10),
(2, '1', '1090002965', '000000', '150854', '0347', 'บริษัท วงศ์ระวีวรรณ จำกัด', '0300', '000100', '000000150000000', '00000009000000', '000004500000', '000004500000', 2013, 8, 7, 11),
(3, '1', '1090002965', '000000', '150854', '0347', 'บริษัท สินทรัพย์วงศ์ระวี จำกัด', '0300', '000100', '000000150000000', '00000009000000', '000004500000', '000004500000', 2017, 8, 9, 12),
(4, '1', '1090002965', '000000', '150854', '0347', 'บริษัท วงศ์ระวีวรรณ จำกัด', '0300', '000100', '000000200000000', '00000009000000', '000004500000', '000004500000', 2014, 5, 7, 11),
(5, '1', '1090002301', '000000', '150447', '0347', 'ผู้บริหาร กลุ่ม อีซูซุเชียงราย', '0300', '000100', '000000150000000', '00000009000000', '000004500000', '000004500000', 2016, 1, 9, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Detail_Record`
--
ALTER TABLE `Detail_Record`
  ADD PRIMARY KEY (`ID_Detail`);

--
-- Indexes for table `Header_Record`
--
ALTER TABLE `Header_Record`
  ADD PRIMARY KEY (`ID_Header`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Detail_Record`
--
ALTER TABLE `Detail_Record`
  MODIFY `ID_Detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `Header_Record`
--
ALTER TABLE `Header_Record`
  MODIFY `ID_Header` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
