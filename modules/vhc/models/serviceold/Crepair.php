<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "crepair".
 *
 * @property integer $id
 * @property string $repair_id
 * @property string $name
 * @property string $emp_id
 * @property string $add_time
 * @property string $sdel
 */
class Crepair extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crepair';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repair_id', 'name', 'emp_id', 'add_time', 'sdel'], 'required'],
            [['repair_id'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 100],
            [['emp_id'], 'string', 'max' => 13],
            [['add_time'], 'string', 'max' => 20],
            [['sdel'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repair_id' => 'Repair ID',
            'name' => 'Name',
            'emp_id' => 'Emp ID',
            'add_time' => 'Add Time',
            'sdel' => 'Sdel',
        ];
    }
}
