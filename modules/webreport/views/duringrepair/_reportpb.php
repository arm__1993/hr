<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use app\modules\webreport\apiwebreport\ApiReport;
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$workOverTime = $session->get('workOverTime');
$selected_company = $session->get('selected_company');
$selected_daterange = $session->get('selected_daterange');
$selected_technician = $session->get('selected_technician');


?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานสถานะใบงานซ่อมที่เกินเวลานัดหมายส่งมอบ</h3>
<h5><?php  $namecompany = ApiReport::getNameCompanyPDF($selected_company) ;
        echo $namecompany['0']['name']; ?></h5>
  <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- AREA TABLE -->
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title"></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                            data-toggle="tooltip"
                                            title="Collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                    <div class="row">
                                        <div class="col-sm-6"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="example1" class="table table-bordered table-hover" role="grid"
                                                   aria-describedby="example2_info">
                                                <thead>
                                                <tr role="row" class="odd">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ลำดับ
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        เลขที่ใบงาน
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        วันที่เข้ารับบริการ
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ทะเบียน
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        สถานะใบงาน
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        เวลาที่เกิน (นาที)
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        SA
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ช่างซ่อม
                                                    </th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending">
                                                        ประเภทใบงานซ่อม
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $item = 1;
                                                foreach ($workOverTime as $value) {
                                                    ?>
                                                    <tr role="row" class="odd">
                                                        <td class="sorting_1" align = "center"><?php echo $item; ?></td>
                                                        <td align = "center"><?php echo $value['id']; ?></td>
                                                        <td align = "center"><?php echo DateTime::CalendarDate($value['guard_time']); ?></td>
                                                        <td align = "center"><?php echo $value['register']; ?></td>
                                                        <td align = "center"><?php echo $value['status']; ?></td>
                                                        <td align = "center"><?php $TotalMoreTime = $value['TotalMoreTime'];
                                                            $arr_Time_total = DateTime::calculateTimeElapsed($TotalMoreTime);
                                                    echo   DateTime::getFormatTime($arr_Time_total['H'],$arr_Time_total['m'],$arr_Time_total['s']);
                                                         ?>
                                                        </td>
                                                        <td align = "center"><?php echo $value['sa_name']; ?></td>
                                                        <td align = "center"><?php echo $value['technician_name']; ?></td>
                                                        <td align = "center"><?php echo $value['typeRepair']['0']; ?></td>
                                                    </tr>
                                                    <?php $item++;
                                                } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div><!--example2_wrapper-->
                            </div><!--box-body-->
                        </div><!--box box-success-->
                        <!-- AREA TABLE -->
                    </div>
                </div>
            </section>
<?php
session_start();
session_destroy();
?>