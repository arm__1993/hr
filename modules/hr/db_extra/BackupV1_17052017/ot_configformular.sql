-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2017 at 09:46 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_configformular`
--

CREATE TABLE `ot_configformular` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `formular` varchar(200) NOT NULL COMMENT 'บันทึกข้อความสูตรคำนวณ',
  `pay_motel` decimal(8,2) NOT NULL COMMENT 'ค่าที่พักจ่ายตามจริง ต่อคืน ต่อคน ต่อห้อง',
  `pay_service_twoway` decimal(8,2) NOT NULL COMMENT 'ค่าเดินทางมาให้บริการที่บริษัทไปกลับรวม',
  `ot_ratio_workday_clockout` decimal(8,2) NOT NULL COMMENT 'อัตรการคิดโอทีหลังเลิกงานวันปกติ',
  `ot_ratio_holiday` decimal(8,2) NOT NULL COMMENT 'อัตราการคิดโอทีวันหยุด',
  `ot_ratio_holiday_clockout` decimal(8,2) NOT NULL COMMENT 'อัตราการคิดโอทีหลังเลิกงานวันหยุด',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0, ',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ot_configformular`
--

INSERT INTO `ot_configformular` (`id`, `formular`, `pay_motel`, `pay_service_twoway`, `ot_ratio_workday_clockout`, `ot_ratio_holiday`, `ot_ratio_holiday_clockout`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, 'ค่าเดินทาง = ระยะทางจากบริษัทไปถึงที่ลูกค้า(ไปกลับ) x 40 บาท / 90 (Km/H)', '500.00', '50.00', '1.50', '1.00', '10.00', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_configformular`
--
ALTER TABLE `ot_configformular`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
