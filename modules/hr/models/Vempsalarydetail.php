<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "V_Emp_salary_detail".
 *
 * @property string $be
 * @property string $empname
 * @property string $surname
 * @property string $ID_Card
 * @property string $workname
 * @property string $empposition
 * @property string $empSection
 * @property string $departmentname
 * @property string $wagesalary
 * @property integer $WAGE_ID
 * @property integer $WAGE_THIS_MONTH_ROW_NUM
 * @property string $WAGE_EMP_ID
 * @property string $WAGE_PAY_DATE
 * @property string $WAGE_POSITION_CODE
 * @property string $WAGE_POSITION_NAME
 * @property string $WAGE_SECTION_ID
 * @property string $WAGE_DEPARTMENT_ID
 * @property string $WAGE_WORKING_COMPANY
 * @property string $WAGE_BANK_NAME
 * @property string $WAGE_ACCOUNT_NUMBER
 * @property string $WAGE_GET_MONEY_TYPE
 * @property string $WAGE_SALARY_CHART
 * @property string $WAGE_SALARY_LEVEL
 * @property integer $WAGE_SALARY_STEP
 * @property string $WAGE_SALARY
 * @property string $WAGE_SALARY_BY_CHART
 * @property string $WAGE_TOTAL_ADDITION
 * @property string $WAGE_EARN_PLUS_ADD
 * @property string $Social_Salary
 * @property string $WAGE_TOTAL_DEDUCTION
 * @property string $WAGE_EARN_MINUS_DEDUCT
 * @property string $WAGE_NET_SALARY
 * @property string $WAGE_THIS_MONTH_CONFIRM
 * @property string $WAGE_THIS_MONTH_CONFIRM_BY
 * @property string $WAGE_THIS_MONTH_CONFIRM_DATE
 * @property integer $WAGE_THIS_MONTH_EMPLOYEE_LOCK
 * @property integer $WAGE_THIS_MONTH_DIRECTOR_LOCK
 * @property string $WAGE_THIS_MONTH_STATUS
 * @property string $WAGE_THIS_MONTH_BANK_CONFIRM_STATUS
 * @property string $WAGE_THIS_MONTH_BANK_CONFIRM_DATE
 * @property string $WAGE_THIS_MONTH_BANK_CONFIRM_BY
 * @property string $WAGE_REMARK
 * @property string $WAGE_CREATE_DATE
 * @property string $WAGE_CREATE_BY
 * @property string $WAGE_UPDATE_DATE
 * @property string $WAGE_UPDATE_BY
 * @property integer $ADD_DEDUCT_THIS_MONTH_ID
 * @property string $WAGE_THIS_MONTH_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_EMP_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_PAY_DATE
 * @property string $ADD_DEDUCT_THIS_MONTH_TMP_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_TMP_NAME
 * @property string $ADD_DEDUCT_THIS_MONTH_DETAIL_ID
 * @property string $ADD_DEDUCT_THIS_MONTH_DETAIL
 * @property string $ADD_DEDUCT_THIS_MONTH_AMOUNT
 * @property string $ADD_DEDUCT_THIS_MONTH_TYPE
 * @property string $ADD_DEDUCT_THIS_MONTH_STATUS
 * @property string $ADD_DEDUCT_THIS_MONTH_SLIP_STATUS
 * @property string $ADD_DEDUCT_THIS_MONTH_CREATE_DATE
 * @property string $ADD_DEDUCT_THIS_MONTH_CREATE_BY
 * @property string $ADD_DEDUCT_THIS_MONTH_UPDATE_DATE
 * @property string $ADD_DEDUCT_THIS_MONTH_UPDATE_BY
 */
class Vempsalarydetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'V_Emp_salary_detail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['be'], 'string'],
            [['empname', 'surname', 'ID_Card', 'workname', 'empSection', 'departmentname', 'wagesalary'], 'required'],
            [['wagesalary', 'WAGE_SALARY', 'WAGE_SALARY_BY_CHART', 'WAGE_TOTAL_ADDITION', 'WAGE_EARN_PLUS_ADD', 'Social_Salary', 'WAGE_TOTAL_DEDUCTION', 'WAGE_EARN_MINUS_DEDUCT', 'WAGE_NET_SALARY', 'ADD_DEDUCT_THIS_MONTH_AMOUNT'], 'number'],
            [['WAGE_ID', 'WAGE_THIS_MONTH_ROW_NUM', 'WAGE_SALARY_STEP', 'WAGE_THIS_MONTH_EMPLOYEE_LOCK', 'WAGE_THIS_MONTH_DIRECTOR_LOCK', 'ADD_DEDUCT_THIS_MONTH_ID'], 'integer'],
            [['WAGE_THIS_MONTH_CONFIRM_DATE', 'WAGE_THIS_MONTH_BANK_CONFIRM_DATE', 'WAGE_CREATE_DATE', 'WAGE_UPDATE_DATE', 'ADD_DEDUCT_THIS_MONTH_CREATE_DATE', 'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE'], 'safe'],
            [['empname', 'surname', 'workname', 'empSection', 'departmentname', 'WAGE_BANK_NAME', 'WAGE_ACCOUNT_NUMBER', 'WAGE_THIS_MONTH_BANK_CONFIRM_BY', 'WAGE_REMARK', 'WAGE_CREATE_BY', 'WAGE_UPDATE_BY', 'ADD_DEDUCT_THIS_MONTH_TMP_NAME', 'ADD_DEDUCT_THIS_MONTH_DETAIL', 'ADD_DEDUCT_THIS_MONTH_CREATE_BY', 'ADD_DEDUCT_THIS_MONTH_UPDATE_BY'], 'string', 'max' => 250],
            [['ID_Card'], 'string', 'max' => 14],
            [['empposition', 'WAGE_PAY_DATE', 'WAGE_POSITION_CODE', 'WAGE_SECTION_ID', 'WAGE_DEPARTMENT_ID', 'WAGE_WORKING_COMPANY', 'WAGE_THIS_MONTH_CONFIRM_BY', 'WAGE_THIS_MONTH_ID', 'ADD_DEDUCT_THIS_MONTH_EMP_ID', 'ADD_DEDUCT_THIS_MONTH_PAY_DATE', 'ADD_DEDUCT_THIS_MONTH_TMP_ID', 'ADD_DEDUCT_THIS_MONTH_DETAIL_ID'], 'string', 'max' => 100],
            [['WAGE_EMP_ID'], 'string', 'max' => 20],
            [['WAGE_POSITION_NAME', 'WAGE_SALARY_CHART', 'WAGE_SALARY_LEVEL'], 'string', 'max' => 200],
            [['WAGE_GET_MONEY_TYPE', 'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS'], 'string', 'max' => 1],
            [['WAGE_THIS_MONTH_CONFIRM', 'WAGE_THIS_MONTH_STATUS', 'ADD_DEDUCT_THIS_MONTH_TYPE', 'ADD_DEDUCT_THIS_MONTH_STATUS', 'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'be' => 'Be',
            'empname' => 'Empname',
            'surname' => 'Surname',
            'ID_Card' => 'Id  Card',
            'workname' => 'Workname',
            'empposition' => 'Empposition',
            'empSection' => 'Emp Section',
            'departmentname' => 'Departmentname',
            'wagesalary' => 'Wagesalary',
            'WAGE_ID' => 'Wage  ID',
            'WAGE_THIS_MONTH_ROW_NUM' => 'Wage  This  Month  Row  Num',
            'WAGE_EMP_ID' => 'Wage  Emp  ID',
            'WAGE_PAY_DATE' => 'Wage  Pay  Date',
            'WAGE_POSITION_CODE' => 'Wage  Position  Code',
            'WAGE_POSITION_NAME' => 'Wage  Position  Name',
            'WAGE_SECTION_ID' => 'Wage  Section  ID',
            'WAGE_DEPARTMENT_ID' => 'Wage  Department  ID',
            'WAGE_WORKING_COMPANY' => 'Wage  Working  Company',
            'WAGE_BANK_NAME' => 'Wage  Bank  Name',
            'WAGE_ACCOUNT_NUMBER' => 'Wage  Account  Number',
            'WAGE_GET_MONEY_TYPE' => 'Wage  Get  Money  Type',
            'WAGE_SALARY_CHART' => 'Wage  Salary  Chart',
            'WAGE_SALARY_LEVEL' => 'Wage  Salary  Level',
            'WAGE_SALARY_STEP' => 'Wage  Salary  Step',
            'WAGE_SALARY' => 'Wage  Salary',
            'WAGE_SALARY_BY_CHART' => 'Wage  Salary  By  Chart',
            'WAGE_TOTAL_ADDITION' => 'Wage  Total  Addition',
            'WAGE_EARN_PLUS_ADD' => 'Wage  Earn  Plus  Add',
            'Social_Salary' => 'Social  Salary',
            'WAGE_TOTAL_DEDUCTION' => 'Wage  Total  Deduction',
            'WAGE_EARN_MINUS_DEDUCT' => 'Wage  Earn  Minus  Deduct',
            'WAGE_NET_SALARY' => 'Wage  Net  Salary',
            'WAGE_THIS_MONTH_CONFIRM' => 'Wage  This  Month  Confirm',
            'WAGE_THIS_MONTH_CONFIRM_BY' => 'Wage  This  Month  Confirm  By',
            'WAGE_THIS_MONTH_CONFIRM_DATE' => 'Wage  This  Month  Confirm  Date',
            'WAGE_THIS_MONTH_EMPLOYEE_LOCK' => 'Wage  This  Month  Employee  Lock',
            'WAGE_THIS_MONTH_DIRECTOR_LOCK' => 'Wage  This  Month  Director  Lock',
            'WAGE_THIS_MONTH_STATUS' => 'Wage  This  Month  Status',
            'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS' => 'Wage  This  Month  Bank  Confirm  Status',
            'WAGE_THIS_MONTH_BANK_CONFIRM_DATE' => 'Wage  This  Month  Bank  Confirm  Date',
            'WAGE_THIS_MONTH_BANK_CONFIRM_BY' => 'Wage  This  Month  Bank  Confirm  By',
            'WAGE_REMARK' => 'Wage  Remark',
            'WAGE_CREATE_DATE' => 'Wage  Create  Date',
            'WAGE_CREATE_BY' => 'Wage  Create  By',
            'WAGE_UPDATE_DATE' => 'Wage  Update  Date',
            'WAGE_UPDATE_BY' => 'Wage  Update  By',
            'ADD_DEDUCT_THIS_MONTH_ID' => 'Add  Deduct  This  Month  ID',
            'WAGE_THIS_MONTH_ID' => 'Wage  This  Month  ID',
            'ADD_DEDUCT_THIS_MONTH_EMP_ID' => 'Add  Deduct  This  Month  Emp  ID',
            'ADD_DEDUCT_THIS_MONTH_PAY_DATE' => 'Add  Deduct  This  Month  Pay  Date',
            'ADD_DEDUCT_THIS_MONTH_TMP_ID' => 'Add  Deduct  This  Month  Tmp  ID',
            'ADD_DEDUCT_THIS_MONTH_TMP_NAME' => 'Add  Deduct  This  Month  Tmp  Name',
            'ADD_DEDUCT_THIS_MONTH_DETAIL_ID' => 'Add  Deduct  This  Month  Detail  ID',
            'ADD_DEDUCT_THIS_MONTH_DETAIL' => 'Add  Deduct  This  Month  Detail',
            'ADD_DEDUCT_THIS_MONTH_AMOUNT' => 'Add  Deduct  This  Month  Amount',
            'ADD_DEDUCT_THIS_MONTH_TYPE' => 'Add  Deduct  This  Month  Type',
            'ADD_DEDUCT_THIS_MONTH_STATUS' => 'Add  Deduct  This  Month  Status',
            'ADD_DEDUCT_THIS_MONTH_SLIP_STATUS' => 'Add  Deduct  This  Month  Slip  Status',
            'ADD_DEDUCT_THIS_MONTH_CREATE_DATE' => 'Add  Deduct  This  Month  Create  Date',
            'ADD_DEDUCT_THIS_MONTH_CREATE_BY' => 'Add  Deduct  This  Month  Create  By',
            'ADD_DEDUCT_THIS_MONTH_UPDATE_DATE' => 'Add  Deduct  This  Month  Update  Date',
            'ADD_DEDUCT_THIS_MONTH_UPDATE_BY' => 'Add  Deduct  This  Month  Update  By',
        ];
    }
}
