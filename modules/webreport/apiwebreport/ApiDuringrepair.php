<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 17:17
 */

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

use app\modules\webreport\apiwebreport\ApiReport;

class ApiDuringrepair
{
    public static function getStatustime($allCvhcmainStatus)
    {
        $arrCvhcmain = [];
        foreach ($allCvhcmainStatus AS $value)
        {
            $arrCvhcmain[] = $value['cvhc_main_id'];
        }
        $strarrCvhcmain = join(",", $arrCvhcmain);

         $sql_statustime = "SELECT 
                          id ,
                          status
                           FROM  cvhc_main
                           WHERE id IN($strarrCvhcmain)
                           ORDER BY id";

        
         $getStatustime = Yii::$app->dbERP_service->createCommand($sql_statustime)->queryAll();
         
         $arrstatus = ApiReport::REPAIR_STATUS;
             //statustime//
                // D1 = 23 = ปิดงานซ่อม รอsaตรวจสภาพรถ กรณีไม่ล้างรถ(ทุกกรณี)
                // C1 = 22 = sa ระหว่างแจ้งซ่อม
                // A1 = 2 = sa รอแจ้งซ่อมกับลูกค้า(หลังจากตรวจสภาพรถเสร็จ)
                // B1 = 4 = เช็คเกอร์รอจ่ายงานให้ช่า
                // E1 = 18 && ัytax day is not null
         $arrCvhcmainTimeStatus = [];
                foreach ($getStatustime AS $value)
                {
                    $status = $value['status'];
                    if($status == 26 or $status==18)
                    {
                        $statusnow = 'E1';
                    }elseif($status == 23)
                    {
                        $statusnow = 'D1';
                    }elseif($status == 22)
                    {
                        $statusnow = 'C1';
                    }elseif($status == 4)
                    {
                        $statusnow = 'B1';
                    }elseif($status == 2)
                    {
                        $statusnow = 'A1';
                    }
                    $arrCvhcmainTimeStatus[$value['id']] = $statusnow ;
                }

        return $arrCvhcmainTimeStatus;
               
    }

    public static function getTypeRepairOvertime($allCvhcmainStatus)
    {
        $arrCvhcmain = [];
        foreach ($allCvhcmainStatus AS $value)
        {
            $arrCvhcmain[] = $value['cvhc_main_id'];
        }
        $strarrCvhcmain = join(",", $arrCvhcmain);

         $sql_typeRepairOvertime = "SELECT 
                                   drepair_inform.cvhc_main_id AS cvhv_main_id,
                                   drepair_inform.id AS drepair_inform_id,
                                   drepair_inform_list.id AS drepair_inform_list_id,
                                   drepair_inform_list.crepair_id AS crepair_id,
                                   crepair.repair_id AS repair_id,
                                   drepair_inform_list.wage AS wage
                                   FROM drepair_inform
                                   INNER JOIN drepair_inform_list ON drepair_inform.id = drepair_inform_list.drepair_inform_id
                                   INNER JOIN crepair ON drepair_inform_list.crepair_id = crepair.id
                                   WHERE drepair_inform.cvhc_main_id IN($strarrCvhcmain)";


        return $typeRepairOvertime = Yii::$app->dbERP_service->createCommand($sql_typeRepairOvertime)->queryAll();

    }

    public  static function getOverTime($type_company,$technician_TypeCar,$str_start,$str_end,$arrAllEmployee,$arrCvhcMainID)
    {
          $sql_totalOverTime = "SELECT
                cvhc_main.id ,
                sa_id,
                cvhc_main.status AS StatusSA,
                DATE_FORMAT(
                    FROM_UNIXTIME(cvhc_main.guard_time) ,
                    '%Y-%m-%d'
                )  as guard_time ,
                    ahistory.register ,
                    erepair_send_all_mechanic.technician_name,		
                DATE_FORMAT(
                    FROM_UNIXTIME(cvhc_main.app_time) ,
                    '%Y-%m-%d %H:%i:%s'
                )  as app_time,
                DATE_FORMAT(
                    FROM_UNIXTIME(cvhc_main.send_time) ,
                    '%Y-%m-%d %H:%i:%s'
                )  as send_time,
                TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(cvhc_main.send_time) ,'%Y-%m-%d %H:%i:%s') ,DATE_FORMAT(FROM_UNIXTIME(cvhc_main.app_time) ,'%Y-%m-%d %H:%i:%s')) as difftime  
                FROM
                    cvhc_main
                INNER JOIN ahistory on ahistory.id = cvhc_main.ahistory_id
                INNER JOIN erepair_send_all_mechanic on erepair_send_all_mechanic.cvhc_main_id = cvhc_main.id
                WHERE
                    cvhc_main.brach_id = '$type_company'
                AND ahistory.agroup_id in($technician_TypeCar)
                AND DATE_FORMAT(
                    FROM_UNIXTIME(cvhc_main.guard_time) ,
                    '%Y-%m-%d %H:%i:%s'
                ) BETWEEN '$str_start 00:00:00'
                AND '$str_end 23:59:59'
                GROUP BY cvhc_main.id HAVING difftime > 0  
                ORDER BY cvhc_main.id";
            //exit;
       

        $model = Yii::$app->dbERP_service->createCommand($sql_totalOverTime)->queryAll();
        $arrworkOverTime= [];
        foreach ($model as $value) {
            array_push($arrCvhcMainID,$value['id']);  //add cvhc_main.id to array
            $arrworkOverTime[$value['id']]['id'] = $value['id'];
            $arrworkOverTime[$value['id']]['guard_time'] = $value['guard_time'];
            $arrworkOverTime[$value['id']]['register'] = $value['register'];
            $arrworkOverTime[$value['id']]['sa_id'] = $value['sa_id'];
            $arrworkOverTime[$value['id']]['sa_name'] = $arrAllEmployee[$value['sa_id']];  //find SA name with ID card, use array key
            $arrworkOverTime[$value['id']]['technician_name'] = $value['technician_name'];
        }
        return $arrworkOverTime;
    }


    public static function getTimeStatusCvhcmain($arrCvhcMainID)
    {
        $strID = join(",",$arrCvhcMainID);
        $sql = "SELECT 	cvhc_main_id ,
                cvhc_main_status ,
                time_begin,
                DATE_FORMAT(
                        FROM_UNIXTIME(time_begin) ,
                        '%Y-%m-%d %H:%i:%s'
                    ) as start_time 
                FROM status_time WHERE cvhc_main_id IN($strID) ORDER BY cvhc_main_id,cvhc_main_status ";

        $model = Yii::$app->dbERP_service->createCommand($sql)->queryAll();
        $arrCvhcMainStatus = [];
        foreach ($model as $item) {
            $arrCvhcMainStatus[$item['cvhc_main_id']] = $item;
        }

        return $arrCvhcMainStatus;
    }





    public static function getNameSA($type_company)
    {
        $sql_totalSa = "SELECT a.id,c.NameUse 
        FROM cvhc_main as a 
        inner join drepair_inform as b on b.cvhc_main_id = a.id 
        inner join erp_easyhr_ou.relation_position as c on c.id_card = b.sa_id 
        where a.brach_id = $type_company  and b.status <> 99";
        $totalSa = Yii::$app->dbERP_service->createCommand($sql_totalSa)->queryAll();
        $nameSA=[];
        foreach ($totalSa as $value) {
            $nameSA[$value['id']]['sa_name'] = $value['NameUse'];
        }
        return $nameSA;
    }


    public static function getCheckCountCarForPresent($countcar)
    {
        if($countcar == 0 )
        {
           return $countcar = "0";
        }else
        {
            return   $countcar;
        }
    }


    public static function getSAinBranch($branchID)
    {
        $sql_totalSa = "SELECT distinct(b.sa_id) as sa_id,c.NameUse as sa_name 
        FROM cvhc_main as a 
        inner join drepair_inform as b on b.cvhc_main_id = a.id 
        inner join ERP_easyhr_OU.relation_position as c on c.id_card = b.sa_id 
        where a.brach_id = '$branchID'  and b.status <> 99";
        //echo $sql_totalSa;
        $totalSa = Yii::$app->dbERP_service->createCommand($sql_totalSa)->queryAll();
        $arrSA=[];
        foreach ($totalSa as $value) {
            $arrSA[$value['sa_id']] = $value['sa_name'];
        }

        return $arrSA;
    }



    public static function getTimeOverTimeMore($arrCvhcMainID,$year)
    {
        $strID = join(",",$arrCvhcMainID);

         $sql_Time_MoreOvertime= "SELECT cvhc_main.id,
                                    (IF(sa_str_cktime - guard_time  > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'A1'),(sa_str_cktime - guard_time)-(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'A1'),0)) AS Time_DiffA , 

                                    (IF(erepair_sent_list.time_start - drepair_inform.start_time >(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'B1'),(erepair_sent_list.time_start - drepair_inform.start_time)-(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'B1'),0))  AS Time_DiffB, 

                                    (IF(check_after_wash.stop - erepair_sent_list.time_stop  >(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'C1'),(check_after_wash.stop - erepair_sent_list.time_stop)-(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '2016'
                                    AND code_timestandard = 'C1'),0)) AS Time_DiffC, 

                                    (IF(cvhc_main.time_warn - check_after_wash.stop>(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'D1'),(cvhc_main.time_warn - check_after_wash.stop)-(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'D1'),0)) AS Time_DiffD, 

                                    (IF(ytax.day  - check_after_wash.stop>(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'E1'),(ytax.day  - check_after_wash.stop)-(SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'E1'),0))AS Time_DiffE 
                                FROM cvhc_main 
                                INNER JOIN ahistory ON ahistory.id = cvhc_main.ahistory_id 
                                INNER JOIN drepair_inform ON cvhc_main.id = drepair_inform.cvhc_main_id
                                INNER JOIN erepair_sent_list ON erepair_sent_list.cvhc_main_id = cvhc_main.id
                                INNER JOIN check_after_wash ON check_after_wash.cvhc_main_id = cvhc_main.id
                                INNER JOIN ytax ON ytax.ref_id = cvhc_main.id
                                WHERE cvhc_main.id IN($strID)
                                AND erepair_sent_list.status_active = '1'
                                AND check_after_wash.job_send = '0'
                                AND check_after_wash.ploblem = '0'
                                AND check_after_wash.status <> 99
                                AND cvhc_main.status <> 99
                                AND ytax.ref_status IN(1,3)
                                AND ytax.day <> ''
                                GROUP BY cvhc_main.id";
       

         $Time_MoreOvertime = Yii::$app->dbERP_service->createCommand($sql_Time_MoreOvertime)->queryAll();

         $arr_Cvhc_Main_Time_Over =[];
         foreach ($Time_MoreOvertime as  $value) {
             $arr_Cvhc_Main_Time_Over[$value['id']] = $value['Time_DiffA']+$value['Time_DiffB']+$value['Time_DiffC']+$value['Time_DiffD']+$value['Time_DiffE'];
         }
        return $arr_Cvhc_Main_Time_Over;
    }
    public static function getTimeStatusYear($type_company, $technician, $year)
    {
        $sql_Time_StatusYear = "SELECT COUNT(cvhc_main.id) AS CountCar,
                                    SUM(IF(sa_str_cktime - guard_time > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'A1'),1,0 )) AS Time_DiffA , 
                                    SUM(IF(erepair_sent_list.time_start - drepair_inform.start_time > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'B1') ,1,0 )) AS Time_DiffB, 
                                    SUM(IF(check_after_wash.stop - erepair_sent_list.time_stop > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'C1') ,1,0 )) AS Time_DiffC, 
                                    SUM(IF(cvhc_main.time_warn - check_after_wash.stop > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'D1') ,1,0 )) AS Time_DiffD, 
                                    SUM(IF(ytax.day - check_after_wash.stop > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '$year'
                                    AND code_timestandard = 'E1') ,1,0 ))AS Time_DiffE 
                                FROM cvhc_main 
                                INNER JOIN ahistory ON ahistory.id = cvhc_main.ahistory_id 
                                INNER JOIN drepair_inform ON cvhc_main.id = drepair_inform.cvhc_main_id
                                INNER JOIN erepair_sent_list ON erepair_sent_list.cvhc_main_id = cvhc_main.id
                                INNER JOIN check_after_wash ON check_after_wash.cvhc_main_id = cvhc_main.id
                                INNER JOIN ytax ON ytax.ref_id = cvhc_main.id
                                WHERE brach_id = '$type_company' 
                                AND ahistory.agroup_id IN ($technician) 
                                AND erepair_sent_list.status_active = '1'
                                AND check_after_wash.job_send = '0'
                                AND check_after_wash.ploblem = '0'
                                AND check_after_wash.status <> 99
                                AND cvhc_main.status <> 99
                                AND ytax.ref_status IN(1,3)
                                AND ytax.day <> ''
                                AND DATE_FORMAT(FROM_UNIXTIME(cvhc_main.guard_time),'%Y') = $year
                                AND DATE_FORMAT(FROM_UNIXTIME(check_after_wash.stop),'%Y') = $year
                                GROUP BY DATE_FORMAT(FROM_UNIXTIME(cvhc_main.guard_time),'%Y-%m')";
       
        return $timeStatusYear = Yii::$app->dbERP_service->createCommand($sql_Time_StatusYear)->queryAll();

    }
    public static function getTimeStatusDaily($type_company,$start_date,$end_date,$technician)
    {
        $sql_Time_Status= "SELECT COUNT(cvhc_main.id) AS CountCar,
                                DATE_FORMAT(FROM_UNIXTIME(cvhc_main.guard_time),'%Y-%m-%d') AS TimeSelect,
                                SUM(IF(sa_str_cktime - guard_time > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '2016'
                                    AND code_timestandard = 'A1'),1,0 )) AS Time_DiffA ,
                                SUM(IF(erepair_sent_list.time_start - drepair_inform.start_time > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '2016'
                                    AND code_timestandard = 'B1') ,1,0 )) AS Time_DiffB,
                                 SUM(IF(check_after_wash.stop - erepair_sent_list.time_stop > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '2016'
                                    AND code_timestandard = 'C1') ,1,0 )) AS Time_DiffC,
                                 SUM(IF(cvhc_main.time_warn - check_after_wash.stop > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '2016'
                                    AND code_timestandard = 'D1') ,1,0 )) AS Time_DiffD, 
                                 SUM(IF(ytax.day - check_after_wash.stop > (SELECT time_standard 
                                        FROM config_timestandard_sa WHERE 
                                        status_timestandard = 1
                                    AND year_timestandard = '2016'
                                    AND code_timestandard = 'E1') ,1,0 ))AS Time_DiffE 
                                FROM cvhc_main 
                                INNER JOIN ahistory ON ahistory.id = cvhc_main.ahistory_id 
                                INNER JOIN drepair_inform ON cvhc_main.id = drepair_inform.cvhc_main_id
                                INNER JOIN erepair_sent_list ON erepair_sent_list.cvhc_main_id = cvhc_main.id
                                INNER JOIN check_after_wash ON check_after_wash.cvhc_main_id = cvhc_main.id
                                INNER JOIN ytax ON ytax.ref_id = cvhc_main.id
                                WHERE DATE_FORMAT(FROM_UNIXTIME(cvhc_main.guard_time),'%Y-%m-%d %H:%i:%s') BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
                                AND brach_id = '$type_company'
                                AND cvhc_main.status <> 99
                                AND ahistory.agroup_id IN ($technician) 
                                AND erepair_sent_list.status_active = '1'
                                AND check_after_wash.job_send = '0'
                                AND check_after_wash.ploblem = '0'
                                AND check_after_wash.status <> 99
                                AND ytax.ref_status IN(1,3)
                                AND ytax.day <> ''
                                GROUP BY DATE_FORMAT(FROM_UNIXTIME(cvhc_main.guard_time),'%Y-%m-%d')";
       
        return $timeStatusYear = Yii::$app->dbERP_service->createCommand($sql_Time_Status)->queryAll();

    }
    public static function processStatusRepair($arrStatus)
    {
        $arrRepairStatus = ApiReport::REPAIR_STATUS;
        //$arrGroupByStatus = $arr
        foreach ($arrStatus as $value) {

        }
    }


}