<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 1/23/2017 AD
 * Time: 15:28
 */


namespace app\modules\gitreport\apigitreport;


class Issue
{
    const arrAllProject = ['service', 'sale', 'account', 'crm', 'hr', 'loan'];
    const arrAllDev = ['golf', 'kwan', 'em'];
    const arrAllIssueType = ['backlog','inprogress','ready','uat','installed'];

    public static function CalIssue($arrAllIssue)
    {
        $arrDevIssue = $arrSumIssueType = [];
        foreach ($arrAllIssue as $keyproject => $projectname)
        {
            //echo key $key;
            foreach ($projectname as $emp => $typeissue) {
                //echo key $emp;
                foreach ($typeissue as $issue => $value) {
                    //echo key $issue;
                    if(in_array($issue,self::arrAllIssueType)) {
                        $arrSumIssueType[$issue] += $value;
                    }

                    $arrDevIssue[$emp][$issue] += $value;
                }
            }
        }

        return [
            'sumissue_type'=>$arrSumIssueType,
            'sumissue_dev' =>$arrDevIssue,
        ];
    }
}