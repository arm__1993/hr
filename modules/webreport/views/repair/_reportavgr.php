<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiRepair;
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$dataTime_Wage_Distance_Only = $session->get('dataTime_Wage_Distance_Only');
$dataTime_Wage_Genaral_Only = $session->get('dataTime_Wage_Genaral_Only');
$dataTime_Wage_intersect = $session->get('dataTime_Wage_intersect');
$selected_company = $session->get('selected_company');
$selected_year = $session->get('selected_year');

?>
<div class="box">
    <h3><i class="fa fa-fw fa-file-text"></i>รายงานเวลารับแจ้งซ่อมเฉลี่ยต่อคัน รายเดือน ตามใบงานซ่อม</h3>
    <h3> <?php  $namecompany = ApiReport::getNameCompanyPDF($selected_company) ;
        echo  $namecompany['0']['name']; ?>
    </h3>
    <h3>
        รายงานปี <?php
        print_r($selected_year);
        ?></h3>
</div>
<table width="100%" bgcolor="#777" ALIGN="CENTER" id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
            เดือน
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
            เวลาเฉลี่ยต่อคัน (นาที) งานเช็คระยะ
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
            เวลาเฉลี่ยต่อคัน (นาที) งานเช็คระยะ + งานซ่อม
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
            เวลาเฉลี่ยต่อคัน (นาที) งานซ่อม
        </th>
    </tr>
    </thead>

    <tbody>
    <?php   for ($i=1; $i <= 12 ; $i++) {
        ?>
        <tr role="row">
            <td bgcolor="#eee"><?php  $arrMonth = DateTime::convertMonth($i) ;
                echo $arrMonth;
                ?>
            </td>
            <td bgcolor="#f0f8ff" align="center"><?php $SumTimeA = $dataTime_Wage_Distance_Only[$i-1]['SumDifftimeTime'];
                $Countcar = $dataTime_Wage_Distance_Only[$i-1]['CountCar'];
                $avgTimeA = ApiRepair::avgTimePerCar($SumTimeA,$Countcar) ;
                $avgTimeA = DateTime::calculateTimeElapsed($avgTimeA);
                echo ApiRepair::getFormatTime($avgTimeA['h'],$avgTimeA['m'],$avgTimeA['s']);
                ?>
            </td>
            <td bgcolor="#f0f8ff" align="center"><?php $SumTimeB = $dataTime_Wage_intersect[$i-1]['SumDifftimeTime'];
                $Countcar = $dataTime_Wage_intersect[$i-1]['CountCar'];
                $avgTimeB = ApiRepair::avgTimePerCar($SumTimeB,$Countcar) ;
                $avgTimeB = DateTime::calculateTimeElapsed($avgTimeB);
                echo ApiRepair::getFormatTime($avgTimeB['h'],$avgTimeB['m'],$avgTimeB['s']);
                ?>
            </td>
            <td bgcolor="#f0f8ff" align="center"><?php $SumTimeC = $dataTime_Wage_Genaral_Only[$i-1]['SumDifftimeTime'];
                $Countcar = $dataTime_Wage_Genaral_Only[$i-1]['CountCar'];
                $avgTimeC = ApiRepair::avgTimePerCar($SumTimeC,$Countcar) ;
                $avgTimeC = DateTime::calculateTimeElapsed($avgTimeC);
                echo ApiRepair::getFormatTime($avgTimeC['h'],$avgTimeC['m'],$avgTimeC['s']); ?>
            </td>
        </tr>
    <?php  }  ?>
    </tbody>
</table>
<?php
session_start();
session_destroy();
?>