-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 30, 2017 at 11:01 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_type`
--

CREATE TABLE `tax_income_type` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'รหัส',
  `taxincome_type` varchar(200) NOT NULL COMMENT 'ชื่อภาษีเงินได้บุคคลธรรมดา',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_income_type`
--

INSERT INTO `tax_income_type` (`id`, `taxincome_type`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(2, 'wqewq', 99, '1509901325106', '2017-03-28 17:23:24', '', NULL),
(3, 'ประเภท', 99, '1509901325106', '2017-03-30 10:19:14', '1509901325106', '2017-03-30 10:35:02'),
(4, 'ภงด 90', 1, '1509901325106', '2017-03-30 10:37:27', '1509901325106', '2017-03-30 10:47:16'),
(5, 'ภงด 91', 1, '1509901325106', '2017-03-30 10:37:47', '1509901325106', '2017-03-30 11:01:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_income_type`
--
ALTER TABLE `tax_income_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_income_type`
--
ALTER TABLE `tax_income_type`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส', AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
