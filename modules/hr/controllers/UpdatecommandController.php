<?php

namespace app\modules\hr\controllers;

use app\modules\hr\apihr\ApiBenefit;
use app\modules\hr\models\AddDeDuctTemplate;
use yii\web\Controller;
use yii\web\Session;
use Yii;
// use app\modules\hr\apihr\;
use app\modules\hr\models\Command;
use app\modules\hr\models\OrganicChart;
use app\modules\hr\controllers\MasterController;
/**
* Default controller for the `hr` module
*/
class UpdatecommandController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;
    
    // /**
    // * function init() check session active or session login, if not redirect to login page
    // * @return \yii\web\Response
    // */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }
        
    //     $this->idcardLogin = $session->get('idcard');
    // }
    
    
    /**
    * Renders the index view for the module
    * @return string
    */
    
    public function actionUpdate(){
        $data =[];
        $command = Command::find()
        ->select('ERP_easyhr_OU.position.*,ERP_easyhr_OU.command.*, ERP_easyhr_checktime.emp_data.Name as Emp_name,ERP_easyhr_checktime.emp_data.Surname,ERP_easyhr_checktime.emp_data.DataNo as EMP_id')
        ->join('INNER JOIN','ERP_easyhr_OU.position','ERP_easyhr_OU.command.Follower = ERP_easyhr_OU.position.id')
        ->join('LEFT JOIN','ERP_easyhr_checktime.emp_data','ERP_easyhr_OU.position.PositionCode = ERP_easyhr_checktime.emp_data.Code')
        // ->where(['!=','status','99'])
        ->asArray()
        ->all();
        foreach ($command as $value) {
            $data[] = [
            'Company' => $value['WorkCompany'],
            'ref_id' => $value['Follower'],
            'Drepartment' => $value['Department'],
            'Name' => $value['Name'],
            'Emp_id' =>  $value['EMP_id'],
            'Emp_name' => $value['Emp_name'].' '.$value['Surname'],
            'parent' => $value['Leader'],
            'active_status' =>  $value['Status'],
            'level' => $value['Level'],
            'Posision_id' =>  $value['Follower'],
            'create_date'=> date('Y-m-d H:i:s')
            ];   
        }
        Yii::$app->dbERP_easyhr_OU->createCommand()->batchInsert( 'Organic_chart', ['Company','ref_id','Drepartment','Name','Emp_id','Emp_name','parent','active_status','level','Posision_id','create_date'], $data)->execute();
        
    } 
    
}