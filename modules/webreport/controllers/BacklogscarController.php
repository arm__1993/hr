<?php

namespace app\modules\webreport\controllers;


use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use mPDF;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiBacklogscar;


class BacklogscarController extends \yii\web\Controller
{
    public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRepair_backlogs()
    {
        if(Yii::$app->session->hasFlash('warning')) {
            Yii::$app->session->remove('warning');
        }
        return $this->render('repair_backlogs',['query'=>false,]);
    }

    public function actionReceive_backlogs()
    {
        $request = Yii::$app->request;
        if($request->isPost)
        {
            $postValue = Yii::$app->request->post();
            $type_company = $postValue['company'];  //retrieve company ID
            $technician_cartype = $postValue['technician_type'];  //retrieve technician type


            if ($technician_cartype == '1') {
                $technician_car = '3,4';
            } elseif ($technician_cartype == '2') {
                $technician_car = '1,2';
            }

            $selected_company = $type_company;
            $selected_technician = $technician_cartype;

            $totalName = ApiBacklogscar::getTotalUserName($type_company, $technician_car);
            if (empty($totalName)) {
                //$message = "ไม่มีข้อมูลบริษัท";
                //echo "<script type='text/javascript'>alert('$message');</script>";
                Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
                return $this->render('repair_backlogs',['query'=>false,]);
            }
            else {

                //Load data from API
                $totalTime      = ApiBacklogscar::getTotalTime($type_company);
                $statusOne      = ApiBacklogscar::getStatusOne($type_company, $technician_car);
                $detail         = ApiBacklogscar::getDetail();
                $statusThree    = ApiBacklogscar::getStatusThree($type_company, $technician_car);
                $statusTwo      = ApiBacklogscar::getStatusTwo($type_company, $technician_car);

                $NameAndTime = [];
                foreach ($totalName as $index => $value) { //รวมข้อมูลรถและเวลา
                    if (array_key_exists($index, $totalTime)) {
                        $NameAndTime[$index]['guard_time'] = date("Y-m-d", $value['guard_time']);
                        $NameAndTime[$index]['id_repair'] = $index;
                        $NameAndTime[$index]['fullmodel_name'] = $value['fullmodel_name'];
                        $NameAndTime[$index]['register'] = $value['register'];
                        $NameAndTime[$index]['technician_name'] = $value['technician_name'];
                        $NameAndTime[$index]['app_time'] = date("Y-m-d", $value['app_time']);
                        $NameAndTime[$index]['name_sa'] = $totalTime[$index]['name'];
                        $NameAndTime[$index]['end_time'] = $totalTime[$index]['end_time'];
                        if (array_key_exists($index, $statusOne)) { //รวมสถานะการทำงาน เข้ากับข้อมูลรถ
                            $NameAndTime[$index]['status'] = $statusOne[$index]['status'];
                        }
                        if (array_key_exists($index, $statusThree)) { //รวมสถานะการทำงาน เข้ากับข้อมูลรถ
                            $NameAndTime[$index]['status'] = $statusThree[$index]['status'];
                        }
                        if (array_key_exists($index, $statusTwo)) { //รวมสถานะการทำงาน เข้ากับข้อมูลรถ
                            $NameAndTime[$index]['status'] = $statusTwo[$index]['status'];
                            $NameAndTime[$index]['status_repair'] = $statusTwo[$index]['status_repair'];
                            $NameAndTime[$index]['erepair_p_code_id'] = $statusTwo[$index]['erepair_p_code_id'];
                        }
                    }
                }


                $dateNOW = strtotime(date("Y-m-d"));//กำหนดวันปัจจุบัน

                foreach ($NameAndTime as $value) { //****ประเภทงานซ่อมยังหาไม่ได้


                    if (empty($value['status'])) { //สถานนะที่ค้าง
                        $checkStatus = "ข้อมูลยังไม่ครบ";
                    } else {
                        if ($value['status'] == 0) {
                            $checkStatus = "ยามรับรถ";
                        } elseif ($value['status'] == 1) {
                            $checkStatus = "SA ตรวจสภาพรถ";
                        } elseif ($value['status'] == 2) {
                            $checkStatus = "SA รอแจ้งซ่อมกับลูกค้า(หลังจากตรวจสภาพรถเสร็จ)";
                        } elseif ($value['status'] == 22) {
                            $checkStatus = "SA ระหว่างแจ้งซ่อม";
                        } elseif ($value['status'] == 21) {
                            $checkStatus = "เช็คเกอร์รอจ่ายงานให้ช่าง";
                        } elseif ($value['status'] == 21) {
                            $checkStatus = "เช็คเกอร์รอจ่ายงานให้ช่าง";
                        } elseif ($value['status'] == 16) {
                            $checkStatus = "SA เช็คสภาพหลังจากล้างรถเรียบร้อยแล้ว";
                        } elseif ($value['status'] == 16) {
                            $checkStatus = "SA เช็คสภาพหลังจากล้างรถเรียบร้อยแล้ว";
                        } elseif ($value['status'] == 17) {
                            $checkStatus = "SA ติดต่อลูกค้ามารับรถ";
                        } elseif ($value['status'] == 18) {
                            $checkStatus = "รอลูกค้ามารับรถ ";
                        } elseif ($value['status'] == 19) {
                            $checkStatus = "ลูกค้ามารับรถและรอคิวจ่ายเงิน";
                        } elseif ($value['status'] == 20) {
                            $checkStatus = "ลลูกค้าจ่ายเงินพร้อมรับรถเรียบร้อย(ยังไม่รับรถ)";
                        } elseif ($value['status'] == 97) {
                            $checkStatus = "ย้อนมาแก้ไขจากหน้าการเงิน";
                        } elseif ($value['status'] == 5 or 6 or 7 or 8 or 9 or 10 or 11 or 12 or 13 or 14 or 24 or 23) {
                            if ($value['status_repair'] == 0) {
                                $checkStatus = "รอเช็คเกอร์จ่ายงาน";
                            } elseif ($value['status_repair'] == 1) {
                                $checkStatus = "ช่างกำลังซ่อม";
                            } elseif ($value['status_repair'] == 2) {
                                $checkStatus = "ปิดจ๊อบ";
                            } elseif ($value['status_repair'] == 4) {
                                $checkStatus = "รอเช็คเกอร์ตรวจงาน";
                            } elseif ($value['status_repair'] == 6) {
                                if (array_key_exists($value['erepair_p_code_id'], $detail)) {
                                    $checkStatus = $detail[$value['erepair_p_code_id']]['detail'];
                                }
                            }

                        }
                    }

                    /*print_r($checkStatus);//สถานะ
                    echo "<br>";
                    echo "========================";
                    echo "<br>";*/
                }
                /* exit();*/

            }



            $session = Yii::$app->session;

            $session->set('NameAndTime', $NameAndTime);
            $session->set('checkStatus', $checkStatus);
            $session->set('dateNOW', $dateNOW);
            $session->set('selected_company', $selected_company);
            $session->set('selected_technician', $selected_technician);

            return $this->render('repair_backlogs', [
                'NameAndTime' => $NameAndTime,
                'checkStatus' => $checkStatus,
                'dateNOW' => $dateNOW,
                'query'=>true,
                'selected_company'=>$selected_company,
                'selected_technician'=>$selected_technician,
            ]);
        }
    }

    public function actionBtn_reportbacklogs()/*05-10-2559 th mpdf*/
        {
            /*$session = Yii::$app->session;
            $company = $session->get('SESSION_company');
            $year = $session->get('SESSION_year');*/


            //foreach($this->_globalCompay as $company) {
            //echo $company->id;
            //}
            /*$mpdf->AddPage('A4');*/
            //$mpdf = new mPDF('th', 'Tharlon-Regular');
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($this->renderPartial('_reportbacklogs'));
            $mpdf->Output();
            exit;

        }
    public function actionExpoldexcelbacklog()
    {
        return $this->render('_exportbacklog');
    }

}
