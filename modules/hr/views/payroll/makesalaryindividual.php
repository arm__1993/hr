<?php
/**
 * Created by PhpStorm.
 * User: watcharaphan
 * Date: 3/11/2018 AD
 * Time: 14:56
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/global';
$imgaction = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/makesalary.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);

//$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->baseUrl."/fonts/01thaifontcss.css");

//payroll/makesalary?menu_id=2141&inject_date=06-2018

$inject_date = Yii::$app->request->get('injectdate');
$date_pay = (isset($inject_date) && $inject_date !='') ? $inject_date : date('m-Y');
?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li class="active">สร้างข้อมูลเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">

            <div class="row">

                <div class="col-lg-12">

                    <hr>
                </div>

                <div class="col-md-6 col-md-offset-3">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-bullhorn"></i>
                            <h3 class="box-title">สร้างเงินเดือน รอบเงินเดือน : <?php echo $date_pay; ?> </h3>
                        </div>
                        <div class="box-body" id="btnCreateSalary">
<!--                            <div class="callout callout-success text-center">
                                <h4 id="monthly_status_text1" style="display: none">ประมวลผลรายการประจำเดือน <?php /*echo $date_pay; */?> เรียบร้อยแล้ว</h4>
                                <div id="monthly_loadimage1" style="display:none"><img src="<?php /*echo $imghr; */?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_monthly_salary1" onclick="runcronjobsmanual('make_monthly_salary1','monthly_status_text1','delete_monthly_salary1','monthly_loadimage1')"><i class="fa fa-bitcoin"></i> ประมวลผลรายการประจำเดือน</button>
                            </div>-->

                            <div class="callout callout-success text-center">
                                <h4 id="monthly_status_text">สร้างเงินเดือนเดือน <?php echo $date_pay; ?> เรียบร้อยแล้ว</h4>
                                <div id="monthly_loadimage" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_monthly_salary" onclick="makeSaraly('make_monthly_salary','monthly_status_text','delete_monthly_salary','monthly_loadimage')"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_monthly_salary" onclick="deleteSaraly('make_monthly_salary','monthly_status_text','delete_monthly_salary','monthly_loadimage')"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo $date_pay; ?></button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span id="pay_date" title="<?php echo $date_pay; ?>"></span>
    <!-- /.box -->
</section><!-- /.content -->