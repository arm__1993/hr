<?php

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/payroll_report_pdf.css");
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
?>
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .f-size {
        font-size: 20px !important;
    }

    .fo-size {
        font-size: 20px !important;
    }

    .fon-size {
        font-size: 20px !important;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }

    .textbox_value {
        font-family: "THSarabun";
        font-size: 13px;
    }

    .text_title {
        font-family: "THSarabun";
        font-size: 14px;
    }

    .text_italic {
        font-style: italic;
    }

    .text_header {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        text-align: center !important;
    }

    .text_header2 {
        font-family: "THSarabun" !important;
        font-size: 20px !important;
        font-weight: bold;
        text-align: center !important;
    }

    .container-page {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 200px;
        padding: 3px 3px;
    }

    .addr_company {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 80px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .addr_pay {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 100%;
        height: 120px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .income_detail {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 100%;
        height: 330px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .pay_in {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 100%;
        height: 100px;
        /* border: 1px solid #2A2E31; */
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .pay_by {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 30px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .warning {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 46%;
        height: 300px;
        /* border: 1px solid #2A2E31; */
        border-radius: 0px;
        float: left;
        padding: 0px 7px;
    }

    .sign {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        /* border: 1px solid #2A2E31; */
        width: 50%;
        height: 300px;
        /* border: 1px solid #2A2E31; */
        border-radius: 0px;
        float: right;
        padding: 0px 7px;
    }

    td {
        font-family: "THSarabun" !important;
        font-size: 14px !important;
    }

    .bod {
        background-image: url('https://scontent.fbkk1-3.fna.fbcdn.net/v/t1.0-9/38126643_2183521141689528_2569118921001009152_n.jpg?_nc_fx=fbkk1-2&_nc_cat=0&oh=af97bc756dd37051b6ef52bcc3372370&oe=5C01704B');
        background-repeat: no-repeat;
        background-position: 80% 97%;

        background-attachment: fixed;
        background-size: 200px
    }

    .circle {
        width: 550px;
        height: 50px;
        border-radius: 10%;
        font-family: "THSarabun" !important;
        font-size: 50px;
        color: #000000;
        line-height: 30px;
        text-align: center;
        background: #ccd1de
    }

    p.thick {
        font-weight: bold;
    }


</style>
<div class='bod'>
    <div class="container" style="border:0px;">
        <!-- <div class ='circle' cellpadding='0' cellspacing ='0'>
        <b>แบบยื่นรายการภาษีเงินได้หัก ณ ที่จ่าย<br>
        ตามมาตรา 59 แห่งประมวลรัษฎากร<br></b>
       <p>สำหรับการหักภาษี ณ ที่จ่ายตามมาตรา 50 (1) กรณีการจ่ายเงินได้พึงประเมินตามมาตรา 40 (1) (2) แห่งประมวลรัษฎากร</p>
        </div> -->
        <div width='70%'
             style="float:left;height:70px; background-color:#cdd1dd; border: 0px solid #000;border-top-left-radius: 45px;border-top-right-radius: 8px;border-bottom-left-radius: 45px;border-bottom-right-radius: 8px;">
            <table width='100%' style='text-align: center;'>
                <tr>
                    <td>
                        <img height="80" width="80" src="<?php echo $imghr; ?>/rd-logo.png" class="img-circle">
                    </td>
                    <td>
                        <p class="thick">แบบยื่นรายการภาษีเงินได้หัก ณ ที่จ่าย</p>
                        <p class="thick">ตามมาตรา 59 แห่งประมวลรัษฎากร</p>
                        <p>สำหรับการหักภาษี ณ ที่จ่ายตามมาตรา 50 (1) กรณีการจ่ายเงินได้พึงประเมินตามมาตรา 40 (1)(2)
                            แห่งประมวลรัษฎากร</p>
                    </td>
                </tr>
            </table>
        </div>
        <div width='28%'
             style="float:right;height:4em; background-color:#ffffff; border: 4px solid #cdd1dd;border-radius:8px;">
            <p style='text-align:center; font-size: 64px; font-weight:bold; padding-top:-80px; padding-bottom:-80px'>
                ภ.ง.ด.1ก</p>
        </div>


        <div class="container-page" style="border:0px; ">


            <div class="warning" style="border-right: 1px solid #cdd1dd; border-bottom: 1px solid  #cdd1dd;">

                <table width="100%">
                    <tr>

                        <td ><p style="font-size:25px"><strong>เลขประจำตัวผู้เสียภาษีอากร</strong></p></td>
                        <td height="30">
                            <table border="1"
                                   style="border-collapse: collapse; border: 0.5px solid #1a2226 padding-top:0px"
                                   class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                                <tr>
                                    <td><?php echo $id_card_extract[1];?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $id_card_extract[2];?></td>
                                    <td style="border-left:0px;"><?php echo $id_card_extract[3];?></td>
                                    <td style="border-left:0px;"><?php echo $id_card_extract[4];?></td>
                                    <td style="border-left:0px;"><?php echo $id_card_extract[5];?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $id_card_extract[6];?></td>
                                    <td style="border-left:0px;"><?php echo $id_card_extract[7];?></td>
                                    <td style="border-left:0px;"><?php echo $id_card_extract[8];?></td>
                                    <td style="border-left:0px;"><?php echo $id_card_extract[9];?></td>
                                    <td><?php echo $id_card_extract[10];?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $id_card_extract[11];?></td>
                                    <td style="border-left:0px;"><?php echo $id_card_extract[12];?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $id_card_extract[13];?></td>
                                </tr>
                            </table>
                    <tr>
                        <td  style="padding-top:-10px;"> <i>(ของผู้มีหน้าที่หักภาษี ณ ที่จ่าย)</i></td>
                    </tr>
                    </td>
                    </tr>
                    <tr width="100%">
                        <td width="80%"><p  style="font-size:23px"><strong>ชื่อผู้มีหน้าที่หักภาษี ณ ที่จ่าย</strong><i>(หน่วยงาน) :</i></p></td>
                        <td width="20%">
                            <table align='right'>
                                <tr>
                                    <td  align='right'><p  style="font-size:23px">สาขาที่</p></td>
                                    <td  align='right'>
                                        <table border='1' style="border-collapse: collapse; border: 1px solid #ccc;border-top: 0px;border-bottom: 0px;  padding-top:0px" cellpadding='4' rowpadding='2' cellspacing='0'>
                                            <tr>
                                                <td style="width: 13px"><p>0</p></td>
                                                <td style="border-left:0px;width: 13px"><p>0</p></td>
                                                <td style="border-left:0px;width: 13px"><p>0</p></td>
                                                <td style="border-left:0px;width: 13px"><p>0</p><br></td>
                                                <td style="width: 13px"><?php echo $company_data['company_type']?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>

                <div>
                    <p style="padding-bottom: -18px"><?php echo $company_data['name']?></p>
                    <p style="padding-top:-20px;">.........................................................................................................................</p>
                    <p style="padding-top:-20px;"><strong>ที่อยู่:</strong> <?php echo $company_data['address']?>
                        <!--                        อาคาร............................. ห้องเลขที่........... ชั้นที่........หมู่บ้าน................. </p>-->
                        <!--                    <p style="padding-top:-20px;">เลขที่.................................หมู่ที่.........ตรอก/ซอย........................แยก............... </p>-->
                        <!---->
                        <!--                    <p style="padding-top:-20px;">ถนน.................................................ตำบล/แขวง............................................  </p>-->
                        <!--                    <p style="padding-top:-20px;">อำเภอ/เขต.................................................จังหวัด.........................................</p>-->
                </div>

                <div>

                    <div width='100%' style="float:left;height:60px; background-color:#cdd1dd; border: 0px solid #000;border-top-left-radius: 8px;border-top-right-radius: 8px;border-bottom-left-radius: 8px;border-bottom-right-radius: 8px;" >
                        <p style='text-align:center;'>
                           <i> โปรดยื่นแบบ ภ.ง.ด.1ก ภายในเดือนกุมภาพันธ์</i>
                        </p>
                    </div>
                </div>


            </div>


            <div class="sign" style=" border-bottom: 1px solid  #cdd1dd;">


                <div  style='border: 1px solid #ccc; position:absolute; top:0;float:right;border-top: 0px;border-right: 0px; border-left: 0px; padding:0px' >
                    <div>
                        <p style='text-align:center;border: 1px solid #ccc; position:absolute; top:0;float:right;border-top: 0px;border-right: 0px; border-left: 0px; padding-top:35px;padding-bottom:35px'>
                            <b>รายการภาษีเงินได้หัก ณ ที่จ่าย ประจำปีภาษี <?php echo $years ?></b>
                        </p>
                    </div>
                    <div>
                        <table align='center' style='padding:15px'>
                            <tr>
                                <td width="30%" ><img height="10" width="10"
                                                      src="<?php echo ($p == 1) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                      class="img-circle"> <strong>(1)</strong>ยื่น<strong>ปกติ</strong></td>
                                <td width="40%" ><p><img height="10" width="10"
                                                         src="<?php echo ($p == 2) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                         class="img-circle"> <strong>(2)</strong> ยื่น<strong>เพิ่มเพิ่ม</strong>ครั้งที่</p></td>
                                <td width="30%">
                                    <table style='border: 1px solid #ccc;padding-top:5px' cellpadding='-1' rowpadding='0' cellspacing='0'>
                                        <tr>
                                            <td style='border:0px;padding-bottom:-5px'>..........</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <!-- <div class="addr_company">

            </div> -->
            <div class="addr_pay">
                <p style="padding-top:-10">ขอยื่นรายการแสดงการจ่ายเงินได้พึงประเมินตาม<strong>มาตรา 40 (1) (2)</strong> ในปีที่ล่วงมาแล้ว</p>
                <table cellpadding='1' width='100%'  style='margin-top:3px;padding-top:-18px'>
                    <tr>
                        <td width='6%'></td>
                        <td  rowspan='4' width='44%'>
                            <p  style="font-size:18px">มีรายละเอียดการหักเป็นรายผู้มีเงินได้ ปรากฏตาม</p>
                            <p  style="font-size:18px"><i>(ให้แสดงรายละเอียด<strong>ในใบแนบ ภ.ง.ด.1ก </strong>หรือใน<strong>สื่อ</strong></i></p>
                            <p  style="font-size:18px"><i><strong>บันทึกในระบบคอมพิวเตอร์</strong>อย่างใดอย่างหนึ่งเท่านั้น)</i></p>
                        </td>
                        <td width='60%'>
                            <table width='100%'>
                                <tr>
                                    <td style="font-size:18px"><?php $ps = 1 ?>
                                        <img height="10" width="10"
                                             src="<?php echo ($ps == 1) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                             class="img-circle"> ใบแนบ
                                        <strong style="font-size:18px">  ภ.ง.ด.1ก</strong> ที่แนบมาพร้อมนี้ :
                                    </td>
                                    <?php $count = ($count) ? $count: '.................';?>
                                    <td  style='text-align: right; font-size:18px"'>จำนวน&nbsp;&nbsp;<?php echo $count?>&nbsp;&nbsp;แผ่น</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table width='100%'>
                                <tr>
                                    <td style="font-size:18px"><img height="10" width="10"
                                                                    src="<?php echo ($ps == 2) ? $imghr . '/checkbox.png' : $imghr . '/uncheck.png'; ?>"
                                                                    class="img-circle"> <strong style="font-size:18px">สื่อบันทึกในระบบคอมพิวเตอร์</strong>
                                        ที่แนบมาพร้อมนี้ :
                                    </td>
                                    <td style='text-align: right; font-size:18px"'>จำนวน.................แผ่น</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table width='100%'>
                                <tr>
                                    <td style="font-size:18px" colspan='2'><i>(ตามหนังสือแสดงความประสงค์ฯ
                                            ทะเบียนรับเลขที่..............................................................</i>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table width='100%'>
                                <tr>
                                    <td style="font-size:18px" colspan='2'><i>หรือตามหนังสือข้อตกลงการใช้งานฯ
                                        เลขอ้างอิงการลงทะเบียน...............................................)</i>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="income_detail">
                <div align='center' width='120%' style='margin-top:7px'>
                    <div style="width: 50%; text-align:center;float:left; background-color:#cdd1dd; border: 0px solid #000;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;"
                         width='65%' id='div-1a'>
                        <strong> สรุปรายการภาษีที่นำส่ง</strong>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff; border: 1px solid #cdd1dd;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;"
                         width='10%' id='div-1b'>
                        <strong> จำนวนราย</strong>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff; border: 1px solid #cdd1dd;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;">
                        <strong> เงินได้ทั้งสิ้น</strong>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff; border: 1px solid #cdd1dd;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;">
                        <strong> ภาษีที่นำส่งทั้งสิ้น</strong>
                    </div>
                </div>

                <div align='center' width='120%' style='margin-top:3px'>
                    <div style="width: 50%; text-align:left;float:left; " width='65%' id='div-1a'>
                        1. เงินได้ตาม <strong>มาตรา 40 (1) </strong>เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"
                         width='10%' id='div-1b'>
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%;'>
                            <tr>
                                <td style='text-align:center;'>
                                    <?php echo ($countnum0); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%;'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($total_monthlys0, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%;'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($taxincluded0, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div align='center' style width='120%' style=''>
                    <div style="width: 50%; text-align:left;float:left; " width='65%' id='div-1a'>
                        2. เงินได้ตาม <strong>มาตรา 40 (1)</strong> เงินเดือน ค่าจ้าง ฯลฯ กรณีได้รับ<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; อนุมัติจากกรมสรรพากรให้หักอัตรา<strong>ร้อยละ 3</strong><br>
                      <i>(ตามหนังสือที่.....................................ลงวันที่...........................................)</i>
                    </div>
                    <div style="margin-top:45px;padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"
                         width='10%' id='div-1b'>
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:center;'>
                                    <?php echo number_format($countnum1, 0, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($total_monthlys1, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($taxincluded1, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div align='center' width='120%' style=''>
                    <div style="width: 50%; text-align:left;float:left; " width='65%' id='div-1a'>
                        3.เงินได้ตาม <strong>มาตรา 40 (1) (2)</strong> กรณีนายจ้างจ่ายให้ครั้งเดียว<br>
                        เพราะเหตุออกจากงาน.....................................................................................................
                    </div>
                    <div style="margin-top:25px;padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"
                         width='10%' id='div-1b'>
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:center;'>
                                    <?php echo number_format($countnum2, 0, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($total_monthlys2, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($taxincluded2, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div align='center' width='120%' style=''>
                    <div style="width: 50%; text-align:left;float:left; " width='65%' id='div-1a'>
                        4. เงินได้ตาม <strong>มาตรา 40 (2)</strong> กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"
                         width='10%' id='div-1b'>
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:center;'>
                                    <?php echo number_format($countnum3, 0, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($total_monthlys3, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($taxincluded3, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div align='center' width='120%' style=''>
                    <div style="width: 50%; text-align:left;float:left; " width='65%' id='div-1a'>
                        5. เงินได้ตาม <strong>มาตรา40 (2)</strong> กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"
                         width='10%' id='div-1b'>
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:center;'>
                                    <?php echo number_format($countnum4, 0, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($total_monthlys4, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php echo number_format($taxincluded4, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div align='center' width='120%' style=''>
                    <div style="width: 50%; text-align:left;float:left; " width='65%' id='div-1a'>
                        6.<strong>รวม</strong>
                        ...............................................................................................................................
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"
                         width='10%' id='div-1b'>
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:center;'>
                                    <?php $countnum5 = $countnum0+$countnum1+$countnum2+$countnum3+$countnum4?>
                                    <?php echo number_format($countnum5, 0, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <?php $total_monthlys5 = $total_monthlys0+$total_monthlys1+$total_monthlys2+$total_monthlys3+$total_monthlys4 ?>
                                <td style='text-align:right;'>
                                    <?php echo number_format($total_monthlys5, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;">
                        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
                            <tr>
                                <td style='text-align:right;'>
                                    <?php $taxincluded5 = $taxincluded0+$taxincluded1+$taxincluded2+$taxincluded3+$taxincluded4 ?>
                                    <?php echo number_format($taxincluded5, 2, ".", ","); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <hr>

            </div>
            <div class="pay_in" >
                <div align='center' width='120%' style='padding-bottom:0px;padding-top:-50px;'>

                    <p style="text-align:center;margin-top:1px">ข้าพระเจ้าขอรับรองว่า รายการที่แจ้งไปข้างต้นนี้
                        เป็นรายการที่ถูกต้องและครบถ้วนทุกประการ</p>
                    <p style="text-align:center;">
                        ลงชื่อ..............................................................................................ผู้จ่ายเงิน</p>
                    <p style="text-align:center;padding-top:-10px;padding-bottom:-10px"><?php echo $dataitem[0][signature_name]; ?></p>
                    <p style="text-align:center;padding-top:-15px">
                        (..............................................................................................)</p>
                    <p style="text-align:center;padding-top:-10px;padding-bottom:-10px"><?php echo $dataitem[0][signature_position]; ?></p>
                    <p style="text-align:center;padding-top:-10px">
                        ตำแหน่ง........................................................................................</p>
                    <p style="text-align:center;padding-top:-10px">
                        ยื่นวันที่...........เดือน.......................................พ.ศ. ................... </p>


                </div>
                <hr style='margin-top:-15px'>
            </div>


        </div>
    </div>
</div>
<div style='float:right;text-align:right;padding-top:-25px'><p><strong><i>(ก่อนกรอกรายการ ดูคำชี้แจงด้านหลัง)</i></strong></p></div>
<div style='float:left;padding-top:-12px'><p><strong><i>สอบถามข้อมูลเพิ่มเติมได้ที่ศูนย์สารนิเทศสรรพากร โทร.</i></strong><img
                style='padding-bottom:-5px' height='20' src="<?php echo $imghr; ?>/210360_WHT1_kor.pdf.png"
                class="img-circle"><strong><i>โทร. 1161</i></p></strong></div>

<script>
