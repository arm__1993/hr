<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "ytax_list_parts".
 *
 * @property integer $id
 * @property string $ytax_id
 * @property string $cvhc_main_id
 * @property string $ref_id
 * @property string $drepair_inform_id
 * @property string $drepair_inform_list_id
 * @property string $drepair_inform_listpart_id
 * @property string $abrand_id
 * @property string $agroup_id
 * @property string $amodel_id
 * @property string $wage_id
 * @property string $ref_status
 * @property string $crepair_id
 * @property string $crepair_name
 * @property string $crepair_list_id
 * @property string $crepair_list_name
 * @property string $pw_id
 * @property string $pw_code
 * @property string $pw_code_backup
 * @property string $kind
 * @property string $pw_name
 * @property string $price_u
 * @property double $unit
 * @property double $unitstock
 * @property integer $unitw
 * @property string $vat_price
 * @property string $invat_price
 * @property string $novat_price
 * @property string $novat_cap
 * @property string $vat_cap
 * @property string $invat_cap
 * @property string $avg_price
 * @property string $avg_vat
 * @property string $avg_includevat
 * @property string $cam_id
 * @property string $id_cam
 * @property string $cam_name
 * @property string $discount
 * @property integer $sale_code_id
 * @property string $salecode_name
 * @property string $salecode_dis
 * @property string $dis_pay
 * @property string $receive
 * @property string $emp_id
 * @property string $time
 * @property string $uses
 * @property string $status
 * @property string $warranty
 * @property string $type_list
 * @property integer $status_claim
 * @property string $ydate_receiveparts
 * @property string $tis_price_p_part
 * @property string $tis_unit_new
 * @property string $tis_sum_price
 * @property string $tis_dis
 * @property string $tis_pw_code
 * @property string $tis_pw_code_backup
 * @property string $tis_pw_name
 * @property integer $status_PDS
 * @property string $DEL_FROM
 * @property integer $sent_view_ytaxlist
 * @property integer $sent_view_receive_Sale_Credit
 */
class YtaxListParts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ytax_list_parts';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ytax_id', 'cvhc_main_id', 'ref_id', 'drepair_inform_id', 'drepair_inform_list_id', 'drepair_inform_listpart_id', 'abrand_id', 'agroup_id', 'amodel_id', 'wage_id', 'ref_status', 'crepair_id', 'crepair_name', 'crepair_list_id', 'crepair_list_name', 'pw_id', 'pw_code', 'pw_code_backup', 'kind', 'pw_name', 'price_u', 'unit', 'unitstock', 'unitw', 'vat_price', 'invat_price', 'novat_price', 'novat_cap', 'vat_cap', 'invat_cap', 'avg_price', 'avg_vat', 'avg_includevat', 'cam_id', 'id_cam', 'cam_name', 'discount', 'sale_code_id', 'salecode_name', 'salecode_dis', 'dis_pay', 'receive', 'emp_id', 'time', 'uses', 'status', 'warranty', 'type_list', 'status_claim', 'ydate_receiveparts', 'tis_price_p_part', 'tis_unit_new', 'tis_sum_price', 'tis_dis', 'tis_pw_code', 'tis_pw_code_backup', 'tis_pw_name', 'status_PDS', 'DEL_FROM', 'sent_view_ytaxlist', 'sent_view_receive_Sale_Credit'], 'required'],
            [['crepair_name', 'crepair_list_name', 'pw_name', 'cam_name'], 'string'],
            [['price_u', 'unit', 'unitstock', 'vat_price', 'invat_price', 'novat_price', 'novat_cap', 'vat_cap', 'invat_cap', 'avg_price', 'avg_vat', 'avg_includevat', 'discount', 'salecode_dis', 'dis_pay', 'receive', 'tis_price_p_part', 'tis_unit_new', 'tis_sum_price', 'tis_dis'], 'number'],
            [['unitw', 'sale_code_id', 'status_claim', 'status_PDS', 'sent_view_ytaxlist', 'sent_view_receive_Sale_Credit'], 'integer'],
            [['ytax_id', 'cvhc_main_id', 'ref_id', 'drepair_inform_id', 'drepair_inform_list_id', 'drepair_inform_listpart_id', 'wage_id', 'crepair_id', 'crepair_list_id', 'pw_id', 'pw_code', 'pw_code_backup', 'cam_id', 'id_cam', 'salecode_name', 'time', 'tis_pw_code'], 'string', 'max' => 20],
            [['abrand_id', 'agroup_id', 'amodel_id'], 'string', 'max' => 200],
            [['ref_status', 'uses', 'type_list'], 'string', 'max' => 1],
            [['kind', 'status', 'warranty', 'DEL_FROM'], 'string', 'max' => 2],
            [['emp_id'], 'string', 'max' => 13],
            [['ydate_receiveparts'], 'string', 'max' => 25],
            [['tis_pw_code_backup'], 'string', 'max' => 30],
            [['tis_pw_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ytax_id' => 'Ytax ID',
            'cvhc_main_id' => 'Cvhc Main ID',
            'ref_id' => 'Ref ID',
            'drepair_inform_id' => 'Drepair Inform ID',
            'drepair_inform_list_id' => 'Drepair Inform List ID',
            'drepair_inform_listpart_id' => 'Drepair Inform Listpart ID',
            'abrand_id' => 'Abrand ID',
            'agroup_id' => 'Agroup ID',
            'amodel_id' => 'Amodel ID',
            'wage_id' => 'Wage ID',
            'ref_status' => 'Ref Status',
            'crepair_id' => 'Crepair ID',
            'crepair_name' => 'Crepair Name',
            'crepair_list_id' => 'Crepair List ID',
            'crepair_list_name' => 'Crepair List Name',
            'pw_id' => 'Pw ID',
            'pw_code' => 'Pw Code',
            'pw_code_backup' => 'Pw Code Backup',
            'kind' => 'Kind',
            'pw_name' => 'Pw Name',
            'price_u' => 'Price U',
            'unit' => 'Unit',
            'unitstock' => 'Unitstock',
            'unitw' => 'Unitw',
            'vat_price' => 'Vat Price',
            'invat_price' => 'Invat Price',
            'novat_price' => 'Novat Price',
            'novat_cap' => 'Novat Cap',
            'vat_cap' => 'Vat Cap',
            'invat_cap' => 'Invat Cap',
            'avg_price' => 'Avg Price',
            'avg_vat' => 'Avg Vat',
            'avg_includevat' => 'Avg Includevat',
            'cam_id' => 'Cam ID',
            'id_cam' => 'Id Cam',
            'cam_name' => 'Cam Name',
            'discount' => 'Discount',
            'sale_code_id' => 'Sale Code ID',
            'salecode_name' => 'Salecode Name',
            'salecode_dis' => 'Salecode Dis',
            'dis_pay' => 'Dis Pay',
            'receive' => 'Receive',
            'emp_id' => 'Emp ID',
            'time' => 'Time',
            'uses' => 'Uses',
            'status' => 'Status',
            'warranty' => 'Warranty',
            'type_list' => 'Type List',
            'status_claim' => 'Status Claim',
            'ydate_receiveparts' => 'Ydate Receiveparts',
            'tis_price_p_part' => 'Tis Price P Part',
            'tis_unit_new' => 'Tis Unit New',
            'tis_sum_price' => 'Tis Sum Price',
            'tis_dis' => 'Tis Dis',
            'tis_pw_code' => 'Tis Pw Code',
            'tis_pw_code_backup' => 'Tis Pw Code Backup',
            'tis_pw_name' => 'Tis Pw Name',
            'status_PDS' => 'Status  Pds',
            'DEL_FROM' => 'Del  From',
            'sent_view_ytaxlist' => 'Sent View Ytaxlist',
            'sent_view_receive_Sale_Credit' => 'Sent View Receive  Sale  Credit',
        ];
    }
}
