<?php

namespace app\modules\baymanagement\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/**
 * This is the model class for table "erepair_group".
 *
 * @property integer $id
 * @property string $name
 * @property integer $priority
 * @property string $emp_id
 * @property string $add_time
 * @property string $status
 */
class ErepairGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erepair_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'priority', 'emp_id', 'add_time', 'status'], 'required'],
            [['priority'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['emp_id'], 'string', 'max' => 13],
            [['add_time'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'priority' => 'Priority',
            'emp_id' => 'Emp ID',
            'add_time' => 'Add Time',
            'status' => 'Status',
        ];
    }


    public function search($params)
    {
        $data = ErepairGroup::find()->where('status <> 99 ');;
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','name',$this->name]); //	ชื่อเต็มบริษัท
        $data->andFilterWhere(['like','priority',$this->priority]); //	ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
