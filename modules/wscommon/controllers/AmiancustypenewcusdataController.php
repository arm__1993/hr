<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Amiancustypenewcusdata;
use yii\helpers\ArrayHelper;


class AmiancustypenewcusdataController extends ActiveController
{
 
 public $modelClass = 'app\modules\wscommon\models\Amiancustypenewcusdata';


    public function behaviors()
    {
        return
            ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                ],
            ],
        ],
        parent::behaviors());
    }


    public function actionSearchtypecusdatabyidcustomer()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $bename = new Amiancustypenewcusdata();
        $benameSearch = $bename->searchtypecusdatabyidcustomer($cusno);
        return $benameSearch;
    }
    

 
}
