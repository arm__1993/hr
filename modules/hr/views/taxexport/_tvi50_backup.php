<?php

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/payroll_report_pdf.css");

$img = Yii::$app->request->baseUrl.'/images/Circle_-_black_simple.svg.png';



?>
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun";
        font-size: 16px;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;

    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }

    .textbox_value {
        font-family: "THSarabun";
        font-size: 13px;
    }

    .text_title {
        font-family: "THSarabun";
        font-size: 14px;
    }
    .text_italic {
        font-style: italic;
    }

    .text_header{
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        text-align: center !important;
    }

    .text_header2{
        font-family: "THSarabun" !important;
        font-size: 20px !important;
        font-weight: bold;
        text-align: center !important;
    }

    .container-page {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 200px;
        padding: 3px 3px;
    }
    .addr_company {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 80px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .addr_pay {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 120px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .income_detail {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 430px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .pay_in {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 30px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }


    .pay_by {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 100%;
        height: 30px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        margin-bottom: 2px;
    }

    .warning {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 43%;
        height: 100px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        float: left;
        padding: 3px 3px;
    }

    .sign {
        font-family: "THSarabun" !important;
        font-size: 16px !important;
        border: 1px solid #2A2E31;
        width: 54%;
        height: 100px;
        border: 1px solid #2A2E31;
        border-radius: 8px;
        float: right;
        padding: 3px 3px;
    }
    td {
        font-family: "THSarabun" !important;
        font-size: 14px !important;
    }
    .circle {
  width: 70px;
  height: 70px;
  border-radius: 10%;
  font-family: "THSarabun" !important;
  font-size: 16px;
  color: #000000;
  line-height: 30px;
  text-align: center;
  background: #F5F5F5
}
.bod {
    background-image: url('https://scontent.fbkk1-3.fna.fbcdn.net/v/t1.0-9/38126643_2183521141689528_2569118921001009152_n.jpg?_nc_fx=fbkk1-2&_nc_cat=0&oh=af97bc756dd37051b6ef52bcc3372370&oe=5C01704B');
    background-repeat: no-repeat;
    background-position: 99% 92%;
   
    background-attachment: fixed;
    background-size :200px
}

}

</style>


<div class='bod'>



<div class="container" style="padding-top:0px">
    <div class="text_title">
        <strong>ฉบับที่ 1</strong> <span class="text_italic">(สำหรับผู้ถูกหักภาษี ณ ที่จ่าย ใช้แนบพร้อมกับแบบแสดงรายการภาษี)</span><br>
        <strong>ฉบับที่ 2</strong> <span class="text_italic">(สำหรับผู้ถูกหักภาษี ณ ที่จ่าย เก็บไว้เป็นหลักฐาน)</span>
    </div>

    <div class="container-page">
        <table border="0" width="100%">
            <tr>
                <td width="90%" style="text-align: center;">
                    <div class="text_header">
                        <span class="text_header2">หนังสือรับรองการหักภาษี ณ ที่จ่าย </span> <br/>
                        ตามมาตรา 50 ทวิ แห่งประมวลรัษฎากร
                    </div>
                </td>
                <td width="10%">
                    <p><font size='1'>เล่มที่......................</font></p>
                    <p><font size='1'>เลขที่......................</font></p>
                </td>
            </tr>
        </table>
        <div class="addr_company">
            <table width="100%">
                <tr>
                    <td width="40%">ผู้มีหน้าที่หักภาษี ณ ที่จ่าย : -</td>
                    <td width="25%">เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)*</td>
                    <td width="35%" height="30"><table border="1" style="border-collapse: collapse; border: 0.5px solid #1a2226 padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                            <tr>
                                <td>3</td>
                                <td style="border:0px;">-</td>
                                <td>5</td>
                                <td style="border-left:0px;">6</td>
                                <td style="border-left:0px;">0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border:0px;">-</td>
                                <td>0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border-left:0px;">7</td>
                                <td style="border-left:0px;">5</td>
                                <td>6</td>
                                <td style="border:0px;">-</td>
                                <td>7</td>
                                <td style="border-left:0px;">7</td>
                                <td style="border:0px;">-</td>
                                <td>8</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>ชื่อ บริษัท นกเงือกโซลูชั่น จำกัด <br/>
                        <span style="font-style: italic;font-size: 12px;padding-top: -5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ให้ระบุว่าเป็น บุคคล นิติบุคคล บริษัท สมาคม หรือคณะบุคคล)</span>
                    </td>
                    <td>เลขประจําตัวผู้เสียภาษีอากร</td>
                    <td align="center">
                        <table border="1" style="border-collapse: collapse; border: 0.5px solid #1a2226 padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='80%'>
                            <tr>
                                <td>3</td>
                                <td style="border:0px;">-</td>
                                <td>5</td>
                                <td style="border-left:0px;">6</td>
                                <td style="border-left:0px;">0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border:0px;">-</td>
                                <td>0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border-left:0px;">5</td>
                                <td>6</td>
                                <td style="border:0px;">-</td>
                                <td>8</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">ที่อยู่ ........................................................................................................................................................................................ <br/>
                        <span style="font-style: italic;font-size: 12px;padding-top: -5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ให้ระบุ ชื่ออาคาร/หมู่บ้าน ห้องเลขที่ ชั้นที่ เลขที่ ตรอก/ซอย หมู่ที่ ถนน ตำบล/แขวง อำเภอ/เขต จังหวัด)</span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="addr_pay">
            <table width="100%">
                <tr>
                    <td width="40%">ผู้ถูกหักภาษี ณ ที่จ่าย : -</td>
                    <td width="25%">เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)*</td>
                    <td width="35%" height="30">
                    <table border="1" style="border-collapse: collapse; border: 0.5px solid #1a2226 padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                            <tr>
                                <td>3</td>
                                <td style="border:0px;">-</td>
                                <td>5</td>
                                <td style="border-left:0px;">6</td>
                                <td style="border-left:0px;">0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border:0px;">-</td>
                                <td>0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border-left:0px;">7</td>
                                <td style="border-left:0px;">5</td>
                                <td>6</td>
                                <td style="border:0px;">-</td>
                                <td>7</td>
                                <td style="border-left:0px;">7</td>
                                <td style="border:0px;">-</td>
                                <td>8</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>ชื่อ บริษัท นกเงือกโซลูชั่น จำกัด <br/>
                        <span style="font-style: italic;font-size: 12px;padding-top: -5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ให้ระบุว่าเป็น บุคคล นิติบุคคล บริษัท สมาคม หรือคณะบุคคล)</span>
                    </td>
                    <td>เลขประจําตัวผู้เสียภาษีอากร</td>
                    <td align="center">
                        <table border="1" style="border-collapse: collapse; border: 0.5px solid #1a2226 padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='80%'>
                            <tr>
                                <td>3</td>
                                <td style="border:0px;">-</td>
                                <td>5</td>
                                <td style="border-left:0px;">6</td>
                                <td style="border-left:0px;">0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border:0px;">-</td>
                                <td>0</td>
                                <td style="border-left:0px;">5</td>
                                <td style="border-left:0px;">5</td>
                                <td>6</td>
                                <td style="border:0px;">-</td>
                                <td>8</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">ที่อยู่ ........................................................................................................................................................................................ <br/>
                        <span style="font-style: italic;font-size: 12px;padding-top: -5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ให้ระบุ ชื่ออาคาร/หมู่บ้าน ห้องเลขที่ ชั้นที่ เลขที่ ตรอก/ซอย หมู่ที่ ถนน ตำบล/แขวง อำเภอ/เขต จังหวัด)</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">

                        <table  width="100%">
                            <tr>
                                <td width="40%" rowspan="2">ลำดับที่ <input> ในแบบ

                                    <br/><span style="font-style: italic; font-size: 12px;"> (ให้สามารถอ้างอิงหรือสอบยันกันได้ระหว่างลำดับที่ตาม หนังสือรับรองฯ กับแบบยื่นรายการภาษีหักที่จ่าย)</span>
                                </td>
                                <td width="15%"><input type="checkbox">&nbsp;(1) ภ.ง.ด.1ก</td>
                                <td width="15%"><input type="checkbox">&nbsp;(2) ภ.ง.ด.1ก พิเศษ</td>
                                <td width="15%"><input type="checkbox">&nbsp;(3) ภ.ง.ด.2</td>
                                <td width="15%"><input type="checkbox" checked="checked">&nbsp;(4) ภ.ง.ด.3</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox">&nbsp;(5) ภ.ง.ด.2ก</td>
                                <td><input type="checkbox">&nbsp;(6) ภ.ง.ด.3ก</td>
                                <td><input type="checkbox">&nbsp;(7) ภ.ง.ด.53</td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="income_detail">
            <div class="row">
                <table class='text_title' width="100%" cellpadding='0' cellspacing ='0' >
                <tr>
                    <th style=" border-bottom: 1px solid #000000;"   width="50%">ประเภทเงินได้พึงประเมินที่จ่าย</th>
                    <th  style=" border-bottom: 1px solid #000000;"   width="10%">วัน เดือน หรือปีภาษี ที่จ่าย</th>
                    <th style=" border-bottom: 1px solid #000000;"    width="30%">จํานวนเงินที่จ่าย</th>
                    <th  style=" border-bottom: 1px solid #000000;"   width="10%">ภาษีที่หัก และ นําส่งไว้</th>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;1.เงินเดือนค่าจ้างเบี้ยเลี้ยงโบนัสฯลฯ ตามมาตรา 40 (1)</td>
                    <td style=" border-right: 1px solid #000000;  border-bottom: 1px dotted #696969;">123</td>
                    <td style=" border-right: 1px solid #000000;  border-bottom: 1px dotted #696969;">456</td>
                    <td style=" border-right: 1px solid #000000; border-bottom: 1px dotted #696969; "> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000; " >&nbsp;&nbsp;&nbsp;2.ค่าธรรมเนียมค่านายหน้าฯลฯตามมาตรา 40 (2)</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;3. ค่าแห่งลิขสิทธ์ ฯลฯ ตามมาตรา 40 (3)</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>

                <tr>
                    <td  style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;4. (ก) ดอกเบี้ย ฯลฯ ตามมาตรา 40 (4) (ก)</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ข) เงิน ปันผล เงิน ส่วนแบ่ง กําไร ฯลฯ ตามมาตรา 40 (4) (ข)</td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1) กรณีผู้ได้รับเงินปันผลได้รับเครดิตภาษี โดยจ่ายจาก</td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กำไรสุทธิของกิจการที่ต้องเสียภาษีได้นิติบุคคลในอัตตรานี้</td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                    <td style=" border-right: 1px solid #000000;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1.1) อัตราร้อยละ 30 ของกําไรสุทธิ</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1.2) อัตราร้อยละ 25 ของกําไรสุทธิ</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1.3) อัตราร้อยละ 20 ของกําไรสุทธิ</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1.4) อัตราอื่น(ระบุ)................ ของกําไรสุทธิ</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>


                 </tr><tr >
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (2) กรณีผู้ได้รับเงินปันผลไม่ได้รับเครดิตภาษี เนื่องจากจ่ายจาก </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2.1) กําไรสุทธิของกิจการที่ได้รับยกเว้นภาษีเงินได้นิตบุคล</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2.2) เงินปันผลหรือเงินส่วนแบ่งของกําไรที่ได้รับยกเว้นไม่ต้องนํามารวม
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;คํานวณเป็นรายได้เพื่อเสียภาษีเงินได้นิติบุคคล</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2.3) กําไรสุทธิส่วนที่ได้หักผลขาดทุนสุทธิยกมาไม่เกิน 5 ปี  ก่อนรอบระยะเวลาบัญชีปีปัจจุบัน</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2.4) กําไรที่รับรู้ทางบัญชีโดยวิธีส่วนได้เสีย (equity method)</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2.5) อื่นๆ (ระบ)ุ ..........................................................................</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;5. การจ่ายเงินได้ที่ต้องหักภาษี ณ ที่จ่ายตามคําสั่งกรมสรรพากรที่ออกตามมาตรา 3 เตรส เช่น
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;รางวัลส่วนลดหรือ ประโยชน์ใดๆ เนื่องจากการส่งเสริมการขายรางวัล ในการประกวด การแข่งขัน
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การชิงโชค ค่าแสดงของนัก แสดงสาธารณะค่าจ้างทําของค่าโฆษณา ค่าเช่า ค่าขนสง่ ค่าบริการ
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค่าเบี้ยประกันวินาศภัย ฯลฯ</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>
                <tr>
                    <td style=" border-right: 1px solid #000000;">&nbsp;&nbsp;&nbsp;6. อื่น ๆ (ระบ)ุ .........................................................................................................................</td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                    <td style=" border-right: 1px solid #000000;border-bottom: 1px dotted #696969;"> </td>
                </tr>

                <tr>
                    <td style=" border-right: 1px solid #000000  border-bottom: 1px solid #696969;"></td>
                    <td style=" border-right: 1px solid #000000; border-bottom: 1px solid #696969;"> </td>
                    <td style=" border-right: 1px solid #000000; border-bottom: 1px solid #696969;"> </td>
                    <td style=" border-right: 1px solid #000000; border-bottom: 1px solid #696969;"> </td>
                </tr>

                </table>






            </div>

        </div>
        <div class="pay_in">
        <strong> &nbsp;&nbsp;&nbsp; รวมเงินภาษีที่หักนําส่ง (ตัวอักษร) </strong> <input type='text' class='textbox_value' size="50" value = "">
        </div>

        <div class="pay_by">
        <strong>&nbsp;&nbsp;ผู้จ่ายเงิน</strong> <tr>
                                <td><input type="checkbox">&nbsp;(1) หัก ณ ที่จ่าย </td>
                                <td><input type="checkbox">&nbsp;(2) ออกให้ตลอดไป</td>
                                <td><input type="checkbox">&nbsp;(3) ออกให้ครั้งเดียว</td>
                                <td><td><input type="checkbox">&nbsp;(4) อื่น ๆ (ระบุ)................................................</td></td>
                            </tr>
        </div>

        <div>
            <div class="warning">
            <table class=''>
                 <tr>
                    <td><strong>คำเตือน</strong></th>
                    <td> ผู้มีหน้าที่ออกหนังสือรับรองการหักภาษี ณ ที่จ่าย</td>
                </tr>
                <tr>
                    <td></td>
                    <td>ฝ่าฝืนไม่ปฏิบัติ ตามมาตรา 50 ทวิแห่งประมวล</td>
                </tr>
                <tr>
                    <td></td>
                    <td> รัษฎากร ต้องรับโทษทางอาญาตามมาตรา 35 แห่งประมวลรัษฎากร</td>
                </tr>
                </table>



            </div>
            <div class="sign">
                <div class="row">
                    <div class="col-md-12">
                        <div  align="center" class="col-md-1" >
                            ขอรับรองว่าข้อความและตัวเลขดังกล่าวข้างต้นถ้าต้องตรงกับความจริงทุกประการ
                        </div>
                        <div  align="center" class="col-md-11">
                        ลงชื่อ....................................................................................ผู้จ่ายเงิน
                        ......................./.................................../..................................

                        </div>
                        <div  align="center" class="col-md-11">
                        (วัน เดือน ปี ที่ออกหนังสือรับ รองฯ)
                        </div>
                        <!-- <div class="circle">ตราประทับ นิติบุคคล(ถ้ามี)</div> -->
                        <!-- <img src="https://thaistudentsdotnl.files.wordpress.com/2013/01/authorized-stamp.gif" hspace="50" vspace="0"   > -->

                    </div>
                </div>
            </div>
        </div>

    <table class=''>
    <tr>
    <td> หมายเหตุ เลขประจําตัวผู้เสียภาษีอากร (13 หลัก)* หมายถึง</th>
    <td> 1.กรณีบคุคลธรรมดาไทย ให้ใช้เลขประจําตัวประชาชนของกรมการปกครอง</td>
    </tr>
    <tr>
    <td></td>
    <td>2.กรณีนิติบุคคลให้ใช้เลขทะเบียนนติบิคุคลของกรมพฒันาธรุกจิการค้า</td>
    </tr>
    <tr>
    <td></td>
    <td>3.กรณอี่นๆ นอกเหนือจาก 1. และ 2. ให้ใช้เลขประจําตัวผู้เสียภาษีอากร (13 หลัก ) ของกรมสรรพากร</td>
    </tr>
    </table>


    </div>
</div>
</div>
<script>
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.beginPath();
ctx.arc(95,50,40,0,2*Math.PI);
ctx.fillText("ประทับตรา นิติบุคคล(ถ้ามี)",10,50);
ctx.stroke();
</script>
