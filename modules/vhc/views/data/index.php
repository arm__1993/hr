<?php

use app\modules\hr\apihr\ApiHr;

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\Utility;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\modules\vhc\models\VHC\VhcModel;
use app\modules\vhc\models\VHC\VhcGrouplist;
use app\modules\vhc\models\VHC\VhcList;


use app\modules\vhc\api\ApiServiceVHC;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/vhc/data/vhc_model.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/vhc/data/service_type.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/vhc/data/vhcblame.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/vhc/data/vhcgrouplist.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/vhc/data/vhclist.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/vhc/vhc/data/vhclistoption.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li>ตั้งค่า VHC</li>

            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">

                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#vhc_model" data-toggle="tab">vhc model</a></li>
                            <li><a href="#setgroup_vhc" data-toggle="tab"> กลุ่มรายการ ​VHC list </a></li>
                            <li><a href="#setlist_vhc" data-toggle="tab">กำหนด รายการ vhc</a></li>
                            <li><a href="#setlistoptions_vhc" data-toggle="tab">กำหนด ตัวเลือกรายการ vhc</a></li>
                            <li><a href="#service_type" data-toggle="tab">ประเภทให้บริการ</a></li>
                            <li><a href="#cannot_check" data-toggle="tab">รอยตำหนิที่ไม่สามารถตรวจสอบได้</a></li>
                        </ul>


                        <div class="tab-content">

                            <!-- Start vhc_model -->
                            <div class="tab-pane active" id="vhc_model">
                                <!--modal start-->
                                <div class="row">
                                    <div class="col-md-11"></div>
                                    <div class="col-md-1">
                                        <?php
                                        Modal::begin([
                                            'id' => 'modalfrmAddVhcmodel',
                                            'header' => '<strong>เพิ่ม VHC MODEL</strong>',
                                            'toggleButton' => [
                                                'id' => 'btnAddNewVhcmodel',
                                                'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                'class' => 'btn btn-success'
                                            ],
                                            'closeButton' => [
                                                'label' => '<i class="fa fa-close"></i>',
                                                //'class' => 'close pull-right',
                                                'class' => 'btn btn-success btn-sm pull-right'
                                            ],
                                            'size' => 'modal-md'
                                        ]);
                                        ?>
                                        <form role="form" id="frmAddVhcmodel">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>ชื่อรายการ <span>*</span></label>
                                                    <input id="model_name" name="model_name"
                                                           data-required="true"
                                                           class="form-control model_name" type="text"
                                                           placeholder="ชื่อรายการ">
                                                </div>
                                                <!-- /.box-body -->

                                                <div class="box-body">
                                                    <label>สถานะ</label>
                                                    <br/>
                                                    <input type="checkbox" class="record_status"
                                                           id="record_status" value="1"
                                                           name="record_status">แสดง

                                                </div>

                                                <div class="box-body">
                                                    <?php echo Html::hiddenInput('hide_activityedit', null, ['id' => 'hide_activityedit', 'class' => 'hide_activityedit']); ?>
                                                    <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivity']); ?>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        Modal::end();
                                        ?>
                                    </div>
                                </div>
                                <!--end start-->

                                <!--table start-->

                                <div class="row">
                                    <div class="col-md-12">

                                        <?php
                                        Pjax::begin(['id' => 'pjax_tb_deducttemp']);
                                        echo GridView::widget([
                                            'dataProvider' => $showvhcindex,
                                            'filterModel' => $showvhc,
                                            'columns' => [
                                                [
                                                    'header' => 'ที่',
                                                    'class' => 'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '23'],
                                                ],
                                                [
                                                    'attribute' => 'model_name',
                                                    'header' => 'ชื่อรายการ',
                                                    'value' => 'model_name',
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'record_status',*/
                                                    'label' => 'สถานะ',
                                                    // 'value' => 'record_status',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->record_status);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                ],
                                                [

                                                    //'attribute' => 'create_by',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'value' => 'create_by',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->create_by);
                                                        //  print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'update_by',*/
                                                    'label' => 'บันทึกด้วยวันเวลา',
                                                    //   'value' => 'create_date',
                                                    'filter' => false,
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_date);
                                                    },
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']
                                                ],
                                                [

                                                    'class' => 'yii\grid\ActionColumn',
                                                    'header' => 'จัดการข้อมูลบริษัท',
                                                    'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        editaddedduct(' . $data->id . ',1);
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->model_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletevhcmodel(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                ],


                                            ],
                                        ]);
                                        Pjax::end();
                                        ?>

                                    </div>
                                </div>


                            </div>
                            <!-- End vhc_model -->
                            <!-- Start setgroup_vhc -->
                            <div class="tab-pane" id="setgroup_vhc">
                                <!--modal start-->
                                <div class="row">
                                    <div class="col-md-11"></div>

                                    <div class="col-md-1">
                                        <?php
                                        Modal::begin([
                                            'id' => 'modalfrmAddVhcgrouplist',
                                            'header' => '<strong>เพิ่ม VHC MODEL</strong>',
                                            'toggleButton' => [
                                                'id' => 'btnAddNewVhcgrouplist',
                                                'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                'class' => 'btn btn-success'
                                            ],
                                            'closeButton' => [
                                                'label' => '<i class="fa fa-close"></i>',
                                                //'class' => 'close pull-right',
                                                'class' => 'btn btn-success btn-sm pull-right'
                                            ],
                                            'size' => 'modal-md'
                                        ]);
                                        ?>
                                        <form role="form" id="frmAddVhcgrouplist">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label class="col-md-2">ชื่อรายการ </label>
                                                    <div class="col-md-10">
                                                        <input id="group_name" name="group_name"
                                                               data-required="true"
                                                               class="form-control group_name" type="text"
                                                               placeholder="ชื่อรายการ">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="box-body">
                                                        <label class="col-md-2">model</label>

                                                        <div class="col-sm-10">
                                                            <?php
                                                            $modelcvhname = VhcModel::find()->where('record_status = 1')->asArray()->all();
                                                            // print_r($modelcvhname);
                                                            ?>
                                                            <select id="model_id" name="model_id" class="col-md-12">
                                                                <option value="">เลือกกลุ่ม</option>
                                                                <?php foreach ($modelcvhname as $k => $v) { ?>
                                                                    <option value="<?php echo $v['id'] ?>"><?php echo $v['model_name'] ?></option>
                                                                <?php }; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->

                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" class="record_status"
                                                                       id="record_status_grouplist" value="1"
                                                                       name="record_status"> สถานะ
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="box-body">
                                                    <?php echo Html::hiddenInput('hide_activityedit_grouplist', null, ['id' => 'hide_activityedit_grouplist', 'class' => 'hide_activityedit_grouplist']); ?>
                                                    <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivitygrouplist']); ?>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        Modal::end();
                                        ?>
                                    </div>

                                </div>
                                <!--end start-->

                                <!--table start-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        Pjax::begin(['id' => 'pjax_tb_grouplist']);
                                        echo GridView::widget([
                                            'dataProvider' => $showvhcgrouplistindex,
                                            'filterModel' => $showvhcgrouplist,
                                            'columns' => [
                                                [
                                                    'header' => 'ที่',
                                                    'class' => 'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '23'],
                                                ],
                                                [
                                                    'attribute' => 'model_id',
                                                    'header' => 'กลุ่มรายการ',
                                                    'value' => function ($data) {
                                                        // return Utility::dispActive($data->model_id);
                                                        $namegrouplist = ApiServiceVHC::grouplistnamebyid($data->model_id);
                                                        return $namegrouplist['model_name'];
                                                    },
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center'],
                                                    'filter' => ApiServiceVHC::grouplistnameall(),
                                                ],
                                                [
                                                    'attribute' => 'group_name',
                                                    'header' => 'ชื่อรายการ',
                                                    'value' => 'group_name',
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'record_status',*/
                                                    'label' => 'สถานะ',
                                                    // 'value' => 'record_status',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->record_status);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                ],
                                                [

                                                    //'attribute' => 'create_by',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    'value' => 'create_by',
                                                    /*'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->create_by);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',*/
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                ],

                                                [
                                                    /* 'attribute' => 'update_by',*/
                                                    'label' => 'บันทึกด้วยวันเวลา',
                                                    //   'value' => 'create_date',
                                                    'filter' => false,
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_date);
                                                    },
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']
                                                ],
                                                [

                                                    'class' => 'yii\grid\ActionColumn',
                                                    'header' => 'จัดการข้อมูลบริษัท',
                                                    'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        editgrouplist(' . $data->id . ',1);
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->group_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletevhcgrouplist(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                ],
                                            ],

                                        ]);
                                        Pjax::end();
                                        ?>
                                    </div>
                                </div>
                                <!--table end-->


                            </div>
                            <!-- End setgroup_vhc-->

                            <!-- Start setlist_vhc -->
                            <div class="tab-pane" id="setlist_vhc">
                                <!--modal start-->
                                <div class="row">
                                    <div class="col-md-11"></div>
                                    <div class="col-md-1">
                                        <?php
                                        Modal::begin([
                                            'id' => 'modalfrmAddVhclist',
                                            'header' => '<strong>กำหนด รายการ vhc</strong>',
                                            'toggleButton' => [
                                                'id' => 'btnAddNewVhclist',
                                                'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                'class' => 'btn btn-success'
                                            ],
                                            'closeButton' => [
                                                'label' => '<i class="fa fa-close"></i>',
                                                //'class' => 'close pull-right',
                                                'class' => 'btn btn-success btn-sm pull-right'
                                            ],
                                            'size' => 'modal-md'
                                        ]);
                                        ?>
                                        <form role="form" id="frmAddVhclist">
                                            <div class="form-group">
                                                <div class="box-body">

                                                    <label class="col-md-2">ชื่อรายการ</label>
                                                    <div class="col-md-10">
                                                        <input id="list_name" name="list_name"
                                                               data-required="true"
                                                               class="form-control group_name " type="text"
                                                               placeholder="ชื่อรายการ">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="box-body">
                                                        <label class="col-md-2">model</label>

                                                        <div class="col-sm-10">
                                                            <?php
                                                            $modelcvhname = VhcGrouplist::find()->where('record_status = 1')->asArray()->all();
                                                            // print_r($modelcvhname);
                                                            ?>
                                                            <select class="col-sm-12" id="group_id" name="group_id">
                                                                <option value="">เลือกกลุ่ม</option>
                                                                <?php foreach ($modelcvhname as $k => $v) { ?>
                                                                    <option value="<?php echo $v['id'] ?>"><?php echo $v['group_name'] ?></option>
                                                                <?php }; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" class="record_status"
                                                                       id="record_status_list" value="1"
                                                                       name="record_status"> สถานะ
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="box-body">
                                                    <?php echo Html::hiddenInput('hide_activityedit_list', null, ['id' => 'hide_activityedit_list', 'class' => 'hide_activityedit_list']); ?>
                                                    <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivitylist']); ?>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        Modal::end();
                                        ?>
                                    </div>
                                </div>
                                <!--end start-->

                                <!--table start-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        Pjax::begin(['id' => 'pjax_tb_list']);
                                        echo GridView::widget([
                                            'dataProvider' => $showvhclistindex,
                                            'filterModel' => $showvhclist,
                                            'columns' => [
                                                [
                                                    'header' => 'ที่',
                                                    'class' => 'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '23'],
                                                ],
                                                [
                                                    'attribute' => 'group_id',
                                                    'header' => 'กลุ่มรายการ',
                                                    'value' => function ($data) {
                                                        // return Utility::dispActive($data->model_id);
                                                        $namelist = ApiServiceVHC::listnamebyid($data->group_id);
                                                        return $namelist['group_name'];
                                                    },
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center'],
                                                    /* 'filter' => ApiServiceVHC::listnameall(),*/
                                                ],
                                                [
                                                    'attribute' => 'list_name',
                                                    'header' => 'ชื่อรายการ',
                                                    'value' => 'list_name',
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'record_status',*/
                                                    'label' => 'สถานะ',
                                                    // 'value' => 'record_status',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->record_status);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                ],
                                                [

                                                    //'attribute' => 'create_by',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    'value' => 'create_by',
                                                    /*'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->create_by);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',*/
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                ],

                                                [
                                                    /* 'attribute' => 'update_by',*/
                                                    'label' => 'บันทึกด้วยวันเวลา',
                                                    //   'value' => 'create_date',
                                                    'filter' => false,
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_date);
                                                    },
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']
                                                ],
                                                [

                                                    'class' => 'yii\grid\ActionColumn',
                                                    'header' => 'จัดการข้อมูลบริษัท',
                                                    'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        editlist(' . $data->id . ',1);
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->list_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletevhclist(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                ],
                                            ],

                                        ]);
                                        Pjax::end();
                                        ?>
                                    </div>
                                </div>
                                <!--table end-->


                            </div>
                            <!-- End setlist_vhc-->

                            <!-- Start setlistoptions_vhc -->
                            <div class="tab-pane" id="setlistoptions_vhc">
                                <!--modal start-->
                                <div class="row">
                                    <div class="col-md-11"></div>
                                    <div class="col-md-1">
                                        <?php
                                        Modal::begin([
                                            'id' => 'modalfrmAddVhclistoption',
                                            'header' => '<strong>เพิ่ม VHC listoption</strong>',
                                            'toggleButton' => [
                                                'id' => 'btnAddNewVhclistoption',
                                                'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                'class' => 'btn btn-success'
                                            ],
                                            'closeButton' => [
                                                'label' => '<i class="fa fa-close"></i>',
                                                //'class' => 'close pull-right',
                                                'class' => 'btn btn-success btn-sm pull-right'
                                            ],
                                            'size' => 'modal-md'
                                        ]);
                                        ?>
                                        <form role="form" id="frmAddVhclistoption">
                                            <div class="form-group">
                                                <div class="modal-body">
                                                    <div class="box-body">

                                                        <div class="form-group">
                                                            <label for="inputPassword3"
                                                                   class="col-sm-2 control-label">รายการ</label>

                                                            <div class="col-sm-10">
                                                                <?php
                                                                $modelcvhnameoption = VhcList::find()->where('record_status = 1')->asArray()->all();
                                                                // print_r($modelcvhname);
                                                                ?>
                                                                <select class="form-control" id="list_id"
                                                                        name="list_id">
                                                                    <option value="">เลือกกลุ่ม</option>
                                                                    <?php foreach ($modelcvhnameoption as $k => $v) { ?>
                                                                        <option value="<?php echo $v['id'] ?>"><?php echo $v['list_name'] ?></option>
                                                                    <?php }; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <br/> <br/>
                                                        <div class="form-group">
                                                            <label for="inputPassword3"
                                                                   class="col-sm-2 control-label">ชื่อตัวเลือก</label>

                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control"
                                                                       id="option_name"
                                                                       name="option_name"
                                                                       placeholder="ชื่อตัวเลือก">
                                                            </div>
                                                        </div>

                                                        <br/> <br/>


                                                        <div class="form-group">
                                                            <label for="inputPassword3"
                                                                   class="col-sm-2 control-label">ค่า</label>

                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control"
                                                                       onKeyUp="if(this.value*1!=this.value) this.value='' ;"
                                                                       maxlength="3"
                                                                       id="option_value"
                                                                       name="option_value"
                                                                       placeholder="---">
                                                            </div>
                                                        </div>
                                                        <br/> <br/>
                                                        <div class="form-group">
                                                            <label for="inputPassword3"
                                                                   class="col-sm-2 control-label">comment</label>

                                                            <div class="col-sm-10"><textarea name="comment"
                                                                                             class="form-control"
                                                                                             rows="3"
                                                                                             id="comment"
                                                                                             placeholder="comment ..."></textarea>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-sm-offset-2 col-sm-10">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" class="record_status"
                                                                               id="record_status_listoption" value="1"
                                                                               name="record_status"> สถานะ
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="box-body">
                                                    <?php echo Html::hiddenInput('hide_activityedit_listoption', null, ['id' => 'hide_activityedit_listoption', 'class' => 'hide_activityedit_listoption']); ?>
                                                    <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivitylistoption']); ?>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        Modal::end();
                                        ?>
                                    </div>

                                </div>
                                <!--end start-->

                                <!--table start-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        Pjax::begin(['id' => 'pjax_tb_listoption']);
                                        echo GridView::widget([
                                            'dataProvider' => $showvhclistoptionindex,
                                            'filterModel' => $showvhclistoption,
                                            'columns' => [
                                                [
                                                    'header' => 'ที่',
                                                    'class' => 'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '23'],
                                                ],
                                                [
                                                    'attribute' => 'list_id',
                                                    'header' => 'กลุ่มรายการ',
                                                    'value' => function ($data) {
                                                        // return Utility::dispActive($data->model_id);
                                                        $namelist = ApiServiceVHC::listoptionnamebyid($data->list_id);
                                                        return $namelist['list_name'];
                                                    },
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center'],
                                                    /* 'filter' => ApiServiceVHC::listnameall(),*/
                                                ],
                                                [
                                                    'attribute' => 'option_name',
                                                    'header' => 'ชื่อรายการ',
                                                    'value' => 'option_name',
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'record_status',*/
                                                    'label' => 'สถานะ',
                                                    // 'value' => 'record_status',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->record_status);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                ],
                                                [

                                                    //'attribute' => 'create_by',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    'value' => 'create_by',
                                                    /*'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->create_by);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',*/
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                ],

                                                [
                                                    /* 'attribute' => 'update_by',*/
                                                    'label' => 'บันทึกด้วยวันเวลา',
                                                    //   'value' => 'create_date',
                                                    'filter' => false,
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_date);
                                                    },
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']
                                                ],
                                                [

                                                    'class' => 'yii\grid\ActionColumn',
                                                    'header' => 'จัดการข้อมูลบริษัท',
                                                    'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        editlistoption(' . $data->id . ',1);
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->option_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletevhclistoption(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                ],
                                            ],

                                        ]);
                                        Pjax::end();
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- End setlistoptions_vhc-->

                            <!-- Start service_type -->
                            <div class="tab-pane" id="service_type">


                                <!--modal start-->
                                <div class="row">
                                    <div class="col-md-11"></div>
                                    <div class="col-md-1">
                                        <?php
                                        Modal::begin([
                                            'id' => 'modalfrmAddVhcservice',
                                            'header' => '<strong>เพิ่ม ประเภทให้บริการ</strong>',
                                            'toggleButton' => [
                                                'id' => 'btnAddNewVhcservice',
                                                'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                'class' => 'btn btn-success'
                                            ],
                                            'closeButton' => [
                                                'label' => '<i class="fa fa-close"></i>',
                                                //'class' => 'close pull-right',
                                                'class' => 'btn btn-success btn-sm pull-right'
                                            ],
                                            'size' => 'modal-md'
                                        ]);
                                        ?>
                                        <form role="form" id="frmAddVhcservice">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>ชื่อรายการ <span>*</span></label>
                                                    <input id="servicetype_name" name="servicetype_name"
                                                           data-required="true"
                                                           class="form-control model_name" type="text"
                                                           placeholder="ชื่อรายการ">
                                                </div>
                                                <!-- /.box-body -->

                                                <div class="box-body">
                                                    <label>สถานะ</label>
                                                    <br/>
                                                    <input type="checkbox" class="record_status"
                                                           id="record_status_servicetype" value="1"
                                                           name="record_status">แสดง

                                                </div>

                                                <div class="box-body">
                                                    <?php echo Html::hiddenInput('hide_activityedit_servicetype', null, ['id' => 'hide_activityedit_servicetype', 'class' => 'hide_activityedit_servicetype']); ?>
                                                    <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivityservice']); ?>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        Modal::end();
                                        ?>
                                    </div>
                                </div>
                                <!--end start-->

                                <!--table start-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        Pjax::begin(['id' => 'pjax_tb_vhcservicetype']);
                                        echo GridView::widget([
                                            'dataProvider' => $showservicetypeindex,
                                            'filterModel' => $showservicetype,
                                            'columns' => [
                                                [
                                                    'header' => 'ที่',
                                                    'class' => 'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '23'],
                                                ],
                                                [
                                                    'attribute' => 'servicetype_name',
                                                    'header' => 'ชื่อรายการ',
                                                    'value' => 'servicetype_name',
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'record_status',*/
                                                    'label' => 'สถานะ',
                                                    // 'value' => 'record_status',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->record_status);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                ],
                                                [

                                                    //'attribute' => 'create_by',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'value' => 'create_by',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->create_by);
                                                        //  print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'update_by',*/
                                                    'label' => 'บันทึกด้วยวันเวลา',
                                                    //   'value' => 'create_date',
                                                    'filter' => false,
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_date);
                                                    },
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']
                                                ],
                                                [

                                                    'class' => 'yii\grid\ActionColumn',
                                                    'header' => 'จัดการข้อมูลบริษัท',
                                                    'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        editservicetype(' . $data->id . ',1);
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->servicetype_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletevhcservicetype(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                ],


                                            ],
                                        ]);
                                        Pjax::end();
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- End service_type-->

                            <!-- Start cannot_check -->
                            <div class="tab-pane" id="cannot_check">


                                <!--modal start-->
                                <div class="row">
                                    <div class="col-md-11"></div>
                                    <div class="col-md-1">
                                        <?php
                                        Modal::begin([
                                            'id' => 'modalfrmAddVhcblame',
                                            'header' => '<strong>เพิ่ม VHC Blame</strong>',
                                            'toggleButton' => [
                                                'id' => 'btnAddNewVhcblame',
                                                'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                'class' => 'btn btn-success'
                                            ],
                                            'closeButton' => [
                                                'label' => '<i class="fa fa-close"></i>',
                                                //'class' => 'close pull-right',
                                                'class' => 'btn btn-success btn-sm pull-right'
                                            ],
                                            'size' => 'modal-md'
                                        ]);
                                        ?>
                                        <form role="form" id="frmAddVhcblame">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>ชื่อรายการ <span>*</span></label>
                                                    <input id="blame_name" name="blame_name"
                                                           data-required="true"
                                                           class="form-control model_name" type="text"
                                                           placeholder="ชื่อรายการ">
                                                </div>
                                                <!-- /.box-body -->

                                                <div class="box-body">
                                                    <label>สถานะ</label>
                                                    <br/>
                                                    <input type="checkbox" class="record_status"
                                                           id="record_statuss" value="1"
                                                           name="record_status">แสดง

                                                </div>

                                                <div class="box-body">
                                                    <?php echo Html::hiddenInput('hide_activityedits', null, ['id' => 'hide_activityedits', 'class' => 'hide_activityedits']); ?>
                                                    <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivityblame']); ?>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        Modal::end();
                                        ?>
                                    </div>
                                </div>
                                <!--end start-->

                                <!--table start-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        Pjax::begin(['id' => 'pjax_tb_vhcblame']);
                                        echo GridView::widget([
                                            'dataProvider' => $showblameindex,
                                            'filterModel' => $showblame,
                                            'columns' => [
                                                [
                                                    'header' => 'ที่',
                                                    'class' => 'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '23'],
                                                ],
                                                [
                                                    'attribute' => 'blame_name',
                                                    'header' => 'ชื่อรายการ',
                                                    'value' => 'blame_name',
                                                    'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'record_status',*/
                                                    'label' => 'สถานะ',
                                                    // 'value' => 'record_status',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->record_status);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                ],
                                                [

                                                    //'attribute' => 'create_by',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'value' => 'create_by',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->create_by);
                                                        //  print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                ],
                                                [
                                                    /* 'attribute' => 'update_by',*/
                                                    'label' => 'บันทึกด้วยวันเวลา',
                                                    //   'value' => 'create_date',
                                                    'filter' => false,
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_date);
                                                    },
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']
                                                ],
                                                [

                                                    'class' => 'yii\grid\ActionColumn',
                                                    'header' => 'จัดการข้อมูลบริษัท',
                                                    'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        editblame(' . $data->id . ',1);
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->blame_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletevhcblame(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                    'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                ],


                                            ],
                                        ]);
                                        Pjax::end();
                                        ?>

                                    </div>
                                </div>
                                <!--table end-->


                            </div>
                        </div>
                        <!--End cannot_check -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->


            <!-- /.col -->
        </div>


    </div>
    </div>

    <!-- /.box -->
</section><!-- /.content -->