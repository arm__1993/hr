<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[OtReturnTransaction]].
 *
 * @see OtReturnTransaction
 */
class OtReturnTransactionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OtReturnTransaction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OtReturnTransaction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
