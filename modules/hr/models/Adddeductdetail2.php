<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ADD_DEDUCT_DETAIL".
 *
 * @property integer $ADD_DEDUCT_DETAIL_ID
 * @property string $ADD_DEDUCT_ID
 * @property string $ADD_DEDUCT_DETAIL
 * @property string $ADD_DEDUCT_DETAIL_PAY_DATE
 * @property string $ADD_DEDUCT_DETAIL_START_USE_DATE
 * @property string $ADD_DEDUCT_DETAIL_END_USE_DATE
 * @property string $INSTALLMENT_START
 * @property string $INSTALLMENT_END
 * @property string $INSTALLMENT_CURRENT
 * @property string $ADD_DEDUCT_DETAIL_EMP_ID
 * @property string $ADD_DEDUCT_DETAIL_AMOUNT
 * @property string $ADD_DEDUCT_DETAIL_TYPE
 * @property integer $ADD_DEDUCT_DETAIL_STATUS
 * @property string $ADD_DEDUCT_DETAIL_HISTORY_STATUS
 * @property string $ADD_DEDUCT_DETAIL_GROUP_STATUS
 * @property string $ADD_DEDUCT_DETAIL_CREATE_DATE
 * @property string $ADD_DEDUCT_DETAIL_CREATE_BY
 * @property string $ADD_DEDUCT_DETAIL_UPDATE_BY
 * @property string $ADD_DEDUCT_DETAIL_UPDATE_DATE
 * @property string $ADD_DEDUCT_DETAIL_RESP_PERSON
 * @property integer $dept_deduct_id
 * @property integer $dept_deduct_detail_id
 */
class Adddeductdetail2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ADD_DEDUCT_DETAIL';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ADD_DEDUCT_ID'], 'required'],
            [['ADD_DEDUCT_DETAIL_AMOUNT'], 'number'],
            [['ADD_DEDUCT_DETAIL_STATUS', 'dept_deduct_id', 'dept_deduct_detail_id'], 'integer'],
            [['ADD_DEDUCT_DETAIL_CREATE_DATE', 'ADD_DEDUCT_DETAIL_UPDATE_DATE'], 'safe'],
            [['ADD_DEDUCT_ID', 'INSTALLMENT_START', 'INSTALLMENT_END', 'INSTALLMENT_CURRENT'], 'string', 'max' => 50],
            [['ADD_DEDUCT_DETAIL'], 'string', 'max' => 250],
            [['ADD_DEDUCT_DETAIL_PAY_DATE', 'ADD_DEDUCT_DETAIL_START_USE_DATE', 'ADD_DEDUCT_DETAIL_END_USE_DATE', 'ADD_DEDUCT_DETAIL_CREATE_BY'], 'string', 'max' => 100],
            [['ADD_DEDUCT_DETAIL_EMP_ID', 'ADD_DEDUCT_DETAIL_UPDATE_BY', 'ADD_DEDUCT_DETAIL_RESP_PERSON'], 'string', 'max' => 20],
            [['ADD_DEDUCT_DETAIL_TYPE', 'ADD_DEDUCT_DETAIL_HISTORY_STATUS', 'ADD_DEDUCT_DETAIL_GROUP_STATUS'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ADD_DEDUCT_DETAIL_ID' => 'Add  Deduct  Detail  ID',
            'ADD_DEDUCT_ID' => 'Add  Deduct  ID',
            'ADD_DEDUCT_DETAIL' => 'Add  Deduct  Detail',
            'ADD_DEDUCT_DETAIL_PAY_DATE' => 'Add  Deduct  Detail  Pay  Date',
            'ADD_DEDUCT_DETAIL_START_USE_DATE' => 'Add  Deduct  Detail  Start  Use  Date',
            'ADD_DEDUCT_DETAIL_END_USE_DATE' => 'Add  Deduct  Detail  End  Use  Date',
            'INSTALLMENT_START' => 'Installment  Start',
            'INSTALLMENT_END' => 'Installment  End',
            'INSTALLMENT_CURRENT' => 'Installment  Current',
            'ADD_DEDUCT_DETAIL_EMP_ID' => 'Add  Deduct  Detail  Emp  ID',
            'ADD_DEDUCT_DETAIL_AMOUNT' => 'Add  Deduct  Detail  Amount',
            'ADD_DEDUCT_DETAIL_TYPE' => 'Add  Deduct  Detail  Type',
            'ADD_DEDUCT_DETAIL_STATUS' => 'Add  Deduct  Detail  Status',
            'ADD_DEDUCT_DETAIL_HISTORY_STATUS' => 'Add  Deduct  Detail  History  Status',
            'ADD_DEDUCT_DETAIL_GROUP_STATUS' => 'Add  Deduct  Detail  Group  Status',
            'ADD_DEDUCT_DETAIL_CREATE_DATE' => 'Add  Deduct  Detail  Create  Date',
            'ADD_DEDUCT_DETAIL_CREATE_BY' => 'Add  Deduct  Detail  Create  By',
            'ADD_DEDUCT_DETAIL_UPDATE_BY' => 'Add  Deduct  Detail  Update  By',
            'ADD_DEDUCT_DETAIL_UPDATE_DATE' => 'Add  Deduct  Detail  Update  Date',
            'ADD_DEDUCT_DETAIL_RESP_PERSON' => 'Add  Deduct  Detail  Resp  Person',
            'dept_deduct_id' => 'Dept Deduct ID',
            'dept_deduct_detail_id' => 'Dept Deduct Detail ID',
        ];
    }
}
