-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 07, 2017 at 10:36 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `b_title`
--

CREATE TABLE `b_title` (
  `id` int(11) NOT NULL,
  `title_name_th` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title_name_en` varchar(30) NOT NULL,
  `status_active` tinyint(3) UNSIGNED NOT NULL,
  `gender` tinyint(3) UNSIGNED NOT NULL COMMENT '1=ชาย,2=หญิง',
  `createby_user` varchar(30) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `updatteby_user` varchar(30) NOT NULL,
  `update_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `b_title`
--

INSERT INTO `b_title` (`id`, `title_name_th`, `title_name_en`, `status_active`, `gender`, `createby_user`, `create_datetime`, `updatteby_user`, `update_datetime`) VALUES
(1, 'นาย', '', 1, 1, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(2, 'นางสาว', '', 1, 2, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(3, 'นาง', '', 1, 2, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(4, '', 'MR', 1, 1, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(5, '', 'Mrs', 1, 2, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(6, '', 'Miss', 1, 2, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(7, 'ว่าที่ รต.', '', 1, 1, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(8, 'ว่าที่ รต.หญิง', '', 1, 2, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(9, 'เด็กชาย', '', 1, 1, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00'),
(10, 'เด็กหญิง', '', 1, 2, '1509901325106', '2017-02-25 00:00:00', '1509901325106', '2017-02-25 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `b_title`
--
ALTER TABLE `b_title`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `b_title`
--
ALTER TABLE `b_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
