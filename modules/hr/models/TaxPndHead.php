<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "tax_pnd_head".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $paid_month
 * @property integer $for_month
 * @property integer $for_year
 * @property integer $pnd_type
 * @property string $tax_regis_code
 * @property string $branch_no
 * @property string $company_name
 * @property string $building
 * @property string $room_no
 * @property string $floor_no
 * @property string $village_name
 * @property string $house_no
 * @property string $moo
 * @property string $soi
 * @property string $junction
 * @property string $road
 * @property string $tumbon
 * @property string $amphur
 * @property string $province
 * @property integer $post_code
 * @property integer $sent_doc
 * @property integer $sent_doc_no
 * @property integer $file_attach_qty
 * @property integer $media_attach_qty
 * @property integer $sum_employee_gnr
 * @property string $sum_income_gnr
 * @property string $sum_tax_gnr
 * @property integer $sum_employee_auth
 * @property string $sum_income_auth
 * @property string $sum_tax_auth
 * @property integer $sum_employee_onepaid
 * @property string $sum_income_onepaid
 * @property string $sum_tax_onepaid
 * @property integer $sum_employee_inthai
 * @property string $sum_income_inthai
 * @property string $sum_tax_inthai
 * @property integer $sum_employee_notin
 * @property string $sum_income_notin
 * @property string $sum_tax_notin
 * @property integer $total_employee
 * @property string $total_income
 * @property string $total_tax
 * @property string $extra_money
 * @property string $grand_total
 * @property string $signature_name
 * @property string $signature_position
 * @property string $signature_date
 * @property string $createby_user
 * @property string $create_datetime
 */
class TaxPndHead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_pnd_head';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'paid_month', 'for_month', 'for_year', 'pnd_type', 'tax_regis_code', 'branch_no', 'company_name', 'building', 'room_no', 'floor_no', 'village_name', 'house_no', 'moo', 'soi', 'junction', 'road', 'tumbon', 'amphur', 'province', 'post_code', 'sent_doc', 'sent_doc_no', 'file_attach_qty', 'media_attach_qty', 'sum_employee_gnr', 'sum_income_gnr', 'sum_tax_gnr', 'sum_employee_auth', 'sum_income_auth', 'sum_tax_auth', 'sum_employee_onepaid', 'sum_income_onepaid', 'sum_tax_onepaid', 'sum_employee_inthai', 'sum_income_inthai', 'sum_tax_inthai', 'sum_employee_notin', 'sum_income_notin', 'sum_tax_notin', 'total_employee', 'total_income', 'total_tax', 'extra_money', 'grand_total', 'signature_name', 'signature_position', 'signature_date', 'createby_user', 'create_datetime'], 'required'],
            [['company_id', 'for_month', 'for_year', 'pnd_type', 'post_code', 'sent_doc', 'sent_doc_no', 'file_attach_qty', 'media_attach_qty', 'sum_employee_gnr', 'sum_employee_auth', 'sum_employee_onepaid', 'sum_employee_inthai', 'sum_employee_notin', 'total_employee'], 'integer'],
            [['sum_income_gnr', 'sum_tax_gnr', 'sum_income_auth', 'sum_tax_auth', 'sum_income_onepaid', 'sum_tax_onepaid', 'sum_income_inthai', 'sum_tax_inthai', 'sum_income_notin', 'sum_tax_notin', 'total_income', 'total_tax', 'extra_money', 'grand_total'], 'number'],
            [['signature_date', 'create_datetime'], 'safe'],
            [['paid_month'], 'string', 'max' => 10],
            [['tax_regis_code', 'createby_user'], 'string', 'max' => 13],
            [['branch_no'], 'string', 'max' => 5],
            [['company_name'], 'string', 'max' => 500],
            [['building', 'soi', 'junction', 'road', 'tumbon', 'amphur', 'province'], 'string', 'max' => 200],
            [['room_no'], 'string', 'max' => 50],
            [['floor_no'], 'string', 'max' => 3],
            [['village_name'], 'string', 'max' => 100],
            [['house_no'], 'string', 'max' => 20],
            [['moo'], 'string', 'max' => 2],
            [['signature_name', 'signature_position'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'paid_month' => 'Paid Month',
            'for_month' => 'For Month',
            'for_year' => 'For Year',
            'pnd_type' => 'Pnd Type',
            'tax_regis_code' => 'Tax Regis Code',
            'branch_no' => 'Branch No',
            'company_name' => 'Company Name',
            'building' => 'Building',
            'room_no' => 'Room No',
            'floor_no' => 'Floor No',
            'village_name' => 'Village Name',
            'house_no' => 'House No',
            'moo' => 'Moo',
            'soi' => 'Soi',
            'junction' => 'Junction',
            'road' => 'Road',
            'tumbon' => 'Tumbon',
            'amphur' => 'Amphur',
            'province' => 'Province',
            'post_code' => 'Post Code',
            'sent_doc' => 'Sent Doc',
            'sent_doc_no' => 'Sent Doc No',
            'file_attach_qty' => 'File Attach Qty',
            'media_attach_qty' => 'Media Attach Qty',
            'sum_employee_gnr' => 'Sum Employee Gnr',
            'sum_income_gnr' => 'Sum Income Gnr',
            'sum_tax_gnr' => 'Sum Tax Gnr',
            'sum_employee_auth' => 'Sum Employee Auth',
            'sum_income_auth' => 'Sum Income Auth',
            'sum_tax_auth' => 'Sum Tax Auth',
            'sum_employee_onepaid' => 'Sum Employee Onepaid',
            'sum_income_onepaid' => 'Sum Income Onepaid',
            'sum_tax_onepaid' => 'Sum Tax Onepaid',
            'sum_employee_inthai' => 'Sum Employee Inthai',
            'sum_income_inthai' => 'Sum Income Inthai',
            'sum_tax_inthai' => 'Sum Tax Inthai',
            'sum_employee_notin' => 'Sum Employee Notin',
            'sum_income_notin' => 'Sum Income Notin',
            'sum_tax_notin' => 'Sum Tax Notin',
            'total_employee' => 'Total Employee',
            'total_income' => 'Total Income',
            'total_tax' => 'Total Tax',
            'extra_money' => 'Extra Money',
            'grand_total' => 'Grand Total',
            'signature_name' => 'Signature Name',
            'signature_position' => 'Signature Position',
            'signature_date' => 'Signature Date',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
        ];
    }
}
