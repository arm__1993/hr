<?php

namespace app\modules\gitreport\models;

use Yii;

/**
 * This is the model class for table "hbso_project".
 *
 * @property integer $id
 * @property string $project_name
 * @property string $project_start_date
 * @property string $project_end_date
 * @property integer $status_active
 * @property string $create_by
 * @property string $create_datetime
 */
class HbsoProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hbso_project';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbGit_Report');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_start_date', 'project_end_date', 'create_datetime'], 'safe'],
            [['status_active'], 'integer'],
            [['project_name'], 'string', 'max' => 250],
            [['create_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_name' => 'Project Name',
            'project_start_date' => 'Project Start Date',
            'project_end_date' => 'Project End Date',
            'status_active' => 'Status Active',
            'create_by' => 'Create By',
            'create_datetime' => 'Create Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return HbsoProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HbsoProjectQuery(get_called_class());
    }
}
