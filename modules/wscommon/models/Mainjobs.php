<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "MAIN_JOBS".
 *
 * @property integer $JOB_ID_NUM
 * @property integer $JOB_CUS_NO
 * @property integer $JOB_REF_MAIN_ID
 * @property string $JOB_NAME
 * @property string $JOB_DESC
 * @property integer $JOB_MAIN_ACTIVE
 * @property integer $JOB_STATUS
 */
class Mainjobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MAIN_JOBS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_new_cusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JOB_CUS_NO', 'JOB_REF_MAIN_ID', 'JOB_NAME', 'JOB_DESC', 'JOB_MAIN_ACTIVE', 'JOB_STATUS'], 'required'],
            [['JOB_CUS_NO', 'JOB_REF_MAIN_ID', 'JOB_MAIN_ACTIVE', 'JOB_STATUS'], 'integer'],
            [['JOB_NAME'], 'string', 'max' => 200],
            [['JOB_DESC'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JOB_ID_NUM' => 'auto run',
            'JOB_CUS_NO' => 'รหัสลูกค้า',
            'JOB_REF_MAIN_ID' => 'ref MainCusData_JobTable',
            'JOB_NAME' => 'อาชีพ',
            'JOB_DESC' => 'Job  Desc',
            'JOB_MAIN_ACTIVE' => 'job หลัก',
            'JOB_STATUS' => 'Job  Status',
        ];
    }

    public function searchjobcusformobile($keyword) // param
    {   

    $sql = "select * FROM MAIN_JOBS where JOB_CUS_NO = $keyword " ;
        $job = Yii::$app->dbERP_new_cusdata
       ->createCommand($sql)
       ->queryAll();

        return $job ;
    }
}
