-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 28, 2017 at 11:10 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.27-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_promise_type`
--

CREATE TABLE `emp_promise_type` (
  `id` int(11) NOT NULL,
  `permise_name` varchar(100) DEFAULT NULL,
  `promise_type` tinyint(4) DEFAULT NULL COMMENT '1:สัญญาต้างรายวัน 2:สัญญาจ้างรายเดือน 3:สัญญาจ้างพนักงานขาย',
  `promise_detail` text COMMENT 'รายละเอียดสัญญาจ้าง',
  `promise_bond` text COMMENT 'รายละเอียดสัญญาค้ำประกัน',
  `promise_attach` text COMMENT 'รายละเอียดบันทึกแนบ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_promise_type`
--
ALTER TABLE `emp_promise_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_promise_type`
--
ALTER TABLE `emp_promise_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
