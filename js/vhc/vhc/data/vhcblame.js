$(document).ready(function () {
    ///ส่วนจ่าย///
    $("#btnSaveActivityblame").on("click", function () {
        var f = frmValidate('frmAddVhcblame');
        if (!f) {
            // console.log('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy');
            showWarningInputForm();
        } else {
            $('.record_status').val('1');
            // console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            savevhcblame(1);

        }
    });

    $('#btnAddNewVhcblame').on("click", function () {
        initForm('btnSaveActivityblame');

    });

    ///ส่วนหัก///
    $("#btnSaveActivityblameDeduct").on("click", function () {
        var f = frmValidate('frmDeduct');
        if (!f) {
            showWarningInputForm();
        } else {
            $('.record_status').val('2');
            savevhcblame(2);
        }
    });

    $('#btnAddNewVhcblame').on("click", function () {
        initForm('btnSaveActivityblame');
        $('#hide_activityedits').val('');
        $('#blame_name').val('');
        $('#record_statuss').prop('checked', true);
    });
});

function deletevhcblame(id, valselect) { //อันนี้ยกเลิกเน้อออออออออออออออออ
    // alert(id);
    $.ajax({
        url: 'deletevhcblame',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showDeleteSuccess();
                $.pjax.reload({container: "#pjax_tb_vhcblame"});  //Reload GridView

                selecttab(valselect);
            } else {
                showDeleteError();
                console.log(data);

            }
        }
    });
}
function savevhcblame(idselectform) {
    if (idselectform == 1) {
        var datavar = $('#frmAddVhcblame').serialize();
    } else {
        var datavar = $('#frmDeduct').serialize();
    }
    // console.log(datavar);
    $.ajax({
        url: 'savevhcblame',
        data: datavar,
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showSaveSuccess();
                if (idselectform == 1) {
                    initForm('frmAddVhcblame');
                } else {
                    initForm('frmDeduct');
                }

                selecttab(idselectform);
                //$.pjax.reload({container: "#pjax_tb_adddeducttemp"});  //Reload GridView
            } else {
                showSaveError();
            }
        }
    });
}

function editblame(id) {//แก้ไขเน้อออออออออออ
    // console.log(id);
    initForm('frmAddVhcblame');//idฟอมมมมม
    $.ajax({
        url: 'updatevhcblame',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            //  console.log(data);
            //
            if(data.record_status==1){
                $('#record_statuss').prop('checked', true);
            }else {
                $('#record_statuss').prop('checked', false);
            }

            $('#hide_activityedits').val(data.id);
            $('#blame_name').val(data.blame_name);
            $('#modalfrmAddVhcblame').modal();
        }
    });
}

function savevhcblame() {//อันนี้นี้เซฟเน้ออออออออออออออออ
    var datavar = $('#frmAddVhcblame').serialize();

    console.log(datavar);
    $.ajax({
        url: 'savevhcblame',
        data: datavar,
        type: 'POST',
        success: function (data) {
            console.log(data);
            if (parseInt(data) == 1) {
                showSaveSuccess();
                initForm('frmAddVhcblame');
                $.pjax.reload({container: "#pjax_tb_vhcblame"});  //Reload GridView
            }
            else {
                showSaveError();
            }
        }
    });
}

function selecttab(val) {
    //alert("2222" + val);
    // console.log($("#tabselect" + val));
    $(".tabselect").removeClass("active");
    $("#tabselect" + val).addClass("active");
    $("#tab" + val).addClass("active");

    if (val == 1) {
        $.pjax.reload({container: "#pjax_tb_adddeducttemp"}); //Reload GridView
    } else {
        $.pjax.reload({container: "#pjax_tb_vhcblame"}); //Reload GridView
    }
}

