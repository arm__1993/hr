<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use app\api\Common;
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

/*** DEFAULT ROUTING **/
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$img = Yii::$app->request->BaseUrl . '/images/global/';
$CurrActionID = Yii::$app->controller->action->id;


//Home logo Links
$currentController = Yii::$app->controller->id;
$homeLinks = 'index';
if($currentController != 'default') $homeLinks = '../default/index';


$siteURL = Common::siteURL().$basePath.'/'.$moduleID;
define('SITE_URL', $siteURL);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='../../auth/default/logout';
                }
            }
          });
    });
});
JS;


$this->registerCssFile(Yii::$app->request->BaseUrl."/fonts/01thaifontcss.css");
$this->registerJs($script);


?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
        <?php echo Html::csrfMetaTags() ?>
        <title>บริษัท อีซูซุเชียงราย จำกัด</title>
        <?php $this->head() ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->homeUrl;?>/css/hr/hr.css">
    </head>


    <body class="hold-transition skin-green-light layout-top-nav">
    <?php $this->beginBody() ?>
    <!-- Site wrapper -->

    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="navbar-header">
                <a href="<?php echo $homeLinks;?>" class="navbar-brand"><span class="logo_text_white"><b>Easy Service Baymanagement</b></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <?php echo $this->params['customParam']; ?>
                </ul>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo Yii::$app->session->get('photo'); ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo Yii::$app->session->get('fullname').'('.Yii::$app->session->get('nickname').')';?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo Yii::$app->session->get('photo'); ?>" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo Yii::$app->session->get('fullname');?> - <?php echo Yii::$app->session->get('positionname');?>

                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="index.php?r=config-hr/viewstaff&id=<?php echo $_SESSION['OBJ_STAFF']->id;?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" id="alogout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <!--<li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>-->
                    </ul>
                </div>
            </div><!--/.nav-collapse -->
        </nav>
        <!-- <nav class="navbar navbar-static-top">
        <div class="navbar-header">
            <a href="<?php echo $homeLinks;?>" class="navbar-brand"><span class="logo_text_white"><b>EasyHR</b></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>
         Collect the nav links, forms, and other content for toggling
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li >
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown'> ข้อมูลองค์กร <b class='caret'></b> </a>
                    <ul class='dropdown-menu'>
                        <li class='dropdown-submenu'>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown'> การจัดการผังองค์กร <b class='caret'></b> </a>
                            <ul class='dropdown-menu'>
                                <li title="ข้อมูลบริษัท" ><a href='/organize/company?menu_id=2096'/> ข้อมูลบริษัท </a></li>
                                <li title="ข้อมูลแผนก" ><a href='/organize/department?menu_id=2097'/> ข้อมูลแผนก </a></li>
                                <li title="ข้อมูลฝ่าย" ><a href='/organize/division?menu_id=2098'/> ข้อมูลฝ่าย </a></li>
                                <li title="ข้อมูลระดับ" ><a href='/organize/level?menu_id=2099'/> ข้อมูลระดับ </a></li>
                                <li title="ข้อมูลตำแหน่งาน" ><a href='/organize/position?menu_id=2100'/> ข้อมูลตำแหน่งาน </a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav> -->
    </header>




    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="bb-alert alert alert-success" style="display:none;">
            <span>The examples populate this alert with dummy content</span>
        </div>
        <?php echo $content; ?>
    </div>
    <!-- /.content-wrapper -->
    <!-- =============================================== -->


    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.9.8
        </div>
        <strong>Copyright &copy; 2017 บริษัท อีซูซุเชียงราย จำกัด</strong> All rights reserved.
    </footer>


    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->endBody() ?>
    </body>
    </html>


<?php $this->endPage() ?>