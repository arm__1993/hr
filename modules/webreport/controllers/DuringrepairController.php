<?php

namespace app\modules\webreport\controllers;

use app\api\Common;
use app\api\Utility;
use app\modules\webreport\apiwebreport\ApiDuringrepair;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use app\api\DateTime;
// use app\api\Common;
use mPDF;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;

class DuringrepairController extends \yii\web\Controller
{
    public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPb_repair_overtime()
    {
        return $this->render('pb_repair_overtime');
    }

    public function actionStatistics_outsize_day()
    {
        return $this->render('statistics_outsize_day',['query' => false]);
    }

    public function actionStatistics_outsize_month()
    {
        return $this->render('statistics_outsize_month');
    }

    public function actionOvertime()
    {

        $request = Yii::$app->request;
        if($request->isPost) {


            //$postCompany = Yii::$app->request->post();
            //$postValue = Yii::$app->request->post();
            //$type_company = $postValue['company'];  //retrieve company ID
            //$technician_cartype = $postValue['technician_type'];  //retrieve technician type

            //$postVal = Yii::$app->request->post();

            $postData = Yii::$app->request->post();
            $type_company = $postData['company'];
            $technician_type = $postData['technician'];
            $strDate = $postData['reservation'];

            $arrDateRange = DateTime::FormatDateFromCalendarRange($strDate);
            $dateStart = $arrDateRange['start_date'];
            $dateEnd = $arrDateRange['end_date'];

            $year = substr($dateStart,0,4);
            //$_arrRET = Helper::calculateSunday($dateStart, $dateEnd);

            $arrAllEmployee = Common::getAllEmployee();  //พนักงานทั้งหมด

            $selected_company = $type_company;
            $selected_technician = $technician_type;
            $selected_daterange = $strDate;


            if ($technician_type == '1') {  //ช่างรถเล็ก
                $technician_TypeCar = '3,4';
            } elseif ($technician_type == '2') {  //ช่างรถใหญ่
                $technician_TypeCar = '1,2';
            } elseif ($technician_type == '3') {  //ช่างทั้งหมด
                $technician_TypeCar = '1,2,3,4';
            }

            $workOverTime = $arrCvhcMainID = [];  //send param by ref
            $workOverTime = ApiDuringrepair::getOverTime($type_company,$technician_TypeCar,$dateStart,$dateEnd,$arrAllEmployee,$arrCvhcMainID);
            //Utility::printobj($arrCvhcMainID);

            foreach ($workOverTime AS $value){
                $arrCvhcMainID[] = $value['id'];
            }

            //$overTime = null;
            if (empty($workOverTime)) {
                //$message = "ไม่พบข้อมูลที่ค้นหา";
               // echo "<script type='text/javascript'>alert('$message');</script>";
                Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
                return $this->render('pb_repair_overtime',[
                    'workOverTime'=>$workOverTime,
                ]);
            } else {

                $allCvhcmainStatus =  ApiDuringrepair::getTimeStatusCvhcmain($arrCvhcMainID);

                ApiDuringrepair::processStatusRepair($allCvhcmainStatus);

                         ///   หาประเภท   ///
                $typeRepair_Overtime = ApiDuringrepair::getTypeRepairOvertime($allCvhcmainStatus);

//               echo "<pre>";
//               print_r($typeRepair_Overtime);
//               echo "</pre>";
//            exit;

                $arrWageDistance = ApiReport::ARRAY_WAGE_GROUP_CHECK_DISTANCE;
                $arrWageGenaral = ApiReport::ARRAY_WAGE_GROUP_REPAIR_GENERAL;

                $arrCvhcmainDistance=[];
                $arrCvhcmainGenaral=[];
            foreach ($typeRepair_Overtime AS $valueOvertime){
                        $_wage = $valueOvertime['wage'];

                foreach ($arrWageDistance as $key => $arrKeyValueWageDistance)
                {
                        if($valueOvertime['repair_id']== $key  && in_array($_wage, $arrKeyValueWageDistance) ){
                            $arrCvhcmainDistance[$valueOvertime['cvhv_main_id']] = $valueOvertime['cvhv_main_id'];

                        }

                }

                foreach ($arrWageGenaral as $key => $arrKeyValueWageGenaral)
                {
                    if($valueOvertime['repair_id']== $key  && in_array($_wage, $arrKeyValueWageGenaral) ){
                        $arrCvhcmainGenaral[$valueOvertime['cvhv_main_id']]= $valueOvertime['cvhv_main_id'];

                    }

                }
            }

                $arrCvhcmainwageIntersect= array_intersect($arrCvhcmainDistance,$arrCvhcmainGenaral);
                $arrCvhcmainDistanceOnly = array_diff_key($arrCvhcmainDistance,$arrCvhcmainwageIntersect);
                $arrCvhcmainGenaralOnly = array_diff_key($arrCvhcmainGenaral,$arrCvhcmainwageIntersect);

                  $getTimeStatus = ApiDuringrepair::getStatustime($allCvhcmainStatus);
                  $arr_Cvhc_Main_Time_Over = ApiDuringrepair::getTimeOverTimeMore($arrCvhcMainID,$year);


                foreach ($workOverTime AS $key => $value)
                {
                    if (array_key_exists($key,$arrCvhcmainDistanceOnly))
                    {
                        $workOverTime[$key]['typeRepair'][] = 'เช็คระยะ';

                    }
                    if (array_key_exists($key,$arrCvhcmainGenaralOnly))
                    {
                        $workOverTime[$key]['typeRepair'][] = 'ซ่อมทั่วไป';
                    }
                    if (array_key_exists($key,$arrCvhcmainwageIntersect))
                    {
                        $workOverTime[$key]['typeRepair'][] = 'เช็คระยะ+ซ่อมทั่วไป';
                    }

                    if (array_key_exists($key,$getTimeStatus))
                    {
                        $workOverTime[$key]['status'] = $getTimeStatus[$key];
                    }
                    if (array_key_exists($key,$arr_Cvhc_Main_Time_Over))
                    {
                        $workOverTime[$key]['TotalMoreTime'] = $arr_Cvhc_Main_Time_Over[$key];
                    }

                }

                // $arrCvhcmainTimeStatus = [];
                // foreach ($getTimeStatus AS $value)
                // {
                //     $arrCvhcmainTimeStatus[$value['id']] = $value['status'];
                // }
                    $TimeOver = ApiDuringrepair::getTimeOverTimeMore($arrCvhcMainID,$year);
              
            $session = Yii::$app->session;

            $session->set('workOverTime', $workOverTime);
            $session->set('selected_company', $selected_company);
            $session->set('selected_daterange', $selected_daterange);
            $session->set('selected_technician', $selected_technician);



                return $this->render('pb_repair_overtime', [
                    //'overTime' => $overTime,
                    'workOverTime' => $workOverTime,
                    'selected_company'=>$selected_company,
                    'selected_daterange'=>$selected_daterange,
                    'selected_technician'=>$selected_technician,
                ]);
            }
        }

    }


    public function actionTime_month()
    {
        $postValue = Yii::$app->request->post();
        $type_company = $postValue['company'];
        $year = $postValue['year'];
        $technician = $postValue['technician'];


        $TimeStatusYear = ApiDuringrepair::getTimeStatusYear($type_company, $technician, $year);

            $session = Yii::$app->session;

            $session->set('TimeStatusYear', $TimeStatusYear);
            $session->set('selected_company', $type_company);
            $session->set('selected_year', $selected_year);
            $session->set('selected_technician', $selected_technician);

            
        return $this->render('statistics_outsize_month', [
            'TimeStatusYear' => $TimeStatusYear,
            'selected_company'=>$type_company,
            'selected_year'=>$year,
            'selected_technician'=>$technician,
            'query' => true
        ]);
    }

    public function actionTime_daily()
    {
        $postValue = Yii::$app->request->post();
        $GET_Company = $postValue['company'];
        $months = DateTime::FormatDateFromCalendarRange($postValue['reservation']);
        $technician = $postValue['technician'];

        $start_date = $months['start_date'];
        $end_date   = $months['end_date'];

        $TimeStatusDaily = ApiDuringrepair::getTimeStatusDaily($GET_Company,$start_date,$end_date,$technician);
        
        
        $session = Yii::$app->session;
            $session->set('TimeStatusDaily', $TimeStatusDaily);
            $session->set('selected_company', $GET_Company);
            $session->set('selected_months', $postValue['reservation']);
            $session->set('selected_technician', $technician);

        return $this->render('statistics_outsize_day', [
            'TimeStatusDaily' => $TimeStatusDaily,
            'selected_company'=>$GET_Company,
            'selected_months'=>$postValue['reservation'],
            'selected_technician'=>$technician,
            'query' => true
        ]);
    }

    public function actionBtn_reportpb()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportpb'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportoday()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportoday'));
        $mpdf->Output();
        exit;

    }

    public function actionBtn_reportomonth()/*05-10-2559 th mpdf*/
    {
        /*$session = Yii::$app->session;
        $company = $session->get('SESSION_company');
        $year = $session->get('SESSION_year');*/


        //foreach($this->_globalCompay as $company) {
        //echo $company->id;
        //}
        /*$mpdf->AddPage('A4');*/
        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_reportomonth'));
        $mpdf->Output();
        exit;

    }

}
