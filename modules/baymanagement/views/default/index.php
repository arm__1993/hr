<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/armboxjs.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/baymanagement/armbox.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$today = date('d/m/Y');


?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">จัดการแผนที่โรงซ่อม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>


        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <select>
                        <option value="">Highlight</option>
                        <option value="#000000" style="background-color: #FFFFFF !important;">Black</option>
                        <option value="#808080" style="background-color: #ff0000 !important;">Gray</option>
                        <option value="#A9A9A9" style="background-color: DarkGray;">DarkGray</option>
                        <option value="#D3D3D3" style="background-color: LightGrey;">LightGray</option>
                        <option value="#FFFFFF" style="background-color: White;">White</option>
                        <option value="#7FFFD4" style="background-color: Aquamarine;">Aquamarine</option>
                        <option value="#0000FF" style="background-color: Blue; !important;">Blue</option>
                        <option value="#000080" style="background-color: Navy;color: #FFFFFF;">Navy</option>
                        <option value="#800080" style="background-color: Purple;color: #FFFFFF;">Purple</option>
                        <option value="#FF1493" style="background-color: DeepPink;">DeepPink</option>
                        <option value="#EE82EE" style="background-color: Violet;">Violet</option>
                        <option value="#FFC0CB" style="background-color: Pink;">Pink</option>
                        <option value="#006400" style="background-color: DarkGreen;color: #FFFFFF;">DarkGreen</option>
                        <option value="#008000" style="background-color: Green;color: #FFFFFF;">Green</option>
                        <option value="#9ACD32" style="background-color: YellowGreen;">YellowGreen</option>
                        <option value="#FFFF00" style="background-color: Yellow;">Yellow</option>
                        <option value="#FFA500" style="background-color: Orange;">Orange</option>
                        <option value="#FF0000" style="background-color: Red;">Red</option>
                        <option value="#A52A2A" style="background-color: Brown;">Brown</option>
                        <option value="#DEB887" style="background-color: BurlyWood;">BurlyWood</option>
                        <option value="#F5F5DC" style="background-color: Beige;">Beige</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10"></div>
                <div class="col-md-2">



                    <button type="button" class="btn btn-success" data-dismiss="modal" id="btn_saveme"
                            onclick="save_me_plz();">บันทึกตำแหน่งทั้งหมด
                    </button>


                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">เพิ่ม
                    </button>

                    <div class="container">
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">เพิ่มลิฟท์</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>กรุณากรอกข้อมูล</p>


                                        <div class="box-body">
                                            <label>ชื่อลิฟท์ <span>*</span></label>
                                            <input id="name" name="name"
                                                   data-required="true"
                                                   class="form-control model_name" type="text"
                                                   placeholder="กรุณาเพิ่มชื่อลิฟท์">
                                        </div>


                                        <div class="box-body">
                                            <label>ประเภทรถ <span>*</span></label>

                                            <select name="dorpdowe" id="dorpdowe"
                                                    onchange="this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor">
                                                <option style="background-color:#F0CA4D" value="#F0CA4D">
                                                    รถใหญ่
                                                </option>
                                                <option style="background-color:#46B29D" value="#46B29D">
                                                    รถกลาง
                                                </option>
                                                <option style="background-color: #E37B40;" value="#E37B40">
                                                    รถเล็ก
                                                </option>
                                                <option style="background-color: #B8E986;" value="#B8E986">
                                                    สำรอง
                                                </option>

                                            </select>


                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" id="btn1">เพิ่ม</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                                            ยกเลิก
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <div id="wrapper" style="height: 630px; width: 960px; border:3px solid black">
                        <p id="demo"></p>
                        <div id="armaddbox"></div>
                    </div>

                </div>
            </div>


        </div>


    </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->