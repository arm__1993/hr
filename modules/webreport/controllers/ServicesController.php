<?php

namespace app\modules\webreport\controllers;

use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiService;
use mPDF;

class ServicesController extends \yii\web\Controller
{
    public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLoop_services()
    {
        return $this->render('loop_services',['query'=>false,]);
    }

    public function actionCustomer_service()
    {
        $postCompany = Yii::$app->request->post();
         $type_company = $postCompany['company'];



        //$type_company = ApiReport::TypeCompany($GET_Company);

        $postMoths = Yii::$app->request->post();
        $months = $postMoths['reservation'];

        $arrDateRange = DateTime::FormatDateFromCalendarRange($months);
        $dateStart = $arrDateRange['start_date'];
        $dateEnd = $arrDateRange['end_date'];


        $dayEnd = substr($months, 13, 2);
        $MonthEnd = substr($months, 16, -5);
        $yearEnd = substr($months, 19);
        $dayStart = substr($months, 0, -21);
        $MonthStart = substr($months, 3, -18);
        $yearStart = substr($months, 6, -13);
        $str_start = (strtotime("$dayStart-$MonthStart-$yearStart"));
        $str_end = (strtotime("$dayEnd-$MonthEnd-$yearEnd"));

        $postTimeRange = Yii::$app->request->post();
        //echo $months."<br>";

        $timerage = $postTimeRange['timerange'];
        //print_r($timerage)."<br>";
        
        $day_rageEnd = substr($timerage, 13, 2);
        $Month_ragehEnd = substr($timerage, 16, -5);
        $year_rageEnd = substr($timerage, 19);
        $day_rageStart = substr($timerage, 0, -21);
        $Month_rageStart = substr($timerage, 3, -18);
        $year_rageStart = substr($timerage, 6, -13);
        $str_rageStart = (strtotime("$day_rageStart-$Month_rageStart-$year_rageStart"));
        $str_rageEnd = (strtotime("$day_rageEnd-$Month_ragehEnd-$year_rageEnd"));


        $postTechnician = Yii::$app->request->post();
        $technician_car = ($postTechnician['technician']);
        if ($technician_car == 'รถเล็ก') {
            $technician_car = '3,4';
        } elseif ($technician_car == 'รถกลาง') {
            $technician_car = '2';
        } elseif ($technician_car == 'รถใหญ่') {
            $technician_car = '1';
        }


    $personal = ApiService::getPersonal($str_start,$str_end,$type_company,$technician_car);

    $rageTime = ApiService::getRageTime($str_rageStart,$str_rageEnd,$type_company,$technician_car);
       
        if(empty($rageTime)){
            //echo "1";
            $totalVIN = null;
        }else{
            //echo "2";
            $totalVIN = count($rageTime);

        }
        
        if (empty($personal)) {
           // echo "3";
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('loop_services',['query'=>false,]);
        } else {

            foreach ($personal as $value) {
                $history_personal[$value['cus_id']]['Date'] = $value['Date'];
                $history_personal[$value['cus_id']]['mile'] = $value['mile'];
                $history_personal[$value['cus_id']]['model_name'] = $value['model_name'];
                $history_personal[$value['cus_id']]['engin_id'] = $value['engin_id'];
                $history_personal[$value['cus_id']]['register'] = $value['register'];
                $history_personal[$value['cus_id']]['chassis'] = $value['chassis'];

            }
            
        $name = ApiService::getName($dateStart,$dateEnd);

            foreach ($name as $value) {
                $history_name[$value['cus_id']]['name'] = $value['name'];
                $history_name[$value['cus_id']]['address'] = $value['address'];
                $history_name[$value['cus_id']]['tel'] = $value['tel'];
            }

            

            foreach ($history_personal as $index => $value) {
                if (array_key_exists($index, $history_name)) {
                    $customers[$index]['name'] = $history_name[$index]['name'];
                    $customers[$index]['address'] = $history_name[$index]['address'];
                    $customers[$index]['tel'] = $history_name[$index]['tel'];
                    if (array_key_exists($index, $history_personal)) {
                        $customers[$index]['date'] = date("Y-m-d", $history_personal[$index]['Date']);
                        $customers[$index]['mile'] = $history_personal[$index]['mile'];
                        $customers[$index]['model_name'] = $history_personal[$index]['model_name'];
                        $customers[$index]['engin_id'] = $history_personal[$index]['engin_id'];
                        $customers[$index]['register'] = $history_personal[$index]['register'];
                        $customers[$index]['chassis'] = $history_personal[$index]['chassis'];
                    }
                }
            }

            // echo "<pre>";
            // print_r($customers);
            // echo "</pre>";
            // EXIT;
            $session = Yii::$app->session;

            $session->set('customers', $customers);
            $session->set('totalVIN', $totalVIN);
            $session->set('year_rageEnd', $year_rageEnd);
            $session->set('year_rageStart', $year_rageStart);
            $session->set('technician_car', $technician_car);

        $selected_company = $type_company;
        $selected_technician = $postTechnician['technician'];
        $selected_months = $months;
        $selected_timerage = $timerage ;


        // echo "$selected_company"."<br>";
        // echo "$selected_technician"."<br>";

        // exit();

            return $this->render('loop_services',
                ['customers' => $customers,
                'totalVIN' => $totalVIN,
                'year_rageEnd' => $year_rageEnd,
                'year_rageStart' => $year_rageStart,
                'technician_car' => $technician_car,
                'query' => true,
                'selected_company'=>$selected_company,
                'selected_technician'=>$selected_technician,
                'selected_months'=>$selected_months,
                'selected_timerage'=>$selected_timerage,
                ]);  

        }


    }

    public function actionExportloop()
    {
        return $this->render('_exportloop');
    }

}
