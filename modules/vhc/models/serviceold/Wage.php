<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "wage".
 *
 * @property integer $id
 * @property string $wage_id
 * @property string $name
 * @property string $crepair_id
 * @property string $crepair_list_id
 * @property string $crepair
 * @property string $crepair_list
 * @property string $emp_id
 * @property string $add_time
 * @property string $status
 * @property string $sdel
 */
class Wage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wage';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wage_id', 'name', 'crepair_id', 'crepair_list_id', 'crepair', 'crepair_list', 'emp_id', 'add_time', 'status', 'sdel'], 'required'],
            [['crepair', 'crepair_list'], 'string'],
            [['wage_id', 'add_time'], 'string', 'max' => 20],
            [['name', 'crepair_id', 'crepair_list_id'], 'string', 'max' => 250],
            [['emp_id'], 'string', 'max' => 13],
            [['status', 'sdel'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wage_id' => 'Wage ID',
            'name' => 'Name',
            'crepair_id' => 'Crepair ID',
            'crepair_list_id' => 'Crepair List ID',
            'crepair' => 'Crepair',
            'crepair_list' => 'Crepair List',
            'emp_id' => 'Emp ID',
            'add_time' => 'Add Time',
            'status' => 'Status',
            'sdel' => 'Sdel',
        ];
    }
}
