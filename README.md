EASY ERP WEB Application and Web Service
========================================

โปรเจ็คมีแกนกลางของระบบพัฒนาด้วย Yii 2  Framework สามารถดูรายละเอียดเพิ่มเติมได้จากเว็บไซต์ [Yii 2](http://www.yiiframework.com/) ซึ่งทำให้นักพัฒนาระบบสามารถพัฒนาระบบงานได้ด้วยความรวดเร็ว มีประสิทธิภาพและมีคุณภาพ

ในโปรเจ็คนี้นักพัฒนาระบบสามารถเพิ่มเติมโมดูลได้ เพื่อให้ระบบสามารถทำงานได้ตามที่นักพัฒนาระบบต้องการ  การแยกโปรแกรมย่อยๆ ออกเป็นโมดูลมีข้อดีคือสามารถให้นักพัฒนาแต่ละคนสามารถพัฒนาระบบงานของตนเองรับผิดชอบ และ share resource ร่วมกันได้

ดังจะเห็นว่าโปรเจ็คนี้มีโมดูลกลางสำหรับไว้ share resource  ระหว่างกัน ซึ่ง จะมีส่วนที่เรียกว่า web service / web application   ดังนั้นทุกโมดูลๆ ในนี้สามารถเรียกใช้งาน   resource  ร่วมกันได้



MODULE STRUCTURE
-------------------

      wssale/             เป็น module web service สำหรับงาน easy sale mobile 0.97
      wsservice/          เป็น module web service สำหรับงาน easy service 
      wscommon/           เป็น module web service สำหรับข้อมูลกลางสำหรับ share resource
      webreport/          เป็น module web application สำหรับแสดงข้อมูลรายงานเฟส1
      hr/                 เป็น module web application สำหรับระบบ Payroll,  Personal และ OU

      




REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0.





CONFIGURATION
-------------
## HR DBConnection api\DBConnection.php

```php

    class DBConnect
    {
    
        public static function getDBConn()
        {
            return [
                'ou' => Yii::$app->params['dbConn']['ERP_easyhr_OU'],
                'ct' => Yii::$app->params['dbConn']['ERP_easyhr_checktime'],
                'pl' => Yii::$app->params['dbConn']['ERP_easyhr_PAYROLL'],
                'pl' => Yii::$app->params['dbConn']['dbERP_easyhr_TIME_ATTENDANCE'],
            ];
        }
    }
    
    ---------------------------------------------------
    $db['ou'] = DBConnect::getDBConn()['ou'];
    $db['ct'] = DBConnect::getDBConn()['ct'];
    $db['pl'] = DBConnect::getDBConn()['pl'];
    
    ---------------------------------------------------
    
    //Example  join 2 table, ERP_easyhr_OU join with ERP_easyhr_PAYROLL
    
        public static function getSeachDataPersonalByIdcard($id_card)
        {
            /* To connection like this */
            $db['ou'] = DBConnect::getDBConn()['ou'];
            $db['ct'] = DBConnect::getDBConn()['ct'];
    
           $sql = "SELECT $db[ct].emp_data.*,
                            $db[ou].department.name AS NameDepartment,
                            $db[ou].section.name AS NameSection,
                            $db[ou].working_company.name AS NameCompany 
                   FROM $db[ct].emp_data
                   INNER JOIN $db[pl].EMP_SALARY ON $db[ct].emp_data.ID_Card = $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD
                   INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY 
                   INNER JOIN $db[ou].department ON $db[ou].department.id = $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT
                   INNER JOIN $db[ou].section ON  $db[ou].section.id = $db[pl].EMP_SALARY.EMP_SALARY_SECTION
                   WHERE $db[ct].emp_data.ID_Card = '$id_card' ";
       
    
            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $data = $connection->createCommand($sql)->queryOne();
            return $data;
        } 
    
   
    
```

MDF version 7.0 / PHP 7
-------------


1. Change permission 777 /vendor/mpdf/mpdf/tmp
2. Copy FONT .TTF font Th sarabun SET  to FOLDER  /vendor/mpdf/mpdf/ttffonts
3. Copy Font DATA, Copy from version 5 to FOLDER  /vendor/mpdf/mpdf/tmp/ttfontdata
4. Config Font Open File /vendor/mpdf/mpdf/src/Config/FontVariables.php
and add new font alias name thsarabun

```php

			'fontdata' => [
				"dejavusanscondensed" => [
					'R' => "DejaVuSansCondensed.ttf",
					'B' => "DejaVuSansCondensed-Bold.ttf",
					'I' => "DejaVuSansCondensed-Oblique.ttf",
					'BI' => "DejaVuSansCondensed-BoldOblique.ttf",
					'useOTL' => 0xFF,
					'useKashida' => 75,
				],
                "thsarabun" => array(
                    'R' => "THSarabun.ttf",
                    'B' => "THSarabunBold.ttf",
                    'I' => "THSarabunItalic.ttf",
                    'BI' => "THSarabunBoldItalic.ttf",
                ),
                
                .....
                ......

```

5. Use font in views / add style sheet 
```php
<style>
    .container {
        font-family: "THSarabun";
        font-size: 16px;
    }

    p {
        font-family: "THSarabun";
        font-size: 16px;
    }
</style>

```

6. Declare MPDF in version 7

```php
    public function actionMpdf()
    {

        //$mpdf = new mPDF('th', 'A4-P', '', 'thsarabun');  <=== here is version 5
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->charset_in='utf-8';
        $mpdf->autoLangToFont = true;
        $mpdf->WriteHTML($this->renderPartial('_report'));
        $mpdf->Output();
    }
```



### Database

Edit the file `config/web.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.
# ERP_HR
# ERP_HR
