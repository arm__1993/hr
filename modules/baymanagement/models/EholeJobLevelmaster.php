<?php

namespace app\modules\baymanagement\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/**
 * This is the model class for table "ehole_job_levelmaster".
 *
 * @property integer $id
 * @property string $job_level_name
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class EholeJobLevelmaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ehole_job_levelmaster';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_level_name', 'status_active', 'createby_user', 'create_datetime'], 'required'],
            [['status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['job_level_name'], 'string', 'max' => 200],
            [['createby_user', 'updateby_user'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'job_level_name' => 'Job Level Name',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }
    public function search($params)
    {
        $data = EholeJobLevelmaster::find() ->where('status_active <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','job_level_name',$this->job_level_name]); // ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
