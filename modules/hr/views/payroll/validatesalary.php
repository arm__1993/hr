<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:32
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.config.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/validatesalary.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);


?>
<style type="text/css">
    .modal-wide .modal-dialog {
        width: 90%; /* or whatever you wish */
    }

    .text_slip_title {
        font-size: 14px;
        font-weight: bold;
        text-decoration: underline;
    }

    .text_slip_titleb {
        font-size: 16px;
        font-weight: bold;
        text-decoration: underline;
    }

    .text_money_sum {
        font-size: 18px;
        font-weight: bold;
    }
</style>
<span id="mode" title="validate"></span>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li class="active">ตรวจสอบข้อมูลเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <form>
                <div class="row">
                    <div class="col-md-1">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp" class="col-sm-4 control-label">บริษัท</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="selectworking" id="selectworking"
                                        onchange="getCompanyForDepartment(this);">
                                    <option value="">เลือกบริษัท</option>
                                    <?php $working = ApiHr::getWorking_company();
                                    foreach ($working as $value) {
                                        echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                    } ?>
                                </select>
                                <span id="urlGetDepartment"
                                      title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp" class="col-sm-3 control-label">แผนก</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="selectdepartment" id="selectdepartment"
                                        onchange="getDepartmentForSection(this);">
                                    <option value=""> เลือกแผนก</option>
                                </select>
                                <span id="urlGetSection"
                                      title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp" class="col-sm-3 control-label">ฝ่าย</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="selectsection" id="selectsection">
                                    <option value=""> เลือกฝ่าย</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    &nbsp;
                </div>
                <div class="row">
                    <div class="col-md-1">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="namefullemp" class="col-sm-4 control-label">ชื่อ</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" name="id_emp" id="id_emp">
                                    <?php $empdata = ApiHr::getempdataall();
                                    foreach ($empdata as $value) {
                                        ?>
                                        <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn  btn-primary btn-sm" id="seachdatalist"><i
                                        class="fa fa-search"></i> ค้นหา
                            </button>
                            <button type="reset" class="btn  btn-danger btn-sm"><i class="fa fa-reply"></i> ล้างข้อมูล
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="box box-info" style="display:none" id="showsearch">
                <div class="box-header with-border">
                    <h3 class="box-title">แสดงรายการ</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th width="3%">
                                <center>ลำดับ</center>
                            </th>
                            <th width="7%">
                                <center>สถานะยืนยัน</center>
                            </th>
                            <th width="12%">
                                <center>ชื่อ-สกุล</center>
                            </th>
                            <th width="8%">
                                <center>ตำแหน่ง</center>
                            </th>
                            <th width="12%">
                                <center>แผนก</center>
                            </th>
                            <th width="12%">
                                <center>ฝ่าย</center>
                            </th>
                            <th width="21%">
                                <center>บริษัท</center>
                            </th>
                            <th width="10%">
                                <center>ผู้ยืนยัน</center>
                            </th>
                            <th width="10%">
                                <center>วันที่ยืนยัน</center>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="tblticket">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->

<div class="modal fade modal-wide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form method="post" id="validateform">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="myModalLabel" class="modal-title">ข้อมูลพนักงานที่ตรวจสอบ</h4>
            </div>
            <div class="modal-body" width="100%" class="table table-bordered table-hover dataTable">
                <div class="row">
                    <input type="hidden" id="id_wage_confirm" value="">
                    <div class="col-md-12 text-center">
                        <span class="text_slip_titleb"><lable id="nameworking"></lable></span>
                    </div>
                </div>
                <div class="row">
                    &nbsp;
                </div>
                <div class="row" style="background-color: #F9F9F9; line-height: 40px;">
                    <div class="col-md-2">
                        <span class="text_slip_title"><lable id="nameemp"></lable></span>
                    </div>
                    <div class="col-md-2">
                        <span class="text_slip_title"><lable id="positionemp"></lable></span>
                    </div>
                    <div class="col-md-2">
                        <span class="text_slip_title"><lable id="diviemp"></lable></span>
                    </div>
                    <div class="col-md-2">
                        <span class="text_slip_title"><lable id="wageemp"></lable></span>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="getsalary"  class="col-sm-4 control-label text_slip_title">ได้รับเงินเดือน : </label>
                            <div class="col-sm-8">
                                <input type="text" id="wage_salary" name="wage_salary"
                                       class="form-control numberformat"
                                       data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true"
                                       data-mask>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    &nbsp;
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <label class="text-center"><strong>รายการเพิ่ม</strong></label>
                        <table width="80%" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th style="width:5%">ลำดับ</th>
                                <th style="width:30%">รายการ</th>
                                <th style="width:40%">รายละเอียด</th>
                                <th style="width:25%">จำนวน</th>
                            </tr>
                            </thead>
                            <tbody id="tbadddeduct">
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3" class="text-right"><span><strong>รวมรายการเพิ่ม</strong></span></td>
                                <td class="text-right"><label id="sumTotalDeduc"></label></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <label class="text-center"><strong>รายการหัก</strong></label>
                        <table width="80%" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                            <th style="width:5%">ลำดับ</th>
                            <th style="width:30%">รายการ</th>
                            <th style="width:40%">รายละเอียด</th>
                            <th style="width:25%">จำนวน</th>
                            </tr>
                            </thead>
                            <tbody id="tbdededuct">

                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3" class="text-right"><span><strong>รวมรายการหัก</strong></span></td>
                                <td class="text-right"><label id="sumTotaldeDeduc"></label></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>หมายเหตุ</label>
                            <textarea class="form-control" name="wage_remark" id="wage_remark" rows="3" style="width: 80%"></textarea>
                        </div>
                        <div class="form-group">
                            <label>เงินสะสมที่บริษัทจ่ายให้</label>
                            <textarea id="bnf_sum" class="form-control text_money_sum text-center" rows="3" style="width: 80%;" readonly></textarea>
                            <!--เงินสะสมประจำเดือน กันยายน  300 บาท ยอดรวมทั้งหมด  300 บาท-->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>รายได้สะสม มกราคม-ปัจุบัน</label>
                                <input type="text" id="wage_summary" class="form-control text_money_sum text-center" value="0.00" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>ภาษีสะสม มกราคม-ปัจุบัน</label>
                                <input type="text" id="tax_summary" class="form-control text_money_sum text-center" value="0.00" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-sm-10">
                            <div class="form-group">
                                <label>ประกันสังคมสะสม มกราคม-ปัจุบัน</label>
                                <input type="text" id="sso_summary" class="form-control text_money_sum text-center" value="0.00" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>รายรับทั้งสิ้น</label>
                            <textarea class="form-control text_money_sum" rows="2" style="width: 80%;text-align: center" readonly
                                      id="sumallnet">
                                </textarea>
                        </div>
                        <input type="hidden" name="force_sso" id="force_sso" value="<?php echo $sso;?>">
                        <input type="hidden" name="force_wht" id="force_wht" value="<?php echo $wht;?>">
                        <input type="hidden" name="force_pnd" id="force_pnd" value="<?php echo $pnd;?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-9">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label id="text_confirm_by"></label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label id="text_confirm_date"></label>
                            </div>
                        </div>
                        <div class="col-sm-4">

                            <button type="button" class="btn btn-block btn-success" id="btn_validated"><i
                                        class="fa fa-check-circle"></i> ยืนยันการตรวจสอบเรียบร้อยแล้ว
                            </button>
                            <button type="button" class="btn btn-block btn-success" id="submit_confirm"><i
                                        class="fa fa-check-circle"></i> ยืนยันการตรวจสอบรายการ
                            </button>

                        </div>
                    </div>
                </div>
                <div class="row"><div class="col-md-12">&nbsp;</div></div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <input type="hidden" id="am_idcard" name="am_idcard">
                        <button type="button" class="btn btn-lg btn-warning" id="btn_previous"><i
                                    class="fa fa-arrow-circle-left"></i> ก่อนหน้า
                        </button>
                        <button type="button" class="btn btn-lg btn-warning" id="btn_next"><i
                                    class="fa fa-arrow-circle-right"></i> ถัดไป
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->