-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 03, 2017 at 09:15 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_requestdetail_return`
--

CREATE TABLE `ot_requestdetail_return` (
  `id` int(11) NOT NULL COMMENT 'รหัส',
  `id_card` char(13) NOT NULL COMMENT 'รหัสบัตรประชาชนพนักงาน',
  `ot_requestdetail_id` int(11) NOT NULL COMMENT 'รหัสรายละเอียดการขออนุมัติโอที',
  `return_id` tinyint(3) NOT NULL COMMENT 'รหัสลำดับการขอค่าตอบแทน',
  `return_name` varchar(100) NOT NULL COMMENT 'ชื่อค่าตอบแทน',
  `return_total` decimal(10,2) NOT NULL COMMENT 'จำนวนค่าตอบแทนได้รับ',
  `return_datetime` datetime NOT NULL,
  `is_used` tinyint(3) NOT NULL COMMENT 'สถานะการใช้ 1=ใช้, 0=ยังไม่ใช้',
  `used_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ใช้ค่าตอบแทน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_requestdetail_return`
--
ALTER TABLE `ot_requestdetail_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_card` (`id_card`),
  ADD KEY `ot_requestdetail_id` (`ot_requestdetail_id`),
  ADD KEY `return_id` (`return_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ot_requestdetail_return`
--
ALTER TABLE `ot_requestdetail_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
