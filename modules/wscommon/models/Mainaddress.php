<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "MAIN_ADDRESS".
 *
 * @property integer $ADDR_ID_NUM
 * @property integer $ADDR_CUS_NO
 * @property string $ADDR_VILLAGE
 * @property string $ADDR_ROOM_NO
 * @property string $ADDR_FLOOR_NO
 * @property string $ADDR_NUMBER
 * @property string $ADDR_GROUP_NO
 * @property string $ADDR_LANE
 * @property string $ADDR_ROAD
 * @property string $ADDR_SUB_DISTRICT
 * @property string $ADDR_DISTRICT
 * @property string $ADDR_PROVINCE
 * @property string $ADDR_POSTCODE
 * @property string $ADDR_GEOGRAPHY
 * @property string $ADDR_GPS_LATITUDE
 * @property string $ADDR_GPS_LONGTITUDE
 * @property string $ADDR_MAP_DESC
 * @property string $ADDR_REMARK
 * @property integer $ADDR_TYPE
 * @property integer $ADDR_MAIN_ACTIVE
 * @property string $ADDR_NAMEWORK
 * @property string $ADDR_NAMEPOSITION
 * @property integer $ADDR_DOCUMENT
 */
class Mainaddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MAIN_ADDRESS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_new_cusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ADDR_CUS_NO', 'ADDR_VILLAGE', 'ADDR_ROOM_NO', 'ADDR_FLOOR_NO', 'ADDR_NUMBER', 'ADDR_GROUP_NO', 'ADDR_LANE', 'ADDR_ROAD', 'ADDR_SUB_DISTRICT', 'ADDR_DISTRICT', 'ADDR_PROVINCE', 'ADDR_POSTCODE', 'ADDR_GEOGRAPHY', 'ADDR_GPS_LATITUDE', 'ADDR_GPS_LONGTITUDE', 'ADDR_MAP_DESC', 'ADDR_REMARK', 'ADDR_TYPE', 'ADDR_MAIN_ACTIVE', 'ADDR_NAMEWORK', 'ADDR_NAMEPOSITION', 'ADDR_DOCUMENT'], 'required'],
            [['ADDR_CUS_NO', 'ADDR_TYPE', 'ADDR_MAIN_ACTIVE', 'ADDR_DOCUMENT'], 'integer'],
            [['ADDR_MAP_DESC', 'ADDR_REMARK'], 'string'],
            [['ADDR_VILLAGE', 'ADDR_SUB_DISTRICT', 'ADDR_DISTRICT', 'ADDR_PROVINCE', 'ADDR_POSTCODE'], 'string', 'max' => 100],
            [['ADDR_ROOM_NO', 'ADDR_FLOOR_NO', 'ADDR_NUMBER', 'ADDR_GROUP_NO', 'ADDR_LANE', 'ADDR_NAMEWORK', 'ADDR_NAMEPOSITION'], 'string', 'max' => 50],
            [['ADDR_ROAD'], 'string', 'max' => 150],
            [['ADDR_GEOGRAPHY'], 'string', 'max' => 10],
            [['ADDR_GPS_LATITUDE', 'ADDR_GPS_LONGTITUDE'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ADDR_ID_NUM' => 'auto run',
            'ADDR_CUS_NO' => 'หมายเลขลูกค้า',
            'ADDR_VILLAGE' => 'ชื่ออาคาร/หมู่บ้าน',
            'ADDR_ROOM_NO' => 'ห้องเลขที่',
            'ADDR_FLOOR_NO' => 'ชั้นที่',
            'ADDR_NUMBER' => 'เลขที่',
            'ADDR_GROUP_NO' => 'หมู่ที่ 7',
            'ADDR_LANE' => 'ตรอก/ซอย',
            'ADDR_ROAD' => 'ถนน',
            'ADDR_SUB_DISTRICT' => 'ตำบล/แขวง',
            'ADDR_DISTRICT' => 'อำเภอ/เขต',
            'ADDR_PROVINCE' => 'จังหวัด',
            'ADDR_POSTCODE' => 'รหัสไปรษณีย์',
            'ADDR_GEOGRAPHY' => 'ภาค ',
            'ADDR_GPS_LATITUDE' => 'GPS ละติจูด ',
            'ADDR_GPS_LONGTITUDE' => 'GPS ลองติจูด ',
            'ADDR_MAP_DESC' => 'คำอธิบายแผนที่ ',
            'ADDR_REMARK' => 'หมายเหตุ',
            'ADDR_TYPE' => '1 บัตรปชช,2 เอกสาร ,3 ทำงาน ,4 อื่นๆ',
            'ADDR_MAIN_ACTIVE' => 'ต้องระบุที่อยู่หลัก',
            'ADDR_NAMEWORK' => 'ชื่อที่ทำงาน',
            'ADDR_NAMEPOSITION' => 'ชื่อตำแหน่งที่ทำงาน',
            'ADDR_DOCUMENT' => '1 :รับเอกสาร 2: ไม่รับเอกสาร ',
        ];
    }

    public function searchgeocusformobile($cusno){
        $sql = "select * from MAIN_ADDRESS where ADDR_CUS_NO = :cusno  ORDER BY `ADDR_ID_NUM` DESC";
        //echo ;
        $showdataaddresscus = Yii::$app->dbERP_new_cusdata
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
      // ->bindParam(':typeadd',$typeadd)
       ->queryAll();

        
        return $showdataaddresscus;
        
    }

}
