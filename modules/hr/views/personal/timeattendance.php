<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li>เพิ่มข้อมูลพนักงาน</li>
                <li class="active">ลงเวลาเข้า - ออกงาน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="row">
                 <div class="col-md-6">
                    <form class="form-horizontal">
                        <div class="box box-info">
                                <div class="box-header with-border">
                                 <h3 class="box-title">ข้อมูลการทำงาน</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="imageEmp" class="col-sm-3 control-label">วันหยุดประจำสัปดาห์</label>
                                        <div class="col-sm-6">
                                            <select class="form-control">
                                                <option>option 1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="codeEmp" class="col-sm-3 control-label">วันที่เริ่มงาน</label>
                                        <div class="col-sm-6">
                                        <input type="date" class="form-control" id="codeEmp" name="codeEmp">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="codeEmpMain" class="col-sm-3 control-label">วันที่ผ่านทดลองงาน</label>
                                        <div class="col-sm-6">
                                        <input type="date" class="form-control" id="codeEmpMain" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="codeEmpMain" class="col-sm-3 control-label">ประเภทการจ้างงาน</label>
                                        <div class="col-sm-6">
                                            <select class="form-control">
                                                <option>option 1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="codeEmpMain" class="col-sm-3 control-label">สถานะการทำงาน</label>
                                        <div class="col-sm-6">
                                            <select class="form-control">
                                                <option>option 1</option>
                                            </select>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </form>
                </div>
                 <div class="col-md-6">
                    <form class="form-horizontal">
                            <div class="box box-info">
                                    <div class="box-header with-border">
                                    <h3 class="box-title">ข้อมูลการลาออก</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="imageEmp" class="col-sm-3 control-label">วันที่ทำงานวันสุดท้าย</label>
                                            <div class="col-sm-6">
                                                <input type="date" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="codeEmp" class="col-sm-3 control-label">เหตุผลที่ลาออก</label>
                                            <div class="col-sm-6"> 
                                                <select class="form-control">
                                                    <option>option 1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="codeEmpMain" class="col-sm-3 control-label">อธิบายเหตุผลที่ลาออก</label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" rows="3" placeholder=""></textarea>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </form>
                 </div>
            </div>
            <div class="row>">
                <form class="form-horizontal">
                        <div class="box box-info">
                                <div class="box-header with-border">
                                 <h3 class="box-title">ข้อมูลเวลาเข้า-ออกงาน</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="imageEmp" class="col-sm-2 control-label">วันหยุดประจำสัปดาห์</label>
                                        <div class="col-sm-2"> 
                                            <select class="form-control">
                                                <option>option 1</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <select class="form-control">
                                                <option>option 1</option>
                                            </select>
                                        </div>
                                         <label for="imageEmp" class="col-sm-1 control-label">เวลาเลิกงาน</label>
                                         <div class="col-sm-2">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label for="imageEmp" class="col-sm-2 control-label">เวลาพักเที่ยง</label>
                                        <div class="col-sm-2">
                                            <select class="form-control">
                                                <option>option 1</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <select class="form-control">
                                                <option>option 1</option>
                                            </select>
                                        </div>
                                         <label for="imageEmp" class="col-sm-1 control-label">ถึงเวลา</label>
                                         <div class="col-sm-2">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label for="imageEmp" class="col-sm-2 control-label">การสแกนบัตร</label>
                                        <div class="col-sm-2">
                                           <input type="radio" name="genderEmp" id="genderEmp"  >
                                                  ใช้
                                        </div>
                                        <div class="col-sm-2">
                                           <input type="radio" name="genderEmp" id="genderEmp"  >
                                                  ไม่ใช้
                                        </div>
                                    </div>
                                   
                                </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->