<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


?>

<div class="box">  
    <table border="1"  width="100%" style="border-collapse: collapse; border: 1px solid black;">
            <tr>
                <td  width="100%"> 
                      <table align="center" width="100%" style=" margin-top: 10px; margin-bottom: 5px; margin-right: 5px;margin-left: 5px;">
                                <tr>
                                    <td align="center" width="75%"> 
                                        <font style="font-size: 16px;">หนังสือรับรองการหักภาษี ณ ที่จ่าย</font>
                                    </td>
                                    <td align="rigth" width="25%">
                                        <font style="font-size: 12px;">เล่มที่</font>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="center" width="75%"> 
                                        <font style="font-size: 12px;">ตามมาตรา 50 ทวิแห่งประมวลรัษฎากร</font>
                                    </td>
                                    <td align="rigth" width="25%">
                                        <font style="font-size: 12px;">เลขที่</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"> 
                                       <table border="1" width="100%" style="border-collapse: collapse; border: 1px solid black; ">
                                            <tr>
                                                <td colspan="4" style="padding-left:10px; padding-top:5px;">
                                                    <div style="font-size:14px;">ผู้ถูกหักภาษี ณ ที่จ่าย</div>
                                                </td>
                                            </tr>
                                       </table>
                                    </td>
                                </tr>
                        </table>
                </td>
            </tr>
    </table>
</div>

