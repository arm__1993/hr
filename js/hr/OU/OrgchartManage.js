/**
 * Created by adithep on 10/7/2017 AD.
 */
//

var testData = [];
$.ajaxSetup({
    async: false
});

$(document).ready(function() {

    $('.select2').css({ "width": "100%" });
    $(".select2").select2();
    $('.select2').select2().val('').trigger("change");

});


$('#btnExport').on("click", function () {
    var idcompane = $('#company_id').val();
    searching(idcompane);
});

function searching(idcompane) {
    nodeDataArray = [];
    var id = idcompane;

    var data = $.get('getchartnode', {company: id}, function (res) {
        $.each(res, function (index, value) {
            obj = {};
            if (value.parent == 0) {
                obj.id = value.ref_id;
                obj.name = value.name;
                obj.company = value.Company;
                obj.title = value.name;
                obj.parent = '0';
                obj.Emp_id = value.Emp_id;
                obj.idPosition = value.Posision_id;
                obj.active_status = value.active_status;
                obj.img = value.img;
            } else {
                obj.id = value.ref_id;
                obj.name = value.name;
                obj.company = value.Company;
                obj.title = value.name;
                obj.parent = value.parent;
                obj.Emp_id = value.Emp_id;
                obj.idPosition = value.Posision_id;
                obj.active_status = value.active_status;
                obj.img = value.img;
            }
            testData[index] = obj;
        });
    });
    var jstring = JSON.stringify(testData);



    org_chart = $('#orgChart').orgChart({
        data: testData,
        showControls: true,
        allowEdit: true,
        async: true,
        onAddNode: function (node, newnode = null) {
            console.log(newnode);
            if (newnode == 'add-node') {
                // $('#myModal').modal('show');
                org_chart.newNode(node.data);
            } else {
                org_chart.newNode(node.data.id);
            }
        },
        onDeleteNode: function (node) {
            //log('Deleted node '+node.data.id);
            org_chart.deleteNode(node.data.id);
        },
        onClickNode: function (node) {
            $('#myModal').modal('show');
        },

    });


}
// just for example purpose
function log(text) {
    $('#consoleOutput').append('<p>' + text + '</p>')
}
