<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li>เพิ่มข้อมูลพนักงาน</li>
                <li class="active">เปลี่ยนแปลงเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <form class="form-horizontal">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ประวัติการเปลี่ยนแปลงเงินเดือน</h3>
                    </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover dataTable">
                                <tbody>
                                    <tr>
                                        <th rowspan = "2"><center>วันที่เปลี่ยนแปลงเงินเดือน</center></th>
                                        <th rowspan = "2"><center>วันที่เงินเดือนมีผล</center></th>
                                        <th rowspan = "2"><center>ลักษณะการเปลี่ยนแปลง</center></th>
                                        <th colspan ="8"><center>เงินเดือน</center></th>
                                        <th colspan ="7"><center>รายได้เพิ่มเติม</center></th>
                                    </tr>
                                    <tr>

                                        <th>ผังเงินเดือนเดิม</th>
                                        <th>กระบอกเดิม</th>
                                        <th>ขั้นเดิม</th>
                                        <th>เงินเดือนเดิม</th>
                                        <th>ผังเงินเดือนใหม่</th>
                                        <th>กระบอกใหม่</th>
                                        <th>ขั้นที่เพิ่ม</th>
                                        <th>เงินเดือนใหม่</th>
                                        <th>รายได้เพิ่มเติมเดิม</th>
                                        <th>F-Skill</th>
                                        <th>M-Skill</th>
                                        <th>ค่าประจำตำแหน่ง</th>
                                        <th>ค่าโทรศัพท์</th>
                                        <th>ค่าช่วยที่พัก</th>
                                        <th>การจัดเบี้ยยังชีพการ</th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->