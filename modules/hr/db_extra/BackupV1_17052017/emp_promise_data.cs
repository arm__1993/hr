using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace ERP_easyhr
{
    #region Emp_promise
    public class Emp_promise
    {
        #region Member Variables
        protected int _id;
        protected int _emp_promise_type_id;
        protected int _emp_data_id;
        protected string _emp_idcard;
        protected string _promise_code;
        protected string _promise_name;
        protected string _promise_detail;
        protected string _promise_bond;
        protected string _promise_attach;
        protected bool _promise_sign_status;
        protected DateTime _promise_sign_date;
        protected unknown _create_date;
        #endregion
        #region Constructors
        public Emp_promise() { }
        public Emp_promise(int emp_promise_type_id, int emp_data_id, string emp_idcard, string promise_code, string promise_name, string promise_detail, string promise_bond, string promise_attach, bool promise_sign_status, DateTime promise_sign_date, unknown create_date)
        {
            this._emp_promise_type_id=emp_promise_type_id;
            this._emp_data_id=emp_data_id;
            this._emp_idcard=emp_idcard;
            this._promise_code=promise_code;
            this._promise_name=promise_name;
            this._promise_detail=promise_detail;
            this._promise_bond=promise_bond;
            this._promise_attach=promise_attach;
            this._promise_sign_status=promise_sign_status;
            this._promise_sign_date=promise_sign_date;
            this._create_date=create_date;
        }
        #endregion
        #region Public Properties
        public virtual int Id
        {
            get {return _id;}
            set {_id=value;}
        }
        public virtual int Emp_promise_type_id
        {
            get {return _emp_promise_type_id;}
            set {_emp_promise_type_id=value;}
        }
        public virtual int Emp_data_id
        {
            get {return _emp_data_id;}
            set {_emp_data_id=value;}
        }
        public virtual string Emp_idcard
        {
            get {return _emp_idcard;}
            set {_emp_idcard=value;}
        }
        public virtual string Promise_code
        {
            get {return _promise_code;}
            set {_promise_code=value;}
        }
        public virtual string Promise_name
        {
            get {return _promise_name;}
            set {_promise_name=value;}
        }
        public virtual string Promise_detail
        {
            get {return _promise_detail;}
            set {_promise_detail=value;}
        }
        public virtual string Promise_bond
        {
            get {return _promise_bond;}
            set {_promise_bond=value;}
        }
        public virtual string Promise_attach
        {
            get {return _promise_attach;}
            set {_promise_attach=value;}
        }
        public virtual bool Promise_sign_status
        {
            get {return _promise_sign_status;}
            set {_promise_sign_status=value;}
        }
        public virtual DateTime Promise_sign_date
        {
            get {return _promise_sign_date;}
            set {_promise_sign_date=value;}
        }
        public virtual unknown Create_date
        {
            get {return _create_date;}
            set {_create_date=value;}
        }
        #endregion
    }
    #endregion
}