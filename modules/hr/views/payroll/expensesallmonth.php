<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use app\api\Common;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\apihr\ApiHr;

$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/employee-lookup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/expensesallmonth.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

/*** DEFAULT ROUTING **/
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);


$script = <<< JS
$(document).ready(function() {

});
JS;


//$this->registerCssFile(Yii::$app->request->BaseUrl."/css/hr/jquery-ui.css");
$this->registerCssFile(Yii::$app->request->baseUrl . "/fonts/01thaifontcss.css");

$this->registerJs($script);
$this->registerCss("
.spacetable {
    padding-top:15px;
}
.spacerow {
    padding-top:5px;
}
");
?>

    <section class="content">
        <!-- Default box -->
        <div class="box box-danger">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">การจัดการเงินเดือน</a>
                    </li>
                    <li class="active">บันทึกเพิ่มเงิน</li>
                    <li class="active"> บันทึกรายการเพิ่มประจำเดือน</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <form method="post" id="AddLevelForm">
                <div class="box-body">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h4 class="box-title">ข้อมูลรายการ </h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                <div class="col-md-12">
                                    <center>
                                        <label>ประเภทรายการ</label>
                                        &nbsp;
                                        <div class="btn-group">
                                            <select class="form-control" name="id_deduct_template"
                                                    id="id_deduct_template">
                                                <option value="0">กรุณาเลือก</option>
                                                <?php
                                                $arrList = ApiPayroll::listdata_add_deduct_template();
                                                foreach ($arrList as $key => $value) {
                                                    echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div align="right">
                    <a href="javascript:;" id="btnAddFile">
                        <img src="<?php echo $imghr; ?>/add.png" class="img-circle">
                        <span>เพิ่มแถว</span>
                        &nbsp;
                    </a>
                </div>
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h4 class="box-title">รายชื่อพนักงาน </h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tblticket" width="100%" cellpadding="3" cellspacing="3">
                                    <tbody>
                                    <tr class="maintr">
                                        <td width="2%">1.</td>
                                        <td width="4%" class="spacetable">
                                            <table width="100%" height="100%" cellspacing="2" cellpadding="2">
                                                <tr>
                                                    <td>
                                                        <label>ชื่อพนักงาน</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="spacerow">
                                                        <label>วันที่เริ่ม</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="15%" class="spacetable">
                                            <table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2">
                                                <tr>
                                                    <td colspan="2">
                                                        <select class="form-control select2" name="id_emp[]" required>
                                                            <?php $empdata = ApiHr::getempdataall();
                                                            foreach ($empdata as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="60%" class="spacerow">
                                                        <select class="form-control loopmonth" id="montsA" onchange="changeloopmonth(this)" name="start_month_add[]">
                                                            <option select=select>เลือกเดือน</option>
                                                        </select>
                                                    </td>
                                                    <td width="40%" class="spacerow">
                                                        <select class="form-control loopyear" id="yearsA" onchange="changeloopyear(this);" name="start_year_add[]">
                                                            <option select=select>เลือกพ.ศ.</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="4%" class="spacetable">
                                            <table width="100%" height="100%" border="0">
                                                <tr>
                                                    <td style="padding-left: 8px;">
                                                        <label>จำนวน</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 8px;" class="spacerow">
                                                        <label>วันที่สิ้นสุด</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="15%" class="spacetable">
                                            <table width="100%" height="100%" border="0">
                                                <tr>
                                                    <td colspan="2">
                                                        <input type="text" name="total[]"
                                                               class="form-control numberformat"
                                                               data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true"
                                                               data-mask>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="60%" class="spacerow" >
                                                        <select class="form-control loopmonth setstart" id="montsB" name="end_month_add[]">
                                                            <option select=select>เลือกเดือน</option>
                                                        </select>
                                                    </td>
                                                    <td width="40%" class="spacerow">
                                                        <select class="form-control loopyear setstart" id="yearsB" onchange="changeloopyearB(this);"  name="end_year_add[]">
                                                            <option select=select>เลือกพ.ศ.</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="4%" class="spacetable">
                                            <table width="100%" height="100%" border="0">
                                                <tr>
                                                    <td style="padding-left: 5px;">
                                                        <label>บาท</label>
                                                        <a href="javascript:;" onclick="showhidedetail(this)">
                                                            <img src="<?php echo $imghr; ?>/detail_icon.png"
                                                                 style="width: 24px;height: 24px;">
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="15%" class="spacetable">
                                            <table width="100%" height="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <input type="text" name="detail[]"
                                                               class="form-control showdetail" style="display: none;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 10px;">
                                                        &nbsp; &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="8%" class="spacetable">
                                            <table width="100%" height="100%" border="0">
                                                <tr>
                                                    <td>
                                                        &nbsp; &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 10px;">
                                                        &nbsp; &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="text-center" style="margin-top: 20px;">
                                    <div class="btn-group">
                                        <button type="button" id="saveAdddeduct" class="btn btn-block btn-success btn-sm"><i
                                                    class="fa fa-save"></i> บันทึก
                                        </button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="reset" class="btn btn-block btn-danger btn-sm"><i
                                                    class="fa fa-reply"></i> ล้างข้อมูล
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-success"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>