<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "amodel".
 *
 * @property integer $id
 * @property string $abrand_id
 * @property string $agroup_id
 * @property string $model
 * @property string $MDL_CD
 * @property string $name
 * @property string $Abbreviation
 * @property string $enginemodel
 * @property string $ModelGroupSale
 * @property string $Model2YG
 * @property string $GearType
 * @property string $Type
 * @property string $LTG_book
 * @property string $contentmodel
 * @property string $horsepowermodel
 * @property string $gearmodel
 * @property string $ModelGroup
 * @property string $year
 * @property string $toyear
 * @property string $comment
 * @property string $emp_id
 * @property string $add_time
 * @property string $status
 * @property string $ck
 * @property string $distance
 */
class Amodel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'amodel';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abrand_id', 'agroup_id', 'model', 'MDL_CD', 'name', 'Abbreviation', 'enginemodel', 'ModelGroupSale', 'Model2YG', 'GearType', 'Type', 'LTG_book', 'contentmodel', 'horsepowermodel', 'gearmodel', 'ModelGroup', 'year', 'toyear', 'comment', 'emp_id', 'add_time', 'status', 'ck', 'distance'], 'required'],
            [['comment'], 'string'],
            [['abrand_id', 'agroup_id', 'enginemodel', 'contentmodel', 'horsepowermodel', 'gearmodel', 'year', 'toyear', 'add_time'], 'string', 'max' => 20],
            [['model', 'LTG_book'], 'string', 'max' => 50],
            [['MDL_CD', 'Abbreviation', 'ModelGroupSale', 'Model2YG', 'GearType', 'Type'], 'string', 'max' => 100],
            [['name'], 'string', 'max' => 60],
            [['ModelGroup'], 'string', 'max' => 5],
            [['emp_id'], 'string', 'max' => 13],
            [['status', 'ck'], 'string', 'max' => 2],
            [['distance'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abrand_id' => 'Abrand ID',
            'agroup_id' => 'Agroup ID',
            'model' => 'Model',
            'MDL_CD' => 'Mdl  Cd',
            'name' => 'Name',
            'Abbreviation' => 'Abbreviation',
            'enginemodel' => 'Enginemodel',
            'ModelGroupSale' => 'Model Group Sale',
            'Model2YG' => 'Model2 Yg',
            'GearType' => 'Gear Type',
            'Type' => 'Type',
            'LTG_book' => 'Ltg Book',
            'contentmodel' => 'Contentmodel',
            'horsepowermodel' => 'Horsepowermodel',
            'gearmodel' => 'Gearmodel',
            'ModelGroup' => 'Model Group',
            'year' => 'Year',
            'toyear' => 'Toyear',
            'comment' => 'Comment',
            'emp_id' => 'Emp ID',
            'add_time' => 'Add Time',
            'status' => 'Status',
            'ck' => 'Ck',
            'distance' => 'Distance',
        ];
    }
}
