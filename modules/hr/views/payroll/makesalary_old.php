<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:32
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/global';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/makesalary.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->baseUrl."/fonts/01thaifontcss.css");


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li class="active">สร้างข้อมูลเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="fa fa-bullhorn"></i>
                            <h3 class="box-title">สร้างเงินเดือนราย 7 วัน</h3>
                        </div>
                        <div class="box-body" id="btnCreateSalaryDay_1">
                            <div class="callout callout-warning text-center">
                                <h4 id="day_status_text_1">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> เรียบร้อยแล้ว</h4>
                                <div id="day_loadimage_1" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_day_salary_1" onclick="makeSaraly('make_day_salary_1','day_status_text_1','delete_day_salary_1','day_loadimage_1',1,1)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน (1)</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_day_salary_1" onclick="deleteSaraly('make_day_salary_1','day_status_text_1','delete_day_salary_1','day_loadimage_1',1,1)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?> (1)</button>
                            </div>
                        </div>
                        <div class="box-body" id="btnCreateSalaryDay_2">
                            <div class="callout callout-warning text-center">
                                <h4 id="day_status_text_2">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> เรียบร้อยแล้ว</h4>
                                <div id="day_loadimage_2" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_day_salary_2" onclick="makeSaraly('make_day_salary_2','day_status_text_2','delete_day_salary_2','day_loadimage_2',1,2)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน (2)</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_day_salary_2" onclick="deleteSaraly('make_day_salary_2','day_status_text_2','delete_day_salary_2','day_loadimage_2',1,2)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?>(2)</button>
                            </div>
                        </div>
                        <div class="box-body" id="btnCreateSalaryDay_3">
                            <div class="callout callout-warning text-center">
                                <h4 id="day_status_text_3">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> เรียบร้อยแล้ว</h4>
                                <div id="day_loadimage_3" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_day_salary_3" onclick="makeSaraly('make_day_salary_3','day_status_text_3','delete_day_salary_3','day_loadimage_3',1,3)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน (3)</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_day_salary_3" onclick="deleteSaraly('make_day_salary_3','day_status_text_3','delete_day_salary_3','day_loadimage_3',1,3)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?>(3)</button>
                            </div>
                        </div>
                        <div class="box-body" id="btnCreateSalaryDay_4">
                            <div class="callout callout-warning text-center">
                                <h4 id="day_status_text_4">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> เรียบร้อยแล้ว</h4>
                                <div id="day_loadimage_4" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_day_salary_4" onclick="makeSaraly('make_day_salary_4','day_status_text_4','delete_day_salary_4','day_loadimage_4',1,4)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน (4)</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_day_salary_4" onclick="deleteSaraly('make_day_salary_4','day_status_text_4','delete_day_salary_4','day_loadimage_4',1,4)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?>(4)</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <i class="fa fa-bullhorn"></i>
                            <h3 class="box-title">สร้างเงินเดือนราย 15 วัน</h3>
                        </div>
                        <div class="box-body" id="btnCreateSalaryWeekly_1">
                            <div class="callout callout-info text-center">
                                <h4 id="weekly_status_text_1">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> (1) เรียบร้อยแล้ว</h4>
                                <div id="weekly_loadimage_1" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_weekly_salary_1" onclick="makeSaraly('make_weekly_salary_1','weekly_status_text_1','delete_weekly_salary_1','weekly_loadimage_1',2,1)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน (1)</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_weekly_salary_1" onclick="deleteSaraly('make_weekly_salary_1','weekly_status_text_1','delete_weekly_salary_1','weekly_loadimage_1',2,1)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?> (1)</button>
                            </div>
                        </div>
                        <div class="box-body" id="btnCreateSalaryWeekly_2">
                            <div class="callout callout-info text-center">
                                <h4 id="weekly_status_text_2">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> (2) เรียบร้อยแล้ว</h4>
                                <div id="weekly_loadimage_2" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_weekly_salary_2" onclick="makeSaraly('make_weekly_salary_2','weekly_status_text_2','delete_weekly_salary_2','weekly_loadimage_2',2,2)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน (2)</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_weekly_salary_2" onclick="deleteSaraly('make_weekly_salary_2','weekly_status_text_2','delete_weekly_salary_2','weekly_loadimage_2',2,2)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?> (2)</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-bullhorn"></i>
                            <h3 class="box-title">สร้างเงินเดือนราย 30 วัน</h3>
                        </div>
                        <div class="box-body" id="btnCreateSalary">
                            <div class="callout callout-success text-center">
                                <h4 id="monthly_status_text">สร้างเงินเดือนเดือน <?php echo date('m-Y'); ?> เรียบร้อยแล้ว</h4>
                                <div id="monthly_loadimage" style="display:none"><img src="<?php echo $imghr; ?>/ajax-loader.gif" class="img-circle"></div>
                                <button type="button" class="btn  btn-default btn-lg" id="make_monthly_salary" onclick="makeSaraly('make_monthly_salary','monthly_status_text','delete_monthly_salary','monthly_loadimage',3,1)"><i class="fa fa-bitcoin"></i> สร้างเงินเดือน</button>
                                <button type="button" class="btn  btn-danger btn-md" style="margin-top: 20px;" id="delete_monthly_salary" onclick="deleteSaraly('make_monthly_salary','monthly_status_text','delete_monthly_salary','monthly_loadimage',3,1)"><i class="fa fa-trash-o"></i> ลบข้อมูลเงินเดือน <?php echo date('m-Y'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->