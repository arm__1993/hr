<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 18:07
 */

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiReceivewage
{

	public static function getSumPriceWageYearNow($checkyear_forpricewage,$type_company)
	{


		$sql_yearnow = "SELECT datePay , SUM(pricewage) as pricewage
        FROM ytax WHERE datePay LIKE '$checkyear_forpricewage'
        and status != 99
        and status != 1
        and ybranch = '$type_company'
        group by date_format(datePay,\"%Y-%m\")";
        // echo $sql_yearnow;
        // exit(); 

        $sumpricewage_yearnow = Yii::$app->dbERP_service->createCommand($sql_yearnow)->queryAll();

        return $sumpricewage_yearnow;

	}
	public static function getArrSumTarget($checkyear,$type_company)
	{
		$sql_target = "SELECT months ,years ,target FROM config_target_year 
        WHERE years = $checkyear and branch_id = '$type_company'";
        $restarget = Yii::$app->dbERP_service->createCommand($sql_target)->queryAll();

        $arr_sumtarget = [];
        for ($i = 0; $i < count($restarget); $i++) {
        $arr_sumtarget[] = $restarget[$i]['target'];
        }

        return $arr_sumtarget;

	}
	public static function getCheckArray($stringyear,$type_company)
	{
		$sql = "SELECT YEAR(datePay) as y , MONTH(datePay) as m, SUM(pricewage) as pricewage 
        FROM ytax WHERE YEAR(datePay) IN $stringyear 
        and ybranch = '$type_company' GROUP BY y,m ORDER BY y,m";
        $res = Yii::$app->dbERP_service->createCommand($sql)->queryAll();
    	
    	$checkarray = [];    
        foreach ($res as $row) {
        $checkarray[$row['y']][$row['m']] = $row['pricewage'];
        }

    return $checkarray;
	}

	public static function getArrDataAVG($checkyear,$type_company)
	{

		$sql_AVG = "SELECT `target` FROM `config_target_year` WHERE years=$checkyear
        and branch_id = $type_company ORDER BY months ASC";
        $res = Yii::$app->dbERP_service->createCommand($sql_AVG)->queryAll();

        $arrDataAVG = [];

        foreach ($res as $row) {
        $arrDataAVG[] = $row['target'];
        }

    return $arrDataAVG;
	}

	public static function getMechanic($checkyear,$technician,$type_company)
	{
        $relation = "SELECT a.position_id , a.NameUse , b.CHECKTIME_DATE as workdate 
        FROM ERP_easyhr_OU.relation_position as a INNER JOIN ERP_easyhr_TIME_ATTENDANCE.CHECKTIME as b ON b.CHECKTIME_EMP_ID =a.id_card 
        WHERE a.status = 1 AND b.CHECKTIME_DATE LIKE $checkyear 
        AND a.position_id IN ( SELECT id FROM `position`
        WHERE `WorkType` LIKE '%3%' AND `Name` LIKE $technician 
        AND `Status` = 1 And WorkCompany = $type_company ) Group by NameUse";
        $mechanic = Yii::$app->dbERP_easyhr_OU
            ->createCommand($relation)
            ->queryAll();


    return $mechanic;
	}
	public static function getSumWage($checkyear,$type_company)
	{
        $sql_sumwage = "SELECT datePay , SUM(pricewage) as pricewage FROM ytax 
                    WHERE `datePay` LIKE $checkyear 
                    and status != 99
                    and status != 1
                    and ybranch = $type_company group by date_format(datePay,\"%Y-%m\")"; //หารายได้ค่าแรงทั้งหมดของเดือนนั้นๆ
        $sumwage = Yii::$app->dbERP_service
                ->createCommand($sql_sumwage)
                ->queryAll();


    return $sumwage;
	}
	public static function getTechnicianWorkday($checkyear,$technician,$type_company)
	{

            $sql_technicianofWorkday = "SELECT b.CHECKTIME_EMP_ID as EMP , b.CHECKTIME_DATE as workdate 
                FROM ERP_easyhr_OU.relation_position as a INNER JOIN ERP_easyhr_TIME_ATTENDANCE.CHECKTIME as b ON b.CHECKTIME_EMP_ID =a.id_card 
                WHERE a.status = 1 AND b.CHECKTIME_DATE LIKE $checkyear 
                AND b.CHECKTIME_STATUS IN (1,3,4,5,15) 
                AND position_id IN ( SELECT id FROM position WHERE WorkType = 3 
                AND Name LIKE $technician AND Status = 1 AND WorkCompany = $type_company ) 
                Group by NameUse ,date_format(CHECKTIME_DATE,\"%Y-%m-%d\")";
            $technician_Workday = Yii::$app->dbERP_easyhr_OU
                ->createCommand($sql_technicianofWorkday)
                ->queryAll();


    return $technician_Workday;
	}
	public static function getYear()
	{

        $year = Yii::$app->dbERP_service
                ->createCommand("SELECT DATE_FORMAT(CHECKTIME_DATE,'%Y') as year FROM ERP_easyhr_TIME_ATTENDANCE.CHECKTIME GROUP BY year ORDER BY year ASC")
                ->queryAll();


    return $year;
	}

}