<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "b_institutioneducation".
 *
 * @property integer $id
 * @property string $institution_education
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updatteby_user
 * @property string $update_datetime
 */
class BInstitutioneducation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b_institutioneducation';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['institution_education'], 'string', 'max' => 30],
            [['createby_user', 'updatteby_user'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institution_education' => 'Institution Education',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updatteby_user' => 'Updatteby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    public function search($params)
    {
        $model = BInstitutioneducation::find();
        $dataProvider = new ActiveDataProvider(
                [
                    'query'=>$model,
                    'pagination' => ['pageSize' => 10,],
                ]);
        $this->load($params);
        $model->andFilterWhere(['like','institution_education',$this->institution_education]);
        $model->andFilterWhere(['not like','status_active','99']);
         return $dataProvider;
    }
}
