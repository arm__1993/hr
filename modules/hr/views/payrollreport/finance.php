<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:22
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;




?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>ออกรายงาน</li>
                <li class="active">การเงิน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            Content Here
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->