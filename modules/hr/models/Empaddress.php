<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "EMP_ADDRESS".
 *
 * @property integer $id
 * @property integer $ADDR_DATA_NO
 * @property string $ADDR_VILLAGE
 * @property string $ADDR_ROOM_NO
 * @property string $ADDR_FLOOR_NO
 * @property string $ADDR_NUMBER
 * @property string $ADDR_GROUP_NO
 * @property string $ADDR_LANE
 * @property string $ADDR_ROAD
 * @property string $ADDR_SUB_DISTRICT
 * @property string $ADDR_DISTRICT
 * @property string $ADDR_PROVINCE
 * @property string $ADDR_POSTCODE
 * @property string $ADDR_GEOGRAPHY
 * @property string $ADDR_GPS_LATITUDE
 * @property string $ADDR_GPS_LONGTITUDE
 * @property string $ADDR_MAP_DESC
 * @property string $ADDR_REMARK
 * @property integer $ADDR_TYPE
 * @property integer $ADDR_MAIN_ACTIVE
 * @property integer $ADDR_REF_MAIN
 */
class Empaddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EMP_ADDRESS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }
     public static function primarykey()
    {
        return ['id'];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ADDR_DATA_NO', 'ADDR_TYPE', 'ADDR_MAIN_ACTIVE', 'ADDR_REF_MAIN'], 'integer'],
            [['ADDR_MAP_DESC', 'ADDR_REMARK'], 'string'],
            [['ADDR_VILLAGE', 'ADDR_SUB_DISTRICT', 'ADDR_DISTRICT', 'ADDR_PROVINCE', 'ADDR_POSTCODE'], 'string', 'max' => 100],
            [['ADDR_ROOM_NO', 'ADDR_FLOOR_NO', 'ADDR_NUMBER', 'ADDR_GROUP_NO', 'ADDR_LANE'], 'string', 'max' => 50],
            [['ADDR_ROAD'], 'string', 'max' => 150],
            [['ADDR_GEOGRAPHY'], 'string', 'max' => 10],
            [['ADDR_GPS_LATITUDE', 'ADDR_GPS_LONGTITUDE'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ADDR_DATA_NO' => 'Addr  Data  No',
            'ADDR_VILLAGE' => 'Addr  Village',
            'ADDR_ROOM_NO' => 'Addr  Room  No',
            'ADDR_FLOOR_NO' => 'Addr  Floor  No',
            'ADDR_NUMBER' => 'Addr  Number',
            'ADDR_GROUP_NO' => 'Addr  Group  No',
            'ADDR_LANE' => 'Addr  Lane',
            'ADDR_ROAD' => 'Addr  Road',
            'ADDR_SUB_DISTRICT' => 'Addr  Sub  District',
            'ADDR_DISTRICT' => 'Addr  District',
            'ADDR_PROVINCE' => 'Addr  Province',
            'ADDR_POSTCODE' => 'Addr  Postcode',
            'ADDR_GEOGRAPHY' => 'Addr  Geography',
            'ADDR_GPS_LATITUDE' => 'Addr  Gps  Latitude',
            'ADDR_GPS_LONGTITUDE' => 'Addr  Gps  Longtitude',
            'ADDR_MAP_DESC' => 'Addr  Map  Desc',
            'ADDR_REMARK' => 'Addr  Remark',
            'ADDR_TYPE' => 'Addr  Type',
            'ADDR_MAIN_ACTIVE' => 'Addr  Main  Active',
            'ADDR_REF_MAIN' => 'Addr  Ref  Main',
        ];
    }
}
