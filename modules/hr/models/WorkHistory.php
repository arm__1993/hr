<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "work_history".
 *
 * @property integer $id
 * @property string $emp_id
 * @property string $old_work_company
 * @property string $position_code
 * @property string $old_work_position
 * @property string $work_since
 * @property string $work_until
 * @property string $old_work_salary
 * @property string $work_since_full
 * @property string $work_until_full
 * @property string $company_type
 * @property string $work_detail
 * @property string $reason_out_type
 * @property string $reason_out_detail
 * @property double $bonus
 * @property integer $amount_uniforms
 * @property integer $amount_food
 * @property integer $amount_buses
 * @property integer $amount_habitat
 * @property double $amount_oil
 * @property integer $amount_insurance
 * @property string $other
 * @property string $assigned
 * @property string $work_performance
 * @property integer $status
 */
class WorkHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_history';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['work_since_full', 'work_until_full'], 'safe'],
            [['work_detail', 'reason_out_detail', 'assigned', 'work_performance'], 'string'],
            [['bonus', 'amount_oil'], 'number'],
            [['amount_uniforms', 'amount_food', 'amount_buses', 'amount_habitat', 'amount_insurance', 'status'], 'integer'],
            [['emp_id'], 'string', 'max' => 13],
            [['old_work_company', 'old_work_position'], 'string', 'max' => 250],
            [['position_code'], 'string', 'max' => 50],
            [['work_since', 'work_until', 'old_work_salary'], 'string', 'max' => 10],
            [['company_type', 'reason_out_type', 'other'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_id' => 'Emp ID',
            'old_work_company' => 'Old Work Company',
            'position_code' => 'Position Code',
            'old_work_position' => 'Old Work Position',
            'work_since' => 'Work Since',
            'work_until' => 'Work Until',
            'old_work_salary' => 'Old Work Salary',
            'work_since_full' => 'Work Since Full',
            'work_until_full' => 'Work Until Full',
            'company_type' => 'Company Type',
            'work_detail' => 'Work Detail',
            'reason_out_type' => 'Reason Out Type',
            'reason_out_detail' => 'Reason Out Detail',
            'bonus' => 'Bonus',
            'amount_uniforms' => 'Amount Uniforms',
            'amount_food' => 'Amount Food',
            'amount_buses' => 'Amount Buses',
            'amount_habitat' => 'Amount Habitat',
            'amount_oil' => 'Amount Oil',
            'amount_insurance' => 'Amount Insurance',
            'other' => 'Other',
            'assigned' => 'Assigned',
            'work_performance' => 'Work Performance',
            'status' => 'Status',
        ];
    }
}
