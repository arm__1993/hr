<?php

namespace app\modules\gitreport\models;

/**
 * This is the ActiveQuery class for [[HbsoProject]].
 *
 * @see HbsoProject
 */
class HbsoProjectQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return HbsoProject[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HbsoProject|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
