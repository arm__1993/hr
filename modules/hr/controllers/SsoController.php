<?php

namespace app\modules\hr\controllers;
use app\modules\hr\apihr\ApiBenefit;
use app\modules\hr\apihr\ApiSSO;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;
use app\modules\hr\controllers\MasterController;


class SsoController extends MasterController
{

    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }




    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSsohistory()
    {
        return Yii::$app->controller->renderPartial('ssohistory');
       // return $this->render('ssohistory');
    }

    public function actionGetyearbenefitlist()
    {
        if (Yii::$app->request->isAjax) {
          $getValue = Yii::$app->request->get();
          $id_card = $getValue['idcard'];
         // echo $id_card;
          $modelYear = ApiBenefit::ListYearFUNIDCrad($id_card);
          //print_r($modelYear);
          $year = [];
            foreach($modelYear as $value){
                $years = DateTime::convertFormatMonthYear($value['BENEFIT_SAVING_DATE']);
                $year[] = $years['year'];
            }
         $yearResult = array_unique($year);
           \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          return $yearResult;
        }
    }

    public function actionGetdatabenefitthis()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $year = $postValue['year'];
            $id_card = $postValue['idcard'];
            $model = ApiBenefit::ListDataBenefit($id_card,$year);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionGetdatabenefitsumthis()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $year = $postValue['year'];
            $id_card = $postValue['idcard'];
            $model = ApiBenefit::SumListDataBenefitThisYear($id_card,$year);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }
        public function actionGetdatabenefitsumall()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $id_card = $postValue['idcard'];
            $model = ApiBenefit::SumListDataBenefitThisAll($id_card);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionGetyearssolist()
    {
        if (Yii::$app->request->isAjax) {
          $getValue = Yii::$app->request->get();
          $id_card = $getValue['idcard'];
         // echo $id_card;
          $modelYear = ApiSSO::ListYearSSOIDCrad($id_card);
          //print_r($modelYear);
          $year = [];
            foreach($modelYear as $value){
                $years = DateTime::convertFormatMonthYear($value['SOCIAL_SALARY_DATE']);
                $year[] = $years['year'];
            }
         $yearResult = array_unique($year);
           \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          return $yearResult;
        }
    }

    public function actionGetdatassothis()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $year = $postValue['year'];
            $id_card = $postValue['idcard'];
            $model = ApiSSO::ListDataSSO($id_card,$year);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionGetdatassosumthis()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $year = $postValue['year'];
            $id_card = $postValue['idcard'];
            $model = ApiSSO::SumListDataSSOThisYear($id_card,$year);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }
    
        public function actionGetdatassosumall()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $id_card = $postValue['idcard'];
            $model = ApiSSO::SumListDataSSOThisAll($id_card);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    public function actionGetstatussso(){
          if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get();
            $id_card = $getValue['idcard'];
            $model = ApiSSO::getdatassoidcard($id_card);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }

    }
}
