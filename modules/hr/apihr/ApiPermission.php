<?php


/**
* Created by PhpStorm.
* User: adithep
* Date: 2/1/2017 AD
* Time: 10:50
*/

namespace app\modules\hr\apihr;


use app\api\Common;
use app\modules\hr\models\Place;
use app\modules\hr\models\Menu;
use app\api\DBConnect;
use Yii;


class ApiPermission
{
    
    public static function getMenu($id_program)
    {
        $data = Menu::find()
        ->where('place_id = ' .$id_program)
        ->groupBy('id')
        ->asArray()
        ->all();
        $result = $resultdata =[];
        foreach($data as $item){
            if($item['parent_id'] == ''){
                $result[$item['id']][$item['id']] = $item;
                $result[$item['id']]['status'] = $item['status'];
            }
            else{
                $result[$item['parent_id']][$item['id']] = $item;
            }
            
        }
        foreach($result as $k => $item){
            if($item['status']!='99'){
                if(array_key_exists($k,$item))
                foreach($item as $key => $i){
                    $resultdata[$key] = $i;
                }
            }
        }
        return $resultdata;
    }
    public static function getchild($id_parent)
    {
        $data = Menu::find()->where(['=','parent_id',$id_parent])
        ->asArray()
        ->all();
        return $data;
    }
    public static function getMenuHead($id_program)
    {
        $data = Menu::find()
        ->where("place_id = " .$id_program." and status != 99 and parent_id='' ")
        ->groupBy('id')
        ->asArray()
        ->all();
        return $data;
    }
    public static function getSubmenu($menuid)
    {
        $data = Menu::find()
        ->where('parent_id = ' .$menuid)
        ->groupBy('id')
        ->asArray()
        ->all();
        $resultdata =[];
        foreach ($data as $key => $item) {
            $resultdata[$item['id']] = $item;
        }
        return $resultdata;
    }
    public static function getPlace($id_program)
    {
        $data = Place::find()
        ->where('id = ' .$id_program. ' AND status <> 99')
        ->groupBy('id')
        ->asArray()
        ->all();
        $result = [];
        foreach($data as $item){
            $result[$item['id']] = $item;
        }
        
        return $result;
    }
    public static function PositionByEmp($idcard)
    {
        $sql = 'SELECT * FROM ERP_easyhr_OU.relation_position as rp  INNER JOIN ERP_easyhr_OU.position as p on rp.position_id = p.id WHERE rp.id_card = '.$idcard;
        $datas = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        return $datas;
    }
    public static function menuById($id)
    {
        return Menu::find()->where(['=','id', $id])->asArray()->all();
    }
    public static function buildMenu($array,$parent_id,$arr)
    {
        foreach ($array as $item)
        {
            if($parent_id != ''){
                $arr[$parent_id][]=$item;
            }
            else{
                $arr[$item['id']] = $item;
                self::buildMenu($array,$item['parent_id'],$arr);
            }
        }
        
    }
    public static function DataMenuByEmp($param,$arr)
    {
        $sql = 'SELECT menu_manage.*,menu.name as menuName,menu.parent_id,menu.status as menu_status,place.id as place_id,place.name as placeName,place.status as place_status ';
        $sql .= 'FROM ERP_easyhr_checktime.menu_manage inner join menu on menu_manage.menu_id = menu.id inner join place on menu.place_id = place.id WHERE position_id = '.$param['selectPosition'].' order by menu.parent_id ASC';
        $datas = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        $result = $resultdata =[];
        foreach($datas as $item){
            $result['parent_id']=$item;
        }
        foreach($result as $item){
            echo "<pre>";
            print_r(self::buildMenu($result,$item['parent_id'],$arr));
            echo "</pre>";
            exit();
        }
        foreach($result as $k => $item){
            if($item['status']!='99'){
                if(array_key_exists($k,$item))
                foreach($item as $key => $i){
                    if($key !='status')
                    $resultdata[$i['place_id']][$i['menu_id']] = $i;
                }
            }
        }
        return $resultdata;
    }
    
    public static function DataMenuByEmpA($param)
    {
        $sql = 'SELECT menu_manage.*,menu.name as menuName,menu.parent_id,menu.status as menu_status,place.id as place_id,place.name as placeName,place.status as place_status, menu.id as memu_id ';
        $sql .= 'FROM ERP_easyhr_checktime.menu_manage inner join menu on menu_manage.menu_id = menu.id inner join place on menu.place_id = place.id WHERE position_id = '.$param['selectPosition'].' order by menu.place_id, menu.parent_id ASC';
        $datas = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        $result = $resultdata =[];
        foreach($datas as $item){
            if($item['parent_id']=='')
            {
               // $result[$item['place_id']][$item['id']][$item['memu_id']] = $item;
            }
            else{
                $result[$item['place_id']][$item['parent_id']][$item['memu_id']] = $item;
            }
        }
        
        return $result;
        
    }
    
    public static function DataMenuByEmpParent($param)
    {
        $sql = 'SELECT menu_manage.*,menu.name as menuName,menu.parent_id,menu.status as menu_status,place.id as place_id,place.name as placeName,place.status as place_status, menu.id as memu_id ';
        $sql .= 'FROM ERP_easyhr_checktime.menu_manage inner join menu on menu_manage.menu_id = menu.id inner join place on menu.place_id = place.id WHERE position_id = '.$param['selectPosition'].' and parent_id="" order by menu.place_id, menu.parent_id ASC';
        $datas = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        $result = $resultdata =[];
        foreach($datas as $item){
            $result[$item['place_id']] = $item;
        }
        
        return $result;
        
    }
    
    public static function dataChecklist($id_company,$id_department,$id_saction,$id_Program)
    {
        $str_section = (is_array($id_saction))?JOIN(',',$id_saction):'';
        $sql = 'SELECT menu.place_id,menu.id,mm.position_id,LEVEL,p.Section,mm.working_company_id,p.id as positionID FROM ERP_easyhr_checktime.menu INNER JOIN ERP_easyhr_checktime.menu_manage as mm on menu.id = mm.menu_id ';
        $sql .= 'INNER join ERP_easyhr_OU.position as p on mm.position_id = p.id ';
        $sql .= 'where menu.status != "99" and mm.status != "99" ';
        $sql .= 'and menu.place_id = '.$id_Program.' ';
        if(isset($id_company)){
            $sql .= 'and p.WorkCompany = '.$id_company.' ';
        }
        if(isset($id_department)){
            $sql .= 'and p.Department = '.$id_department.' ';
        }
        
        if(isset($str_section) && $str_section !=''){
            $sql .= ' and p.Section in ('.$str_section.') ';
        }
        // on mm.position_id = p.id where menu.status != "99" and mm.status != "99" and place_id = 26 and WorkCompany = '',WorkCompany='',Section='' group by  p.id
        $datas = Yii::$app
        ->dbERP_easyhr_checktime
        ->createCommand($sql)
        ->queryAll();
        return $datas;
    }
    
}


?>