<?php

namespace app\modules\vhc\controllers;
use Yii;
use yii\helpers\BaseJson;
use yii\helpers\Json;
use app\modules\vhc\controllers\MasterController;
use app\modules\vhc\models\serviceold\Crepair;
use app\modules\vhc\models\serviceold\CrepairList;
use app\modules\vhc\models\serviceold\Wage;
use app\modules\vhc\models\serviceold\YtaxListParts;
use app\modules\vhc\models\serviceold\Amodel;
use app\modules\vhc\models\VHC\VhcModel;
use app\modules\vhc\models\serviceold\Parts;
use app\modules\vhc\api\ApiServiceVHC;
use app\modules\vhc\models\service\VhcMapping;
use app\modules\vhc\models\service\VhcSeviceLabor;
use app\modules\vhc\models\service\VhcSevicePart;

class ServiceController extends MasterController
{
    public function actionIndex()
    {
        $crepair = Crepair::find()->where(['!=','sdel','99'])->all();
        return $this->render('index',[
            'crepair' => $crepair,
        ]);
    }
    public function actionCrepairlist()
    {
        $getValue = Yii::$app->request->get('data');
        $html = '';
        $crepair = CrepairList::find()->where([crepair_id=>$getValue])->all();
        foreach ($crepair as $item) {
            $html .= $this->renderPartial('_crepairList',[
                'parentId'=>$getValue,
                'item' => $item,
            ]);
        }
        return $html;
    }
    public function actionLaber()
    {
        $getValue = Yii::$app->request->get('data');
        $html = '';
        $laber = Wage::find()->where(['like','crepair_id',$getValue['crepairId']])
        ->andWhere(['like','crepair_list_id',$getValue['crepair_list_id']])
        ->andWhere(['!=','status','99'])
        ->all();
        foreach ($laber as $item) {
            $html .= $this->renderPartial('_laber',[
                'crepairId'=>$getValue['crepairId'],
                'crepair_list_id'=>$getValue['crepair_list_id'],
                'item' => $item,
            ]);
        }
        return $html;
    }
    public function actionParts()
    {
        $getValue = Yii::$app->request->get('data');
        $html = '';
        $parts = YtaxListParts::find()
        ->select('*,count(pw_id) as countpw')
        ->where(['crepair_id'=>$getValue['crepairId']])
        ->andWhere(['crepair_list_id'=>$getValue['crepair_list_id']])
        ->andWhere(['wage_id'=>$getValue['wage']])
        ->andWhere(['!=','status','99'])
        ->groupBy(['pw_id'])
        ->having('countpw >3') 
        ->limit(2)      
        ->all();
        foreach ($parts as $item) {
            $html .= $this->renderPartial('_part',[
                'crepairId'=>$getValue['crepairId'],
                'crepair_list_id'=>$getValue['crepair_list_id'],
                'item' => $item,
            ]);
        }
        return $html;
    }
    public function actionSeartpart()
    {
        $html =  '';
        $model = Parts::find()->select(['id','bar_code','parts_name'])->where(['!=','status','99'])->limit(5)->asArray()->all();
        foreach ($model as $key => $value) {
            $html .= $this->renderPartial('_optionSelect',[
                'item' => $value,
            ]);
        }
        return $html; 
    }
    
    public function actionSelectmodel()
    {
        $model = Amodel::find()->where(['!=','status','99'])
        ->andWhere(['not', ['Abbreviation' => '']])
        ->groupBy(['Abbreviation'])->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $model;
    }
    public function actionMapdata()
    {
        $postValue = Yii::$app->request->post('data');
        $dataObj = Json::decode($postValue, $asArray = true);
        $wage_model = ApiServiceVHC::mapModelwage($dataObj['modelgroup'],$dataObj['labor']['id']);
        $mappingsave = ApiServiceVHC::saveServiceMapping(null,$dataObj['model'],$dataObj['vhcmodel']);
        if($mappingsave->savestatus==true){
            $laborsave = ApiServiceVHC::saveServiceLabor(null,$wage_model[0],$mappingsave,$dataObj['crepair'],$dataObj['crepair_list'],$dataObj['labor']);
        }

    }
    public function actionSelectvhcmodel()
    {
        $model = VhcModel::find()->asArray()->all();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return  $model;
    }
    public function actionSelectpart()
    {
        $getValue = Yii::$app->request->post('data');
        $html = '';
        $dataget = json_decode($getValue);
        foreach ($dataget as $item) {
            $data = json_decode($item);
            $html .= $this->renderPartial('_partselect',[
                'item' => $data,
            ]);
        }
        return $html;
    }
    public function actionselectcrepai()
    {
        return $this->renderPartial('_systemrepair');
    }

}
