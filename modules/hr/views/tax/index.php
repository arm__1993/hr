<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\Utility;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

//use app\api\AjaxSubmitButton;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/vendor/bower/admin-lte/plugins/input-mask/jquery.inputmask.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_structure.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_type.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_section.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_mapping.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_income_mapp.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tax/tax_reduce_other.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li class="active">ตั้งค่าข้อมูลภาษี</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tax_income1" data-toggle="tab">โครงสร้างภาษีเงินได้บุคคลธรรมดา</a>
                        </li>
                        <li><a href="#tax_income2" data-toggle="tab">ลดหย่อนอื่นๆ รายปี </a></li>
                        <li><a href="#tax_income3" data-toggle="tab">mapping รายได้กับภาษี</a></li>
                        <li><a href="#tax_income5" data-toggle="tab">รายได้ ภาษีมาตรา 40 </a></li>
                        <li><a href="#tax_income6" data-toggle="tab">ประเภทภาษีบุคคลธรรมดา</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tax_income1">
                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmstructure',
                                        'header' => '<strong>โครงสร้างภาษีเงินได้บุคคลธรรมดา</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewstructure',
                                            'onClick' => 'document.getElementById("frmOtActivity").reset();document.getElementById("structure_id").value = "";',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มโครงสร้างภาษีเงินได้บุคคลธรรมดา ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-lg'
                                    ]);
                                    ?>
                                    <form role="form" id="frmOtActivity" onsubmit="return suubmitfrm();"
                                          data-toggle="validator">
                                        <div class='row'>
                                            <div class='col-md-6'>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>ช่วงเงินได้ เริ่มต้น <span>*</span></label>
                                                        <input id="income_floor" name="income_floor"
                                                               data-required="true"
                                                               class="form-control" type="text"
                                                               placeholder="ช่วงเงินได้ เริ่มต้น" required>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>ช่วงเงินได้ สิ้นสุด <span>*</span></label>
                                                        <input id="income_ceil" name="income_ceil" data-required="true"
                                                               class="form-control" type="text"
                                                               placeholder="ช่วงเงินได้ สิ้นสุด" required>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>อัตราภาษี <span>*</span></label>
                                                        <input id="tax_rate" name="tax_rate" data-required="true"
                                                               class="form-control" type="text"
                                                               placeholder="อัตราภาษี (%)" required>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>ภาษีแต่ละขั้นเงินได้สุทธิ <span>*</span></label>
                                                        <input id="gross_step" name="gross_step" data-required="true"
                                                               class="form-control" type="text"
                                                               placeholder="ภาษีแต่ละขั้นเงินได้สุทธิ" required>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>ภาษีสะสมสูงสุดของขั้น <span>*</span></label>
                                                        <input id="max_accumulate" name="max_accumulate"
                                                               data-required="true"
                                                               class="form-control" type="text"
                                                               placeholder="ภาษีสะสมสูงสุดของขั้น" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-6'>
                                                <div class="box-body">
                                                    <div class="form-group" id="frm-from_year">
                                                        <label>เริ่มใช้ตั้งแต่ปี <span>*</span></label>
                                                        <input id="from_year" name="from_year" data-required="true"
                                                               min="<?php echo date('Y') - 2 ?>"
                                                               max="<?php echo date('Y') + 5 ?>" step="1"
                                                               value="<?php echo date('Y') ?>" class="form-control"
                                                               type="number" placeholder="ชื่อกิจกรรม" require>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group" id="frm-to_year">
                                                        <label>จนถึงปี <span>*</span></label>
                                                        <input id="to_year" name="to_year" data-required="true"
                                                               min="<?php echo date('Y') ?>"
                                                               max="<?php echo date('Y') + 10 ?>" step="1"
                                                               class="form-control" type="number"
                                                               placeholder="จนถึงปี" require>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>หมายเหตุ </label>
                                                        <input id="remark" name="remark" data-required="true"
                                                               class="form-control" type="text"
                                                               placeholder="หมายเหตุ">
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                                <div class="box-body">
                                                    <label>สถานะ</label>
                                                    <select class="form-control" id="status_structure"
                                                            name="status_structure">
                                                        <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                            Active
                                                        </option>
                                                        <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                            In Active
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="box-body">
                                            <?php echo Html::hiddenInput('structure_id', null, ['id' => 'structure_id']); ?>
                                            <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSavestructure', 'type' => 'submit']); ?>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <br/>
                                        <?php

                                        Pjax::begin(['id' => 'pjax_reduce_structure']);
                                        echo GridView::widget([
                                            'id' => 'gridstructure',
                                            'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                            'dataProvider' => $TaxIcomeStructureProvider,
                                            'filterModel' => $TaxIcomeStructureSearch,
                                            //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                //'id',
                                                [
                                                    'attribute' => 'income_floor',
                                                    'label' => 'เงินได้สุทธิเริ่มต้น',
                                                    'value' => function($data){
                                                        return Helper::displayDecimal($data->income_floor);
                                                    },  
                                                    //'income_floor',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                                                ],
                                                [
                                                    'attribute' => 'income_ceil',
                                                    'label' => 'เพดานเงินได้สุทธิ',
                                                    'value' => function($data){
                                                        return Helper::displayDecimal($data->income_ceil);
                                                    },   
                                                    //'income_ceil',
                                                    //'format' => 'raw',
                                                    'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                                                ],
                                                [
                                                    'attribute' => 'tax_rate',
                                                    'label' => 'อัตราภาษี (%)',
                                                    'value' => function($data){
                                                        return Helper::displayDecimal($data->tax_rate);
                                                    },
                                                    //'value' => 'tax_rate',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                                                ],
                                                [
                                                    'attribute' => 'gross_step',
                                                    'label' => 'ภาษีแต่ละขั้นเงินได้สุทธิ',
                                                    'value' => function($data){
                                                        return Helper::displayDecimal($data->gross_step);
                                                    },
                                                    //'value' => 'gross_step',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                                                ],
                                                [
                                                    'attribute' => 'max_accumulate',
                                                    'label' => 'ภาษีสะสมสูงสุดแต่ละขั้น',
                                                    'value' => function($data){
                                                        return Helper::displayDecimal($data->max_accumulate);
                                                    },
                                                    //'value' => 'max_accumulate',
                                                    //'format' => 'raw',
                                                    //'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                                                ],
                                                [
                                                    'attribute' => 'statuc_active',
                                                    'label' => 'สถานะ',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 50px;text-align:center']
                                                ],

                                                [
                                                    'attribute' => 'createby_user',
                                                    'value' => 'createby_user',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_datetime);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'headerOptions' => ['width' => '100'],
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{update}  &nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        getUpdatestructure(' . $data->id . ');
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    Deletestructure(' . $data->id . ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        Pjax::end();  //end pjax_gridcorclub

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tax_income2">
                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmReduce_other',
                                        'header' => '<strong>ลดหย่อนอื่นๆรายปี</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewReduce_other',
                                            'onClick' => 'document.getElementById("frmReduce_other").reset();document.getElementById("reduc_id").value = "";',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลลดหย่อนอื่นๆรายปี',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-lg'
                                    ]);
                                    ?>
                                    <form role="form" id="frmReduce_other"
                                          onsubmit="return suubmitfrmReduce_other();" data-toggle="validator">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>ชื่อการลดหย่อน <span>*</span></label>
                                                <input id="reduce_name" name="reduce_name" data-required="true"
                                                       class="form-control" type="text" placeholder="ชื่อการลดหย่อน"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>จำนวนที่ลดหย่อนได้ <span>*</span></label>
                                                <input id="reduce_amount" name="reduce_amount" data-required="true"
                                                       class="form-control" type="text"
                                                       placeholder="จำนวนที่ลดหย่อนได้" required>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>จำนวนที่ลดหย่อนได้สูงสุด <span>*</span></label>
                                                <input id="reduce_amount_max" name="reduce_amount_max"
                                                       data-required="true"
                                                       class="form-control" type="text"
                                                       placeholder="จำนวนที่ลดหย่อนได้สูงสุด" required>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>ปีที่ได้รับการลดหย่อน <span>*</span></label>
                                                <input id="for_year" name="for_year" data-required="true"
                                                       class="form-control" type="number"
                                                       placeholder="ปีที่ได้รับการลดหย่อน" required>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-body">
                                            <label>สถานะ</label>
                                            <select class="form-control" id="activity_status_reduc"
                                                    name="activity_status">
                                                <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                    Active
                                                </option>
                                                <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                    In Active
                                                </option>
                                            </select>
                                        </div>
                                        <div class="box-body">
                                            <?php echo Html::hiddenInput('reduc_id', null, ['id' => 'reduc_id']); ?>
                                            <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveReduce', 'type' => 'submit']); ?>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <br/>
                                        <?php

                                        Pjax::begin(['id' => 'pjax_reduce_other']);
                                        echo GridView::widget([
                                            'id' => 'gridreduce_other',
                                            'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                            'dataProvider' => $TaxReduceOtherProvider,
                                            'filterModel' => $TaxReduceOtherSearch,
                                            //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                //'id',
                                                [
                                                    'attribute' => 'reduce_name',
                                                    'label' => 'ชื่อการลดหย่อน',
                                                    'value' => 'reduce_name',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;']
                                                ],
                                                [
                                                    'attribute' => 'reduce_amount',
                                                    'label' => 'จำนวนที่ลดหย่อนได้',
                                                    'value' => function($data){
                                                        return Helper::displayDecimal($data->reduce_amount);
                                                    },//'reduce_amount',
                                                    //'format' => 'raw',
                                                    'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                                                ],
                                                [
                                                    'attribute' => 'reduce_amount_max',
                                                    'label' => 'จำนวนที่ลดหย่อนได้สูงสุด',
                                                    'value' => function($data){
                                                        return Helper::displayDecimal($data->reduce_amount_max);
                                                    }, 
                                                    //'reduce_amount_max',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;text-align:right;']
                                                ],
                                                [
                                                    'attribute' => 'for_year',
                                                    'label' => 'ปีที่ได้รับการลดหย่อน',
                                                    'value' => 'for_year',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;']
                                                ],
                                                [
                                                    'attribute' => 'statuc_active',
                                                    'label' => 'สถานะ',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 50px;text-align:center']
                                                ],

                                                [
                                                    'attribute' => 'createby_user',
                                                    'value' => 'createby_user',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_datetime);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'headerOptions' => ['width' => '100'],
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{update}  &nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        getUpdateReduce_other(' . $data->id . ');
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    DeleteReduce_other(' . $data->id . ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        Pjax::end();  //end pjax_gridcorclub

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tax_income3">
                            <form action="#" method="POST" id="frmtaxincome" name="frmtaxincome">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <div class="pull-right">
                                            <?php
                                            echo Html::button('<i class="fa fa-save"></i> บันทึก',
                                                [
                                                    'class' => 'btn btn-primary',
                                                    'id' => 'btnSaveTaxIncome',
                                                ]);
                                            ?>
                                            <br/>
                                            <br/>
                                        </div>
                                        <table id="example1" class="table table-bordered table-striped dataTable"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr>
                                                <th style="width: 10%">ลำดับ</th>
                                                <th style="width: 25%">รายการเพิ่ม</th>
                                                <th style="width: 45%">รายได้มาตรา 40</th>
                                                <th style="width: 20%">อัตราภาษี (%)</th>
                                            </tr>
                                            <thead>
                                            <tbody>
                                            <?php
                                            $idx =1;
                                            foreach ($arrAddDeduct as $item) {

                                                $tax_rate = $item['tax_rate'];
                                                ?>
                                                <tr>
                                                    <td><?php echo $idx; ?></td>
                                                    <td><?php echo $item['ADD_DEDUCT_TEMPLATE_NAME'];?></td>
                                                    <td><?php
                                                        $key = $item['ADD_DEDUCT_TEMPLATE_ID'];
                                                        $dName = "taxincomesection_id[$key]";
                                                        $tName = "taxrate[$key]";

                                                        echo Html::dropDownList($dName, $item['tax_section_id'], $arrTaxIncomeSection, [
                                                            'id' => 'taxincomesection_id',
                                                            'prompt' => 'ไม่คิดภาษี',
                                                            'class' => 'form-control',
                                                            //'data-required' => 'true',
                                                        ]);

                                                        ?></td>
                                                    <td><input type="text" id="taxrate" name="<?php echo $tName;?>" class="form-control" value="<?php echo $tax_rate;?>"
                                                               placeholder="0.00"></td>
                                                </tr>
                                                <?php
                                                $idx++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="tax_income5">

                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmIncomeSection',
                                        'header' => '<strong>รายได้ ภาษีมาตรา 40</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewOTActivity',
                                            'onClick' => 'document.getElementById("frmIncomeSection").reset();document.getElementById("hide_activityedit").value = "";',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลรายได้ ภาษีมาตรา 40',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-lg'
                                    ]);
                                    ?>
                                    <form role="form" id="frmIncomeSection"
                                          onsubmit="return suubmitfrmIncomeSection();" data-toggle="validator">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>ประเภทเงินได้<span>*</span></label>
                                                    <input id="tax_section_name" name="tax_section_name"
                                                           data-required="true" class="form-control" type="text"
                                                           placeholder="ประเภทเงินได้" required>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <input type="hidden" id="id" name="id">
                                                <div class="form-group">
                                                    <label>ประเภทภาษีบุคคลธรรมดา<span>*</span></label>
                                                    <input id="tax_income_type_id" name="tax_income_type_id"
                                                           data-required="true" class="form-control" type="text"
                                                           placeholder="ประเภทภาษีบุคคลธรรมดา" list="sectionList"
                                                           required>
                                                    <datalist id="sectionList"></datalist>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <input type="hidden" id="id" name="id">
                                                <div class="form-group">
                                                    <label>หักค่าใช้จ่ายได้ %<span>*</span></label>
                                                    <input id="expenses_percent" name="expenses_percent"
                                                           data-required="true" class="form-control" type="number"
                                                           placeholder="หักค่าใช้จ่ายได้ %" >
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <input type="hidden" id="id" name="id">
                                                <div class="form-group">
                                                    <label>หักค่าใช้จ่ายไม่เกิน<span>*</span></label>
                                                    <input id="expenses_notover" name="expenses_notover"
                                                           data-required="true" class="form-control" type="number"
                                                           placeholder="หักค่าใช้จ่ายไม่เกิน" list="sectionList"
                                                           required>
                                                    <datalist id="sectionList"></datalist>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>คำอธิบาย</label>
                                                    <textarea id="comment" name="comment" style="width:100%"
                                                              data-required="true" class="form-control"
                                                              type="text"></textarea>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control" id="activity_status"
                                                        name="activity_status">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <?php echo Html::hiddenInput('hide_activityedit', null, ['id' => 'hide_activityedit']); ?>
                                                <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveSection', 'type' => 'submit']); ?>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <br/>
                                        <?php

                                        Pjax::begin(['id' => 'pjax_taxSection']);
                                        echo GridView::widget([
                                            'id' => 'gridtaxSection',
                                            'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                            'dataProvider' => $TaxIncomeSectionProvider,
                                            //'filterModel' => $TaxIncomeSectionSearch,
                                            //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                //'id',
                                                [
                                                    'attribute' => 'tax_income_type_name',
                                                    'label' => 'ประเภทภาษีบุคคลธรรมดา',
                                                    'value' => 'tax_income_type_name',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'width: 10%;']
                                                ],
                                                [
                                                    'attribute' => 'tax_section_name',
                                                    'label' => 'ประเภทเงินได้',
                                                    'value' => 'tax_section_name',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'width: 30%;']
                                                ],
                                                [
                                                    'attribute' => 'expenses_percent',
                                                    'label' => 'หักค่าใช้จ่ายได้ %',
                                                    'value' => 'expenses_percent',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'width: 10%;text-align:right']
                                                ],

                                                [
                                                    'attribute' => 'expenses_notover',
                                                    'label' => 'หักค่าใช้จ่ายไม่เกิน',
                                                    //'value' => 'expenses_notover',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'value'=>function($data){
                                                        return Helper::displayDecimal($data->expenses_notover);
                                                    },
                                                    'contentOptions' => ['style' => 'width: 10%;text-align:right']
                                                ],
                                                [
                                                    'attribute' => 'statuc_active',
                                                    'label' => 'สถานะ',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5%;text-align:center']
                                                ],

                                                [
                                                    'attribute' => 'createby_user',
                                                    'value' => 'createby_user',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 10%;']
                                                ],
                                                [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_datetime);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 10%;']
                                                ],
                                                [
                                                    'headerOptions' => ['width' => '100'],
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{update}  &nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        getUpdateSection(' . $data->id . ');
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    DeleteSection(' . $data->id . ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        Pjax::end();  //end pjax_gridcorclub

                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tax_income6">

                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmincomeType',
                                        'header' => '<strong>ประเภทภาษีบุคคลธรรมดา</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewOTActivity',
                                            'onClick' => 'document.getElementById("frmincomeType").reset();document.getElementById("id_type").value = "";',
                                            'label' => '<i class="fa fa-plus-circle"></i>  ประเภทภาษีบุคคลธรรมดา',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-lg'
                                    ]);
                                    ?>
                                    <form role="form" id="frmincomeType" onsubmit="return suubmitfrmincomeType();"
                                          data-toggle="validator">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>ชื่อประเภทภาษีเงินได้บุคคลธรรมดา <span>*</span></label>
                                                    <input id="taxincome_type" name="taxincome_type"
                                                           data-required="true" class="form-control" type="text"
                                                           placeholder="ชื่อภาษีเงินได้บุคคลธรรมดา" required>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control" id="type_status" name="type_status">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <?php echo Html::hiddenInput('id_type', null, ['id' => 'id_type']); ?>
                                                <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveconfigType', 'type' => 'submit']); ?>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <br/>
                                        <?php

                                        Pjax::begin(['id' => 'pjax_taxtype']);
                                        echo GridView::widget([
                                            'id' => 'gridtaxtype',
                                            'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                            'dataProvider' => $TaxIncomeTypeProvider,
                                            //'filterModel' => $TaxIncomeTypeSearch,
                                            //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                //'id',
                                                [
                                                    'attribute' => 'taxincome_type',
                                                    'label' => 'ชื่อประเภทภาษีเงินได้บุคคลธรรมดา',
                                                    'value' => 'taxincome_type',
                                                    //'format' => 'raw',
                                                    // 'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 50px;']
                                                ],
                                                [
                                                    'attribute' => 'statuc_active',
                                                    'label' => 'สถานะ',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 50px;text-align:center']
                                                ],

                                                [
                                                    'attribute' => 'createby_user',
                                                    'value' => 'createby_user',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_datetime);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'headerOptions' => ['width' => '100'],
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{update}  &nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                        getUpdateType(' . $data->id . ');
                                                                })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    DeleteType(' . $data->id . ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                            ]);
                                                        },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        Pjax::end();  //end pjax_gridcorclub

                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->