<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/6/2017 AD
 * Time: 11:57
 */



use app\bundle\AppAsset;

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payrollextra/showdeduction.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/ot_manageot.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

?>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li><a href="#">บันทึกจ่ายเงินระหว่างเดือน</a></li>
                <li class="active">แสดงรายการจ่ายเงินระหว่างเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <?php echo Html::dropDownList('company_id', null, [], [
                            'id' => 'activity_id',
                            'prompt' => 'เลือกบริษัท',
                            'class' => 'form-control',
                            'data-required' => 'true',
                        ]); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo Html::dropDownList('for_month', null, [], [
                            'id' => 'activity_id',
                            'prompt' => 'เลือกเดือนที่จ่าย',
                            'class' => 'form-control',
                            'data-required' => 'true',
                        ]); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo Html::dropDownList('for_year', null, [], [
                            'id' => 'activity_id',
                            'prompt' => 'เลือกปี',
                            'class' => 'form-control',
                            'data-required' => 'true',
                        ]); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo Html::dropDownList('template_id', null, [], [
                            'id' => 'activity_id',
                            'prompt' => 'รายการจ่าย',
                            'class' => 'form-control',
                            'data-required' => 'true',
                        ]); ?>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary" id="btnSearch"><i class="fa fa-search"></i> ค้นหา</button>
                    </div>
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">แสดงรายชื่อพนักงานที่ได้รับค่าตอบแทน</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive" id="dvEmpList">

                                <?php
                                Pjax::begin(['id' => 'pjax_tbemployeeot']);

                                ?>
                                <table id="tbemployeeot" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 2%">ลำดับ</th>
                                        <th style="width: 15%">ชื่อ-สกุล</th>
                                        <th style="width: 15%">บริษัท</th>
                                        <th style="width: 10%">แผนก</th>
                                        <th style="width: 8%">จำนวน</th>
                                        <th style="width: 10%">หมายเหตุ</th>
                                        <th style="width: 8%">แก้ไข/ลบ</th>
                                    </tr>
                                    <thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>นายอดิเทพ ไชยสาร</td>
                                        <td>บริษัท นกเงือก โซลูชั่น จำกัด</td>
                                        <td>Production</td>
                                        <td style="text-align: right">5,000</td>
                                        <td>ค่าทะลุเป้า</td>
                                        <td>
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/global/edit-icon.png">&nbsp;
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/global/delete-icon.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>นายณัฐวุฒิ จุมปา</td>
                                        <td>บริษัท นกเงือก โซลูชั่น จำกัด</td>
                                        <td>Production</td>
                                        <td style="text-align: right">5,000</td>
                                        <td>ค่าทะลุเป้า</td>
                                        <td>
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/global/edit-icon.png">&nbsp;
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/global/delete-icon.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>นายบัญญัติ ปัญญาโกน</td>
                                        <td>บริษัท นกเงือก โซลูชั่น จำกัด</td>
                                        <td>Production</td>
                                        <td style="text-align: right">5,000</td>
                                        <td>ค่าทะลุเป้า</td>
                                        <td>
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/global/edit-icon.png">&nbsp;
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/global/delete-icon.png">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <?php
                                Pjax::end();  //end pjax_tbemployeeot

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="height: 20px;"></div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
