-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2017 at 09:33 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `collins_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_data`
--

CREATE TABLE `hrlog_data` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส runing number เป็น PK',
  `action_type` varchar(10) DEFAULT NULL COMMENT 'INSERT, UPDATE, DELETE',
  `table_name` varchar(100) DEFAULT NULL COMMENT 'ชื่อตารางที่เกิด log',
  `record_id` int(11) DEFAULT NULL COMMENT 'record id หรือ id ของ table นั้นๆ',
  `field_name` varchar(100) DEFAULT NULL COMMENT 'ชื่อ field ที่เกิด log',
  `old_data` varchar(255) DEFAULT NULL COMMENT 'ข้อมูลเดิม',
  `replace_data` varchar(255) DEFAULT NULL COMMENT 'ข้อมูลที่ถูกแก้ไข',
  `user_account` varchar(50) DEFAULT NULL COMMENT 'user account ที่ดำเนินการ',
  `controller_id` varchar(100) DEFAULT NULL COMMENT 'controller ที่ทำงาน',
  `action_id` varchar(100) DEFAULT NULL COMMENT 'action ที่ทำงาน',
  `host_name` varchar(100) DEFAULT NULL COMMENT 'ชื่อ hostname',
  `ip_address` varchar(50) DEFAULT NULL COMMENT 'หมายเลข ip ของเครื่อง user',
  `log_datetime` datetime DEFAULT NULL COMMENT 'วันเวลาที่เกิด log'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_data`
--
ALTER TABLE `hrlog_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_data`
--
ALTER TABLE `hrlog_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส runing number เป็น PK';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
