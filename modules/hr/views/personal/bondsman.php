<?php 
use app\modules\hr\controllers\PersonalController;
?>
<div class="row">

    <!--ข้อมูลเงินค้ำประกัน-->
    <div class="col-md-6">
        <form class="form-horizontal">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">ข้อมูลเงินค้ำประกัน</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">สถานะเงินค้ำประกัน</label>
                        <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone':'';?>">
                            <div class="col-sm-6" style="padding-top:6px">
                                <input type="radio" onclick="moneyBondHave(1)" <?php echo ($bmModel['start_money_bonds'] == 1) ? 'checked':'' ?> name="start_money_bonds" id="start_money_bonds_1" value="1" >
                                มีเงินค้ำประกัน
                            </div>
                            <div class="col-md-offset-3 col-sm-6" style="padding-top:6px">
                                <input type="radio" onclick="moneyBondHave(0)"  <?php echo ($bmModel['start_money_bonds'] == 0) ? 'checked':'' ?> name="start_money_bonds" id="start_money_bonds_2" value="0" >
                                ไม่มีเงินค้ำประกัน
                            </div>
                        </div>
                        <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone':'';?>">
                             <div  class="col-sm-6" style="padding-top:6px" >
                                <?php echo ($bmModel['start_money_bonds'] == 0) ? 'มีเงินค้ำประกัน':'ไม่มีเงินค้ำประกัน' ?>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group bond-series">
                        <label for="imageEmp" class="col-sm-3 control-label">การชำระเงินค้ำประกัน</label>
                        <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone':'';?>">
                             <div class="col-sm-6" style="padding-top:6px">
                                <label onclick="toggleDivPayType(1)" for="start_pay_bonds_1" style="font-weight:normal !important;">
                                    <input type="radio" <?php echo ($bmModel['start_pay_bonds'] == 1) ? 'checked':'' ?> name="start_pay_bonds" id="start_pay_bonds_1" value="1" >
                                    ชำระครบ
                                </label>
                            </div>
                            <div class="col-sm-6" style="padding-top:6px">
                                <label onclick="toggleDivPayType(0)" for="start_pay_bonds_2" style="font-weight:normal !important;">
                                    <input type="radio" <?php echo ($bmModel['start_pay_bonds'] == 2) ? 'checked':'' ?>  name="start_pay_bonds" id="start_pay_bonds_2" value="2" >
                                    ผ่อนชำระรายเดือน
                                </label>
                            </div>
                        </div>
                        <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone':'';?>">
                            <div  class="col-sm-6" style="padding-top:6px" >
                                <?php echo ($bmModel['start_pay_bonds'] == 1) ? 'ชำระครบ':'ผ่อนชำระรายเดือน' ?>
                            </div>
                        </div>
                       
                    </div>
                    <div class="form-group  bond-series">
                            <label for="imageEmp" class="col-sm-3 control-label">เงินค้ำประกัน</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="money_bonds" 
                                    name="money_bonds" 
                                    value="<?php echo $bmModel['money_bonds'];?>"
                                       maxlength="6"
                                />
                            </div>
                            <div class="col-sm-1  control-label">บาท</div>
                        </div>
                    <div class="bond-series" style="display:<?php echo ($bmModel['start_pay_bonds'] == 1)? 'none':'block'?>;" id="paymentTypeDiv">
                        
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">เงินค้ำประกันจ่ายครั้งแรก</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="first_money" 
                                    name="first_money" 
                                    value="<?php echo $bmModel['first_money'];?>"   
                                />
                            </div>
                            <div class="col-sm-1  control-label">บาท</div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ผ่อนชำระเดือนละ</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="pay_money" 
                                    name="pay_money" 
                                    value="<?php echo $bmModel['pay_money'];?>"   
                                />
                            </div>
                            <div class="col-sm-1 control-label">บาท</div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ตั้งแต่เดือน</label>
                            <div class="col-sm-6">
                    

                                <?php if(!$optional['viewOnly']){?>
                                    <div class="input-group date">
                                        <input type="text"  readonly="readonly" class="form-control" 
                                            id="start_date_pay" 
                                            name="start_date_pay" 
                                            value="<?php echo PersonalController::toggleToDataPicker($bmModel['start_date_pay'])?>" 
                                            placeholder="dd/mm/yyyy" />

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                <?php }else{?>
                                    <?php echo PersonalController::toggleToDataPicker($bmModel['start_date_pay'])?>
                                <?php }?>



                            </div>
                        </div>
                         <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ถึงเดือน</label>
                            <div class="col-sm-6">
               
                                <?php if(!$optional['viewOnly']){?>
                                    <div class="input-group date">
                                        <input type="text"  readonly="readonly" class="form-control" 
                                            id="end_date_pay" 
                                            name="end_date_pay" 
                                            value="<?php echo PersonalController::toggleToDataPicker($bmModel['end_date_pay'])?>" 
                                            placeholder="dd/mm/yyyy" />

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                <?php }else{?>
                                    <?php echo PersonalController::toggleToDataPicker($bmModel['end_date_pay'])?>
                                <?php }?>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                               <button type="button" class="btn btn-primary btn-sm" onclick="showMoneybondsList()">
                                <i class="glyphicon glyphicon-th-list"></i> 
                                ประวัติการหักเงินค้ำประกัน
                               </button>
                            </div>
                        </div>

                    </div>
                   
                    <div class="form-group bond-series">
                        <label for="imageEmp" class="col-sm-3 control-label">ธนาคาร</label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bank_name" 
                                name="bank_name" 
                                value="<?php echo $bmModel['bank_name'];?>"   
                            />
                        </div>
                    </div>
                    <div class="form-group  bond-series">
                        <label for="imageEmp" class="col-sm-3 control-label">หมายเลขบัญชี</label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bank_no" 
                                name="bank_no" 
                                value="<?php echo $bmModel['bank_no'];?>"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!--ข้อมูลผู้ค้ำประกัน-->
    <div class="col-md-6">
        <form class="form-horizontal">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">ข้อมูลผู้ค้ำประกัน</h3>
                </div>
                <div class="box-body">

                  
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">เพศ</label>
                        <div class="<?php echo ($optional['viewOnly']) ? 'empDisplayNone':'';?>">
                             <div class="col-sm-6" style="padding-top:6px">
                                <input onclick="relativeGetGender(1,'#bondsman_prefix')"   type="radio" <?php echo ($bmModel['bondsman_sex'] == 1) ? 'checked':'';  ?> name="bondsman_sex" value="1" >
                                ชาย
                                <input onclick="relativeGetGender(2,'#bondsman_prefix')"  type="radio" <?php echo ($bmModel['bondsman_sex'] == 2) ? 'checked':'';  ?>  name="bondsman_sex" value="2"  style="margin-left:8px" >
                                หญิง
                            </div>
                        </div>
                        <div class="<?php echo (!$optional['viewOnly']) ? 'empDisplayNone':'';?>">
                            <div class="col-sm-6" style="padding-top:6px">
                                <?php echo ($bmModel['bondsman_sex'] == 1) ? 'ชาย':'หญิง';  ?>
                            </div>
                        </div>
                       
                    </div>
                     <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">คำนำหน้า</label>
                        <div class="col-sm-6">
                       
                           <select 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['disabled'];?>  
                                id="bondsman_prefix" 
                                name="bondsman_prefix" >
                                <option value="" >เลือกคำนำหน้า</option>
                                <?php echo $optional['option_bename_th'];?>
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">ชื่อ<span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bondsman_name" 
                                name="bondsman_name" 
                                value="<?php echo $bmModel['bondsman_name'];?>"   
                            />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">สกุล<span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bondsman_surname" 
                                name="bondsman_surname" 
                                value="<?php echo $bmModel['bondsman_surname'];?>"   
                            />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">หมายเลขบัตรประชาชน<span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bondsman_idcard" 
                                name="bondsman_idcard" 
                                value="<?php echo $bmModel['bondsman_idcard'];?>"   
                            />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">วัน เดือน ปีเกิด<span style="color:red">*</span></label>
                        <div class="col-sm-6">
                           

                            <?php if(!$optional['viewOnly']){?>
                                <div class="input-group date">
                                    <input type="text"  readonly="readonly" class="form-control" 
                                        id="bondsman_birthday" 
                                        name="bondsman_birthday" 
                                        value="<?php echo PersonalController::toggleToDataPicker($bmModel['bondsman_birthday'])?>" 
                                        placeholder="dd/mm/yyyy" 
                                        onchange="calDateToDay(this,'#bondsmanAge')"/>

                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            <?php }else{?>
                                <?php echo PersonalController::toggleToDataPicker($bmModel['bondsman_birthday'])?>
                            <?php }?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">อายุ</label>
                        <div class="col-sm-2">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bondsmanAge" 
                                disabled
                                value=""   
                            />
                        </div>
                        <div class="col-sm-1 control-label">ปี</div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">อาชีพ</label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bondsman_business" 
                                name="bondsman_business" 
                                value="<?php echo $bmModel['bondsman_business'];?>"   
                            />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">ที่อยู่ที่ทำงาน</label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bondsman_work_place" 
                                name="bondsman_work_place" 
                                value="<?php echo $bmModel['bondsman_work_place'];?>"   
                            />
                        </div>
                        <div class="col-sm-3">Ex.เทศบาลเชียงราย</div>
                    </div>
                    <div class="form-group">
                        <label for="imageEmp" class="col-sm-3 control-label">จำนวนปีที่ทำงาน</label>
                        <div class="col-sm-6">
                            <input type="text" 
                                class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                id="bondsman_year" 
                                name="bondsman_year" 
                                value="<?php echo $bmModel['bondsman_year'];?>"   
                            />
                        </div>
                    </div>
                    <div class="bondsman_tel_contends">
                        <?php if(empty($bmTel)){ ?>
                        <div class="form-group bondsman_tel_list main">
                            <label for="imageEmp" class="col-sm-3 control-label">เบอร์โทร</label>
                            <div class="col-sm-4">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="" 
                                    value=""
                                       data-inputmask="'mask': ['099-999-9999']" data-mask/>
                            </div>
                            <div class="col-sm-3 control-label  <?php echo ($optional['viewOnly']) ? 'empDisplayNone':'';?>" style="text-align:left">
                                <input type="radio" value="1" name="bondsman_sel_main_tel" >
                                เบอร์หลัก
                            </div>
                            
                        </div>
                        <?php } ?>
                        
                        <?php foreach($bmTel as $no => $mTel){ ?>

                        <div class="form-group bondsman_tel_list main">
                            <label for="imageEmp" class="col-sm-3 control-label">เบอร์โทร</label>
                            <div class="col-sm-4">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="" 
                                    value="<?php echo $mTel->tel;?>"   
                                />
                            </div>

                            <div class="col-sm-3 control-label <?php echo ($optional['viewOnly']) ? 'empDisplayNone':'';?>" style="text-align:left">
                                <input type="radio" <?php echo ($mTel->main_active == 1) ? 'checked':'' ?> value="1" name="bondsman_sel_main_tel" >
                                เบอร์หลัก
                            </div>
                             <div class="col-sm-3 control-label <?php echo (!$optional['viewOnly']) ? 'empDisplayNone':'';?>" style="text-align:left">
                               <?php echo ($mTel->main_active == 1) ? 'เบอร์โทรหลัก':'' ?>
                            </div>
                            
                        </div>

                        <?php }?>

                    </div>
                    <div class="form-group <?php echo ($optional['viewOnly']) ? 'empDisplayNone':'';?>" style="text-align:center">
                       
                        <button type="button" class="btn btn-primary btn-sm" onclick="boundsmanAddTelForm()"><i class="glyphicon glyphicon-plus"></i> เพิ่มเบอร์โทร</button>
                    </div>
                    <div id="bondsmanFormRef" style="display:none">
                        <div class="form-group bondsman_tel_list" >
                            <label for="imageEmp" class="col-sm-3 control-label">เบอร์โทร</label>
                            <div class="col-sm-4">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="" 
                                    value=""
                                       data-inputmask="'mask': ['099-999-9999']" data-mask
                                />
                            </div>
                            <div class="col-sm-5" style="">
                                <label class="control-label">
                                    <input type="radio" value="1" name="bondsman_sel_main_tel" >
                                    เบอร์หลัก
                                </label>
                                <span class="" style="">
                                    <button type="button" class="btn btn-warning btn-sm p" onclick="boundsmanDelTelForm(this)"><i class="glyphicon glyphicon-minus"></i> ลบ</button>
                                </span>
                            </div>
                            
                        </div>
                    </div>
                    


                </div>
            </div>
        </form>
    </div>
</div>
<div class="row>">

    <!--ที่อยู่ตามบัตรประชาชนผู้ค้ำประกัน-->
    <div class="col-md-6">
        <form class="form-horizontal">
            <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ที่อยู่ตามบัตรประชาชนผู้ค้ำประกัน</h3>
                    </div>
                    <div class="box-body" id="bonds_idcardaddr">
                
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">บ้านเลขที่</label>
                            <div class="col-sm-4">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_address" 
                                    name="addr_idcard_address" 
                                    value="<?php echo $bmModel['addr_idcard_address'];?>"   
                                />
                            </div>
                            <label for="imageEmp" class="col-sm-2 control-label">หมู่ที่</label>
                            <div class="col-sm-2">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_moo" 
                                    name="addr_idcard_moo" 
                                    value="<?php echo $bmModel['addr_idcard_moo'];?>"   
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">หมู่บ้าน</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_village" 
                                    name="addr_idcard_village" 
                                    value="<?php echo $bmModel['addr_idcard_village'];?>"   
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ตรอก/ซอย</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_lane" 
                                    name="addr_idcard_lane" 
                                    value="<?php echo $bmModel['addr_idcard_lane'];?>"   
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ถนน</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_road" 
                                    name="addr_idcard_road" 
                                    value="<?php echo $bmModel['addr_idcard_road'];?>"   
                                />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">ตำบล</label>
                            <div class="col-sm-6">
                                <!--<select class="home_addr-select2-ajax form-control"  id="selTumbon" style="width: 100% !important;"></select>-->
                                <input type="text" id="addr_idcard_district" name="addr_idcard_district"
                                       class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>
                                       value="<?php echo $bmModel['addr_idcard_district'];?>" readonly="readonly">
                            </div>
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="btnTBondsHomeAddr" data-title="ค้นหาตำบล" type="button" data-placement="bottom" data-toggle="popover"  data-container="body"  data-html="true"><i class="fa fa-search"></i> </button>
                            </span>
                        </div>

                       <!-- <div class="form-group">
                            <label for="" class="col-sm-3 control-label">ตำบล</label>
                            <div class="col-sm-6">
                                <select class="bonds_idcardaddr-select2-ajax form-control"  style="width: 100% !important;"></select>
                                <input type="hidden" id="addr_idcard_district" name="addr_idcard_district">
                            </div>
                        </div>-->
                        <!--
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ตำบล</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_district" 
                                    name="addr_idcard_district" 
                                    value="<?php echo $bmModel['addr_idcard_district'];?>"   
                                />
                            </div>
                        </div>
                        -->
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">อำเภอ</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_amphur" 
                                    name="addr_idcard_amphur" 
                                    value="<?php echo $bmModel['addr_idcard_amphur'];?>"
                                       readonly="readonly"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">จังหวัด</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_province" 
                                    name="addr_idcard_province" 
                                    value="<?php echo $bmModel['addr_idcard_province'];?>"
                                       readonly="readonly"
                                />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">รหัสไปรษณีย์</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_idcard_postcode" 
                                    name="addr_idcard_postcode" 
                                    value="<?php echo $bmModel['addr_idcard_postcode'];?>"
                                       maxlength="5" onkeypress="return onlyNumber(event);"
                                />
                            </div>
                        </div>

                    </div>
                </div>
        
        </form>
    </div>

    <!--ที่อยู่ตามสถานที่ทำงานผู้ค้ำประกัน-->
    <div class="col-md-6">
        <form class="form-horizontal">
            <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ที่อยู่ตามสถานที่ทำงานผู้ค้ำประกัน</h3>
                    </div>
                    <div class="box-body" id="bonds_workaddr">
                
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">บ้านเลขที่</label>
                            <div class="col-sm-4">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_address" 
                                    name="addr_plance_address" 
                                    value="<?php echo $bmModel['addr_plance_address'];?>"   
                                />
                            </div>
                            <label for="imageEmp" class="col-sm-2 control-label">หมู่ที่</label>
                            <div class="col-sm-2">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_moo" 
                                    name="addr_plance_moo" 
                                    value="<?php echo $bmModel['addr_plance_moo'];?>"   
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">หมู่บ้าน</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_village" 
                                    name="addr_plance_village" 
                                    value="<?php echo $bmModel['addr_plance_village'];?>"   
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ตรอก/ซอย</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_lane" 
                                    name="addr_plance_lane" 
                                    value="<?php echo $bmModel['addr_plance_lane'];?>"   
                                />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ถนน</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_road" 
                                    name="addr_plance_road" 
                                    value="<?php echo $bmModel['addr_plance_road'];?>"   
                                />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">ตำบล</label>
                            <div class="col-sm-6">

                                <input type="text" id="addr_plance_district" name="addr_plance_district"
                                       class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?>
                                       value="<?php echo $bmModel['addr_plance_district'];?>" readonly="readonly">
                            </div>
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="btnTBondsWorkAddr" data-title="ค้นหาตำบล" type="button" data-placement="bottom" data-toggle="popover"  data-container="body"  data-html="true"><i class="fa fa-search"></i> </button>
                            </span>
                        </div>

                       <!-- <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">ตำบล</label>

                            <div class="col-sm-6">
                                <select class="bonds_workaddr-select2-ajax form-control"  style="width: 100% !important;"></select>
                                <input type="hidden" id="addr_plance_district" name="addr_plance_district">
                            </div>
                            <!--
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php /*echo $optional['dontouch'];*/?>" <?php /*echo $optional['readonly'];*/?>
                                    id="addr_plance_district" 
                                    name="addr_plance_district" 
                                    value="<?php /*echo $bmModel['addr_plance_district'];*/?>"
                                />
                            </div>

                        </div>-->
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">อำเภอ</label>
                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_amphur" 
                                    name="addr_plance_amphur" 
                                    value="<?php echo $bmModel['addr_plance_amphur'];?>"
                                       readonly="readonly"
                                />
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">จังหวัด</label>

                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_province" 
                                    name="addr_plance_province" 
                                    value="<?php echo $bmModel['addr_plance_province'];?>"
                                       readonly="readonly"
                                />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="imageEmp" class="col-sm-3 control-label">รหัสไปรษณีย์</label>

                            <div class="col-sm-6">
                                <input type="text" 
                                    class="form-control <?php echo $optional['dontouch'];?>" <?php echo $optional['readonly'];?> 
                                    id="addr_plance_postcode" 
                                    name="addr_plance_postcode" 
                                    value="<?php echo $bmModel['addr_plance_postcode'];?>"
                                       maxlength="5" onkeypress="return onlyNumber(event);"
                                />
                            </div>
                        </div>

                    </div>
                </div>
        
        </form>
    </div>
</div>

<div class="modal fade modal-wide" id="modalMoneybondslists" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">ประวัติการหักเงินค้ำประกัน</h4>
            </div>
            <div class="modal-body" width="100%" >
                <div id="moneybondsTable">
                    <center><h2>รอสักครู่..</h2></center>
                </div>    
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  