<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:27
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
$selectProgram = ApiHr::selectProgram();
$selectworking = ApiHr::getWorking_company();
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/permission.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/js/jquery.sumoselect.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/permission/sumoselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li class="active">รายงานสิทธิ์การใช้โปรแกรม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
        <form  onsubmit="return Search()" data-toggle="validator" id='seartFrom'>
             <div class="col-md-1">
                &nbsp;
            </div>
            <div class="col-md-3">
                
            </div>
            <div class="col-md-3">
                 <div class="form-group">
                        <label for="numberPassportEmp"  class="col-sm-4 control-label">โปรแกรม</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="programname" id="programid" required>
                                        <option value="">เลือกบริษัท</option>
                                            <?php 
                                                foreach ($selectProgram as $value) {
                                                        echo '<option value="' . $value['id']. '">' . $value['name'] . '</option>';
                                        } ?>
                                </select>
                            </div>
                    </div>
            </div>
            <div class="col-md-3">
            
            </div>
            <br>
            <div class="row" style="text-align:center">
            </div>
            <br>
            <div class="row">
                <div class="col-md-1">
                    &nbsp;
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp"  class="col-sm-4 control-label">บริษัท</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="selectworking" id="selectworking" onchange="getCompanyForDepartment(this);" required>
                                        <option value="">เลือกบริษัท</option>
                                            <?php 
                                                foreach ($selectworking as  $value) {
                                                        echo '<option value="' . $value['id']. '">' . $value['name'] . '</option>';
                                        } ?>
                                </select>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="numberPassportEmp"  class="col-sm-3 control-label">แผนก</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="selectdepartment" id="selectdepartment" onchange="getDepartmentForSection(this);" required>
                                    <option value=""> เลือกแผนก </option>
                                </select>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                <div class="form-group">
                    <label for="numberPassportEmp"  class="col-sm-3 control-label">ฝ่าย</label>
                        <div class="col-sm-9">
                        
                            <select class="form-control" name="selectsection" id="selectsection" required>
                                <!--<option value=""> เลือกฝ่าย </option>-->
                            </select>
                         
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row" style="text-align:center">
                <input type="submit" class="btn btn-primary btn-md" value="ค้นหา">
                <input type="reset" class="btn btn-warning btn-md" value="ล้าง">
            </div>
    </form>

    <!-- /.box -->
</section><!-- /.content -->
<div id='dataserat'>
</div>