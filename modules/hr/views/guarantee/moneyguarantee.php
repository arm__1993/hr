<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 16:12
 */




use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;




?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li>เพิ่มข้อมูลพนักงาน</li>
                <li class="active">ข้อมูลเงินค้ำประกัน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <form class="form-horizontal">
                        <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">ข้อมูลเงินค้ำประกัน</h3>
                                </div>
                                <div class="box-body">
                                </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form class="form-horizontal">
                        <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">ข้อมูลผู้ค้ำประกัน</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group">
                                                <label for="genderEmp" class="col-sm-2 control-label">เพศ</label>
                                                  <div class="col-sm-6">
                                                  <input type="radio" name="genderEmp" id="genderEmp" value="ชาย" >
                                                  ชาย
                                                  <input type="radio" name="genderEmp" id="genderEmp" value="หญิง" >
                                                  หญิง
                                                  </div>
                                        </div>
                                         <div class="form-group">
                                                <label  class="col-sm-2 control-label">คำนำหน้า</label>
                                                  <div class="col-sm-3">
                                                    <select class="form-control">
                                                      <option>option 1</option>
                                                    </select>
                                                  </div>
                                        </div>
                                        <div class="form-group">
                                                    <label for="nameEmp" class="col-sm-2 control-label">ชื่อ<font color="red">*</font></label>
                                                    <div class="col-sm-4">
                                                      <input type="text" class="form-control" id="nameEmp" size="13">
                                                    </div>
                                        </div>
                                        <div class="form-group">
                                                    <label for="lastNameEmp" class="col-sm-2 control-label">นามสกุล<font color="red">*</font></label>
                                                    <div class="col-sm-4">
                                                      <input type="text" class="form-control" id="lastNameEmp"  size="13">
                                                    </div>
                                        </div>
                                        <div class="form-group">
                                                  <label for="genderEmp" class="col-sm-2 control-label">หมายเลขบัตรประชาชน</label>
                                                    <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="genderEmp" id="genderEmp"  >
                                                    </div>
                                        </div>
                                        <div class="form-group">
                                                    <label for="birthdayEmp" class="col-sm-2 control-label">วัน เดือน ปีเกิด</label>
                                                    <div class="col-sm-4">
                                                      <input type="date" class="form-control" id="birthdayEmp" size="10" onchange="selectBirthdayEmp(this)">
                                                    </div>
                                        </div>
                                         <div class="form-group">
                                                    <label for="lastNameEmp" class="col-sm-2 control-label">อาชีพ<font color="red">*</font></label>
                                                    <div class="col-sm-4">
                                                      <input type="text" class="form-control" id="lastNameEmp"  size="13">
                                                    </div>
                                        </div>
                                         <div class="form-group">
                                                    <label for="lastNameEmp" class="col-sm-2 control-label">ที่อยู่ที่ทำงาน</label>
                                                    <div class="col-sm-4">
                                                      <input type="text" class="form-control" id="lastNameEmp"  size="13">
                                                    </div>
                                        </div>
                                        <div class="form-group">
                                                    <label for="lastNameEmp" class="col-sm-2 control-label">จำนวนปีที่ทำงาน</label>
                                                    <div class="col-sm-4">
                                                      <input type="text" class="form-control" id="lastNameEmp"  size="13">
                                                    </div>
                                        </div>
                                         <div class="form-group">
                                                    <label for="lastNameEmp" class="col-sm-2 control-label">เบอร์โทร<font color="red">*</font></label>
                                                    <div class="col-sm-4">
                                                      <input type="text" class="form-control" id="lastNameEmp"  size="13">
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                        <h3 class="box-title">ที่อยู่ตามบัตรประชาชนผู้ค้ำประกัน</h3>
                        </div>
                        <div class="box-body">
                                
                                <div class="form-group">
                                    <label for="imageEmp" class="col-sm-2 control-label">บ้านเลขที่</label>

                                    <div class="col-sm-2">
                                    <input type="text"  name="fileEmp" class="form-control" id="imageEmp" size="3">
                                    </div>
                                    <label for="imageEmp" class="col-sm-2 control-label">หมู่</label>

                                    <div class="col-sm-2">
                                    <input type="text"  name="fileEmp" class="form-control" id="imageEmp" size="3">
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">หมู่บ้าน</label>

                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" >
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">ตรอก/ซอย</label>

                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" >
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">ถนน</label>

                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" >
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">ตำบล</label>

                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" >
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">อำเภอ</label>

                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" >
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">จังหวัด</label>

                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" >
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">รหัสไปรษณีย์</label>

                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" >
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ที่อยู่ตามสถานที่ทำงานผู้ค้ำประกัน</h3>
                    </div>
                    <div class="box-body">
                            <div class="form-group">
                                <label for="imageEmp" class="col-sm-2 control-label">บ้านเลขที่</label>

                                <div class="col-sm-2">
                                <input type="text"  name="fileEmp" class="form-control" id="imageEmp" size="3">
                                </div>
                                <label for="imageEmp" class="col-sm-2 control-label">หมู่</label>

                                <div class="col-sm-2">
                                <input type="text"  name="fileEmp" class="form-control" id="imageEmp" size="3">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">หมู่บ้าน</label>

                                <div class="col-sm-6">
                                <input type="text" class="form-control" id="" >
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">ตรอก/ซอย</label>

                                <div class="col-sm-6">
                                <input type="text" class="form-control" id="" >
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">ถนน</label>

                                <div class="col-sm-6">
                                <input type="text" class="form-control" id="" >
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">ตำบล</label>

                                <div class="col-sm-6">
                                <input type="text" class="form-control" id="" >
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">อำเภอ</label>

                                <div class="col-sm-6">
                                <input type="text" class="form-control" id="" >
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">จังหวัด</label>

                                <div class="col-sm-6">
                                <input type="text" class="form-control" id="" >
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">รหัสไปรษณีย์</label>

                                <div class="col-sm-6">
                                <input type="text" class="form-control" id="" >
                                </div>
                            </div>
                    </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->