<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "education".
 *
 * @property integer $db_log_id
 * @property string $db_do_time
 * @property string $db_do_by
 * @property string $db_do_log
 * @property string $db_do_sql
 * @property integer $id
 * @property string $emp_id
 * @property string $edu_level
 * @property string $edu_school
 * @property string $edu_major
 * @property string $edu_finish
 * @property string $edu_GPA
 * @property integer $status
 */
class Education extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'education';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['db_log_id', 'status'], 'integer'],
            [['db_do_time'], 'safe'],
            [['db_do_sql', 'edu_school', 'edu_major'], 'string'],
            [['db_do_by'], 'string', 'max' => 20],
            [['db_do_log'], 'string', 'max' => 200],
            [['emp_id'], 'string', 'max' => 13],
            [['edu_level'], 'string', 'max' => 250],
            [['edu_finish', 'edu_GPA'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'db_log_id' => 'Db Log ID',
            'db_do_time' => 'Db Do Time',
            'db_do_by' => 'Db Do By',
            'db_do_log' => 'Db Do Log',
            'db_do_sql' => 'Db Do Sql',
            'id' => 'ID',
            'emp_id' => 'Emp ID',
            'edu_level' => 'Edu Level',
            'edu_school' => 'Edu School',
            'edu_major' => 'Edu Major',
            'edu_finish' => 'Edu Finish',
            'edu_GPA' => 'Edu  Gpa',
            'status' => 'Status',
        ];
    }
}
