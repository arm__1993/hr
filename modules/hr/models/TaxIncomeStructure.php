<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tax_income_structure".
 *
 * @property integer $id
 * @property string $income_floor
 * @property string $income_ceil
 * @property string $tax_rate
 * @property string $gross_step
 * @property string $max_accumulate
 * @property integer $from_year
 * @property integer $to_year
 * @property string $remark
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class TaxIncomeStructure extends \yii\db\ActiveRecord
{
    protected $_pageSize;

    function __construct() {
        $this->_pageSize = Yii::$app->params['PAGE_SIZE'];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_income_structure';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['income_floor', 'income_ceil', 'tax_rate', 'from_year'], 'required'],
            [['income_floor', 'income_ceil', 'tax_rate', 'gross_step', 'max_accumulate'], 'number'],
            [['from_year', 'to_year', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['remark'], 'string', 'max' => 255],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'income_floor' => 'Income Floor',
            'income_ceil' => 'Income Ceil',
            'tax_rate' => 'Tax Rate',
            'gross_step' => 'Gross Step',
            'max_accumulate' => 'Max Accumulate',
            'from_year' => 'From Year',
            'to_year' => 'To Year',
            'remark' => 'Remark',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return TaxIncomeStructureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaxIncomeStructureQuery(get_called_class());
    }


    public function search($params)
    {

        $query = TaxIncomeStructure::find()->where(['!=','status_active','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'income_floor', $this->income_floor])
            ->andFilterWhere(['like', 'income_ceil', $this->income_ceil])
            ->andFilterWhere(['like', 'tax_rate', $this->tax_rate])
            ->andFilterWhere(['like', 'gross_step', $this->gross_step])
            ->andFilterWhere(['like', 'max_accumulate', $this->max_accumulate])
            ->andFilterWhere(['like', 'from_year', $this->from_year])
            ->andFilterWhere(['like', 'to_year', $this->to_year]);

        return $dataProvider;
    }




}
