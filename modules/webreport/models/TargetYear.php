<?php

namespace app\modules\webreport\models;

use Yii;

/**
 * This is the model class for table "target_year".
 *
 * @property integer $id
 * @property integer $months
 * @property integer $years
 * @property string $target
 * @property string $user_edit
 * @property string $create_at
 * @property string $update_at
 */
class TargetYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'target_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['months', 'years'], 'integer'],
            [['target'], 'number'],
            [['create_at', 'update_at'], 'safe'],
            [['user_edit'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'months' => 'Months',
            'years' => 'Years',
            'target' => 'Target',
            'user_edit' => 'User Edit',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
