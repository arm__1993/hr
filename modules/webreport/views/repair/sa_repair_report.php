
<?php

/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 13/10/2559
 * Time: 16:30
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use app\api\ApiReport;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiRepair;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);


$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานยอดรับแจ้งซ่อม SA รายคนตามใบงานซ่อมต่อเดือน</h3>


<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">

        <div class="row">

            <form method="post" action="receive_sa_repair_report">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                <div class="col-md-12">
                    <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key=>$value) {
                                    $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>เลือกเดือน</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="Start_Month" id="Start_Month" style="width: 150px">
                                <option value="">เลือกเดือน</option>
                                <?php
                                for ($i=1 ; $i<=12 ; $i++ ){
                                    $sel = ($i==$selected_startmonth) ? 'selected="selected"': '';
                                    echo '<option value="'.$i.'" '.$sel.'>'.\app\api\DateTime::convertMonth($i).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>ถึงเดือน</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="End_Month" id="End_Month" style="width: 150px">
                                <option value="">เลือกเดือน</option>
                                <?php
                                for ($i=1 ; $i<=12 ; $i++ ){
                                    $sel = ($i==$selected_endmonth) ? 'selected="selected"': '';
                                    echo '<option value="'.$i.'" '.$sel.'>'.\app\api\DateTime::convertMonth($i).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>เลือกปี</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="year" id="year">
                                <?php
                                $get_year = ApiReport::getAppYear();
                                foreach ($get_year as $key=>$value) {
                                    $sel = ($selected_year==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </center>

                    <center><br>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-fw fa-search"></i>ค้นหา
                            </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                            <a href="<?php echo SITE_URL;?>/repair/btn_reportsar">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                                </button>
                            </a>
                        </div>
                    </center>
                </div>
                <!--/col-md-12-->
            </form>
        </div>
        <!--/row-->
    </div>
    <!--/box-body-->

    <?php if($query) { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- AREA TABLE -->
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1" data-toggle="tab">
                                                    งานเช็คระยะ
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_2" data-toggle="tab">
                                                    งานเช็คระยะ+งานซ่อมทั่วไป
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_3" data-toggle="tab">
                                                    งานซ่อมทั่วไป
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                            ชื่อ SA
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                                                            จำนวนคันรับแจ้งซ่อม
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                            เฉลี่ยต่อวัน
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            รวมเวลารับแจ้งซ่อม (นาที)
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                            เวลาเฉลี่ยต่อคัน (นาที)
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <?php if(!empty($dataSa_Wage_Distance_Only)){
                                                        $countdataSa_Wage_Distance_Only = count($dataSa_Wage_Distance_Only);

                                                        $sum_countcarA = [];
                                                        $sum_avgAmountcarPerDayA =[];
                                                        $sum_TimeA = [];
                                                        $sum_avgTimePerAmountcarA = []

                                                        ?>
                                                        <?php
                                                         $sumcountday = ApiRepair::countDayOfMonthStartStop($selected_year,$selected_startmonth,$selected_year,$selected_endmonth);
                                                        ?>
                                                        <tbody>
                                                        <?php foreach ($dataSa_Wage_Distance_Only as $valueDataSa_Wage){?>
                                                            <tr role="row">
                                                                <td><?php echo $valueDataSa_Wage['Name'] ;?>
                                                                    &nbsp&nbsp&nbsp
                                                                    <?php echo $valueDataSa_Wage['Surname'] ; ?></td>
                                                                <td><?php echo  $sum_countcarA[] = $CountA = $valueDataSa_Wage['Count_Car'] ;?></td>
                                                                <td><?php  $sum_avgAmountcarPerDayA[] = $coutCarAvgA = $valueDataSa_Wage['Count_Car']/$sumcountday;
                                                                    echo   Helper::displayDecimal($coutCarAvgA);
                                                                ?>
                                                                </td>
                                                                <td><?php    $sum_TimeA[] = $TimeA =$valueDataSa_Wage['SumDifftime'];
                                                                    $getTimeA = DateTime::calculateTimeElapsed($TimeA);
                                                                    echo  ApiRepair::getFormatTime($getTimeA['h'],$getTimeA['m'],$getTimeA['s']);
                                                                    ?>
                                                                </td>
                                                                <td><?php   $AvgTimePercar = ApiRepair::avgTimePerCar($TimeA,$CountA);
                                                                    $sum_avgTimePerAmountcarA[] =  $secAvgA =  Helper::displayDecimal($AvgTimePercar);
                                                                      $getFormatTimeA = DateTime::calculateTimeElapsed($secAvgA);
                                                                    echo  ApiRepair::getFormatTime($getFormatTimeA['h'],$getFormatTimeA['m'],$getFormatTimeA['s']);
                                                                     ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>

                                                        <tfoot>
                                                        <tr>
                                                            <th>รวม</th>
                                                            <td><?php echo array_sum($sum_countcarA); ?></td>
                                                            <td><?php  $TotalSum_avgAmountcarPerDayA = array_sum($sum_avgAmountcarPerDayA);
                                                                echo   Helper::displayDecimal($TotalSum_avgAmountcarPerDayA);
                                                            ?></td>
                                                            <td><?php  $TotalSum = array_sum($sum_TimeA);
                                                                 $getTotalFormatTimeA = DateTime::calculateTimeElapsed($TotalSum);
                                                                 echo ApiRepair::getFormatTime($getTotalFormatTimeA['h'],$getTotalFormatTimeA['m'],$getTotalFormatTimeA['s']);
                                                            ?></td>
                                                            <td><?php  $sum_avgTimePerAmountcarA = array_sum($sum_avgTimePerAmountcarA);
                                                                $sum_avgTimePerAmountcarA = DateTime::calculateTimeElapsed($sum_avgTimePerAmountcarA);
                                                                echo ApiRepair::getFormatTime($sum_avgTimePerAmountcarA['h'],$sum_avgTimePerAmountcarA['m'],$sum_avgTimePerAmountcarA['s']);
                                                            ?></td>
                                                        </tr>
                                                        </tfoot>
                                                    <?php }?>
                                                </table>
                                            </div>
                                            <!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                            ชื่อ SA
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                                                            จำนวนคันรับแจ้งซ่อม
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                            เฉลี่ยต่อวัน
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            รวมเวลารับแจ้งซ่อม (นาที)
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                            เวลาเฉลี่ยต่อคัน (นาที)
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <?php if(!empty($dataSa_Wage_intersect)){
                                                        $countdataSa_Wage_intersect = count($dataSa_Wage_intersect);

                                                        $sum_countcarB = [];
                                                        $sum_avgBmountcarPerDayB =[];
                                                        $sum_TimeB = [];
                                                        $sum_avgTimePerBmountcarB = []

                                                        ?>
                                                        <?php
                                                        $sumcountday = ApiRepair::countDayOfMonthStartStop($selected_year,$selected_startmonth,$selected_year,$selected_endmonth);
                                                        ?>
                                                        <tbody>
                                                        <?php foreach ($dataSa_Wage_intersect as $valueDataSa_Wage){?>
                                                            <tr role="row">
                                                                <td><?php echo $valueDataSa_Wage['Name'] ;?>
                                                                    &nbsp&nbsp&nbsp
                                                                    <?php echo $valueDataSa_Wage['Surname'] ; ?></td>
                                                                <td><?php echo  $sum_countcarB[] = $CountB = $valueDataSa_Wage['Count_Car'] ;?></td>
                                                                <td><?php  $sum_avgBmountcarPerDayB[] = $coutCarBvgB = $valueDataSa_Wage['Count_Car']/$sumcountday;
                                                                    echo   Helper::displayDecimal($coutCarBvgB);
                                                                    ?>
                                                                </td>
                                                                <td><?php    $sum_TimeB[] = $TimeB =$valueDataSa_Wage['SumDifftime'];
                                                                    $getTimeB = DateTime::calculateTimeElapsed($TimeB);
                                                                    echo  ApiRepair::getFormatTime($getTimeB['h'],$getTimeB['m'],$getTimeB['s']);
                                                                    ?>
                                                                </td>
                                                                <td><?php   $BvgTimePercar = ApiRepair::avgTimePerCar($TimeB,$CountB);
                                                                    $sum_avgTimePerBmountcarB[] =  $secBvgB =  Helper::displayDecimal($BvgTimePercar);
                                                                    $getFormatTimeB = DateTime::calculateTimeElapsed($secBvgB);
                                                                    echo  ApiRepair::getFormatTime($getFormatTimeB['h'],$getFormatTimeB['m'],$getFormatTimeB['s']);
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>

                                                        <tfoot>
                                                        <tr>
                                                            <th>รวม</th>
                                                            <td><?php echo array_sum($sum_countcarB); ?></td>
                                                            <td><?php  $TotalSum_avgBmountcarPerDayB = array_sum($sum_avgBmountcarPerDayB);
                                                                echo   Helper::displayDecimal($TotalSum_avgBmountcarPerDayB);
                                                                ?></td>
                                                            <td><?php   $TotalSum = array_sum($sum_TimeB);
                                                                 $getTotalFormatTimeB = DateTime::calculateTimeElapsed($TotalSum);
                                                                echo ApiRepair::getFormatTime($getTotalFormatTimeB['h'],$getTotalFormatTimeB['m'],$getTotalFormatTimeB['s']);
                                                                ?></td>
                                                            <td><?php  $sum_avgTimePerBmountcarB = array_sum($sum_avgTimePerBmountcarB);
                                                                $sum_avgTimePerBmountcarB = DateTime::calculateTimeElapsed($sum_avgTimePerBmountcarB);
                                                                echo ApiRepair::getFormatTime($sum_avgTimePerBmountcarB['h'],$sum_avgTimePerBmountcarB['m'],$sum_avgTimePerBmountcarB['s']);
                                                                ?></td>
                                                        </tr>
                                                        </tfoot>
                                                    <?php }?>
                                                </table>
                                            </div>
                                            <!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_3">
                                                <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                                            ชื่อ SA
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                                                            จำนวนคันรับแจ้งซ่อม
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                            เฉลี่ยต่อวัน
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            รวมเวลารับแจ้งซ่อม (นาที)
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                            เวลาเฉลี่ยต่อคัน (นาที)
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <?php if(!empty($dataSa_Wage_Genaral_Only)){
                                                        $countdataSa_Wage_Genaral_Only = count($dataSa_Wage_Genaral_Only);

                                                        $sum_countcarC = [];
                                                        $sum_avgAmountcarPerDayC = [];
                                                        $sum_countTimeC = [];
                                                        $sum_avgTimePerCmountcarC = []

                                                        ?>
                                                        <?php
                                                        $sumcountday = ApiRepair::countDayOfMonthStartStop($selected_year,$selected_startmonth,$selected_year,$selected_endmonth);
                                                        ?>
                                                        <tbody>
                                                        <?php foreach ($dataSa_Wage_Genaral_Only as $valueDataSa_Wage){?>
                                                            <tr role="row">
                                                                <td><?php echo $valueDataSa_Wage['Name'] ;?>
                                                                    &nbsp&nbsp&nbsp
                                                                    <?php echo $valueDataSa_Wage['Surname'] ; ?></td>
                                                                <td><?php echo  $sum_countcarC[] = $CountC = $valueDataSa_Wage['Count_Car'] ;?></td>
                                                                <td><?php  $sum_avgCmountcarPerDayC[] = $coutCarCvgC = $valueDataSa_Wage['Count_Car']/$sumcountday;
                                                                    echo   helper::displayDecimal($coutCarCvgC);
                                                                    ?>
                                                                </td>
                                                                <td><?php    $sum_TimeC[] = $TimeC =$valueDataSa_Wage['SumDifftime'];
                                                                    $getTimeC = DateTime::calculateTimeElapsed($TimeC);
                                                                    echo  ApiRepair::getFormatTime($getTimeC['h'],$getTimeC['m'],$getTimeC['s']);
                                                                    ?>
                                                                </td>
                                                                <td><?php   $CvgTimePercar = ApiRepair::avgTimePerCar($TimeC,$CountC);
                                                                    $sum_avgTimePerCmountcarC[] =  $secCvgC =  helper::displayDecimal($CvgTimePercar);
                                                                    $getFormatTimeC = DateTime::calculateTimeElapsed($secCvgC);
                                                                    echo  ApiRepair::getFormatTime($getFormatTimeC['h'],$getFormatTimeC['m'],$getFormatTimeC['s']);
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>

                                                        <tfoot>
                                                        <tr>
                                                            <th>รวม</th>
                                                            <td><?php echo array_sum($sum_countcarC); ?></td>
                                                            <td><?php  $TotalSum_avgCmountcarPerDayC = array_sum($sum_avgCmountcarPerDayC);
                                                                echo   helper::displayDecimal($TotalSum_avgCmountcarPerDayC);
                                                                ?></td>
                                                            <td><?php  $TotalSum = array_sum($sum_TimeC);
                                                                $getTotalFormatTimeC = DateTime::calculateTimeElapsed($TotalSum);
                                                                echo ApiRepair::getFormatTime($getTotalFormatTimeC['h'],$getTotalFormatTimeC['m'],$getTotalFormatTimeC['s']);
                                                                ?></td>
                                                            <td><?php  $sum_avgTimePerCmountcarC = array_sum($sum_avgTimePerCmountcarC);
                                                                $sum_avgTimePerCmountcarC = DateTime::calculateTimeElapsed($sum_avgTimePerCmountcarC);
                                                                echo ApiRepair::getFormatTime($sum_avgTimePerCmountcarC['h'],$sum_avgTimePerCmountcarC['m'],$sum_avgTimePerCmountcarC['s']);
                                                                ?></td>
                                                        </tr>
                                                        </tfoot>
                                                    <?php }?>
                                                </table>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- nav-tabs-custom -->
                                </div>
                                <!-- col-sm-12 -->
                            </div>
                            <!-- row -->
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA TABLE -->
        </div>
</div>
    </section>
<?php } ?>
</div><!--box-->
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>