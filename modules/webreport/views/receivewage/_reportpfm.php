<!-- //TODO : remove 3 line javascript -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */
use app\modules\webreport\models\Ytax;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;

use kartik\grid\GridView;
use kartik\export\ExportMenu;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;




AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;


?>


<?php


$session = Yii::$app->session;

$weekend = $session->get('weekend');

$workingday = $session->get('workingday');

$_arrRET = $session->get('_arrRET');

$sumwage = $session->get('sumwage');

if(empty($weekend))
        {
            
        }
    else{
        $sumweekend = array_sum($weekend); //มาทำงานวันอาทิตย์
        $sumworkingday = array_sum($workingday); //มาทำงานวันปกติ

        $Avg_workday = ($_arrRET['total_workday'] > 0) ?  ($sumworkingday/$_arrRET['total_workday']): 0;
        $Avg_sunday = ($_arrRET['total_sunday'] > 0) ?  ($sumweekend/$_arrRET['total_sunday']): 0;

        $date_work = $_arrRET['total_workday']*8*$Avg_workday;
        $date_sun = $_arrRET['total_sunday']*8*$Avg_sunday;

        $resultsum = ($date_work+$date_sun)-(250+80);
    }

?>

<div class="box">
    <h3><i class="fa fa-fw fa-file-text"></i>รายงานประสิทธิภาพงานโดยรวม</h3>

    <h3>ตารางแสดงชั่วโมงการทำงานของช่าง </h3>

    <!-- /.box-header -->


    <table bgcolor="#777" ALIGN="CENTER">

        <thead>

                            <tr bgcolor="#eee">
                                <th>
                                    เดือน
                                </th>
                                <th>
                                    จันทร์-เสาร์
                                </th>
                                <th>
                                    อาทิตย์
                                </th>
                                <th>
                                    ลา(ชม.)
                                </th>
                                <th>
                                    OT(ชม.)
                                </th>
                                <th>
                                    รวมชั่วโมงการทำงาน
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th bgcolor="#eee">
                                    จำนวนวันทำงาน (เดือน)
                                </th>
                                <td bgcolor="#fff">
                                    <?php
                                    echo ($_arrRET['total_workday']) ? $_arrRET['total_workday'] : 0;
                                        ?>
                                </td>
                                <td bgcolor="#fff">
                                    <?php
                                    echo ($_arrRET['total_sunday']) ? $_arrRET['total_sunday'] : 0;
                                        ?>
                                </td>
                                <td bgcolor="#fff">
                                   250
                                </td>
                                <td bgcolor="#fff">
                                   80
                                </td>
                                <td bgcolor="#fff">
                                    <?php
                                    echo ($resultsum) ? $resultsum : 0;
                                    ?>
                                </td>                              
                            </tr>
                            <tr>
                                <th bgcolor="#eee">
                                    จำนวนช่างต่อวัน (เฉลี่ย)
                                </th>
                                <td bgcolor="#fff">
                                    <?php
                                    echo ($Avg_workday) ? (int)$Avg_workday : 0;
                                        ?>
                                </td>
                                <td bgcolor="#fff">
                                    <?php
                                    echo ($Avg_sunday) ? (int)($Avg_sunday) : 0;
                                        ?>
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                            </tr>
                            <tr>
                                <th bgcolor="#eee">
                                    ชั่วโมงทำงานต่อวัน
                                </th>
                                <td bgcolor="#fff">
                                   8
                                </td>
                                <td bgcolor="#fff">
                                   8
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                            </tr>
                            <tr>
                                <th bgcolor="#eee">
                                    จำนวนชั่วโมงทำงาน (เดือน)
                                </th>
                                <td bgcolor="#fff">
                                     <?php
                                     echo ($date_work) ? $date_work : 0;
                                        ?>
                                </td>
                                <td bgcolor="#fff">
                                    <?php
                                    echo ($date_sun) ? $date_sun : 0;
                                        ?>
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                                <td bgcolor="#fff">
                                   -
                                </td>
                            </tr>
                            </tbody>
    </table>


<br>
    <br>
      <?php
        $sumwage = $sumwage[0]['pricewage'];
        $divwage = $sumwage/470;
        $percentwage = ($resultsum > 0) ?  ($divwage/$resultsum)*100 : 0;
        ?>

            <h4>รายได้ค่าแรงทั้งหมด          <?php echo Helper::displayDecimal($sumwage); ?><br>
            ค่าแรงต่อชั่วโมงที่ขาย           470<br>
            ค่าแรงต่อชั่วโมงที่ขายได้   <?php echo Helper::displayDecimal($divwage); ?><br>
            ประสิทธิภาพงานซ่อมโดยรวม      <?php echo Helper::displayDecimal($percentwage); ?>%</h4>
</div>
<!-- ./box -->

<?php
session_start();
session_destroy();
?>