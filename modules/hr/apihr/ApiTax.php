<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 18:10
 */

namespace app\modules\hr\apihr;

use app\api\Helper;
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\models\PayrollConfigWage;
use app\modules\hr\models\TaxIncomeStructure;
use app\modules\hr\models\Wagethismonth;
use yii\helpers\ArrayHelper;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\TaxIncomeSection;
use app\modules\hr\models\TaxIncome;
use Yii;
use yii\db\Expression;

class ApiTax
{

    public static function TaxInCome($datemakesalary,$wage_type)
    {
        //load config
        $arrWageConfig = self::getPayrollConfigWage();
        //$_daycal = $_arrWageConfig['wage_day'];
        $_multiply = $arrWageConfig[$wage_type]['wage_multiply'];


        // $TaxInCome = new TaxIncome();
        //$id_cradmakesalary = Yii::$app->session->get('idcard');

        //collect employee wage this month
        $arrWageData = self::getEmpWageThisMonth($datemakesalary,$_multiply);
        $arrEmpWageThisMonth = $arrWageData['TOTAL'];

        //collect employee wage add deduct this month
        $arrEmpWageThisMonthDetail = self::getEmpWageArray($datemakesalary);

        //collect employee wage this month
        $arrEmpTaxDeduction = self::getEmpTaxDeduction($arrEmpWageThisMonth); //TODO : implement function

        $arrIncomeStructureRule = self::getArrayIncomeStructure();
        //$arrPersonIncomeTax = self::calculateIncomeTax($arrWageData,$wage_type,$arrEmpTaxDeduction,$arrEmpWageThisMonth,$arrIncomeStructureRule,$_arrWageConfig);
        $arrPersonIncomeTax = self::calculateIncomeTax($arrWageData,$wage_type,$arrEmpTaxDeduction,$arrIncomeStructureRule,$arrWageConfig,$datemakesalary);

    }



    public static function calculateIncomeTax($arrWageData,$wage_type,$arrEmpTaxDeduction, $arrIncomeStructureRule,$arrWageConfig,$datemakesalary)
    {
        /** @var
         * $arrPersonIncomeRange  ==> จัดเก็บรายละเอียดการคำนวณ ของภาษีแต่ละขั้น
         * $arrPersonGrossStepTax ==> จัดเก็บภาษี ที่คำนวณได้แต่ละขั้น
         * $arrPersonIncomeRate  ==> จัดเก็บอัตราภาษีแต่ละขั้น
         * $arrPersonAccumulateTax  ==> จัดเก็บอัตราสะสมภาษี ในแต่ละขั้น
         * */

        $arrPersonNetIncome = $arrWageData['TOTAL'];

        $arrPersonGrossStepTax = $arrPersonIncomeRange = $arrPersonIncomeStep = $arrPersonIncomeRate = $arrPersonAccumulateTax = [];
        foreach ($arrPersonNetIncome as $CompanyID => $Person) {

            foreach ($Person as $IDCard => $IncomeStep) {

                //Check income if = 0 OR lessthan 0
                if($IncomeStep <= 0) { echo 'Income incorrect, verify income ID CARD '.$IDCard; exit; }

                $r = 1;
                $startIncome = $IncomeStep;
                $RemainIncome = $TotalBreakIncome = 0;

                foreach ($arrIncomeStructureRule as $rule) {

                    $ceil = $rule['income_ceil'];
                    $floor = $rule['income_floor'];
                    $TaxRate = $rule['tax_rate'] / 100;
                    $IncomeRuleCeil = ($ceil - $floor) + 1;

                    $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = 0;

                    $InconeBeforeCal = $RemainIncome; //เงินได้ก่อน หักคำนวณตามสูตร
                    $IncomeStep -= $IncomeRuleCeil;
                    $RemainIncome = $IncomeStep;

                    //กรณีเกินขอบเขตที่กำหนด เป็นประเภทเหมาจ่าย
                    if ($ceil == 0) {
                        $balloon = ($startIncome - $TotalBreakIncome);  //นำเงินได้ที่ สะสมหักในอัตราปกติ ลบออกจากเงินได้ทั้งหมด จะได้เงินก้อน (balloon) เพื่อหาภาษีขั้นสุดท้าย
                        $StepTax = $balloon * $TaxRate;
                        $arrPersonIncomeStep[$CompanyID][$IDCard][$rule['id']] = $balloon;
                        $arrPersonIncomeRange[$CompanyID][$IDCard][$rule['id']] = 'Income Over Floor/Upper bound (Balloon Amount) ' . Helper::displayDecimal($balloon) . ' Rate ' . ($TaxRate * 100) . '% Tax:' . Helper::displayDecimal(($TaxRate * $balloon));;
                        $arrPersonGrossStepTax[$CompanyID][$IDCard][$rule['id']] = $StepTax;
                        $arrPersonIncomeRate[$CompanyID][$IDCard][$rule['id']] = $TaxRate;
                        $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = array_sum($arrPersonGrossStepTax[$CompanyID][$IDCard]);

                    } else {
                        //กรณีหักเงินได้ตามอัตรา และมีเงินคงเหลือ ให้นำเงินได้ ที่หักคำนวณได้ในขั้น มาคูณอัตราภาษี
                        $TotalBreakIncome += $IncomeRuleCeil;  //รวมจำนวนเงินได้ที่หัก ไปแล้ว ในอัตราปกติ เพื่อใช้ในการคำนวน ต่อเงินได้ เกินเพดาน

                        if ($RemainIncome >= 0) {
                            $StepTax =  $IncomeRuleCeil * $TaxRate;

                            $IncomeRange = 'C:' . Helper::displayDecimal($ceil) . ' - F:' . Helper::displayDecimal($floor, 2) . ' = Rule:' . Helper::displayDecimal($IncomeRuleCeil);
                            $IncomeRange .= ' Remain:' . Helper::displayDecimal($RemainIncome) . ' Rate:' . ($TaxRate * 100) . '%' . ', Tax:' . Helper::displayDecimal($StepTax);

                            $arrPersonIncomeStep[$CompanyID][$IDCard][$rule['id']] = $IncomeRuleCeil;
                            $arrPersonIncomeRange[$CompanyID][$IDCard][$rule['id']] = $IncomeRange;
                            $arrPersonGrossStepTax[$CompanyID][$IDCard][$rule['id']] = $StepTax;
                            $arrPersonIncomeRate[$CompanyID][$IDCard][$rule['id']] = $TaxRate;
                            $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = array_sum( $arrPersonGrossStepTax[$CompanyID][$IDCard]);

                        }
                        else {
                            //กรณีหักเงินได้ตามอัตรา แต่ไม่พอ ให้นำเงินได้ ก่อนหักคำนวณ มา คูณอัตราภาษี
                            $StepTax =  $InconeBeforeCal * $TaxRate;

                            $IncomeRange = 'C:' . Helper::displayDecimal($ceil) . ' - F:' . Helper::displayDecimal($floor, 2) . ' = Rule:' . Helper::displayDecimal($IncomeRuleCeil);
                            $IncomeRange .= ' Remain:' . Helper::displayDecimal($RemainIncome) . ' Rate:' . ($TaxRate * 100) . '%' . ', Tax:' . Helper::displayDecimal($StepTax);

                            $arrPersonIncomeStep[$CompanyID][$IDCard][$rule['id']] = ($r==1) ? $startIncome :  $InconeBeforeCal;
                            $arrPersonIncomeRange[$CompanyID][$IDCard][$rule['id']] = $IncomeRange;
                            $arrPersonGrossStepTax[$CompanyID][$IDCard][$rule['id']] = $StepTax;
                            $arrPersonIncomeRate[$CompanyID][$IDCard][$rule['id']] = $TaxRate;
                            $arrPersonAccumulateTax[$CompanyID][$IDCard][$rule['id']] = array_sum($arrPersonGrossStepTax[$CompanyID][$IDCard]);
                            break;
                        }
                    }
                    $r++;
                }

            }
        }

        self::batchInsertIncomeTax($arrWageConfig,$arrWageData,$wage_type,$arrEmpTaxDeduction,$arrPersonIncomeRange,$arrPersonGrossStepTax,$arrPersonIncomeRate,$arrPersonAccumulateTax,$arrPersonIncomeStep,$datemakesalary);


/*

        echo '<br>'. ApiHr::getOrgMasterConfig()->getWorkDayInMonth();  //จำนวนวันทำงานในเดือน
        echo '<br>'. ApiHr::getOrgMasterConfig()->getWorkHourInDay();  //จำนวนชั่วโมงทำงานในวัน
        echo '<br>'. ApiHr::getOrgMasterConfig()->getStartWorkHour(); //กำหนดเวลาเข้างานปกติ
        echo '<br>'.ApiHr::getOrgMasterConfig()->getEndWorkHour(); //กำหนดเวลาเลิกงานปกติ
        echo '<br>'.ApiHr::getOrgMasterConfig()->getSSORate();  //อัตราการคิดเงินประกันสังคมพนักงาน (%)
        echo '<br>'.ApiHr::getOrgMasterConfig()->getSavingRate(); // อัตราการคิดเงินสะสมพนักงาน (%)
        echo '<br>'.ApiHr::getOrgMasterConfig()->getDayStartSalary(); //วันตัดรอบเงินเดือน

        echo '<hr>';

        echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getOtTemplateID(); //รหัสประเภทเทมเพลตของโอที
        echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getSsoTemplateID(); //รหัสประเภทเทมเพลตของประกันสังคม
        echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getTaxIncomeTemplateID(); //รหัสประเภทเทมเพลตของภาษี ภงด 91
        echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getTaxPndTemplateID(); //รหัสประเภทเทมเพลตของภาษี ภงด 1
        echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getTaxTaviTemplateID(); //รหัสประเภทเทมเพลตของภาษี หัก ณ ที่จ่าย
        echo  '<br>'.ApiHr::getPayrollConfigTemplate()->getGuaranteeTemplateID(); //รหัสของประเภทเทมเพลตเงินค้ำประกัน
        */


    }


    public static function batchInsertIncomeTax($arrWageConfig,$arrWageData,$wage_type,$arrEmpTaxDeduction,$arrPersonIncomeRange,$arrPersonGrossStepTax,$arrPersonIncomeRate,$arrPersonAccumulateTax,$arrPersonIncomeStep,$datemakesalary)
    {

/*        echo '<pre>';
        echo print_r($arrPersonIncomeStep[12][1549900454352]);
        echo '<pre>';
        echo '<hr>';

        echo '<pre>';
        echo print_r($arrPersonAccumulateTax[12][1549900454352]);
        echo '<pre>';
        echo '<hr>';

        echo '<pre>';
        echo print_r($arrPersonGrossStepTax[12][1549900454352]);
        echo '<pre>';
        echo '<hr>';

        echo '<pre>';
        echo print_r($arrPersonIncomeRate[12][1549900454352]);
        echo '<pre>';
        echo '<hr>';

        echo '<pre>';
        echo print_r($arrPersonIncomeRange[12][1549900454352]);
        echo '<pre>';
        echo '<hr>';

        exit;*/

        $arrCompany = ApiHr::getArrayCompany();

        $wageCfg = $arrWageConfig[$wage_type];
        $wage_type = $wageCfg['wage_type'];
        $wage_multiply = $wageCfg['wage_multiply'];

        $my = explode('-',$datemakesalary);
        $this_month = $my[0];
        $this_year = $my[1];

        $connection = Yii::$app->dbERP_easyhr_PAYROLL;
        $transaction = $connection->beginTransaction();

        try {

            foreach ($arrWageData['TOTAL'] as $CompanyID => $Emp)
            {
                $empIDCard = key($Emp);  //Lookup Key
                $arrEmpWageData = $arrWageData['TOTAL'];
                $company = $arrCompany[$CompanyID];

                $company_name = $company['name'];
                $company_tax_code = 'zzz';

                $position_code = 'aaa';
                $position_name = 'bbb';

                $salary_wage_monthly = 0;
                $salary_wage_other = 0;
                $total_monthly = 0;
                $total_yearly = 0;
                $total_deduction = 0;
                $total_income = 0;
                $total_tax_year = 0;
                $total_tax_month = 0;

                $createby_user = Yii::$app->session->get('idcard');
                $create_datetime = date('Y-m-d H:i:s');

                $sql_master = "INSERT INTO `tax_calculate` (`id`, `emp_idcard`, `months`, `years`, `for_month`, `wage_type`, `wage_multiply`, 
                    `company_id`, `company_name`, `company_tax_code`, `position_code`, `position_name`, 
                    `salary_wage_monthly`, `salary_wage_other`, `total_monthly`, `total_yearly`, `total_deduction`, 
                    `total_income`, `total_tax_year`, `total_tax_month`, `createby_user`, `create_datetime`) VALUES 
                    (NULL, '$empIDCard', $this_month, $this_year, '$datemakesalary', $wage_type, $wage_multiply, 
                    $CompanyID, '$company_name', '$company_tax_code', '$position_code', '$position_name' ,$salary_wage_monthly , $salary_wage_other, 
                    $total_monthly , $total_yearly, $total_deduction, $total_income, $total_tax_year, $total_tax_month, '$createby_user', '$create_datetime');";

                $eff = $connection->createCommand($sql_master)->execute();
                $tax_calculate_id = $connection->getLastInsertID();

                $_table_detail = 'tax_calculate_step';
                $_arrDetailCol = ['id', 'tax_calculate_id', 'tax_income_structure_id', 'income_range', 'tax_rate', 'income_step', 'gross_step_tax', 'total_accumulate_tax'];

                $eff = $idx = 0;
                $_arr_data = null;

                $arrEmpIncomeRange = $arrPersonIncomeRange[$CompanyID][$empIDCard];
                $arrEmpIncomeStep = $arrPersonIncomeStep[$CompanyID][$empIDCard];
                $arrEmpGrossStepTax = $arrPersonGrossStepTax[$CompanyID][$empIDCard];
                $arrEmpIncomeRate = $arrPersonIncomeRate[$CompanyID][$empIDCard];
                $arrEmpAccumulateTax = $arrPersonAccumulateTax[$CompanyID][$empIDCard];

                foreach ($arrEmpIncomeRange as $ruleID => $value) {
                    $_arr_data[] = [
                        $_arrDetailCol[0] => NULL, //id
                        $_arrDetailCol[1] => $tax_calculate_id,  //tax_calculate_id
                        $_arrDetailCol[2] => $ruleID, //tax_income_structure_id
                        $_arrDetailCol[3] => $value,   //income_range
                        $_arrDetailCol[4] => $arrEmpIncomeRate[$ruleID],   //tax_rate
                        $_arrDetailCol[5] => $arrEmpIncomeStep[$ruleID],   //income_step
                        $_arrDetailCol[6] => $arrEmpGrossStepTax[$ruleID],   //gross_step_tax
                        $_arrDetailCol[7] => $arrEmpAccumulateTax[$ruleID],   //total_accumulate_tax
                    ];

                }

               $connection->createCommand()->batchInsert($_table_detail, $_arrDetailCol, $_arr_data)->execute();
            }

            $transaction->commit();

        } catch (\ErrorException $e) {
            $transaction->rollBack();
            throw new \Exception('ERROR' . $e->getMessage());
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

    }

    public static function getPayrollConfigWage()
    {
        $models = PayrollConfigWage::find()->where([
            'status_active'=>1,
        ])->orderBy('wage_type ASC')->asArray()->all();

        $arrWage = [];
        foreach ($models as $item) {
            $arrWage[$item['wage_type']] = $item;
        }

        return $arrWage;
    }

    public static function getAddDeduct()
    {
        $model = Adddeducttemplate::find()
            ->where(['=', 'ADD_DEDUCT_TEMPLATE_TYPE', '1'])
            ->andWhere(['=', 'ADD_DEDUCT_TEMPLATE_STATUS', '1'])
            ->all();
        $arrAddDeduct = [];
        foreach ($model as $item) {
            $arrAddDeduct[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item;
        }

        return $arrAddDeduct;
    }

    public static function getTaxIncomeSection($obj = null)
    {
        $models = TaxIncomeSection::find()
            ->where(['status_active' => 1])
            ->asArray()
            ->all();

        return ($obj == 'obj') ? $models : ArrayHelper::map($models, 'id', 'tax_section_name');
    }

    public static function getTaxSection()
    {
        $models = TaxIncomeSection::find()
            ->where(['status_active' => 1])
            ->asArray()
            ->all();
        $arrSection = [];
        foreach ($models as $item) {
            $arrSection[$item['id']] = $item;
        }

        return $arrSection;
    }


    public static function getArrayIncomeStructure()
    {
        $model = TaxIncomeStructure::find()->where([
            'status_active' => 1
        ])->asArray()->orderBy('income_floor ASC')->all();

        $arrIncomeStructure = [];
        if ($model != null) {
            foreach ($model as $item) {
                $arrIncomeStructure[] = $item;
            }
        }

        return $arrIncomeStructure;
    }


    public static function getArrayTemplateTax()
    {
        $model = Adddeducttemplate::find()->where([
            'ADD_DEDUCT_TEMPLATE_TYPE' => Yii::$app->params['PAYROLL']['TYPE_ADD'],
            'ADD_DEDUCT_TEMPLATE_STATUS' => '1',
        ])->orderBy('ADD_DEDUCT_TEMPLATE_ID ASC')->asArray()->all();
        $arrTemplateTax = [];
        if ($model != null) {
            foreach ($model as $item) {
                $data = [];
                $data['template_id'] = $item['ADD_DEDUCT_TEMPLATE_ID'];
                $data['template_name'] = $item['ADD_DEDUCT_TEMPLATE_NAME'];
                $data['taxincome_type'] = $item['taxincome_type_id'];
                $data['taxincome_rate'] = $item['tax_rate'];

                $arrTemplateTax[$item['ADD_DEDUCT_TEMPLATE_ID']] = $data;
            }
        }
        return $arrTemplateTax;
    }


    public static function makeEmpSalaryArray($objEmp)
    {
        $arrEmpSalary = [];
        if ($objEmp != null) {
            foreach ($objEmp as $item) {

                $data = [];
                $data['position_code'] = $item['Code'];
                $data['position_name'] = $item['Position'];
                $data['company_id'] = $item['Working_Company'];
                //$data['company_name'] = $item['Working_Company'];
                $data['salary'] = $item['EMP_SALARY_WAGE'];
                $arrEmpSalary[$item['empIdCard']][$item['Working_Company']] = $data;

            }
        }

        return $arrEmpSalary;
    }

    /**
     * Function การคำนวณหารายได้ตามประเภทของพนักงานแต่ละคน
     * @param $dateSalary
     * @return array
     */
    public static function getEmpWageArray($wage_date)
    {
        $model = Adddeductthismonth::find()->where([
            'ADD_DEDUCT_THIS_MONTH_PAY_DATE' => $wage_date,
            'ADD_DEDUCT_THIS_MONTH_TYPE' => Yii::$app->params['PAYROLL']['TYPE_ADD'],
        ])->all();

        $arrEmpWageDetail = [];
        if ($model != null) {
            foreach ($model as $item) {
                $data = [];
                $data['template_id'] = $item->ADD_DEDUCT_THIS_MONTH_TMP_ID;
                $data['template_name'] = $item->ADD_DEDUCT_THIS_MONTH_TMP_NAME;
                //$data['template_name'] = $item->ADD_DEDUCT_THIS_MONTH_TMP_NAME;
                $data['amount'] = $item->ADD_DEDUCT_THIS_MONTH_AMOUNT;

                $arrEmpWageDetail[$item->ADD_DEDUCT_THIS_MONTH_EMP_ID][$item->ADD_DEDUCT_THIS_MONTH_TMP_ID] = $data;
            }
        } else {
            echo 'ไม่พบรายการ';
            exit;
        }

        return $arrEmpWageDetail;
    }


    /**
     * Function การคำนวณหารายได้รวมต่อเดือนของพนักงานตามบริษัท
     * @param $dateSalary, $multiply
     * @return array
     */
    public static function getEmpWageThisMonth($dateSalary,$multiply)
    {

        $model = Wagethismonth::find()
            ->select('WAGE_ID,WAGE_WORKING_COMPANY,WAGE_EMP_ID,WAGE_POSITION_CODE,WAGE_POSITION_NAME,WAGE_PAY_DATE,SUM(WAGE_SALARY) as WAGE, SUM(WAGE_TOTAL_ADDITION) as ADDDEDUCT, SUM(WAGE_EARN_PLUS_ADD) as TOTAL_WAGE')
            ->where([
            'WAGE_PAY_DATE'=>$dateSalary
            ])
            ->groupBy('WAGE_WORKING_COMPANY,WAGE_EMP_ID')
            ->orderBy('WAGE_WORKING_COMPANY, WAGE_POSITION_CODE, WAGE_EMP_ID ASC')->asArray()->all();

        $arrEmpWageThisMonth = [];
        $arrEmpWageThisMonthZero = [];
        $arrEmpWageDetailThisMonth = [];
        foreach ($model as $item) {
            if ($item['TOTAL_WAGE'] > 0) {
                $data = [];
                $data['WAGE'] = $item['WAGE'];
                $data['ADDDEDUCT'] = $item['ADDDEDUCT'];
                $data['TOTAL_WAGE'] = $item['TOTAL_WAGE'];
                $data['POSITION_CODE'] = $item['WAGE_POSITION_CODE'];
                $data['POSITION_NAME'] = $item['WAGE_POSITION_NAME'];

                $arrEmpWageDetailThisMonth[$item['WAGE_WORKING_COMPANY']][$item['WAGE_EMP_ID']] = $data;
                $arrEmpWageThisMonth[$item['WAGE_WORKING_COMPANY']][$item['WAGE_EMP_ID']] = ($item['TOTAL_WAGE'] * $multiply);
            }
            else {
                $arrEmpWageThisMonthZero[$item['WAGE_WORKING_COMPANY']][$item['WAGE_EMP_ID']] = 0;
            }
        }

        return [
            'TOTAL'=>$arrEmpWageThisMonth,
            'DETAIL'=>$arrEmpWageDetailThisMonth,
        ];
        //$arrEmpWageThisMonth;
    }

    public static function getEmpTaxDeduction()
    {
        $model = TaxIncome::find()->orderBy('emp_id ASC')->asArray()->all();
        $arrEmpDeduct = [];
        foreach ($model as $item) {
            $arrEmpDeduct[$item['emp_id']] = $item;
        }
        return $arrEmpDeduct;
    }

    public static function calculateEmpTaxDeduction($arrEmpWageThisMonth)
    {
        $arrEmpID = ApiHr::findEmpIDByIDCard();
        //$arrEmpWageThisMonth
        //echo '<pre>';
        //print_r($arrEmpWageThisMonth);
        //echo '</pre>';
        //exit;

        return [];
    }




    public static function getAccumulateTax()
    {

    }


    public static function getTotalFixedReduce()
    {

    }

    public static function getReduceOther()
    {

    }





}

?>