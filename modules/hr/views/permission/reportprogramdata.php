<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:27
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
$selectProgram = ApiHr::selectProgram();
$selectworking = ApiHr::getWorking_company();
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/permission.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/js/jquery.sumoselect.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/permission/sumoselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb"></ul>
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body" style='overflow: scroll;height: 600px;'>
             <table class="table table-bordered" >
                <thead>
                    
                    <tr>
                        <th rowspan="2">โปรแกรม</th>
                        <th rowspan="2">เมนู</th>
                        <?php foreach ($head1  as $value) { ?>
                            <?php foreach ($value  as $item) { ?>
                             <th colspan="<?php echo count($value);?>"> <?php echo $item['name']  ?> </th>
                             
                            <?php 
                                break;
                            }
                        }?>
                    </tr>
                    <tr>
                         <?php foreach ($head1  as $value) { ?>
                             <?php foreach ($value  as $item) { ?>
                                <th> <?php echo $item['Name']  ?> </th>
                            <?php  }?>
                        <?php }?>
                         
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data as $key1 => $item){ // program
                    foreach ($item as $key2 => $subitem) { //menu
                        ?>
                    <tr>
                       
                        <td><?php echo $subitem['program']?></td>
                        <td><?php echo $subitem['menu']?> </td>
                       
                        <?php foreach ($head1  as $key3 => $value) { //section ?>
                             <?php foreach ($value  as $key4 => $item) {//level ?>
                                <th> <input type="checkbox" id='<?php echo $key1.'-'.$key2.'-'.$key3.'-'.$key4 ?>' style='width: 20px; height: 20px;' value="" disabled> </th>
                            <?php } 
                            }
                            ?>
                        <?php }?>
                    </tr>
                <?php    }?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box -->
</section><!-- ทำต่อ /.content  -->