<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:25
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payrollexport/exporttobank.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>ส่งออกข้อมูล</li>
                <li class="active">ข้อมูลส่งธนาคาร</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
             <div class="row">
                <form id='exporttoexcel'  data-toggle="validator"  method="post">
                <div class="col-md-12">
                        <div class="box box-warning">
                                <div class="box-header with-border">
                                <h4 class="box-title">เลือกวันที่ออกรายงานส่งธนาคาร </h4>
                                </div>
                                <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class ="row">
                                            <div class="col-md-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="numberPassportEmp"  class="col-sm-4 control-label">บริษัท</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control multiselect" name="selectworking[]" id="arrComexport"
                                                                   multiple="multiple" required>
                                                                
                                                                <?php $working = ApiHr::getWorking_company();
                                                                foreach ($working as $value) {
                                                                    echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                                } ?>
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="row">
                                            &nbsp;
                                        </div>
                                        <div class ="row">
                                            <div class="col-md-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="numberPassportEmp"  class="col-sm-4 control-label">เลือกวันที่จ่ายเงินเดือน</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control dateselect" name="datepayexport" id="datepayexport" required>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                       <div class ="row">
                                            &nbsp;
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-5">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button type="button" class="btn  btn-primary btn-sm" id="submitexportbank"><i class="fa fa-download"></i> ส่งออกข้อมูล</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>    
                </div>
                </form>

             </div>
             <div class="row">
                <div class="col-md-1">
                &nbsp;
                </div>

                <h5>ส่งออกข้อมูล = ส่งออกข้อมูลเงินเดือนเป็นไฟล์ .xlsx ตามบริษัทที่ทำการเลือก  ตัวอย่างไฟล์ที่ต้องการ  ICS2002.xlsx </h5>
             </div>
             <div class="row">
                <div class="col-md-12">
                    <form id="getreportsalarythismonth"  data-toggle="validator" action="getreportsalarythismonth" method="post">
                     <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                        <div class="box box-warning">
                                <div class="box-header with-border">
                                <h4 class="box-title">ออกรายงานเฉพาะรายกการ (แบบได้เฉพาะเดือน)</h4>
                                </div>
                                <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-3">
                                                 <div class="form-group">
                                                    <label for="numberPassportEmp"  class="col-sm-4 control-label">บริษัท</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control " name="Workingreport" id="arrWorkingreport">
                                                                
                                                                <?php $working = ApiHr::getWorking_company();
                                                                foreach ($working as $value) {
                                                                    echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                                } ?>
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                           
                                            <div class="col-md-3">
                                               <div class="form-group">
                                                    <label for="numberPassportEmp multiselect"  class="col-sm-4 control-label">รายการเพิ่ม</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control multiselect" name="arrIdtemp[]" id="arrIdtemp" multiple="multiple" required>
                                                        <?php $working = ApiPayroll::listdata_add_deduct_template();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                        } ?>
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                                &nbsp;
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-3">
                                                 <div class="form-group">
                                                    <label for="numberPassportEmp"  class="col-sm-4 control-label">รอบจ่ายเงินเดือน</label>
                                                        <div class="col-sm-8">
                                                           <input type="text" class="form-control monthselect" name="monthselectreport" id="monthselectreport" required>
                                                        </div>
                                                </div>
                                            </div>
                                           
                                            <div class="col-md-3">
                                               <div class="form-group">
                                                    <label for="numberPassportEmp"  class="col-sm-4 control-label">วันที่โอน</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control dateselect" name="dateselectreport" id="dateselectreport" required>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                                &nbsp;
                                        </div>
                                         <div class="row">
                                            <div class="col-md-6">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="submit" class="btn  btn-primary btn-sm" id="seachdatalistreport" value="ส่งออกข้อมูล">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                                &nbsp;
                                        </div>
                                         <div class="row">
                                            <div class="col-md-4">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: none;">
                                                    <h4>รายงานการออก Excel ของเดือน <?php echo date('t-m-Y');?> ของบริษัท  <span>xxxxxxx</span></h4>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    <div style="overflow: scroll;">
                                               <?php if($activeShow){?>
                                                <table width="100%" border="1" id="showdetail" class="table table-responsive">
                                                        <thead>
                                                            <tr>
                                                                <th width="100%">ชื่อ-สกุล</th>
                                                                <th width="10%">เงินเดือน</th>
                                                                <?php foreach($modelReportTemp as $values){?>
                                                                <th width="10%"><?php echo $values['ADD_DEDUCT_TEMPLATE_NAME'];?></th>
                                                                <?php } ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach($modelReportGetEmp as $key => $valemp){?>
                                                            <tr>
                                                                <td><?php echo $valemp['Fullname'];?></td>
                                                                <td><?php $resultSalary ;
                                                                        foreach($modelReportSalary as $valSalary){
                                                                            if($valemp['ID_Card']==$valSalary['WAGE_EMP_ID']){
                                                                                $resultSalary = $valSalary['WAGE_SALARY'];
                                                                            }
                                                                        }
                                                                        echo $resultSalary;
                                                                ?></td>
                                                                <?php foreach($modelReportTemp as $values){?>
                                                                <td>
                                                                <?php $resultAdddeduc="0.00";
                                                                foreach($modelReportAdddeducthismonth as $valaddeduc){
                                                                    if($valaddeduc['ADD_DEDUCT_THIS_MONTH_TMP_ID']==$values['ADD_DEDUCT_TEMPLATE_ID']
                                                                    &&$valaddeduc['ADD_DEDUCT_THIS_MONTH_EMP_ID']==$valemp['ID_Card']){
                                                                        $resultAdddeduc=$valaddeduc['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                                                                    }
                                                                }
                                                                    echo $resultAdddeduc;
                                                                ?>
                                                                </td>
                                                                <?php } ?>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                </table>
                                                <?php } ?>
                                    </div>
                                 
                                    </div>
                                    
                        </div>
                    </form>    
                </div>
             </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->