<?php

namespace app\modules\baymanagement\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/**
 * This is the model class for table "ehole_baydetail".
 *
 * @property integer $id
 * @property integer $bay_id
 * @property string $bay_name
 * @property integer $branch_id
 * @property integer $agroup_id
 * @property string $agroup_tis_name
 * @property integer $bay_x
 * @property integer $bay_y
 * @property string $bay_color
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class EholeBaydetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ehole_baydetail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bay_id', 'bay_name', 'branch_id', 'agroup_id', 'agroup_tis_name', 'bay_x', 'bay_y' ,'bay_color', 'status_active', 'createby_user', 'create_datetime'], 'required'],
            [['bay_id', 'branch_id', 'agroup_id', 'bay_x', 'bay_y', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['bay_name'], 'string', 'max' => 255],
            [['agroup_tis_name'], 'string', 'max' => 100],
            [['bay_color'], 'string', 'max' => 7],
            [['createby_user', 'updateby_user'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bay_id' => 'Bay ID',
            'bay_name' => 'Bay Name',
            'branch_id' => 'Branch ID',
            'agroup_id' => 'Agroup ID',
            'agroup_tis_name' => 'Agroup Tis Name',
            'bay_x' => 'Bay X',
            'bay_y' => 'Bay Y',
            'bay_color' => 'Bay Color',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }


    public function search($params)
    {
        $data = EholeBaydetail::find()->where('status_active <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like', 'id', $this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like', 'bay_name', $this->bay_name]); //	ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }

}
