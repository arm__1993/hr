<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "MAIN_TELEPHONE".
 *
 * @property integer $TEL_ID
 * @property integer $TEL_CUS_NO
 * @property string $TEL_NUM
 * @property integer $TEL_TYPE
 * @property string $TEL_DESC
 * @property string $TEL_REMARK
 * @property integer $TEL_MAIN_ACTIVE
 */
class Maintelephone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MAIN_TELEPHONE';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_new_cusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TEL_CUS_NO', 'TEL_NUM', 'TEL_TYPE', 'TEL_DESC', 'TEL_REMARK', 'TEL_MAIN_ACTIVE'], 'required'],
            [['TEL_CUS_NO', 'TEL_TYPE', 'TEL_MAIN_ACTIVE'], 'integer'],
            [['TEL_REMARK'], 'string'],
            [['TEL_NUM'], 'string', 'max' => 30],
            [['TEL_DESC'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TEL_ID' => 'auto run',
            'TEL_CUS_NO' => 'รหัสลูกค้า',
            'TEL_NUM' => 'เบอร์โทร',
            'TEL_TYPE' => '1=มือถือ,2=ที่ทำงาน,3=บ้าน,4=fax,5=อื่นๆ',
            'TEL_DESC' => 'คำอธิบาย',
            'TEL_REMARK' => 'หมายเหตุ',
            'TEL_MAIN_ACTIVE' => '1=เบอร์โทรหลัก',
        ];
    }
    public function countrowmaintel($cusno)
    {

        $sql = "select count(*) As Numloop from MAIN_TELEPHONE where TEL_CUS_NO = :cusno ORDER BY MAIN_TELEPHONE.TEL_ID ASC";
        //echo ;
        $countrow = Yii::$app->dbERP_new_cusdata
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
        return $countrow;

    }

    public function searchmaintelcusformobile($cusno)
    {

        $sql = "select * from MAIN_TELEPHONE where TEL_CUS_NO = :cusno ORDER BY MAIN_TELEPHONE.TEL_ID ASC";
        //echo ;
        $showdatatelcus = Yii::$app->dbERP_new_cusdata
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
        return $showdatatelcus;
 
    }

    
}
