<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "emp_data".
 *
 * @property integer $DataNo
 * @property string $BNo
 * @property string $Code
 * @property string $Old_Code
 * @property string $TIS_id
 * @property string $Pictures_HyperL
 * @property string $IDCard_HyperL
 * @property string $HomeDoc_HyperL
 * @property string $Be
 * @property string $Name
 * @property string $Surname
 * @property string $Nickname
 * @property string $username
 * @property string $BenameEN
 * @property string $nameEmpEN
 * @property string $lastNameEmpEN
 * @property string $nickNameEmpEN
 * @property string $password
 * @property string $password_old
 * @property string $password_test
 * @property string $password_backup_by_kenz
 * @property string $Sex
 * @property string $Birthday
 * @property string $race
 * @property string $Nationality
 * @property string $Religion
 * @property string $Weight
 * @property string $Height
 * @property string $Blood_G
 * @property integer $Foot_Step_Num
 * @property string $ID_Card
 * @property string $districtRelease
 * @property string $DateOfIssueIdcrad
 * @property string $DateOfExpiryIdcrad
 * @property string $Social_Card
 * @property string $Father
 * @property string $Father_Job
 * @property string $Mother
 * @property string $Mother_Job
 * @property string $Parent_Status
 * @property string $Status
 * @property string $Spouse_prefix
 * @property string $Spouse_Name
 * @property string $Spouse_Surname
 * @property string $Spouse_Birthday
 * @property string $Spouse_career
 * @property string $Spouse_workplace
 * @property string $Childs
 * @property string $Relative_Person
 * @property string $Relative_Person_Tel
 * @property string $Address
 * @property string $Moo
 * @property string $Road
 * @property string $Village
 * @property string $SubDistrict
 * @property string $District
 * @property string $Province
 * @property string $geography
 * @property string $Postcode
 * @property string $Live_with
 * @property string $Resident_type
 * @property string $Tel_Num
 * @property string $Mobile_Num
 * @property string $E_Mail
 * @property string $HighSchool
 * @property string $Adv_HighSchool
 * @property string $Adv_Major
 * @property string $Bechelor
 * @property string $Bec_Major
 * @property string $Master
 * @property string $Master_Major
 * @property string $Working_Company
 * @property string $work_type_id
 * @property string $Position
 * @property string $Department
 * @property string $Section
 * @property string $Team
 * @property string $Level
 * @property string $Salary_Bank
 * @property string $Salary_BankNo
 * @property integer $SalaryViaBank
 * @property integer $promtpay_type
 * @property string $promtpay_number
 * @property string $Guarantee_Money
 * @property integer $Prosonnal_Being
 * @property string $DayOff
 * @property string $Start_date
 * @property string $Probation_Date
 * @property string $End_date
 * @property string $startWorkTime
 * @property string $beginLunch
 * @property string $endLunch
 * @property string $endWorkTime
 * @property string $Main_EndWork_Reason
 * @property string $EndWork_Reason
 * @property string $Footnote
 * @property string $Emergency_Call_Person
 * @property string $Emergency_Relation
 * @property string $Emergency_Phone_Num
 * @property string $can_la
 * @property string $use_check
 * @property string $socialBenifitPercent
 * @property string $socialBenefitStatus
 * @property string $benefitFundPercent
 * @property string $benifitFundStatus
 * @property string $to_salary_status
 * @property integer $disease
 * @property string $diseaseDetail
 * @property string $numberPassportEmp
 * @property string $DateOfIssuePassport
 * @property string $DateOfExpiryPassport
 * @property string $Pic_map
 * @property string $Latitude
 * @property string $Longitude
 * @property string $UPDATE_TIME
 * @property string $UPDATE_BY
 * @property integer $status_confirmData
 * @property string $image_card
 * @property string $job_type
 */
class Empdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }
    public static function primarykey()
    {
        return ['DataNo'];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BNo', 'Code', 'Old_Code', 'TIS_id', 'Pictures_HyperL', 'IDCard_HyperL', 'HomeDoc_HyperL', 'Be', 'password', 'password_old', 'password_test', 'password_backup_by_kenz', 'Spouse_workplace', 'Main_EndWork_Reason', 'EndWork_Reason', 'Footnote'], 'string'],
            [['Name', 'Surname', 'username', 'password', 'ID_Card'], 'required'],
            [['Birthday', 'DateOfIssueIdcrad', 'DateOfExpiryIdcrad', 'Spouse_Birthday', 'Start_date', 'Probation_Date', 'End_date', 'DateOfIssuePassport', 'DateOfExpiryPassport', 'UPDATE_TIME'], 'safe'],
            [['Foot_Step_Num', 'SalaryViaBank','promtpay_type' ,'Prosonnal_Being', 'disease', 'status_confirmData'], 'integer'],
            [['Name', 'Surname', 'Nickname', 'Father', 'Father_Job', 'Mother', 'Mother_Job', 'Spouse_Name', 'Spouse_Surname', 'Spouse_career', 'Relative_Person', 'Road', 'Live_with', 'Salary_Bank', 'Emergency_Call_Person'], 'string', 'max' => 250],
            [['username', 'BenameEN', 'nameEmpEN', 'lastNameEmpEN', 'nickNameEmpEN', 'districtRelease', 'E_Mail', 'HighSchool', 'Adv_HighSchool', 'Adv_Major', 'Bechelor', 'Bec_Major', 'Master', 'Master_Major'], 'string', 'max' => 200],
            [['Sex', 'Weight', 'Height', 'Spouse_prefix', 'Childs', 'Moo', 'Level', 'DayOff'], 'string', 'max' => 10],
            [['race', 'Address', 'Village', 'SubDistrict', 'District', 'Province', 'geography', 'Working_Company', 'Position', 'Department', 'Section', 'Emergency_Relation', 'Pic_map', 'Latitude', 'Longitude', 'UPDATE_BY','image_card','job_type'], 'string', 'max' => 100],
            [['Nationality', 'Religion', 'Social_Card', 'Parent_Status', 'Status', 'Salary_BankNo', 'Guarantee_Money'], 'string', 'max' => 20],
            [['Blood_G', 'socialBenifitPercent', 'socialBenefitStatus', 'benefitFundPercent', 'benifitFundStatus'], 'string', 'max' => 2],
            [['ID_Card'], 'string', 'max' => 14],
            [['Relative_Person_Tel', 'Resident_type', 'Tel_Num', 'Mobile_Num', 'Team', 'Emergency_Phone_Num', 'numberPassportEmp'], 'string', 'max' => 50],
            [['Postcode', 'startWorkTime', 'beginLunch', 'endLunch', 'endWorkTime'], 'string', 'max' => 5],
            [['work_type_id'], 'string', 'max' => 11],
            [['can_la', 'use_check'], 'string', 'max' => 3],
            [['to_salary_status'], 'string', 'max' => 1],
            [['diseaseDetail'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'DataNo' => 'Data No',
            'BNo' => 'Bno',
            'Code' => 'Code',
            'Old_Code' => 'Old  Code',
            'TIS_id' => 'Tis ID',
            'Pictures_HyperL' => 'Pictures  Hyper L',
            'IDCard_HyperL' => 'Idcard  Hyper L',
            'HomeDoc_HyperL' => 'Home Doc  Hyper L',
            'Be' => 'Be',
            'Name' => 'Name',
            'Surname' => 'Surname',
            'Nickname' => 'Nickname',
            'username' => 'Username',
            'BenameEN' => 'Bename En',
            'nameEmpEN' => 'Name Emp En',
            'lastNameEmpEN' => 'Last Name Emp En',
            'nickNameEmpEN' => 'Nick Name Emp En',
            'password' => 'Password',
            'password_old' => 'Password Old',
            'password_test' => 'Password Test',
            'password_backup_by_kenz' => 'Password Backup By Kenz',
            'Sex' => 'Sex',
            'Birthday' => 'Birthday',
            'race' => 'Race',
            'Nationality' => 'Nationality',
            'Religion' => 'Religion',
            'Weight' => 'Weight',
            'Height' => 'Height',
            'Blood_G' => 'Blood  G',
            'Foot_Step_Num' => 'Foot  Step  Num',
            'ID_Card' => 'Id  Card',
            'districtRelease' => 'District Release',
            'DateOfIssueIdcrad' => 'Date Of Issue Idcrad',
            'DateOfExpiryIdcrad' => 'Date Of Expiry Idcrad',
            'Social_Card' => 'Social  Card',
            'Father' => 'Father',
            'Father_Job' => 'Father  Job',
            'Mother' => 'Mother',
            'Mother_Job' => 'Mother  Job',
            'Parent_Status' => 'Parent  Status',
            'Status' => 'Status',
            'Spouse_prefix' => 'Spouse Prefix',
            'Spouse_Name' => 'Spouse  Name',
            'Spouse_Surname' => 'Spouse  Surname',
            'Spouse_Birthday' => 'Spouse  Birthday',
            'Spouse_career' => 'Spouse Career',
            'Spouse_workplace' => 'Spouse Workplace',
            'Childs' => 'Childs',
            'Relative_Person' => 'Relative  Person',
            'Relative_Person_Tel' => 'Relative  Person  Tel',
            'Address' => 'Address',
            'Moo' => 'Moo',
            'Road' => 'Road',
            'Village' => 'Village',
            'SubDistrict' => 'Sub District',
            'District' => 'District',
            'Province' => 'Province',
            'geography' => 'Geography',
            'Postcode' => 'Postcode',
            'Live_with' => 'Live With',
            'Resident_type' => 'Resident Type',
            'Tel_Num' => 'Tel  Num',
            'Mobile_Num' => 'Mobile  Num',
            'E_Mail' => 'E  Mail',
            'HighSchool' => 'High School',
            'Adv_HighSchool' => 'Adv  High School',
            'Adv_Major' => 'Adv  Major',
            'Bechelor' => 'Bechelor',
            'Bec_Major' => 'Bec  Major',
            'Master' => 'Master',
            'Master_Major' => 'Master  Major',
            'Working_Company' => 'Working  Company',
            'work_type_id' => 'Work Type ID',
            'Position' => 'Position',
            'Department' => 'Department',
            'Section' => 'Section',
            'Team' => 'Team',
            'Level' => 'Level',
            'Salary_Bank' => 'Salary  Bank',
            'Salary_BankNo' => 'Salary  Bank No',
            'SalaryViaBank' => 'Salary Via Bank',
            'promtpay_type' => 'Promtpay Type',
            'promtpay_number' => 'Promtpay Number',
            'Guarantee_Money' => 'Guarantee  Money',
            'Prosonnal_Being' => 'Prosonnal  Being',
            'DayOff' => 'Day Off',
            'Start_date' => 'Start Date',
            'Probation_Date' => 'Probation  Date',
            'End_date' => 'End Date',
            'startWorkTime' => 'Start Work Time',
            'beginLunch' => 'Begin Lunch',
            'endLunch' => 'End Lunch',
            'endWorkTime' => 'End Work Time',
            'Main_EndWork_Reason' => 'Main  End Work  Reason',
            'EndWork_Reason' => 'End Work  Reason',
            'Footnote' => 'Footnote',
            'Emergency_Call_Person' => 'Emergency  Call  Person',
            'Emergency_Relation' => 'Emergency  Relation',
            'Emergency_Phone_Num' => 'Emergency  Phone  Num',
            'can_la' => 'Can La',
            'use_check' => 'Use Check',
            'socialBenifitPercent' => 'Social Benifit Percent',
            'socialBenefitStatus' => 'Social Benefit Status',
            'benefitFundPercent' => 'Benefit Fund Percent',
            'benifitFundStatus' => 'Benifit Fund Status',
            'to_salary_status' => 'To Salary Status',
            'disease' => 'Disease',
            'diseaseDetail' => 'Disease Detail',
            'numberPassportEmp' => 'Number Passport Emp',
            'DateOfIssuePassport' => 'Date Of Issue Passport',
            'DateOfExpiryPassport' => 'Date Of Expiry Passport',
            'Pic_map' => 'Pic Map',
            'Latitude' => 'Latitude',
            'Longitude' => 'Longitude',
            'UPDATE_TIME' => 'Update  Time',
            'UPDATE_BY' => 'Update  By',
            'status_confirmData' => 'Status Confirm Data',
            'image_card' => 'image card',
            'job_type' => 'job type',
        ];
    }

    public function search($params){

        $model = Empdata::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $model
        ]);
        $this->load($params);
        $model->andFilterWhere(['like','Name', $this->Name]);
        $model->andFilterWhere(['like','DataNo', $this->DataNo]);
        $model->andFilterWhere(['like','Code', $this->Code]);
        $model->andFilterWhere(['like','Surname', $this->Surname]);
        $model->andFilterWhere(['like','Nickname', $this->Nickname]);
        $model->andFilterWhere(['like','ID_Card', $this->ID_Card]);
        $model->andFilterWhere(['not like','username','#####Out Off###']);
        //
        return $dataProvider;
    }
}
