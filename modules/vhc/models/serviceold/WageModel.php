<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "wage_model".
 *
 * @property integer $id
 * @property string $abrand_id
 * @property string $agroup_id
 * @property string $model_group
 * @property string $name
 * @property string $amodel_id
 * @property integer $id_wage
 * @property integer $time
 * @property string $erepair_group_id
 * @property integer $STDHOUR
 * @property integer $STDMINUTE
 * @property double $LABOURFEE
 * @property string $STS1
 * @property string $STS2
 * @property string $STS3
 * @property string $emp_id
 * @property string $CONDITIONC
 * @property string $COMBCODE
 * @property string $time_add
 * @property string $status
 */
class WageModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wage_model';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abrand_id', 'agroup_id', 'model_group', 'name', 'amodel_id', 'id_wage', 'time', 'erepair_group_id', 'STDHOUR', 'STDMINUTE', 'LABOURFEE', 'STS1', 'STS2', 'STS3', 'emp_id', 'CONDITIONC', 'COMBCODE', 'time_add', 'status'], 'required'],
            [['name'], 'string'],
            [['id_wage', 'time', 'STDHOUR', 'STDMINUTE'], 'integer'],
            [['LABOURFEE'], 'number'],
            [['abrand_id', 'agroup_id', 'model_group', 'amodel_id', 'erepair_group_id', 'STS1', 'STS2', 'CONDITIONC', 'COMBCODE', 'time_add'], 'string', 'max' => 20],
            [['STS3'], 'string', 'max' => 5],
            [['emp_id'], 'string', 'max' => 13],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abrand_id' => 'Abrand ID',
            'agroup_id' => 'Agroup ID',
            'model_group' => 'Model Group',
            'name' => 'Name',
            'amodel_id' => 'Amodel ID',
            'id_wage' => 'Id Wage',
            'time' => 'Time',
            'erepair_group_id' => 'Erepair Group ID',
            'STDHOUR' => 'Stdhour',
            'STDMINUTE' => 'Stdminute',
            'LABOURFEE' => 'Labourfee',
            'STS1' => 'Sts1',
            'STS2' => 'Sts2',
            'STS3' => 'Sts3',
            'emp_id' => 'Emp ID',
            'CONDITIONC' => 'Conditionc',
            'COMBCODE' => 'Combcode',
            'time_add' => 'Time Add',
            'status' => 'Status',
        ];
    }
}
