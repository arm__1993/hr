<?php

namespace app\modules\hr\apihr;

use yii;

use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Department;
use app\modules\hr\models\Section;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Position;
use app\modules\hr\models\Command;
use app\modules\hr\models\RelationPosition;
use app\modules\hr\models\Organizechart;
use app\modules\hr\models\OrganicChart;
use yii\helpers\VarDumper;
use app\api\DBConnect;

class ApiOrganizeChart
{
    private $chart = [];

    public static function getWorking_company($id)
    {
        $data = Workingcompany::find()
            ->where('status <> 99 ')
            ->andWhere('id =' . $id)
            ->asArray()
            ->all();
        return $data;
    }

    public static function getDepartment($id_company)
    {
        $data = Department::find()
            ->where('company = ' . $id_company . ' ')
            ->asArray()
            ->all();

        return $data;
    }

    public static function getSection($id_department)
    {
        $data = Section::find()
            ->where('department = ' . $id_department . ' AND status <> 99')
            ->asArray()
            ->all();

        return $data;
    }

    public static function graphHead($arr, $paramkey, $data)
    {
        $indata = [];
        if (array_key_exists($paramkey, $arr)) {


            //   ini_set("memory_limit",-1);
            $ref = self::graphHead($arr, $arr[$paramkey]['parent'], $data);
            return ($ref == '') ? $arr[$paramkey] : $ref;


        } else {
            return '';
        }
    }


    public static function graphHeadSESSION($arr, $paramkey, $data)
    {
        $indata = [];
        if (array_key_exists($paramkey, $arr)) {


            //   ini_set("memory_limit",-1);
            $ref = self::graphHeadSESSION($arr, $arr[$paramkey]['Leader'], $data);
            return ($ref == '') ? $arr[$paramkey] : $ref;


        } else {
            return '';
        }
    }

    public static function graphHeadbyNode($arr, $arrall, $data)
    {

        $arrsoft = [];
        $arrsoftdata = [];
        foreach ($arrall as $key => $item) {
            if ($key != $arr[0]['ref_id']) {
                $arrsoft[$item['parent']][] = $item;
            }
        }
        ksort($arrsoft);
        foreach ($arrsoft as $key => $value) {
            foreach ($value as $key => $item) {
                $arrsoftdata[] = $item;
            }
        }
        return $arrsoftdata;

    }

    public static function graphNode($arr, $paramkey, $data)
    {

        unset($arr[$paramkey]);
        $bool = false;
        foreach ($arr as $key => $item) {
            if ($item['parent'] == $paramkey) {
                // $data['parentnodedata'] = $item;
                $item['className'] = (trim($item['Emp_name']) != '') ? $item['className'] : 'recruit-enabled';
                $item['className'] = ($item['recruit_status'] == '2') ? 'recruit-disbled' : $item['className'];
                $item['className'] = ($item['recruit_status'] == '99') ? 'recruit-close' : $item['className'];
                $item['className'] = ($item['recruit_status'] == '1' && trim($item['Emp_name']) == '') ? 'recruit-open' : $item['className'];

                $item['classfa'] = ($item['recruit_status'] == '1' && trim($item['Emp_name']) == '') ? '<a id="recruiting" value="' . $item['ref_id'] . '" onclick="showmodal(' . $item['ref_id'] . ',' . $item['id'] . ',1)">สรรหา</a>' : $item['classfa'];
                $item['classfa'] = ($item['recruit_status'] == '2' && trim($item['Emp_name']) == '') ? '<a id="recruiting" value="' . $item['ref_id'] . '" onclick="showmodal(' . $item['ref_id'] . ',' . $item['id'] . ',2)">ปิดสรรหา</a>' : $item['classfa'];
                $item['classfa'] = ($item['recruit_status'] == '99' && trim($item['Emp_name']) == '') ? '<a id="recruiting" value="' . $item['ref_id'] . '" onclick="showmodal(' . $item['ref_id'] . ',' . $item['id'] . ',99)">ปิดตำแหน่ง</a>' : $item['classfa'];

                $arr[$key]['classfa'] = $item['classfa'];
                $arr[$key]['className'] = $item['className'];

                $ref = self::graphNode($arr, $key, $item);
                $data['children'][] = $ref;
            }

        }
        return $data;
    }


    public static function graphNodeSESSION($arr, $paramkey, $data)
    {

        unset($arr[$paramkey]);
        $bool = false;
        foreach ($arr as $key => $item) {
            if ($item['Leader'] == $paramkey) {
                $ref = self::graphNodeSESSION($arr, $key, $item);
                $data['team'][] = $ref;
            }

        }
        return $data;
    }

    public static function getPosition($id_company/*,$id_department,$id_Section*/)
    {
        $data = Position::find()
            ->select(['id'])
            ->where(['=', 'WorkCompany', $id_company])
            ->andWhere('status <> 99')
            ->asArray()
            ->all();
        return $data;
    }

    public static function getPositionName($id_company)
    {
        $data = Position::find()
            ->select(['position.id', 'position.Name', 'position.PositionCode'])
            ->where(['=', 'WorkCompany', $id_company])
            ->andWhere('Status <> 99')
            ->andWhere('addToCommand != 1')
            ->asArray()
            ->all();

        return $data;
    }

    public static function getCommand($idPosition)
    {
        $str_id_in = JOIN(',', $idPosition);
        $arr = [];
        $dataFollower = Command::find()
            ->select(['Leader', 'Follower'])
            ->innerJoin('position', ' command.Follower=position.id')
            ->where('Follower in (' . $str_id_in . ')')
            ->andWhere('command.status <> 99')
            ->andWhere('position.WorkCompany = 999')
            ->asArray()
            ->all();
        return $dataFollower;
    }

    public static function getCommandnode($idPosition)
    {
        $str_id_in = JOIN(',', $idPosition);
        $arr = [];
        $dataFollower = Command::find()
            ->select([
                'command.id as id',
                'command.Leader',
                'command.Follower',
                'command.parent_id',
                'command.command_start',
                'relation_position.NameUse'
            ])
            ->innerJoin('relation_position', 'command.Follower = relation_position.position_id')
            ->where('Follower in (' . $str_id_in . ')')
            ->andWhere('command.status <> 99')
            //->andWhere('relation_position.status <> 99')
            ->asArray()
            ->all();
        // foreach($dataFollower as $item){
        //     $arr[$item['Leader']][] = $item;
        // }
        return $dataFollower;
    }

    public function formatJson($Marray)
    {
        idRef;
        $modelChart = new Organizechart();
        if (count($Marray) != 0) {
            foreach ($Marray as $items) {
                $modelChart->id = $items['id'];
                $modelChart->idReal = $items['Follower'];
                $modelChart->name = $items['NameUse'];
                $modelChart->parent = ($items['command_start'] == 1) ? '' : $items['parent_id'];
                array_push($this->chart, clone $modelChart);

            }
        }
        return idRef;
    }

    public function formatJsonNode($Marray)
    {
        $photopath = Yii::$app->session->get('photopath');
        $chart = [];
        $modelChart = new Organizechart();
        foreach ($Marray as $items) {
            $modelChart->id = $items['id'];
            $modelChart->idReal = $items['id'];
            $modelChart->name = $items['Name'];
            $modelChart->parent = $items['parent'];
            $modelChart->company = $items['Company'];
            $modelChart->Emp_id = $items['Emp_id'];
            $modelChart->Emp_name = $items['Emp_name'];
            $modelChart->drepartment = $items['Drepartment'];
            $modelChart->ref_id = $items['ref_id'];
            $modelChart->active_status = $items['active_status'];
            $modelChart->position = $items['Posision_id'];
            $modelChart->img = $photopath . $items['Pictures_HyperL'];
            array_push($chart, clone $modelChart);
        }
        return $chart;
    }

    public function getchart()
    {
        $a = $this->chart;
        return $this->chart;
    }

    public static function getCommandbyid($id)
    {
        $dataFollower = Command::find()
            ->select([
                'relation_position.id as id',
                'command.Leader',
                'command.Follower',
                'relation_position.NameUse'
            ])
            ->innerJoin('relation_position', 'command.Leader = relation_position.position_id')
            ->where('Leader = ' . $id)
            ->andWhere('command.status <> 99')
            ->asArray()
            ->one();
        $arr[$dataFollower['Leader']][] = $dataFollower;
        return $arr;
    }

    public static function SaveNodeOrg($arrInsert, $company)
    {
        $arrayInsert = [];
        foreach ($arrInsert as $item) {

            $_empID = (array_key_exists("Emp_id", $item)) ? $item['Emp_id'] : 0;
            $_empVal = (is_numeric($_empID)) ? $_empID : 0;

            $arrayInsert[] = [
                'Company' => (array_key_exists("company", $item)) ? (($item['company'] != '') ? $item['company'] : $company) : $company,
                'ref_id' => (array_key_exists("idPosition", $item)) ? $item['idPosition'] : 0,
                'Drepartment' => (array_key_exists("drepartment", $item)) ? $item['drepartment'] : 0,
                'Name' => (array_key_exists("title", $item)) ? $item['title'] : '',
                'Emp_id' => $_empVal,
                'Emp_name' => (array_key_exists("name", $item)) ? $item['name'] : '',
                'parent' => (array_key_exists("parent", $item)) ? ((strlen($item['parent']) >= 3) ? substr($item['parent'], 2) : $item['parent']) : 0,
                'active_status' => (array_key_exists("active_status", $item)) ? ($item['active_status'] + 1) : 0, //TODO : change later if change orgchart tools
                'level' => (array_key_exists("level", $item)) ? $item['level'] : 0,
                'Posision_id' => (array_key_exists("idPosition", $item)) ? $item['idPosition'] : 0,
                'create_date' => date('Y-m-d H:i:s')
            ];
        }
        Yii::$app->dbERP_easyhr_OU->createCommand()->batchInsert(
            'Organic_chart',
            ['Company', 'ref_id', 'Drepartment', 'Name', 'Emp_id', 'Emp_name', 'parent', 'active_status', 'level', 'Posision_id', 'create_date']
            , $arrayInsert)->execute();
    }

    public static function DeleteNodeOrg($id_company)
    {
        if ($id_company != '') {
            $company = (int)$id_company;
            $command = Yii::$app->dbERP_easyhr_OU->createCommand()
                ->update('Organic_chart', ['active_status' => Yii::$app->params['DELETE_STATUS'], 'update_date' => date('Y-m-d H:i:s')], 'company=' . $company . '')
                ->execute();
        }
        return s;
    }

    public static function SaveNode($obj)
    {
        if ($obj . idReal) {
            $obj_Chart = OrganicChart::findOne($obj['idReal']);
            $obj_Chart->Name = $obj['name'];
            $obj_Chart->Posision_id = $obj['position'];
            if ($obj_Chart->validate()) {
                $statusSave = $obj_Chart->save();
            } else {
                $errors = $obj_Chart->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }
        } else {
            echo 'f';
            //TODO : ตัวแปรมีปัญหาป่าว??
            $obj_Chart = new OrganicChart();
            $obj_Chart->Company = null;
            $obj_Chart->Drepartment = null;
            $obj_Chart->ref_id = null;
            $obj_Chart->Name = null;
            $obj_Chart->Emp_id = null;
            $obj_Chart->parent = null;
            $obj_Chart->active_status = null;
            $obj_Chart->level = null;
            $obj_Chart->Posision_id = null;
            if ($obj_Chart->validate()) {
                $statusSave = $obj_Chart->save();
            } else {
                $errors = $obj_Chart->errors;
                echo "model can't validater <pre>";
                print_r($errors);
                echo "</pre>";
            }
        }
    }

    public static function getCommandbyHead($id)
    {
        $dataFollower = Command::find()
            ->select([
                'relation_position.id as id',
                'command.Leader',
                'command.Follower',
                'relation_position.NameUse'
            ])
            ->innerJoin('relation_position', 'command.Leader = relation_position.position_id')
            ->where('Follower = ' . $id)
            ->asArray()
            ->one();
        $arr[$dataFollower['Leader']][] = $dataFollower;
        return $arr;
    }

    public static function getOrgNode($id)
    {
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];
        $where = 'active_status != 99';

        if (!empty($id) && $id != 'all') {

            $where .= ' and Company = ' . $id;
            $data = OrganicChart::find()
                ->select('Organic_chart.*,emp_data.Pictures_HyperL, position.recruit_status, position.Status')
                ->leftJoin($db['ct'] . ".emp_data", "Organic_chart.Emp_id = " . $db['ct'] . ".emp_data.DataNo")
                ->leftJoin($db['ou'] . ".position", "Organic_chart.Posision_id = " . $db['ou'] . ".position.id")
                ->where($where)
                ->asArray()
                ->all();

        } else if (empty($id) || $id == 'all') {
            $data = OrganicChart::find()
                ->select('Organic_chart.*,emp_data.Pictures_HyperL, position.recruit_status, position.Status')
                ->leftJoin($db['ct'] . ".emp_data", "Organic_chart.Emp_id = " . $db['ct'] . ".emp_data.DataNo")
                ->leftJoin($db['ou'] . ".position", "Organic_chart.Posision_id = " . $db['ou'] . ".position.id")
                ->where($where)
                ->asArray()
                ->all();



        }
        else {
            $data = new Organizechart();
        }


        return $data;
    }


    public static function getOrgNodeSESSION($id)
    {
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];
        $where = 'active_status != 99';

        if (!empty($id) && $id != 'all') {

            $where .= ' and Company = ' . $id;
            $data = OrganicChart::find()
                ->select('Organic_chart.*')
                ->leftJoin($db['ct'] . ".emp_data", "Organic_chart.Emp_id = " . $db['ct'] . ".emp_data.DataNo")
                ->leftJoin($db['ou'] . ".position", "Organic_chart.Posision_id = " . $db['ou'] . ".position.id")
                ->where($where)
                ->asArray()
                ->all();

        } else if (empty($id) || $id == 'all') {
            $data = OrganicChart::find()
                ->select('Organic_chart.*,position.Section')
                ->leftJoin($db['ct'] . ".emp_data", "Organic_chart.Emp_id = " . $db['ct'] . ".emp_data.DataNo")
                ->leftJoin($db['ou'] . ".position", "Organic_chart.Posision_id = " . $db['ou'] . ".position.id")
                ->where($where)
                ->asArray()
                ->all();


        } else {
            $data = new Organizechart();
        }


        return $data;
    }


    public static function getOrgNodeEMPid($id)
    {
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];
        $where = 'active_status != 99';

        if (!empty($id) && $id != 'all') {

            $where .= ' and Emp_IDCard = ' . $id;
            $data = OrganicChart::find()
                ->select('Organic_chart.*,emp_data.Pictures_HyperL, position.recruit_status, position.Status')
                ->leftJoin($db['ct'] . ".emp_data", "Organic_chart.Emp_id = " . $db['ct'] . ".emp_data.DataNo")
                ->leftJoin($db['ou'] . ".position", "Organic_chart.Posision_id = " . $db['ou'] . ".position.id")
                ->where($where)
                ->asArray()
                ->all();

        } else if (empty($id) || $id == 'all') {
            $data = OrganicChart::find()
                ->select('Organic_chart.*,emp_data.Pictures_HyperL, position.recruit_status, position.Status')
                ->leftJoin($db['ct'] . ".emp_data", "Organic_chart.Emp_id = " . $db['ct'] . ".emp_data.DataNo")
                ->leftJoin($db['ou'] . ".position", "Organic_chart.Posision_id = " . $db['ou'] . ".position.id")
                ->where($where)
                ->asArray()
                ->all();


        } else {
            $data = new Organizechart();
        }


        return $data;
    }

    public static function removeOrgNode($id)
    {
        $obj_org = OrganicChart::findOne($id);
        $obj_org->active_status = '2';
        $obj_org->Emp_id = null;
        $obj_org->update();
    }

    public static function deletenodeNew($id)
    {


        $orgchart = OrganicChart::find()
            ->where(['ref_id' => $id])
           ->one();
        $orgchartdel = OrganicChart::find()
            ->where(['ref_id' => $id])
            ->asArray()
            ->all();

        $idCommand = Command::find()
            ->where(['Follower' => $id])
            ->asArray()
            ->all();
        foreach ($idCommand as $key => $val){//del_command
            $modelCommand = Command::findOne(['id' => $val['id']]);
            $modelCommand->Status = 99;
            $modelCommand->save();
        }
        foreach ($orgchartdel as $k => $v){//del_position
            $modelposision = Position::findOne(['id' => $v['Posision_id']]);
            $modelposision->addToCommand = '0';
            $modelposision->save();
        }
        if ($orgchart) {
            $orgchart->delete();
        }
    }

    public static function addOrgNode($data)
    {
        //TODO : OrganicChart field ไม่ครบ
        $objChart = new OrganicChart();
        $objChart->id = $data['id'];
        $objChart->Company = $data['Company'];
        $objChart->Drepartment = $data['Drepartment'];
        $objChart->Section = $data['Section'];
        $objChart->Emp_id = $data['Emp_id'];
        $objChart->parent = $data['parent'];
        $objChart->Name = $data['name'];
        $objChart->active_status = $data['active_status'];
        $objChart->level = $data['level'];
        $objChart->Posision_id = $data['Posision_id'];

        if ($objChart->validate()) {
            $statusSave = $objChart->save();
            return $objChart->id;
        } else {
            $errors = $objChart->errors;
            echo "model can't validater <pre>";
            print_r($errors);
            echo "</pre>";
        }

    }

    public static function addOrgNodeNew($data)
    {
        $objChart = new OrganicChart();
        $objChart->Company = $data['company'];
        $objChart->Drepartment = $data['Drepartment'];
        $objChart->ref_id = $data['ref_id'];
        $objChart->Emp_id = $data['Emp_id'];
        $objChart->parent = $data['parent'];
        $objChart->Name = $data['name'];
        $objChart->active_status = $data['active_status'];
        $objChart->level = $data['level'];
        $objChart->Posision_id = $data['idPosition'];
        $objChart->create_date = date('Y-m-d');
        $objChart->update_date = date('Y-m-d');


        if ($objChart->validate()) {
            $databd = OrganicChart::findOne($data['id']);
            if (($databd != null || $databd != '') && $objChart->ref_id != '') {
                //$databd->id = $data['id'];

                $databd->ref_id = $objChart->ref_id;
                $databd->Name = $objChart->Name;
                $databd->Posision_id = $objChart->Posision_id;
                $command = new Command();
                $command->Leader = $data['parent'];
                $command->Follower = $data['ref_id'];
                $command->Leader_id_card = '-';
                $command->Follower_id_card = '-';
                $command->command_start = '0';
                $command->Command_start_use_date = date('Y-m-d H:i:s');
                $command->Status = 1;
                $command->save();


                $idPosition = Position::findOne($data['idPosition']);
                $idPosition->addToCommand = '1';
                $idPosition->save();


                $databd->save();
            } else {
                $objChart->id = null;

                $objChart->save();
            }
            return $objChart->id;
        } else {
            $errors = $objChart->errors;
            echo "model can't validater <pre>";
            print_r($errors);
            echo "</pre>";
        }

    }

    public function SearchHead($dataFollower, $idhead = '')
    {
        foreach ($dataFollower as $item) {
            if ($idhead == '') {
                if (@isset($dataFollower[$item])) {
                    $idhead = $item;
                    SearchHead($dataFollower, $idhead);
                } else {
                    return $item;
                }
            } else {
                if (@isset($dataFollower[$idhead])) {
                    $idhead = $item;
                    SearchHead($dataFollower, $idhead);
                } else {
                    return $idhead;
                }
            }

        }
    }

}

?>