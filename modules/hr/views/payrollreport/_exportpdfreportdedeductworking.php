<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;
$modelCommpaneyAdddeduct = $session->get('modelCommpaneyAdddeduct');
$modelCommpaneySSO = $session->get('modelCommpaneySSO');
$querySalaryTotalThisMonth4 = $session->get('querySalaryTotalThisMonth4');
$statusSSO = $session->get('statusSSO');
$monthselect = $session->get('monthselect');
$modelTemp = $session->get('modelTemp');
$modelWorkingcompany = $session->get('modelWorkingcompany');


?>
<h3 style="text-align:center;">รายละเอียดรายงานเงินเดือน</h3>


<div class="row">
    <div class="row">
    </div>
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-6">
            <h3><?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                //print_r($getMonth);
                $monthGetReport1 = $getMonth['monthresult'];
                $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
                $yearGetReport1 = $getMonth['year'];
                ?>รายงานสรุปผลเงินเดือนรายบุคคล (รายการเพิ่ม) จ่าย <?php echo $resultMonthReport1;
                echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h3>
        </div>
    </div>
    <?php if ($querySalaryTotalThisMonth4) { ?>
        <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example1_wrapper" width="80%">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                    aria-sort="ascending" width="30%">
                    รายการเพิ่ม
                </th>
                <?php foreach ($modelWorkingcompany as $key => $value) { ?>
                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                        aria-sort="ascending" width="10%">
                        <?php echo $value ['short_name']; ?>
                    </th>
                <?php } ?>
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                    aria-sort="ascending" width="30%">
                    รวม
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if ($statusSSO) { ?>
                <tr>
                    <td>ประกันสังคม</td>
                    <?php

                    foreach ($modelWorkingcompany as $key2 => $values) {
                        ?>
                        <td align="right"><?php
                            $sumSSO = '0.00';
                            foreach ($modelCommpaneySSO AS $key => $valueResultSSO) {
                                switch ($valueResultSSO['WAGE_WORKING_COMPANY']) {
                                    case $valueResultSSO['WAGE_WORKING_COMPANY'] == $values['id'];
                                        $sumSSO = $valueResultSSO['SUM_ADD_DEDUCT_HIS'];
                                        break;
                                }
                            }
                            echo Helper::displayDecimal($sumSSO);
                            $arrSumSSO[] = $sumSSO;

                            ?></td>
                    <?php } ?>
                    <td><?php echo Helper::displayDecimal(array_sum($arrSumSSO)); ?></td>
                </tr>
            <?php } ?>
            <?php foreach ($modelTemp as $key1 => $value) {
                $somrow = [];
                ?>
                <tr>
                    <td><?php echo $value['ADD_DEDUCT_TEMPLATE_NAME']; ?></td>
                    <?php foreach ($modelWorkingcompany as $key2 => $values) { ?>
                        <td align="right"><?php
                            $setZero = "0.00";
                            foreach ($modelCommpaneyAdddeduct AS $key => $valueResult) {
                                switch ($valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] AND $valueResult['WAGE_WORKING_COMPANY']) {
                                    case $valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $value['ADD_DEDUCT_TEMPLATE_ID'] && $valueResult['WAGE_WORKING_COMPANY'] == $values['id'];
                                        // echo "//////".$valueResult['SUM_ADD_DEDUCT_HIS']."//////";
                                        $setZero = $valueResult['SUM_ADD_DEDUCT_HIS'];
                                        break;
                                }

                            }
                            echo Helper::displayDecimal($setZero);
                            $somrow[] = $setZero;

                            ?></td>
                    <?php } ?>
                    <td><?php echo Helper::displayDecimal(array_sum($somrow)); ?></td>
                </tr>
                <?php ?>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

</div>
