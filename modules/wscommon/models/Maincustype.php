<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "MainCus_Type".
 *
 * @property integer $CusTypeID
 * @property string $Cus_Type
 * @property string $CusType_Remark
 * @property string $Who_Edit_ID
 * @property string $Who_Edit_posCode
 * @property string $TimeUpdate
 */
class Maincustype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MainCus_Type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Who_Edit_posCode'], 'required'],
            [['Cus_Type'], 'string', 'max' => 30],
            [['CusType_Remark'], 'string', 'max' => 250],
            [['Who_Edit_ID'], 'string', 'max' => 13],
            [['Who_Edit_posCode'], 'string', 'max' => 20],
            [['TimeUpdate'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CusTypeID' => 'Cus Type ID',
            'Cus_Type' => 'Cus  Type',
            'CusType_Remark' => 'Cus Type  Remark',
            'Who_Edit_ID' => 'Who  Edit  ID',
            'Who_Edit_posCode' => 'ตำแหน่ง',
            'TimeUpdate' => 'Time Update',
        ];
    }
}
