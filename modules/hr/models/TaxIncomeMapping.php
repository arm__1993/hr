<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "tax_income_mapping".
 *
 * @property integer $id
 * @property integer $tax_income_section_id
 * @property integer $add_deduct_template_id
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class TaxIncomeMapping extends \yii\db\ActiveRecord
{
    protected $_pageSize;

    function __construct() {
        $this->_pageSize = Yii::$app->params['PAGE_SIZE'];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_income_mapping';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_income_section_id', 'add_deduct_template_id'], 'required'],
            [['tax_income_section_id', 'add_deduct_template_id', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tax_income_section_id' => 'Tax Income Section ID',
            'add_deduct_template_id' => 'Add Deduct Template ID',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return TaxIncomeMappingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaxIncomeMappingQuery(get_called_class());
    }
    public function search($params)
    {

        $query = TaxIncomeMapping::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'tax_income_section_id', $this->tax_income_section_id],
                               ['like', 'add_deduct_template_id', $this->add_deduct_template_id]
                                );

        return $dataProvider;
    }

}
