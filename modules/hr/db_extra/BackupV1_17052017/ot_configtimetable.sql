-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 15, 2017 at 04:45 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_configtimetable`
--

CREATE TABLE `ot_configtimetable` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `profile_name` varchar(200) NOT NULL COMMENT 'ชื่อโปรไฟล์',
  `time_start` time DEFAULT NULL COMMENT 'เวลาเริ่มคิดโอที',
  `time_end` time DEFAULT NULL COMMENT 'เวลาสิ้นสุดการคิดโอที ถ้าเป็นค่า null แสดงว่าไม่ระบุเวลาสิ้นสุด',
  `budget` decimal(8,2) DEFAULT NULL COMMENT 'จำนวนเงินที่ได้รับในช่วงเวลา',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0, ',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ot_configtimetable`
--

INSERT INTO `ot_configtimetable` (`id`, `profile_name`, `time_start`, `time_end`, `budget`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, 'ค่าเดินทางหลังเวลา 17:00 น.', '17:01:00', '18:00:00', '40.00', 1, NULL, NULL, NULL, NULL),
(2, 'ค่าเดินทางหลังเวลา 18:00 น.', '18:01:00', NULL, '40.00', 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_configtimetable`
--
ALTER TABLE `ot_configtimetable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ot_configtimetable`
--
ALTER TABLE `ot_configtimetable`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
