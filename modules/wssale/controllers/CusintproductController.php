<?php

namespace app\modules\wssale\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wssale\models\Cusint;
use yii\helpers\ArrayHelper;



class CusintproductController extends ActiveController
{
    public $modelClass = 'app\modules\wssale\models\Cusint';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}

}
