<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "children".
 *
 * @property integer $db_log_id
 * @property string $db_do_time
 * @property string $db_do_by
 * @property string $db_do_log
 * @property string $db_do_sql
 * @property integer $id
 * @property string $emp_id
 * @property string $sex
 * @property string $prefix
 * @property string $name
 * @property string $surname
 * @property integer $education_status
 * @property string $school
 * @property string $class
 * @property integer $grade
 * @property integer $age
 * @property string $birthday
 * @property integer $status
 */
class Children extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'children';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['db_log_id', 'education_status', 'grade', 'age', 'status'], 'integer'],
            [['db_do_time', 'birthday'], 'safe'],
            [['db_do_sql', 'school'], 'string'],
            [['db_do_by', 'sex'], 'string', 'max' => 20],
            [['db_do_log'], 'string', 'max' => 200],
            [['emp_id'], 'string', 'max' => 13],
            [['prefix'], 'string', 'max' => 50],
            [['name', 'surname', 'class'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'db_log_id' => 'Db Log ID',
            'db_do_time' => 'Db Do Time',
            'db_do_by' => 'Db Do By',
            'db_do_log' => 'Db Do Log',
            'db_do_sql' => 'Db Do Sql',
            'id' => 'ID',
            'emp_id' => 'Emp ID',
            'sex' => 'เพศบุตร',
            'prefix' => 'คำนำหน้าบุตร',
            'name' => 'ชื่อบุตร',
            'surname' => 'นามสกุลบุตร',
            'education_status' => 'Education Status',
            'school' => 'School',
            'class' => 'Class',
            'grade' => 'Grade',
            'age' => 'Age',
            'birthday' => 'Birthday',
            'status' => 'Status',
        ];
    }
}
