<?php

namespace app\modules\wssale\controllers;
use app\modules\wssale\models\Cusintcusrecommend;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;


class CusintcusrecommendController extends ActiveController
{
   public $modelClass = 'app\modules\wssale\models\Cusintcusrecommend';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}

	public function actionSearchcusintcusrecommendformobile()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $datacussearchCusintcusrecommendformobile = new Cusintcusrecommend();
        $showdatasearchCusintcusrecommendformobile = $datacussearchCusintcusrecommendformobile->searchcusintcusrecommendformobile($cusno);
        return $showdatasearchCusintcusrecommendformobile;
    }



}
