-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2017 at 10:54 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_structure`
--

CREATE TABLE `tax_income_structure` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'รหัส',
  `income_floor` decimal(15,2) NOT NULL COMMENT 'ช่วงเงินได้ เริ่มต้น',
  `income_ceil` decimal(15,2) NOT NULL COMMENT 'ช่วงเงินได้ สิ้นสุด',
  `tax_rate` decimal(4,2) NOT NULL COMMENT 'อัตราภาษี',
  `gross_step` decimal(15,2) DEFAULT NULL COMMENT 'ภาษีแต่ละขั้นเงินได้สุทธิ',
  `max_accumulate` decimal(15,2) DEFAULT NULL COMMENT 'ภาษีสะสมสูงสุดของขั้น',
  `from_year` int(11) NOT NULL COMMENT 'เริ่มใช้ตั้งแต่ปี (คศ)',
  `to_year` int(11) DEFAULT NULL COMMENT 'จนถึงปี',
  `remark` varchar(255) DEFAULT NULL COMMENT 'หมายเหตุ',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_income_structure`
--
ALTER TABLE `tax_income_structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_year` (`from_year`),
  ADD KEY `to_year` (`to_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_income_structure`
--
ALTER TABLE `tax_income_structure`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
