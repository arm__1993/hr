-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 03, 2017 at 09:15 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_approved_history`
--

CREATE TABLE `ot_approved_history` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `ot_requestmaster_id` int(11) NOT NULL COMMENT 'รหัสตารางการเสนอขอโอที',
  `ot_requestdetail_id` int(11) NOT NULL COMMENT 'รหัสรายละเอียดการขออนุมัติโอที',
  `field_name` varchar(100) NOT NULL COMMENT 'ชื่อคอลัมน์',
  `old_data` varchar(100) DEFAULT NULL COMMENT 'ข้อมูลเดิม',
  `new_data` varchar(100) DEFAULT NULL COMMENT 'ข้อมูลใหม่',
  `history_datetime` datetime NOT NULL COMMENT 'วันที่เกิดประวัติ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_approved_history`
--
ALTER TABLE `ot_approved_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ot_requestmaster_id` (`ot_requestmaster_id`),
  ADD KEY `ot_requestdetail_id` (`ot_requestdetail_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ot_approved_history`
--
ALTER TABLE `ot_approved_history`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
