<?php

namespace app\modules\hr\apihr;

use app\api\Helper;
use app\api\Utility;
use app\modules\hr\models\DetailRecord;
use app\modules\hr\models\HeaderRecord;
use yii;
use yii\db\Expression;
use app\modules\hr\models\SsoPaidHead;
use app\modules\hr\models\SsoPaidDetail;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\TaxPndHead;
use app\modules\hr\models\TaxPndDetail;

Class ApiSSOExport
{
    private $handle;
    private $file;
    private $location;
    private $data = [];
    private $data_head = [];
    private $type_data;
    private $Company_name;
    private $information_head;
    private $information;
    private $dataH = [];
    private $dataDetail = [];


    public static function InsertDataSSO135($connection, $arrAllCompany, $arrCompanySum, $arrAllEmp, $pay_date)
    {
        $all_dbcompany = ApiHr::getWorking_company();
        $_table = "Detail_Record";
        $detailColumns = DetailRecord::getTableSchema()->getColumnNames();

        $eff = $deff = 0;
        foreach ($arrAllCompany as $CompanyID => $item) {


            //  print_r($item);

            $arrDBComp = $all_dbcompany[$CompanyID];
            $arrSumComp = $arrCompanySum[$CompanyID];

            $acc_no = Utility::removeDashIDcard($arrDBComp['soc_acc_number']);

            $RECORD_TYPE = '1';
            $ACC_NO = $acc_no;
            $BRANCH_NO = '000000'; //Fixed
            $PAID_DATE = $item['PAID_DATE'];
            $PAID_PERIOD = $item['PAID_PERIOD'];
            $COMPANY_NAME = $arrDBComp['name'];
            $RATE = ApiSSOExport::encode_RATE($item['RATE'], 4);
            $TOTAL_EMPLOYEE = ApiSSOExport::encode_TOTAL_EMPLOYEE($arrSumComp['SUM_EMPLOYEE'], 6);
            $TOTAL_WAGES = ApiSSOExport::encode_TOTAL_WAGES($arrSumComp['SUM_WAGES'], 15);
            $TOTAL_PAID = ApiSSOExport::encode_TOTAL_PAID(($arrSumComp['SUM_PAID_AMOUNT'] + $arrSumComp['SUM_EMPLOYER_PAID']), 14);
            $TOTAL_PAID_BY_EMPLOYEE = ApiSSOExport::encode_TOTAL_PAID_BY_EMPLOYEE($arrSumComp['SUM_PAID_AMOUNT'], 12);
            $TOTAL_PAID_BY_EMPLOYER = ApiSSOExport::encode_TOTAL_PAID_BY_EMPLOYER($arrSumComp['SUM_EMPLOYER_PAID'], 12);

            $YEARS = $item['YEAR'];
            $MONTHS = $item['MONTH'];
            $DEPARTMENT_ID = 0;
            $COMPANY_ID = $CompanyID;

            $sql = "INSERT INTO Header_Record VALUES(null,'$RECORD_TYPE','$ACC_NO','$BRANCH_NO','$PAID_DATE','$PAID_PERIOD','$COMPANY_NAME','$RATE','$TOTAL_EMPLOYEE','$TOTAL_WAGES','$TOTAL_PAID','$TOTAL_PAID_BY_EMPLOYEE','$TOTAL_PAID_BY_EMPLOYER','$YEARS','$MONTHS','$DEPARTMENT_ID','$COMPANY_ID','$pay_date')";
            $e = $connection->createCommand($sql)->execute();
            if ($e) {
                $ID_Header = $connection->getLastInsertID();
                $arrEmp = $arrAllEmp[$CompanyID];

                foreach ($arrEmp as $key => $emp) {

                    $Detail_Record = new DetailRecord();
                    $Detail_Record->ID_Header = $ID_Header;
                    $Detail_Record->RECORD_TYPE = '2';
                    $Detail_Record->SSO_ID = $emp['SSO_ID'];
                    $Detail_Record->PREFIX = $emp['PREFIX'];
                    $Detail_Record->FNAME = $emp['FNAME'];
                    $Detail_Record->LNAME = $emp['LNAME'];
                    $Detail_Record->WAGES = ApiSSOExport::encode_TOTAL_WAGES($emp['WAGES'], 14);
                    $Detail_Record->PAID_AMOUNT = ApiSSOExport::encode_TOTAL_PAID_BY_EMPLOYEE($emp['PAID_AMOUNT'], 12);
                    $Detail_Record->BLANK = '';
                    $Detail_Record->COMPANY_ID = (string)$COMPANY_ID;
                    $Detail_Record->pay_date = $pay_date;
                    $Detail_Record->save();


                }

//                foreach ($arrEmp as  $emp) {
//                    $arrData[] = [
//                        $detailColumns[0] => null,//id
//                        $detailColumns[1] => $ID_Header,
//                        $detailColumns[2] => '2',
//                        $detailColumns[3] => $emp['SSO_ID'],//$SSO_ID,
//                        $detailColumns[4] => $emp['PREFIX'],
//                        $detailColumns[5] => $emp['FNAME'],//'1',
//                        $detailColumns[6] => $emp['LNAME'],//'1',
//                        $detailColumns[7] => ApiSSOExport::encode_TOTAL_WAGES($emp['WAGES'],14),
//                        $detailColumns[8] => ApiSSOExport::encode_TOTAL_PAID_BY_EMPLOYEE($emp['PAID_AMOUNT'],12),
//                        $detailColumns[9] => '',
//                        $detailColumns[10] => $COMPANY_ID,
//                        $detailColumns[11] => $pay_date,
//                    ];
//                }
//                $deff += $connection->createCommand()->batchInsert($_table, $detailColumns, $arrData)->execute();
            }
        }


//        return [
//            'e'=>$eff,
//            'de' => $deff,
//        ];
    }


    //function Insert into Head_Record table
    public static function InsertHeadRecord135($connection, $arrHead)
    {


//        $RECORD_TYPE = ApiSSOExport::tranform("1", 1); //Fixed = 1
//        $ACC_NO = ApiSSOExport::tranform($arrHead['ACC_NO'], 10);
//        $BRANCH_NO = ApiSSOExport::tranform($arrHead['BRANCH_NO'], 6);
//        $PAID_DATE = ApiSSOExport::tranform($arrHead['PAID_DATE'], 6);
//        $PAID_PERIOD = ApiSSOExport::tranform($arrHead['PAID_PERIOD'], 4);
//        $COMPANY_NAME = ApiSSOExport::tranform($arrHead['COMPANY_NAME'], 45);
//        $RATE = ApiSSOExport::tranform($arrHead['RATE'], 4);
//        $TOTAL_EMPLOYEE = ApiSSOExport::tranform($arrHead['TOTAL_EMPLOYEE'], 6);
//        $TOTAL_WAGES = ApiSSOExport::tranform($arrHead['TOTAL_WAGES'], 15);
//        $TOTAL_PAID = ApiSSOExport::tranform($arrHead['TOTAL_PAID'], 14);
//        $TOTAL_PAID_BY_EMPLOYEE = ApiSSOExport::tranform($arrHead['TOTAL_PAID_BY_EMPLOYEE'], 12);
//        $TOTAL_PAID_BY_EMPLOYER = ApiSSOExport::tranform($arrHead['TOTAL_PAID_BY_EMPLOYER'], 12);

        $YEARS = '';
        $MONTHS = '';
        $DEPARTMENT_ID = '';
        $COMPANY_ID = '';

        $_table = "Header_Record";
        $addDeDuctColumns = HeaderRecord::getTableSchema()->getColumnNames();

        $arrData = null;

        // foreach ($data as $no => $inf) {
        for ($i = 1; $i <= 10; $i++) {

            $arrData[] = [
                $addDeDuctColumns[0] => null,//id
                $addDeDuctColumns[1] => '8',
                $addDeDuctColumns[2] => 'sdsad',//'1',
                $addDeDuctColumns[3] => '333456789756',//$SSO_ID,
                $addDeDuctColumns[4] => '1',
                $addDeDuctColumns[5] => '1',
                $addDeDuctColumns[6] => '1',
                $addDeDuctColumns[7] => '1',
                $addDeDuctColumns[8] => '1',
                $addDeDuctColumns[9] => '1',
                $addDeDuctColumns[10] => 'rrr',
                $addDeDuctColumns[11] => 'rrr',
                $addDeDuctColumns[12] => 'qweqw',
                $addDeDuctColumns[13] => 'weqwe',
                $addDeDuctColumns[14] => 3,
                $addDeDuctColumns[15] => 2,
                $addDeDuctColumns[16] => 1,
            ];
        }


        if (count($arrData) > 0) {
            $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();
        }


    }

    //Input decimal  1234.07 ==> Out put string 123407
    public static function cleanFormatter($decimal)
    {
        $a = Helper::displayDecimal($decimal, 2);
        $b = str_replace(',', '', $a);
        $c = str_replace('.', '', $b);
        return $c;
    }

    //Input @interger 3 OR 5 OR 7
    public static function encode_RATE($rate, $length)
    {
        $a = (strlen($rate) == 2) ? $rate : '0' . $rate;
        return str_pad($a, $length, '0', STR_PAD_RIGHT);
    }

    public static function encode_TOTAL_EMPLOYEE($total_employee, $length)
    {
        return str_pad($total_employee, $length, '0', STR_PAD_LEFT);
    }


    public static function encode_TOTAL_WAGES($total_wages, $length)
    {
        $aa = self::cleanFormatter($total_wages);
        return str_pad($aa, $length, '0', STR_PAD_LEFT);
    }

    public static function encode_TOTAL_PAID($total_paid, $length)
    {
        $aa = self::cleanFormatter($total_paid);
        return str_pad($aa, $length, '0', STR_PAD_LEFT);
    }

    public static function encode_TOTAL_PAID_BY_EMPLOYEE($total_paid_by_employee, $length)
    {
        $aa = self::cleanFormatter($total_paid_by_employee);
        return str_pad($aa, $length, '0', STR_PAD_LEFT);
    }

    public static function encode_TOTAL_PAID_BY_EMPLOYER($total_paid_by_employer, $length)
    {
        $aa = self::cleanFormatter($total_paid_by_employer);
        return str_pad($aa, $length, '0', STR_PAD_LEFT);
    }


    //function Insert into Detail_Record table
    public static function InsertDetailRecord135($arrSalaryAll, $connection)
    {
        $emp_data = ApiPDF::get_emp_data_all();
        $all_company = ApiHr::getWorking_company();


        $_table = "Detail_Record";
        $addDeDuctColumns = DetailRecord::getTableSchema()->getColumnNames();


        $arrData = null;

        foreach ($arrSalaryAll as $getallsalary) {
            $company_data = $all_company[$getallsalary['MainWorking_Company']];
            $emp_dataall = $emp_data[$getallsalary['ID_Card']];

            if ($emp_dataall['Be'] == 'นาย') {
                $PREFIX = '003';
            } elseif ($emp_dataall['Be'] == 'นาง') {
                $PREFIX = '004';
            } elseif ($emp_dataall['Be'] == 'นางสาว') {
                $PREFIX = '005';
            }


            // $commpny = $company_data['name'];


            $RECORD_TYPE = ApiSSOExport::tranform("2", 1);  //Fixed = 2
            $SSO_ID = ApiSSOExport::tranform($getallsalary['ID_Card'], 13);//เลขที่บตัรประชาชน
            $PREFIXS = ApiSSOExport::tranform($PREFIX, 3);//คานาหน้าชื่อ
            $FNAME = ApiSSOExport::tranform($getallsalary['first_name'], 30);//ชื่อผ้ปูระกนัตน
            $LNAME = ApiSSOExport::tranform($getallsalary['last_name'], 35);//นามสกลุผ้ปู ระกนัตน
            //$WAGES = ApiSSOExport::tranform($arrDetail['WAGES'],14);//คา่จ้าง
            //   $PAID_AMOUNT = ApiSSOExport::tranform($arrDetail['PAID_AMOUNT'],12);//จานวนเงินสมทบ
            $BLANK = ApiSSOExport::tranform("", 27);  //Fixed SPACE
            $COMPANY_ID = $getallsalary['MainWorking_Company'];
            // $ID_Header = $HeadID;


            $arrData[] = [
                $addDeDuctColumns[0] => null,//id
                $addDeDuctColumns[1] => 1,
                $addDeDuctColumns[2] => $RECORD_TYPE,//'1',
                $addDeDuctColumns[3] => $SSO_ID,//'333456789756',//$SSO_ID,
                $addDeDuctColumns[4] => $PREFIXS,//'1',
                $addDeDuctColumns[5] => $FNAME,//'1',
                $addDeDuctColumns[6] => $LNAME,//'1',
                $addDeDuctColumns[7] => '1',
                $addDeDuctColumns[8] => '1',
                $addDeDuctColumns[9] => ' ',
                $addDeDuctColumns[10] => $COMPANY_ID,
            ];
        }


        if (count($arrData) > 0) {
            $connection->createCommand()->batchInsert($_table, $addDeDuctColumns, $arrData)->execute();
        }


    }


    public static function selectMinus5Year()
    {
        $yearStart = date('Y');
        $yearEnd = date('Y', strtotime('- 5 year'));
        $yearRange = Range($yearStart, $yearEnd);
        return $yearRange;
    }

    public static function getcompany($branch)
    {
        $company = Workingcompany::find()
            ->where(['=', 'id', $branch])
            ->asArray()
            ->all();
        return $company;
    }

    public static function select_company()
    {
        /*        $data = SsoPaidHead::find()
                ->all();
                return $data;*/

        $data = HeaderRecord::find()->all();
        return $data;
    }

    public static function sps10($id_company, $year, $months)
    {
        /*        $data = SsoPaidHead::find()
                ->where(['=','BRANCH_NO',$id_company])
                ->andWhere(['=','YEARS',$year])
                ->andWhere(['=','MONTHS',$months])
                ->asArray()
                ->all();*/

        $data = HeaderRecord::find()
            ->where(['=', 'BRANCH_NO', $id_company])
            ->andWhere(['=', 'YEARS', $year])
            ->andWhere(['=', 'MONTHS', $months])
            ->asArray()
            ->all();

        return $data;
    }

    public static function find_company($id_company, $month, $year)
    {
        //$id_company = 2;
        /*        $data_head = SsoPaidHead::find()
                ->where(['YEARS'=>$year,'MONTHS'=>$month,'COMPANY_ID'=>$id_company])
                ->asArray()
                ->all();*/
        // echo "<pre>";
        // print_r($data_head);
        // exit();


        $data_head = HeaderRecord::find()
            ->where(['YEARS' => $year, 'MONTHS' => $month, 'COMPANY_ID' => $id_company])
            ->asArray()
            ->all();

        if (count($data_head) == 0) {
            return 0;
        }
        return $data_head;
    }

    public static function find_detail($id_company)
    {

        //$id_company = 2;
        /*        $data_detail = SsoPaidDetail::find()
                ->where(['COMPANY_ID'=>[$id_company]])
                ->asArray()
                ->all();*/

        $data_detail = DetailRecord::find()
            ->where(['COMPANY_ID' => [$id_company]])
            ->asArray()
            ->all();

        return $data_detail;
    }

    public static function mb_str_pad($input, $pad_length, $pad_string = " ", $pad_style = STR_PAD_RIGHT, $encoding = "UTF-8")
    {
        if (strlen($input) > 0) {
            return str_pad($input, strlen($input) - mb_strlen($input, $encoding) + $pad_length,
                $pad_string,
                $pad_style);
        } else {
            return '';
        }
    }

    public static function tranform($value, $length)
    {
        if (strlen($value) > 0) {
            $tranfrom = mb_substr($value, -1 * $length, $length, 'utf-8');
            $a_name = ApiSSOExport::mb_str_pad($tranfrom, $length, ' ', STR_PAD_RIGHT);//
            return $a_name;
        } else {
            return '';
        }
    }

    public static function tranform135($value, $length)
    {
        if (strlen($value) > 0) {
            $tranfrom = mb_substr($value, -1 * $length, $length, 'utf-8');
            $a_name = ApiSSOExport::mb_str_pad($tranfrom, $length, ' ', STR_PAD_RIGHT);//
            return $a_name;
        } else {
            return '';
        }
    }


    public static function type_data($id_type)
    {

        $type_data = $id_type;
        return $type_data;
    }


    public static function builddata_head1($id_company, $month, $year)
    {
        $arrData = ApiSSOExport::find_company($id_company, $month, $year);
        $arr_company = array();
        foreach ($arrData as $value) {

            $RECORD_TYPE = ApiSSOExport::tranform($value['RECORD_TYPE'], 1);
            $ACC_NO = ApiSSOExport::tranform($value['ACC_NO'], 10);
            $BRANCH_NO = ApiSSOExport::tranform($value['BRANCH_NO'], 6);
            $PAID_DATE = ApiSSOExport::tranform($value['PAID_DATE'], 6);
            $PAID_PERIOD = ApiSSOExport::tranform($value['PAID_PERIOD'], 4);
            $COMPANY_NAME = ApiSSOExport::tranform($value['COMPANY_NAME'], 45);
            $RATE = ApiSSOExport::tranform($value['RATE'], 4);
            $TOTAL_EMPLOYEE = ApiSSOExport::tranform($value['TOTAL_EMPLOYEE'], 6);
            $TOTAL_WAGES = ApiSSOExport::tranform($value['TOTAL_WAGES'], 15);
            $TOTAL_PAID = ApiSSOExport::tranform($value['TOTAL_PAID'], 14);
            $TOTAL_PAID_BY_EMPLOYEE = ApiSSOExport::tranform($value['TOTAL_PAID_BY_EMPLOYEE'], 12);
            $TOTAL_PAID_BY_EMPLOYER = ApiSSOExport::tranform($value['TOTAL_PAID_BY_EMPLOYER'], 12);
            $show =
                $RECORD_TYPE .
                $ACC_NO .
                $BRANCH_NO .
                $PAID_DATE .
                $PAID_PERIOD .
                $COMPANY_NAME .
                $RATE .
                $TOTAL_EMPLOYEE .
                $TOTAL_WAGES .
                $TOTAL_PAID .
                $TOTAL_PAID_BY_EMPLOYEE .
                $TOTAL_PAID_BY_EMPLOYER .
                "\n";
            array_push($arr_company, $show);
        }
        $information_head = implode("", $arr_company);

        return $information_head;
    }


    public static function builddata1($id_company)
    {
        //$id_company=1;
        $arrData = ApiSSOExport::find_detail($id_company);
        $person = array();

        foreach ($arrData as $value) {
            $RECORD_TYPE = ApiSSOExport::tranform($value['RECORD_TYPE'], 1);
            $SSO_ID = ApiSSOExport::tranform($value['SSO_ID'], 13);
            $PREFIX = ApiSSOExport::tranform($value['PREFIX'], 3);
            $FNAME = ApiSSOExport::tranform($value['FNAME'], 30);
            $LNAME = ApiSSOExport::tranform($value['LNAME'], 35);
            $WAGES = ApiSSOExport::tranform($value['WAGES'], 14);
            $PAID_AMOUNT = ApiSSOExport::tranform($value['PAID_AMOUNT'], 12);
            $BLANK = ApiSSOExport::tranform($value['BLANK'], 27);
            $show =
                $RECORD_TYPE
                . $SSO_ID
                . $PREFIX
                . $FNAME
                . $LNAME
                . $WAGES
                . $PAID_AMOUNT
                . $BLANK
                . "\n";
            array_push($person, $show);
        }
        $information = implode("", $person);

        return $information;
    }


    public static function builddata_head2($id_company, $month, $year)
    {

        $arrData = ApiSSOExport::find_company($id_company, $month, $year);

        $arr_company = array();

        foreach ($arrData as $value) {

            $RECORD_TYPE = ApiSSOExport::tranform($value['RECORD_TYPE'], 1);
            $ACC_NO = ApiSSOExport::tranform($value['ACC_NO'], 10);
            $BRANCH_NO = ApiSSOExport::tranform($value['BRANCH_NO'], 6);
            $PAID_DATE = ApiSSOExport::tranform($value['PAID_DATE'], 6);
            $PAID_PERIOD = ApiSSOExport::tranform($value['PAID_PERIOD'], 4);
            $COMPANY_NAME = ApiSSOExport::tranform($value['COMPANY_NAME'], 45);
            $RATE = ApiSSOExport::tranform($value['RATE'], 4);
            $TOTAL_EMPLOYEE = ApiSSOExport::tranform($value['TOTAL_EMPLOYEE'], 6);
            $TOTAL_WAGES = ApiSSOExport::tranform($value['TOTAL_WAGES'], 12);
            $TOTAL_PAID = ApiSSOExport::tranform($value['TOTAL_PAID'], 12);
            $TOTAL_PAID_BY_EMPLOYEE = ApiSSOExport::tranform($value['TOTAL_PAID_BY_EMPLOYEE'], 10);
            $TOTAL_PAID_BY_EMPLOYER = ApiSSOExport::tranform($value['TOTAL_PAID_BY_EMPLOYER'], 10);
            $show =
                $RECORD_TYPE .
                $ACC_NO .
                $BRANCH_NO .
                $PAID_DATE .
                $PAID_PERIOD .
                $COMPANY_NAME .
                $RATE .
                $TOTAL_EMPLOYEE .
                $TOTAL_WAGES .
                $TOTAL_PAID .
                $TOTAL_PAID_BY_EMPLOYEE .
                $TOTAL_PAID_BY_EMPLOYER .
                "\n";
            array_push($arr_company, $show);
        }
        $information_head = implode("", $arr_company);

        return $information_head;
    }

    public static function builddata2($id_company)
    {

        $arrData = ApiSSOExport::find_detail($id_company);
        $person = array();

        foreach ($arrData as $value) {
            $RECORD_TYPE = ApiSSOExport::tranform($value['RECORD_TYPE'], 1);
            $SSO_ID = ApiSSOExport::tranform($value['SSO_ID'], 13);
            $PREFIX = ApiSSOExport::tranform($value['PREFIX'], 3);
            $FNAME = ApiSSOExport::tranform($value['FNAME'], 30);
            $LNAME = ApiSSOExport::tranform($value['LNAME'], 35);
            $WAGES = ApiSSOExport::tranform($value['WAGES'], 8);
            $PAID_AMOUNT = ApiSSOExport::tranform($value['PAID_AMOUNT'], 7);
            $BLANK = ApiSSOExport::tranform($value['BLANK'], 29);
            $show =
                $RECORD_TYPE
                . $SSO_ID
                . $PREFIX
                . $FNAME
                . $LNAME
                . $WAGES
                . $PAID_AMOUNT
                . $BLANK
                . "\n";
            array_push($person, $show);
        }
        $information = implode("", $person);

        return $information;
    }

    public static function setpath($id_company, $month, $year)
    {
        $path = Yii::$app->basePath . '/upload/ssodownload/' . $id_company . '_' . $year . '-' . $month . '.txt';
        // echo $path;
        // exit();
        return $path;
    }

    public static function sethandle()
    {

        $handle = fopen(ApiSSOExport::setpath($id_company, $month, $year), 'w');
        return $handle;
    }

    public static function writefile($id_company, $id_type, $month, $year)
    {
        $fileName = $id_company . '_' . $year . '-' . $month . '.txt';
        $type = ApiSSOExport::type_data($id_type);
        $path = ApiSSOExport::setpath($id_company, $month, $year);

        $handle = fopen($path, 'wd');

        if ($type == 1) {
            $text_head = ApiSSOExport::builddata_head1($id_company, $month, $year);
            $text_detail = ApiSSOExport::builddata1($id_company);
            if (!empty($text_head)) {
                fwrite($handle, $text_head);
                echo "\r";
                fwrite($handle, $text_detail);

                return $path;
            } else {
                fwrite(ApiSSOExport::sethandle(), "Not have personnel in company that you select");
                return true;
            }
        } else if ($type == 2) {
            $text_head = ApiSSOExport::builddata_head2($id_company, $month, $year);
            $text_detail = ApiSSOExport::builddata2($id_company);
            if (!empty($text_head)) {
                fwrite($handle, $text_head);
                echo "\r";
                fwrite($handle, $text_detail);

                //fwrite($this->handle, $text_detail);
                return true;
            } else {
                fwrite(ApiSSOExport::sethandle(), "Not have personnel in company that you select");
                return true;
            }
        } else {
            fwrite($handle, "Plese select of data type");
            return true;

        }

    }

    public static function download($id_company, $month, $year)
    {

        $handle = ApiSSOExport::setpath($id_company, $month, $year);
        $filename = $id_company . '_' . $year . '-' . $month . '.txt';
        $part = Yii::$app->basePath . '/upload/ssodownload/' . $filename;
        return Yii::$app->response->sendFile($part);
    }

    public static function formatPAIDDateSSO($ssodate)
    {
        if ($ssodate == '') {
            $ssodate = date('d/m/Y');
        }

        $tmp = explode('/', $ssodate);
        $d = ($tmp[0]) ? $tmp[0] : date('d');
        $m = ($tmp[1]) ? $tmp[1] : date('m');
        $ye = ($tmp[2]) ? $tmp[2] : date('Y');

        $yz = ($ye + 543);  //convert to buddish
        $y = substr($yz, 2, 2);

        return $d . $m . $y;
    }

    public static function update($id_company, $month, $year, $ssodate)
    {
        // $ssodatefoment  = date('ymd',strtotime($ssodate));
        $ssodatefoment = self::formatPAIDDateSSO($ssodate);
        if ($id_company != '' && $year != '' && $month != '') {
            $company = (int)$id_company;
            $command = Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
                ->update('Header_Record', ['PAID_DATE' => $ssodatefoment], ['COMPANY_ID' => $company, 'MONTHS' => $month, 'YEARS' => $year])
                ->execute();
        }


        /*        if($id_company!=''&&$year!=''&&$month!=''){
                    $company = (int)$id_company;
                    $command = Yii::$app->ERP_easyhr_PAYROLL->createCommand()
                    ->update('sso_paid_head', ['PAID_DATE'=>$ssodatefoment], ['COMPANY_ID'=>$company,'MONTHS'=>$month,'YEARS'=>$year])
                    ->execute();
                }*/
    }

    public static function checkstructure($part)
    {
        $handle = @fopen($part, "r");
        if ($handle) {
            while (($buffer = fgets($handle, 135)) == false) {
                return 0;
            }
            while (($buffer = fgets($handle, 126)) == false) {
                return 0;
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
    }

    public static function closefile()
    {
        if (ApiSSOExport::setpath($id_company)) {
            fclose(ApiSSOExport::setpath($id_company));
            return true;
        }
    }

    public static function addducthestory($id_cade, $date_string)
    {
        $addducthestory = Adddeducthistory::find()
            ->where(['=', 'ADD_DEDUCT_THIS_MONTH_EMP_ID', $id_cade])
            ->andWhere(['=', 'ADD_DEDUCT_THIS_MONTH_PAY_DATE', $date_string])
            ->andWhere(['=', 'ADD_DEDUCT_THIS_MONTH_TYPE', '1'])
            ->asArray()
            ->all();
        return $addducthestory;
    }

    public static function emp_data($id_cade)
    {
        $addducthestory = Empdata::find()
            ->select(
                ["emp_data.*",
                    "REPLACE(Address,'ไมระบุ','-') as Address",
                    "REPLACE(Moo,'ไมระบุ','-') as Moo",
                    "REPLACE(Road,'ไมระบุ','-') as Road",
                    "REPLACE(SubDistrict,'ไมระบุ','-') as SubDistrict",
                    "REPLACE(District,'ไมระบุ','-') as District",
                    "REPLACE(Province,'ไมระบุ','-') as Province",
                    "REPLACE(Postcode,'ไมระ','-') as Postcode"]
            )
            ->where(['=', 'ID_Card', $id_cade])
            ->asArray()
            ->all();

        $a = $addducthestory;
        return $addducthestory;
    }

    public static function getTaxPndHead($month, $year, $branch)
    {
        $model = TaxPndHead::find()
            ->where(['=', 'paid_month', $month])
            ->andWhere(['=', 'for_year', $year])
            ->andWhere(['=', 'branch_no', $branch])
            ->asArray()
            ->all();
        return $model;
    }

    public static function getTaxPndDetail($idHead)
    {
        $model = TaxPndDetail::find()
            ->where(['in', 'tax_pnd_head_id', $idHead])
            ->asArray()
            ->all();
        return $model;
    }

    public static function idTaxPndHead($year, $branch)
    {
        $model = TaxPndHead::find()
            ->select('id')
            ->where(['=', 'for_year', $year])
            ->andWhere(['=', 'branch_no', $branch])
            ->asArray()
            ->all();
        $data = [];
        foreach ($model as $item) {
            $data[] = $item['id'];
        }
        return $data;
    }

    public static function getPad1aTaxPndHead($year, $branch)
    {
        $model = TaxPndHead::find()
            ->select(['*', new Expression('SUM(sum_income_gnr) as totle_income_gnr'),
                new Expression('SUM(sum_tax_gnr) as totle_tax_gnr'),
                new Expression('SUM(sum_income_auth) as totle_income_auth'),
                new Expression('SUM(sum_tax_auth) as totle_income_auth'),
                new Expression('SUM(sum_income_onepaid) as totle_income_onepaid'),
                new Expression('SUM(sum_tax_onepaid) as totle_tax_onepaid'),
                new Expression('SUM(sum_income_inthai) as totle_income_inthai'),
                new Expression('SUM(sum_tax_inthai) as totle_tax_inthai'),
                new Expression('SUM(sum_income_notin) as totle_income_notin'),
                new Expression('SUM(sum_tax_notin) as totle_tax_notin'),
            ])
            ->where(['=', 'for_year', $year])
            ->andWhere(['=', 'branch_no', $branch])
            ->asArray()
            ->all();
        return $model;
    }

    public function getPad1aTaxPndamount($idHead)
    {
        $str_id = join(',', $idHead);
        $model = TaxPndDetail::find()
            ->where(['in', 'tax_pnd_head_id', $idHead])
            ->asArray()
            ->all();
        $dataincome_gnr = $dataincome_auth = $dataincome_onepaid = $dataincome_inthai = $dataincome_notin = 0;
        $data = [];
        foreach ($model as $item) {
            if (floatval($item['income_gnr']) != 0) {
                $dataincome_gnr++;
            } else if (floatval($item['income_auth']) != 0) {
                $dataincome_auth++;
            } else if (floatval($item['income_onepaid']) != 0) {
                $dataincome_onepaid++;
            } else if (floatval($item['$income_inthai']) != 0) {
                $dataincome_inthai++;
            } else if (floatval($item['$income_notin']) != 0) {
                $dataincome_notin++;
            }
        }
        $data['income_gnr'] = $dataincome_gnr;
        $data['income_auth'] = $dataincome_auth;
        $data['income_onepaid'] = $dataincome_onepaid;
        $data['income_inthai'] = $dataincome_inthai;
        $data['income_notin'] = $dataincome_notin;
        $this->dataDetail = $model;
        return $data;
    }

    public function getDatadetail()
    {
        $arr_data = [];
        foreach ($this->dataDetail as $item) {
            $arr_data[$item['emp_idcard']]['tax_pnd_head_id'] = $item['tax_pnd_head_id'];
            $arr_data[$item['emp_idcard']]['emp_no'] = $item['emp_no'];
            $arr_data[$item['emp_idcard']]['emp_idcard'] = $item['emp_idcard'];
            $arr_data[$item['emp_idcard']]['emp_firstname'] = $item['emp_firstname'];
            $arr_data[$item['emp_idcard']]['emp_lastname'] = $item['emp_lastname'];
            $arr_data[$item['emp_idcard']]['emp_address'] = $item['emp_address'];
            $arr_data[$item['emp_idcard']]['paid_date'] = $item['paid_date'];
            $arr_data[$item['emp_idcard']]['paid_amount'] += $item['paid_amount'];
            $arr_data[$item['emp_idcard']]['paid_tax'] += $item['paid_tax'];
        }
        return $arr_data;
    }

    public static function manageArray($array)
    {
        $count = $index = 1;
        $arr = [];
        foreach ($array as $item) {
            $arr[$index][] = $item;
            if (($count % 8) == 0) {
                $index++;
            }
            $count++;
        }
        return $arr;
    }
}

?>