<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-chart/jquery.orgchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/js/hr/jquery-chart/jquery.orgchart.css', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/permission/js/jquery.sumoselect.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/permission/sumoselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");

$this->registerCss("
#orgChart{
    width: auto;
    height: auto;
    font-size:10px !important;
}

#orgChartContainer{
    width: 100%;
    height: 650px;
    overflow: scroll;
    background: #eeeeee;
}

");

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/OrgchartManage.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/exporttorevenues.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดการผังองค์กร</a>
                </li>
                <li class="active">ผังโครงสร้างองค์กร</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body" style="overflow: scroll">

            <div class="row" style="margin-bottom: 10px;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <center><label for="monthselect" class="col-sm-3 control-label">ชื่อบริษัท</label>
                            <div class="col-sm-3">
                                <select class="form-control select2" name="company_id" id="company_id" required>
                                    <option value="all">--- ทั้งหมด ---</option>
                                    <?php
                                    foreach ($arrCompany as $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <button type="button" id="btnExport"
                                        class="btn btn-sm btn-success btn-sm"><i class="fa fa fa-search"></i> ค้นหา
                                </button>
                            </div>
                        </center>
                    </div>

                </div>
            </div>


            <div id="orgChartContainer">
                <div id="orgChart"></div>
            </div>
            <div id="consoleOutput">
            </div>
        </div>
    </div>
    <!-- /.box -->
    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
    </button> -->


</section><!-- /.content -->