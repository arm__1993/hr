<?php

namespace app\modules\wssale\models;
 

use Yii;

class Modelcode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Model_Code';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CRPAndAirIncVat', 'D_netAndAirIncVat', 'Extension_Code', 'who_up_load', 'who_up_load_posCode', 'TimeupDate', 'MDL_EXP', 'idRef_Model_conver'], 'required'],
            [['Model_C_M'], 'string', 'max' => 80],
            [['Unit_Type', 'Model_Class', 'who_up_load'], 'string', 'max' => 250],
            [['Model_CodeName', 'CRP', 'CRP_ExVat', 'CRP_Vat', 'D_Net', 'D_Net_ExVat', 'D_Net_Vat', 'Air_Con_CRP', 'Air_Con_CRP_ExVat', 'Air_Con_CRP_Vat', 'Air_Con_D_Net', 'Air_Con_D_Net_ExVat', 'Air_Con_D_Net_Vat', 'CRPAndAirIncVat', 'D_netAndAirIncVat', 'Yan_Ass', 'R_Kb', 'B_Kb', 'CVIP', 'TTBudgetForSale', 'TTBudgetForSup'], 'string', 'max' => 50],
            [['Model_GName'], 'string', 'max' => 100],
            [['Extension_Code', 'Start_Use_Date', 'End_Use_Date'], 'string', 'max' => 10],
            [['Remark'], 'string', 'max' => 255],
            [['who_up_load_posCode', 'TimeupDate', 'MDL_EXP'], 'string', 'max' => 30],
            [['idRef_Model_conver'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tranc_No' => 'หมายเลขดัชนี',
            'Model_C_M' => 'รหัสรุ่นรถ',
            'Unit_Type' => 'ชนิดของรถ เช่น รถใหญ่,รถเล็ก,รถไทยรุ่ง ฯลฯ',
            'Model_Class' => 'ชื่อระดับรุ่น เช่น Dragon Power ,D-Max',
            'Model_CodeName' => 'รหัสแบบรถ TFR54HPM8AAM',
            'Model_GName' => 'แบบรถทั่วไป SLX',
            'CRP' => 'CRP IncVat',
            'CRP_ExVat' => 'CRP Ex Vat',
            'CRP_Vat' => 'CRP Vat',
            'D_Net' => 'Dnet + Vat',
            'D_Net_ExVat' => 'Dnet - Vat',
            'D_Net_Vat' => 'Vat of Dnet',
            'Air_Con_CRP' => 'ราคาแอร์ CRP Inc Vat',
            'Air_Con_CRP_ExVat' => 'ราคาแอร์ CRP Ex Vat',
            'Air_Con_CRP_Vat' => 'ราคาแอร์ CRP Vat',
            'Air_Con_D_Net' => 'ราคาแอร์ Dnet + Vat',
            'Air_Con_D_Net_ExVat' => 'ราคาแอร์ Dnet - Vat',
            'Air_Con_D_Net_Vat' => 'Vat ของ Dnetแอร์',
            'CRPAndAirIncVat' => 'CRP IncVat',
            'D_netAndAirIncVat' => 'Dnet + Vat ',
            'Extension_Code' => 'OPTION_CODE รหัสอุปกรณ์ติดตั้งพิเศษ',
            'Yan_Ass' => 'ค่าอุปกรณ์พิเศษ ยานฯ',
            'R_Kb' => 'จะไม่ใช้',
            'B_Kb' => 'จะไม่ใช้',
            'CVIP' => 'จะไม่ใช้',
            'Start_Use_Date' => 'วันที่เริ่มใช้',
            'End_Use_Date' => 'วันที่เลิกใช้',
            'TTBudgetForSale' => 'งบประมาณสำหรับเชล',
            'TTBudgetForSup' => 'งบประมาณหัวหน้า',
            'Remark' => 'หมายเหตุ',
            'who_up_load' => 'ผู้อัพโหลด',
            'who_up_load_posCode' => 'รหัสตำแห่นงผู้อัพ',
            'TimeupDate' => 'เวลาที่แก้ไขข้อมูล',
            'MDL_EXP' => 'Mdl  Exp',
            'idRef_Model_conver' => 'รหัส Model Code ที่ไว้สลับ',
        ];
    }


    public function unittype()
    {

     // echo  $NameCarband ;

        $sql = "select DISTINCT `Unit_Type` FROM `Model_Code` WHERE   `End_Use_Date` IS NULL " ;
        $dataUnittypemodelcar = Yii::$app->dbERP_Easysale_icmba
       
       ->createCommand($sql)
       ->queryAll();

       return $dataUnittypemodelcar ;
    }

    public function modelclasscar($unittype)
    {

     // echo  $NameCarband ;

        $sql = "select DISTINCT `Model_Class` 
                        FROM    `Model_Code` 
                        WHERE   `End_Use_Date` IS NULL 
                        AND     `Unit_Type` = :unittype" ;
        $dataUnittypemodelcar = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':unittype',$unittype)
       ->queryAll();

       return $dataUnittypemodelcar ;
    }

    public function modelclassname($unittype,$modelclass)
    {

     // echo  $NameCarband ;

        $sql = "select DISTINCT `Model_CodeName` 
                        FROM    `Model_Code` 
                        WHERE   `End_Use_Date` IS NULL 
                        AND  `Unit_Type` = :unittype
                        AND  `Model_Class` = :modelclass " ;
        $data = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':unittype',$unittype)
       ->bindParam(':modelclass',$modelclass)
       ->queryAll();

       return $data ;
    }
    public function modelgname($unittype,$modelclass,$modelcodename)
    {

    $unittype = '%'.$unittype.'%';
    $modelclass = '%'.$modelclass.'%';
    $modelcodename = '%'.$modelcodename.'%';


        $sql = "select  `Model_GName` 
                        FROM  `Model_Code` 
                        WHERE  `End_Use_Date` IS NULL 
                        AND  `Unit_Type`  LIKE '$unittype'
                        AND  `Model_Class` LIKE '$modelclass'
                        AND  `Model_CodeName` LIKE '$modelcodename'" ;

     //echo $sql ;                   
        $data = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':unittype',$unittype)
       ->bindParam(':modelclass',$modelclass)
       ->bindParam(':modelcodename',$modelcodename)
       ->queryAll();

       return $data ;

    }

    public function crpandairincvat($unittype,$modelclass,$modelcodename,$modelgname)
        {

    $unittype = '%'.$unittype.'%';
    $modelclass = '%'.$modelclass.'%';
    $modelcodename = '%'.$modelcodename.'%';
    $modelgname = '%'.$modelgname.'%';


        $sql = "select CRPAndAirIncVat
                    FROM  `Model_Code` 
                    WHERE  `End_Use_Date` IS NULL 
                    AND  `Unit_Type` LIKE '$unittype'
                    AND  `Model_Class` LIKE '$modelclass'
                    AND  `Model_CodeName` LIKE '$modelcodename'   
                    AND  `Model_GName` LIKE  '$modelgname'
                    " ;

 
                  
        $data = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':unittype',$unittype)
       ->bindParam(':modelclass',$modelclass)
       ->bindParam(':modelcodename',$modelcodename)
       ->bindParam(':modelgname',$modelgname)
       ->queryAll();

       return $data ;

    }

}
