<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "wage_confirm_head".
 *
 * @property integer $id
 * @property integer $id_company
 * @property integer $statusconfirm
 * @property string $createby
 * @property string $datecreate
 * @property string $updateby
 * @property string $updatedate
 */
class WageConfirmHead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wage_confirm_head';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company', 'statusconfirm', 'createby', 'datecreate', 'updateby', 'updatedate'], 'required'],
            [['id_company', 'statusconfirm'], 'integer'],
            [['datecreate', 'updatedate'], 'safe'],
            [['createby', 'updateby'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_company' => 'Id Company',
            'statusconfirm' => 'Statusconfirm',
            'createby' => 'Createby',
            'datecreate' => 'Datecreate',
            'updateby' => 'Updateby',
            'updatedate' => 'Updatedate',
        ];
    }
}
