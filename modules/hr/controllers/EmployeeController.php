<?php

namespace app\modules\hr\controllers;


// use yii\rest\ActiveController;
// use yii\filters\Cors;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\models\Empdata;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\Session;
use Yii;
use app\modules\hr\controllers\MasterController;
class EmployeeController extends MasterController
{


    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function actionFinddepartment()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $_CompanyID = $postValue['CompanyID'];
            $model =  ApiHr::getDepartment($_CompanyID);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }


    public function actionFindsection()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $_CompanyID = $postValue['CompanyID'];
            $_DepartmentID = $postValue['DepartmentID'];
            $model =  ApiHr::getSection($_DepartmentID);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }



    public static function getempnormal()
    {
        $model = Empdata::find()
            ->where('emp_data.username != "#####Out Off###"
                                        AND Prosonnal_Being IN (1,2)')
            ->asArray()
            ->all();
        //echo "<pre>";
        //print_r($mode);
        return $model;
    }

    public static function getempnormalandleave()
    {

        $model = Empdata::find()
            ->where('DATEDIFF(NOW(),End_date)<=37
                                        OR End_date is null
                                        OR End_date="0000-00-00"')
            ->asArray()
            ->all();
        $modelResult = [];
        foreach ($model as $key => $value) {
            if ($value['Prosonnal_Being'] == "3" && $value['End_date'] == "0000-00-00") {

            } else {
                $modelResult[$key] = $value;
            }
        }
        return $modelResult;
    }

    public static function getempall()
    {
        $model = Empdata::find()
            ->asArray()
            ->all();
        return $model;
    }

    public static function getempnewthismonth()
    {
        $model = Empdata::find()
            ->where('Prosonnal_Being = 1
                                AND DATEDIFF(NOW(),Start_date)<=37')
            ->asArray()
            ->all();
        return $model;
    }

    public function actionGetempdatabycompany()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $id_company = $postValue['CompanyID'];
            $id_department = $postValue['DepartmentID'];
            $id_section = $postValue['SectionID'];
            $model =  ApiHr::getEmpdataByCompany($id_company,$id_department,$id_section);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }

    }

}

    