<?php

namespace app\modules\auth\controllers;

//session_start();

use yii\web\Controller;
use Yii;
use yii\web\Session;
use app\modules\hr\models\Place;
use app\modules\hr\apihr\ApiHr;
use app\api\Pepper;
use app\api\DBConnect;
use yii\bootstrap\Alert;
use yii\db\Command;

/**
 * Default controller for the `auth` module
 */
class DashboadController extends Controller
{
    public $layout = 'dashboardlayout';
    public function actionIndex()
    {
        $msg = Yii::$app->request->get();
        if ($msg) {
            echo $msg;
        }
        $data = Place::find()->where(['!=','status','99'])->asArray()->all();
        return $this->render('index',['data'=>$data]);

    }
    public function actionGetplace()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }
}