<?php

namespace app\modules\hr\controllers;

use app\api\Utility;
use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\models\Benefitfund;
use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

use mPDF;

use app\modules\hr\apihr\ApiHr;

use app\modules\hr\models\Vwagehistoryjoincompany;
use app\modules\hr\models\Wagehistory;
use app\modules\hr\models\Adddeducttemplate;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Empdata;
use app\modules\hr\controllers\MasterController;

class PayrollreportController extends MasterController
{

    public $color = "#ccc";
    public $fonthearder = "font-size:16px; font-weight: bold;  text-align:center;";

    public $layout = 'hrlayout';

    // public $idcardLogin;


    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPayslip()
    {
        return $this->render('payslip');
    }

    public function actionFinance()
    {
        return $this->render('finance');
    }

    public function actionAccount()
    {
        $empdata=ApiHr::getempdataall();
        $Company = ApiHr::getWorking_company();
        $AddDeductTemplate = ApiPayroll::listdata_add_deduct_template();
        $DeductTemplate = ApiPayroll::getDataDeductTemp();
        return $this->render('account',[
            'Company'=>$Company,
            'AddDeductTemplate'=>$AddDeductTemplate,
            'DeductTemplate' =>$DeductTemplate,
            'empdata'=>$empdata,
        ]);

    }

    public function actionGetcompanywage()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax)
        {
            $postValue = Yii::$app->request->post();
            $model = Wagehistory::find()
                ->select('DISTINCT(`WAGE_WORKING_COMPANY`) AS COMPANY_ID, `COMPANY_NAME` ')
                ->where('WAGE_PAY_DATE = :monthselect')
                ->groupby('WAGE_WORKING_COMPANY')
                ->addParams([':monthselect' => $postValue['month'],])
                ->asArray()
                ->all();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    /*
    public function actionReportsalarymonth()
    {
        $postValue = Yii::$app->request->post();
        if (isset($postValue)) {
            $arrCompany = $postValue['companyid'];
            $monthselect = $postValue['monthselect'];
            $joinarrCompany = join(",", (array)$arrCompany);

            $modelreportsalarythismonth = Wagehistory::find()
                ->select('SUM(WAGE_SALARY) as SUMSALARY,
                        COMPANY_NAME,
                        SUM(WAGE_TOTAL_ADDITION) as SUM_WAGE_TOTAL_ADDITION,
                        SUM(WAGE_TOTAL_DEDUCTION) as WAGE_TOTAL_DEDUCTION,
                        SUM(WAGE_NET_SALARY) as SUM_WAGE_NET_SALARY')
                ->where('WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ') AND  WAGE_PAY_DATE = :monthselect')
                ->groupby('WAGE_WORKING_COMPANY')
                ->addParams([':monthselect' => $monthselect,])
                ->asArray()
                ->all();

            $session = Yii::$app->session;
            $session->set('reportSalaryTotal', $modelreportsalarythismonth);
            $session->set('monthselect', $monthselect);
            // echo "<pre>";
            // print_r($modelreportsalarythismonth);
            return $this->render('account', ['reportSalaryTotal' => $modelreportsalarythismonth,
                'querySalaryTotalThisMonth1' => true,
                'querySalaryTotalThisMonth2' => false,
                'querySalaryTotalThisMonth3' => false,
                'tab' => 1,
                'monthselect' => $monthselect,]);

        }

    }
    */

    public function actionExportpdfsumtotalallthismonth()
    {
        //$mpdf = new mPDF('th', 'Tharlon-Regular', '12px');
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDefaultFont('THSarabun');
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfsumtotalallthismonth'));
        $mpdf->Output();
        exit;
    }

    public function actionReportadddeductworking()
    {
        $postValue = Yii::$app->request->post();

        $arrIdtemp = $postValue['idtemp'];
        $joinarrIdtemp = join(",", (array)$arrIdtemp);
        $arrCompany = $postValue['companyid'];
        $joinarrCompany = join(",", (array)$arrCompany);
        $monthselect = $postValue['monthselect'];
        //$arrIdtemp = $postValue['idtemp'];


        $sql = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        WAGE_PAY_DATE ,
                        SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS
                        FROM WAGE_HISTORY 
                        INNER JOIN ADD_DEDUCT_HISTORY ON  WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                        
                        WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ') 
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN (' . $joinarrIdtemp . ') 
                        AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"
                        GROUP BY ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                        WAGE_HISTORY.WAGE_WORKING_COMPANY
                        ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';
        //echo $sql;

        $modelCommpaney = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

        $modelTemp = Adddeducttemplate::find()
            ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
            ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
            ->asArray()
            ->all();

        $modelWorkingcompany = Workingcompany:: find()
            ->select('id ,short_name')
            ->where('id IN (' . $joinarrCompany . ')')
            ->asArray()
            ->all();
        $session = Yii::$app->session;
        $session->set('modelCommpaneyAdddeduct', $modelCommpaney);
        $session->set('monthselect', $monthselect);
        $session->set('arrIdtemp', $modelTemp);
        $session->set('modelWorkingcompany', $modelWorkingcompany);

        return $this->render('account', ['modelCommpaneyAdddeduct' => $modelCommpaney,
            'querySalaryTotalThisMonth1' => false,
            'querySalaryTotalThisMonth2' => true,
            'querySalaryTotalThisMonth3' => false,
            'monthselect' => $monthselect,
            'arrIdtemp' => $modelTemp,
            'modelWorkingcompany' => $modelWorkingcompany,
            'tab' => 2]);
    }

    public function actionExportpdfsumadddeduct()
    {


        $session = Yii::$app->session;
        $modelWorkingcompany = $session->get('modelWorkingcompany');
        $arrIdtemp = $session->get('arrIdtemp');
        $modelCommpaneyAdddeduct = $session->get('modelCommpaneyAdddeduct');
        $monthselect = $session->get('monthselect');

        $getMonth = Datetime::convertFormatMonthYear($monthselect);
        //print_r($getMonth);
        $monthGetReport1 = $getMonth['monthresult'];
        $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
        $yearGetReport1 = $getMonth['year'];
        // echo count( $modelWorkingcompany);

        $toTalrecord = count($modelWorkingcompany);
        //   echo "<pre>";
        //   print_r ($modelWorkingcompany);
        //   exit;
        $toTalpage = ceil($toTalrecord / 8);

        //$mpdf = new mPDF('th', 'Tharlon-Regular', '12px');
        $mpdf = new \Mpdf\Mpdf();
        $k = 0;
        $en = 0;
        $em = 0;
        $lastrecord = 0;

        $somrow = [];
        for ($i = 1; $i <= $toTalpage; $i++) {
            $table = "<hr>";
            $table .= '<table style="border-collapse: collapse; border: 1px solid ' . $this->color . ';" cellpadding="5"  width="100%"  align="center"> ';
            //$table .= '<thead>';
            $table .= '<tr>';
            $table .= '<th style="border: 1px solid ' . $this->color . ';"  rowspan="1" colspan="1"  width="30%">';
            $table .= 'รายการเพิ่ม';
            $table .= '</th>';
            $x = 0;
            for ($j = $en; $j <= ($en + 6); $j++) {
                $x++;
                $table .= '<th style="border: 1px solid ' . $this->color . ';">' . $modelWorkingcompany[$j]['short_name'] . '</th>';
                $em++;
                if ($i == $toTalpage && $em == $toTalrecord) {
                    $table .= '<th style="border: 1px solid ' . $this->color . ';" rowspan="1" colspan="1"  width="15%">';
                    $table .= 'รวม';
                    $table .= '</th>';
                    $lastrecord = $x;
                    break;
                }
            }

            $table .= '</tr>';
            //$table .= '</thead>';
            //$table .= '<tbody>';
            foreach ($arrIdtemp as $key1 => $value) {

                $table .= '<tr>';
                $table .= '<td style="border: 1px solid ' . $this->color . ';">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</td>';
                for ($k = 0; $k <= 6; $k++) {
                    $em2++;
                    // $table .= '<td align="right">xxxx';
                    // $table .= '</td>';
                    if ($i == $toTalpage && $k == $lastrecord) {
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                        $table .= Helper::displayDecimal($somrow[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                        $table .= '</td>';
                        break;
                    } else {

                        $indexA = ($k + $en);
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueadddeduc($value['ADD_DEDUCT_TEMPLATE_ID'], $modelWorkingcompany[$indexA]['id'], $modelCommpaneyAdddeduct, $somrow) . '';
                        $table .= '</td>';
                    }
                }
                $table .= '</tr>';
            }
            //$table .= '</tbody>';
            $table .= '</table>';

            $mpdf->SetHTMLHeader('<div style="' . $this->fonthearder . '">รายงานสรุปผลเงินเดือน (รายการเพิ่ม) ' . $resultMonthReport1 . ' ปี ' . $yearGetReport1 . '</div>');
            $mpdf->WriteHTML($table);
            $mpdf->AddPage();


            $en = $j;
        }

        $mpdf->Output();
        exit;

    }

    public function getValueadddeduc($temp, $compa, $model, &$sumlineref)
    {
        $out = "0.00";
        foreach ($model as $no => $val) {
            if ($val['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $temp && $val['WAGE_WORKING_COMPANY'] == $compa) {
                $out = Helper::displayDecimal($val['SUM_ADD_DEDUCT_HIS']);
                $sumlineref[$val['ADD_DEDUCT_THIS_MONTH_TMP_ID']] += $val['SUM_ADD_DEDUCT_HIS'];
                break;
            }
        }

        return $out;

    }

    public function actionReportadddetaildeduct()
    {
        $postValue = Yii::$app->request->post();

        $arrIdtemp = $postValue['idtemp'];
        $joinarrIdtemp = join(",", (array)$arrIdtemp);

        $companyid = $postValue['companyid'];
        $monthselect = $postValue['monthselect'];
        $checksalary = substr($joinarrIdtemp, 0, 2);

        if ($checksalary == "S1") {
            $checksalary;

            $joinarrIdtemp = substr($joinarrIdtemp, 3);


            $sql = 'SELECT WAGE_EMP_ID ,
                        ERP_easyhr_checktime.emp_data.Name AS NAMEEMP,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT 
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON 
                        WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN ERP_easyhr_checktime.emp_data ON ERP_easyhr_checktime.emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';

            $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $sqlsalary = 'SELECT `WAGE_EMP_ID`, `WAGE_WORKING_COMPANY`, `WAGE_DEPARTMENT_ID`, `WAGE_SALARY`, `WAGE_PAY_DATE` 
                        FROM `WAGE_HISTORY` 
                        WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                        AND WAGE_PAY_DATE = "' . $monthselect . '"';

            $modelSalary = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlsalary)->queryAll();

            $sqlSum = 'SELECT ERP_easyhr_checktime.emp_data.Name, 
                            WAGE_HISTORY.WAGE_EMP_ID, WAGE_WORKING_COMPANY,
                            ERP_easyhr_OU.working_company.short_name AS short_name,WAGE_PAY_DATE , 
                            WAGE_HISTORY.WAGE_SALARY,
                            SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS 
                    FROM WAGE_HISTORY 
                    INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                    INNER JOIN ERP_easyhr_OU.working_company ON ERP_easyhr_OU.working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY 
                    INNER JOIN ERP_easyhr_checktime.emp_data on ERP_easyhr_checktime.emp_data.ID_Card=WAGE_HISTORY.WAGE_EMP_ID
                    WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                    AND WAGE_HISTORY.WAGE_PAY_DATE  = "' . $monthselect . '" 
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"  
                    GROUP BY WAGE_HISTORY.WAGE_EMP_ID
                    ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';

            $modelSumTotal = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlSum)->queryAll();
            $statusSalary = true;


        } else {
            $joinarrIdtemp = join(",", (array)$arrIdtemp);


            $sql = 'SELECT WAGE_EMP_ID ,
                        ERP_easyhr_checktime.emp_data.Name AS NAMEEMP,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT 
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON 
                        WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN ERP_easyhr_checktime.emp_data ON ERP_easyhr_checktime.emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';
            $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $sqlSum = 'SELECT ERP_easyhr_checktime.emp_data.Name, 
                            WAGE_HISTORY.WAGE_EMP_ID, WAGE_WORKING_COMPANY,
                            ERP_easyhr_OU.working_company.short_name AS short_name,WAGE_PAY_DATE , 
                            SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS 
                    FROM WAGE_HISTORY 
                    INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                    INNER JOIN ERP_easyhr_OU.working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY 
                    INNER JOIN ERP_easyhr_checktime.emp_data on ERP_easyhr_checktime.emp_data.ID_Card=WAGE_HISTORY.WAGE_EMP_ID
                    WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                    AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '" 
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"  
                    GROUP BY WAGE_HISTORY.WAGE_EMP_ID
                    ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';

            $modelSumTotal = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlSum)->queryAll();

        }

        $modelTemp = Adddeducttemplate::find()
            ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
            ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
            ->asArray()
            ->all();

        $sqlIdcardcompany = 'SELECT  WAGE_EMP_ID
                             FROM WAGE_HISTORY 
                             WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                             AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"';
        $modelIdcard = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlIdcardcompany)->queryAll();

        $arrIDCARD = [];
        foreach ($modelIdcard AS $value) {
            $arrIDCARD[] = ($value['WAGE_EMP_ID']);
        };

        $modelEmp = Empdata::find()
            ->select(['emp_data.ID_Card as ID_Card',
                'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname'])
            ->where(['in', 'ID_Card', $arrIDCARD])
            ->asArray()
            ->all();

        $session = Yii::$app->session;
        $session->set('modelEmp', $modelEmp);
        $session->set('modelTemp', $modelTemp);
        $session->set('modelSalary', $modelSalary);
        $session->set('modelAdddeductdtail', $modelAdddeductdtail);
        $session->set('modelSumTotal', $modelSumTotal);
        $session->set('statusSalary', $statusSalary);
        $session->set('querySalaryTotalThisMonth3', true);
        $session->set('monthselect', $monthselect);

        return $this->render('account', ['modelEmp' => $modelEmp,
            'modelTemp' => $modelTemp,
            'modelSalary' => $modelSalary,
            'modelAdddeductdtail' => $modelAdddeductdtail,
            'modelSumTotal' => $modelSumTotal,
            'querySalaryTotalThisMonth1' => false,
            'querySalaryTotalThisMonth2' => false,
            'querySalaryTotalThisMonth3' => true,
            'statusSalary' => $statusSalary,
            'tab' => 3
        ]);
    }

    public function actionExportpdfreportadddetaildeduct()
    {

        $session = Yii::$app->session;
        $modelEmp = $session->get('modelEmp');
        $modelTemp = $session->get('modelTemp');
        $modelSalary = $session->get('modelSalary');
        $modelAdddeductdtail = $session->get('modelAdddeductdtail');
        $modelSumTotal = $session->get('modelSumTotal');
        $statusSalary = $session->get('statusSalary');
        $monthselect = $session->get('monthselect');

        $getMonth = Datetime::convertFormatMonthYear($monthselect);
        //print_r($getMonth);
        $monthGetReport1 = $getMonth['monthresult'];
        $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
        $yearGetReport1 = $getMonth['year'];
        $toTalrecord = count($modelEmp);
        //   echo "<pre>";
        //   print_r ($modelSumTotal);
        //   exit;
        $toTalpage = ceil($toTalrecord / 8);


        //$mpdf = new mPDF('th', 'Tharlon-Regular', '12px');
        $mpdf = new \Mpdf\Mpdf();

        $k = 0;
        $q = 0;
        $z = 0;
        $en = 0;
        $em = 0;
        $lastrecord = 0;
        $this->color = "#ccc";
        $somrow = [];
        for ($i = 1; $i <= $toTalpage; $i++) {

            $table = '<table style="border-collapse: collapse; border: 1px solid ' . $this->color . ';" cellpadding="5"  width="100%"  align="center"> ';
            //$table .= '<thead>';
            $table .= '<tr>';
            $table .= '<th style="border: 1px solid ' . $this->color . ';"  rowspan="1" colspan="1"  width="30%">';
            $table .= 'รายการเพิ่ม';
            $table .= '</th>';
            $x = 0;
            for ($j = $en; $j <= ($en + 6); $j++) {
                $x++;
                $table .= '<th  style="border: 1px solid ' . $this->color . ';">' . $modelEmp[$j]['Fullname'] . '</th>';
                $em++;
                if ($i == $toTalpage && $em == $toTalrecord) {
                    $table .= '<th style="border: 1px solid ' . $this->color . ';" rowspan="1" colspan="1"  width="15%">';
                    $table .= 'รวม';
                    $table .= '</th>';
                    $lastrecord = $x;
                    break;
                }
            }

            $table .= '</tr>';
            //$table .= '</thead>';
            //$table .= '<tbody>';
            if ($statusSalary) {
                $table .= '<tr>';
                $table .= '<td style="border: 1px solid ' . $this->color . ';">เงินเดือน</td>';
                for ($q = 0; $q <= 6; $q++) {
                    if ($i == $toTalpage && $q == $lastrecord) {
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                        $table .= Helper::displayDecimal($somrowsalary[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                        $table .= '</td>';
                        break;
                    } else {
                        $indexB = ($q + $en);
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueAdddeducPersonalSalary($modelEmp[$indexB]['ID_Card'], $modelSalary, $somrowsalary) . '</td>';
                    }
                }
                $table .= '</tr>';
            }
            foreach ($modelTemp as $key1 => $value) {

                $table .= '<tr>';
                $table .= '<td style="border: 1px solid ' . $this->color . ';">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</td>';
                for ($k = 0; $k <= 6; $k++) {
                    // $table .= '<td align="right">xxxx';
                    // $table .= '</td>';
                    if ($i == $toTalpage && $k == $lastrecord) {
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                        $table .= Helper::displayDecimal($somrow[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                        $table .= '</td>';
                        break;
                    } else {

                        $indexA = ($k + $en);
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueAdddeducPersonal($value['ADD_DEDUCT_TEMPLATE_ID'], $modelEmp[$indexA]['ID_Card'], $modelAdddeductdtail, $somrow) . '';
                        $table .= '</td>';
                    }
                }
                $table .= '</tr>';
            }
            $table .= '<tr>';
            $table .= '<td style="border: 1px solid ' . $this->color . ';"><B>รวมรับ</B></td>';

            for ($z = 0; $z <= 6; $z++) {
                if ($i == $toTalpage && $z == $lastrecord) {
                    $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                    // $table .= //Helper::displayDecimal($somrowsumsalary[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                    $table .= '</td>';
                    break;
                } else {
                    $indexC = ($z + $en);
                    $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueAdddeducPersonalSalaryTotal($modelEmp[$indexC]['ID_Card'], $modelSumTotal) . '</td>';
                }
            }
            $table .= '</tr>';
            //$table .= '</tbody>';
            $table .= '</table>';

            $mpdf->SetHTMLHeader('<div style="' . $this->fonthearder . '">รายละเอียดส่วนเพิ่ม (รายการเพิ่ม) ' . $resultMonthReport1 . ' ปี ' . $yearGetReport1 . '</div>');
            $mpdf->WriteHTML($table);
            $mpdf->AddPage();


            $en = $j;
        }


        //$mpdf->WriteHTML($this->renderPartial('_exportpdfreportadddetaildeduct'));

        $mpdf->Output();
        exit;
    }


    public function getValueAdddeducPersonal($temp, $emp, $model, &$sumlineref)
    {
        $out = "0.00";
        foreach ($model as $no => $val) {
            if ($val['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $temp && $val['WAGE_EMP_ID'] == $emp) {
                $out = Helper::displayDecimal($val['ADD_DEDUCT_THIS_MONTH_AMOUNT']);
                $sumlineref[$val['ADD_DEDUCT_THIS_MONTH_TMP_ID']] += $val['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                break;
            }
        }
        return $out;

    }

    public function getValueAdddeducPersonalSalary($emp, $model, &$sumlineref)
    {
        $out = "0.00";
        foreach ($model as $no => $val) {
            if ($val['WAGE_EMP_ID'] == $emp) {
                $out = Helper::displayDecimal($val['WAGE_SALARY']);
                $sumlinerefsalary[$val['WAGE_EMP_ID']] += $val['WAGE_SALARY'];
                break;
            }
        }
        return $out;

    }

    public function getValueAdddeducPersonalSalaryTotal($emp, $model)
    {
        $out = "0.00";
        foreach ($model as $no => $val) {
            if ($val['WAGE_EMP_ID'] == $emp) {
                $out = Helper::displayDecimal($val['WAGE_SALARY'] + $val['SUM_ADD_DEDUCT_HIS']);
                break;
            }
        }
        return $out;
    }

    public function actionReportdedeductworking()
    {
        $postValue = Yii::$app->request->post();

        $arrIdtemp = $postValue['idtemp'];
        $joinarrIdtemp = join(",", (array)$arrIdtemp);
        $arrCompany = $postValue['companyid'];
        $joinarrCompany = join(",", (array)$arrCompany);
        $monthselect = $postValue['selectmonth'];
        //$arrIdtemp = $postValue['idtemp'];

        $checksalary = substr($joinarrIdtemp, 0, 4);
        if ($checksalary == "SSO1") {
            $joinarrIdtemp = substr($joinarrIdtemp, 5);
            $SSO = "%ประกันสังคม";
            $sql = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                                WAGE_WORKING_COMPANY,
                                ERP_easyhr_OU.working_company.short_name AS short_name,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                WAGE_PAY_DATE ,
                                SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS
                                FROM WAGE_HISTORY 
                                INNER JOIN ADD_DEDUCT_HISTORY ON 
                                WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                                INNER JOIN ERP_easyhr_OU.working_company ON
                                ERP_easyhr_OU.working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ') 
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN (' . $joinarrIdtemp . ')
                                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                                GROUP BY ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                WAGE_HISTORY.WAGE_WORKING_COMPANY
                                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';
            $modelCommpaney = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $sqlsso = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                                WAGE_WORKING_COMPANY,
                                ERP_easyhr_OU.working_company.short_name AS short_name,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                WAGE_PAY_DATE ,
                                SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS
                                FROM WAGE_HISTORY 
                                INNER JOIN ADD_DEDUCT_HISTORY ON 
                                WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                                INNER JOIN ERP_easyhr_OU.working_company ON
                                ERP_easyhr_OU.working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME LIKE "' . $SSO . '"
                                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                                GROUP BY ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                WAGE_HISTORY.WAGE_WORKING_COMPANY
                                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';
            $modelCommpaneySSO = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlsso)->queryAll();
            $statusSSO = true;

            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();


        } else {
            $sql = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                                WAGE_WORKING_COMPANY,
                                working_company.short_name AS short_name,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                WAGE_PAY_DATE ,
                                SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS
                                FROM WAGE_HISTORY 
                                INNER JOIN ADD_DEDUCT_HISTORY ON 
                                WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                                INNER JOIN working_company ON
                                working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ') 
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN (' . $joinarrIdtemp . ') 
                                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                                GROUP BY ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                                WAGE_HISTORY.WAGE_WORKING_COMPANY
                                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';
            $modelCommpaney = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();

        }


        $modelWorkingcompany = Workingcompany:: find()
            ->select('id ,short_name')
            ->where('id IN (' . $joinarrCompany . ')')
            ->asArray()
            ->all();
        $session = Yii::$app->session;
        $session->set('modelCommpaneyAdddeduct', $modelCommpaney);
        $session->set('modelCommpaneySSO', $modelCommpaneySSO);
        $session->set('querySalaryTotalThisMonth4', true);
        $session->set('statusSSO', $statusSSO);
        $session->set('monthselect', $monthselect);
        $session->set('modelTemp', $modelTemp);
        $session->set('modelWorkingcompany', $modelWorkingcompany);
        //  echo "<pre>";
        //  print_r($modelCommpaneySSO);

        return $this->render('account', ['modelCommpaneyAdddeduct' => $modelCommpaney,
            'modelCommpaneySSO' => $modelCommpaneySSO,
            'querySalaryTotalThisMonth1' => false,
            'querySalaryTotalThisMonth2' => false,
            'querySalaryTotalThisMonth3' => false,
            'querySalaryTotalThisMonth4' => true,
            'statusSSO' => $statusSSO,
            'monthselect' => $monthselect,
            'modelTemp' => $modelTemp,
            'modelWorkingcompany' => $modelWorkingcompany,
            'tab' => 4]);

    }

    public function actionExportpdfreportdedeductworking()
    {
        $session = Yii::$app->session;
        $modelWorkingcompany = $session->get('modelWorkingcompany');
        $modelTemp = $session->get('modelTemp');
        $modelCommpaneyAdddeduct = $session->get('modelCommpaneyAdddeduct');
        $monthselect = $session->get('monthselect');
        $modelCommpaneySSO = $session->get('modelCommpaneySSO');
        $statusSSO = $session->get('statusSSO');

        $getMonth = Datetime::convertFormatMonthYear($monthselect);
        //print_r($getMonth);
        $monthGetReport1 = $getMonth['monthresult'];
        $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
        $yearGetReport1 = $getMonth['year'];
        // echo count( $modelWorkingcompany);

        $toTalrecord = count($modelWorkingcompany);
        //   echo "<pre>";
        //   print_r ($modelCommpaneySSO);
        //   exit;
        $toTalpage = ceil($toTalrecord / 8);

        //$mpdf = new mPDF('th', 'Tharlon-Regular', '12px');
        $mpdf = new \Mpdf\Mpdf();
        $k = 0;
        $q = 0;
        $en = 0;
        $em = 0;
        $lastrecord = 0;
        $somrow = [];
        for ($i = 1; $i <= $toTalpage; $i++) {

            $table = '<table style="border-collapse: collapse; border: 1px solid ' . $this->color . ';" cellpadding="5"  width="100%"  align="center"> ';
            //$table .= '<thead>';
            $table .= '<tr>';
            $table .= '<th style="border: 1px solid ' . $this->color . ';"  rowspan="1" colspan="1"  width="30%">';
            $table .= 'รายการหัก';
            $table .= '</th>';
            $x = 0;
            for ($j = $en; $j <= ($en + 6); $j++) {
                $x++;
                $table .= '<th style="border: 1px solid ' . $this->color . ';">' . $modelWorkingcompany[$j]['short_name'] . '</th>';
                $em++;
                if ($i == $toTalpage && $em == $toTalrecord) {
                    $table .= '<th style="border: 1px solid ' . $this->color . ';" rowspan="1" colspan="1"  width="15%">';
                    $table .= 'รวม';
                    $table .= '</th>';
                    $lastrecord = $x;
                    break;
                }
            }

            $table .= '</tr>';
            //$table .= '</thead>';
            //$table .= '<tbody>';
            if ($statusSSO) {
                $table .= '<tr>';
                $table .= '<td style="border: 1px solid ' . $this->color . ';">ประกันสังคม</td>';
                for ($q = 0; $q <= 6; $q++) {
                    if ($i == $toTalpage && $q == $lastrecord) {
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                        $table .= Helper::displayDecimal($somrowsso[$value['WAGE_WORKING_COMPANY']]);
                        $table .= '</td>';
                        break;
                    } else {
                        $indexB = ($q + $en);
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueSSO($modelWorkingcompany[$indexB]['id'], $modelCommpaneySSO, $somrowsso) . '</td>';
                    }
                }
                $table .= '</tr>';
            }
            foreach ($modelTemp as $key1 => $value) {

                $table .= '<tr>';
                $table .= '<td style="border: 1px solid ' . $this->color . ';">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</td>';
                for ($k = 0; $k <= 6; $k++) {
                    $em2++;
                    // $table .= '<td align="right">xxxx';
                    // $table .= '</td>';
                    if ($i == $toTalpage && $k == $lastrecord) {
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                        $table .= Helper::displayDecimal($somrow[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                        $table .= '</td>';
                        break;
                    } else {

                        $indexA = ($k + $en);
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueadddeduc($value['ADD_DEDUCT_TEMPLATE_ID'], $modelWorkingcompany[$indexA]['id'], $modelCommpaneyAdddeduct, $somrow) . '';
                        $table .= '</td>';
                    }
                }
                $table .= '</tr>';
            }
            //$table .= '</tbody>';
            $table .= '</table>';

            $mpdf->SetHTMLHeader('<div style="' . $this->fonthearder . '">รายงานสรุปผลเงินเดือน (รายการหัก)  ' . $resultMonthReport1 . ' ปี ' . $yearGetReport1 . '</div>');
            $mpdf->WriteHTML($table);
            $mpdf->AddPage();


            $en = $j;
        }

        $mpdf->Output();
        exit;

    }

    public function getValueSSO($comp, $modelsso, &$sumlineref)
    {
        $out = "0.00";
        foreach ($modelsso as $no => $val) {
            if ($val['WAGE_WORKING_COMPANY'] == $comp) {
                $out = Helper::displayDecimal($val['SUM_ADD_DEDUCT_HIS']);
                $sumlineref[$val['WAGE_WORKING_COMPANY']] += $val['SUM_ADD_DEDUCT_HIS'];
                break;
            }
        }

        return $out;

    }

    public function actionReportdedetaildeduct()
    {
        $postValue = Yii::$app->request->post();

        $arrIdtemp = $postValue['idtemp'];
        $joinarrIdtemp = join(",", (array)$arrIdtemp);

        $companyid = $postValue['companyid'];
        $monthselect = $postValue['monthselect'];
        $checksalary = substr($joinarrIdtemp, 0, 4);

        if ($checksalary == "SSO1") {

            $joinarrIdtemp = substr($joinarrIdtemp, 5);
            $SSO = "%ประกันสังคม";

            $sql = 'SELECT WAGE_EMP_ID ,
                        ERP_easyhr_checktime.emp_data.Name AS NAMEEMP,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT 
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON 
                        WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN ERP_easyhr_checktime.emp_data ON ERP_easyhr_checktime.emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';



            $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();


            //cho "<br>";

            $sqlsso = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                                WAGE_WORKING_COMPANY,
                                WAGE_EMP_ID,
                                ERP_easyhr_OU.working_company.short_name AS short_name,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                WAGE_PAY_DATE ,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT AS ADD_DEDUCT_THIS_MONTH_AMOUNT
                                FROM WAGE_HISTORY 
                                INNER JOIN ADD_DEDUCT_HISTORY ON 
                                WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                                INNER JOIN ERP_easyhr_OU.working_company ON
                                ERP_easyhr_OU.working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                               
                                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';
            $modelSSO = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlsso)->queryAll();
            $statusSSO = true;

            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();

            $statusSSO = true;


        } else {
            $joinarrIdtemp = join(",", (array)$arrIdtemp);


            $sql = 'SELECT WAGE_EMP_ID ,
                        ERP_easyhr_checktime.emp_data.Name AS NAMEEMP,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT 
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON 
                        WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN ERP_easyhr_checktime.emp_data ON ERP_easyhr_checktime.emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';


              $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();


            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();


        }


        // echo "<pre>";
        // print_r($modelAdddeductdtail);
        // exit;
        //$modelAdddeductdtailIdcard = array_unique($modelAdddeductdtailIdcard);


        $sqlIdcardcompany = 'SELECT  WAGE_EMP_ID
                             FROM WAGE_HISTORY 
                             WHERE WAGE_WORKING_COMPANY = "' . $companyid . '"
                             AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"';
        $modelIdcard = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlIdcardcompany)->queryAll();

        $arrIDCARD = [];
        foreach ($modelIdcard AS $value) {
            $arrIDCARD[] = ($value['WAGE_EMP_ID']);
        };

        $modelEmp = Empdata::find()
            ->select(['emp_data.ID_Card as ID_Card',
                'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname'])
            ->where(['in', 'ID_Card', $arrIDCARD])
            ->asArray()
            ->all();
        // echo "<pre>";
        // print_r($modelEmp);
        // exit();


        $session = Yii::$app->session;
        $session->set('modelEmp', $modelEmp);
        $session->set('modelTemp', $modelTemp);
        $session->set('modelSSO', $modelSSO);
        $session->set('modelAdddeductdtail', $modelAdddeductdtail);
        $session->set('modelSumTotal', $modelSumTotal);
        $session->set('statusSSO', $statusSSO);
        $session->set('querySalaryTotalThisMonth3', true);
        $session->set('monthselect', $monthselect);


        return $this->render('account', ['modelEmp' => $modelEmp,
            'modelTemp' => $modelTemp,
            'modelSSO' => $modelSSO,
            'modelAdddeductdtail' => $modelAdddeductdtail,
            'modelSumTotal' => $modelSumTotal,
            'querySalaryTotalThisMonth1' => false,
            'querySalaryTotalThisMonth2' => false,
            'querySalaryTotalThisMonth3' => false,
            'querySalaryTotalThisMonth4' => false,
            'querySalaryTotalThisMonth5' => true,
            'statusSSO' => $statusSSO,
            'tab' => 5,
        ]);

    }

    public function actionExportpdfreportdedetaildeduct()
    {

        $session = Yii::$app->session;
        $modelEmp = $session->get('modelEmp');
        $modelTemp = $session->get('modelTemp');
        $modelSSO = $session->get('modelSSO');
        $modelAdddeductdtail = $session->get('modelAdddeductdtail');
        $modelSumTotal = $session->get('modelSumTotal');
        $statusSSO = $session->get('statusSSO');
        $monthselect = $session->get('monthselect');

        $getMonth = Datetime::convertFormatMonthYear($monthselect);
        $monthGetReport1 = $getMonth['monthresult'];
        $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
        $yearGetReport1 = $getMonth['year'];

        $toTalrecord = count($modelEmp);
        //   echo "<pre>";
        //   print_r ($modelSumTotal);
        //   exit;
        $toTalpage = ceil($toTalrecord / 8);


        //$mpdf = new mPDF('th', 'Tharlon-Regular', '12px');
        $mpdf = new \Mpdf\Mpdf();

        $k = 0;
        $q = 0;
        $z = 0;
        $en = 0;
        $em = 0;
        $lastrecord = 0;
        $somrow = [];
        for ($i = 1; $i <= $toTalpage; $i++) {

            $table = '<table style="border-collapse: collapse; border: 1px solid ' . $this->color . ';" cellpadding="5"  width="100%"  align="center"> ';
            //$table .= '<thead>';
            $table .= '<tr>';
            $table .= '<th style="border: 1px solid ' . $this->color . ';"  rowspan="1" colspan="1"  width="30%">';
            $table .= 'รายการเพิ่ม';
            $table .= '</th>';
            $x = 0;
            for ($j = $en; $j <= ($en + 6); $j++) {
                $x++;
                $table .= '<th  style="border: 1px solid ' . $this->color . ';">' . $modelEmp[$j]['Fullname'] . '</th>';
                $em++;
                if ($i == $toTalpage && $em == $toTalrecord) {
                    $table .= '<th style="border: 1px solid ' . $this->color . ';" rowspan="1" colspan="1"  width="15%">';
                    $table .= 'รวม';
                    $table .= '</th>';
                    $lastrecord = $x;
                    break;
                }
            }

            $table .= '</tr>';
            //$table .= '</thead>';
            //$table .= '<tbody>';
            if ($statusSSO) {
                $table .= '<tr>';
                $table .= '<td style="border: 1px solid ' . $this->color . ';">ประกันสังคม</td>';
                for ($q = 0; $q <= 6; $q++) {
                    if ($i == $toTalpage && $q == $lastrecord) {
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                        $table .= Helper::displayDecimal($somrowsso[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                        $table .= '</td>';
                        break;
                    } else {
                        $indexB = ($q + $en);
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueAdddeducPersonalSSO($modelEmp[$indexB]['ID_Card'], $modelSSO, $somrowsso) . '</td>';
                    }
                }
                $table .= '</tr>';
            }
            foreach ($modelTemp as $key1 => $value) {

                $table .= '<tr>';
                $table .= '<td style="border: 1px solid ' . $this->color . ';">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</td>';
                for ($k = 0; $k <= 6; $k++) {
                    // $table .= '<td align="right">xxxx';
                    // $table .= '</td>';
                    if ($i == $toTalpage && $k == $lastrecord) {
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                        $table .= Helper::displayDecimal($somrow[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                        $table .= '</td>';
                        break;
                    } else {

                        $indexA = ($k + $en);
                        $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">' . $this->getValueAdddeducPersonal($value['ADD_DEDUCT_TEMPLATE_ID'], $modelEmp[$indexA]['ID_Card'], $modelAdddeductdtail, $somrow) . '';
                        $table .= '</td>';
                    }
                }
                $table .= '</tr>';
            }
            $table .= '<tr>';
            $table .= '<td style="border: 1px solid ' . $this->color . ';"><B>รวมรับ</B></td>';

            for ($z = 0; $z <= 6; $z++) {
                if ($i == $toTalpage && $z == $lastrecord) {
                    $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">';
                    // $table .= //Helper::displayDecimal($somrowsumsalary[$value['ADD_DEDUCT_TEMPLATE_ID']]);
                    $table .= '</td>';
                    break;
                } else {
                    $indexC = ($z + $en);
                    $table .= '<td align="right" style="border: 1px solid ' . $this->color . ';">111</td>';
                }
            }
            $table .= '</tr>';
            //$table .= '</tbody>';
            $table .= '</table>';

            $mpdf->SetHTMLHeader('<div style="' . $this->fonthearder . '">รายละเอียดส่วนหัก (รายการหัก) ' . $resultMonthReport1 . ' ปี ' . $yearGetReport1 . '</div>');
            $mpdf->WriteHTML($table);
            $mpdf->AddPage();


            $en = $j;
        }


        //$mpdf->WriteHTML($this->renderPartial('_exportpdfreportadddetaildeduct'));

        $mpdf->Output();
        exit;


    }

    public function getValueAdddeducPersonalSSO($emp, $model, &$sumlineref)
    {
        $out = "0.00";
        foreach ($model as $no => $val) {
            if ($val['WAGE_EMP_ID'] == $emp) {
                $out = Helper::displayDecimal($val['ADD_DEDUCT_THIS_MONTH_AMOUNT']);
                $sumlineref[$val['WAGE_EMP_ID']] += $val['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                break;
            }
        }
        return $out;
    }

    public function getValueAdddeducPersonalSSOTotal($emp, $model)
    {
        $out = "0.00";
        foreach ($model as $no => $val) {
            if ($val['WAGE_EMP_ID'] == $emp) {
                $out = Helper::displayDecimal($val['WAGE_SALARY'] + $val['SUM_ADD_DEDUCT_HIS']);
                break;
            }
        }
        return $out;
    }


    public function actionReportdetailadddeductdetail()
    {
        $postValue = Yii::$app->request->post();

        $arrIdtemp = $postValue['idtemp'];
        $joinarrIdtemp = join(",", (array)$arrIdtemp);

        $arrCompany = $postValue['companyid'];
        $joinarrCompany = join(",", (array)$arrCompany);
        $monthselect = $postValue['monthselect'];
        $checksalary = substr($joinarrIdtemp, 0, 2);

        if ($checksalary == "S1") {
            $checksalary;

            $joinarrIdtemp = substr($joinarrIdtemp, 3);


            $sql = 'SELECT WAGE_EMP_ID ,
                        CONCAT(emp_data.Name," ",emp_data.Surname) as NAMEEMP,
                        department.name AS departmentname,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL AS ADD_DEDUCT_THIS_MONTH_DETAIL,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT 
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON  WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN emp_data ON emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN department ON department.id = WAGE_HISTORY.WAGE_DEPARTMENT_ID
                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';
            $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $sqlsalary = 'SELECT `WAGE_EMP_ID`, `WAGE_WORKING_COMPANY`, `WAGE_DEPARTMENT_ID`, `WAGE_SALARY`, `WAGE_PAY_DATE` 
                        FROM `WAGE_HISTORY` 
                        WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                        AND WAGE_PAY_DATE = "' . $monthselect . '"';

            $modelSalary = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlsalary)->queryAll();

            $sqlSum = 'SELECT emp_data.Name, 
                            WAGE_HISTORY.WAGE_EMP_ID, WAGE_WORKING_COMPANY,
                            working_company.short_name AS short_name,WAGE_PAY_DATE , 
                            WAGE_HISTORY.WAGE_SALARY,
                            SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS 
                    FROM WAGE_HISTORY 
                    INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                    INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY 
                    INNER JOIN emp_data on emp_data.ID_Card=WAGE_HISTORY.WAGE_EMP_ID
                    WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                    AND WAGE_HISTORY.WAGE_PAY_DATE  = "' . $monthselect . '" 
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"  
                    GROUP BY WAGE_HISTORY.WAGE_EMP_ID
                    ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';

            $modelSumTotal = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlSum)->queryAll();
            $statusSalary = true;

            $sqlworkingaddeduct = 'SELECT DISTINCT working_company.id AS working_companyid,
                                working_company.short_name AS short_name,
                                working_company.name AS working_companyname,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL
                                FROM WAGE_HISTORY
                                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                                INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                                AND WAGE_HISTORY.WAGE_PAY_DATE  = "' . $monthselect . '" 
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1" ';
            $modelWorkingcompany = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlworkingaddeduct)->queryAll();

        } else {
            $joinarrIdtemp = join(",", (array)$arrIdtemp);


            $sql = 'SELECT WAGE_EMP_ID ,
                        CONCAT(emp_data.Name," ",emp_data.Surname) as NAMEEMP,
                        department.name AS departmentname,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL AS ADD_DEDUCT_THIS_MONTH_DETAIL,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT 
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN emp_data ON emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN department ON department.id = WAGE_HISTORY.WAGE_DEPARTMENT_ID
                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';
            $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $sqlSum = 'SELECT emp_data.Name, 
                            WAGE_HISTORY.WAGE_EMP_ID, WAGE_WORKING_COMPANY,
                            working_company.short_name AS short_name,WAGE_PAY_DATE , 
                            SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS 
                    FROM WAGE_HISTORY 
                    INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                    INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY 
                    INNER JOIN emp_data on emp_data.ID_Card=WAGE_HISTORY.WAGE_EMP_ID
                    WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                    AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '" 
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"  
                    GROUP BY WAGE_HISTORY.WAGE_EMP_ID
                    ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';

            $modelSumTotal = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlSum)->queryAll();

            $sqlworkingaddeduct = 'SELECT DISTINCT working_company.id AS working_companyid,
                    working_company.short_name AS short_name,
                    working_company.name AS working_companyname,
                    ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                    ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL
                    FROM WAGE_HISTORY
                    INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                    INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                    WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                    AND WAGE_HISTORY.WAGE_PAY_DATE  = "' . $monthselect . '" 
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                    AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"';
            $modelWorkingcompany = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlworkingaddeduct)->queryAll();

        }

        $modelTemp = Adddeducttemplate::find()
            ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
            ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
            ->asArray()
            ->all();

        $modelAdddeductdtailIdcard = [];
        foreach ($modelAdddeductdtail AS $value) {
            $modelAdddeductdtailIdcard[] = ($value['WAGE_EMP_ID']);
        };

        //$modelAdddeductdtailIdcard = array_unique($modelAdddeductdtailIdcard);
        $modelEmp = Empdata::find()
            ->select(['emp_data.ID_Card as ID_Card',
                'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname'])
            ->where(['in', 'ID_Card', $modelAdddeductdtailIdcard])
            ->asArray()
            ->all();

        // $modelWorkingcompany = Workingcompany:: find()
        //                               ->select('id ,name')
        //                               ->where('id IN ('.$joinarrCompany.')')
        //                               ->asArray()
        //                               ->all();

        // echo "<pre>";
        // print_r($modelEmp);
        // print_r($modelTemp);
        // print_r($modelSalary);
        // print_r($modelAdddeductdtail);
        // print_r($modelWorkingcompany);

        // //print_r($modelSumTotal);
        // exit;

        $session = Yii::$app->session;
        $session->set('modelEmp', $modelEmp);
        $session->set('modelTemp', $modelTemp);
        $session->set('modelSalary', $modelSalary);
        $session->set('modelAdddeductdtail', $modelAdddeductdtail);
        $session->set('modelSumTotal', $modelSumTotal);
        $session->set('statusSalary', $statusSalary);
        $session->set('modelWorkingcompany', $modelWorkingcompany);
        $session->set('querySalaryTotalThisMonth6', true);
        $session->set('monthresult', $postValue['monthselect']);


        return $this->render('account', ['modelEmp' => $modelEmp,
            'modelTemp' => $modelTemp,
            'modelSalary' => $modelSalary,
            'modelAdddeductdtail' => $modelAdddeductdtail,
            'modelSumTotal' => $modelSumTotal,
            'modelWorkingcompany' => $modelWorkingcompany,
            'querySalaryTotalThisMonth1' => false,
            'querySalaryTotalThisMonth2' => false,
            'querySalaryTotalThisMonth3' => false,
            'querySalaryTotalThisMonth4' => false,
            'querySalaryTotalThisMonth5' => false,
            'querySalaryTotalThisMonth6' => true,
            'statusSalary' => $statusSalary,
            'tab' => 6,
        ]);

    }

    public function actionExportpdfreportdetailadddeductdetail()
    {
        //$mpdf = new mPDF('th', 'Tharlon-Regular', '16px');
        $mpdf = new \Mpdf\Mpdf();

        $mpdf->WriteHTML($this->renderPartial('_exportpdfreportdetailadddeductdetail'));
        $mpdf->Output();
        exit;
    }


    public function actionReportdetaildeductdetail()
    {
        $postValue = Yii::$app->request->post();

        $arrIdtemp = $postValue['idtemp'];
        $joinarrIdtemp = join(",", (array)$arrIdtemp);

        $arrCompany = $postValue['companyid'];
        $joinarrCompany = join(",", (array)$arrCompany);
        $monthselect = $postValue['selectmonth'];
        $checksalary = substr($joinarrIdtemp, 0, 4);


        if ($checksalary == "SSO1") {
            $joinarrIdtemp = substr($joinarrIdtemp, 5);
            $SSO = "%ประกันสังคม";

            $sql = 'SELECT WAGE_EMP_ID ,
                        CONCAT(emp_data.Name," ",emp_data.Surname) as NAMEEMP,
                        department.name AS departmentname,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS ADD_DEDUCT_THIS_MONTH_AMOUNT
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN emp_data ON emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN department ON department.id = WAGE_HISTORY.WAGE_DEPARTMENT_ID
                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT != "0"
                GROUP BY WAGE_EMP_ID
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';

            $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            //cho "<br>";

            $sqlsso = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                                WAGE_WORKING_COMPANY,
                                CONCAT(emp_data.Name," ",emp_data.Surname) as NAMEEMP,
                                WAGE_EMP_ID,
                                department.name AS departmentname,
                                working_company.short_name AS short_name,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                WAGE_PAY_DATE ,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT AS ADD_DEDUCT_THIS_MONTH_AMOUNT,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_DETAIL AS ADD_DEDUCT_THIS_MONTH_DETAIL
                                FROM WAGE_HISTORY 
                                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                                INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                INNER JOIN emp_data ON emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                                INNER JOIN department ON department.id = WAGE_HISTORY.WAGE_DEPARTMENT_ID
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME LIKE "' . $SSO . '"
                                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';
            $modelSSO = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlsso)->queryAll();
            $statusSSO = true;

            $sqlworkingaddeduct = 'SELECT DISTINCT working_company.id AS working_companyid,
                                working_company.short_name AS short_name,
                                working_company.name AS working_companyname,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL
                                FROM WAGE_HISTORY
                                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                                INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                                AND WAGE_HISTORY.WAGE_PAY_DATE  = "' . $monthselect . '" 
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT != "0"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2" ';
            $modelWorkingcompany = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlworkingaddeduct)->queryAll();

            $sqlworkingsso = 'SELECT DISTINCT working_company.id AS working_companyid,
                                working_company.short_name AS short_name,
                                working_company.name AS working_companyname,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL
                                FROM WAGE_HISTORY
                                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                                INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME LIKE "' . $SSO . '"
                                AND WAGE_HISTORY.WAGE_PAY_DATE  = "' . $monthselect . '" 
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT != "0"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2" ';
            $modelWorkingcompanysso = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlworkingsso)->queryAll();
        } else {

            $sql = 'SELECT WAGE_EMP_ID ,
                        CONCAT(emp_data.Name," ",emp_data.Surname) as NAMEEMP,
                        department.name AS departmentname,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        WAGE_PAY_DATE ,
                        SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS ADD_DEDUCT_THIS_MONTH_AMOUNT
                FROM WAGE_HISTORY 
                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN emp_data ON emp_data.ID_Card = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                INNER JOIN department ON department.id = WAGE_HISTORY.WAGE_DEPARTMENT_ID
                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT != "0"
                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                GROUP BY WAGE_EMP_ID
                ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC';

            $modelAdddeductdtail = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();

            $sqlworkingaddeduct = 'SELECT DISTINCT working_company.id AS working_companyid,
                                working_company.short_name AS short_name,
                                working_company.name AS working_companyname,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                                ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL
                                FROM WAGE_HISTORY
                                INNER JOIN ADD_DEDUCT_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID 
                                INNER JOIN working_company ON working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN(' . $joinarrIdtemp . ')
                                AND WAGE_HISTORY.WAGE_PAY_DATE  = "' . $monthselect . '" 
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT != "0"
                                AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2" ';
            $modelWorkingcompany = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlworkingaddeduct)->queryAll();


        }

        $modelTemp = Adddeducttemplate::find()
            ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
            ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
            ->asArray()
            ->all();

        $modelAdddeductdtailIdcard = [];
        foreach ($modelAdddeductdtail AS $value) {
            $modelAdddeductdtailIdcard[] = ($value['WAGE_EMP_ID']);
        };

        //$modelAdddeductdtailIdcard = array_unique($modelAdddeductdtailIdcard);
        $modelEmp = Empdata::find()
            ->select(['emp_data.ID_Card as ID_Card',
                'CONCAT(emp_data.Name," ",emp_data.Surname) as Fullname'])
            ->where(['in', 'ID_Card', $modelAdddeductdtailIdcard])
            ->asArray()
            ->all();

        $session = Yii::$app->session;
        $session->set('modelEmp', $modelEmp);
        $session->set('modelTemp', $modelTemp);
        $session->set('modelSalary', $modelSalary);
        $session->set('modelAdddeductdtail', $modelAdddeductdtail);
        $session->set('modelSumTotal', $modelSumTotal);
        $session->set('statusSSO', $statusSSO);
        $session->set('modelSSO', $modelSSO);
        $session->set('modelWorkingcompany', $modelWorkingcompany);
        $session->set('querySalaryTotalThisMonth7', true);
        $session->set('monthresult', $postValue['monthselect']);
        $session->set('modelWorkingcompanysso', $modelWorkingcompanysso);


        return $this->render('account', ['modelEmp' => $modelEmp,
            'modelTemp' => $modelTemp,
            'modelSalary' => $modelSalary,
            'modelAdddeductdtail' => $modelAdddeductdtail,
            'modelSumTotal' => $modelSumTotal,
            'modelWorkingcompany' => $modelWorkingcompany,
            'querySalaryTotalThisMonth1' => false,
            'querySalaryTotalThisMonth2' => false,
            'querySalaryTotalThisMonth3' => false,
            'querySalaryTotalThisMonth4' => false,
            'querySalaryTotalThisMonth5' => false,
            'querySalaryTotalThisMonth6' => false,
            'querySalaryTotalThisMonth7' => true,
            'statusSSO' => $statusSSO,
            'modelSSO' => $modelSSO,
            'modelWorkingcompanysso' => $modelWorkingcompanysso,
            'tab' => 7,
        ]);

    }

    public function actionExportpdfreportdetaildeductdetail()
    {
        //$mpdf = new mPDF('th', 'Tharlon-Regular', '16px');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_exportpdfreportdetaildeductdetail'));
        $mpdf->Output();
        exit;
    }


    public function actionReportbenefitfund()
    {
        $postValue = Yii::$app->request->post();
        $companyid = $postValue['companyid'];
        $monthselect = $postValue['selectmonth'];
        $beforemonth = substr($postValue['selectmonth'], 0, 2);
        $beforeyear = substr($postValue['selectmonth'], 3, 4);
        $mydate = "$beforeyear-$beforemonth";
        $lastyear = strtotime("-1 month", strtotime($mydate));
        $monthselectbefore = date("m-Y", $lastyear);

        // echo "<pre>";
        // print_r($postValue);
        $joinarrCompany = join(",", (array)$companyid);

        $sqlbenefit = 'SELECT WAGE_WORKING_COMPANY,
                             BENEFIT_FUND.*,
                             CONCAT(ERP_easyhr_checktime.emp_data.Name," ",ERP_easyhr_checktime.emp_data.Surname) as NAMEEMP,
                             WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY
                    FROM BENEFIT_FUND 
                    INNER JOIN WAGE_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = BENEFIT_FUND.BENEFIT_EMP_ID
                    INNER JOIN ERP_easyhr_checktime.emp_data ON ERP_easyhr_checktime.emp_data.ID_Card = BENEFIT_FUND.BENEFIT_EMP_ID
                    WHERE BENEFIT_SAVING_DATE = "' . $monthselect . '"
                    AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                    AND WAGE_HISTORY.WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                    AND BENEFIT_FUND.BENEFIT_FUND_STATUS != "99"';
        $modelbenefit = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlbenefit)->queryAll();

        $sqlbenefitbefore = 'SELECT WAGE_WORKING_COMPANY,
                             BENEFIT_FUND.*,
                             CONCAT(ERP_easyhr_checktime.emp_data.Name," ",ERP_easyhr_checktime.emp_data.Surname) as NAMEEMP,
                             WAGE_HISTORY.WAGE_SALARY as WAGE_SALARY
                    FROM BENEFIT_FUND 
                    INNER JOIN WAGE_HISTORY ON WAGE_HISTORY.WAGE_EMP_ID = BENEFIT_FUND.BENEFIT_EMP_ID
                    INNER JOIN ERP_easyhr_checktime.emp_data ON ERP_easyhr_checktime.emp_data.ID_Card = BENEFIT_FUND.BENEFIT_EMP_ID
                    WHERE BENEFIT_SAVING_DATE = "' . $monthselectbefore . '"
                    AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselectbefore . '"
                    AND WAGE_HISTORY.WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')
                    AND BENEFIT_FUND.BENEFIT_FUND_STATUS != "99"';
        $modelbenefitbefore = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlbenefitbefore)->queryAll();

        $sqlworkingcompany = 'SELECT DISTINCT ERP_easyhr_OU.working_company.id AS working_companyid,
                                    ERP_easyhr_OU.working_company.short_name AS short_name,
                                    ERP_easyhr_OU.working_company.name AS working_companyname,
                                    WAGE_HISTORY.WAGE_WORKING_COMPANY
                                FROM WAGE_HISTORY
                                INNER JOIN ERP_easyhr_OU.working_company ON ERP_easyhr_OU.working_company.id = WAGE_HISTORY.WAGE_WORKING_COMPANY
                                WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ')';


        $modelcompany = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sqlworkingcompany)->queryAll();
        // echo "<pre>";
        // print_r($modelbenefit);

        $session = Yii::$app->session;
        $session->set('modelbenefit', $modelbenefit);
        $session->set('modelcompany', $modelcompany);
        $session->set('monthselect', $monthselect);
        $session->set('modelbenefitbefore', $modelbenefitbefore);

        return $this->render('account', ['modelbenefit' => $modelbenefit,
            'modelcompany' => $modelcompany,
            'monthselect' => $monthselect,
            'modelbenefitbefore' => $modelbenefitbefore,
            'querySalaryTotalThisMonth8' => true,
            'tab' => 8,]);
    }

    public function actionExportpdfreportbenefitfund()
    {
        //$mpdf = new mPDF('th', 'Tharlon-Regular', '12px');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_exportpdfreportbenefitfund'));
        $mpdf->Output();
        exit;
    }


    /** Report Function  */
    public function actionGetreport1()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {

            $postValue = Yii::$app->request->post();
            $getMonth = Datetime::convertFormatMonthYear($postValue['xmonth']);
            $monthGetReport1 = $getMonth['monthresult'];
            $resultMonthReport1 = Datetime::mappingMonth($monthGetReport1);
            $yearGetReport1 = $getMonth['year'];


            if (isset($postValue)) {
                $arrCompany = $postValue['xcompany'];
                $monthselect = $postValue['xmonth'];
                $joinarrCompany = join(",", (array)$arrCompany);

                $modeldata = Wagehistory::find()
                    ->select('SUM(WAGE_SALARY) as SUMSALARY,
                        COMPANY_NAME,
                        SUM(WAGE_TOTAL_ADDITION) as SUM_WAGE_TOTAL_ADDITION,
                        SUM(WAGE_TOTAL_DEDUCTION) as WAGE_TOTAL_DEDUCTION,
                        SUM(WAGE_NET_SALARY) as SUM_WAGE_NET_SALARY')
                    ->where('WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ') AND  WAGE_PAY_DATE = :monthselect')
                    ->groupby('WAGE_WORKING_COMPANY')
                    ->addParams([':monthselect' => $monthselect,])
                    ->asArray()
                    ->all();

                $session = Yii::$app->session;
                $session->set('reportSalaryTotal', $modeldata);
                $session->set('monthselect', $monthselect);



                $tbl = '';
                $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong> รายงานสรุปรวมผลเงินเดือน รอบเงินเดือน ' . $resultMonthReport1 . ' ' . $yearGetReport1 . '</strong></p>
                            </div>
                    </div>';

                $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfsumtotalallthismonth" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF
                                    </button>
                                </a>
                            </div>
                            <table  cellspacing="2" class="table table-striped table-bordered rpt" width="80%" align="center">
                                <thead>
                                <tr role="row">
                                    <th aria-sort="ascending">ชื่อบริษัท</th>
                                    <th aria-sort="ascending" class="text-center">เงินเดือน</th>
                                    <th aria-sort="ascending" class="text-center">ส่วนเพิ่ม</th>
                                    <th aria-sort="ascending" class="text-center">รวมรับ</th>
                                    <th aria-sort="ascending" class="text-center">ส่วนหัก</th>
                                    <th aria-sort="ascending" class="text-center">รวมรับทั้งสิ้น</th>
                                </tr>
                                </thead>
                                <tbody>';
                                $TotalSalary = $TotalAdd = $TotalEarn = $TotalDeduct = $TotalNet = 0;
                                foreach ($modeldata as $value) {
                                    $sumSalaryAddDeduc = $value['SUMSALARY'] + $value['SUM_WAGE_TOTAL_ADDITION'];

                                    $TotalSalary += $value['SUMSALARY'];
                                    $TotalAdd += $value['SUM_WAGE_TOTAL_ADDITION'];
                                    $TotalEarn += $sumSalaryAddDeduc;
                                    $TotalDeduct += $value['WAGE_TOTAL_DEDUCTION'];
                                    $TotalNet += $sumSalaryAddDeduc-$value['WAGE_TOTAL_DEDUCTION'];

                                    $tbl .='<tr>
                                        <td width="30%">'.$value['COMPANY_NAME'].'</td>
                                        <td width="15"  align="right">'.Helper::displayDecimal($value['SUMSALARY']) .'</td>
                                        <td width="15%" align="right">'.Helper::displayDecimal($value['SUM_WAGE_TOTAL_ADDITION']) .'</td>
                                        <td width="15%" align="right">'. Helper::displayDecimal($sumSalaryAddDeduc).'</td>
                                        <td width="15%" align="right">'.Helper::displayDecimal($value['WAGE_TOTAL_DEDUCTION']).'</td>
                                        <td width="15%" align="right">'.Helper::displayDecimal($sumSalaryAddDeduc - $value['WAGE_TOTAL_DEDUCTION']).'</td>
                                    </tr>';

                                }

                                $tbl .='</tbody>
                                <tfoot>
                                    <tr role="row">
                                        <th aria-sort="ascending" >รวม</th>
                                        <th aria-sort="ascending" >'.Helper::displayDecimal($TotalSalary).'</th>
                                        <th aria-sort="ascending" >'.Helper::displayDecimal($TotalAdd).'</th>
                                        <th aria-sort="ascending" >'. Helper::displayDecimal($TotalEarn).'</th>
                                        <th aria-sort="ascending" >'.Helper::displayDecimal($TotalDeduct).'</th>
                                        <th aria-sort="ascending" >'.Helper::displayDecimal($TotalNet).'</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>';

                return $tbl;
            }
        }
    }

    public function actionTesta() {
        //calculate data for PDF and set to session
        $modelWorkingcompany = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

        $data_page = 8;
        $total_data_columns = count($modelWorkingcompany);
        $total_page = ceil(($total_data_columns/$data_page));
        $arrDataPagePDF = array_chunk($modelWorkingcompany,$data_page,true);

        /*
        echo '<pre>';
        print_r($arrDataPagePDF);
        echo '</pre>';
        */

        //$mpdf = new mPDF('th', 'Tharlon-Regular', '16px');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->renderPartial('_exporttest',[
            'arrDataPagePDF'=>$arrDataPagePDF,
        ]));
        $mpdf->Output();
        exit;

    }

    public function actionGetreport2()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $arrIdtemp = $postValue['xtemplate'];
            $joinarrIdtemp = join(",", (array)$arrIdtemp);
            $arrCompany = $postValue['xcompany'];
            $joinarrCompany = join(",", (array)$arrCompany);
            $monthselect = $postValue['xmonth'];

            $dt = explode('-',$monthselect);
            $_dpMonth = DateTime::convertMonth($dt[0]);
            $_dpYear  = $dt[1];


            $sql = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        WAGE_PAY_DATE ,
                        SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS
                        FROM WAGE_HISTORY 
                        INNER JOIN ADD_DEDUCT_HISTORY ON  WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                        
                        WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ') 
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN (' . $joinarrIdtemp . ') 
                        AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "1"
                        GROUP BY ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                        WAGE_HISTORY.WAGE_WORKING_COMPANY
                        ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';

            $modelData = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
            $arrItemData = [];
            foreach ($modelData as $item) {
                $arrItemData[$item['ADD_DEDUCT_THIS_MONTH_TMP_ID']][$item['WAGE_WORKING_COMPANY']] = $item['SUM_ADD_DEDUCT_HIS'];
            }


            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();

            $modelWorkingcompany = Workingcompany:: find()
                ->select('id ,short_name')
                ->where('id IN (' . $joinarrCompany . ')')
                ->asArray()
                ->all();


            //calculate data for PDF and set to session
            $data_page = 8;
            $total_data_columns = count($modelWorkingcompany);
            $total_page = ceil(($total_data_columns/$data_page));

            $arrDataColumnPDF = array_chunk($modelWorkingcompany,$data_page,true);


            //store to SESSION
            $session = Yii::$app->session;
            $session->set('total_page', $total_page);
            $session->set('arrItemData', $arrItemData);
            $session->set('monthselect', $monthselect);
            $session->set('modelTemp', $modelTemp);
            $session->set('modelWorkingcompany', $modelWorkingcompany);
            $session->set('arrDataColumnPDF', $arrDataColumnPDF);


            $tbl = '';
            $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong> รายงานสรุปส่วนเพิ่ม รอบเงินเดือน ' . $_dpMonth  . ' ' . $_dpYear . '</strong></p>
                            </div>
                    </div>';

            $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfrpt2" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF
                                    </button>
                                </a>
                            </div>
                            <table  cellspacing="2" class="table table-striped table-bordered rpt"  align="center">
                                <thead>
                                <tr role="row" style="font-size: 12px;font-weight: bold">
                                    <th aria-sort="ascending" width="200">ชื่อบริษัท/รายการเพิ่ม</th>';
                                    foreach ($modelWorkingcompany as $item) {
                                        $tbl .='<th width="100" aria-sort="ascending" class="text-center">'.$item['short_name'].'</th>';
                                    }
                        $tbl .='<th width="100" aria-sort="ascending" class="text-center">รวม</th>';

                        $tbl .= '</tr>
                                    </thead>
                                    <tbody>';
                                    $TotalComp = [];
                                    foreach ($modelTemp as $value) {
                                        $tbl .='<tr>';
                                        $tbl .='<td width="200">'.$value['ADD_DEDUCT_TEMPLATE_NAME'].'</td>';
                                        $LineTotal = 0;
                                        foreach ($modelWorkingcompany as $item) {
                                            $data = (isset($arrItemData[$value['ADD_DEDUCT_TEMPLATE_ID']][$item['id']])) ? $arrItemData[$value['ADD_DEDUCT_TEMPLATE_ID']][$item['id']] : 0;
                                            $LineTotal += $data;
                                            $TotalComp[$item['id']][$value['ADD_DEDUCT_TEMPLATE_ID']] = $data;
                                            $show = ($data > 0 ) ? Helper::displayDecimal($data) : '';
                                            $tbl .='<td width="100" style="text-align:right">'.$show.'</td>';
                                        }
                                        $SumLine = ($LineTotal > 0) ? Helper::displayDecimal($LineTotal) : '';
                                        $tbl .='<td width="100" style="text-align:right">'.$SumLine.'</td>';
                                        $tbl .='</tr>';
                                    }

                                $tbl .='</tbody>
                                        <tfoot>
                                        <tr role="row">
                                            <th aria-sort="ascending" >รวม</th>';
                                            $TotalMoney = 0;
                                            foreach ($modelWorkingcompany as $item) {
                                                $SumComp = (isset($TotalComp[$item['id']])) ? array_sum($TotalComp[$item['id']]) : 0;
                                                $show = ($SumComp > 0 ) ? Helper::displayDecimal($SumComp) : '';
                                                $TotalMoney += $SumComp;
                                                $tbl .='<th aria-sort="ascending" class="text-center">'.$show.'</th>';
                                            }
                                $tbl .='<th aria-sort="ascending" class="text-center">'.Helper::displayDecimal($TotalMoney).'</th>';
                                $tbl .='</tr>
                                    </tfoot>
                             </table>
                        </div>
                    </div>';

            return $tbl;
        }
    }

    public function actionExportpdfrpt2()
    {
        //$mpdf = new mPDF('th', 'Tharlon-Regular', '16px');
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->title = 'รายงานสรุปส่วนเพิ่ม';
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfrpt2'));
        $mpdf->Output();
        exit;
    }

    public function actionGetreport3()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $arrIdtemp = $postValue['xtemplate'];
            $joinarrIdtemp = join(",", (array)$arrIdtemp);
            $companyid = $postValue['xcompany'];
            $monthselect = $postValue['xmonth'];


            $dt = explode('-',$monthselect);
            $_dpMonth = DateTime::convertMonth($dt[0]);
            $_dpYear  = $dt[1];


            $sql = "SELECT  WAGE_HISTORY.WAGE_EMP_ID, CONCAT(WAGE_HISTORY.WAGE_FIRST_NAME,' ',WAGE_HISTORY.WAGE_LAST_NAME) as FULL_NAME, WAGE_HISTORY.WAGE_SALARY
                    FROM WAGE_HISTORY WHERE  WAGE_WORKING_COMPANY = '$companyid' AND WAGE_PAY_DATE = '$monthselect' ORDER BY FULL_NAME ";



            $modelEmpSalary = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
            $arrStaffData = $arrItemData = $arrEmpIDCard = [];
            foreach ($modelEmpSalary as $item) {
                $arrStaffData[$item['WAGE_EMP_ID']] = $item;
                $arrItemData[0][$item['WAGE_EMP_ID']] = $item['WAGE_SALARY'];
                $arrEmpIDCard[$item['WAGE_EMP_ID']] = $item['WAGE_EMP_ID'];
            }


            $modelData = Adddeducthistory::find()
                ->select('ADD_DEDUCT_THIS_MONTH_TMP_ID,ADD_DEDUCT_THIS_MONTH_EMP_ID,ADD_DEDUCT_THIS_MONTH_AMOUNT')
                ->where([
                    'ADD_DEDUCT_THIS_MONTH_TYPE'=>1,
                    'ADD_DEDUCT_THIS_MONTH_PAY_DATE'=>$monthselect,
                ])
                ->andWhere([
                    'IN','ADD_DEDUCT_THIS_MONTH_EMP_ID',$arrEmpIDCard
                ])
                ->orderBy('ADD_DEDUCT_THIS_MONTH_TMP_ID')
                ->asArray()
                ->all();

            foreach ($modelData as $item) {
                $arrItemData[$item['ADD_DEDUCT_THIS_MONTH_TMP_ID']][$item['ADD_DEDUCT_THIS_MONTH_EMP_ID']] = $item['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
            }


            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();
            $arrTemplate = [];
            $arrTemplate[0] = 'เงินเดือน';
            foreach ($modelTemp as $item) {
                $arrTemplate[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item['ADD_DEDUCT_TEMPLATE_NAME'];
            }


            $modelWorkingcompany = Workingcompany:: find()
                ->select('id ,short_name,name')
                ->where('id IN (' . $companyid . ')')
                ->asArray()
                ->all();


            //calculate data for PDF and set to session
            $data_page = 5;
            $total_data_columns = count($arrStaffData);
            $total_page = ceil(($total_data_columns/$data_page));

            $arrDataColumnPDF = array_chunk($arrStaffData,$data_page,true);

            //store to SESSION
            $session = Yii::$app->session;
            $session->set('total_page', $total_page);
            $session->set('arrItemData', $arrItemData);
            $session->set('monthselect', $monthselect);
            $session->set('arrTemplate', $arrTemplate);
            $session->set('modelWorkingcompany', $modelWorkingcompany);
            $session->set('arrDataColumnPDF', $arrDataColumnPDF);



            $tbl = '';
            $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong> รายงานรายละเอียดส่วนเพิ่ม รอบเงินเดือน ' . $_dpMonth  . ' ' . $_dpYear .' '.$modelWorkingcompany[0]['name']. '</strong></p>
                            </div>
                    </div>';
            $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfrpt3" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF</button>
                                </a>
                            </div>
                            
                            <table  cellspacing="2" class="table table-striped table-bordered rpt"  align="center">
                                <thead>
                                <tr role="row" style="font-size: 12px;font-weight: bold">
                                    <th width="200" style="white-space: nowrap;">รายการเพิ่ม/ชื่อพนักงาน</th>';
                                    foreach ($arrStaffData as $item) {
                                        $tbl .='<th width="200"  class="text-center">'.$item['FULL_NAME'].'</th>';
                                    }
                                    $tbl .='<th width="100" class="text-center">รวม</th>';
                                    $tbl .= '</tr>
                                            </thead>
                                            <tbody>';
                                    $TotalComp = [];
                                    foreach ($arrTemplate as $key => $value) {
                                        $tbl .='<tr style="font-size: 12px;">';
                                        $tbl .='<td width="200">'.$value.'</td>';
                                        $LineTotal = 0;
                                        foreach ($arrStaffData as $IDCard => $item) {
                                            //$TotalComp[$item['id']][$value['ADD_DEDUCT_TEMPLATE_ID']] = $data;
                                            $data = (isset($arrItemData[$key][$IDCard])) ? $arrItemData[$key][$IDCard] : 0 ;
                                            $LineTotal += $data;
                                            $TotalComp[$IDCard][$key] = $data;
                                            $show = ($data > 0 ) ? Helper::displayDecimal($data) : '';
                                            $tbl .='<td width="100" class="text-right">'.$show.'</td>';
                                        }
                                        $SumLine = ($LineTotal > 0) ? Helper::displayDecimal($LineTotal) : '';
                                        $tbl .='<td width="100">'.$SumLine.'</td>';
                                        $tbl .='</tr>';
                                    }

                                    $tbl .='</tbody>
                                                <tfoot>
                                                <tr role="row">
                                                    <th aria-sort="ascending" >รวม</th>';
                                    $TotalMoney = 0;
                                    foreach ($arrStaffData as $item) {
                                        $SumComp = (isset($TotalComp[$item['WAGE_EMP_ID']])) ? array_sum($TotalComp[$item['WAGE_EMP_ID']]) : 0;
                                        $show = ($SumComp > 0 ) ? Helper::displayDecimal($SumComp) : '';
                                        $TotalMoney += $SumComp;
                                        $tbl .='<th aria-sort="ascending" class="text-center">'.$show.'</th>';
                                    }
                                    $tbl .='<th aria-sort="ascending" class="text-center">'.Helper::displayDecimal($TotalMoney).'</th>';
                                    $tbl .='</tr>
                                    </tfoot>
                             </table>
                        </div>
                    </div>';


            return $tbl;
        }
    }

    public function actionExportpdfrpt3()
    {
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDefaultFont('THSarabun');
        $mpdf->title = 'รายงานรายละเอียดส่วนเพิ่ม';
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfrpt3'));
        $mpdf->Output();
        exit;
    }


    public function actionGetreport4()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();


            $arrIdtemp = $postValue['xtemplate'];
            $joinarrIdtemp = join(",", (array)$arrIdtemp);
            $arrCompany = $postValue['xcompany'];
            $joinarrCompany = join(",", (array)$arrCompany);
            $monthselect = $postValue['xmonth'];

            $_c = count($arrCompany);
            $_fc_width = 200;
            $_lc_with = 100;
            $_tbl_width = ($_c*100) + $_fc_width + $_lc_with;

            $dt = explode('-',$monthselect);
            $_dpMonth = DateTime::convertMonth($dt[0]);
            $_dpYear  = $dt[1];


            $sql = 'SELECT ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_NAME AS ADD_DEDUCT_NAME_DETAIL,
                        WAGE_WORKING_COMPANY,
                        
                        ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID AS ADD_DEDUCT_THIS_MONTH_TMP_ID,
                        WAGE_PAY_DATE ,
                        SUM(ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_AMOUNT) AS SUM_ADD_DEDUCT_HIS
                        FROM WAGE_HISTORY 
                        INNER JOIN ADD_DEDUCT_HISTORY ON  WAGE_HISTORY.WAGE_EMP_ID = ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_EMP_ID
                        
                        WHERE WAGE_WORKING_COMPANY IN (' . $joinarrCompany . ') 
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID IN (' . $joinarrIdtemp . ') 
                        AND WAGE_HISTORY.WAGE_PAY_DATE = "' . $monthselect . '"
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_PAY_DATE = "' . $monthselect . '"
                        AND ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TYPE = "2"
                        GROUP BY ADD_DEDUCT_HISTORY.ADD_DEDUCT_THIS_MONTH_TMP_ID ,
                        WAGE_HISTORY.WAGE_WORKING_COMPANY
                        ORDER BY `WAGE_HISTORY`.`WAGE_WORKING_COMPANY` ASC ';


            $modelData = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
            $arrItemData = [];
            foreach ($modelData as $item) {
                $arrItemData[$item['ADD_DEDUCT_THIS_MONTH_TMP_ID']][$item['WAGE_WORKING_COMPANY']] = $item['SUM_ADD_DEDUCT_HIS'];
            }


            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();

            $modelWorkingcompany = Workingcompany:: find()
                ->select('id ,short_name')
                ->where('id IN (' . $joinarrCompany . ')')
                ->asArray()
                ->all();



            //calculate data for PDF and set to session
            $data_page = 8;
            $total_data_columns = count($modelWorkingcompany);
            $total_page = ceil(($total_data_columns/$data_page));

            $arrDataColumnPDF = array_chunk($modelWorkingcompany,$data_page,true);


            //store to SESSION
            $session = Yii::$app->session;
            $session->set('total_page', $total_page);
            $session->set('arrItemData', $arrItemData);
            $session->set('monthselect', $monthselect);
            $session->set('modelTemp', $modelTemp);
            $session->set('modelWorkingcompany', $modelWorkingcompany);
            $session->set('arrDataColumnPDF', $arrDataColumnPDF);


            $tbl = '';
            $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong> รายงานสรุปส่วนหัก รอบเงินเดือน ' . $_dpMonth  . ' ' . $_dpYear . '</strong></p>
                            </div>
                    </div>';

            $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfrpt4" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF</button>
                                </a>
                            </div>
                            
                            <table  cellspacing="2" class="table table-striped table-bordered rpt"  align="center">
                                <thead>
                                <tr role="row" style="font-size: 12px;font-weight: bold">
                                    <th width="200" style="white-space: nowrap;">ชื่อบริษัท/รายการหัก</th>';
                                        foreach ($modelWorkingcompany as $item) {
                                            $tbl .='<th width="'.$_lc_with.'"  class="text-center">'.$item['short_name'].'</th>';
                                        }
                            $tbl .='<th width="'.$_lc_with.'" class="text-center">รวม</th>';
                            $tbl .= '</tr>
                                    </thead>
                                    <tbody>';
                            $TotalComp = [];
                            foreach ($modelTemp as $value) {
                                $tbl .='<tr style="font-size: 12px;">';
                                $tbl .='<td width="200">'.$value['ADD_DEDUCT_TEMPLATE_NAME'].'</td>';
                                $LineTotal = 0;
                                foreach ($modelWorkingcompany as $item) {
                                    $data = (isset($arrItemData[$value['ADD_DEDUCT_TEMPLATE_ID']][$item['id']])) ? $arrItemData[$value['ADD_DEDUCT_TEMPLATE_ID']][$item['id']] : 0;
                                    $LineTotal += $data;
                                    $TotalComp[$item['id']][$value['ADD_DEDUCT_TEMPLATE_ID']] = $data;
                                    $show = ($data > 0 ) ? Helper::displayDecimal($data) : '';
                                    $tbl .='<td width="100"  style="text-align:right">'.$show.'</td>';
                                }
                                $SumLine = ($LineTotal > 0) ? Helper::displayDecimal($LineTotal) : '';
                                $tbl .='<td width="100"  style="text-align:right">'.$SumLine.'</td>';
                                $tbl .='</tr>';
                            }

                        $tbl .='</tbody>
                        <tfoot>
                        <tr role="row">
                            <th aria-sort="ascending" >รวม</th>';
                                $TotalMoney = 0;
                                foreach ($modelWorkingcompany as $item) {
                                    $SumComp = (isset($TotalComp[$item['id']])) ? array_sum($TotalComp[$item['id']]) : 0;
                                    $show = ($SumComp > 0 ) ? Helper::displayDecimal($SumComp) : '';
                                    $TotalMoney += $SumComp;
                                    $tbl .='<th aria-sort="ascending" class="text-center">'.$show.'</th>';
                                }
                                $tbl .='<th aria-sort="ascending" class="text-center">'.Helper::displayDecimal($TotalMoney).'</th>';
                                $tbl .='</tr>
                                    </tfoot>
                             </table>
                        </div>
                    </div>';


            return $tbl;
        }
    }

    public function actionExportpdfrpt4()
    {
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDefaultFont('THSarabun');
        $mpdf->title = 'รายงานสรุปส่วนหัก';
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfrpt4'));
        $mpdf->Output();
        exit;
    }

    public function actionGetreport5()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $arrIdtemp = $postValue['xtemplate'];
            $joinarrIdtemp = join(",", (array)$arrIdtemp);
            $companyid = $postValue['xcompany'];
            $monthselect = $postValue['xmonth'];


            $dt = explode('-',$monthselect);
            $_dpMonth = DateTime::convertMonth($dt[0]);
            $_dpYear  = $dt[1];



            $sql = "SELECT  WAGE_HISTORY.WAGE_EMP_ID, CONCAT(WAGE_HISTORY.WAGE_FIRST_NAME,' ',WAGE_HISTORY.WAGE_LAST_NAME) as FULL_NAME, WAGE_HISTORY.WAGE_SALARY
                    FROM WAGE_HISTORY WHERE  WAGE_WORKING_COMPANY = '$companyid' AND WAGE_PAY_DATE = '$monthselect' ORDER BY FULL_NAME ";

            $modelEmpSalary = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
            $arrStaffData = $arrItemData = $arrEmpIDCard = [];
            foreach ($modelEmpSalary as $item) {
                $arrStaffData[$item['WAGE_EMP_ID']] = $item;
                //$arrItemData[0][$item['WAGE_EMP_ID']] = $item['WAGE_SALARY'];
                $arrEmpIDCard[$item['WAGE_EMP_ID']] = $item['WAGE_EMP_ID'];
            }


            $modelData = Adddeducthistory::find()
                ->select('ADD_DEDUCT_THIS_MONTH_TMP_ID,ADD_DEDUCT_THIS_MONTH_EMP_ID,ADD_DEDUCT_THIS_MONTH_AMOUNT')
                ->where([
                    'ADD_DEDUCT_THIS_MONTH_TYPE'=>2,
                    'ADD_DEDUCT_THIS_MONTH_PAY_DATE'=>$monthselect,
                ])
                ->andWhere([
                    'IN','ADD_DEDUCT_THIS_MONTH_EMP_ID',$arrEmpIDCard
                ])
                ->orderBy('ADD_DEDUCT_THIS_MONTH_TMP_ID')
                ->asArray()
                ->all();

            foreach ($modelData as $item) {
                $arrItemData[$item['ADD_DEDUCT_THIS_MONTH_TMP_ID']][$item['ADD_DEDUCT_THIS_MONTH_EMP_ID']] += $item['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
            }


            $modelTemp = Adddeducttemplate::find()
                ->select('ADD_DEDUCT_TEMPLATE_ID ,ADD_DEDUCT_TEMPLATE_NAME')
                ->where('ADD_DEDUCT_TEMPLATE_ID IN (' . $joinarrIdtemp . ')')
                ->asArray()
                ->all();
            $arrTemplate = [];
            //$arrTemplate[0] = 'เงินเดือน';
            foreach ($modelTemp as $item) {
                $arrTemplate[$item['ADD_DEDUCT_TEMPLATE_ID']] = $item['ADD_DEDUCT_TEMPLATE_NAME'];
            }


            $modelWorkingcompany = Workingcompany:: find()
                ->select('id ,short_name,name')
                ->where('id IN (' . $companyid . ')')
                ->asArray()
                ->all();


            //calculate data for PDF and set to session
            $data_page = 5;
            $total_data_columns = count($arrStaffData);
            $total_page = ceil(($total_data_columns/$data_page));

            $arrDataColumnPDF = array_chunk($arrStaffData,$data_page,true);

            //store to SESSION
            $session = Yii::$app->session;
            $session->set('total_page', $total_page);
            $session->set('arrItemData', $arrItemData);
            $session->set('monthselect', $monthselect);
            $session->set('arrTemplate', $arrTemplate);
            $session->set('modelWorkingcompany', $modelWorkingcompany);
            $session->set('arrDataColumnPDF', $arrDataColumnPDF);




            $tbl = '';
            $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong> รายงานรายละเอียดส่วนหัก รอบเงินเดือน ' . $_dpMonth  . ' ' . $_dpYear .' '.$modelWorkingcompany[0]['name']. '</strong></p>
                            </div>
                    </div>';
            $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfrpt5" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF</button>
                                </a>
                            </div>
                            
                            <table  cellspacing="2" class="table table-striped table-bordered rpt"  align="center">
                                <thead>
                                <tr role="row" style="font-size: 12px;font-weight: bold">
                                    <th width="200" style="white-space: nowrap;">รายการหัก/ชื่อพนักงาน</th>';
                                        foreach ($arrStaffData as $item) {
                                            $tbl .='<th width="200"  class="text-center">'.$item['FULL_NAME'].'</th>';
                                        }
                                        $tbl .='<th width="100" class="text-center">รวม</th>';
                                        $tbl .= '</tr>
                                                                        </thead>
                                                                        <tbody>';
                                        $TotalComp = [];
                                        foreach ($arrTemplate as $key => $value) {
                                            $tbl .='<tr style="font-size: 12px;">';
                                            $tbl .='<td width="200">'.$value.'</td>';
                                            $LineTotal = 0;
                                            foreach ($arrStaffData as $IDCard => $item) {
                                                //$TotalComp[$item['id']][$value['ADD_DEDUCT_TEMPLATE_ID']] = $data;
                                                $data = (isset($arrItemData[$key][$IDCard])) ? $arrItemData[$key][$IDCard] : 0 ;
                                                $LineTotal += $data;
                                                $TotalComp[$IDCard][$key] = $data;
                                                $show = ($data > 0 ) ? Helper::displayDecimal($data) : '';
                                                $tbl .='<td width="100" class="text-right">'.$show.'</td>';
                                            }
                                            $SumLine = ($LineTotal > 0) ? Helper::displayDecimal($LineTotal) : '';
                                            $tbl .='<td width="100" class="text-right">'.$SumLine.'</td>';
                                            $tbl .='</tr>';
                                        }

                                        $tbl .='</tbody>
                                                <tfoot>
                                                <tr role="row">
                                                    <th >รวม</th>';
                                    $TotalMoney = 0;
                                    foreach ($arrStaffData as $item) {
                                        $SumComp = (isset($TotalComp[$item['WAGE_EMP_ID']])) ? array_sum($TotalComp[$item['WAGE_EMP_ID']]) : 0;
                                        $show = ($SumComp > 0 ) ? Helper::displayDecimal($SumComp) : '';
                                        $TotalMoney += $SumComp;
                                        $tbl .='<th  class="text-right">'.$show.'</th>';
                                    }
                                    $tbl .='<th class="text-right">'.Helper::displayDecimal($TotalMoney).'</th>';
                                    $tbl .='</tr>
                                    </tfoot>
                             </table>
                        </div>
                    </div>';


            return $tbl;
        }
    }

    public function actionExportpdfrpt5()
    {
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDefaultFont('THSarabun');
        $mpdf->title = 'รายงานรายละเอียดส่วนหัก';
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfrpt5'));
        $mpdf->Output();
        exit;
    }

    public function actionGetreport6()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $arrIdtemp = $postValue['xtemplate'];
            $joinarrIdtemp = join(",", (array)$arrIdtemp);
            $arrCompany = $postValue['xcompany'];
            $joinarrCompany = join(",", (array)$arrCompany);
            $monthselect = $postValue['xmonth'];

            $dt = explode('-',$monthselect);
            $_dpMonth = DateTime::convertMonth($dt[0]);
            $_dpYear  = $dt[1];


            $title_report = 'รายงานรายละเอียดส่วนเพิ่มตามรายการ รอบเงินเดือน ' . $_dpMonth  . ' ' . $_dpYear ;

            $arr_wc = ApiHr::getWorking_company();
            $arr_template = ApiPayroll::getArrayAdddeductTemplate();


            $sql = "SELECT  a.ADD_DEDUCT_THIS_MONTH_EMP_ID as ID_Card  , b.WAGE_WORKING_COMPANY as company_id,
                        CONCAT(b.WAGE_FIRST_NAME,' ',b.WAGE_LAST_NAME) as full_name,
                        b.DEPARTMENT_NAME as dept_name,
                        a.ADD_DEDUCT_THIS_MONTH_TMP_ID as template_id,
                        a.ADD_DEDUCT_THIS_MONTH_TMP_NAME as template_name,
                        a.ADD_DEDUCT_THIS_MONTH_AMOUNT as amount,
                        a.ADD_DEDUCT_THIS_MONTH_DETAIL as remarks
                        FROM ADD_DEDUCT_HISTORY as a 
                        INNER JOIN WAGE_HISTORY as b ON a.ADD_DEDUCT_THIS_MONTH_EMP_ID = b.WAGE_EMP_ID
                        WHERE ADD_DEDUCT_THIS_MONTH_PAY_DATE = '$monthselect'
                        AND b.WAGE_PAY_DATE = '$monthselect'
                        AND ADD_DEDUCT_THIS_MONTH_TYPE = 1
                        AND ADD_DEDUCT_THIS_MONTH_TMP_ID IN($joinarrIdtemp)
                        AND WAGE_WORKING_COMPANY IN ($joinarrCompany)
                        ORDER BY WAGE_WORKING_COMPANY ,DEPARTMENT_NAME ASC
                        ";
            $itemsData = [];
            $modelData = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
            foreach ($modelData as $item) {
                $itemsData[$item['template_id']][$item['company_id']][$item['ID_Card']] = $item;
            }


            //store to SESSION
            $session = Yii::$app->session;
            $session->set('itemsData', $itemsData);
            $session->set('title_report', $title_report);
            $session->set('arrIdtemp', $arrIdtemp);
            $session->set('arr_wc', $arr_wc);
            $session->set('arr_template', $arr_template);
            $session->set('_dpMonth', $_dpMonth);
            $session->set('_dpYear', $_dpYear);


            $tbl = '';
            $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong> '.$title_report.'</strong></p>
                            </div>
                    </div>';

            $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfrpt6" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF</button>
                                </a>
                            </div>';

            $tbl .='<table cellspacing="2" class="table table-striped table-bordered rpt"  align="center">';
            $tbl .='<thead>';

            foreach ($arrIdtemp as $template) {

                if (isset($itemsData[$template])) {
                $tbl .= '<tr role="row" style="font-size: 14px;font-weight: bold;">';
                $tbl .= '<th colspan="5" class="text-left" style="background-color: #73C6B6;">รายงานการเพิ่ม ' . $arr_template[$template]['ADD_DEDUCT_TEMPLATE_NAME'] . ' รอบเงินเดือน ' . $_dpMonth . ' ' . $_dpYear . '</th>';
                $tbl .= '</tr>';

                    $arrData = $itemsData[$template];
                    foreach ($arrData as $company_id => $company_items ) {

                        $text_comp = $arr_wc[$company_id]['name'] . ' [ ' . $arr_wc[$company_id]['short_name'] . ' ]';

                        $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold">';
                        $tbl .= '<th colspan="5" class="text-left" style="background-color: #F8C471;">' . $text_comp . '</th>';
                        $tbl .= '</tr>';
                        $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold">';
                        $tbl .= '<th style="width: 10%" class="text-left">ลำดับ</th>';
                        $tbl .= '<th style="width: 30%" class="text-left">ชื่อ - สกุล</th>';
                        $tbl .= '<th style="width: 30%" class="text-left">แผนก</th>';
                        $tbl .= '<th style="width: 10%" class="text-left">จำนวนเงิน</th>';
                        $tbl .= '<th style="width: 20%" class="text-left">รายละเอียด</th>';
                        $tbl .= '</tr>';
                        $tbl .= '</thead>';
                        $tbl .= '<tbody>';

                        $total_deduct = 0;
                        if (isset($itemsData[$template][$company_id])) {
                            $i =1;
                            $arrDataEmp = $itemsData[$template][$company_id];
                            foreach ($arrDataEmp as $item) {
                                $total_deduct += $item['amount'];
                                $tbl .= '<tr role="row" style="font-size: 12px;">';
                                $tbl .= '<td class="text-center">' . $i . '</td>';
                                $tbl .= '<td class="text-left">'.$item['full_name'].'</td>';
                                $tbl .= '<td class="text-left">'.$item['dept_name'].'</td>';
                                $tbl .= '<td class="text-right">'.Helper::displayDecimal($item['amount']).'</td>';
                                $tbl .= '<td class="text-left">'.$item['remarks'].'</td>';
                                $tbl .= '</tr>';
                                $i++;
                            }
                        }

                        $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold;background-color: #EAEAEA;">';
                        $tbl .= '<td colspan="3" class="text-center">รวม</td>';
                        $tbl .= '<td class="text-right">'.Helper::displayDecimal($total_deduct).'</td>';
                        $tbl .= '<td class="text-left"></td>';
                        $tbl .= '</tr>';
                        $tbl .= '<tr role="row" style="background-color: #FFF">';
                        $tbl .= '<td colspan="5"></td>';
                        $tbl .= '</tr>';
                        $tbl .= '</tbody>';
                    }
                }
            }

            $tbl .='</table>';
            $tbl .= '</div></div>';


            return $tbl;

        }
    }

    public function actionExportpdfrpt6()
    {
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDefaultFont('THSarabun');
        $mpdf->title = 'รายงานรายละเอียดส่วนเพิ่มตามรายการ';
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfrpt6'));
        $mpdf->Output();
        exit;
    }

    public function actionGetreport7()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();

            $arrIdtemp = $postValue['xtemplate'];
            $joinarrIdtemp = join(",", (array)$arrIdtemp);
            $arrCompany = $postValue['xcompany'];
            $joinarrCompany = join(",", (array)$arrCompany);
            $monthselect = $postValue['xmonth'];

            $dt = explode('-',$monthselect);
            $_dpMonth = DateTime::convertMonth($dt[0]);
            $_dpYear  = $dt[1];

            $title_report = 'รายงานรายละเอียดส่วนหักตามรายการ รอบเงินเดือน ' . $_dpMonth  . ' ' . $_dpYear ;

            $arr_wc = ApiHr::getWorking_company();
            $arr_template = ApiPayroll::getArrayAdddeductTemplate();

            $sql = "SELECT  a.ADD_DEDUCT_THIS_MONTH_EMP_ID as ID_Card  , b.WAGE_WORKING_COMPANY as company_id,
                        CONCAT(b.WAGE_FIRST_NAME,' ',b.WAGE_LAST_NAME) as full_name,
                        b.DEPARTMENT_NAME as dept_name,
                        a.ADD_DEDUCT_THIS_MONTH_TMP_ID as template_id,
                        a.ADD_DEDUCT_THIS_MONTH_TMP_NAME as template_name,
                        a.ADD_DEDUCT_THIS_MONTH_AMOUNT as amount,
                        a.ADD_DEDUCT_THIS_MONTH_DETAIL as remarks
                        FROM ADD_DEDUCT_HISTORY as a 
                        INNER JOIN WAGE_HISTORY as b ON a.ADD_DEDUCT_THIS_MONTH_EMP_ID = b.WAGE_EMP_ID
                        WHERE ADD_DEDUCT_THIS_MONTH_PAY_DATE = '$monthselect'
                        AND b.WAGE_PAY_DATE = '$monthselect'
                        AND ADD_DEDUCT_THIS_MONTH_TYPE = 2
                        AND ADD_DEDUCT_THIS_MONTH_TMP_ID IN($joinarrIdtemp)
                        AND WAGE_WORKING_COMPANY IN ($joinarrCompany)
                        ORDER BY WAGE_WORKING_COMPANY,DEPARTMENT_NAME ASC
                        ";

            $itemsData = [];
            $modelData = Yii::$app->dbERP_easyhr_PAYROLL->createCommand($sql)->queryAll();
            foreach ($modelData as $item) {
                $itemsData[$item['template_id']][$item['company_id']][$item['ID_Card']] = $item;
            }

            //store to SESSION
            $session = Yii::$app->session;
            $session->set('itemsData', $itemsData);
            $session->set('title_report', $title_report);
            $session->set('arrIdtemp', $arrIdtemp);
            $session->set('arr_wc', $arr_wc);
            $session->set('arr_template', $arr_template);
            $session->set('_dpMonth', $_dpMonth);
            $session->set('_dpYear', $_dpYear);

            $tbl = '';
            $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong>'.$title_report.'</strong></p>
                            </div>
                    </div>';

            $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfrpt7" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF</button>
                                </a>
                            </div>';

            $tbl .='<table cellspacing="2" class="table table-striped table-bordered rpt"  align="center">';
            $tbl .='<thead>';

            foreach ($arrIdtemp as $template) {

                if (isset($itemsData[$template])) {
                    $tbl .= '<tr role="row" style="font-size: 14px;font-weight: bold;">';
                    $tbl .= '<th colspan="5" class="text-left" style="background-color: #73C6B6;">รายงานการหัก ' . $arr_template[$template]['ADD_DEDUCT_TEMPLATE_NAME'] . ' รอบเงินเดือน ' . $_dpMonth . ' ' . $_dpYear . '</th>';
                    $tbl .= '</tr>';

                    $arrData = $itemsData[$template];
                    foreach ($arrData as $company_id => $company_items ) {

                        $text_comp = $arr_wc[$company_id]['name'] . ' [ ' . $arr_wc[$company_id]['short_name'] . ' ]';

                        $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold">';
                        $tbl .= '<th colspan="5" class="text-left" style="background-color: #F8C471;">' . $text_comp . '</th>';
                        $tbl .= '</tr>';
                        $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold">';
                        $tbl .= '<th style="width: 10%" class="text-left">ลำดับ</th>';
                        $tbl .= '<th style="width: 30%" class="text-left">ชื่อ - สกุล</th>';
                        $tbl .= '<th style="width: 30%" class="text-left">แผนก</th>';
                        $tbl .= '<th style="width: 10%" class="text-left">จำนวนเงิน</th>';
                        $tbl .= '<th style="width: 20%" class="text-left">รายละเอียด</th>';
                        $tbl .= '</tr>';
                        $tbl .= '</thead>';
                        $tbl .= '<tbody>';

                        $total_deduct = 0;
                        if (isset($itemsData[$template][$company_id])) {
                            $i =1;
                            $arrDataEmp = $itemsData[$template][$company_id];
                            foreach ($arrDataEmp as $item) {
                                $total_deduct += $item['amount'];
                                $tbl .= '<tr role="row" style="font-size: 12px;">';
                                $tbl .= '<td class="text-center">' . $i . '</td>';
                                $tbl .= '<td class="text-left">'.$item['full_name'].'</td>';
                                $tbl .= '<td class="text-left">'.$item['dept_name'].'</td>';
                                $tbl .= '<td class="text-right">'.Helper::displayDecimal($item['amount']).'</td>';
                                $tbl .= '<td class="text-left">'.$item['remarks'].'</td>';
                                $tbl .= '</tr>';
                                $i++;
                            }
                        }

                        $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold;background-color: #EAEAEA;">';
                        $tbl .= '<td colspan="3" class="text-center">รวม</td>';
                        $tbl .= '<td class="text-right">'.Helper::displayDecimal($total_deduct).'</td>';
                        $tbl .= '<td class="text-left"></td>';
                        $tbl .= '</tr>';
                        $tbl .= '<tr role="row" style="background-color: #FFF">';
                        $tbl .= '<td colspan="5"></td>';
                        $tbl .= '</tr>';
                        $tbl .= '</tbody>';
                    }
                }
            }

            $tbl .='</table>';
            $tbl .= '</div></div>';


            return $tbl;
        }
    }

    public function actionExportpdfrpt7()
    {
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();

        $mpdf->SetDefaultFont('THSarabun');
        $mpdf->title = 'รายงานรายละเอียดส่วนหักตามรายการ';
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfrpt7'));
        $mpdf->Output();
        exit;
    }

    public function actionGetreport8()
    {
        if(Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();


            $emp_idcard = $postValue['emp_idcard'];

            $emp_data = ApiHr::getempdataInidcard($emp_idcard);
            $data_bnf = Benefitfund::find()->where([
                'BENEFIT_EMP_ID'=>$emp_idcard,
            ])->orderBy([
                'BENEFIT_ID'=>SORT_DESC,
            ])->asArray()->all();

            $display_text = 'รายงานเงินสะสมของ : '.$emp_data[0]['Fullname'];

            //store to SESSION
            $session = Yii::$app->session;
            $session->set('emp_data', $emp_data);
            $session->set('data_bnf', $data_bnf);
            $session->set('display_text', $display_text);


            $tbl = '';
            $tbl .= '<div class="row">
                            <div class="col-md-12 text-center">
                                <p style="font-size: 16pt;"><br/><strong> '.$display_text.' </strong></p>
                            </div>
                    </div>';

            $tbl .= '<div class="row">
                        <div class="col-md-12">
                            <div class="text-right" style="margin-bottom: 15px;">
                                <a href="exportpdfrpt8" target="_blank">
                                    <button class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> ออก PDF
                                    </button>
                                </a>
                            </div>
                            <p style="font-size: 14px;font-weight: bold;"></p>
                            <table  cellspacing="2" class="table table-striped table-bordered rpt" width="80%" align="center">
                                <thead>
                                <tr role="row">
                                    <th aria-sort="ascending">ลำดับ</th>
                                    <th aria-sort="ascending" class="text-center">เงินเดือน</th>
                                    <th aria-sort="ascending" class="text-center">รอบเงินเดือน</th>
                                    <th aria-sort="ascending" class="text-center">เงินสะสม</th>
                                    <th aria-sort="ascending" class="text-center">เงินสะสมรวมทั้งหมด</th>
                                </tr>
                                </thead>
                                <tbody>';

                            $rec = 1;
                            foreach ($data_bnf as $value) {
                                $tbl .='<tr>
                                        <td width="15%" align="right">'.$rec.'</td>
                                        <td width="30%">'.$value['BENEFIT_DETAIL'].'</td>
                                        <td width="15"  class="text-center">'.$value['BENEFIT_SAVING_DATE'] .'</td>
                                        <td width="15%" align="right">'.Helper::displayDecimal($value['BENEFIT_AMOUNT']) .'</td>
                                        <td width="15%" align="right">'.Helper::displayDecimal($value['BENEFIT_TOTAL_AMOUNT']).'</td>
                                    </tr>';
                                $rec++;
                                }
                        $tbl .='</tbody>
                            </table>
                        </div>
                    </div>';


            return $tbl;
        }
    }

    public function actionExportpdfrpt8()
    {
        $_footer = "พิมพ์จากโปรแกรม ".Yii::$app->params['PROGRAM_NAME']." แผ่นที่ {PAGENO} วันที่ ".DateTime::ThaiDateTime(DateTime::getTodayDateTime());
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDefaultFont('THSarabun');
        $mpdf->title = 'รายงานรายเงินสะสมพนักงาน';
        $mpdf->SetFooter($_footer);
        $mpdf->WriteHTML($this->renderPartial('_exportpdfrpt8'));
        $mpdf->Output();
        exit;
    }

}
