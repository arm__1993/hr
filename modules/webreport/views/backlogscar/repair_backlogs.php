<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 20/10/2559
 * Time: 11:16 6.1
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;


HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);


$script = <<< JS
$(document).ready(function() {
    
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
    
    
  });

JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->

<h3><i class="fa fa-fw fa-file-text"></i>รายงานรถค้างซ่อมสะสมประจำวัน</h3>

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <!--<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>-->
    </div>
    <!-- ./box-header -->

    <div class="box-body">
        <div class="row">
            <form method="post" action="receive_backlogs">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key=>$value) {
                                    $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>เลือกประเภทรถ</label>
                        &nbsp;
                            <div class="btn-group">
                            <input type="radio" name="technician_type" value="1" <?php echo ($selected_technician !=2) ? 'checked' : '';?>> รถเล็ก
                            &nbsp;
                            <input type="radio" name="technician_type" value="2" <?php echo ($selected_technician==2) ? 'checked' : '';?>> รถใหญ่
                            </div>
                        &nbsp;&nbsp;
                        </center>
                        <center><br>
                        <div class="btn-group">
                        <button type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/backlogscar/btn_reportbacklogs">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/backlogscar/expoldexcelbacklog">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-file-excel-o"></i> ออกรายงาน Excel
                        </button>
                        </div>
                    </center>
                    </div>
                </form>
            </div>
        </div>
        <!-- ./box-body -->



    <?php if($query) { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title">รายงานรถค้างซ่อมสะสมประจำวันที่ &nbsp; <?php echo date('d/m/Y');?></h3>
                    <!--<div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>-->
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row" class="odd"> 
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ลำดับที่
                                        </th>                 
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขที่ใบซ่อม
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันที่เข้ารับบริการ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รุ่นรถ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ทะเบียน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันที่ปิดงานโดย SA
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ประเภทงานซ่อม
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ช่างซ่อม
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        SAรับแจ้งซ่อม
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันนัดส่งมอบรถ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        สถานะที่ค้าง
                                        </th>
                                         <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        จำนวนวันค้างซ่อมสะสม/วัน
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody> 
                                    <?php
                                    $item = 1;
                                    foreach ($NameAndTime as $value) {
                                    ?>
                                    <tr role="row" class="odd">
                                        <td><?php print_r($item); ?></td>  
                                        <td>
                                        <?php echo ($value['id_repair']) ? $value['id_repair'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['guard_time']) ? DateTime::CalendarDate($value['guard_time']) : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['fullmodel_name']) ? $value['fullmodel_name'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['register']) ? $value['register'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['end_time']) ? DateTime::CalendarDate($value['end_time']) : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['technician_name']) ? $value['technician_name'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['name_sa']) ? $value['name_sa'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['app_time']) ? DateTime::CalendarDate(date("Y-m-d",$value['app_time'])) : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($checkStatus) ? $checkStatus : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo (int)(($dateNOW - $value['app_time'])/86400); ?>
                                        </td>
                                    </tr>                                    
                                    <?php $item++;} ?>                              
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php } ?>

   
</div>
<!-- /.box-->


<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>