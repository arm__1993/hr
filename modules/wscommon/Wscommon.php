<?php

namespace app\modules\wscommon;

/**
 * wscommon module definition class
 */
class Wscommon extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\wscommon\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
