<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "position".
 *
 * @property integer $id
 * @property string $PositionCode
 * @property string $WorkCompany
 * @property string $Department
 * @property string $Section
 * @property string $WorkType
 * @property string $Level
 * @property string $runNumber
 * @property string $Name
 * @property string $Note
 * @property string $addToCommand
 * @property integer $Status
 * @property string $position_Edit_date
 * @property integer $recruit_status
 */
class Position extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PositionCode', 'recruit_status', 'WorkCompany', 'Department', 'Section', 'WorkType', 'Level', 'runNumber', 'Name', 'Status', 'position_Edit_date'], 'required'],
            [['Note'], 'string'],
            [['Status', 'recruit_status'], 'integer'],
            [['position_Edit_date'], 'safe'],
            [['PositionCode'], 'string', 'max' => 20],
            [['WorkCompany', 'Department', 'Section', 'WorkType'], 'string', 'max' => 50],
            [['Level'], 'string', 'max' => 2],
            [['runNumber'], 'string', 'max' => 4],
            [['Name'], 'string', 'max' => 250],
            [['addToCommand'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'PositionCode' => 'Position Code',
            'WorkCompany' => 'Work Company',
            'Department' => 'Department',
            'Section' => 'Section',
            'WorkType' => 'Work Type',
            'Level' => 'Level',
            'runNumber' => 'Run Number',
            'Name' => 'Name',
            'Note' => 'Note',
            'addToCommand' => 'Add To Command',
            'Status' => 'Status',
            'position_Edit_date' => 'Position  Edit Date',
            'recruit_status' => 'recruit status'
        ];
    }

    /**
     * @inheritdoc
     * @return PositionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PositionQuery(get_called_class());
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'sectionname',
            'departmentname',
            'companyname',
            'sectionid',
            'STATUS1',
            'STATUS_2',
            'STATUS_99',
            'pass',
            'notpass',
            'STATUS_19',
            'COUNTposition'
        ]);
    }

    public static function getTotal($provider, $fieldName)
    {
        $total = 0;

        foreach ($provider as $item) {
            $total += $item[$fieldName];
        }

        return $total;
    }
}
