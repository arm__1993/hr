<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/28/2017 AD
 * Time: 14:35
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.config.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/deptdeductlist.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);




//$this->registerCssFile(Yii::$app->request->BaseUrl."/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->BaseUrl."/fonts/01thaifontcss.css");



?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>รายการหักจากหน่วยงาน</li>
                <li class="active">รายการหักทั้งหมด</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
              <form class="form-horizontal">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ข้อมูลรายการ</h3>
                    </div>
                        <div class="box-body">
                            <div class="row">
                                    <div class="col-md-1">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="monthselect"  class="col-sm-4 control-label">รอบเงินเดือน</label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control" id="monthselect" value="<?php echo date('m-Y');?>">
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="add_deduct_template_id"  class="col-sm-4 control-label">ประเภทรายการ</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="add_deduct_template_id"
                                                        id="add_deduct_template_id">
                                                    <option value="0">ทั้งหมด</option>
                                                    <?php
                                                    $arrList = ApiPayroll::getDataDeductTemp();
                                                    foreach ($arrList as $key => $value) {
                                                        echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn  btn-primary btn-sm" id="seachdatalist"><i class="fa fa-search"></i> ค้นหา</button>
                                        <!--<button type="reset" class="btn  btn-danger btn-sm"><i class="fa fa-reply"></i> ล้างข้อมูล</button>-->
                                    </div>
                                </div>
                            </div>

                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">แสดงรายการ</h3>
                                </div>
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover dataTable" >
                                            <thead>
                                                 <tr>
                                                    <th width="5%"><center>ลำดับ</center></th>
                                                    <th width="20%"><center>ชื่อ-สกุล</center></th>
                                                    <th width="20%"><center>ชื่อรายการ</center></th>
                                                    <th width="20%"><center>จำนวน</center></th>
                                                    <th width="20%"><center>รายละเอียด</center></th>
                                                    <th width="15%"><center>จัดการ</center></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tblticket">
                                               
                                            </tbody>
                                        </table>
                                    </div>
                            </div>   
                                        
                        </div>
                    </div>
                </div>
            </form>   
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->