<?php
use yii\helpers\ArrayHelper;

//Yii::$app->request->BaseUrl;
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/login/checklogin.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$css = <<< CSS
.login-box
{
   font-size: 1em !important;
}
.erp_head{
    color:#ff5500;
    font-weight: bold;
}
CSS;


$this->registerCSS($css);

?>
<div class="login-box">
    <input type="hidden" id="msg" value="<?php echo  $msg; ?>">
    <div class="login-box-body">
        <div class="login-logo logo_text">

            <!--<img src="/images/logo.png"  alt="Logo">-->
            Easy<span class="erp_head">ERP</span><br/>
            <small>Hornbill Group</small>
        </div><!-- /.login-logo -->
        <p class="login-box-msg">Sign in with your organization account</p>
        <form action="authenticate?func=submit"  method="post"  onsubmit="return ValidationEvent()">
            <div class="form-group has-feedback">
                <input type="text" id="myusername" name="myusername" autocomplete="false"   onblur="eventUsername(this.value)" class="form-control" placeholder="Username">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" id="mypassword" name="mypassword" autocomplete="false"  placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label>Position Code</label>
                <select class="form-control" id="postID" name="s_position_code">
                    <option> Select Position</option>
                    <!--<option>Developer</option>-->
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </select>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <a href="#">Privacy</a> | <a href="#">Terms</a> | <a href="#">About</a>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <input type="hidden" name="_csrf" value="<?php echo Yii::$app->request->getCsrfToken();?>" />
                    <button type="submit"  class="btn btn-success btn-block btn-flat">Sign In</button>
                </div><!-- /.col -->
            </div>
        </form>
        <?php if(Yii::$app->session->hasFlash('loginfail')):?>
            <br/><br/><br/>
            <?php echo  \yii\bootstrap\Alert::widget([
                'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('loginfail'), 'body'),
                'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('loginfail'), 'options'),
            ]); ?>
        <?php endif; ?>
        <!--
        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div><!-- /.social-auth-links -->
        <!--
        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>
        -->
        <hr/>
        <?php if($msg!='' || $msg!=null){ ?>
        <ul>
            <li><font color='red'><?php echo $msg; ?></font></li>
        </ul>
        <?php } ?>
        <p>Browser Support</p>
        <p>
        
        <ul class="order">
            <li>Google Chrome, Mozilla Firefox, Apple Safari, Internet Explorer 10+</li>
        </ul>
        </p>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
