<?php
use yii\helpers\Html;
use app\api\DateTime;
use app\api\Utility;
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
 $item = $data;
 $datacompany = $company;
?>
<!--<meta charset="UTF-8">-->

  <table>
    <tr>
      <td>
        <img height="84" width="84" src="<?php echo $imghr; ?>/sps.png" class="img-circle">
      </td>
      <td>
      </td>
    </tr>
    
  </table>
  <table class="table_bordered" width="100%" height="100%" border="0" cellpadding="2" cellspacing="0">
    <tr >
        <td width="60%" colspan="2">
            <table width="100%">
                <tr>
                    <td>
                        ชื่อสถานประกอบการ
                    </td>
                    <td>
                        <?php echo $item[0]['COMPANY_NAME'] ?>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        ที่อยู่สถานประกอบการ
                    </td>
                    <td>
                        <?php echo $datacompany[0]['address'] ?>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        เบอร์โทรศัพท์
                    </td>
                    <td>
                        <?php echo $datacompany[0]['Tel'] ?>
                    </td>
                    </tr>
            </table>
        </td>
          <td colspan="2" width="40%">
            <table width="100%">
                <tr>
                    <td width="15%">
                    
                    </td>
                    <td width="85%">
                        <table width="100%">
                            <tr>
                                <td width="50%">
                                    เลขที่บัญชี
                                </td>
                                <td width="50%">
                                    <?php echo $datacompany[0]['soc_acc_number'] ?>		
                                </td>
                            </tr>
                            <tr>
                                <td width="60%">
                                    สาขา
                                </td>
                                <td width="40%">
                                    <?php echo '0000'.$item[0]['BRANCH_NO'] ?>			
                                </td>
                            </tr>
                            <tr>
                                <td width="60%">
                                    อัตราเงินสมทบร้อยละ	
                                </td>
                                <td width="40%">
                                      <?php echo substr($item[0]['RATE'],0,-2).'.'.substr($item[0]['RATE'],-2); ?>				
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr >
      <td margin-top='0' colspan="2" width="55%">
            <table  width="100%"  border="0" > 
                <tr>
                    <td colspan="4">
                        <table width="100%">
                             <tr>
                                <td width="40%">
                                    การนำส่งเงินสมทบสำหรับค่าจ้างเดือน		
                                </td>
                                <td width="20%">
                                    <?php echo DateTime::mappingMonth($item[0]['MONTHS']); ?>
                                </td>
                                <td width="20%">
                                    พ.ศ.
                                </td>
                                <td width="20%"> 
                                    <?php echo $item[0]['YEARS']+543 ?>		
                                </td>
                            </tr>
                        </table>		
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        รายการ
                    </td>
                    <td colspan="2">
                        จำนวนเงิน		
                    </td>
                </tr>
                <tr>
                    <td width="15%">
                        1.		
                    </td>
                    <td width="50%">
                        เงินค่าจ้างทั้งสิ้น
                    </td>
                    <td width="20%">
                          <?php echo number_format((int)(substr($item[0]['TOTAL_WAGES'],0,-2)).'.'.substr($item[0]['TOTAL_WAGES'],-2),2); ?>
                    </td>
                    <td width="15%">
                        บาท		
                    </td>
                </tr>
                <tr>
                    <td width="15%">
                        2.		
                    </td>
                    <td width="50%">
                        เงินสมทบผู้ประกันตน
                    </td>
                    <td width="20%">
                           <?php echo number_format((int)substr($item[0]['TOTAL_PAID_BY_EMPLOYEE'],0,-2).'.'.substr($item[0]['TOTAL_PAID_BY_EMPLOYEE'],-2),2); ?>
                    </td>
                    <td width="15%">
                        บาท		
                    </td>
                </tr>
                <tr>
                    <td width="15%">
                        3.		
                    </td>
                    <td width="50%">
                        เงินสมทบนายจ้าง
                    </td>
                    <td width="20%">
                           <?php echo number_format((int)substr($item[0]['TOTAL_PAID_BY_EMPLOYER'],0,-2).'.'.substr($item[0]['TOTAL_PAID_BY_EMPLOYER'],-2),2); ?>
                    </td>
                    <td width="15%">
                        บาท		
                    </td>
                </tr>
                <tr>
                    <td width="15%">
                        4.		
                    </td>
                    <td width="50%">
                        รวมเงินสมทบที่นำส่งทั้งสิ้น
                    </td>
                    <td width="20%">
                           <?php echo number_format((int)substr($item[0]['TOTAL_PAID'],0,-2).'.'.substr($item[0]['TOTAL_PAID'],-2),2); ?>
                    </td>
                    <td width="15%">
                        บาท		
                    </td>
                </tr>
                <tr>
                    <td >
                        
                    </td>
                    <td colspan="3">
                         <center><?php 
                            $number = number_format((int)substr($item[0]['TOTAL_PAID'],0,-2).'.'.substr($item[0]['TOTAL_PAID'],-2),2);
                            echo $a = Utility::convnum2str((float)$number)  ?></center>								
                    </td>
                </tr>
                <tr>
                    <td width="15%">
                        5.		
                    </td>
                    <td width="50%">
                        จำนวนผู้ประกันตนที่ส่งเงินสมทบ			
                    </td>
                    <td width="20%">
                           <?php echo (int)$item[0]['TOTAL_EMPLOYEE'] ?>	
                    </td>
                    <td width="15%">
                        คน		
                    </td>
                </tr>
            </table>
            <table  width="100%" border="0" > 
                <tr>
                    <td colspan="4">
                        ข้าพเจ้าขอรับรองว่ารายการที่แจ้งเป็นรายการที่ถูกต้องครบถ้วนและเป็นจริงทุกประการ								
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                    	พร้อมนี้ได้แนบ
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                       <img height="12" width="12" src="<?php echo $imghr; ?>/checkbox.png" class="img-circle"> รายละเอียดการนำส่งเงินสมทบ<br>	
                    </td>
                    <td width="15%">
                        จำนวน
                    </td>
                    <td width="20%">
                         1
                    </td>
                    <td width="15%">
                        แผ่น		
                    </td>
                    <td width="15%">
                        หรือ		
                    </td>
                </tr>
               <tr>
                    <td width="50%">
                       <img height="12" width="12" src="<?php echo $imghr; ?>/checkbox.png" class="img-circle"> แผ่นจานแม่เหล็ก<br>	
                    </td>
                    <td width="15%">
                        จำนวน
                    </td>
                    <td width="20%">
                         1
                    </td>
                    <td width="15%">
                        แผ่น		
                    </td>
                    <td width="15%">
                        		
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table >
                            <tr>
                                <td width="25%">
                                
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td >
                                                <center>ลงชื่อ ..............................................นายจ้าง/ผู้รับมอบอำนาจ	 </center>						
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <center>(นางสาวสุปรียา เจริญอาภรณ์)</center>														
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                 <center>ยื่นแบบวันที่ ....... เดือน ................... พ.ศ. ....... </center>																					
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
            </table>
      </td>
      <td colspan="2" width="45%">
        <table width="100%">
            <tr>
                <td width="15%">
                
                </td>
                <td width="85%">
                    <table width="100%">
                         
                        <tr>
                            <td>
                                <center>สำหรับเจ้าหน้าที่ประกันสังคม</center>				
                            </td>
                        </tr>
                        <tr>
                            <td >
                                ชำระเงินวันที่  ................................................			
                            </td>
                        </tr>
                        <tr>
                            <td >
                                เงินเพิ่ม(ถ้ามี)  ...............................................
                            </td>
                        </tr>
                        <tr>
                            <td >
                                ใบเสร็จรับเงินเลขที่  .......................................
                            </td>
                        </tr>
                        <tr>
                            <td >
                            </td>
                        </tr>
                        <tr>
                            <td >
                                ลงชื่อ  .............................................................				
                            </td>
                        </tr>
                        <tr>
                            <td >
                               <center>( ........................................... )</center>							
                            </td>
                        </tr>
                         <tr>
                            <td>
                               <center>สำหรับเจ้าหน้าที่ธนาคาร</center>				
                            </td>
                        </tr>
                        <tr>
                            <td >
                                ชำระเงินวันที่  ................................................			
                            </td>
                        </tr>
                        <tr>
                            <td >
                                เงินเพิ่ม(ถ้ามี)  ...............................................
                            </td>
                        </tr>
                        <tr>
                            <td >
                                ใบเสร็จรับเงินเลขที่  .......................................
                            </td>
                        </tr>
                        <tr>
                            <td >
                            </td>
                        </tr>
                        <tr>
                            <td >
                                ลงชื่อ  .............................................................				
                            </td>
                        </tr>
                        <tr>
                            <td >
                               <center>( ........................................... )</center>						
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
      </td>
    </tr>
    
  </table>