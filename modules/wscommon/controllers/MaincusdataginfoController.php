<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Maincusdataginfo;
use app\modules\wscommon\models\Mainaddress;
use yii\helpers\ArrayHelper;


class MaincusdataginfoController extends ActiveController
{


   	public $modelClass = 'app\modules\wscommon\models\Maincusdataginfo';
	//public $modelClass = 'app\modules\wscommon\models\Testmaincus';

	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}

	public function actionListcustomer()
	{
		$showdatalimit10 = Maincusdataginfo::find()->limit(5)->all();
    	return $showdatalimit10;
	}

    public function actionInsertcustomerdataginfo()
    {
        ///ข้อมูลทั่วไป///
        $Be = $_POST['Be'];
        $BeType = $_POST['BeType'];
        $Cus_Name = $_POST['Cus_Name'];
        $Cus_Surename = $_POST['Cus_Surename'];
        $Cus_Nickname = $_POST['Cus_Nickname'];
        $Sex = $_POST['Sex'];
        $DateOfBirth = $_POST['DateOfBirth'];
        $Cus_IDNo_Type = $_POST['Cus_IDNo_Type'];
        $Cus_IDNo = $_POST['Cus_IDNo'];
        $Married_Status = $_POST['Married_Status'];
        $Date_Receive = $_POST['Date_Receive'];
        $Data_Received = $_POST['Data_Received'];
        $Data_Received_Num = $_POST['Data_Received_Num'];
        $stSourceData = $_POST['1stSourceData'];
        $Remark = $_POST['Remark'];
        $Updater = $_POST['Updater'];
        $UpdaterNO = $_POST['UpdaterNO'];
        $UpdateT = $_POST['UpdateT'];
        $company_type = $_POST['company_type'];
        $cus_branch_id = $_POST['cus_branch_id'];
        $cus_branch_txt = $_POST['cus_branch_txt'];
         ///ข้อมูลที่อยู่///



        $datanew = new Maincusdataginfo();
        $LastidInsert = $datanew->insertmaincusdataginfo($Be,$BeType,$Cus_Name,$Cus_Surename,$Cus_Nickname,$Sex,$DateOfBirth,$Cus_IDNo_Type,$Cus_IDNo,$Married_Status,$Date_Receive,$Data_Received,$Data_Received_Num,$stSourceData,$Remark,$Updater,$UpdaterNO,$UpdateT,$company_type,$cus_branch_id,$cus_branch_txt);
        

        // $dataInsertMainaddress = new Mainaddress();
        // $Inserreation = $dataInsertMainaddress
        //               ->insertmainaddress($LastidInsert);

        echo "LastidInsert=".$LastidInsert;
        //return $Lastid;
    }
	public function actionSearchcustomerdataforsalemobile()
    {	

        $c = \Yii::$app->request->get('c');
        $s = \Yii::$app->request->get('s');
        $i = \Yii::$app->request->get('i');
        $customerdata = new Maincusdataginfo();
        $customerdataSearch = $customerdata->searchcustomerdataforsalemobile($c,$s,$i);
        return $customerdataSearch;
    }
    public function actionSearchcustomerdataforshoweditmobile()
    {

        $Cusno = $_GET['Cusno'];
       // return $Cusno;
        // $Cusno = \Yii::$app->request->get('Cusno');
       
        $customerdata = new Maincusdataginfo();
        $customerdataSearch = $customerdata->searchcustomerdataforshoweditmobile($Cusno);
        return $customerdataSearch;
    }


}
