<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_promise".
 *
 * @property integer $id
 * @property integer $emp_promise_type_id
 * @property integer $emp_data_id
 * @property string $emp_idcard
 * @property string $promise_code
 * @property string $promise_name
 * @property string $promise_detail
 * @property string $promise_bond
 * @property string $promise_attach
 * @property integer $promise_sign_status
 * @property string $promise_sign_date
 * @property string $create_date
 */
class EmpPromise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_promise';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emp_promise_type_id', 'emp_data_id', 'promise_sign_status'], 'integer'],
            [['promise_detail', 'promise_bond', 'promise_attach'], 'string'],
            [['promise_sign_date', 'create_date'], 'safe'],
            [['emp_idcard'], 'string', 'max' => 13],
            [['promise_code'], 'string', 'max' => 20],
            [['promise_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_promise_type_id' => 'Emp Promise Type ID',
            'emp_data_id' => 'Emp Data ID',
            'emp_idcard' => 'Emp Idcard',
            'promise_code' => 'Promise Code',
            'promise_name' => 'Promise Name',
            'promise_detail' => 'Promise Detail',
            'promise_bond' => 'Promise Bond',
            'promise_attach' => 'Promise Attach',
            'promise_sign_status' => 'Promise Sign Status',
            'promise_sign_date' => 'Promise Sign Date',
            'create_date' => 'Create Date',
        ];
    }
}
