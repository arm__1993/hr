<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Maineducation;
use yii\helpers\ArrayHelper;


class MaineducationController extends ActiveController
{
    public $modelClass = 'app\modules\wscommon\models\Maineducation';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}


 
    public function actionSearchmaineducationcusformobile()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $dataeducus = new Maineducation();
        $showdataeducus = $dataeducus->searchmaineducationcusformobile($cusno);
        return $showdataeducus;
    }


}
