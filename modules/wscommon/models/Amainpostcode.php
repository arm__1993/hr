<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "aMain_postcode".
 *
 * @property integer $id
 * @property integer $PostCode
 * @property integer $amphur_id
 * @property string $Amphur
 * @property integer $province_id
 * @property string $Province
 * @property string $Note
 * @property string $PostOffice
 */
class Amainpostcode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aMain_postcode';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PostCode', 'amphur_id', 'province_id'], 'integer'],
            [['amphur_id', 'province_id'], 'required'],
            [['Note', 'PostOffice'], 'string'],
            [['Amphur', 'Province'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'PostCode' => 'Post Code',
            'amphur_id' => 'Amphur ID',
            'Amphur' => 'Amphur',
            'province_id' => 'Province ID',
            'Province' => 'Province',
            'Note' => 'Note',
            'PostOffice' => 'Post Office',
        ];
    }

    public function searchpostcode($amphurid,$provinceid) // param
    {
        // $provinceid = "%".$provinceid."%";
        // $amphurid = "%".$amphurid."%";

        $postcode = Yii::$app->dbERP_maincusdata
       ->createCommand("select * from aMain_postcode where amphur_id = :amphurid AND province_id = :provinceid")
       ->bindParam(':amphurid',$amphurid)
       ->bindParam(':provinceid',$provinceid)->queryAll();
        return $postcode;

    }
}
