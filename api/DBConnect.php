<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 5/23/2017 AD
 * Time: 10:28
 */

namespace app\api;

use Yii;

class DBConnect
{

    public static function getDBConn()
    {
        return [
            'ou' => Yii::$app->params['dbConn']['ERP_easyhr_OU'],
            'ct' => Yii::$app->params['dbConn']['ERP_easyhr_checktime'],
            'pl' => Yii::$app->params['dbConn']['ERP_easyhr_PAYROLL'],
            'td' => Yii::$app->params['dbConn']['dbERP_easyhr_TIME_ATTENDANCE'],
        ];
    }
}