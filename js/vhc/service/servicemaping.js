$(document).ready(function() {
    $('#savebtn').click(function() {
        var obj = {};
        var arrparts = [];
        var model = $('#selectmodel').val().split('_');

        function data() {
            obj.model = model[0];
            obj.modelgroup = model[1];
            obj.vhcmodel = $('#selectvhcmodel').val();
            obj.crepair = $('.crepair-item.active').data('obj-crepair');
            obj.crepair_list = $('.clist-item.active').data('obj-crelist');
            obj.labor = $('.list-group-item.laber.active').data('obj-labor');
            $.each($('.parts.active').toArray(), function(i, v) {
                var parts_id = v.id.split("-");
                var objdata = $('#' + v.id).data('obj-parts');
                arrparts[parts_id[1]] = objdata;
                obj.parts = arrparts.filter((n) => n != undefined || n != '' || n != null)

            });
            return obj;
        }
        $.when(
            data()
        ).then(function(res) {
            var data = JSON.stringify(res);
            $.post('mapdata', { data: data }, function(respond) {
                if (respond == 1) {
                    showSaveSuccess();
                } else {
                    showSaveError();
                }
            });
        });

    });
    seartpart();
    LodeModel();
    LoadVHCModel();
    $('#select').click(function() {
        alert("Handler for .click() called.");
        getchildren();
    });
    $('#unselect').click(function() {
        alert("Handler for .click() called.");
        unselect();
    });
    $('.btnOk').click(function() {
        alert("Handler for .click() btnOk.");
    });
});
var objpart = [];
var objpartselect = [];

function seartpart() {
    $.get('seartpart', {}, function(res) {
        console.log(res);
        $('#seartpart').html(res).promise().done(function() {
            $('#dvloadot').addClass('hidden');
            $('#seartpart').attr("multiple", "multiple");
            $('#seartpart').SumoSelect({ selectAll: true, search: true, okCancelInMulti: true, triggerChangeCombined: true });
        });
    });
}

function toggleProgramListActive(li, pId, noValid) {

    $('.program-lists-ul li').removeClass('active');
    $(li).addClass('active');

    $('#manage_menu_title').html($(li).find('.list-menu-name').text());

    $('#pre-load').hide();
    $('#manage-data-content').show();

    if (noValid == undefined) {
        //check save menu
        var isEmty = $('#tree').data('obj-take');
        if (isEmty != undefined && isEmty.length > 0) {
            //alert('กรุณาบันทึกเมนู');

        }
    }
}

function crepairList(li, pId, noValid) {
    $('.crepair-lists-ul li').removeClass('active');
    $(li).addClass('active');
    $('#selectpart').html('');
    $('#manage_menu_title').html($(li).find('.list-menu-name').text());
    $('#pre-load').hide();
    $('#manage-data-content').show();

    $.get('crepairlist', { data: pId }, function(res) {
        $('#testww').html(res);
        $('#form-move-crepairlist').addClass('hidden');
        $('#testleber').html('');
        $('#partstest').html('');
        $('#form-move-laber').removeClass('hidden');
        $('#form-move-part').removeClass('hidden');
    });
}

function LoadLaber(li, pId, parantId) {
    $('li.list-group-item.clist').removeClass('active');
    $(li).addClass('active');
    $('#selectpart').html('');
    $('#manage_menu_title').html($(li).find('.list-menu-name').text());

    $('#pre-load').hide();
    $('#manage-data-content').show();
    $.get('laber', { data: { crepairId: pId, crepair_list_id: parantId } }, function(res) {
        $('#partstest').html('');
        $('#form-move-laber').addClass('hidden');
        $('#form-move-part').removeClass('hidden');
        $('#testleber').html(res);
    });
}

function LoadParts(li, cre, cre_list, wage) {
    $('li.list-group-item.laber').removeClass('active');
    $(li).addClass('active');

    $('#manage_menu_title').html($(li).find('.list-menu-name').text());
    $('#selectpart').html('');
    $('#pre-load').hide();
    $('#manage-data-content').show();
    /*$.get('parts', { data: { crepairId: cre, crepair_list_id: cre_list, wage: wage } }, function(res) {
        $('#partstest').html(res);
    });*/
    $.get('parts', { data: { crepairId: 1, crepair_list_id: 1, wage: 10 } }, function(res) {
        $('#partstest').html(res);
        $('#form-move-part').addClass('hidden');
        $('#divseartpart').removeClass('hidden');

    });

};
!(function($) {
    $.fn.classes = function(callback) {
        var classes = [];
        $.each(this, function(i, v) {
            var splitClassName = v.className.split(/\s+/);
            for (var j = 0; j < splitClassName.length; j++) {
                var className = splitClassName[j];
                if (-1 === classes.indexOf(className)) {
                    classes.push(className);
                }
            }
        });
        if ('function' === typeof callback) {
            for (var i in classes) {
                callback(classes[i]);
            }
        }
        return classes;
    };
})(jQuery);

function LodeModel() {
    var dropdrow = '<option value=""> กรุณาเลือก </option>';
    $.get('selectmodel', function(res) {
        console.log(res);
        $.each(res, function(index, value) {
            dropdrow += '<option value="' + value.id + '_' + value.model + '"> ' + value.Abbreviation + ' </option>';
        });
        $('#selectmodel').html(dropdrow).promise().done(function(res) {
            $('#selectmodel').SumoSelect({ search: true });
            $('#selectmodel')[0].sumo.reload();
        });
    });

}

function LoadVHCModel() {
    var dropdrow = '<option value=""> กรุณาเลือก </option>';
    $.get('selectvhcmodel', function(res) {
        $.each(res, function(index, value) {
            dropdrow += '<option value="' + value.id + '"> ' + value.model_name + ' </option>';
        });
        $('#selectvhcmodel').html(dropdrow).promise().done(function(res) {
            $('#selectvhcmodel').SumoSelect({ search: true });
            $('#selectvhcmodel')[0].sumo.reload();
        });
    });
}



function SelectParts(li) {

    var classes = li.getAttribute('class');
    var classList = classes.split(" ");
    if (classList[classList.length - 1] == 'active') {
        $(li).removeClass('active');
    } else {
        $(li).addClass('active');
    }
    var data = jQuery.parseJSON($('#' + li.id).attr('data-obj-plan'));
    if (data != undefined) {
        this.objpart[data.pw_id] = $('#' + li.id).attr('data-obj-plan');
    }
}

function getchildren() {

    var arrayfilter = objpart.filter(function(v) { return v !== '' });
    var objpartstr = JSON.stringify(arrayfilter);
    $.each(arrayfilter, function(index, value) {
        var decode = JSON.parse(value);
        $('#part-' + decode.pw_id).remove();
    });
    $.post('selectpart', { data: objpartstr }, function(res) {
        $('#selectpart').html(res);
        $('#form-move-partselect').addClass('hidden');
    });
}

function SelectPartsselect(li) {
    var classes = li.getAttribute('class');
    var classList = classes.split(" ");
    if (classList[classList.length - 1] == 'active') {
        $(li).removeClass('active');
    } else {
        $(li).addClass('active');
    }
};
Array.prototype.remove = function(el) {
    return this.splice(this.indexOf(el), 1);
}

function unselect() {
    var html = '';
    var li = $('li.partsselect.active').toArray();
    $.each(li, function(index, value) {
        $(li).removeClass('partsselect');
        $(li).removeClass('active');
        $(li).addClass('parts');
        $(li).attr('onclick', 'SelectParts(this)')
        $('#partstest').append(value).promise().done(function(res) {
            console.log($('#partstest').find('li').toArray());
            // objpart.remove(value.pw_id);
            // console.log(objpart);
        });
    })
}