<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "relation_position".
 *
 * @property integer $id
 * @property integer $position_id
 * @property string $id_card
 * @property string $NameUse
 * @property string $Note
 * @property integer $status
 * @property string $relation_position_Start_date
 */
class RelationPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relation_position';
    }

    public static function primarykey()
    {
        return ['id'];
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'position_id', 'id_card', 'NameUse'], 'required'],
            [['id', 'position_id', 'status'], 'integer'],
            [['Note'], 'string'],
            [['relation_position_Start_date'], 'safe'],
            [['id_card'], 'string', 'max' => 14],
            [['NameUse'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_id' => 'Position ID',
            'id_card' => 'Id Card',
            'NameUse' => 'Name Use',
            'Note' => 'Note',
            'status' => 'Status',
            'relation_position_Start_date' => 'Relation Position  Start Date',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(),[
            'Position_id',
            'Position_PositionCode',
            'Position_WorkCompany',
            'Position_Department',
            'Position_Section',
            'Position_WorkType',
            'Position_Level',
            'Position_runNumber',
            'Position_Name',
            'Position_Note',
            'Position_addToCommand',
            'Position_Status',
            'Position_position_Edit_date',
            'Position_typepersonnel',
            'Position_typeapprentice',
            'Position_typeAll',
            'Position_typeFree',
            'companyName',
            'departmentName',
            'sectionName',
            'Position_typecloseposition',
            'Position_typeclose'
        ]);
    }
}
