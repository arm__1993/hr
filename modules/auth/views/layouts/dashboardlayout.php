<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


/*** DEFAULT ROUTING **/
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$img = Yii::$app->request->BaseUrl . '/images/global/';
$CurrActionID = Yii::$app->controller->action->id;



//Home logo Links
$currentController = Yii::$app->controller->id;
$homeLinks = 'index';
if ($currentController != 'default') $homeLinks = '../default/index';


$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;
define('SITE_URL', $siteURL);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout, #btnSignOut').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='../default/logout';
                }
            }
          });
    });
    
    $('#linklogout,  #btnSignOut').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='../default/logout';
                }
            }
          });
    });
});
JS;


$this->registerCssFile(Yii::$app->request->BaseUrl . "/fonts/01thaifontcss.css");
$this->registerJs($script);

$idcard =  Yii::$app->session->get('idcard');
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
        <?php echo Html::csrfMetaTags() ?>
        <title>บริษัท อีซูซุเชียงราย จำกัด</title>
        <?php $this->head() ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->homeUrl; ?>/css/hr/hr.css">
    </head>


    <body class="hold-transition skin-green-light layout-top-nav">
    <?php $this->beginBody() ?>
    <!-- Site wrapper -->

    <header class="main-header">
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <div class="navbar-header">
                <a href="<?php echo $homeLinks; ?>" class="navbar-brand"><span
                            class="logo_text_white"><b>EasyERP</b></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">


                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo Yii::$app->request->BaseUrl . '/upload/emp_img/Pictures_HyperL/' . $idcard . '.png' ?>" class="user-image"
                                     alt="User Image">
                                <span class="hidden-xs"><?php echo Yii::$app->session->get('fullname') . '(' . Yii::$app->session->get('nickname') . ')'; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo Yii::$app->request->BaseUrl . '/upload/emp_img/Pictures_HyperL/' . $idcard . '.png' ?>" class="img-circle"
                                         alt="User Image">
                                    <p>
                                        <?php echo Yii::$app->session->get('fullname'); ?>
                                        <?php echo Yii::$app->session->get('positionname'); ?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="#" id="alogout" class="btn btn-default btn-flat"> ออกจากระบบ</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!--/.nav-collapse -->


        </nav>
    </header>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content">
            <div style="width:100%; height: 50px;margin-bottom:  10px;">
                <div class="pull-right">
                    <button type="button" id="btnSignOut" class="btn btn-block btn-danger btn-lg"><i class="fa fa-sign-out"></i> ออกจากระบบ</button>
                </div>
            </div>
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">ชื่อ: <?php echo Yii::$app->session->get('name') ?>  <?php echo Yii::$app->session->get('surname') ?> ตำแหน่ง : <?php echo Yii::$app->session->get('positioncode').'('.Yii::$app->session->get('positionname').')' ?></h3>
                    <!-- /.box-tools -->
                    <div class="pull-right"><?php echo DateTime::datefullformate(); ?></div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php echo $content; ?>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">แจ้งปัญหา Easy ERP  :   @pns4297p, เบอร์โทรแจ้งปัญหา Easy ERP  :  053 - 711 - 605   ต่อ  221  </h3>
                    <!-- /.box-tools -->

                </div>
                <!-- /.box-header -->
                <div class="box-body">

                </div>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    <!-- =============================================== -->


    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.9.8
        </div>
        <strong>Copyright &copy; 2017 บริษัท อีซูซุเชียงราย จำกัด</strong> All rights reserved.
    </footer>


    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>