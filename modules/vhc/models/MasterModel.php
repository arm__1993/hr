<?php

namespace app\modules\vhc\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "vhc_list".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $list_name
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class MasterModel extends \yii\db\ActiveRecord
{
    // 'create_by' => 'Create By',
    // 'update_by' => 'Update By',
    // 'create_date' => 'Create Date',
    // 'update_date' => 'Update Date',
    public function behaviors()
    {
        $session = Yii::$app->session;
        $session->open();
        $_account = $session->get('idcard');
        $session->close();
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_by'],
                ],
                'value' => $_account, //$_SESSION['USER_ACCOUNT'],
            ],
        ];
    }
}
