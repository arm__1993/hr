<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "EMP_SALARY".
 *
 * @property integer $EMP_SALARY_ID
 * @property string $EMP_SALARY_ID_CARD
 * @property string $EMP_SALARY_POSITION_CODE
 * @property string $EMP_SALARY_POSITION_LEVEL
 * @property string $EMP_SALARY_WORKING_COMPANY
 * @property string $EMP_SALARY_DEPARTMENT
 * @property string $EMP_SALARY_SECTION
 * @property string $EMP_SALARY_CHART
 * @property string $EMP_SALARY_STEP
 * @property string $EMP_SALARY_LEVEL
 * @property string $EMP_SALARY_WAGE
 * @property string $EMP_SALARY_CHART_ID
 * @property string $EMP_SALARY_GUARANTEE_MONEY
 * @property integer $status
 * @property integer $statusSSO
 * @property integer $statusCalculate
 * @property integer $statusMainJob
 * @property string $start_effective_date
 * @property string $end_effective_date
 * @property string $probation_date
 * @property string $EMP_SALARY_CREATE_DATE
 * @property string $EMP_SALARY_CREATE_BY
 * @property string $EMP_SALARY_UPDATE_DATE
 * @property string $EMP_SALARY_UPDATE_BY
 * @property string $SALARY_STEP_ADD
 * @property integer $SALARY_CHANGE_ID
 * @property string $SALARY_CHANGE_NAME
 * @property integer $id_ref_orgchart
 */
class Empsalary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EMP_SALARY';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EMP_SALARY_WAGE', 'EMP_SALARY_GUARANTEE_MONEY', 'SALARY_STEP_ADD'], 'number'],
            [['status', 'statusSSO', 'statusCalculate', 'statusMainJob', 'SALARY_CHANGE_ID', 'id_ref_orgchart'], 'integer'],
            [['start_effective_date', 'end_effective_date', 'probation_date', 'EMP_SALARY_CREATE_DATE', 'EMP_SALARY_UPDATE_DATE'], 'safe'],
            [['EMP_SALARY_ID_CARD'], 'string', 'max' => 20],
            [['EMP_SALARY_POSITION_CODE', 'EMP_SALARY_POSITION_LEVEL', 'EMP_SALARY_WORKING_COMPANY', 'EMP_SALARY_DEPARTMENT', 'EMP_SALARY_SECTION', 'EMP_SALARY_CHART', 'EMP_SALARY_STEP', 'EMP_SALARY_LEVEL', 'EMP_SALARY_CHART_ID'], 'string', 'max' => 100],
            [['EMP_SALARY_CREATE_BY', 'EMP_SALARY_UPDATE_BY'], 'string', 'max' => 250],
            [['SALARY_CHANGE_NAME'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EMP_SALARY_ID' => 'Emp  Salary  ID',
            'EMP_SALARY_ID_CARD' => 'Emp  Salary  Id  Card',
            'EMP_SALARY_POSITION_CODE' => 'Emp  Salary  Position  Code',
            'EMP_SALARY_POSITION_LEVEL' => 'Emp  Salary  Position  Level',
            'EMP_SALARY_WORKING_COMPANY' => 'Emp  Salary  Working  Company',
            'EMP_SALARY_DEPARTMENT' => 'Emp  Salary  Department',
            'EMP_SALARY_SECTION' => 'Emp  Salary  Section',
            'EMP_SALARY_CHART' => 'Emp  Salary  Chart',
            'EMP_SALARY_STEP' => 'Emp  Salary  Step',
            'EMP_SALARY_LEVEL' => 'Emp  Salary  Level',
            'EMP_SALARY_WAGE' => 'Emp  Salary  Wage',
            'EMP_SALARY_CHART_ID' => 'Emp  Salary  Chart  ID',
            'EMP_SALARY_GUARANTEE_MONEY' => 'Emp  Salary  Guarantee  Money',
            'status' => 'Status',
            'statusSSO' => 'Status Sso',
            'statusCalculate' => 'Status Calculate',
            'statusMainJob' => 'Status Main Job',
            'start_effective_date' => 'Start Effective Date',
            'end_effective_date' => 'End Effective Date',
            'probation_date' => 'Probation Date',
            'EMP_SALARY_CREATE_DATE' => 'Emp  Salary  Create  Date',
            'EMP_SALARY_CREATE_BY' => 'Emp  Salary  Create  By',
            'EMP_SALARY_UPDATE_DATE' => 'Emp  Salary  Update  Date',
            'EMP_SALARY_UPDATE_BY' => 'Emp  Salary  Update  By',
            'SALARY_STEP_ADD' => 'Salary  Step  Add',
            'SALARY_CHANGE_ID' => 'Salary  Change  ID',
            'SALARY_CHANGE_NAME' => 'Salary  Change  Name',
            'id_ref_orgchart' => 'Id Ref Orgchart',
        ];
    }
}
