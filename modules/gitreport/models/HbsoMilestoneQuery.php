<?php

namespace app\modules\gitreport\models;

/**
 * This is the ActiveQuery class for [[HbsoMilestone]].
 *
 * @see HbsoMilestone
 */
class HbsoMilestoneQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return HbsoMilestone[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HbsoMilestone|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
