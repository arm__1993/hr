<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Maintelephone;
use yii\helpers\ArrayHelper;

class MaintelephoneController extends ActiveController
{
	public $modelClass = 'app\modules\wscommon\models\Maintelephone';
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionCountrowmaintel()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $countrow = new Maintelephone();
        $showcountrow = $countrow->countrowmaintel($cusno);
        return $showcountrow;
    }
    
    public function actionSearchmaintelcusformobile()
    {   
        //return 1;

        $cusno = $_GET['Cusno'];

        //return $firstname;
 
        $datatelcus = new Maintelephone();
        $showdatatelcus = $datatelcus->searchmaintelcusformobile($cusno);
        return $showdatatelcus;
    }
}
