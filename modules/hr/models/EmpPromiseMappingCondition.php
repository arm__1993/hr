<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_promise_mapping_condition".
 *
 * @property integer $id
 * @property string $field_name
 * @property string $param_name
 * @property string $operation_name
 * @property string $default_value
 */
class EmpPromiseMappingCondition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_promise_mapping_condition';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_name', 'param_name', 'operation_name'], 'string', 'max' => 100],
            [['default_value'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'field_name' => 'Field Name',
            'param_name' => 'Param Name',
            'operation_name' => 'Operation Name',
            'default_value' => 'Default Value',
        ];
    }
}
