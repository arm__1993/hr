-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 17, 2017 at 12:25 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_pnd_head`
--

CREATE TABLE `tax_pnd_head` (
  `id` int(11) NOT NULL COMMENT 'รหัส',
  `company_id` tinyint(3) NOT NULL COMMENT 'รหัสบริษัท',
  `paid_month` varchar(10) NOT NULL COMMENT 'ข้อมูลของเดือน',
  `for_month` tinyint(3) NOT NULL COMMENT 'ข้อมูลของเดือน',
  `for_year` int(11) NOT NULL COMMENT 'ข้อมูลของปี',
  `pnd_type` tinyint(3) NOT NULL COMMENT 'ประเภท ภงด1, 1=ภงด1, 2=ภงด1ก',
  `tax_regis_code` char(13) NOT NULL COMMENT 'เลขประจำตัวผู้เสียภาษี',
  `branch_no` varchar(5) NOT NULL COMMENT 'สาขาเลขที่',
  `company_name` varchar(500) NOT NULL COMMENT 'ชื่อบริษัท',
  `building` varchar(200) NOT NULL COMMENT 'ชื่ออาคาร',
  `room_no` varchar(50) NOT NULL COMMENT 'ชื่ออาคาร',
  `floor_no` varchar(3) NOT NULL COMMENT 'ชั้น',
  `village_name` varchar(100) NOT NULL COMMENT 'ชื่อหมู่บ้าน',
  `house_no` varchar(20) NOT NULL COMMENT 'บ้านเลขที่',
  `moo` varchar(2) NOT NULL COMMENT 'หมู่',
  `soi` varchar(200) NOT NULL COMMENT 'ซอย',
  `junction` varchar(200) NOT NULL COMMENT 'แยก',
  `road` varchar(200) NOT NULL COMMENT 'ถนน',
  `tumbon` varchar(200) NOT NULL COMMENT 'ตำบล',
  `amphur` varchar(200) NOT NULL COMMENT 'อำเภอ',
  `province` varchar(200) NOT NULL COMMENT 'จังหวัด',
  `post_code` int(11) NOT NULL COMMENT 'จังหวัด',
  `sent_doc` tinyint(3) NOT NULL COMMENT '1=ยื่นปกติ, 2=ยื่นเพิ่มเติม',
  `sent_doc_no` tinyint(3) NOT NULL COMMENT '1=ยื่นปกติ, 2=ยื่นเพิ่มเติม',
  `file_attach_qty` tinyint(3) NOT NULL COMMENT 'จำนวนเอกสารแนบ',
  `media_attach_qty` tinyint(3) NOT NULL COMMENT 'จำนวนสื่อบันทึกแนบ',
  `sum_employee_gnr` int(11) NOT NULL COMMENT 'จำนวนพนักงานนำส่ง กรณีทั่วไป',
  `sum_income_gnr` decimal(15,2) NOT NULL COMMENT 'จำนวนยอดเงินได้ กรณีทั่วไป',
  `sum_tax_gnr` decimal(15,2) NOT NULL COMMENT 'จำนวนภาษีนำส่ง กรณีทั่วไป',
  `sum_employee_auth` int(11) NOT NULL COMMENT 'จำนวนพนักงานนำส่ง กรณีได้รับอนุมัติสรรพากร',
  `sum_income_auth` decimal(15,2) NOT NULL COMMENT 'จำนวนยอดเงินได้ กรณีได้รับอนุมัติสรรพากร',
  `sum_tax_auth` decimal(15,2) NOT NULL COMMENT 'จำนวนภาษีนำส่ง กรณีได้รับอนุมัติสรรพากร',
  `sum_employee_onepaid` int(11) NOT NULL COMMENT 'จำนวนพนักงานนำส่ง กรณีออกจ่ายครั้งเดียว',
  `sum_income_onepaid` decimal(15,2) NOT NULL COMMENT 'จำนวนยอดเงินได้ กรณีออกจ่ายครั้งเดียว',
  `sum_tax_onepaid` decimal(15,2) NOT NULL COMMENT 'จำนวนภาษีนำส่ง กรณีออกจ่ายครั้งเดียว',
  `sum_employee_inthai` int(11) NOT NULL COMMENT 'จำนวนพนักงานนำส่ง กรณี ผู้รับเงินอยู่ในไทย',
  `sum_income_inthai` decimal(15,2) NOT NULL COMMENT 'จำนวนยอดเงินได้ กรณี ผู้รับเงินอยู่ในไทย',
  `sum_tax_inthai` decimal(15,2) NOT NULL COMMENT 'จำนวนภาษีนำส่ง กรณี ผู้รับเงินอยู่ในไทย',
  `sum_employee_notin` int(11) NOT NULL COMMENT 'จำนวนพนักงานนำส่ง กรณี ผู้รับเงินไม่อยู่ในไทย',
  `sum_income_notin` decimal(15,2) NOT NULL COMMENT 'จำนวนยอดเงินได้ กรณี ผู้รับเงินไม่อยู่ในไทย',
  `sum_tax_notin` decimal(15,2) NOT NULL COMMENT 'จำนวนภาษีนำส่ง กรณี ผู้รับเงินไม่อยู่ในไทย',
  `total_employee` int(11) NOT NULL COMMENT 'ยอดรวมจำนวนพนักงานนำส่ง',
  `total_income` decimal(15,2) NOT NULL COMMENT 'ยอดรวมจำนวนยอดเงินได้',
  `total_tax` decimal(15,2) NOT NULL COMMENT 'ยอดรวมจำนวนภาษีนำส่ง',
  `extra_money` decimal(15,2) NOT NULL COMMENT 'จำนวนเงินเพิ่ม',
  `grand_total` decimal(15,2) NOT NULL COMMENT 'ยอดรวมภาษีนำส่ง + จำนวนเงินเพิ่ม',
  `signature_name` varchar(255) NOT NULL COMMENT 'ชื่อผู้มีอำนาจเซ็นต์',
  `signature_position` varchar(255) NOT NULL COMMENT 'ตำแหน่งผู้มีอำนาจเซ็นต์',
  `signature_date` date NOT NULL COMMENT 'ตำแหน่งผู้มีอำนาจเซ็นต์',
  `createby_user` char(13) NOT NULL COMMENT 'เลขประจำตัวผู้สร้าง',
  `create_datetime` datetime NOT NULL COMMENT 'สร้างรายการเมื่อวันที่เวลา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_pnd_head`
--
ALTER TABLE `tax_pnd_head`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_pnd_head`
--
ALTER TABLE `tax_pnd_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
