<?php

namespace app\modules\wssale\models;

use Yii;

/**
 * This is the model class for table "CusInt_Product".
 *
 * @property integer $Int_Product_TrcNo
 * @property string $Int_Num_Ref
 * @property string $Int_CusNo
 * @property string $Int_PlanContact_Ref
 * @property string $BuyNo
 * @property string $Int_Unit_Type_1C
 * @property string $Int_Model_Class_1C
 * @property string $Int_Model_CodeName
 * @property string $Int_Model_GName
 * @property string $Color
 * @property string $BuyVolume
 * @property string $Remark
 * @property string $Segment_id_ref
 * @property string $Segment_Remark
 * @property string $status
 * @property string $active_status
 */
class Cusintproduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CusInt_Product';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BuyVolume', 'Segment_id_ref', 'Segment_Remark', 'status', 'active_status'], 'required'],
            [['Remark'], 'string'],
            [['Int_Num_Ref', 'Int_CusNo', 'Int_PlanContact_Ref', 'BuyNo', 'Int_Unit_Type_1C', 'Int_Model_Class_1C', 'Int_Model_CodeName', 'BuyVolume'], 'string', 'max' => 50],
            [['Int_Model_GName', 'Segment_Remark'], 'string', 'max' => 100],
            [['Color'], 'string', 'max' => 10],
            [['Segment_id_ref'], 'string', 'max' => 11],
            [['status', 'active_status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Int_Product_TrcNo' => 'หมายเลขข้อมูล',
            'Int_Num_Ref' => 'อ้างอิงหมายเลขความสนใจ',
            'Int_CusNo' => 'หมายเลขอ้างอิงลูกค้า',
            'Int_PlanContact_Ref' => 'หมายเลขอ้างอิงการนำเสนอติดตาม',
            'BuyNo' => 'ลำดับคันที่ซื่อ Ex คันที่ 1 , 2',
            'Int_Unit_Type_1C' => 'ชนิดรถ',
            'Int_Model_Class_1C' => 'รุ่นรถ',
            'Int_Model_CodeName' => 'รหัสแบบรถ',
            'Int_Model_GName' => 'แบบรถทั่วไป',
            'Color' => 'สี',
            'BuyVolume' => 'จำนวนที่สนใจซื้อ',
            'Remark' => 'หมายเหตุ',
            'Segment_id_ref' => 'รหัส SegmentId  จากตาราง SEGMENT',
            'Segment_Remark' => 'หมายเหตุ กรณี segment=90=อื่นๆ',
            'status' => '1=จองได้',
            'active_status' => '1=active',
        ];
    }
}
