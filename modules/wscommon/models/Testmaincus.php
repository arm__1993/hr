<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "TestMainCus".
 *
 * @property integer $id
 * @property string $name
 */
class Testmaincus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
       /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }
    public static function tableName()
    {
        return 'TestMainCus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    public function searchcustomerdataforsalemobile($Cus_Name,$Cus_Surename,$Cus_IDNo) // param
    {   
        $Cus_Name = "%".$Cus_Name."%";
        $Cus_Surename = "%".$Cus_Surename."%";
        $Cus_IDNo = "%".$Cus_IDNo."%";
        $datacustomer = Yii::$app->dbERP_maincusdata
       ->createCommand("select * from MainCusData where Cus_Name LIKE :Cus_Name 
        AND Cus_Surename LIKE :Cus_Surename AND Cus_IDNo LIKE :Cus_IDNo
        ")
       ->bindParam(':Cus_Name',$Cus_Name)
       ->bindParam(':Cus_Surename',$Cus_Surename)
       ->bindParam(':Cus_IDNo',$Cus_IDNo)
       ->queryAll();
        return $datacustomer;

    }
}
