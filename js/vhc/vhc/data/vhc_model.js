$(document).ready(function () {
    ///ส่วนจ่าย///
    $("#btnSaveActivity").on("click", function () {
        var f = frmValidate('frmAddVhcmodel');
        if (!f) {
           // console.log('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy');
            showWarningInputForm();
        } else {
            $('.record_status').val('1');
            // console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            savevhcmodel(1);

        }
    });

    $('#btnAddNewVhcmodel').on("click", function () {
        initForm('btnSaveActivity');

    });

    ///ส่วนหัก///
    $("#btnSaveActivityDeduct").on("click", function () {
        var f = frmValidate('frmDeduct');
        if (!f) {
            showWarningInputForm();
        } else {
            $('.record_status').val('2');
            savevhcmodel(2);
        }
    });

    $('#btnAddNewVhcmodel').on("click", function () {
        initForm('btnSaveActivity');
        $('#hide_activityedit').val('');
        $('#model_name').val('');
        $('#record_status').prop('checked', true);
    });
});

function deletevhcmodel(id, valselect) { //อันนี้ยกเลิกเน้อออออออออออออออออ
    // alert(id);
    $.ajax({
        url: 'deletevhcmodel',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showDeleteSuccess();
                $.pjax.reload({container: "#pjax_tb_deducttemp"});  //Reload GridView

                selecttab(valselect);
            } else {
                showDeleteError();
                console.log(data);

            }
        }
    });
}
function savevhcmodel(idselectform) {
    if (idselectform == 1) {
        var datavar = $('#frmAddVhcmodel').serialize();
    } else {
        var datavar = $('#frmDeduct').serialize();
    }
    // console.log(datavar);
    $.ajax({
        url: 'savevhcmodel',
        data: datavar,
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showSaveSuccess();
                if (idselectform == 1) {
                    initForm('frmAddVhcmodel');
                } else {
                    initForm('frmDeduct');
                }

                selecttab(idselectform);
                //$.pjax.reload({container: "#pjax_tb_adddeducttemp"});  //Reload GridView
            } else {
                showSaveError();
            }
        }
    });
}

/*

function editvhcmodel(id, valueform) {
    // console.log(id);
    if (valueform == 1) {
        initForm('frmAddVhcmodel');
    } else {
        initForm('frmDeduct');
    }

    $.ajax({
        url: 'updatevhcmodel',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            // console.log(data);
            $('.hide_activityedit').val(data.id);
            $('.model_name').val(data.model_name);

            $('.record_status').val(data.record_status);

            $(".record_status option[value='" + data.record_status + "']").attr("selected", "selected");
            if (data.record_status == '1') {
                $('.record_status').prop('checked', true);
            } else {
                $('.record_status').prop('checked', false);
            }
            if (data.record_status == '1') {
                $('.record_status').val('1');
                //$(".tax_section_id option[value='" + data.tax_section_id + "']").attr("selected", "selected");
            } else {
                $('.record_status').val('2');
            }

            if (valueform == 1) {
                $('#modalfrmAddVhcmodel').modal();
            } else {
                $('#modalfrmDeduct').modal();
            }

        }
    });
}
*/



function editaddedduct(id) {//แก้ไขเน้อออออออออออ
   // console.log(id);
    initForm('frmAddVhcmodel');//idฟอมมมมม
    $.ajax({
        url: 'updatevhcmodel',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
          //  console.log(data);
            //
            if(data.record_status==1){
                $('#record_status').prop('checked', true);
            }else {
                $('#record_status').prop('checked', false);
            }

            $('#hide_activityedit').val(data.id);
            $('#model_name').val(data.model_name);
            $('#modalfrmAddVhcmodel').modal();
        }
    });
}

function savevhcmodel() {//อันนี้นี้เซฟเน้ออออออออออออออออ
    var datavar = $('#frmAddVhcmodel').serialize();

    console.log(datavar);
    $.ajax({
        url: 'savevhcmodel',
        data: datavar,
        type: 'POST',
        success: function (data) {
            console.log(data);
            if (parseInt(data) == 1) {
                showSaveSuccess();
                initForm('frmAddVhcmodel');
                $.pjax.reload({container: "#pjax_tb_deducttemp"});  //Reload GridView
            }
            else {
                showSaveError();
            }
        }
    });
}

function selecttab(val) {
    //alert("2222" + val);
    // console.log($("#tabselect" + val));
    $(".tabselect").removeClass("active");
    $("#tabselect" + val).addClass("active");
    $("#tab" + val).addClass("active");

    if (val == 1) {
        $.pjax.reload({container: "#pjax_tb_adddeducttemp"}); //Reload GridView
    } else {
        $.pjax.reload({container: "#pjax_tb_deducttemp"}); //Reload GridView
    }


}