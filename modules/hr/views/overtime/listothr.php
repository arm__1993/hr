<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 12/4/2017 AD
 * Time: 07:13
 */



use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/ot_listhr.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#"> ข้อมูลเงินเดือน</a>
                </li>
                <li>อนุมัติข้อมูลการทำงานล่วงเวลา</li>
                <li class="active">รายการจ่ายค่าล่วงเวลารออนุมัติ (HR)</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>


        <form class="form-horizontal">
            <div class="box-header with-border">
                <h3 class="box-title">รายการจ่ายค่าล่วงเวลารออนุมัติ (HR)</h3>
            </div>

            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        &nbsp;
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="btn-group">
                                <label for="numberPassportEmp" class="col-sm-3 control-label">บริษัท</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="company_id" id="company_id">
                                        <option value="">--- ทั้งหมด ---</option>
                                        <?php $working = ApiHr::getWorking_company();
                                        foreach ($working as $value) {
                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>


        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">รายการจ่ายค่าล่วงเวลา</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive" id="dvloadot" style="padding: 5px;">
                        <img class="loading-image" src="../../images/global/ajax-loader.gif" alt="loading..">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->