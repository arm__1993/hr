-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2017 at 09:06 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr_PAYROLL`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_structure`
--

CREATE TABLE `tax_income_structure` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'รหัส',
  `income_floor` decimal(15,2) NOT NULL COMMENT 'ช่วงเงินได้ เริ่มต้น',
  `income_ceil` decimal(15,2) NOT NULL COMMENT 'ช่วงเงินได้ สิ้นสุด',
  `tax_rate` decimal(4,2) NOT NULL COMMENT 'อัตราภาษี',
  `gross_step` decimal(15,2) DEFAULT NULL COMMENT 'ภาษีแต่ละขั้นเงินได้สุทธิ',
  `max_accumulate` decimal(15,2) DEFAULT NULL COMMENT 'ภาษีสะสมสูงสุดของขั้น',
  `from_year` int(11) NOT NULL COMMENT 'เริ่มใช้ตั้งแต่ปี (คศ)',
  `to_year` int(11) DEFAULT NULL COMMENT 'จนถึงปี',
  `remark` varchar(255) DEFAULT NULL COMMENT 'หมายเหตุ',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_income_structure`
--

INSERT INTO `tax_income_structure` (`id`, `income_floor`, `income_ceil`, `tax_rate`, `gross_step`, `max_accumulate`, `from_year`, `to_year`, `remark`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, '1.00', '150000.00', '0.00', '0.00', '0.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 17:43:36', '1509901325106', '2017-06-14 20:56:44'),
(2, '150001.00', '300000.00', '5.00', '7500.00', '7500.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 17:44:17', '1509901325106', '2017-06-14 17:46:06'),
(3, '300001.00', '500000.00', '10.00', '20000.00', '27500.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 17:47:30', '1509901325106', '2017-06-14 17:55:17'),
(4, '500001.00', '750000.00', '15.00', '37500.00', '65000.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 17:48:21', '1509901325106', '2017-06-14 19:57:34'),
(5, '750001.00', '1000000.00', '20.00', '50000.00', '115000.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 17:51:16', '1509901325106', '2017-06-14 19:58:03'),
(6, '1000001.00', '2000000.00', '25.00', '250000.00', '365000.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 17:52:08', '1509901325106', '2017-06-14 20:09:37'),
(7, '2000001.00', '5000000.00', '30.00', '900000.00', '1265000.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 17:53:18', '1509901325106', '2017-06-14 20:10:28'),
(8, '5000001.00', '0.00', '35.00', '0.00', '0.00', 2017, NULL, '', 1, '1509901325106', '2017-06-14 18:02:46', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_income_structure`
--
ALTER TABLE `tax_income_structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_year` (`from_year`),
  ADD KEY `to_year` (`to_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_income_structure`
--
ALTER TABLE `tax_income_structure`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส', AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
