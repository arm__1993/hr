<?php

namespace app\modules\vhc\models\VHC;

use Yii;

/**
 * This is the model class for table "vhc_save_service_list".
 *
 * @property integer $id
 * @property integer $vhc_save_sevice_id
 * @property string $bar_code
 * @property integer $item_id
 * @property string $itme_type
 * @property string $price
 * @property string $vat
 * @property string $invatprice
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcSaveServiceList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_save_service_list';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vhc_save_sevice_id', 'bar_code', 'item_id', 'itme_type', 'price', 'vat', 'invatprice'], 'required'],
            [['vhc_save_sevice_id', 'item_id', 'status'], 'integer'],
            [['price', 'vat', 'invatprice'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['bar_code'], 'string', 'max' => 50],
            [['itme_type'], 'string', 'max' => 2],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vhc_save_sevice_id' => 'Vhc Save Sevice ID',
            'bar_code' => 'Bar Code',
            'item_id' => 'Item ID',
            'itme_type' => 'Itme Type',
            'price' => 'Price',
            'vat' => 'Vat',
            'invatprice' => 'Invatprice',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
