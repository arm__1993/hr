<?php

namespace app\modules\wssale\models;

use Yii;

class Sell extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Sell';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['S_Booking_No', 'cusBuy', 'Sell_Date', 'Eng_No_S', 'Sell_Type_Num', 'credit_Type', 'nummonny', 'datailfrinan', 'Sale_Sell_posCode', 'Sale_Deliver_posCode', 'Date_Deliver', 'PFComment', 'SaleNameFull', 'Sale_DeliveFull', 'whoadd', 'first_installment', 'last_installment', 'end_installment', 'first_offer_date', 'reg_no', 'reg_date', 'reg_exp_date', 'status_interviwe', 'delivery_place', 'sell_branch', 'Sstatus_tis', 'SDate_upstatus', 'price_prb', 'price_insure', 'Rebase_money', 'comp_insurance', 'comp_insurance_name', 'comp_insurance_cusnum', 'comp_finance', 'detail_tax', 'book_hieasy', 'cus_hieasy', 'idRef_vihicle', 'vehicle_dlr_id', 'vehicle_dlr_code', 'status_report', 'status_report_sell'], 'required'],
            [['Sell_Type_Num', 'credit_Type', 'nummonny', 'sell_branch', 'comp_insurance_cusnum', 'idRef_vihicle', 'vehicle_dlr_id'], 'integer'],
            [['Remark', 'PFComment'], 'string'],
            [['first_installment', 'last_installment', 'end_installment', 'first_offer_date', 'reg_date', 'reg_exp_date', 'book_hieasy', 'cus_hieasy'], 'safe'],
            [['S_Booking_No', 'Sell_Card_No', 'CusNo', 'cusBuy', 'Sell_Date', 'Eng_No_S', 'Price_CRP', 'Price_Discount', 'Price_Sell', 'Price_Vat', 'Price_ExVat', 'Sub_UsedCar_S', 'AccessoryCost_S', 'Down_S', 'Sub_Down_S', 'Real_DownPay_S', 'Int_S', 'Time_S', 'Install_Month_S', 'RecCost_S', 'OthCost_S', 'Total_Budget_S', 'Reg_Free_C', 'Tran_Free_C', 'Ins_Free_C', 'Ins_Code', 'Turn_data', 'Sale_Deliver', 'Date_Deliver', 'Campaign_Sell01', 'Campaign_Sell02', 'S_Input_Time', 'PFCheckPerson', 'PFCheckDate', 'price_prb', 'price_insure', 'Rebase_money', 'vehicle_dlr_code'], 'string', 'max' => 50],
            [['Sell_NS', 'Sale_Sell', 'detail_tax'], 'string', 'max' => 100],
            [['Sell_Add', 'Chassi_No_S', 'Sell_Type_S', 'PF', 'SaleNameFull', 'Sale_DeliveFull'], 'string', 'max' => 255],
            [['datailfrinan'], 'string', 'max' => 200],
            [['Sale_Sell_posCode', 'Sale_Deliver_posCode', 'Campaign_Tis01', 'Campaign_Tis02', 'reg_no'], 'string', 'max' => 30],
            [['whoadd', 'delivery_place', 'comp_insurance', 'comp_insurance_name'], 'string', 'max' => 250],
            [['status_interviwe'], 'string', 'max' => 2],
            [['Sstatus_tis'], 'string', 'max' => 10],
            [['SDate_upstatus'], 'string', 'max' => 20],
            [['comp_finance'], 'string', 'max' => 150],
            [['status_report', 'status_report_sell'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Sell_No' => 'วิ่ง',
            'S_Booking_No' => 'หมายเลขจอง',
            'Sell_Card_No' => 'หมายเลขการด์',
            'CusNo' => 'หมายเลขลูกค้า',
            'cusBuy' => 'crmข้อมูลคน',
            'Sell_Date' => 'วันที่ขาย',
            'Sell_NS' => 'ชื่อใช้',
            'Sell_Add' => 'ที่อยู่ใช้',
            'Eng_No_S' => 'เลขเครื่อง',
            'Chassi_No_S' => 'เลขแชสสี',
            'Price_CRP' => 'ราคาตั้ง',
            'Price_Discount' => 'ส่วนลด',
            'Price_Sell' => 'ราคาขายจริงรวม Vat',
            'Price_Vat' => 'ราคาขายจริง Vat',
            'Price_ExVat' => 'ราคาขายจริง ExVat',
            'Sub_UsedCar_S' => 'ส่วนลดที่ใช้กับรถมือสอง',
            'AccessoryCost_S' => 'รวมราคาอุปกรณ์ตกแต่ง',
            'Sell_Type_Num' => '1 = สด , 2 ผ่อน',
            'Sell_Type_S' => 'ขายโดย',
            'credit_Type' => '1=มีเคดิต 2=ไม่มีเคดิต',
            'nummonny' => 'จำนวนเงินเครดิต',
            'datailfrinan' => 'DetailFinance คำสั่งพิเศษ(ไฟแนนซ์)',
            'Down_S' => 'เงินดาวน์',
            'Sub_Down_S' => 'ส่วนลดเงินดาวน์',
            'Real_DownPay_S' => 'เงินดาวน์ที่จ่ายจริง',
            'Int_S' => 'ดอกเบี้ย',
            'Time_S' => 'ระยะเวลา',
            'Install_Month_S' => 'ส่งต่อเดือน',
            'RecCost_S' => 'ค่าแนะนำรวม',
            'OthCost_S' => 'ค่าใช้จ่ายอื่นๆรวม',
            'Total_Budget_S' => 'งบประมาณที่ใช้ทั้งหมด',
            'Reg_Free_C' => 'ค่าใช้จ่ายอื่นๆ (ค่าทะเบียน)',
            'Tran_Free_C' => 'ค่าขนส่งทุน จะไม่ใช้',
            'Ins_Free_C' => 'ค่าพรบ.ทุน จะไม่ใช้',
            'Ins_Code' => 'รหัสผู้ทำ พรบ. จะไม่ใช้',
            'Turn_data' => 'รถเทอร์น(เลขทะเบียน) จะไม่ใช้',
            'Sale_Sell' => 'เซลส์ผู้ขาย บันทึกรหัสแทน',
            'Sale_Sell_posCode' => 'รหัสตำแหน่งผู้ขาย บันทึกรหัสแทน',
            'Sale_Deliver' => 'เซลส์ผู้ปล่อยรถ จะไม่ใช้',
            'Sale_Deliver_posCode' => 'รหัสตำแหน่งผู้ปล่อยรถ',
            'Date_Deliver' => 'วันที่ออกรถ',
            'Campaign_Tis01' => 'แคมเปญTis01 จะไม่ใช้',
            'Campaign_Sell01' => 'แคมเปญที่ลูกค้าเลือกใช้01 จะไม่ใช้',
            'Campaign_Tis02' => 'แคมเปญTis02 จะไม่ใช้',
            'Campaign_Sell02' => 'แคมเปญที่ลูกค้าเลือกใช้02 จะไม่ใช้',
            'Remark' => 'หมายเหตุ',
            'S_Input_Time' => 'เวลาบันทึกข้อมูล',
            'PF' => 'PF',
            'PFComment' => 'หมายเหตุ PF',
            'PFCheckPerson' => 'ผู้เช็คPF',
            'PFCheckDate' => 'เวลาPF',
            'SaleNameFull' => 'เซลส์ผู้ขาย',
            'Sale_DeliveFull' => 'Sale ผู้ส่งมอบรถ',
            'whoadd' => 'พนง ตัดขาย',
            'first_installment' => 'วันที่จ่ายงวดแรก',
            'last_installment' => 'วันที่จ่ายค่างวด งวดสุดท้าย',
            'end_installment' => 'วันที่ปิดค่างวด',
            'first_offer_date' => 'วันที่เสนอเปลี่ยนรถ',
            'reg_no' => 'หมายเลขทะเบียน',
            'reg_date' => 'วันที่จดทะเบียน',
            'reg_exp_date' => 'วันที่ทะเบียนหมดอายุ',
            'status_interviwe' => 'สถานะแบบสอบถาม 1=3วัน 2=45วัน',
            'delivery_place' => 'สาขาที่ส่งมอบ',
            'sell_branch' => 'ขายสาขาไหน ',
            'Sstatus_tis' => 'Sstatus Tis',
            'SDate_upstatus' => 'Sdate Upstatus',
            'price_prb' => 'ค่านายหน้าจาก พรบ',
            'price_insure' => 'ค่านายหน้า ป.1 ',
            'Rebase_money' => 'ค่า Rebase',
            'comp_insurance' => 'ค่าคอมประกันชีวต(รถใหม่)',
            'comp_insurance_name' => 'ชื่อบริษัทประกันชีวิต ',
            'comp_insurance_cusnum' => 'ชื่อบริษัทประกันชีวิต ',
            'comp_finance' => 'ค่าคอมไฟแนนซ์',
            'detail_tax' => 'หมายเหตุ_ใบกำกับภาษี',
            'book_hieasy' => '1=export แล้ว',
            'cus_hieasy' => 'เชื่อมต่อ hieays ubon ไม่ใช่ function หลักใช้เฉพาะ ubon',
            'idRef_vihicle' => 'crmข้อมูลรถ',
            'vehicle_dlr_id' => 'Vehicle Dlr ID',
            'vehicle_dlr_code' => 'Vehicle Dlr Code',
            'status_report' => '1:คำนวณรายงานเรียบร้อยแล้ว,2:คำนวณส่งมอบรถแล้ว',
            'status_report_sell' => '1.คำนวนรายงานสรุปการจำหน่ายรายคัน',
        ];
    }
    public function searchsellformobile($cusno)
    {

        $sql = "select Sell.Sell_No,
                       Sell.Sell_Date,
                       Booking.B_Model_C_M, 
                       Booking.B_CusNo, 
                       SUBSTR(cusBuy,INSTR(Sell.cusBuy,'_')+1 ) AS ResultcusBuy 
                FROM Sell INNER JOIN Booking ON 
                        Sell.S_Booking_No= Booking.Booking_No 
                WHERE Booking.CusNo = :cusno 
                        ORDER BY Sell.Sell_No ASC";
        //echo ;
        $showdataeducusSell = Yii::$app->dbERP_Easysale_icmba
       ->createCommand($sql)
       ->bindParam(':cusno',$cusno)
       ->queryAll();

        
        return $showdataeducusSell;
 
    }
}
