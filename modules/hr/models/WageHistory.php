<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "WAGE_HISTORY".
 *
 * @property integer $WAGE_ID
 * @property integer $WAGE_THIS_MONTH_ROW_NUM
 * @property string $WAGE_EMP_ID
 * @property string $WAGE_FIRST_NAME
 * @property string $WAGE_LAST_NAME
 * @property string $WAGE_PAY_DATE
 * @property string $WAGE_POSITION_CODE
 * @property string $WAGE_POSITION_NAME
 * @property integer $WAGE_SECTION_ID
 * @property integer $WAGE_DEPARTMENT_ID
 * @property integer $WAGE_WORKING_COMPANY
 * @property string $WAGE_BANK_NAME
 * @property string $WAGE_ACCOUNT_NUMBER
 * @property integer $WAGE_GET_MONEY_TYPE
 * @property string $WAGE_SALARY_CHART
 * @property string $WAGE_SALARY_LEVEL
 * @property integer $WAGE_SALARY_STEP
 * @property string $WAGE_SALARY
 * @property string $WAGE_SALARY_BY_CHART
 * @property string $WAGE_TOTAL_ADDITION
 * @property string $WAGE_EARN_PLUS_ADD
 * @property string $WAGE_THIS_MONTH_TAX
 * @property string $WAGE_TOTAL_DEDUCTION
 * @property string $WAGE_EARN_MINUS_DEDUCT
 * @property string $WAGE_NET_SALARY
 * @property integer $WAGE_THIS_MONTH_CONFIRM
 * @property string $WAGE_THIS_MONTH_CONFIRM_BY
 * @property string $WAGE_THIS_MONTH_CONFIRM_DATE
 * @property integer $WAGE_THIS_MONTH_EMPLOYEE_LOCK
 * @property integer $WAGE_THIS_MONTH_DIRECTOR_LOCK
 * @property integer $WAGE_THIS_MONTH_STATUS
 * @property integer $WAGE_THIS_MONTH_BANK_CONFIRM_STATUS
 * @property string $WAGE_THIS_MONTH_BANK_CONFIRM_DATE
 * @property string $WAGE_THIS_MONTH_BANK_CONFIRM_BY
 * @property string $WAGE_REMARK
 * @property string $WAGE_CREATE_DATE
 * @property string $WAGE_CREATE_BY
 * @property string $WAGE_UPDATE_DATE
 * @property string $WAGE_UPDATE_BY
 * @property integer $is_endofmonth
 * @property integer $cal_times
 * @property string $cal_date
 * @property integer $wage_calculate_id
 * @property string $COMPANY_NAME
 * @property string $DEPARTMENT_NAME
 * @property string $SECTION_NAME
 * @property string $DIRECTOR_LOCK_COMFIRM_BY
 * @property string $DIRECTOR_LOCK_COMFIRM_DATE
 * @property string $WTH_AMOUNT
 * @property string $WTH_AMOUNT_TAX
 * @property string $BNF_AMOUNT
 * @property string $PND_TOTAL_INCOME
 * @property string $PND_AMOUNT_REDUCE
 * @property string $PND_AMOUNT_TAX
 * @property string $SSO_AMOUNT
 */
class WageHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'WAGE_HISTORY';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WAGE_THIS_MONTH_ROW_NUM', 'WAGE_EMP_ID', 'WAGE_FIRST_NAME', 'WAGE_LAST_NAME', 'WAGE_PAY_DATE', 'WAGE_POSITION_CODE', 'WAGE_POSITION_NAME', 'WAGE_SECTION_ID', 'WAGE_DEPARTMENT_ID', 'WAGE_WORKING_COMPANY', 'WAGE_BANK_NAME', 'WAGE_ACCOUNT_NUMBER', 'WAGE_GET_MONEY_TYPE', 'WAGE_SALARY_CHART', 'WAGE_SALARY_LEVEL', 'WAGE_SALARY_STEP', 'WAGE_SALARY', 'WAGE_SALARY_BY_CHART', 'WAGE_TOTAL_ADDITION', 'WAGE_EARN_PLUS_ADD', 'WAGE_THIS_MONTH_TAX', 'WAGE_TOTAL_DEDUCTION', 'WAGE_EARN_MINUS_DEDUCT', 'WAGE_NET_SALARY', 'WAGE_THIS_MONTH_CONFIRM', 'WAGE_THIS_MONTH_CONFIRM_BY', 'WAGE_THIS_MONTH_CONFIRM_DATE', 'WAGE_THIS_MONTH_EMPLOYEE_LOCK', 'WAGE_THIS_MONTH_DIRECTOR_LOCK', 'WAGE_THIS_MONTH_STATUS', 'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS', 'WAGE_THIS_MONTH_BANK_CONFIRM_DATE', 'WAGE_THIS_MONTH_BANK_CONFIRM_BY', 'WAGE_CREATE_DATE', 'WAGE_CREATE_BY', 'WAGE_UPDATE_DATE', 'WAGE_UPDATE_BY', 'COMPANY_NAME', 'DEPARTMENT_NAME', 'SECTION_NAME', 'DIRECTOR_LOCK_COMFIRM_BY', 'DIRECTOR_LOCK_COMFIRM_DATE', 'WTH_AMOUNT', 'WTH_AMOUNT_TAX', 'BNF_AMOUNT', 'PND_TOTAL_INCOME', 'PND_AMOUNT_REDUCE', 'PND_AMOUNT_TAX', 'SSO_AMOUNT'], 'required'],
            [['WAGE_THIS_MONTH_ROW_NUM', 'WAGE_SECTION_ID', 'WAGE_DEPARTMENT_ID', 'WAGE_WORKING_COMPANY', 'WAGE_GET_MONEY_TYPE', 'WAGE_SALARY_STEP', 'WAGE_THIS_MONTH_CONFIRM', 'WAGE_THIS_MONTH_EMPLOYEE_LOCK', 'WAGE_THIS_MONTH_DIRECTOR_LOCK', 'WAGE_THIS_MONTH_STATUS', 'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS', 'is_endofmonth', 'cal_times', 'wage_calculate_id'], 'integer'],
            [['WAGE_SALARY', 'WAGE_SALARY_BY_CHART', 'WAGE_TOTAL_ADDITION', 'WAGE_EARN_PLUS_ADD', 'WAGE_THIS_MONTH_TAX', 'WAGE_TOTAL_DEDUCTION', 'WAGE_EARN_MINUS_DEDUCT', 'WAGE_NET_SALARY', 'WTH_AMOUNT', 'WTH_AMOUNT_TAX', 'BNF_AMOUNT', 'PND_TOTAL_INCOME', 'PND_AMOUNT_REDUCE', 'PND_AMOUNT_TAX', 'SSO_AMOUNT'], 'number'],
            [['WAGE_THIS_MONTH_CONFIRM_DATE', 'WAGE_THIS_MONTH_BANK_CONFIRM_DATE', 'WAGE_CREATE_DATE', 'WAGE_UPDATE_DATE', 'cal_date', 'DIRECTOR_LOCK_COMFIRM_DATE'], 'safe'],
            [['WAGE_EMP_ID'], 'string', 'max' => 20],
            [['WAGE_FIRST_NAME', 'WAGE_LAST_NAME', 'WAGE_POSITION_NAME', 'WAGE_SALARY_CHART', 'WAGE_SALARY_LEVEL'], 'string', 'max' => 200],
            [['WAGE_PAY_DATE', 'WAGE_POSITION_CODE'], 'string', 'max' => 100],
            [['WAGE_BANK_NAME', 'WAGE_ACCOUNT_NUMBER', 'WAGE_THIS_MONTH_CONFIRM_BY', 'WAGE_THIS_MONTH_BANK_CONFIRM_BY', 'WAGE_REMARK', 'WAGE_CREATE_BY', 'WAGE_UPDATE_BY', 'DIRECTOR_LOCK_COMFIRM_BY'], 'string', 'max' => 250],
            [['COMPANY_NAME', 'DEPARTMENT_NAME', 'SECTION_NAME'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'WAGE_ID' => 'Wage  ID',
            'WAGE_THIS_MONTH_ROW_NUM' => 'Wage  This  Month  Row  Num',
            'WAGE_EMP_ID' => 'Wage  Emp  ID',
            'WAGE_FIRST_NAME' => 'Wage  First  Name',
            'WAGE_LAST_NAME' => 'Wage  Last  Name',
            'WAGE_PAY_DATE' => 'Wage  Pay  Date',
            'WAGE_POSITION_CODE' => 'Wage  Position  Code',
            'WAGE_POSITION_NAME' => 'Wage  Position  Name',
            'WAGE_SECTION_ID' => 'Wage  Section  ID',
            'WAGE_DEPARTMENT_ID' => 'Wage  Department  ID',
            'WAGE_WORKING_COMPANY' => 'Wage  Working  Company',
            'WAGE_BANK_NAME' => 'Wage  Bank  Name',
            'WAGE_ACCOUNT_NUMBER' => 'Wage  Account  Number',
            'WAGE_GET_MONEY_TYPE' => 'Wage  Get  Money  Type',
            'WAGE_SALARY_CHART' => 'Wage  Salary  Chart',
            'WAGE_SALARY_LEVEL' => 'Wage  Salary  Level',
            'WAGE_SALARY_STEP' => 'Wage  Salary  Step',
            'WAGE_SALARY' => 'Wage  Salary',
            'WAGE_SALARY_BY_CHART' => 'Wage  Salary  By  Chart',
            'WAGE_TOTAL_ADDITION' => 'Wage  Total  Addition',
            'WAGE_EARN_PLUS_ADD' => 'Wage  Earn  Plus  Add',
            'WAGE_THIS_MONTH_TAX' => 'Wage  This  Month  Tax',
            'WAGE_TOTAL_DEDUCTION' => 'Wage  Total  Deduction',
            'WAGE_EARN_MINUS_DEDUCT' => 'Wage  Earn  Minus  Deduct',
            'WAGE_NET_SALARY' => 'Wage  Net  Salary',
            'WAGE_THIS_MONTH_CONFIRM' => 'Wage  This  Month  Confirm',
            'WAGE_THIS_MONTH_CONFIRM_BY' => 'Wage  This  Month  Confirm  By',
            'WAGE_THIS_MONTH_CONFIRM_DATE' => 'Wage  This  Month  Confirm  Date',
            'WAGE_THIS_MONTH_EMPLOYEE_LOCK' => 'Wage  This  Month  Employee  Lock',
            'WAGE_THIS_MONTH_DIRECTOR_LOCK' => 'Wage  This  Month  Director  Lock',
            'WAGE_THIS_MONTH_STATUS' => 'Wage  This  Month  Status',
            'WAGE_THIS_MONTH_BANK_CONFIRM_STATUS' => 'Wage  This  Month  Bank  Confirm  Status',
            'WAGE_THIS_MONTH_BANK_CONFIRM_DATE' => 'Wage  This  Month  Bank  Confirm  Date',
            'WAGE_THIS_MONTH_BANK_CONFIRM_BY' => 'Wage  This  Month  Bank  Confirm  By',
            'WAGE_REMARK' => 'Wage  Remark',
            'WAGE_CREATE_DATE' => 'Wage  Create  Date',
            'WAGE_CREATE_BY' => 'Wage  Create  By',
            'WAGE_UPDATE_DATE' => 'Wage  Update  Date',
            'WAGE_UPDATE_BY' => 'Wage  Update  By',
            'is_endofmonth' => 'Is Endofmonth',
            'cal_times' => 'Cal Times',
            'cal_date' => 'Cal Date',
            'wage_calculate_id' => 'Wage Calculate ID',
            'COMPANY_NAME' => 'Company  Name',
            'DEPARTMENT_NAME' => 'Department  Name',
            'SECTION_NAME' => 'Section  Name',
            'DIRECTOR_LOCK_COMFIRM_BY' => 'Director  Lock  Comfirm  By',
            'DIRECTOR_LOCK_COMFIRM_DATE' => 'Director  Lock  Comfirm  Date',
            'WTH_AMOUNT' => 'Wth  Amount',
            'WTH_AMOUNT_TAX' => 'Wth  Amount  Tax',
            'BNF_AMOUNT' => 'Bnf  Amount',
            'PND_TOTAL_INCOME' => 'Pnd  Total  Income',
            'PND_AMOUNT_REDUCE' => 'Pnd  Amount  Reduce',
            'PND_AMOUNT_TAX' => 'Pnd  Amount  Tax',
            'SSO_AMOUNT' => 'Sso  Amount',
        ];
    }
}
