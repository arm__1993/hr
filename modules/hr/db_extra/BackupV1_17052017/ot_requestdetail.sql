-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 03, 2017 at 09:15 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_requestdetail`
--

CREATE TABLE `ot_requestdetail` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `ot_requestmaster_id` int(11) NOT NULL COMMENT 'รหัสตารางการเสนอขอโอที',
  `id_card` char(13) NOT NULL COMMENT 'รหัสบัตรประชาชน พนักงานที่ขอโอที',
  `time_start` time NOT NULL COMMENT 'เวลาเริ่มทำโอที',
  `time_end` time NOT NULL COMMENT 'เวลาสิ้นสุดทำโอที',
  `time_total` decimal(10,2) NOT NULL COMMENT 'จำนวนชั่วโมงที่ได้รับ',
  `return_id` tinyint(3) NOT NULL COMMENT 'รหัสลำดับการขอค่าตอบแทน',
  `return_name` varchar(100) NOT NULL COMMENT 'ชื่อค่าตอบแทน',
  `is_check` tinyint(3) NOT NULL COMMENT 'การตรวจสอบจากหัวหน้างาน 1=ตรวจสอบแล้ว, 0=ยังไม่ได้ตรวจสอบ',
  `check_byuser` char(13) DEFAULT NULL COMMENT 'ตรวจสอบโดยผู้ใช้',
  `check_date` date DEFAULT NULL COMMENT 'วันที่ที่ตรวจสอบ',
  `check_time` time DEFAULT NULL COMMENT 'เวลาที่ตรวจสอบ',
  `is_approved` tinyint(3) NOT NULL COMMENT 'สถานะ approved โดย HR,  1= approved, 0 =  Not approved',
  `approved_byuser` char(13) DEFAULT NULL COMMENT 'approved โดยผู้ใช้',
  `approved_date` date DEFAULT NULL COMMENT 'วันที่ approved',
  `approved_time` time DEFAULT NULL COMMENT 'เวลาที่ approved',
  `createby_user` char(13) NOT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime NOT NULL COMMENT 'วันที่เวลาสร้างข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_requestdetail`
--
ALTER TABLE `ot_requestdetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_card` (`id_card`),
  ADD KEY `ot_requestmaster_id` (`ot_requestmaster_id`),
  ADD KEY `return_id` (`return_id`),
  ADD KEY `check_byuser` (`check_byuser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ot_requestdetail`
--
ALTER TABLE `ot_requestdetail`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
