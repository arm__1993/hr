<?php 
use yii\helpers\Html;
use app\api\DateTime;
use app\api\Utility;
$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$dataitem = $data;
$preperNumber=str_split($dataitem[0][tax_regis_code]);
$branchNumber = str_split((strlen($dataitem[0][branch_no])<5)?str_pad($dataitem[0][branch_no],5,"0",STR_PAD_LEFT):$dataitem[0][branch_no]);
$post_code = str_split($dataitem[0][post_code])
?>
<div width='120%' >

     <div width='83%' style="float:left;height:70px; background-color:#cdd1dd; border: 0px solid #000;border-top-left-radius: 45px;border-top-right-radius: 8px;border-bottom-left-radius: 45px;border-bottom-right-radius: 8px;" >
           <table width='100%' style='text-align: center;'>
                <tr>
                    <td>
                        <img height="60" width="60" src="<?php echo $imghr; ?>/rd-logo.png" class="img-circle">
                    </td>
                    <td>
                        <p>แบบยื่นรายการภาษีเงินได้หัก ณ ที่จ่าย</p>
                        <p>ตามมาตรา 59 แห่งประมวลรัษฎากร</p>
                        <p style='font-size:10px'>สำหรับการหักภาษี ณ ที่จ่ายตามมาตรา 50 (1) กรณีการจ่ายเงินได้พึงประเมินตามมาตรา 40 (1)(2) แห่งประมวลรัษฎากร</p>
                    </td>
                </tr>
            </table>
    </div>

    <div width='15%' style="float:right;height:4em; background-color:#ffffff; border: 3px solid #cdd1dd;border-radius: 8px;" >
         <span style='font-size:22pt;font-weight:bold;text-align:center'>ภ.ง.ด.1</span>
    </div>

</div>
<div width='120%' style='border: 1px solid #ccc;border-left: 0px;border-right:0px;border-right:0px;border-top: 0px;position:relative;padding-top:15px;' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
    <div width='54.7%' style='border: 1px solid #ccc;border-left: 0px;border-top: 0px;border-bottom: 0px; position:absolute;float:left'>
        <div  style='border: 1px solid #ccc;border-left: 0px;border-top: 0px;border-top: 0px; '>
            <table >
                <tr>
                    <td style='font-size:7pt;'><b>เลขประจำตัวผู้เสียภาษีอากร</b></td>
                    <td>
                        <table  border='1' style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc padding-top:0px" class='borderunset' cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                                <tr >
                                    <td><?php echo $preperNumber[0] ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $preperNumber[1] ?></td>
                                    <td style="border-left:0px;"><?php echo $preperNumber[2] ?></td>
                                    <td style="border-left:0px;"><?php echo $preperNumber[3] ?></td>
                                    <td style="border-left:0px;"><?php echo $preperNumber[4] ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $preperNumber[5] ?></td>
                                    <td style="border-left:0px;"><?php echo $preperNumber[6] ?></td>
                                    <td style="border-left:0px;"><?php echo $preperNumber[7] ?></td>
                                    <td style="border-left:0px;"><?php echo $preperNumber[8] ?></td>
                                    <td><?php echo $preperNumber[9] ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $preperNumber[10] ?></td>
                                    <td style="border-left:0px;"><?php echo $preperNumber[11] ?></td>
                                    <td style="border:0px;">-</td>
                                    <td><?php echo $preperNumber[12] ?></td>
                                    
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td style='font-size:5pt;text-align:right'>(ของผู้มีหน้าที่หักภาษี ณ ที่จ่าย)</td>
                    <td>
                    </td>
                </tr>
            </table>
            <table width='100%'>
                <tr>
                    <td style='font-size:7pt;width:60%;text-align:left'><b>ชื่่อผู้มีหน้าที่หักภาษี ณ ที่จ่าย</b> (หน่วยงาน) :</td>
                    <td>
                        <table align='right'>
                            <tr>
                                <td style='font-size:7pt;' align='right'>สาขาที่</td>
                                <td style='font-size:7pt;' align='right'>
                                    <table border='1' style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc;border-top: 0px;border-bottom: 0px;  padding-top:0px" cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                                        <tr>    
                                                <?php foreach($branchNumber as $item){
                                                    echo "<td style='font-size:7pt;'>".$item."</td>";
                                                } ?>
                                                <!--<td style='font-size:7pt;'>0</td>
                                                <td style="border-left:0px; font-size:7pt;">0</td>
                                                <td style="border-left:0px; font-size:7pt;">4</td>
                                                <td style="border-left:0px; font-size:7pt;">9</td>
                                                <td style='font-size:7pt;'>9</td>-->
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    ........................................................................................
                    </td>
                </tr>
                <!--style='padding-bottom:-15px'-->
                <tr>
                    <td  style='padding-bottom:-15px' colspan='2'>
                    <p style='font-size:9pt;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $data[0][building]?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $data[0][room_no];?>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <?php echo $data[0][floor_no];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $data[0][village_name]?></p>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    <p style='font-size:9pt'><b>ที่อยู่:</b> อาคาร...........................ห้องเลขที่ .......ชั้นที่.......หมู่บ้าน................</p>
                    </td>
                </tr>
                <tr>
                    <td  style='padding-bottom:-15px' colspan='2'>
                    <p style='font-size:9pt;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $data[0][house_no] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $data[0][moo] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $data[0][soi] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $data[0][junction]; ?> </p>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    <p style='font-size:9pt'>เลขที่ ...........................หมู่ที่.......ตรอก/ซอย..........................แยก..........</p>
                    </td>
                </tr>
                <tr>
                    <td  style='padding-bottom:-15px' colspan='2'>
                    <p style='font-size:9pt;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $data[0][road]  ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $data[0][tumbon]  ?></p>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    <p style='font-size:9pt'>ถนน.......................................ตำบล/แขวง..........................................</p>
                    </td>
                </tr>
                <tr>
                    <td  style='padding-bottom:-15px' colspan='2'>
                    <p style='font-size:9pt;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $data[0] [amphur]  ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $data[0] [province]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                    <p style='font-size:9pt'>อำเภอ/เขต.......................................จังหวัด..........................................</p>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <table>
                            <tr>
                                <td  style='font-size:9pt' width='20%'>รหัสไปษณีย์</td>
                                <td style='font-size:9pt'>
                                    <table border='1' style="font-size:7pt;border-collapse: collapse; border: 1px solid #ccc;border-top: 0px;border-bottom: 0px;  padding-top:0px" cellpadding='4' rowpadding='2' cellspacing='0' width='100%'>
                                        <tr>
                                                <td style='font-size:7pt;'><?php echo $post_code[0]; ?></td>
                                                <td style="border-left:0px; font-size:7pt;"><?php echo $post_code[1]; ?></td>
                                                <td style="border-left:0px; font-size:7pt;"><?php echo $post_code[2]; ?></td>
                                                <td style="border-left:0px; font-size:7pt;"><?php echo $post_code[3]; ?></td>
                                                <td style='font-size:7pt;'><?php echo $post_code[4]; ?></td>
                                        </tr>
                                    </table>
                                </td >
                                <td style='font-size:9pt'> ..............................................................</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
       <div>
            <table align='center' style='padding:15px'>
            <tr>
                <td style='font-size:9pt;' ><input type="checkbox">(1) ยื่นปกติ</td>
                <td style='font-size:9pt;' ><input type="checkbox">(1) ยื่น<b>เพิ่มเพิ่ม</b>ครั้งที่</td>
                <td style='font-size:9pt;' >
                    <table style='border: 1px solid #ccc;padding-top:5px' cellpadding='-1' rowpadding='0' cellspacing='0'>
                        <tr>
                            <td style='border: 0px'>..........</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
       </div>
    </div>

    <div width='44.7%' style='border: 1px solid #ccc; position:absolute; top:0;float:right;border-top: 0px;border-right: 0px; border-left: 0px; padding:0px' >
        <table cellspacing='0' cellpadding='5'>
            <tr>
                <td colspan='4' style='font-size:9pt;'><b>เดือน</b>ที่จ่ายเงินได้พึงประเมิน</td>
            </tr>
            <tr>
                <td colspan='4' style='font-size:6pt;'>(ให้ทำเครื่องหมาย “” ลงใน “ ” หน้าชื่อเดือน) พ.ศ. .......................</td>
            </tr>
            <tr>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(1) มกราคม </td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(4) เมษายน</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(7) กรกฎาคม</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(10) ตุลาคม</td>
            </tr>
               
            <tr>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(2) กุมภาพันธ์</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(5 )พฤษภาคม</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(8)สิงหาคม</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(11)	พฤศจิกายน</td>
            </tr>
            <tr>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(3)มีนาคม</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(6)มิถุนายนน</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(9)กันยายน</td>
                <td style='font-size:9px;'><input type="checkbox" name="vehicle" value="Bike">(12)ธันวาคม</td>
            </tr>
             <tr>
                <td colspan='4'></td>
            </tr>
        </table>
    </div>
</div>
<table cellpadding='1' width='100%' style='margin-top:3px'>
    <tr>
        <td width='6%'></td>
        <td rowspan='4' width='44%'>
            <p style='font-size:9pt;'>มีรายละเอียดการหักเป็นรายผู้มีเงินได้ ปรากฏตาม</p>
            <p style='font-size:7pt;'>(ให้แสดงรายละเอียดในใบแนบ ภ.ง.ด.1 หรือในสื่อ</p>
            <p style='font-size:7pt;'>บันทึกในระบบคอมพิวเตอร์อย่างใดอย่างหนึ่งเท่านั้น)</p>
        </td>
        <td width='60%'>
            <table width='100%'>
                <tr>
                    <td style='font-size:9pt;'> <input type="checkbox" name="vehicle" value="Bike">ใบแนบ <b>ภ.ง.ด.1</b> ที่แนบมาพร้อมนี้ : </td>
                    <td style='font-size:9pt;text-align: right;'>จำนวน.................แผ่น</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>  
        <td></td>
        <td>
            <table width='100%'>
                <tr>
                    <td style='font-size:9pt;'><input type="checkbox" name="vehicle" value="Bike"><b>สื่อบันทึกในระบบคอมพิวเตอร์</b> ที่แนบมาพร้อมนี้ : </td>
                    <td style='font-size:9pt;text-align: right;'>จำนวน.................แผ่น</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <table width='100%'>
                <tr>
                    <td style='font-size:7pt;' colspan='2'>(ตามหนังสือแสดงความประสงค์ฯ ทะเบียนรับเลขที่.....................................................................</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <table width='100%'>
                <tr>
                    <td style='font-size:7pt;' colspan='2'>หรือตามหนังสือข้อตกลงการใช้งานฯ เลขอ้างอิงการลงทะเบียน...............................................)</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div align='center' width='120%' style='margin-top:7px'>
    <div  style="width: 50%; text-align:center;float:left; background-color:#cdd1dd; border: 0px solid #000;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;"  width='65%' id='div-1a'>
        สรุปรายการภาษีที่นำส่ง
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff; border: 1px solid #cdd1dd;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;"  width='10%' id='div-1b'>
        จำนวนราย
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff; border: 1px solid #cdd1dd;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;"  >
        เงินได้ทั้งสิ้น
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff; border: 1px solid #cdd1dd;border-top-left-radius: 10px;border-top-right-radius: 8px;border-bottom-left-radius: 10px;border-bottom-right-radius: 8px;" >
        ภาษีที่นำส่งทั้งสิ้น
    </div>
</div>
<div align='center' width='120%' style='font-size:9pt;margin-top:3px'>
    <div  style="width: 50%; text-align:left;float:left; "  width='65%' id='div-1a'>
        1. เงินได้ตามมาตรา 40 (1) เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"  width='10%' id='div-1b'>
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%;font-size:9pt;'>
            <tr>
                <td style='text-align:center;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_employee_gnr],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;"  >
       <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%;font-size:9pt;'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_income_gnr],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
         <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%;font-size:9pt;'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_tax_gnr],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div align='center' style width='120%' style='font-size:9pt;'>
    <div  style="width: 50%; text-align:left;float:left; "  width='65%' id='div-1a'>
        2. เงินได้ตามมาตรา 40 (1) เงินเดือน ค่าจ้าง ฯลฯ กรณีได้รับ<br>
           อนุมัติจากกรมสรรพากรให้หักอัตราร้อยละ 3<br>
            (ตามหนังสือที่...................ลงวันที่......................)
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"  width='10%' id='div-1b'>
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:center;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_employee_auth],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;"  >
       <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_income_auth],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_tax_auth],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div align='center' width='120%' style='font-size:9pt;'>
    <div  style="width: 50%; text-align:left;float:left; "  width='65%' id='div-1a'>
        3.เงินได้ตามมาตรา 40 (1) (2) กรณีนายจ้างจ่ายให้ครั้งเดียว<br>
        เพราะเหตุออกจากงาน  
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"  width='10%' id='div-1b'>
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:center;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_employee_onepaid],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;"  >
       <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_income_onepaid],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_tax_onepaid],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div align='center' width='120%' style='font-size:9pt;'>
    <div  style="width: 50%; text-align:left;float:left; "  width='65%' id='div-1a'>
        4. เงินได้ตามมาตรา 40 (2) กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย 
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"  width='10%' id='div-1b'>
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:center;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_employee_inthai],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;"  >
       <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_income_inthai],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_tax_inthai],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div align='center' width='120%' style='font-size:9pt;'>
    <div  style="width: 50%; text-align:left;float:left; "  width='65%' id='div-1a'>
        5. เงินได้ตามมาตรา40 (2) กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"  width='10%' id='div-1b'>
         <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:center;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_employee_notin],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;"  >
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_income_notin],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
         <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_tax_notin],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div align='center' width='120%' style='font-size:9pt;'>
    <div  style="width: 50%; text-align:left;float:left; "  width='65%' id='div-1a'>
        6.รวม ...........................................
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width:10%;text-align:center;float:left; background-color:#fff;"  width='10%' id='div-1b'>
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:center;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][total_employee],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div  style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;"  >
       <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][total_income],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][sum_income_gnr],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div align='center' width='120%' style='font-size:9pt;'>
    <div  style="width: 80.5%; text-align:left;float:left; "  width='65%' id='div-1a'>
        7. เงินเพิ่ม (ถ้ามี) ...........................................................................................................................................
    </div>
    
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][extra_money],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div align='center' width='120%' style='font-size:9pt;'>
    <div  style="width: 80.5%; text-align:left;float:left; "  width='65%' id='div-1a'>
        8.รวมยอดภาษีที่นำส่งทั้งสิ้น และเงินเพิ่ม (6. + 7.) ...........................................................................................
    </div>

    <div style="padding-left:5px;padding-right:5px;margin-left:10px;width: 15%;text-align:center;float:left; background-color:#fff;" >
        <table style='border: 1px solid #ccc;border-collapse: collapse;width:100%'>
            <tr>
                <td style='text-align:right;font-size:9pt;'>
                    <?php echo number_format($dataitem[0][grand_total],2,".",","); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<hr>
<div align='center' width='120%' style='font-size:9pt;padding-bottom:0px'>
    <p style="text-align:center;margin-top:1px">
        ข้าพระเจ้าขอรับรองว่า รายการที่แจ้งไปข้างต้นนี้ เป็นรายการที่ถูกต้องและครบถ้วนทุกประการ
    </p>
    
    <div style="width:65%;padding-left:5px;padding-right:5px;margin-left:10px;text-align:center;float:left; background-color:#fff;">
            <p style="text-align:center;">ลงชื่อ..............................................................................................ผู้จ่ายเงิน</p>
            <p style="text-align:center;padding-top:-15px;padding-bottom:-15px"><?php echo $dataitem[0][signature_name]; ?></p>
            <p style="text-align:center;padding-top:-15px">(..............................................................................................)</p>
            <p style="text-align:center;padding-top:-15px;padding-bottom:-15px"><?php echo $dataitem[0][signature_position];?></p>
            <p style="text-align:center;padding-top:-15px">ตำแหน่ง........................................................................................</p>
            <p style="text-align:center;padding-top:-10px">ยื่นวันที่...........เดือน.......................................พ.ศ. ................... </p>
    </div>
    <div style="padding-left:5px;padding-right:5px;margin-left:10px;padding-bottom:-2px;width: 30%;text-align:left;float:right; background-color:#fff;">
        <p></p>
        <p></p>
        <p></p>
        <img height="60" width="60" src="<?php echo $imghr; ?>/stamp-icon.png" class="img-circle">
    </div>    
</div>
<hr style='margin-top:-15px'>
<div style='float:right;text-align:right;font-size:9pt;padding-top:-15px'><p>(ก่อนกรอกรายการ ดูคำชี้แจงด้านหลัง)</p></div>
<div style='float:left;text-align:lefts;font-size:9pt;padding-top:-5px'><p>สอบถามข้อมูลเพิ่มเติมได้ที่ศูนย์สารนิเทศสรรพากร โทร.<img style='padding-bottom:-5px' height='20' src="<?php echo $imghr; ?>/210360_WHT1_kor.pdf.png" class="img-circle"> 1161</p></div>
