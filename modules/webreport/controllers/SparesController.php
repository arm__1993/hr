<?php

namespace app\modules\webreport\controllers;

use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiSpares;

use mPDF;

class SparesController extends \yii\web\Controller
{
    public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionProfit_lost()
    {
        return $this->render('profit_lost');
    }

    public function actionReceive_profit()
    {
        $postCompany = Yii::$app->request->post();
        $GET_Company = $postCompany['company'];
        //$type_company = ApiReport::TypeCompany($GET_Company);
        $type_company = $GET_Company;
        $postReservation = Yii::$app->request->post();
        $months = ($postReservation['reservation']);

        $dayEnd = substr($months, 13, 2);
        $MonthEnd = substr($months, 16, -5);
        $yearEnd = substr($months, 19);
        $dayStart = substr($months, 0, -21);
        $MonthStart = substr($months, 3, -18);
        $yearStart = substr($months, 6, -13);
        $str_start = (strtotime("$dayStart-$MonthStart-$yearStart"));
        $str_end = (strtotime("$dayEnd-$MonthEnd-$yearEnd"));


        //$sql_NoNumber =  แบบไม่มีเลขรถ

        $NoNumber = ApiSpares::getNoNumber($type_company,$str_start,$str_end);




        foreach ($NoNumber as $value_nonumber) {
            $totalParts[] = $value_nonumber;
        }
        //$sql_Number = แบบมีเลขรถ
        $Number = ApiSpares::getNumber($type_company,$str_start,$str_end);
        foreach ($Number as $value_number) {
            $totalParts[] = $value_number;
        }
        
        //print_r($totalParts);
        if(empty($totalParts)){
            
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('profit_lost',['query'=>false,]);
        
        }else if(!empty($totalParts)){
        //     echo "not empty";
        // exit();

        $session = Yii::$app->session;

        $session->set('totalParts', $totalParts);

        $selected_company = $GET_Company;
        $selected_months = $months;

        return $this->render('profit_lost',[
            'totalParts' => $totalParts,
            'query'=>true,
            'selected_company'=>$selected_company,
            'selected_months'=>$selected_months,            
            ]);
        }
    }

    public function actionExportlost()
    {
        return $this->render('_exportlost');
    }

}
