<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiRepair;
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;


$dataSa_Wage_Distance_Only = $session->get('dataSa_Wage_Distance_Only');
$dataSa_Wage_Genaral_Only = $session->get('dataSa_Wage_Genaral_Only');
$dataSa_Wage_intersect = $session->get('dataSa_Wage_intersect');
$selected_company = $session->get('selected_company');
$selected_startmonth = $session->get('selected_startmonth');
$selected_endmonth = $session->get('selected_endmonth');
$selected_year = $session->get('selected_year');



?>

<div class="box">
    <h3><i class="fa fa-fw fa-file-text"></i>รายงานยอดรับแจ้งซ่อม SA รายคนตามใบงานซ่อมต่อเดือน</h3>
    <h3> <?php  $namecompany = ApiReport::getNameCompanyPDF($selected_company) ;
        echo  $namecompany['0']['name']; ?>
    </h3>
    <h3>เดือน <?php echo DateTime::convertMonth($selected_startmonth);?>
        ถึง <?php  echo DateTime::convertMonth($selected_endmonth);?>
        ปี <?php print_r($selected_year);?>
    </h3>
</div>
<h3>งานเช็คระยะ</h3>

<table width="100%" bgcolor="#777" ALIGN="CENTER" id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
            ชื่อ SA
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
            จำนวนคันรับแจ้งซ่อม
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
            เฉลี่ยต่อวัน
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
            รวมเวลารับแจ้งซ่อม (นาที)
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
            เวลาเฉลี่ยต่อคัน (นาที)
        </th>
    </tr>
    </thead>
    <?php if(!empty($dataSa_Wage_Distance_Only)){
        $countdataSa_Wage_Distance_Only = count($dataSa_Wage_Distance_Only);

        $sum_countcarA = [];
        $sum_avgAmountcarPerDayA =[];
        $sum_TimeA = [];
        $sum_avgTimePerAmountcarA = []

        ?>
        <?php
        $sumcountday = ApiRepair::countDayOfMonthStartStop($selected_year,$selected_startmonth,$selected_year,$selected_endmonth);
        ?>
        <tbody>
        <?php foreach ($dataSa_Wage_Distance_Only as $valueDataSa_Wage){?>
            <tr role="row">
                <td bgcolor="#eee" ALIGN="CENTER" ><?php echo $valueDataSa_Wage['Name'] ;?>
                    <?php echo $valueDataSa_Wage['Surname'] ; ?></td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php echo  $sum_countcarA[] = $CountA = $valueDataSa_Wage['Count_Car'] ;?></td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $sum_avgAmountcarPerDayA[] = $coutCarAvgA = $valueDataSa_Wage['Count_Car']/$sumcountday;
                    echo   Helper::displayDecimal($coutCarAvgA);
                    ?>
                </td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php    $sum_TimeA[] = $TimeA =$valueDataSa_Wage['SumDifftime'];
                    $getTimeA = DateTime::calculateTimeElapsed($TimeA);
                    echo  ApiRepair::getFormatTime($getTimeA['h'],$getTimeA['m'],$getTimeA['s']);
                    ?>
                </td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php   $AvgTimePercar = ApiRepair::avgTimePerCar($TimeA,$CountA);
                    $sum_avgTimePerAmountcarA[] =  $secAvgA =  Helper::displayDecimal($AvgTimePercar);
                    $getFormatTimeA = DateTime::calculateTimeElapsed($secAvgA);
                    echo  ApiRepair::getFormatTime($getFormatTimeA['h'],$getFormatTimeA['m'],$getFormatTimeA['s']);
                    ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>

        <tfoot>
        <tr>
            <th>รวม</th>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php echo array_sum($sum_countcarA); ?></td>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $TotalSum_avgAmountcarPerDayA = array_sum($sum_avgAmountcarPerDayA);
                echo   Helper::displayDecimal($TotalSum_avgAmountcarPerDayA);
                ?></td>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $TotalSum = array_sum($sum_TimeA);
                $getTotalFormatTimeA = DateTime::calculateTimeElapsed($TotalSum);
                echo ApiRepair::getFormatTime($getTotalFormatTimeA['h'],$getTotalFormatTimeA['m'],$getTotalFormatTimeA['s']);
                ?></td>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $sum_avgTimePerAmountcarA = array_sum($sum_avgTimePerAmountcarA);
                $sum_avgTimePerAmountcarA = DateTime::calculateTimeElapsed($sum_avgTimePerAmountcarA);
                echo ApiRepair::getFormatTime($sum_avgTimePerAmountcarA['h'],$sum_avgTimePerAmountcarA['m'],$sum_avgTimePerAmountcarA['s']);
                ?></td>
        </tr>
        </tfoot>
    <?php }?>
</table>

<br>
<br>
<h3> งานเช็คระยะ+งานซ่อมทั่วไป</h3>
<table width="100%" bgcolor="#777" ALIGN="CENTER" id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
            ชื่อ SA
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
            จำนวนคันรับแจ้งซ่อม
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
            เฉลี่ยต่อวัน
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
            รวมเวลารับแจ้งซ่อม (นาที)
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
            เวลาเฉลี่ยต่อคัน (นาที)
        </th>
    </tr>
    </thead>
    <?php if(!empty($dataSa_Wage_intersect)){
        $countdataSa_Wage_intersect = count($dataSa_Wage_intersect);

        $sum_countcarB = [];
        $sum_avgBmountcarPerDayB =[];
        $sum_TimeB = [];
        $sum_avgTimePerBmountcarB = []

        ?>
        <?php
        $sumcountday = ApiRepair::countDayOfMonthStartStop($selected_year,$selected_startmonth,$selected_year,$selected_endmonth);
        ?>
        <tbody>
        <?php foreach ($dataSa_Wage_intersect as $valueDataSa_Wage){?>
            <tr role="row">
                <td bgcolor="#eee" ALIGN="CENTER"><?php echo $valueDataSa_Wage['Name'] ;?>
                    <?php echo $valueDataSa_Wage['Surname'] ; ?></td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php echo  $sum_countcarB[] = $CountB = $valueDataSa_Wage['Count_Car'] ;?></td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $sum_avgBmountcarPerDayB[] = $coutCarBvgB = $valueDataSa_Wage['Count_Car']/$sumcountday;
                    echo   Helper::displayDecimal($coutCarBvgB);
                    ?>
                </td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php    $sum_TimeB[] = $TimeB =$valueDataSa_Wage['SumDifftime'];
                    $getTimeB = DateTime::calculateTimeElapsed($TimeB);
                    echo  ApiRepair::getFormatTime($getTimeB['h'],$getTimeB['m'],$getTimeB['s']);
                    ?>
                </td>
                <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php   $BvgTimePercar = ApiRepair::avgTimePerCar($TimeB,$CountB);
                    $sum_avgTimePerBmountcarB[] =  $secBvgB =  Helper::displayDecimal($BvgTimePercar);
                    $getFormatTimeB = DateTime::calculateTimeElapsed($secBvgB);
                    echo  ApiRepair::getFormatTime($getFormatTimeB['h'],$getFormatTimeB['m'],$getFormatTimeB['s']);
                    ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>

        <tfoot>
        <tr>
            <th>รวม</th>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php echo array_sum($sum_countcarB); ?></td>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $TotalSum_avgBmountcarPerDayB = array_sum($sum_avgBmountcarPerDayB);
                echo   Helper::displayDecimal($TotalSum_avgBmountcarPerDayB);
                ?></td>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php   $TotalSum = array_sum($sum_TimeB);
                $getTotalFormatTimeB = DateTime::calculateTimeElapsed($TotalSum);
                echo ApiRepair::getFormatTime($getTotalFormatTimeB['h'],$getTotalFormatTimeB['m'],$getTotalFormatTimeB['s']);
                ?></td>
            <td bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $sum_avgTimePerBmountcarB = array_sum($sum_avgTimePerBmountcarB);
                $sum_avgTimePerBmountcarB = DateTime::calculateTimeElapsed($sum_avgTimePerBmountcarB);
                echo ApiRepair::getFormatTime($sum_avgTimePerBmountcarB['h'],$sum_avgTimePerBmountcarB['m'],$sum_avgTimePerBmountcarB['s']);
                ?></td>
        </tr>
        </tfoot>
    <?php }?>
</table>

<br>
<br>
<h3>  งานซ่อมทั่วไป</h3>
<table  width="100%" bgcolor="#777" ALIGN="CENTER" id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
            ชื่อ SA
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
            จำนวนคันรับแจ้งซ่อม
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
            เฉลี่ยต่อวัน
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
            รวมเวลารับแจ้งซ่อม (นาที)
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
            เวลาเฉลี่ยต่อคัน (นาที)
        </th>
    </tr>
    </thead>
    <?php if(!empty($dataSa_Wage_Genaral_Only)){
        $countdataSa_Wage_Genaral_Only = count($dataSa_Wage_Genaral_Only);

        $sum_countcarC = [];
        $sum_avgAmountcarPerDayC = [];
        $sum_countTimeC = [];
        $sum_avgTimePerCmountcarC = []

        ?>
        <?php
        $sumcountday = ApiRepair::countDayOfMonthStartStop($selected_year,$selected_startmonth,$selected_year,$selected_endmonth);
        ?>
        <tbody>
        <?php foreach ($dataSa_Wage_Genaral_Only as $valueDataSa_Wage){?>
            <tr role="row">
                <td bgcolor="#eee" ALIGN="CENTER"><?php echo $valueDataSa_Wage['Name'] ;?>
                    <?php echo $valueDataSa_Wage['Surname'] ; ?></td>
                <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php echo  $sum_countcarC[] = $CountC = $valueDataSa_Wage['Count_Car'] ;?></td>
                <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $sum_avgCmountcarPerDayC[] = $coutCarCvgC = $valueDataSa_Wage['Count_Car']/$sumcountday;
                    echo   helper::displayDecimal($coutCarCvgC);
                    ?>
                </td>
                <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php    $sum_TimeC[] = $TimeC =$valueDataSa_Wage['SumDifftime'];
                    $getTimeC = DateTime::calculateTimeElapsed($TimeC);
                    echo  ApiRepair::getFormatTime($getTimeC['h'],$getTimeC['m'],$getTimeC['s']);
                    ?>
                </td>
                <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php   $CvgTimePercar = ApiRepair::avgTimePerCar($TimeC,$CountC);
                    $sum_avgTimePerCmountcarC[] =  $secCvgC =  helper::displayDecimal($CvgTimePercar);
                    $getFormatTimeC = DateTime::calculateTimeElapsed($secCvgC);
                    echo  ApiRepair::getFormatTime($getFormatTimeC['h'],$getFormatTimeC['m'],$getFormatTimeC['s']);
                    ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>

        <tfoot>
        <tr>
            <th>รวม</th>
            <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php echo array_sum($sum_countcarC); ?></td>
            <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $TotalSum_avgCmountcarPerDayC = array_sum($sum_avgCmountcarPerDayC);
                echo   helper::displayDecimal($TotalSum_avgCmountcarPerDayC);
                ?></td>
            <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $TotalSum = array_sum($sum_TimeC);
                $getTotalFormatTimeC = DateTime::calculateTimeElapsed($TotalSum);
                echo ApiRepair::getFormatTime($getTotalFormatTimeC['h'],$getTotalFormatTimeC['m'],$getTotalFormatTimeC['s']);
                ?></td>
            <td  bgcolor="#f0f8ff" ALIGN="CENTER"><?php  $sum_avgTimePerCmountcarC = array_sum($sum_avgTimePerCmountcarC);
                $sum_avgTimePerCmountcarC = DateTime::calculateTimeElapsed($sum_avgTimePerCmountcarC);
                echo ApiRepair::getFormatTime($sum_avgTimePerCmountcarC['h'],$sum_avgTimePerCmountcarC['m'],$sum_avgTimePerCmountcarC['s']);
                ?></td>
        </tr>
        </tfoot>
    <?php }?>
</table>

<?php
session_start();
session_destroy();
?>
