<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_promise_mapping".
 *
 * @property integer $id
 * @property string $condition_id
 * @property string $param_text
 * @property integer $fixable_type
 * @property string $db_name
 * @property string $tb_name
 * @property string $field_name
 * @property string $sel_command
 * @property string $default_text
 * @property string $desc_text
 */
class EmpPromiseMapping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_promise_mapping';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fixable_type'], 'integer'],
            [['condition_id', 'param_text'], 'string', 'max' => 50],
            [['db_name', 'tb_name', 'field_name'], 'string', 'max' => 100],
            [['sel_command'], 'string', 'max' => 200],
            [['default_text', 'desc_text'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'condition_id' => 'Condition ID',
            'param_text' => 'Param Text',
            'fixable_type' => 'Fixable Type',
            'db_name' => 'Db Name',
            'tb_name' => 'Tb Name',
            'field_name' => 'Field Name',
            'sel_command' => 'Sel Command',
            'default_text' => 'Default Text',
            'desc_text' => 'Desc Text',
        ];
    }
}
