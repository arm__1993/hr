<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\modules\hr\apihr\ApiHr;

$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
?>

<table width="100%">
    <tr>
        <td>
            <div class="row">
                <div class="col-sm-4">
                <h4><u>กลุ่มนกเงือก</u></h4></div>
                <div class="col-sm-4">&nbsp;</div>
                <div class="col-sm-4 text-right">
                <h5>ใบแจ้งเงินเดือน <br>(Pay Slip)</h5></div>
            </div>
        </td>
    </tr>
    
    <?php 
        $html= '';
        foreach($data as $key => $item){
        $html .= '<tr>';
        $html .= '<td>'.$item.'</td>';
        $html .= '<td ><a href="#" onClick="previewPDF(this);" title="'.$key.'-'.$year.'"><img style="text-align:left" src="'.$imghr.'/icon_PDF.png" class="img-circle"></a></td>';
        $html .= '</tr>';
        } 
    echo $html;
    ?>
</table>