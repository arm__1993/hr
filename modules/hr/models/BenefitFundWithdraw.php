<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "benefit_fund_withdraw".
 *
 * @property integer $id
 * @property string $emp_icard
 * @property string $date_pay
 * @property string $bnf_last_month
 * @property string $bnf_this_month
 * @property string $bnf_total_withdraw
 * @property string $paid_date
 * @property integer $paid_status
 */
class BenefitFundWithdraw extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'benefit_fund_withdraw';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emp_icard', 'date_pay', 'bnf_last_month', 'bnf_this_month', 'bnf_total_withdraw', 'paid_date', 'paid_status'], 'required'],
            [['bnf_last_month', 'bnf_this_month', 'bnf_total_withdraw'], 'number'],
            [['paid_date'], 'safe'],
            [['paid_status'], 'integer'],
            [['emp_icard'], 'string', 'max' => 13],
            [['date_pay'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_icard' => 'Emp Icard',
            'date_pay' => 'Date Pay',
            'bnf_last_month' => 'Bnf Last Month',
            'bnf_this_month' => 'Bnf This Month',
            'bnf_total_withdraw' => 'Bnf Total Withdraw',
            'paid_date' => 'Paid Date',
            'paid_status' => 'Paid Status',
        ];
    }
}
