<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 17/10/2559
 * Time: 11:03
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

$objPHPExcel = new \PHPExcel();
$date_rpt = date('Ymd');

$session = Yii::$app->session;

$carRepairman = $session->get('carRepairman');
$NameAndTime = $session->get('NameAndTime');
$checkStatus = $session->get('checkStatus');
$dateNOW = $session->get('dateNOW');
$selected_company = $session->get('selected_company');
$selected_technician = $session->get('selected_technician');



// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('AngsanaUPC')
    ->setSize(14);

// Add some data  วิว


$objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
$objWorkSheet->setCellValue('A1', 'ลำดับที่');
$objWorkSheet->setCellValue('B1', ' เลขที่ใบซ่อม');
$objWorkSheet->setCellValue('C1', 'วันที่เข้ารับบริการ');
$objWorkSheet->setCellValue('D1', 'รุ่นรถ');
$objWorkSheet->setCellValue('E1', 'ทะเบียน');
$objWorkSheet->setCellValue('F1', 'วันที่ปิดงานโดย SA');
$objWorkSheet->setCellValue('G1', 'ประเภทงานซ่อม');
$objWorkSheet->setCellValue('H1', 'ช่างซ่อม.');
$objWorkSheet->setCellValue('I1', 'SAรับแจ้งซ่อม');
$objWorkSheet->setCellValue('J1', 'วันนัดส่งมอบรถ');
$objWorkSheet->setCellValue('K1', 'สถานะที่ค้าง');
$objWorkSheet->setCellValue('L1', 'จำนวนวันค้างซ่อมสะสม/วัน');



$_srow = 2; // เริ่มใส่ข้อมูลบรรทัดที่ 2
$i = 0;
// $value = count($NameAndTime);
// //foreach($result as $row) {
// for($_srow=2;$_srow<=$value
// ;$_srow++)
foreach ($NameAndTime as  $value) 
{
    $objWorkSheet->setCellValue('A' . $_srow, ($i+1));
    $objWorkSheet->setCellValue('B' . $_srow, ($value['id_repair']));
    $objWorkSheet->setCellValue('C' . $_srow, ($value['guard_time']));
    $objWorkSheet->setCellValue('D' . $_srow, ($value['fullmodel_name']));
    $objWorkSheet->setCellValue('E' . $_srow, ($value['register']));
    $objWorkSheet->setCellValue('F' . $_srow, ($value['end_time']));
    $objWorkSheet->setCellValue('G' . $_srow, '-');
    $objWorkSheet->setCellValue('H' . $_srow,  ($value['technician_name']));
    $objWorkSheet->setCellValue('I' . $_srow, ($value['name_sa']));
    $objWorkSheet->setCellValue('J' . $_srow, ($value['app_time']));
    $objWorkSheet->setCellValue('K' . $_srow, $checkStatus);
    $objWorkSheet->setCellValue('L'. $_srow, (int)(($dateNOW - $value['app_time'])/86400));
    $_srow ++;
    $i++;
}

//วิา
/*// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A4', 'Miscellaneous glyphs')
    ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');*/



// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('รายงานจำนวนรถที่ซ่อมต่อช่าง');//ชื่อในไฟล
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel2007)
/*foreach(range('A','B','C','D','E','F','G','H','I','J','K','L') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}*/
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="รายงานรถค้างซ่อมสะสมประจำวัน'.$date_rpt.'.xlsx"');//=ชื่อไฟล์
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
<?php
session_start();
session_destroy();
?>