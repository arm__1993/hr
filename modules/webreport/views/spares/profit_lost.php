<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 20/10/2559
 * Time: 11:16 6.1
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/jquery.datetimepicker.full.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

 $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });   

});
JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานกำไรขาดทุนขายอะไหล่</h3>

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
    </div>
    <!-- ./box-header -->

    <div class="box-body">
        <div class="row">
            <form method="post" action="receive_profit">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key=>$value) {
                                    $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>ช่วงวันที่</label>
                        &nbsp;
                          <div class="btn-group">                            
                            <div class="input-group" style="width: 250px">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" name="reservation" id="reservation" data-provide="datepicker" data-date-language="th" <?php if(!empty($selected_company)){ echo 'value="'.$selected_months.'"';}?>>
                            </div>
                          </div>
                          &nbsp;
                          <div class="btn-group">
                          <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>  
                        </div>                      
                          &nbsp;&nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/spares/exportlost">
                        <button type="button" class="btn btn-success">                        
                            <i class="fa fa-file-excel-o"></i> ออกรายงาน Excel
                        </button>
                        </a>
                        </div>
                    </center>
                    </div>
                </form>
            </div>
        </div>
        <!-- ./box-body -->
 <?php if($query){ ?>
    <?php if(empty($totalParts)){ ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title">รายงานกำไรขาดทุนการขายอะไหล่ประจำวัน</h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขใบงาน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รุ่นรถ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ทะเบียน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขเครื่อง
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เบอร์อะไหล่
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ชื่ออะไหล่
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        จำนวน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ราคาขาย
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ราคาทุน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        กำไร
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">12563</td>
                                        <td>TFR54HP</td>
                                        <td>ชรบน2251</td>
                                        <td>BC2256</td>
                                        <td>9-88531905-A</td>
                                        <td>น้ำมันเครื่อง</td>
                                        <td>1</td>
                                        <td>725</td>
                                        <td>508</td>
                                        <td>30%</td>
                                    </tr> 
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">12563</td>
                                        <td>TFR54HP</td>
                                        <td>ชรบน2251</td>
                                        <td>BC2256</td>
                                        <td>8-98451206-1</td>
                                        <td>กรองเครื่อง</td>
                                        <td>1</td>
                                        <td>220</td>
                                        <td>180</td>
                                        <td>22%</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">12569</td>
                                        <td>TFR77HP</td>
                                        <td>ชรกค1296</td>
                                        <td>CD8274</td>
                                        <td>8-95623256-0</td>
                                        <td>โช็คอัพหลัง L</td>
                                        <td>2</td>
                                        <td>1240</td>
                                        <td>1054</td>
                                        <td>15%</td>
                                    </tr>                                   
                                    </tbody>
                                </table>
                                <h5>ราคาขายรวม&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                2185
                                </h5>
                                <h5>ราคาทุนรวม&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                1742
                                </h5>
                                <h5>กำไรเบื้องต้น&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                20.28%
                                </h5>
                                <h5>จำนวนชิ้นรวม&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                4
                                </h5>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php }else { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title">รายงานกำไรขาดทุนการขายอะไหล่ประจำวัน</h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขใบงาน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รุ่นรถ
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ทะเบียน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขเครื่อง
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เบอร์อะไหล่
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ชื่ออะไหล่
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        จำนวน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ราคาขาย
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ราคาทุน
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        กำไร
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($totalParts as $totalPartss) {
                                        $persen = ($totalPartss['sum_novatcap']>0) ? (($totalPartss['receive']-$totalPartss['sum_novatcap'])*100)/$totalPartss['sum_novatcap'] : 0;
                                        if($persen <= 15) { ?>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.$totalPartss['ref_id'].'</span>'; ?>
                                        </td>
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.($totalPartss['model_name']?$totalPartss['model_name']:'-').'</span>'; ?>
                                        </td>
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.($totalPartss['register']?$totalPartss['register']:'-').'</span>'; ?>
                                        </td>
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.($totalPartss['engin_id'] ? $totalPartss['engin_id'] : '-').'</span>'; ?>
                                        </td>
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.$totalPartss['pw_code'].'</span>'; ?>           
                                        </td>
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.$totalPartss['pw_name'].'</span>'; ?>
                                        </td>
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.$totalPartss['unit'].'</span>'; ?>
                                        </td>
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.Helper::displayDecimal($totalPartss['receive']).'</span>'; ?>
                                        </td>                                        
                                        <td>
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.Helper::displayDecimal($totalPartss['sum_novatcap']).'</span>'; ?>
                                        </td>
                                        <td> 
                                        <?php 
                                        echo '<span style="color:#CC0000;">'.Helper::displayDecimal($persen).'%'.'</span>'; ?>
                                        </td>
                                    </tr> 
                                    <?php }else { ?>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">
                                        <?php echo $totalPartss['ref_id']; ?>
                                        </td>
                                        <td>
                                        <?php echo $totalPartss['model_name'] ? $totalPartss['model_name'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo $totalPartss['register'] ? $totalPartss['register'] : '-' ; ?>
                                        </td>
                                        <td>
                                        <?php echo $totalPartss['engin_id'] ? $totalPartss['engin_id'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo $totalPartss['pw_code']; ?>            
                                        </td>
                                        <td>
                                        <?php echo $totalPartss['pw_name']; ?>
                                        </td>
                                        <td>
                                        <?php echo $totalPartss['unit']; ?>
                                        </td>
                                        <td>
                                        <?php echo Helper::displayDecimal($totalPartss['receive']); ?>
                                        </td>                                        
                                        <td>
                                        <?php echo Helper::displayDecimal($totalPartss['sum_novatcap']); ?>
                                        </td>
                                        <td>                                        
                                        <?php echo Helper::displayDecimal($persen); ?>
                                        </td>
                                    </tr> 
                                    <?php }} ?>                               
                                    </tbody>
                                </table>
                                <?php foreach ($totalParts as $totalPartss) {
                                        $persens[] = ($totalPartss['sum_novatcap']>0) ? (($totalPartss['receive']-$totalPartss['sum_novatcap'])*100)/$totalPartss['sum_novatcap'] : 0;
                                        $sale_price[] = $totalPartss['receive'];
                                        $cost_price[] = $totalPartss['sum_novatcap'];
                                        $piece[] = $totalPartss['unit'];
                                        }?>
                                <h5>ราคาขายรวม&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php echo Helper::displayDecimal(array_sum($sale_price));?>&nbsp;บาท
                                </h5>
                                <h5>ราคาทุนรวม&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php echo Helper::displayDecimal(array_sum($cost_price));?>&nbsp;บาท
                                </h5>
                                <h5>กำไรเบื้องต้น&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php echo Helper::displayDecimal(array_sum($persens));?>&nbsp;%
                                </h5>
                                <h5>จำนวนชิ้นรวม&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php echo (int)array_sum($piece);?>&nbsp;ชิ้น
                                </h5>
                                <h6>สีแดงคืออัตรกำไรต่ำกว่า15%</h6>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
    <?php } ?>

 <?php } ?>  
</div>
<!-- /.box-->
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>