<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "dept_deduct_detail".
 *
 * @property integer $id
 * @property integer $dept_deduct_id
 * @property string $id_card
 * @property integer $is_record_approved
 * @property string $record_approved_datetime
 * @property string $record_approved_by
 * @property integer $add_deduct_template_id
 * @property string $add_deduct_template_name
 * @property string $dept_deduct_amount
 * @property integer $doc_notice_id
 * @property string $doc_notice_code
 * @property string $record_comment
 * @property string $create_by
 * @property string $create_datetime
 * @property string $update_by
 * @property string $update_datetime
 * @property string $pay_date
 */
class Deptdeductdetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dept_deduct_detail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dept_deduct_id', 'is_record_approved', 'add_deduct_template_id', 'doc_notice_id'], 'integer'],
            [['record_approved_datetime', 'create_datetime', 'update_datetime'], 'safe'],
            [['dept_deduct_amount'], 'number'],
            [['id_card', 'record_approved_by', 'create_by', 'update_by'], 'string', 'max' => 13],
            [['add_deduct_template_name'], 'string', 'max' => 250],
            [['doc_notice_code'], 'string', 'max' => 15],
            [['record_comment'], 'string', 'max' => 200],
            [['pay_date'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dept_deduct_id' => 'Dept Deduct ID',
            'id_card' => 'Id Card',
            'is_record_approved' => 'Is Record Approved',
            'record_approved_datetime' => 'Record Approved Datetime',
            'record_approved_by' => 'Record Approved By',
            'add_deduct_template_id' => 'Add Deduct Template ID',
            'add_deduct_template_name' => 'Add Deduct Template Name',
            'dept_deduct_amount' => 'Dept Deduct Amount',
            'doc_notice_id' => 'Doc Notice ID',
            'doc_notice_code' => 'Doc Notice Code',
            'record_comment' => 'Record Comment',
            'create_by' => 'Create By',
            'create_datetime' => 'Create Datetime',
            'update_by' => 'Update By',
            'update_datetime' => 'Update Datetime',
            'pay_date' => 'Pay Date',
        ];
    }
}
