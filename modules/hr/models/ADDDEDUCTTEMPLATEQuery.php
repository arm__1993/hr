<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[AddDeDuctTemplate]].
 *
 * @see AddDeDuctTemplate
 */
class ADDDEDUCTTEMPLATEQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return AddDeDuctTemplate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AddDeDuctTemplate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
