<li class="list-group-item" 
    data-obj-plan='<?php echo json_encode($place->attributes);?>'
    onclick="toggleProgramListActive(this,<?php echo $place->id;?>)">
    <span class="list-menu-name">
        <?php echo $place->name;?>
    </span>
    <div class="pull-right action-buttons list-menu-action">
        <a href="#" onclick="showModelFormPlace(this, 'edit',event)"><span class="glyphicon glyphicon-pencil"></span></a>
        <a href="#" onclick="delPlace('<?php echo $place->id;?>',this,event)" class="trash" ><span class="glyphicon glyphicon-trash"></span></a>
    </div>
</li>