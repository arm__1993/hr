
<?php

/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 13/10/2559
 * Time: 16:30
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use app\api\ApiReport;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    // $("#example1").DataTable({
    //   "lengthChange": false,
    //   "searching": true,
    //   "paging": true,
    //   "info": true,
    //   "pageLength" : 10,
    // }
    // );
    
  });
 
});
JS;


$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานเวลารับแจ้งซ่อมเฉลี่ยต่อคัน รายเดือน</h3>


    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>        
        
        <div class="box-body">

                <div class="row">

                <form method="post" action="receive_avg_repair">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key=>$value) {
                                    echo '<option value="'.$key.'">'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                         <label>เลือกเดือน</label>
                         &nbsp;
                         <div class="btn-group">
                            <select class="form-control" name="startmonth" id="startmonth" style="width: 150px">
                                <option value="">เลือกเดือน</option>
                                    <?php
                                    for ($i=1 ; $i<=12 ; $i++ ){
                                    $sel = ($i==date('m')) ? 'selected="selected"': '';
                                    echo '<option value="'.$i.'" '.$sel.'>'.\app\api\DateTime::convertMonth($i).'</option>';
                                    }
                                    ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                         <label>ถึงเดือน</label>
                         &nbsp;
                         <div class="btn-group">
                            <select class="form-control" name="endmonth" id="endmonth" style="width: 150px">
                                <option value="">เลือกเดือน</option>
                                    <?php
                                    for ($i=1 ; $i<=12 ; $i++ ){
                                    $sel = ($i==date('m')) ? 'selected="selected"': '';
                                    echo '<option value="'.$i.'" '.$sel.'>'.\app\api\DateTime::convertMonth($i).'</option>';
                                    }
                                    ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <label>เลือกปี</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="year[]" id="year">
                                <?php
                                $get_year = ApiReport::getAppYear();
                                foreach ($get_year as $key=>$value) {
                                    echo '<option value="'.$key.'">'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        </center>         

                    <center><br>
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/repair/btn_reportavg">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                    </center>
                    </div>
                    <!--/col-md-12-->
                </form>
            </div>
            <!--/row-->
        </div>
        <!--/box-body-->
<?php if($query) { ?>        
<?php if(empty($groupby)) { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php
                                            echo Highcharts::widget([
                                                'options' => [
                                                    'title' => [
                                                        'text' => ''
                                                    ],
                                                    'xAxis' => [
                                                        'categories' => ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน']
                                                    ],
                                                    'labels' => [
                                                        'items' => [
                                                            'html' => 'Total fruit consumption',
                                                            'style' => [
                                                                'left' => '50px',
                                                                'top' => '18px',
                                                                'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                            ]
                                                        ]
                                                    ],
                                                    'credits' => [
                                                        'enabled' => false
                                                    ],

                                                    'series' => [
                                                        [
                                                            'name' => 'จำนวนคันที่ซ่อมต่อช่าง 1 คน',
                                                            'data' => [12.33, 12.05, 14.23, 15.12, 14.53, 15.25]
                                                        ],

                                                    ]
                                                ]
                                            ]);
                                            ?>
                                    </div>
                                </div><!--row-->
                        </div>
                    </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA CHART -->
            </div>
        </div>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-warning">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                        เดือน
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                        เวลาเฉลี่ยต่อคัน (นาที)
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>                
                                    <tr role="row">
                                        <td>มกราคม</td>
                                        <td>12.33</td>                                    
                                    </tr>
                                    <tr role="row">
                                        <td>กุมภาพันธ์</td>
                                        <td>12.05</td>
                                    </tr>
                                    <tr role="row">
                                        <td>มีนาคม</td>
                                        <td>14.23</td>
                                    </tr>
                                    <tr role="row">
                                        <td>เมษายน</td>
                                        <td>15.12</td>
                                    </tr>
                                    <tr role="row">
                                        <td>พฤษภาคม</td>
                                        <td>14.53</td>
                                    </tr>  
                                    <tr role="row">
                                        <td>มิถุนายน</td>
                                        <td>15.25</td>
                                    </tr>                             
                                </tbody>
                            </table>                                                        
                            </div>
                            <!-- col-sm-12 -->
                        </div>
                        <!-- row -->
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-warning-->
            <!-- AREA TABLE -->
            </div>
        </div>        
    </section>

    <?php } else{ ?>        
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                    <?php foreach ($groupby as $index => $value) {
                                        if($index == 1){
                                            $index = "มกราคม";
                                        }elseif ($index == 2) {
                                            $index = "กุมภาพันธ์";
                                        }elseif ($index == 3) {
                                            $index = "มีนาคม";
                                        }elseif ($index == 4) {
                                            $index = "เมษายน";
                                        }elseif ($index == 5) {
                                            $index = "พฤษภาคม";
                                        }elseif ($index == 6) {
                                            $index = "มิถุนายน";
                                        }elseif ($index == 7) {
                                            $index = "กรกฎาคม";
                                        }elseif ($index == 8) {
                                            $index = "สิงหาคม";
                                        }elseif ($index == 9) {
                                            $index = "กันยายน";
                                        }elseif ($index == 10) {
                                            $index = "ตุลาคม";
                                        }elseif ($index == 11) {
                                            $index = "พฤศจิกายน";
                                        }elseif ($index == 12) {
                                            $index = "ธันวาคม";
                                        }

                                        $month[] = $index;
                                        $minute[] = (count($value)>0) ? (float)(array_sum($value)/count($value)) : 0;

                                     } ?>
                                        <?php
                                            echo Highcharts::widget([
                                                'options' => [
                                                    'title' => [
                                                        'text' => ''
                                                    ],
                                                    'xAxis' => [
                                                        'categories' => $month
                                                    ],
                                                    'labels' => [
                                                        'items' => [
                                                            'html' => 'Total fruit consumption',
                                                            'style' => [
                                                                'left' => '50px',
                                                                'top' => '18px',
                                                                'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                            ]
                                                        ]
                                                    ],

                                                    'series' => [
                                                        [
                                                            'name' => 'จำนวนคันที่ซ่อมต่อช่าง 1 คน',
                                                            'data' => $minute
                                                        ],

                                                    ]
                                                ]
                                            ]);
                                            ?>
                                    </div>
                                </div><!--row-->
                        </div>
                    </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA CHART -->
            </div>
        </div>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-warning">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                        เดือน
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                        เวลาเฉลี่ยต่อคัน (นาที)
                                        </th>
                                    </tr>
                                </thead>

                                <tbody> 
                                    <?php foreach ($groupby as $index => $value) {
                                        if($index == 1){
                                            $index = "มกราคม";
                                        }elseif ($index == 2) {
                                            $index = "กุมภาพันธ์";
                                        }elseif ($index == 3) {
                                            $index = "มีนาคม";
                                        }elseif ($index == 4) {
                                            $index = "เมษายน";
                                        }elseif ($index == 5) {
                                            $index = "พฤษภาคม";
                                        }elseif ($index == 6) {
                                            $index = "มิถุนายน";
                                        }elseif ($index == 7) {
                                            $index = "กรกฎาคม";
                                        }elseif ($index == 8) {
                                            $index = "สิงหาคม";
                                        }elseif ($index == 9) {
                                            $index = "กันยายน";
                                        }elseif ($index == 10) {
                                            $index = "ตุลาคม";
                                        }elseif ($index == 11) {
                                            $index = "พฤศจิกายน";
                                        }elseif ($index == 12) {
                                            $index = "ธันวาคม";
                                        }
                                     ?>               
                                    <tr role="row">
                                        <td><?php echo $index; ?></td>
                                        <td>
                                        <?php echo (count($value)>0) ? Helper::displayDecimal(array_sum($value)/count($value)) : 0;?>             
                                        </td>                                    
                                    </tr> 
                                    <?php } ?>                            
                                </tbody>
                            </table>                                                        
                            </div>
                            <!-- col-sm-12 -->
                        </div>
                        <!-- row -->
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-warning-->
            <!-- AREA TABLE -->
            </div>
        </div>        
    </section>
    <?php } ?>
<?php } ?>   
</div><!--box-->
 <?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>