<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "V_Emp_Adddeductdetail".
 *
 * @property integer $IdEmp
 * @property string $IdCard
 * @property string $EmpPosition
 * @property string $EmpSection
 * @property string $NameSection
 * @property string $EmpDepartment
 * @property string $NameDepartment
 * @property integer $Add_Deductdetail_Id
 * @property integer $Ref_Idtemple
 * @property string $Add_Deductdetail_Detail
 * @property string $Add_Deductdetail_Paydate
 * @property string $Add_Deductdetail_Startdate
 * @property string $Add_Deductdetail_Enddate
 * @property string $Add_Deductdetail_Idemp
 * @property string $Add_Deductdetail_Amount
 * @property integer $Add_Deductdetail_Type
 * @property integer $Add_Deductdetail_Status
 * @property string $Add_Deductdetail_Createdate
 * @property string $Add_Deductdetail_By
 * @property integer $Add_Deducttemplate_Id
 * @property string $Add_Deducttemplate_Name
 * @property string $Add_Deducttemplate_Type
 */
class Vempadddeductdetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'V_Emp_Adddeductdetail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdEmp', 'Add_Deductdetail_Id', 'Ref_Idtemple', 'Add_Deductdetail_Type', 'Add_Deductdetail_Status', 'Add_Deducttemplate_Id'], 'integer'],
            [['IdCard', 'NameSection', 'NameDepartment', 'Ref_Idtemple', 'Add_Deductdetail_Detail', 'Add_Deductdetail_Paydate', 'Add_Deductdetail_Startdate', 'Add_Deductdetail_Enddate', 'Add_Deductdetail_Idemp', 'Add_Deductdetail_Amount', 'Add_Deductdetail_Type', 'Add_Deductdetail_Status', 'Add_Deductdetail_Createdate', 'Add_Deductdetail_By', 'Add_Deducttemplate_Name', 'Add_Deducttemplate_Type'], 'required'],
            [['Add_Deductdetail_Startdate', 'Add_Deductdetail_Enddate', 'Add_Deductdetail_Createdate'], 'safe'],
            [['Add_Deductdetail_Amount'], 'number'],
            [['IdCard'], 'string', 'max' => 14],
            [['EmpPosition', 'EmpSection', 'EmpDepartment', 'Add_Deductdetail_By'], 'string', 'max' => 100],
            [['NameSection', 'NameDepartment', 'Add_Deductdetail_Detail', 'Add_Deducttemplate_Name'], 'string', 'max' => 250],
            [['Add_Deductdetail_Paydate'], 'string', 'max' => 7],
            [['Add_Deductdetail_Idemp'], 'string', 'max' => 13],
            [['Add_Deducttemplate_Type'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdEmp' => 'Id Emp',
            'IdCard' => 'Id Card',
            'EmpPosition' => 'Emp Position',
            'EmpSection' => 'Emp Section',
            'NameSection' => 'Name Section',
            'EmpDepartment' => 'Emp Department',
            'NameDepartment' => 'Name Department',
            'Add_Deductdetail_Id' => 'Add  Deductdetail  ID',
            'Ref_Idtemple' => 'Ref  Idtemple',
            'Add_Deductdetail_Detail' => 'Add  Deductdetail  Detail',
            'Add_Deductdetail_Paydate' => 'Add  Deductdetail  Paydate',
            'Add_Deductdetail_Startdate' => 'Add  Deductdetail  Startdate',
            'Add_Deductdetail_Enddate' => 'Add  Deductdetail  Enddate',
            'Add_Deductdetail_Idemp' => 'Add  Deductdetail  Idemp',
            'Add_Deductdetail_Amount' => 'Add  Deductdetail  Amount',
            'Add_Deductdetail_Type' => 'Add  Deductdetail  Type',
            'Add_Deductdetail_Status' => 'Add  Deductdetail  Status',
            'Add_Deductdetail_Createdate' => 'Add  Deductdetail  Createdate',
            'Add_Deductdetail_By' => 'Add  Deductdetail  By',
            'Add_Deducttemplate_Id' => 'Add  Deducttemplate  ID',
            'Add_Deducttemplate_Name' => 'Add  Deducttemplate  Name',
            'Add_Deducttemplate_Type' => 'Add  Deducttemplate  Type',
        ];
    }
}
