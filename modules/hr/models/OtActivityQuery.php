<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[OtActivity]].
 *
 * @see OtActivity
 */
class OtActivityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OtActivity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OtActivity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
