<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 4/4/2018 AD
 * Time: 08:19
 */

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>
                    ออกรายงาน
                </li>
                <li class="active">ดาวน์โหลดใบเซ็นต์ชื่อ</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            Content Here
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
