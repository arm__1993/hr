<div class="col-sm-12" style="padding:5px;border: 1px solid #78cc78;">
                        <div class="nav-tabs-custom tab-filter" >
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#fill_pinf_tab_1" data-toggle="tab">
                                    ข้อมูลบุคคล
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_pinf_tab_2" data-toggle="tab">
                                    ที่อยู่
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_pinf_tab_3" data-toggle="tab">
                                    ครอบครัว
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_pinf_tab_4" data-toggle="tab">
                                    ลดหย่อนภาษี
                                    </a>
                                </li>
                                <li>
                                    <a href="#fill_pinf_tab_5" data-toggle="tab">
                                    การศึกษา
                                    </a>
                                </li>
                               
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="fill_pinf_tab_1">
                                    <div class="box-body">
                                        <div id="contend-personal-inf">
                                            <div class="row col-sm-12">
                                                <label class="control-label" style="text-align: left;">
                                                    <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_pinf_tab_1 form')" > เลือกทั้งหมด
                                                </label>    
                                            </div>
                                            <form class="form-horizontal">
                                                <div class="col-sm-4">


                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>" id="Pictures_HyperL" name="Pictures_HyperL" data-mtitle="รูปพนักงาน" > รูปพนักงาน
                                                        </label>
                                                    </div>
                                                   <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Code" name="Code" data-mtitle="รหัสพนักงาน" > รหัสพนักงาน
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="TIS_id" name="TIS_id" data-mtitle="รหัสพนักงานตรีเพชร" > รหัสพนักงานตรีเพชร
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Sex" name="Sex" data-mtitle="เพศ" > เพศ
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Be" name="Be" data-mtitle="คำนำหน้า"  > คำนำหน้าชื่อ
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Name" name="Name" data-mtitle="ชื่อ" > ชื่อ
                                                        </label>
                                                    </div>
                                                     <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Surname" name="Surname" data-mtitle="สกุล" > สกุล
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="BenameEN" name="BenameEN" data-mtitle="คำนำหน้าชื่อ  [Eng]"  > คำนำหน้าชื่อ [Eng]
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="nameEmpEN" name="nameEmpEN" data-mtitle="ชื่อ  [Eng]" > ชื่อ [Eng]
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="nickNameEmpEN" name="nickNameEmpEN"data-mtitle="ชื่อเล่น"  > ชื่อเล่น [Eng]
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Birthday" name="Birthday" data-mtitle="น เดือน ปี เกิด" > วัน เดือน ปี เกิด
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-special="1" id="" name="age" data-mtitle="อายุ" > อายุ
                                                        </label>
                                                    </div>
                                                   

                                                </div>

                                                 <div class="col-sm-6">


                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="race" name="race" data-mtitle="เชื้อชาติ" > เชื้อชาติ
                                                        </label>
                                                    </div>
                                                   <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Nationality" name="Nationality" data-mtitle="สัญชาติ"  > สัญชาติ
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Religion" name="Religion" data-mtitle="ศาสนา"  > ศาสนา
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Weight" name="Weight" data-mtitle="น้ำหนัก"  > น้ำหนัก
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Height" name="Height" data-mtitle="ส่วนสูง"  > ส่วนสูง
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Blood_G" name="Blood_G" data-mtitle="กลุ่มเลือด"  > กลุ่มเลือด
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="diseaseDetail" name="diseaseDetail" data-mtitle="โรคประจำตัว"  > โรคประจำตัว
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="ID_Card" name="ID_Card" data-mtitle="หมายเลขบัตรประชาชน"  > หมายเลขบัตรประชาชน
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="numberPassportEmp" name="numberPassportEmp" data-mtitle="หนังสือเดินทางเลขที่"  > หนังสือเดินทางเลขที่
                                                        </label>
                                                    </div>


                                                </div>
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_pinf_tab_2">
                                    <div class="box-body">
                                         <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_pinf_tab_2 form')" > เลือกทั้งหมด
                                            </label>    
                                         </div>
                                         <form class="form-horizontal">
                                                <div class="col-sm-4">


                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-special="1" id="addr1" name="addr1" data-mtitle="ที่อยู่ที่สามารถติดต่อได้" > ที่อยู่ที่สามารถติดต่อได้
                                                        </label>
                                                    </div>
                                                   <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-special="1"  id="ADDR_MAP_DESC" name="ADDR_MAP_DESC" data-mtitle="รูปบ้าน" > รูปบ้าน
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-special="1"  id="addr2" name="addr2" data-mtitle="ที่อยู่ตามบัตรประชาชน" > ที่อยู่ตามบัตรประชาชน
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-special="1"  id="addr3" name="addr3" data-mtitle="ที่อยู่ตามทะเบียนบ้าน" > ที่อยู่ตามทะเบียนบ้าน
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Live_with" name="Live_with" data-mtitle="การพักอาศัย" > การพักอาศัย
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Resident_type" name="Resident_type" data-mtitle="ประเภทที่พัก" > ประเภทที่พัก
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Tel_Num" name="Tel_Num" data-mtitle="เบอร์โทร" > เบอร์โทร
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                            <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="E_Mail" name="E_Mail" data-mtitle="E-mail" > E-mail
                                                        </label>
                                                    </div>
                                                   
                                                   

                                                </div>

                                               
                                               
                                            </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_pinf_tab_3">
                                    <div class="box-body">
                                        
                                          <div class="col-sm-12" style="padding:5px">
                                            <div class="nav-tabs-custom tab-filter" >
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#fill_family_1" data-toggle="tab">
                                                        ข้อมูลบิดา-มารดา
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#fill_family_2" data-toggle="tab">
                                                        ข้อมูลผู้ติดต่อฉุกเฉิน
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#fill_family_3" data-toggle="tab">
                                                        ข้อมูลการสมรส
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#fill_family_4" data-toggle="tab">
                                                        ข้อมูลบุตร
                                                        </a>
                                                    </li>
                                                   
                                                </ul>

                                                <div id="border-emp-filter-field" class="tab-content">
                                                    <div class="tab-pane active" id="fill_family_1">
                                                        <div class="box-body">
                                                            <div class="row col-sm-12">
                                                                <label class="control-label" style="text-align: left;">
                                                                    <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_family_1 form')" > เลือกทั้งหมด
                                                                </label>    
                                                            </div>
                                                            <form class="form-horizontal">
                                                                <div class="col-sm-5">

                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="parent_status" name="parent_status" data-mtitle="สถานะบิดา-มารดา" > สถานะบิดา-มารดา
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>" id="father_tax_status" name="father_tax_status" data-mtitle="สิทธิลดหย่อนบิดา" > สิทธิลดหย่อนบิดา
                                                                        </label>
                                                                    </div>
                                                                   
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="father_be" name="father_be" data-mtitle="คำนำหน้า" > คำนำหน้า
                                                                        </label>
                                                                    </div>
                                                                   
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="father_name" name="father_name" data-mtitle="ชื่อ-สกุล" > ชื่อ-สกุล
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="father_idcard" name="father_idcard" data-mtitle="หมายเลขบัตรประชาชน" > หมายเลขบัตรประชาชน
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="father_birthday" name="father_birthday" data-mtitle="วัน เดือน ปีเกิด" > วัน เดือน ปีเกิด
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-special="1"  id="" name="father_age" data-mtitle="อายุบิดา" > อายุ
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>" id="father_insurance" name="father_insurance" data-mtitle="เบี้ยประกันสุขภาพ" > เบี้ยประกันสุขภาพ
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>" id="father_business" name="father_business" data-mtitle="อาชีพ" > อาชีพ
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>" id="father_place" name="father_place" data-mtitle="ที่อยู่ที่ทำงาน" > ที่อยู่ที่ทำงาน
                                                                        </label>
                                                                    </div>
                                                                     <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>" id="father_phone" name="father_phone" data-mtitle="เบอร์โทร" > เบอร์โทร
                                                                        </label>
                                                                    </div>
                                                                

                                                                </div>

                                                                <div class="col-sm-6">


                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_tax_status" name="mother_tax_status" data-mtitle="สิทธิลดหย่อนมาดา" > สิทธิลดหย่อนมาดา
                                                                        </label>
                                                                    </div>
                                                                
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_be" name="mother_be" data-mtitle="คำนำหน้า" > คำนำหน้า
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_name" name="mother_name" data-mtitle="ชื่อ-สกุล" > ชื่อ-สกุล
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_idcard" name="mother_idcard" data-mtitle="หมายเลขบัตรประชาชน" > หมายเลขบัตรประชาชน
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_birthday" name="mother_birthday" data-mtitle="วัน เดือน ปีเกิด" > วัน เดือน ปีเกิด
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-special="1"  id="" name="mother_age" data-mtitle="อายุมารดา" > อายุ
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_insurance" name="mother_insurance" data-mtitle="เบี้ยประกันสุขภาพ" > เบี้ยประกันสุขภาพ
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_business" name="mother_business" data-mtitle="อาชีพ" > อาชีพ
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_place" name="mother_place" data-mtitle="ที่อยู่ที่ทำงาน" > ที่อยู่ที่ทำงาน
                                                                        </label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-6" style="text-align: left;">
                                                                            <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="mother_phone" name="mother_phone" data-mtitle="เบอร์โทร" > เบอร์โทร
                                                                        </label>
                                                                    </div>
                                                                   


                                                                </div>
                                                            
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="fill_family_2">
                                                        <div class="box-body">
                                                             <div class="row col-sm-12">
                                                                <label class="control-label" style="text-align: left;">
                                                                    <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_family_2 form')" > เลือกทั้งหมด
                                                                </label>    
                                                            </div>
                                                            <form class="form-horizontal">
                                                                <div class="col-sm-12">
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="relative_be" name="relative_be" data-mtitle="คำนำหน้า" > คำนำหน้า
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="relative_sex" name="relative_sex" data-mtitle="เพศ" > เพศ
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="relative_name" name="relative_name" data-mtitle="ชื่อ" > ชื่อ
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="relative_surname" name="relative_surname" data-mtitle="สกุล" > สกุล
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="relative_status" name="relative_status" data-mtitle="ความสัมพันธ์" > ความสัมพันธ์
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="relative_phone" name="relative_phone" data-mtitle="เบอร์โทร" > เบอร์โทร
                                                                    </label>
                                                               </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="fill_family_3">
                                                        <div class="box-body">
                                                            <div class="row col-sm-12">
                                                                <label class="control-label" style="text-align: left;">
                                                                    <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_family_3 form')" > เลือกทั้งหมด
                                                                </label>    
                                                            </div>
                                                            <form class="form-horizontal">
                                                                <div class="col-sm-12">
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_status" name="spouse_status" data-mtitle="สถานะภาพ" > สถานะภาพ
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_be" name="spouse_be" data-mtitle="คำนำหน้า" > คำนำหน้า
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_sex" name="spouse_sex" data-mtitle="เพศ" > เพศ
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_name" name="spouse_name" data-mtitle="ชื่อ" > ชื่อ
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_surname" name="spouse_surname" data-mtitle="สกุล" > สกุล
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_idcard" name="spouse_idcard" data-mtitle="หมายเลขบัตรประชาชน" > หมายเลขบัตรประชาชน
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_birthday" name="spouse_birthday" data-mtitle="วัน เดือน ปีเกิด" > วัน เดือน ปีเกิด
                                                                    </label>
                                                                     <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-special="1"  id="" name="spouse_age" data-mtitle="อายุคู่สมรส" > อายุ
                                                                    </label>
                                                                     <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_insurance" name="spouse_insurance" data-mtitle="เบี้ยประกันสุขภาพ" > เบี้ยประกันสุขภาพ
                                                                    </label>
                                                                     <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_business" name="spouse_business" data-mtitle="อาชีพ" > อาชีพ
                                                                    </label>
                                                                     <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_place" name="spouse_place" data-mtitle="ที่อยู่ที่ทำงาน" > ที่อยู่ที่ทำงาน
                                                                    </label>
                                                                    <label class="control-label col-sm-6" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_family;?>"  id="spouse_phone" name="spouse_phone" data-mtitle="เบอร์โทร" > เบอร์โทร
                                                                    </label>

                                                               </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="fill_family_4">
                                                        <div class="box-body">
                                                            <div class="row col-sm-12">
                                                                <label class="control-label" style="text-align: left;">
                                                                    <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_family_4 form')" > เลือกทั้งหมด
                                                                </label>    
                                                            </div>
                                                            <form class="form-horizontal">
                                                                <div class="col-sm-4">
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $emp_data;?>"  id="Childs" name="Childs" data-mtitle="สถานะบุตร" > สถานะบุตร
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox"  data-special="1"  id="" name="count_child_boy" data-mtitle="จำนวนบุตรชาย" > จำนวนบุตรชาย
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-special="1"  id="" name="count_child_girl" data-mtitle="จำนวนบุตรหญิง" > จำนวนบุตรหญิง
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="education_status" name="education_status" data-mtitle="สถานะทางการศึกษา" > สถานะทางการศึกษา
                                                                    </label>
                            
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="prefix" name="prefix" data-mtitle="คำนำหน้าบุตร" > คำนำหน้า
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="sex" name="sex" data-mtitle="เพศบุตร" > เพศ
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="name" name="name" data-mtitle="ชื่อบุตร" > ชื่อ
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="surname" name="surname" data-mtitle="สกุลบุตร" > สกุล
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="school" name="school" data-mtitle="สถานศึกษาบุตร" > สถานศึกษา
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="class" name="class" data-mtitle="บุระดับการศึกษาตร" > ระดับการศึกษา
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="school" name="school" data-mtitle="ชั้นปีที่กำลังศึกษาบุตร" > ชั้นปีที่กำลังศึกษา
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="birthday" name="birthday" data-mtitle="วัน เดือน ปีเกิด บุตร" > วัน เดือน ปีเกิด
                                                                    </label>
                                                                    <label class="control-label col-sm-12" style="text-align: left;">
                                                                        <input type="checkbox" data-tb="<?php echo $children;?>"  id="age" name="age" data-mtitle="อายุบุตร" > อายุ
                                                                    </label>
                                
                                                                </div>
                                                            </form>
                                                          
                                                        </div>
                                                    </div>
                                                   
                                                    
                                                </div>


                                            </div>
                                        </div>   



                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_pinf_tab_4">
                                    <div class="box-body">
                                        <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_pinf_tab_4 form')" > เลือกทั้งหมด
                                            </label>    
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="col-sm-4">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_personal" name="tax_personal" data-mtitle="เบี้ยประกันชีวิตตัวเอง" > เบี้ยประกันชีวิตตัวเอง
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_keepback" name="tax_keepback" data-mtitle="เงินสะสมกองทุนสำรองเลี้ยงชีพ" > เงินสะสมกองทุนสำรองเลี้ยงชีพ
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_rating" name="tax_rating" data-mtitle="เงินสะสม กบข." > เงินสะสม กบข.
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_teachers" name="tax_teachers" data-mtitle="เงินสะสมกองทุนสงเคราะห์ครูโรงเรียนเอกชน" > เงินสะสมกองทุนสงเคราะห์ครูโรงเรียนเอกชน
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_rmf" name="tax_rmf" data-mtitle="กองทุนเพื่อการเลี้ยงชีพ (RMF)" > กองทุนเพื่อการเลี้ยงชีพ (RMF)
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_ltf" name="tax_ltf" data-mtitle="กองทุนรวมหุ้นระยะยาว (LTF)" > กองทุนรวมหุ้นระยะยาว (LTF)
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_social" name="tax_social" data-mtitle="กองทุนประกันสังคม" > กองทุนประกันสังคม
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_education" name="tax_education" data-mtitle="เงินสนับสนุนเพื่อการศึกษา" > เงินสนับสนุนเพื่อการศึกษา
                                                </label>
                                               
        
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_income_spouse_status" name="tax_income_spouse_status" data-mtitle="คู่สมรสมีเงินได้" > คู่สมรสมีเงินได้
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_income_with_spouse_status" name="tax_income_with_spouse_status" data-mtitle="การยื่นแบบภาษี" > การยื่นแบบภาษี
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_income_spouse" name="tax_income_spouse" data-mtitle="รายได้คู่สมรส" > รายได้คู่สมรส
                                                </label>
                                                 <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $tax_income;?>"  id="tax_insurance_spouse" name="tax_insurance_spouse" data-mtitle="เบี้ยประกันชีวิตคู่สมรส" > เบี้ยประกันชีวิตคู่สมรส
                                                </label>
                                               
            
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fill_pinf_tab_5">
                                    <div class="box-body">
                                        <div class="row col-sm-12">
                                            <label class="control-label" style="text-align: left;">
                                                <input type="checkbox" id="" name="" onclick="filterToggleCheck(this,'#fill_pinf_tab_5 form')" > เลือกทั้งหมด
                                            </label>    
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="col-sm-12">
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $education;?>"  id="edu_level" name="edu_level" data-mtitle="ระดับการศึกษา" > ระดับการศึกษา
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $education;?>"  id="edu_school" name="edu_school" data-mtitle="สถานศึกษา" > สถานศึกษา
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $education;?>"  id="edu_major" name="edu_major" data-mtitle="สาขาวิชา" > สาขาวิชา
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $education;?>"  id="edu_finish" name="edu_finish" data-mtitle="จบ พ.ศ." > จบ พ.ศ.
                                                </label>
                                                <label class="control-label col-sm-12" style="text-align: left;">
                                                    <input type="checkbox" data-tb="<?php echo $education;?>"  id="edu_GPA" name="edu_GPA" data-mtitle="เกรดเฉลี่ย" > เกรดเฉลี่ย
                                                </label>
                                           
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                               
                            </div>


                        </div>
                    </div>