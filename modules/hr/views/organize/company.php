<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:24
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\modules\hr\apihr\ApiHr;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/jsValidator/bootstrapValidator.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/OU/company.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::className()]]);

// $this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/OU/bootstrapValidator.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);

$company = ApiHr::getWorking_company();

$script = <<< JS
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
    });
    $("[data-mask]").inputmask();
JS;
$this->registerJs($script);
?>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดผังองค์กร</a>
                </li>
                <li class="active">ข้อมูลบริษัท</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div>
                <!--<div >
                    <h4 class="box-title">ข้อมูลบริษัท </h4>
                </div>-->
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-10">

                                </div>
                                <div class="col-md-2">
                                    <a data-toggle="modal" onclick="reset()"><img src="<?php echo $imghr; ?>/add.png"
                                                                                  class="img-circle">
                                        <span>เพิ่มบริษัท</span></a>
                                </div>
                            </div>
                            <?php {
                                Pjax::begin(['id' => 'pjax_grid_company']);
                                echo GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $modelSearch,
                                    'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn',
                                            'headerOptions' => ['width' => '20'],
                                        ],
                                        [
                                            'attribute' => 'code_name',
                                            'label' => 'รหัสบริษัท',
                                            'value' => 'code_name',
                                            //'format' => 'raw',
                                            //'filter' => true,
                                            'contentOptions' => ['style' => 'width: 50px;', 'align=center']
                                        ],
                                        [
                                            'attribute' => 'name',
                                            'label' => 'ชื่อเต็มบริษัท',
                                            'value' => 'name',
                                            //'format' => 'raw',
                                            //'filter' => true,
                                            'contentOptions' => ['style' => 'width: 250px;', 'align=center']
                                        ],
                                        [
                                            'attribute' => 'short_name',
                                            'label' => 'ชื่อย่อ',
                                            'value' => 'short_name',
                                            //'format' => 'raw',
                                            //'filter' => true,
                                            'contentOptions' => ['style' => 'width: 120px;', 'align=center']
                                        ],
                                        // [
                                        // 'attribute' => 'name',
                                        // 'label' => 'จัดการข้อมูลบริษัท',
                                        // 'value' => 'name',
                                        // //'format' => 'raw',
                                        // //'filter' => true,
                                        // 'contentOptions' => ['style' => 'width: 120px;','align=center']
                                        // ],
                                        [
                                            'headerOptions' => ['width' => '100'],
                                            'class' => 'yii\grid\ActionColumn',
                                            'header' => 'จัดการข้อมูลบริษัท',
                                            'template' => '{update}{delete}',
                                            'buttons' => [
                                                'update' => function ($url, $data) use ($companyUse) {
                                                    $editall = (!in_array($data->id, $companyUse)) ? 1 : 0;

                                                    return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                        'title' => 'แก้ไข',
                                                        'onclick' => '(function($event) {
                                                                            editCompanyByid_get(' . $data->id . ',' . $editall . ');
                                                                    })();'
                                                    ]);

                                                },

                                                'delete' => function ($url, $data) use ($companyUse) {
                                                    if (!in_array($data->id, $companyUse)) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                            'title' => 'ลบ',
                                                            'onclick' => '(function($event) {
                                                                            deleteCompanyByid(' . $data->id . ',/' . $data->name . '/' . ');
                                                                })();'
                                                        ]);
                                                    }
                                                },
                                            ],
                                        ],
                                        [
                                            'headerOptions' => ['width' => '100'],
                                            'class' => 'yii\grid\ActionColumn',
                                            'header' => 'จัดการข้อมูลแผนก',
                                            'template' => '{update}',
                                            'buttons' => [
                                                'update' => function ($url, $data) {
                                                    $urls = Url::to([
                                                        'organize/department',
                                                        'id' => $data->id
                                                    ]);
                                                    return Html::button('ข้อมูลแผนก', $options = [
                                                        'onclick' => '(function($event) {
                                                                    window.location.href="' . $urls . '";
                                                                })();'

                                                    ]);
                                                },
                                            ],
                                        ],


                                    ],
                                ]);
                                Pjax::end(); //end pjax_gridcorclub
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.box -->
</section><!-- /.content -->
<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div style="width: 1900px" class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ยืนยันการบันทึก</h5>
            </div>
            <div class="modal-body">
                <form id="companyForm" onsubmit="return getdatesubmit();" data-toggle="validator" method="post"
                      class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>"
                                   value="<?php echo Yii::$app->request->csrfToken; ?>"/>
                            <input value="0" type="hidden" id="id" name="id" disable/>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">ชื่อบริษัท<i style"color:red">*</i></label>
                                <div class="col-xs-5">
                                    <input type="text" id="name" class="form-control" name="name" required/>
                                </div>
                                <div class="col-xs-4">
                                    <label class="control-label">ตัวอย่าง อีซูซุเชียงราย</label>
                                </div>
                            </div>
                            <input value="0" type="hidden" id="check" name="check"/>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">ชื่อบริษัท [Eng]<i style"color:red">*</i></label>
                                <div class="col-xs-5">
                                    <input type="text" id="name_eng" class="form-control" name="name_eng" required/>
                                </div>
                                <div class="col-xs-4">
                                    <label class="control-label" style="text-align:left">ตัวอย่าง ISUZU CHAIANGRAI
                                        CO.,LTD.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">ชื่อย่อบริษัท<i style"color:red">*</i></label>
                                <div class="col-xs-5">
                                    <input type="text" id="short_name" class="form-control"  maxlength="20" name="short_name" required/>
                                </div>
                                <div class="col-xs-4">
                                    <label class="control-label" style="text-align:left">ตัวอย่าง IC
                                        ใส่เป็นตัวภาษาอังกฤษและตัวเลขเท่านั้น</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">รหัสบริษัท<i style"color:red">*</i></label>
                                <div class="col-xs-5">
                                    <input type="text" id="code_name" class="form-control" maxlength="2"
                                           onkeypress="return onlyAlphabets(event,this);" name="code_name" required/>
                                </div>
                                <div class="col-xs-4">
                                    <label class="control-label" style="text-align:left">ตัวอย่าง
                                        ใส่เป็นตัวอังกฤษเท่านั้น</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">อักษรนำหน้าใบกำกับภาษี</label>
                                <div class="col-xs-5">
                                    <input type="text" id="codeName_tax" class="form-control" maxlength="2"
                                           onkeypress="return onlyAlphabets(event,this);" name="codeName_tax"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">หมายเลขทะเบียนการค้า</label>
                                <div class="col-xs-5">
                                    <input type="text" id="business_number" class="form-control" maxlength="50"
                                           name="business_number"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">เลขที่สาขา(จากระบบiosของตรีเพชร)</label>
                                <div class="col-xs-5">
                                    <input type="text" id="numberbranch" class="form-control" maxlength="10" name="numberbranch"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">รหัสสาขาที่ใช้กับservice</label>
                                <div class="col-xs-5">
                                    <input type="text" id="Code" class="form-control" maxlength="20" name="Code"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">รหัสสาขาที่จะเอาไปโชว์ต่อท้าย</label>
                                <div class="col-xs-5">
                                    <input type="text" id="branch_number" class="form-control" name="branch_number"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">รหัสสาขาที่จะเอาไปโชว์ต่อท้าย</label>
                                <div class="col-xs-5">
                                    <input type="text" id="branch_number" class="form-control" name="branch_number"/>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">คอมเม้นชื่อสาขาที่จะเอาไปโชว์ต่อท้าย</label>
                                <div class="col-xs-5">
                                    <input type="text" id="branch_comment" class="form-control" name="branch_comment"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">รหัสพนักงานขายประจำสาขา</label>
                                <div class="col-xs-5">
                                    <input type="text" id="branch_salesman_cd" class="form-control"
                                           maxlength="10"  name="branch_salesman_cd"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">ชื่อสาขาจากทางตรีเพชร</label>
                                <div class="col-xs-5">
                                    <input type="text" id="BranchName_TIS" class="form-control" name="BranchName_TIS"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label" >ที่อยู่บริษัท <i style"color:red">*</i> </label>
                                <div class="col-xs-5">
                                    <input type="text" id="address" class="form-control" name="address" required/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">เบอร์โทร <i style"color:red">*</i></label>
                                <div class="col-xs-5">
                                    <input type="text" id="Tel" class="form-control" name="Tel"
                                           data-inputmask="'mask': ['099-999-999']" data-mask  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Fax <i style"color:red">*</i> </label>
                                <div class="col-xs-5">
                                    <input type="text" id="Fax" name="Fax" class="form-control"
                                           data-inputmask="'mask': ['999-999-999']" data-mask required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">เลขประจำตัวผู้เสียภาษี <i style"color:red">*</i> </label>
                                <div class="col-xs-5">
                                    <input type="text" class="form-control" id="inv_number" name="inv_number"
                                           data-inputmask="'mask': ['9-9999-99999-9-99']" data-mask required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">เลขบัญชีประกันสังคม<i style"color:red">*</i> </label>
                                <div class="col-xs-5">
                                    <input type="text" class="form-control" id="soc_acc_number" name="soc_acc_number"
                                           data-inputmask="'mask': ['99-9999999-9']" data-mask required></data-mask>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">ประเภทบริษัท</label>
                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="company_type" id="TypeCompany1" value="1"
                                                       checked="">
                                                สำนักงานใหญ่
                                            </label>
                                            <label>
                                                <input type="radio" name="company_type" id="TypeCompany2" value="2">
                                                สาขา
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">dealercode <i style"color:red">*</i> </label>
                                <div class="col-xs-5">
                                    <input type="text" id="Code" class="form-control" name="dealercode" required/>
                                </div>
                            </div>

                        </div>

                        <!-- The form is placed inside the body of modal -->
                    </div>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-10">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
