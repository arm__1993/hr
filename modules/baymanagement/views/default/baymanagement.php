<?php
/**
 * Created by PhpStorm.
 * User: watcharaphan
 * Date: 31/5/2018 AD
 * Time: 09:06
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\modules\hr\apihr\ApiHr;
use yii\helpers\Url;
use app\modules\baymanagement\models\EholeTake;
use app\modules\baymanagement\models\EholeTakeLevel;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use app\api\Utility;
use app\modules\baymanagement\models\ErepairGroup;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/eholejob.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); // level
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/baymanagement/armbox.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/armboxjs.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //Layout js  --re
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/baylayout.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //addLayout  --re
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/levelmaster.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //levelmaster
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/baymanagement/technician.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);  //edit  here  ?t='. time()   --re


$today = date('d/m/Y');


/*echo '<pre>';
print_r($showeholetakeindex);
echo '</pre>';*/


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">จัดการแผนที่โรงซ่อม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#Layout" data-toggle="tab">จัดการแผนที่โรงซ่อม</a></li>
                        <li><a href="#addLayout" data-toggle="tab">จัดการรูปโรงซ่อม </a></li>
                        <li><a href="#add_technician" data-toggle="tab">Add Technician</a></li>
                        <li><a href="#add_level" data-toggle="tab">Add level</a></li>
                        <li><a href="#levelmaster" data-toggle="tab">Add levelmaster</a></li>
                    <div class="tab-content">

                        <div class="tab-pane active" id="Layout">
                            <!--modal start-->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10"></div>
                                    <div class="col-md-2">


                                        <button type="button" class="btn btn-success" data-dismiss="modal" id="btn_saveme"
                                                onclick="save_me_plz();">บันทึกตำแหน่งทั้งหมด
                                        </button>


                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">เพิ่ม
                                        </button>

                                        <div class="container">
                                            <div class="modal fade" id="myModal" role="dialog">
                                                <div class="modal-dialog">

                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>


                                                            <h4 class="modal-title">เพิ่มลิฟท์</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>กรุณากรอกข้อมูล</p>


                                                            <div class="box-body form-group">
                                                                <label>ชื่อลิฟท์ <span>*</span></label>
                                                                <input id="name" name="name" required
                                                                       data-required="true"
                                                                       class="form-control model_name validate[required] text-input"
                                                                       type="text"
                                                                       placeholder="กรุณาเพิ่มชื่อลิฟท์" value="">
                                                            </div>


                                                            <div class="box-body">
                                                                <label>ประเภทรถ <span>*</span></label>

                                                                <select name="dorpdowe" id="dorpdowe"
                                                                        onchange="this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor">
                                                                    <option style="background-color:#F0CA4D" value="#F0CA4D">
                                                                        รถใหญ่
                                                                    </option>
                                                                    <option style="background-color:#46B29D" value="#46B29D">
                                                                        รถกลาง
                                                                    </option>
                                                                    <option style="background-color: #E37B40;" value="#E37B40">
                                                                        รถเล็ก
                                                                    </option>
                                                                    <option style="background-color: #B8E986;" value="#B8E986">
                                                                        สำรอง
                                                                    </option>

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-primary" type="submit" id="btn1">เพิ่ม</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                                ยกเลิก
                                                            </button>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <div id="wrapper" style="height: 630px; width: 960px;">
                                            <div id="armaddbox"></div>
                                            <img src="<?php echo Yii::$app->request->baseUrl . '/upload/baymanagement/' . $imgttr['background_image']; ?>"
                                                 width="960" HEIGHT="630">
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>   <!--จัดการแผนที่โรงซ่อม-->



                        <div class="tab-pane" id="addLayout">
                            <!--modal start-->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-10"></div>


                                        <form id="form1" runat="server">
                                            <div class="col-md-8"></div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-success" data-dismiss="modal" id="btn_saveme"
                                                        onclick="Photo_Storage();">บันทึกรูปภาพ
                                                </button>
                                            </div>
                                            <div class="col-md-2">
                                                <input type='file' id="imgInp">

                                            </div>

                                            <div class="col-md-4"></div>
                                            <div class="col-md-7">
                                                <div id='img_contain'>

                                                    <img src="<?php

                                                    echo Yii::$app->request->baseUrl . '/upload/baymanagement/' . $imgttr['background_image'];


                                                    ?>" width="200" HEIGHT="200"


                                                         id="blah"
                                                         align='middle'
                                                         title=''/>
                                                </div>
                                            </div>


                                        </form>


                                        <!-- <img src="<?php /*echo Yii::$app->request->baseUrl . '/upload/baymanagement/789712482.jpg'; */ ?>"
                         width="200" HEIGHT="200">-->
                                    </div>
                                </div>
                            </div>
                        </div> <!--จัดการรูปโรงซ่อม-->


                        <div class="tab-pane" id="add_technician">
                            <!--modal start-->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">


                                        <!--modal start-->
                                        <div class="row">
                                            <div class="col-md-11"></div>

                                            <div class="col-md-1">
                                                <?php
                                                Modal::begin([
                                                    'id' => 'modalfrmAddtechnician',
                                                    'header' => '<strong>เพิ่มช่าง </strong>',
                                                    'toggleButton' => [
                                                        'id' => 'btnAddNewtechnician',
                                                        'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                        'class' => 'btn btn-success'
                                                    ],
                                                    'closeButton' => [
                                                        'label' => '<i class="fa fa-close"></i>',
                                                        //'class' => 'close pull-right',
                                                        'class' => 'btn btn-success btn-sm pull-right'
                                                    ],
                                                    'size' => 'modal-md',
                                                ]);
                                                ?>
                                                <form role="form" id="frmAddtechnician">

                                                    <div class="box-body">
                                                        <div class="col-md-12">
                                                            <label class="col-md-2">เลือกช่าง</label>

                                                            <div class="col-sm-10">

                                                                <select id="techician_name_sername" name="techician_name_sername"
                                                                        class="form-control select2">
                                                                    <option value="">เลือกช่าง</option>
                                                                    <?php foreach ($modelcvhname as $k => $v) { ?>
                                                                        <option value="<?php echo $v['technician'] ?>"><?php echo $v['techician_name_sername'] ?></option>
                                                                    <?php }; ?>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <br><br><br><br>

                                                        <div class="col-md-12">
                                                            <label class="col-md-2">เลือกlavel</label>

                                                            <div class="col-sm-10">

                                                                <select id="technician_level_id" name="technician_level_id"
                                                                        class="form-control ">
                                                                    <?php foreach ($modeleholelevelmasterfor as $k => $v) { ?>
                                                                        <option value="<?php echo $v['id'] ?>"><?php echo $v['technician_level_name'] ?></option>
                                                                    <?php }; ?>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <br><br><br><br>
                                                        <?php

                                                        // print_r($modelcvhname);
                                                        ?>
                                                        <div class="col-md-12">
                                                            <?php foreach ($showerepairgroupfor as $k => $v) { ?>
                                                                <input type="checkbox" class="form-check-input col-md-2 checkinputarm_<?php echo $v['id'] ?> "
                                                                       id="erepair_group_id" name="erepair_group_id[]"
                                                                       value="<?php echo $v['id'] ?>">
                                                                <label class="form-check-label col-sm-10"
                                                                       for="exampleCheck1"><?php echo $v['name'] ?></label>
                                                            <?php };?>
                                                        </div>
                                                    </div>

                                                    <!--   <input type="hidden" class="status_active"
                                                              id="status_active" value="1"
                                                              name="status_active">
                       -->
                                                    <!-- /.box-body -->


                                                    <div class="box-body">
                                                        <?php echo Html::hiddenInput('hide_activityedit_technician', null, ['id' => 'hide_activityedit_technician', 'class' => 'hide_activityedit_technician']); ?>
                                                        <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivitytechnician']); ?>
                                                    </div>

                                                </form>
                                                <?php
                                                Modal::end();
                                                ?>
                                            </div>

                                        </div>
                                        <!--end start-->

                                        <!--table start-->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?php
                                                Pjax::begin(['id' => 'pjax_tb_technician']);
                                                echo GridView::widget([
                                                    'dataProvider' => $showeholetakelevelindex,
                                                    'filterModel' => $showeholetakelevel,
                                                    'columns' => [
                                                        [
                                                            'header' => 'ที่',
                                                            'class' => 'yii\grid\SerialColumn',
                                                            'headerOptions' => ['width' => '23'],
                                                        ],

                                                        [
                                                            'attribute' => 'emp_idcard',
                                                            'header' => 'ชื่อช่าง',
                                                            'value' => function ($data) {
                                                                $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->emp_idcard);
                                                                return $modelResultemp['0']['Fullname'];
                                                            },
                                                            'label' => 'ชื่อช่าง',
                                                            //'format' => 'raw',
                                                            'filter' => false,
                                                            'contentOptions' => ['style' => 'width: 155px;', 'align=center']

                                                        ],

                                                        [

                                                            //'attribute' => 'create_by',
                                                            'label' => 'ระดับความชำนาญ',
                                                            // 'value' => 'emp_idcard',
                                                            'value' => 'technician_level_name',
                                                            'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                        ],
                                                        [

                                                            //'attribute' => 'create_by',
                                                            'label' => 'กลุ่มงานซ้อม',
                                                            // 'value' => 'emp_idcard',
                                                            'value' => function ($data) use ($arrItemData) {
                                                                //return ;


                                                                $arraya = $arrItemData[$data->emp_idcard];
                                                                return $arraya;

                                                                /* echo '<pre>';
                                                                 print_r($arraya);
                                                                 echo '</pre>';*/

                                                                //  return $abxc;
                                                                //return count($arraya);
                                                            },
                                                            /* 'value' => 'erepair_group_id',*/
                                                            'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                        ],

                                                        [
                                                            /* 'attribute' => 'record_status',*/
                                                            'label' => 'สถานะ',
                                                            // 'value' => 'record_status',
                                                            'format' => 'image',
                                                            'value' => function ($data) {
                                                                return Utility::dispActive($data->status_active);
                                                            },
                                                            'filter' => false,
                                                            'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                        ],
                                                        [

                                                            'class' => 'yii\grid\ActionColumn',
                                                            'header' => 'จัดการข้อมูลบริษัท',
                                                            'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                            'buttons' => [
                                                                'update' => function ($url, $data) {
                                                                    return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                        'title' => 'แก้ไข',
                                                                        'onclick' => '(function($event) {
                                                                        updatetechnician(' . $data->emp_idcard . ',' . $data->technician_level_id . ');
                                                                })();'
                                                                    ]);
                                                                },


                                                                'delete' => function ($url, $data) {
                                                                    return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                        'title' => 'ลบ',
                                                                        'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->technician_level_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletetechnicians('. $data->emp_idcard . ',' . $data->technician_level_id .');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                    ]);
                                                                },
                                                            ],
                                                            'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                        ],

                                                    ],

                                                ]);
                                                Pjax::end();
                                                ?>
                                            </div>
                                        </div>
                                        <!--table end-->


                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="add_level">
                            <!--modal start-->
                            <div class="row">
                                <div class="col-md-12">


                                    <!--modal start-->
                                    <div class="row">
                                        <div class="col-md-11"></div>

                                        <div class="col-md-1">
                                            <?php
                                            Modal::begin([
                                                'id' => 'modalfrmAddjob',
                                                'header' => '<strong>จัดการlevel </strong>',
                                                'toggleButton' => [
                                                    'id' => 'btnAddNewjob',
                                                    'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                    'class' => 'btn btn-success'
                                                ],
                                                'closeButton' => [
                                                    'label' => '<i class="fa fa-close"></i>',
                                                    //'class' => 'close pull-right',
                                                    'class' => 'btn btn-success btn-sm pull-right'
                                                ],
                                                'size' => 'modal-md',
                                            ]);
                                            ?>
                                            <form role="form" id="frmAddjob">
                                                <div class="form-group">
                                                    <div class="box-body">
                                                        <div class="col-md-12">

                                                        </div>
                                                    </div>

                                                    <div class="box-body">
                                                        <label>ชื่อlevel <span>*</span></label>
                                                        <input id="job_level_name" name="job_level_name"
                                                               data-required="true"
                                                               class="form-control model_name" type="text"
                                                               placeholder="ชื่อlevel">
                                                    </div>
                                                    <!-- /.box-body -->

                                                    <div class="box-body">
                                                        <label>สถานะ</label>
                                                        <br/>
                                                        <input type="hidden" class="status_active"
                                                               id="status_active" value="1"
                                                               name="status_active">แสดง

                                                    </div>


                                                    <div class="box-body">
                                                        <?php echo Html::hiddenInput('hide_activityedit_job', null, ['id' => 'hide_activityedit_job', 'class' => 'hide_activityedit_job']); ?>
                                                        <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivityjob']); ?>
                                                    </div>
                                                </div>
                                            </form>
                                            <?php
                                            Modal::end();
                                            ?>
                                        </div>

                                    </div>
                                    <!--end start-->

                                    <!--table start-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?php
                                            Pjax::begin(['id' => 'pjax_tb_job']);
                                            echo GridView::widget([
                                                'dataProvider' =>  $showeholejoblevelmasterindex,
                                                'filterModel' => $showeholejoblevelmaster,
                                                'columns' => [
                                                    [
                                                        'header' => 'ที่',
                                                        'class' => 'yii\grid\SerialColumn',
                                                        'headerOptions' => ['width' => '23'],
                                                    ],
                                                    [
                                                        'attribute' => 'job_level_name',
                                                        'header' => 'ชื่อlevel',
                                                        'value' => 'job_level_name',
                                                        'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                    ],
                                                    [

                                                        //'attribute' => 'create_by',
                                                        'label' => 'บันทึกโดยผู้ใช้',
                                                        // 'value' => 'emp_idcard',
                                                        'value' => function ($data) {
                                                            $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                            return $modelResultemp['0']['Fullname'];
                                                        },
                                                        'label' => 'บันทึกโดยผู้ใช้',
                                                        //'format' => 'raw',
                                                        'filter' => false,
                                                        'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                    ],
                                                    [

                                                        'label' => 'สถานะ',
                                                        // 'value' => 'record_status',
                                                        'format' => 'image',
                                                        'value' => function ($data) {
                                                            return Utility::dispActive($data->status_active);
                                                        },
                                                        'filter' => false,
                                                        'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                    ],




                                                    [

                                                        'class' => 'yii\grid\ActionColumn',
                                                        'header' => 'จัดการข้อมูลบริษัท',
                                                        'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        updateeholejoblevelmaster(' . $data->id . ',1);
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->job_level_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletejob(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                        'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                    ],


                                                ],

                                            ]);
                                            Pjax::end();
                                            ?>
                                        </div>
                                    </div>
                                    <!--table end-->


                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="levelmaster">


                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">


                                        <!--modal start-->
                                        <div class="row">
                                            <div class="col-md-11"></div>

                                            <div class="col-md-1">
                                                <?php
                                                Modal::begin([
                                                    'id' => 'modalfrmAddjobmaster',
                                                    'header' => '<strong>จัดการlevel </strong>',
                                                    'toggleButton' => [
                                                        'id' => 'btnAddNewjobmaster',
                                                        'label' => '<i class="fa fa-plus-circle"></i>  เพิ่ม ',
                                                        'class' => 'btn btn-success'
                                                    ],
                                                    'closeButton' => [
                                                        'label' => '<i class="fa fa-close"></i>',
                                                        //'class' => 'close pull-right',
                                                        'class' => 'btn btn-success btn-sm pull-right'
                                                    ],
                                                    'size' => 'modal-md',
                                                ]);
                                                ?>
                                                <form role="form" id="frmAddjobmaster">
                                                    <div class="form-group">
                                                        <div class="box-body">
                                                            <div class="col-md-12">

                                                            </div>
                                                        </div>

                                                        <div class="box-body">
                                                            <label>ชื่อlevel <span>*</span></label>
                                                            <input id="technician_level_name" name="technician_level_name"
                                                                   data-required="true"
                                                                   class="form-control model_name" type="text"
                                                                   placeholder="ชื่อlevel">
                                                        </div>
                                                        <!-- /.box-body -->
                                                        <!-- <div class="box-body">
                                                             <label>ชื่อlevel <span>*</span></label>
                                                             <input id="level_code" name="level_code"
                                                                    data-required="true"
                                                                    class="form-control model_name" type="text"
                                                                    placeholder="ชื่อlevel">
                                                         </div>-->

                                                        <div class="box-body">
                                                            <label>สถานะ</label>
                                                            <br/>
                                                            <input type="hidden" class="status_active"
                                                                   id="status_active" value="1"
                                                                   name="status_active">แสดง

                                                        </div>


                                                        <div class="box-body">
                                                            <?php echo Html::hiddenInput('hide_activityedit_jobmaster', null, ['id' => 'hide_activityedit_jobmaster', 'class' => 'hide_activityedit_jobmaster']); ?>
                                                            <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivityjobmaster']); ?>
                                                        </div>
                                                    </div>
                                                </form>
                                                <?php
                                                Modal::end();
                                                ?>
                                            </div>

                                        </div>
                                        <!--end start-->

                                        <!--table start-->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?php
                                                Pjax::begin(['id' => 'pjax_tb_jobmaster']);
                                                echo GridView::widget([
                                                    'dataProvider' =>  $showelevelmasterindex,
                                                    'filterModel' => $showelevelmaster,
                                                    'columns' => [
                                                        [
                                                            'header' => 'ที่',
                                                            'class' => 'yii\grid\SerialColumn',
                                                            'headerOptions' => ['width' => '23'],
                                                        ],
                                                        [
                                                            'attribute' => 'technician_level_name',
                                                            'header' => 'ชื่อlevel',
                                                            'value' => 'technician_level_name',
                                                            'contentOptions' => ['style' => 'width: 500px;', 'align=center']
                                                        ],
                                                        [

                                                            //'attribute' => 'create_by',
                                                            'label' => 'บันทึกโดยผู้ใช้',
                                                            // 'value' => 'emp_idcard',
                                                            'value' => function ($data) {
                                                                $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                                return $modelResultemp['0']['Fullname'];
                                                            },
                                                            'label' => 'บันทึกโดยผู้ใช้',
                                                            //'format' => 'raw',
                                                            'filter' => false,
                                                            'contentOptions' => ['style' => 'width: 155px;', 'align=center']
                                                        ],
                                                        [

                                                            'label' => 'สถานะ',
                                                            // 'value' => 'record_status',
                                                            'format' => 'image',
                                                            'value' => function ($data) {
                                                                return Utility::dispActive($data->status_active);
                                                            },
                                                            'filter' => false,
                                                            'contentOptions' => ['style' => 'width: 23px;', 'align=center']
                                                        ],




                                                        [

                                                            'class' => 'yii\grid\ActionColumn',
                                                            'header' => 'จัดการข้อมูลบริษัท',
                                                            'template' => '{update}  &nbsp;&nbsp; {delete}',
                                                            'buttons' => [
                                                                'update' => function ($url, $data) {
                                                                    return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                        'title' => 'แก้ไข',
                                                                        'onclick' => '(function($event) {
                                                                        updatelevelmaster(' . $data->id . ',1);
                                                                })();'
                                                                    ]);
                                                                },

                                                                'delete' => function ($url, $data) {
                                                                    return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                        'title' => 'ลบ',
                                                                        'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                           message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->technician_level_name . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletejobmaster(' . $data->id . ',1);
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                    ]);
                                                                },
                                                            ],
                                                            'contentOptions' => ['style' => 'width: 112px;', 'align=center']

                                                        ],


                                                    ],

                                                ]);
                                                Pjax::end();
                                                ?>
                                            </div>
                                        </div>
                                        <!--table end-->


                                    </div>
                                </div>
                            </div>

                            <!--modal start-->

                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
</section>