<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;


AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$maxYear = $session->get('maxYear');
$pw_yearnow = $session->get('pw_yearnow');
$arrDataAVG = $session->get('arrDataAVG');
$checkarray = $session->get('checkarray');
$Wages = $session->get('Wages');
$Targets = $session->get('Targets');

?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานรายได้ค่าแรงศูนย์บริการ</h3>

<div class="box">  

    <?php
    $arrMonth = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
    ?>

    <?php if (!empty($checkarray)) { ?>
        <?php
        /*<!--------------------table--------------------->*/

        $arrKeyYear = array_keys($checkarray);
        $arrValue = [];
        foreach ($arrKeyYear as $item) {
            foreach ($checkarray[$item] as $val) {
                $arrValue[$item][] = $val;
            }
        } ?>
    
        <h3 class="box-title">ตารางเปรียบเทียบค่าแรงต่อปี</h3>
            <table bgcolor="#777" ALIGN="CENTER">
                <thead>
                    <tr bgcolor="#eee">
                        <th>เดือน</th>
                        <?php for ($col = 0; $col <= count($arrKeyYear) - 1; $col++) { ?>
                        <th>รายได้ค่าแรงต่อปี <?php print_r($arrKeyYear[$col]); ?>
                        </th>
                        <?php } ?>
                        <th> ยอดเป้าค่าแรงปี <?php echo (date("Y"))?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($row = 0; $row < 12; $row++) { ?>
                    <tr bgcolor="#fff">
                        <td bgcolor="#eee">
                        <?php echo $arrMonth[$row] ?>
                        </td>
                        <?php for ($col = 0; $col < count($arrKeyYear); $col++) { ?>
                        <td>
                        <?php if (isset($arrValue[$arrKeyYear[$col]][$row])) { ?>
                        <?php echo(Helper::displayDecimal($arrValue[$arrKeyYear[$col]][$row])) ?>
                        <?php } else if (!isset($arrValue[$arrKeyYear[$col]][$row])) { ?>
                        <?php echo "0.00 " ?>
                        <?php } ?>
                        </td>
                        <?php } ?>
                        <td>
                        <?php if (isset($arrDataAVG[$row])) { ?>
                        <?php echo(Helper::displayDecimal($arrDataAVG[$row])) ?>
                        <?php } else if (!isset($arrDataAVG[$row])) { ?>
                        <?php echo "0.00 " ?>
                        <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <?php $message = "ไม่มีข้อมูล";
            echo "<script type='text/javascript'>alert('$message');</script>";
            return $this->render('wage'); ?>
        <?php } ?>  

        <h4>ยอดรายได้ค่าแรงเทียบกับปีที่ผ่านมา สะสม <?php print_r(count($arrDataAVG)); ?> เดือน  <?php echo ($Wages) ? $Wages : 0 ?> % </h4>
        <h4>ยอดรายได้ค่าแรงเทียบเป้าสะสม <?php print_r(count($arrDataAVG)); ?> เดือน <?php echo ($Targets) ? $Targets : 0 ?>%</h4>

</div>
<!-- /.box-body -->
<?php
session_start();
session_destroy();
?>