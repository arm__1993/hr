-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 05, 2017 at 05:26 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_section`
--

CREATE TABLE `tax_income_section` (
  `id` tinyint(3) NOT NULL COMMENT 'รหัส',
  `tax_income_type_id` tinyint(3) NOT NULL COMMENT 'ประเภทภาษีบุคคลธรรมดา FK tax_income_type.id',
  `tax_section_name` varchar(255) NOT NULL COMMENT 'ประเภทเงินได้',
  `comment` varchar(1000) DEFAULT NULL,
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0,  99 = delete',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_income_section`
--

INSERT INTO `tax_income_section` (`id`, `tax_income_type_id`, `tax_section_name`, `comment`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(15, 4, 'เงินได้ตามมาตรา 40 (1) ได้แก ่ เงินได้เนื่องจากการจ้าง แรงงาน', 'ไม่ว่าจะเป็นเงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัสเบี้ยหวัด บำเหน็จ\nบำนาญ เงินค่าเช่าบ้าน  เงินที่คำนวณได้จากมูลค่าของการได้อยู่บ้าน ที่นายจ้างให้อยู่โดยไม่เสียค่าเช่า เงินที่นายจ้างจ่ายชำระหนี้ใดๆ ซึ่ง\nลูกจ้างมีหน้าที่ต้องชำระ และเงิน ทรัพย์สิน หรือ ประโยชน์ใดๆ บรรดา\nที่ได้เนื่องจากการจ้างแรงงาน', 1, '1509901325106', '2017-04-05 14:54:29', '', NULL),
(16, 4, 'เงินได้ตามมาตรา 40 (2) ได้แก่ เงินได้เนื่องจากหน้าที่หรือ ตำแหน่งงานที่ทำหรือจากการรับทำงานให้', 'ไม่ว่าจะเป็นค่าธรรมเนียม\nค่านายหน้าค่าส่วนลดเงินอุดหนุนในงานที่ทำเบี้ยประชุม บำเหน็จโบนัส\nเงินค่าเช่าบ้าน เงินที่คำนวณได้จากมูลค่าของการได้อยู่บ้านที่ผู่จ่ายเงินได้\nให้อยู่โดยไม่เสียค่าเช่า เงินที่ผู้จ่ายเงินได้จ่ายชำระหนี้ใดๆ ซึ่งผู้มีเงินได้ มีหน้าที่ต้องชำระ และเงิน ทรัพย์สิน หรือประโยชน์ใดๆ บรรดาที่ได้\nเนื่องจากหน้าที่หรือตำแหน่งงานที่ทำหรือ จากการรับทำงานให้นั้น ไม่ว่า\nหน้าที ่หรือตำแหน่งงานหรืองานที ่รับทำให้นั้นจะเป็นการประจำหรือ\nชั่วคราว', 1, '1509901325106', '2017-04-05 14:56:12', '', NULL),
(17, 5, 'เงินได้ตามมาตรา40 (1) งินได้เนื่องจากการจ้างแรงงาน', 'ได้แก่ เงินได้เนื่องจากการจ้างแรงงาน\nไม่ว่าจะเป็นเงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัส เบี้ยหวัด บำเหน็จ บำนาญ\nเงินค่าเช่าบ้าน เงินที่คำนวณได้จากมูลค่าของการได้อยู่บ้านที่นายจ้างให้\nอยู่โดยไม่เสียค่าเช่า เงินที่นายจ้างจ่ายชำระหนี้ใดๆ ซึ่งลูกจ้างมีหน้าที่\nต้องชำระและเงิน ทรัพย์สิน หรือประโยชน์ใดๆ บรรดาที่ได้เนื่องจากการ\nจ้างแรงงาน เงินที่นายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงานรวมทั้ง\nเงินชดเชยตามกฎหมายแรงงานที่มีระยะเวลาการทำงานน้อยกว่า 5 ปี\nเงินที่นายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงานรวมทั้งเงินชดเชย\nตามกฎหมายแรงงานที่มีระยะเวลาการทำงาน 5 ปีขึ้นไปที่เลือกนำมา\nรวมคำนวณภาษีกับเงินได้อื่น', 1, '1509901325106', '2017-04-05 15:01:19', '1509901325106', '2017-04-05 15:02:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_income_section`
--
ALTER TABLE `tax_income_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_income_type_id` (`tax_income_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_income_section`
--
ALTER TABLE `tax_income_section`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'รหัส', AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
