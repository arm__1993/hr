<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\Utility;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

//use app\api\AjaxSubmitButton;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/ot_activity.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/ot_configpayroll.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //config
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/ot_profileroute.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li>ตั้งค่าข้อมูลองค์กร</li>
                <li class="active">ตั้งค่าทำงานล่วงเวลา (โอที)</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#otactivity" data-toggle="tab">ข้อมูลกิจกรรมโอที</a></li>
                        <li><a href="#otconfig" data-toggle="tab">ตั้งค่าข้อมูลโอที</a></li>
                        <li><a href="#profileroute" data-toggle="tab">โปรไฟล์ค่าเดินทาง</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="otactivity">
                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmOtActivity',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลกิจกรรมโอที</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewOTActivity',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลกิจกรรมโอที ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                    <form role="form" id="frmOtActivity">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>ชื่อกิจกรรม <span>*</span></label>
                                                <input id="activity_name" name="activity_name" data-required="true"
                                                       class="form-control" type="text" placeholder="ชื่อกิจกรรม">
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control" id="activity_status"
                                                        name="activity_status">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <?php echo Html::hiddenInput('hide_activityedit', null, ['id' => 'hide_activityedit']); ?>
                                                <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveActivity']); ?>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <br/>
                                        <?php

                                        Pjax::begin(['id' => 'pjax_otactivity']);
                                        echo GridView::widget([
                                            'id' => 'gridtitle',
                                            'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                            'dataProvider' => $OtActivityProvider,
                                            'filterModel' => $OtActivitySearch,
                                            //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                //'id',
                                                [
                                                    'attribute' => 'activity_name',
                                                    'label' => 'ชื่อกิจกรรม',
                                                    'value' => 'activity_name',
                                                    //'format' => 'raw',
                                                    'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 200px;']
                                                ],

                                                [
                                                    'attribute' => 'statuc_active',
                                                    'label' => 'สถานะ',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 50px;text-align:center']
                                                ],

                                                [
                                                    'attribute' => 'createby_user',
                                                    'value' => 'createby_user',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_datetime);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'headerOptions' => ['width' => '100'],
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{update}  &nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                    updateotactivity(' . $data->id . ');
                                                            })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                      bootbox.confirm({
                                                                        size: "small",
                                                                        message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->activity_name . '? </h4>",
                                                                        callback: function(result){
                                                                            if(result==1) {
                                                                                deleteotactivity(' . $data->id . ');
                                                                            }
                                                                        }
                                                                      });

                                                                })();'
                                                            ]);
                                                        },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        Pjax::end();  //end pjax_gridcorclub

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="otconfig">
                            <?php $form = ActiveForm::begin([
                                'id' => 'configtime-table',
                                'layout' => 'horizontal',
                                'options' => ['class' => 'form-verticle'],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-6',
                                        'offset' => 'col-sm-offset-4',
                                        'wrapper' => 'col-sm-6',
                                        'error' => '',
                                        'hint' => '',
                                    ],
                                ],
                                //'action' => 'saveotconfig',
                                //'enableAjaxValidation' => true,
                                //'validationUrl' => 'validateotconfig',
                            ]);
                            ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php echo $form->field($OtConfigFormular, 'formular')->textInput(['autofocus' => true]) ?>
                                    <?php echo $form->field($OtConfigFormular, 'pay_motel')->textInput(['maxlength' => 6]) ?>
                                    <?php echo $form->field($OtConfigFormular, 'pay_service_twoway')->textInput(['maxlength' => 5]) ?>
                                    <?php
                                    //echo $form->field($OtConfigFormular, 'work_day_inmonth')->textInput(['maxlength' => 5]) ?>
                                    <?php
                                    //echo $form->field($OtConfigFormular, 'work_hour_inday')->textInput(['maxlength' => 5]) ?>
                                    <?php


                                    /*echo $form->field($OtConfigFormular, 'work_start_hour_inday')->widget(\yii\widgets\MaskedInput::className(), [
                                        'mask' => '99:99:99',
                                    ]);*/

                                    /*echo $form->field($OtConfigFormular, 'work_end_hour_inday')->widget(\yii\widgets\MaskedInput::className(), [
                                        'mask' => '99:99:99',
                                    ]);*/


                                    //echo $form->field($OtConfigFormular, 'work_start_hour_inday')->dropDownList($listData, [ 'prompt' => 'กรุณาเลือก']);
                                    ?>
                                    <?php echo $form->field($OtConfigFormular, 'ot_ratio_workday_clockout')->textInput(['maxlength' => 4]) ?>
                                    <?php echo $form->field($OtConfigFormular, 'ot_ratio_holiday')->textInput(['maxlength' => 4]) ?>
                                    <?php echo $form->field($OtConfigFormular, 'ot_ratio_holiday_clockout')->textInput(['maxlength' => 4]) ?>

                                </div>

                                <div class="form-group">
                                    <div class="text-center">
                                        <?php

                                        echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'name' => 'btnSaveOtConfigFormular', 'id' => 'btnSaveOtConfigFormular'])
                                        /*AjaxSubmitButton::begin([
                                            'label' => 'Add',
                                            'useWithActiveForm' => 'configtime-table',
                                            'ajaxOptions' => [
                                                'type' => 'POST',
                                                'success' => new \yii\web\JsExpression("function(data) {
                                                console.log(data);
                                                   // if (data.status == true)
                                                    //{
                                                       // closeTopbar();

                                                    //}
                                                }"),
                                            ],
                                            'options' => ['class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'add-button'],
                                        ]);
                                        AjaxSubmitButton::end();*/

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <div class="tab-pane" id="profileroute">
                            <div class="row">
                                <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmOtProfileRoute',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลโปรไฟล์การเดินทาง</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewOTProfileRoute',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลโปรไฟล์การเดินทาง',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                    <form role="form" id="frmOTProfileRoute">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>โปรไฟล์การเดินทาง <span>*</span></label>
                                                <input id="profile_name" name="profile_name" data-required="true"
                                                       class="form-control" type="text" placeholder="โปรไฟล์การเดินทาง">
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-body">
                                                <label>เวลาเริ่มต้น (ชั่วโมง:นาที) <span>*</span></label>
                                                <input id="time_start" name="time_start" data-required="true"
                                                       class="form-control" type="text" placeholder="เวลาเริ่มต้น"
                                                       data-inputmask='"mask": "99:99"' data-mask>
                                            </div>
                                            <div class="box-body">
                                                <label>เวลาสิ้นสุด (ชั่วโมง:นาที)
                                                    <span>หากไม่ระบุหมายถึงเป็นต้นไป</span></label>
                                                <input id="time_end" name="time_end"
                                                       class="form-control" type="text" placeholder="เวลาสิ้นสุด"
                                                       data-inputmask='"mask": "99:99"' data-mask>
                                            </div>
                                            <div class="box-body">
                                                <label> จำนวนเงิน <span>*</span></label>
                                                <input id="budget" name="budget"
                                                       class="form-control" type="text" data-required="true"
                                                       placeholder="จำนวนเงิน" maxlength="5"
                                                       data-inputmask="'alias': 'decimal'" data-mask>
                                            </div>
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control" id="timetable_status"
                                                        name="timetable_status">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="box-body">
                                                <?php echo Html::hiddenInput('hide_timetableedit', null, ['id' => 'hide_timetableedit']); ?>
                                                <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveOTProfileRoute']); ?>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <br/>
                                        <?php

                                        Pjax::begin(['id' => 'pjax_otconfigtimetable']);
                                        echo GridView::widget([
                                            'id' => 'otconfigtimetable',
                                            'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                            'dataProvider' => $OtConfigTimeTableProvider,
                                            'filterModel' => $OtConfigTimeTableSearch,
                                            //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                //'id',
                                                [
                                                    'attribute' => 'profile_name',
                                                    'label' => 'ชื่อโปรไฟล์',
                                                    'value' => 'profile_name',
                                                    //'format' => 'raw',
                                                    'filter' => true,
                                                    'contentOptions' => ['style' => 'max-width: 150px;']
                                                ],

                                                [
                                                    'attribute' => 'time_start',
                                                    'value' => 'time_start',
                                                    'label' => 'เวลาเริ่มต้น',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'attribute' => 'time_end',
                                                    'value' => 'time_end',
                                                    'label' => 'เวลาสิ้นสุด',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'attribute' => 'budget',
                                                    'value' => 'budget',
                                                    'label' => 'จำนวนเงิน',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 100px;text-align:right']
                                                ],
                                                [
                                                    'attribute' => 'statuc_active',
                                                    'label' => 'สถานะ',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 50px;text-align:center']
                                                ],

                                                [
                                                    'attribute' => 'createby_user',
                                                    'value' => 'createby_user',
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => function ($data) {
                                                        return DateTime::ThaiDateTime($data->create_datetime);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                    'headerOptions' => ['width' => '100'],
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{update}  &nbsp; {delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                'title' => 'แก้ไข',
                                                                'onclick' => '(function($event) {
                                                                    editprofileroute(' . $data->id . ');
                                                            })();'
                                                            ]);
                                                        },

                                                        'delete' => function ($url, $data) {
                                                            return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                'title' => 'ลบ',
                                                                'onclick' => '(function($event) {
                                                                      bootbox.confirm({
                                                                        size: "small",
                                                                        message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->profile_name . '? </h4>",
                                                                        callback: function(result){
                                                                            if(result==1) {
                                                                                deleteprofileroute(' . $data->id . ');
                                                                            }
                                                                        }
                                                                      });

                                                                })();'
                                                            ]);
                                                        },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        Pjax::end();  //end pjax_gridcorclub

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
</section><!-- /.content -->
