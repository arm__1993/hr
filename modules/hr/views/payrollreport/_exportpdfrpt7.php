<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;

$itemsData = $session->get('itemsData');
$title_report = $session->get('title_report');
$arrIdtemp = $session->get('arrIdtemp');
$arr_wc = $session->get('arr_wc');
$arr_template = $session->get('arr_template');

$_dpMonth = $session->get('_dpMonth');
$_dpYear = $session->get('_dpYear');


?>
<style>
    .container{
        font-family: "THSarabun";
        font-size: 16px;
    }
    p {
        font-family: "THSarabun";
        font-size: 16px;
    }
    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }
    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;
    }
    .rpt tr.odd { background: #f3f3f3; }
    .rpt tr.even { background: #FFF; }


</style>

<div class="row container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-6">
            <h3 style="text-align: center"><?php echo $title_report; ?></h3>
        </div>
    </div>
    <?php

    $tbl .= '<div class="row">'; //start div row
    $tbl .= '<div class="col-md-12">'; //start div col


    foreach ($arrIdtemp as $template) {

        if (isset($itemsData[$template])) {
            $tbl = '';
            $tbl .='<table cellspacing="0" class="table table-striped table-bordered rpt"  align="center">';
           // $tbl .='<thead>';
            $tbl .= '<tr role="row" style="font-size: 14px;font-weight: bold;">';
            $tbl .= '<th colspan="5" class="text-left" style="background-color: #73C6B6;">รายงานการหัก ' . $arr_template[$template]['ADD_DEDUCT_TEMPLATE_NAME'] . ' รอบเงินเดือน ' . $_dpMonth . ' ' . $_dpYear . '</th>';
            $tbl .= '</tr>';

            $arrData = $itemsData[$template];
            foreach ($arrData as $company_id => $company_items ) {

                $text_comp = $arr_wc[$company_id]['name'] . ' [ ' . $arr_wc[$company_id]['short_name'] . ' ]';

                $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold">';
                $tbl .= '<th colspan="5" class="text-left" style="background-color: #F8C471;">' . $text_comp . '</th>';
                $tbl .= '</tr>';
                $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold">';
                $tbl .= '<th style="width: 10%" class="text-left">ลำดับ</th>';
                $tbl .= '<th style="width: 30%" class="text-left">ชื่อ - สกุล</th>';
                $tbl .= '<th style="width: 30%" class="text-left">แผนก</th>';
                $tbl .= '<th style="width: 10%" class="text-left">จำนวนเงิน</th>';
                $tbl .= '<th style="width: 20%" class="text-left">รายละเอียด</th>';
                $tbl .= '</tr>';
                //$tbl .= '</thead>';
                //$tbl .= '<tbody>';


                $total_deduct = 0;
                if (isset($itemsData[$template][$company_id])) {
                    $i =1;
                    $arrDataEmp = $itemsData[$template][$company_id];
                    foreach ($arrDataEmp as $item) {
                        $cls = ($i%2==0) ? 'odd' : 'even';
                        $total_deduct += $item['amount'];
                        $tbl .= '<tr class="'.$cls.'" role="row" style="font-size: 12px;">';
                        $tbl .= '<td style="text-align: center;">' . $i . '</td>';
                        $tbl .= '<td class="text-left">'.$item['full_name'].'</td>';
                        $tbl .= '<td class="text-left">'.$item['dept_name'].'</td>';
                        $tbl .= '<td style="text-align: right;">'.Helper::displayDecimal($item['amount']).'</td>';
                        $tbl .= '<td class="text-left">'.$item['remarks'].'</td>';
                        $tbl .= '</tr>';
                        $i++;
                    }
                }

                $tbl .= '<tr role="row" style="font-size: 12px;font-weight: bold;background-color: #EAEAEA;">';
                $tbl .= '<td colspan="3" style="text-align: center;font-weight: bold">รวม</td>';
                $tbl .= '<td style="text-align: right;font-weight: bold">'.Helper::displayDecimal($total_deduct).'</td>';
                $tbl .= '<td class="text-left"></td>';
                $tbl .= '</tr>';
                //$tbl .= '</tbody>';
            }

            $tbl .='</table>';
            $tbl .= '</div>';  //end div columns col-12
            $tbl .= '</div>';  //end div row
            echo $tbl;
        }
    }



    ?>
</div>