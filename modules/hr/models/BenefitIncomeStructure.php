<?php

namespace app\modules\hr\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "benefit_income_structure".
 *
 * @property integer $id
 * @property string $workage_floor
 * @property string $workage_ceil
 * @property string $benefit_rate
 * @property integer $from_year
 * @property integer $to_year
 * @property string $remark
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class BenefitIncomeStructure extends \yii\db\ActiveRecord
{

    protected $_pageSize;

    function __construct() {
        $this->_pageSize = Yii::$app->params['PAGE_SIZE'];
    }


    public function behaviors()
    {
        $session = Yii::$app->session;
        $session->open();
        $_account = $session->get('idcard');
        $session->close();
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_datetime'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_datetime'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createby_user'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateby_user'],
                ],
                'value' => $_account, //$_SESSION['USER_ACCOUNT'],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'benefit_income_structure';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workage_floor', 'workage_ceil', 'benefit_rate'], 'number'],
            [['from_year', 'to_year', 'status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['remark'], 'string', 'max' => 200],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'workage_floor' => 'Workage Floor',
            'workage_ceil' => 'Workage Ceil',
            'benefit_rate' => 'Benefit Rate',
            'from_year' => 'From Year',
            'to_year' => 'To Year',
            'remark' => 'Remark',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    public function search($params)
    {

        //$query = OtActivity::find(); //show all record exclude admin
        $query = BenefitIncomeStructure::find()->where('status_active != :del', [':del' => Yii::$app->params['DELETE_STATUS']]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_datetime' => $this->create_datetime,
        ]);

        $query->andFilterWhere(['like', 'workage_floor', $this->workage_floor])
            ->andFilterWhere(['like', 'workage_ceil', $this->workage_ceil])
            ->andFilterWhere(['like', 'from_year', $this->from_year])
            ->andFilterWhere(['like', 'to_year', $this->to_year])
            ->andFilterWhere(['like', 'benefit_rate', $this->benefit_rate])
            ->andFilterWhere(['like', 'create_byuser', $this->createby_user])
            ->andFilterWhere(['like', 'update_byuser', $this->updateby_user]);
        return $dataProvider;
    }
}
