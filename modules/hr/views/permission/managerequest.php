<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/21/2017 AD
 * Time: 17:48
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/hr/managerequest.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/hr/managerequest.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java



?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">การจัดการสิทธิ์</a>
                </li>
                <li class="active">สร้างคำขอเปิด/ปิด สิทธิ์การใช้งานโปรแกรม</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            
             <div class="panel panel-success">
                <div class="panel-body">
                    <div class="col-sm-12" >
                        <div class="col-sm-6">
                            <h5>รายการ รอนุมัติคำขอเปิด/ปิดสิทธิ์การใช้งานโปรแกรม</h5>
                        </div>
                        
                        <div class="col-sm-6">
                            <button type="button" id="" class="btn btn-primary pull-right" onclick="showModalManageRequestForm()"  >
                                <i class="glyphicon glyphicon-plus"></i>
                                สร้างคำขอ
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:20px">
                        
                       <table id="showManageRequest"  width="100%"></table>

                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->


<div class="modal fade" 
    id="modal-form-manage-request" 
    role="dialog" 
    aria-labelledby="myModalLabel" 
    aria-hidden="true"
   
    data-backdrop="static" data-keyboard="false" >

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">สร้างคำขอเปิด/ปิดสิทธิ์การใช้งานโปรแกรม</h4>
                </div>
                <div class="modal-body" width="100%" >

                    <div id="form-manage-request" style="display:none">
                       
                        <form role="form" class="form-horizontal" >
                            <div class="row">
                                <div class="col-sm-3" style="text-align:right">
                                    <label class="control-label" for="name">สร้างคำขอ:</label>
                                </div>
                                <div class="col-sm-9 form-group">
                                    <label class="radio-inline"><input checked="checked" type="radio" value="1" name="change_to">ขอเปิดสิทธิ์</label>
                                    <label class="radio-inline"><input type="radio" value="2" name="change_to">ขอปิดสิทธิ์</label>
                                </div>
                                
                            </div>
                             <div class="row">
                                <div class="col-sm-3" style="text-align:right">
                                    <label class="control-label" for="name">เลือกโปรแกรม:</label>
                                </div>
                                <div class="col-sm-9 form-group">
                                    <div class="option_content_place">
                                        <select onchange="showOptionMenu(this)">
                                            <?php echo $optionPlace;?>
                                        </select>
                                    </div>
                                    <div class="option_content_menus">
                                       
                                    </div>
                                    <input type="hidden" id="last_menu_id" value="" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3" style="text-align:right">
                                    <label class="control-label" for="name">เลือกพนักงาน:</label>
                                </div>
                                <div class="col-sm-9 form-group">
                                    <select id="empId" >
                                        <option value=""> เลือกพนักงาน </option>
                                    </select>

                                    <input type="hidden" id="emp_info" data-inf="{}" value="" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3" style="text-align:right">
                                    <label class="control-label" for="name">เหตุผลการขอ:</label>
                                </div>
                                <div class="col-sm-9 form-group">
                                   <textarea id="reason_request" name="reason_request" style="width:289px"></textarea>
                                </div>
                            </div>

                        </form>

                    </div>    
                    
                    <div id="pre_load_form">
                        <h1 style="color:#555">
                            รอสักครู่..
                        </h1>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" onclick="saveManageRequest()" value="บันทึก">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  

<input type="hidden" id="allMenus" data-allmenu="[]"  value='' />