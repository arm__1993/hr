<?php

namespace app\modules\hr\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "ot_configformular".
 *
 * @property integer $id
 * @property string $formular
 * @property string $pay_motel
 * @property string $pay_service_twoway
 * @property string $ot_ratio_workday_clockout
 * @property string $ot_ratio_holiday
 * @property string $ot_ratio_holiday_clockout
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class OtConfigformular extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ot_configformular';
    }


    public function behaviors()
    {
        $session = Yii::$app->session;
        $session->open();
        $_account = $session->get('idcard');
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_datetime'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_datetime'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createby_user'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateby_user'],
                ],
                'value' => $_account, //$_SESSION['USER_ACCOUNT'],
            ],
        ];
    }



    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'formular', 'pay_motel', 'pay_service_twoway', 'ot_ratio_workday_clockout', 'ot_ratio_holiday', 'ot_ratio_holiday_clockout'], 'required'],
            [['id', 'status_active'], 'integer'],
            [['pay_motel', 'pay_service_twoway', 'ot_ratio_workday_clockout', 'ot_ratio_holiday', 'ot_ratio_holiday_clockout'], 'number'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['formular'], 'string', 'max' => 200],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'formular' => 'สูตรคำนวณค่าเดินทาง',
            'pay_motel' => 'ค่าที่พักจ่ายตามจริง',
            'pay_service_twoway' => 'ค่าเดินทางมาให้บริการที่บริษัทรวม ไป-กลับ',
            'ot_ratio_workday_clockout' => 'สัดส่วนคำนวณการได้รับโอทีหลังเวลาวันปกติ',
            'ot_ratio_holiday' => 'สัดส่วนคำนวณการได้รับโอทีเวลาปกติวันหยุด',
            'ot_ratio_holiday_clockout' => 'สัดส่วนคำนวณการได้รับโอทีหลังเวลาปกติวันหยุด',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }
}
