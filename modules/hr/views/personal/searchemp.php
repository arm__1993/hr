<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;

use app\bundle\AppAsset;
// API กลาง
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;
// API HR
use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\models\Adddeductdetail;


$imghr = Yii::$app->request->BaseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/tooltip.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/personal/searchemp.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);


//$this->registerCssFile(Yii::$app->request->BaseUrl."/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->BaseUrl."/fonts/01thaifontcss.css");


$this->registerJs($script);

$this->registerCss("
#preview{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
");
?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลพนักงาน</a>
                </li>
                <li class="active">ค้นหาข้อมูลพนักงาน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">ค้นหาข้อมูลพนักงาน</h3>
                </div>
                    <div class="box-body">
                    <form method="post" action="#">
                    <div class="row">
                       <div class="form-group">
                            <label for="genderEmp" class="col-sm-1 control-label"></label>
                            <label for="genderEmp" class="col-sm-1 control-label">บริษัท</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="selectworking" id="selectworking"
                                        onchange="getCompanyForDepartment(this);" ;>
                                    <option value="">เลือกบริษัท</option>
                                    <?php $working = ApiHr::getWorking_company();
                                    foreach ($working as $value) {
                                        echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                    } ?>
                                </select>
                                <span id="urlGetDepartment" title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                            </div>
                            <label for="genderEmp" class="col-sm-1 control-label">แผนก</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="selectdepartment" id="selectdepartment"
                                        onchange="getDepartmentForSection(this);">
                                    <option value=""> เลือกแผนก</option>
                                </select>
                                <span id="urlGetSection" title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                            </div>
                            <label for="genderEmp" class="col-sm-1 control-label">ฝ่าย</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="selectsection" id="selectsection">
                                    <option value=""> เลือกฝ่าย</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="form-group">
                            <label for="genderEmp" class="col-sm-1 control-label"></label>
                            <label for="genderEmp" class="col-sm-1 control-label">ชื่อ</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="nameEmp" size="12">
                            </div>
                            <label for="genderEmp" class="col-sm-1 control-label">นามสกุล</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="lastNameEmp" size="12">
                            </div>
                            <label for="genderEmp" class="col-sm-1 control-label">ชื่อเล่น</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="nickName" size="12">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="genderEmp" class="col-sm-1 control-label"></label>
                            <label for="start_date" class="col-sm-1 control-label">วันที่เริ่ม</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="start_date" name="start_date" size="12">
                            </div>
                            <label for="end_date" class="col-sm-1 control-label">วันที่สิ้นสุด</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="end_date" name="end_date" size="12">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                         <div class="form-group">
                            <label for="genderEmp" class="col-sm-2 control-label"></label>
                            <div class="col-sm-2">
                                <input type="checkbox" name="personal_begin1" id="personal_begin1" value="1" checked>
                                ผ่านงาน
                                <img src="../../images/global/userpass.png" width="25px" higth="25px">
                             </div>
                             <div class="col-sm-2">
                                <input type="checkbox" name="personal_begin2" id="personal_begin2" value="2" checked>
                                ทดลองงาน
                                 <img src="../../images/global/usertest.png" width="25px" higth="25px">
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" name="Personal_daily" id="Personal_daily" value="1" >
                                พนักงานรายวัน
                                <img src="../../images/global/userdaily.png" width="25px" higth="25px">
                           </div>
                           <div class="col-sm-2">
                                <input type="checkbox" name="personal_begin3" id="personal_begin3" value="3">
                                ลาออก
                               <img src="../../images/global/usermoveout.png" width="25px" higth="25px">
                           </div>
                        </div>
                    </div>

                   <br>
                   <br>
                    <div class="row">
                         <div class="form-group">
                            <label for="genderEmp" class="col-sm-5 control-label"></label>
                            <div class="col-sm-5">
                                <button type="button" class="btn btn-primary" id="searchEmp">ค้นหา</button>

                                <button type="button" class="btn btn-danger">ยกเลิก</button>   
                           </div>
                        </div>
                    </div>
                    <br>
                    <br>
                      <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr style="font-size: 12px !important;">
                                        <th>&nbsp;</th>
                                        <th>รหัสตำแหน่ง</th>
                                        <th>ชื่อ-นามสกุล</th>
                                        <th>ตำแหน่ง</th>
                                        <th>บริษัท</th>
                                        <th>แผนก</th>
                                        <th>ฝ่าย</th>
                                        <th>วันที่เริ่มงาน</th>
                                        <th>วันที่ลาออก</th>
                                        <th>สถานะ</th>

                                    </tr>
                                </thead>
                                <tbody id="tbticker">
                                </tbody>
                            </table>
                        <div class="text-center" id="btn_info">

                        </div>
                    </form>
                    </div>
             </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->