<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $code_name
 * @property integer $company
 * @property string $name
 * @property integer $status
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        
        return 'department';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_name', 'company', 'name'], 'required'],
            [['company', 'status'], 'integer'],
            [['code_name'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_name' => 'Code Name',
            'company' => 'Company',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'departmentid', 
            'departmentcode_name', 
            'departmentcompany', 
            'departmentname', 
            'sectioncode_name', 
            'sectiondepartment', 
            'sectionname',
            'sectionid',
            'companyid']);
    }
    public function SearchModel($postValue,$params)
    {
        
        $data = Department::find()
            ->select([
                'working_company.id as companyid',
                'department.id as departmentid',
                'department.code_name as departmentcode_name',
                'department.company as departmentcompany',
                'department.name as departmentname'
                 ]
                )  
            ->from('department')
            ->join('INNER JOIN', 'working_company',
                        'department.company =working_company.id')
            ->where('department.company = '.$postValue['id'].' ')
            ->andWhere('department.status <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','departmentcode_name',$this->departmentcode_name]);
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
    }
}
