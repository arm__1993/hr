<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_family".
 *
 * @property integer $id
 * @property integer $emp_data_id
 * @property string $parent_status
 * @property integer $father_tax_status
 * @property string $father_be
 * @property string $father_name
 * @property string $father_surname
 * @property string $father_idcard
 * @property string $father_birthday
 * @property string $father_insurance
 * @property string $father_business
 * @property string $father_place
 * @property string $father_phone
 * @property integer $mother_tax_status
 * @property string $mother_be
 * @property string $mother_name
 * @property string $mother_surname
 * @property string $mother_idcard
 * @property string $mother_birthday
 * @property string $mother_insurance
 * @property string $mother_business
 * @property string $mother_place
 * @property string $mother_phone
 * @property string $spouse_status
 * @property string $spouse_be
 * @property string $spouse_sex
 * @property string $spouse_name
 * @property string $spouse_surname
 * @property string $spouse_idcard
 * @property string $spouse_birthday
 * @property string $spouse_insurance
 * @property string $spouse_business
 * @property string $spouse_place
 * @property string $spouse_phone
 * @property string $relative_be
 * @property string $relative_status
 * @property string $relative_sex
 * @property string $relative_name
 * @property string $relative_surname
 * @property string $relative_phone
 */
class Empfamily extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_family';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emp_data_id', 'father_tax_status', 'mother_tax_status'], 'integer'],
            [['father_birthday', 'mother_birthday', 'spouse_birthday'], 'safe'],
            [['parent_status', 'father_phone', 'mother_phone', 'spouse_insurance', 'spouse_phone', 'relative_phone'], 'string', 'max' => 50],
            [['father_be', 'father_insurance', 'mother_be', 'mother_insurance', 'spouse_status', 'spouse_be', 'relative_be'], 'string', 'max' => 100],
            [['father_name', 'father_surname', 'father_business', 'mother_name', 'mother_surname', 'mother_business', 'mother_place', 'spouse_name', 'spouse_surname', 'relative_status', 'relative_name', 'relative_surname'], 'string', 'max' => 200],
            [['father_idcard', 'mother_idcard'], 'string', 'max' => 14],
            [['father_place', 'spouse_business', 'spouse_place'], 'string', 'max' => 250],
            [['spouse_sex', 'relative_sex'], 'string', 'max' => 10],
            [['spouse_idcard'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_data_id' => 'Emp Data ID',
            'parent_status' => 'Parent Status',
            'father_tax_status' => 'Father Tax Status',
            'father_be' => 'Father Be',
            'father_name' => 'Father Name',
            'father_surname' => 'Father Surname',
            'father_idcard' => 'Father Idcard',
            'father_birthday' => 'Father Birthday',
            'father_insurance' => 'Father Insurance',
            'father_business' => 'Father Business',
            'father_place' => 'Father Place',
            'father_phone' => 'Father Phone',
            'mother_tax_status' => 'Mother Tax Status',
            'mother_be' => 'Mother Be',
            'mother_name' => 'Mother Name',
            'mother_surname' => 'Mother Surname',
            'mother_idcard' => 'Mother Idcard',
            'mother_birthday' => 'Mother Birthday',
            'mother_insurance' => 'Mother Insurance',
            'mother_business' => 'Mother Business',
            'mother_place' => 'Mother Place',
            'mother_phone' => 'Mother Phone',
            'spouse_status' => 'Spouse Status',
            'spouse_be' => 'Spouse Be',
            'spouse_sex' => 'Spouse Sex',
            'spouse_name' => 'Spouse Name',
            'spouse_surname' => 'Spouse Surname',
            'spouse_idcard' => 'Spouse Idcard',
            'spouse_birthday' => 'Spouse Birthday',
            'spouse_insurance' => 'Spouse Insurance',
            'spouse_business' => 'Spouse Business',
            'spouse_place' => 'Spouse Place',
            'spouse_phone' => 'Spouse Phone',
            'relative_be' => 'Relative Be',
            'relative_status' => 'Relative Status',
            'relative_sex' => 'Relative Sex',
            'relative_name' => 'Relative Name',
            'relative_surname' => 'Relative Surname',
            'relative_phone' => 'Relative Phone',
        ];
    }
}
