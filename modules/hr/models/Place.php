<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "place".
 *
 * @property integer $id
 * @property string $name
 * @property string $path
 * @property string $IP
 * @property string $Icon
 * @property string $Color
 * @property integer $status
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'place';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['IP','Icon',], 'string', 'max' => 255],
            [['Color'], 'string', 'max' => 20],
            [['path'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'path' => 'Path',
            'IP' => 'IP',
            'Icon' => 'Icon',
            'Color' => 'Color',
            'status' => 'Status',
        ];
    }
}
