<?php

namespace app\modules\vhc\models\VHC;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_grouplist".
 *
 * @property integer $id
 * @property string $group_name
 * @property integer $record_status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcGrouplist extends MasterModel
{
    /**
     * @inheritdoc
     */
    protected $_pageSize;
    public static function tableName()
    {
        return 'vhc_grouplist';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_name','model_id','record_status'], 'required'],
            [['record_status','model_id'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['group_name'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id'=>'Model ID',
            'group_name' => 'Group Name',
            'record_status' => 'Record Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
    public function searc4h($params)
    {

        $query = VhcGrouplist::find()->where(['!=','record_status','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'group_name', $this->group_name]
                                );

        return $dataProvider;
    }
    public function search($params)
    {
        $data = VhcGrouplist::find() ->where('record_status <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','id',$this->id]); //รหัสบริษัท
        $data->andFilterWhere(['like','model_id',$this->model_id]); //รหัสบริษัท
        $data->andFilterWhere(['like','group_name',$this->group_name]); //	ชื่อเต็มบริษัท
        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

    }
}
