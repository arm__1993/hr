<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 6/6/2017 AD
 * Time: 9:55 AM
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use kartik\date\DatePicker;


use app\modules\hr\apihr\ApiHr;


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<section class="content">
    <!-- Default box -->
    <!--<div class="box box-danger">-->
    <!--<div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">ข้อมูลพนักงาน</a>
            </li>
            <li>เพิ่มข้อมูลพนักงาน</li>
            <li class="active">ประกันสังคมและเงินสะสม</li>
        </ul>
    </div>-->
    <?php
    print_r($data);
    ?>
    <div class="box-body">
        <input type="hidden" id="idcardset" value="1509901325106">
        <form class="form-horizontal"  method="post">
            <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf">
            <div class="row">
                <div class="col-md-12" id="showMonth">
                    <center>
                        <label>ภาษีประจำปี</label>
                        &nbsp;
                        <!-- <div class="btn-group">
                            <select class="form-control" name="selectmonth" id="selectmonthtax" >
                                <?php

                                for ($i = 1; $i <= 12; $i++) {
                                    $sel = ($i == date('m')) ? 'selected="selected"' : '';
                                    echo '<option value="' . $i . '" ' . $sel . '>' . \app\api\DateTime::convertMonth($i) . '</option>';
                                }
                                ?>
                            </select>
                        </div> -->
                        <!-- &nbsp;&nbsp; -->
                        <div class="btn-group">
                            <div class="btn-group">
                                <select class="form-control" name="selectyeartax" id="selectyeartax">

                                </select>
                            </div>
                        </div>
                        <!-- &nbsp;&nbsp;
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <div class="btn-group">
                                        <button type="button" id="searchTax" class="btn btn-block btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="reset" class="btn btn-block btn-danger btn-sm">ล้างข้อมูล</button>
                                    </div>
                                </center>
                            </div>
                        </div> -->
                    </center>
        <br>
        <br>
                    <div class="row">
                        <div class="col-sm- text-center">
                            <h4>รายการหักภาษีส่งสรรพากรประจำปี</h4>
                            <table  class="table table-bordered table-striped dataTable">
                                <thead style="background-color: #EAEAEA; text-align: center;">
                                    <tr>
                                        <th style="text-align: center;">ลำดับ</th>
                                        <th style="text-align: center;">ประเภทการหักภาษี</th>
                                        <th style="text-align: center;">จำนวนเงิน</th>
                                        <th style="text-align: center;">หมายเหตุ</th>
                                    </tr>
                                </thead>
                            <tbody id="detailtaxpersonal">

                            </tbody>
                            <tfoot id="foot_sumtax_all" style="display: none;">
                            <tr style="background-color: #EAEAEA;">
                                <th style="text-align: center;" colspan="2">รวม</th>
                                <th style="text-align: center;" id="sum_tax_all"></th>
                                <th style="text-align: center;"></th>
                            </tr>
                            </tfoot>
                            </table>

                        </div>
                    </div>

    <!-- /.box -->
</section><!-- /.content -->
