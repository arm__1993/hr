
<?php

/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 8/10/2559
 * Time: 13:48
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiTime;
HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/jquery.datetimepicker.full.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

 $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });

});
JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานเวลาซ่อมโดยเฉลี่ยต่อวัน</h3>


    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>        
        
        <div class="box-body">

                <div class="row">

                <form method="post" action="time_average">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key=>$value) {
                                $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                            }
                            ?>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                         <!-- <label>วันที่</label>
                         &nbsp;
                         <div class="btn-group">
                            <input type="text" name="startDate" id="startDate" value="">
                        </div>
                        &nbsp;&nbsp;
                         <label>ถึงวันที่</label>
                         &nbsp;
                         <div class="btn-group">
                            <input type="text" name="endDate" id="endDate" value="">
                        </div>
                        &nbsp;&nbsp; -->
                        <label>ช่วงวันที่</label>
                        &nbsp;
                          <div class="btn-group">                            
                            <div class="input-group" style="width: 250px">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" name="reservation" id="reservation" data-provide="datepicker" data-date-language="th" <?php if(!empty($selected_startmonth)){ echo 'value="'.$selected_startmonth.'"';}?>>
                            </div>
                          </div>
                          &nbsp;&nbsp;
                        <label>เลือกช่าง</label>
                        &nbsp;<?php
                            ?>
                        <div class="btn-group">
                        <input type="radio" name="technician" value="1" <?php echo ($selected_technician_car =='1') ? 'checked' : '';?> checked> ช่างรถเล็ก
                        &nbsp;
                        <input type="radio" name="technician" value="2" <?php echo ($selected_technician_car =='2') ? 'checked' : '';?> > ช่างรถใหญ่
                        &nbsp;
                        <input type="radio" name="technician" value="3" <?php echo ($selected_technician_car =='3') ? 'checked' : '';?>> ช่างทั้งหมด
                        </div>
                        </center>                    

                    <center><br>
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/time/btn_reporttime">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                    </center>
                    </div>
                </form>
            </div>
        </div><!--/box-body-->
<?php if($query) { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php
                                        $arrDateTime = DateTime::FormatDateFromCalendarRange($selected_startmonth);
                                        $dateRang  = ApiTime::getDayRangtime($arrDateTime['start_date'],$arrDateTime['end_date']);

                                        $valueTimeRang = [];
                                         foreach ($dateRang AS $valueTime) {
                                             $valueTimeRang[] = $valueTime ;
                                                }

                                        $totalSum = (array_merge_recursive($dateRangTimeAvgDistance,$dateRangTimeAvgIntersect,$dateRangTimeAvgGenaral,$dateRangTimeAvgFree));
                                        $totalSumListChart = [];
                                        foreach ($totalSum as $k=>$subArray) {
                                             $value = array_sum($subArray);
                                            $valueTimeAvgToltal = ApiTime::getAvgTimeReport1($value);
                                            $result = Helper::displayDecimal($valueTimeAvgToltal);
                                            $totalSumList[] = (float)$result;
                                        }

                                            echo Highcharts::widget([
                                                'options' => [
                                                    'title' => [
                                                        'text' => ''
                                                    ],
                                                    'xAxis' => [
                                                        'categories' => $valueTimeRang
                                                    ],
                                                    'labels' => [
                                                        'items' => [
                                                            'html' => '',
                                                            'style' => [
                                                                'left' => '50px',
                                                                'top' => '18px',
                                                                'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                            ]
                                                        ]
                                                    ],
                                                    'credits' => [
                                                        'enabled' => false
                                                    ],

                                                    'series' => [
                                                        [
                                                            'name' => 'รวม',
                                                            'data' => $totalSumList
                                                        ],

                                                    ]
                                                ]
                                            ]);
                                            ?>
                                    </div>
                                </div><!--row-->
                        </div>
                    </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA CHART -->
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">

                                <table id="example2" class="table table-striped table-bordered table-hover"  role="grid" aria-describedby="example2_info">
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <th width="30%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        </th>
                                        <?php
                                        $arrDateTime = DateTime::FormatDateFromCalendarRange($selected_startmonth);
                                        $dateRang  = ApiTime::getDayRangtime($arrDateTime['start_date'],$arrDateTime['end_date']);
                                        ?>
                                        <?php foreach ($dateRang AS $value){ ?>
                                        <td><?php echo $value;?></td>
                                        <?php } ?>

                                    </tr>
                                    <?php
                                    $totalSum = (array_merge_recursive($dateRangTimeAvgDistance,$dateRangTimeAvgIntersect,$dateRangTimeAvgGenaral,$dateRangTimeAvgFree));
                                    $totalSumList = [];
                                    foreach ($totalSum as $k=>$subArray) {
                                        $totalSumList[$k] = array_sum($subArray);
                                    }

                                    ?>
                                    <tr  role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            งานเช็คระยะ
                                        </th>
                                        <?php foreach ($dateRangTimeAvgDistance AS $value){?>
                                            <td>
                                                <?php
                                                $valueTimeAvgDistance = ApiTime::getAvgTimeReport1($value);
                                                echo Helper::displayDecimal($valueTimeAvgDistance);
                                                ;?>
                                            </td>
                                        <?php }?>
                                    </tr>
                                    <tr role="row" class="even">
                                        <th   class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            งานเช็คระยะ+ซ่อมทั่วไป
                                        </th>
                                        <?php foreach ($dateRangTimeAvgIntersect AS $value){?>
                                            <td>
                                                <?php  $valueTimeAvgIntersect = ApiTime::getAvgTimeReport1($value);
                                                echo Helper::displayDecimal($valueTimeAvgIntersect);
                                                ?>
                                            </td>
                                        <?php }?>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <th  class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            งานซ่อมทั่วไป
                                        </th>
                                        <?php foreach ($dateRangTimeAvgGenaral AS $value){?>
                                            <td>
                                                <?php  $valueTimeAvgGenaral = ApiTime::getAvgTimeReport1($value);
                                                echo Helper::displayDecimal($valueTimeAvgGenaral);
                                                ?>
                                            </td>
                                        <?php }?>
                                    </tr>
                                    <tr role="row" class="even">
                                        <th  class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            งานฟรี
                                        </th>
                                        <?php foreach ($dateRangTimeAvgFree AS $value){?>
                                            <td>
                                                <?php  $valueTimeAvgFree =  ApiTime::getAvgTimeReport1($value);
                                                echo Helper::displayDecimal($valueTimeAvgFree);
                                                ?>
                                            </td>
                                        <?php }?>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            รวม
                                        </th>
                                        <?php foreach ($totalSumList AS $value){
                                            ?>
                                            <td><?php $valueTimeAvgTotal=  ApiTime::getAvgTimeReport1($value);
                                                echo Helper::displayDecimal($valueTimeAvgTotal);?></td>
                                        <?php } ?>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>        
<?php } ?>
</div><!--box-->

<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>