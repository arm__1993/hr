<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "crepair_list".
 *
 * @property integer $id
 * @property string $crepair_id
 * @property string $repairlist_id
 * @property string $name
 * @property string $emp_id
 * @property string $add_time
 * @property string $sdel
 */
class CrepairList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crepair_list';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crepair_id', 'repairlist_id', 'name', 'emp_id', 'add_time', 'sdel'], 'required'],
            [['crepair_id', 'repairlist_id', 'add_time'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 250],
            [['emp_id'], 'string', 'max' => 13],
            [['sdel'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'crepair_id' => 'Crepair ID',
            'repairlist_id' => 'Repairlist ID',
            'name' => 'Name',
            'emp_id' => 'Emp ID',
            'add_time' => 'Add Time',
            'sdel' => 'Sdel',
        ];
    }
}
