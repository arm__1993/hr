<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "aMain_district".
 *
 * @property integer $district_id
 * @property string $district_code
 * @property string $district_name
 * @property integer $amphur_id
 * @property integer $province_id
 * @property integer $GEO_ID
 */
class Amaindistrict extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
      /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_maincusdata');
    }
    public static function tableName()
    {
        return 'aMain_district';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['district_code', 'district_name'], 'required'],
            [['amphur_id', 'province_id', 'GEO_ID'], 'integer'],
            [['district_code'], 'string', 'max' => 6],
            [['district_name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'district_id' => 'District ID',
            'district_code' => 'District Code',
            'district_name' => 'District Name',
            'amphur_id' => 'Amphur ID',
            'province_id' => 'Province ID',
            'GEO_ID' => 'Geo  ID',
        ];
    }

    public function searchamphurbyamphurid($keyword) // param
    {
        $amphur = Yii::$app->dbERP_maincusdata
       ->createCommand("select * from aMain_district  where amphur_id = :keyword ORDER BY district_name ASC")
       ->bindParam(':keyword',$keyword)->queryAll();
        return $amphur;

    }
}
