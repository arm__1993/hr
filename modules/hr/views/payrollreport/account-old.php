<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:22
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPayroll;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/report/accountreport.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //route

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hr-config.js?t='.time());
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/MonthPicker.min.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl."/css/hr/MonthPicker.min.css");

$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");


$script = <<< JS
$(document).ready(function() {
    setTimeout(function() {
    $('#message').fadeOut('slow');
    }, 3000);

// $("#example2").DataTable({
//       "lengthChange": false,
//       "searching": true,
//       "paging": false,
//       "info": false,
//       "pageLength" : 10,
//     }
//     );

});
JS;

$this->registerJs($script);

?>

<span id="activetab" title="2"></span>
<input type="hidden" id="tabselectsubmit" value="<?php if ($tab) {
    echo $tab;
} else {
    echo 1;
} ?>">
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>ออกรายงาน</li>
                <li class="active">บัญชี</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active tabselect" id="tabselect1"><a href="#tab1"
                                                                    data-toggle="tab">รายงานสรุปผลเงินเดือน</a></li>
                    <li id="tabselect2" class="tabselect"><a href="#tab2" data-toggle="tab">สรุปส่วนเพิ่ม</a></li>
                    <li id="tabselect3" class="tabselect"><a href="#tab3" data-toggle="tab">รายละเอียดส่วนเพิ่ม</a></li>
                    <li id="tabselect4" class="tabselect"><a href="#tab4" data-toggle="tab">สรุปส่วนหัก</a></li>
                    <li id="tabselect5" class="tabselect"><a href="#tab5" data-toggle="tab">รายละเอียดส่วนหัก</a></li>
                    <li id="tabselect6" class="tabselect"><a href="#tab6"
                                                             data-toggle="tab">รายละเอียดส่วนเพิ่มตามรายการ</a></li>
                    <li id="tabselect7" class="tabselect"><a href="#tab7"
                                                             data-toggle="tab">รายละเอียดส่วนหักตามรายการ</a></li>
                    <li id="tabselect8" class="tabselect"><a href="#tab8" data-toggle="tab">รายงานเงินสะสม</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active tabselect" id="tab1">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <form class="form-horizontal" id="formSearchOT" action="reportsalarymonth"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid[]"
                                                        multiple="multiple" required>
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        &nbsp;
                                        &nbsp;
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" class="form-control monthselect" name="monthselect" require
                                               style="width:130px" required>
                                        &nbsp;
                                        <button type="submit" class="btn btn-primary btn-sm btnselect" title="1">ค้นหา
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-4">
                            </div>

                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-5">
                                </div>
                                <div class="col-md-6">
                                    <h3>รายละเอียดรายงานเงินเดือน</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-6">
                                    <h3><?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                                        //print_r($getMonth);
                                        $monthGetReport1 = $getMonth['monthresult'];
                                        $resultMonthReport1 = Datetime::mappingMonth($monthGetReport1);
                                        $yearGetReport1 = $getMonth['year'];
                                        ?>
                                        รายงานสรุปรวมผลเงินเดือน จ่าย วันที่ <?php echo $resultMonthReport1;
                                        echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h3>
                                </div>
                            </div>
                            <?php if ($querySalaryTotalThisMonth1) {
                                //  echo "<pre>";
                                //   print_r($reportSalaryTotal);?>
                                <div class="text-right">
                                    <a href="exportpdfsumtotalallthismonth">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>
                                <table id="example2" class="table-bordered table-striped dataTable" width="80%"
                                       align="center">
                                    <thead>
                                    <tr role="row">
                                        <th aria-sort="ascending">
                                            ชื่อบริษัท
                                        </th>
                                        <th aria-sort="ascending">
                                            เงินเดือน
                                        </th>
                                        <th aria-sort="ascending">
                                            ส่วนเพิ่ม
                                        </th>
                                        <th aria-sort="ascending">
                                            รวมรับ
                                        </th>
                                        <th aria-sort="ascending">
                                            ส่วนหัก
                                        </th>
                                        <th aria-sort="ascending">
                                            รวมรับทั้งสิน
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($reportSalaryTotal as $value) { ?>
                                        <tr>
                                            <td width="30%"><?php echo $value['companyname'] ?></td>
                                            <td width="15"
                                                align="right"><?php echo Helper::displayDecimal($value['SUMSALARY']) ?></td>
                                            <td width="15%"
                                                align="right"><?php echo Helper::displayDecimal($value['SUM_WAGE_TOTAL_ADDITION']) ?></td>
                                            <td width="15%"
                                                align="right"><?php $sumSalaryAddDeduc = $value['SUMSALARY'] + $value['SUM_WAGE_TOTAL_ADDITION'];
                                                echo Helper::displayDecimal($sumSalaryAddDeduc);
                                                ?></td>
                                            <td width="15%"
                                                align="right"><?php echo Helper::displayDecimal($value['WAGE_TOTAL_DEDUCTION']) ?></td>
                                            <td width="15%"
                                                align="right"><?php echo Helper::displayDecimal($sumSalaryAddDeduc - $value['WAGE_TOTAL_DEDUCTION']) ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab2">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <form class="form-horizontal" id="formSearchOT" action="reportadddeductworking"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid[]"
                                                        multiple="multiple">
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        <br>
                                        &nbsp;
                                        &nbsp;
                                        <label>รายการเพิ่ม</label>
                                        &nbsp;
                                        &nbsp;
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="idtemp[]"
                                                        multiple="multiple">
                                                        <?php $working = ApiPayroll::listdata_add_deduct_template();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        &nbsp;
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" name="selectmonth" class="form-control monthselect"
                                               style="width:130px">
                                        &nbsp;
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-sm btnselect" title="2">ค้นหา
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-4">
                            </div>

                        </div>
                        <?php if ($querySalaryTotalThisMonth2) { ?>
                            <div class="row" style="overflow: auto;">
                                <div class="row">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดรายงานเงินเดือน</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                                        //print_r($getMonth);
                                        $monthGetReport1 = $getMonth['monthresult'];
                                        $resultMonthReport1 = Datetime::mappingMonth($monthGetReport1);
                                        $yearGetReport1 = $getMonth['year'];
                                        ?>
                                        <h3>รายงานสรุปผลเงินเดือน (รายการเพิ่ม) จ่าย
                                            วันที่ <?php echo $resultMonthReport1;
                                            echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h3>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="exportpdfsumadddeduct">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
                                       aria-describedby="example1_wrapper">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="20%">
                                            รายการเพิ่ม
                                        </th>
                                        <?php foreach ($modelWorkingcompany as $key => $value) { ?>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-sort="ascending" width="10%">
                                                <?php echo $value ['short_name']; ?>
                                            </th>
                                        <?php } ?>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="20%">
                                            รวม
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($arrIdtemp as $key1 => $value) {
                                        $somrow = [];
                                        ?>
                                        <tr>
                                            <td><?php echo $value['ADD_DEDUCT_TEMPLATE_NAME']; ?></td>
                                            <?php foreach ($modelWorkingcompany as $key2 => $values) { ?>
                                                <td align="right"><?php
                                                    $setZero = "0.00";
                                                    foreach ($modelCommpaneyAdddeduct AS $key => $valueResult) {
                                                        switch ($valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] AND $valueResult['WAGE_WORKING_COMPANY']) {
                                                            case $valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $value['ADD_DEDUCT_TEMPLATE_ID'] && $valueResult['WAGE_WORKING_COMPANY'] == $values['id'];
                                                                // echo "//////".$valueResult['SUM_ADD_DEDUCT_HIS']."//////";
                                                                $setZero = $valueResult['SUM_ADD_DEDUCT_HIS'];
                                                                break;
                                                        }

                                                    }
                                                    echo Helper::displayDecimal($setZero);
                                                    $somrow[] = $setZero;

                                                    ?></td>
                                            <?php } ?>
                                            <td><?php echo Helper::displayDecimal(array_sum($somrow)); ?></td>
                                        </tr>
                                        <?php ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane tabselect" id="tab3">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <form class="form-horizontal" id="formSearchOT" action="reportadddetaildeduct"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid">
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        <br>
                                        &nbsp;
                                        &nbsp;
                                        <label>รายการเพิ่ม</label>
                                        &nbsp;
                                        &nbsp;
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="idtemp[]"
                                                        multiple="multiple">
                                                        <option value="S1">เงืนเดือน</option>
                                                    <?php $working = ApiPayroll::listdata_add_deduct_template();
                                                    foreach ($working as $value) {
                                                        echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                    } ?>
                                                </select>
                                            </span>
                                        &nbsp;
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" class="form-control monthselect" name="monthselect"
                                               style="width:130px">
                                        &nbsp;
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-4">
                            </div>

                        </div>
                        <?php if ($querySalaryTotalThisMonth3) { ?>
                            <div class="row" style="overflow: auto;">
                                <div class="row">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดรายงานเงินเดือน</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายงานสรุปผลเงินเดือนรายบุคคล (รายการเพิ่ม) จ่าย </h3>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="exportpdfreportadddetaildeduct">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>

                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
                                       aria-describedby="example1_wrapper" width="80%">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="20%">
                                            ชื่อพนักงาน
                                        </th>
                                        <?php foreach ($modelEmp as $key => $value) { ?>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-sort="ascending" width="10%">
                                                <?php echo $value ['Fullname']; ?>
                                            </th>
                                        <?php } ?>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="30%">
                                            รวม
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($statusSalary) { ?>
                                        <tr>
                                            <td>เงินเดือน</td>
                                            <?php

                                            foreach ($modelEmp as $key2 => $values) {
                                                ?>
                                                <td align="right"><?php
                                                    $sumsalary = '0.00';
                                                    foreach ($modelSalary AS $key => $valueResultSalary) {
                                                        switch ($valueResultSalary['WAGE_EMP_ID']) {
                                                            case $valueResultSalary['WAGE_EMP_ID'] == $values['ID_Card'];
                                                                $sumsalary = $valueResultSalary['WAGE_SALARY'];
                                                                break;
                                                        }

                                                    }
                                                    echo Helper::displayDecimal($sumsalary);
                                                    $somrowsalary[] = $sumsalary;

                                                    ?></td>
                                            <?php } ?>
                                            <td><?php echo Helper::displayDecimal(array_sum($somrowsalary)); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php foreach ($modelTemp as $key1 => $value) {
                                        $somrow = [];
                                        ?>
                                        <tr>
                                            <td><?php echo $value['ADD_DEDUCT_TEMPLATE_NAME']; ?></td>
                                            <?php foreach ($modelEmp as $key2 => $values) { ?>
                                                <td align="right"><?php
                                                    $setZero = "0.00";
                                                    foreach ($modelAdddeductdtail AS $key => $valueResult) {
                                                        switch ($valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] AND $valueResult['WAGE_EMP_ID']) {
                                                            case $valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $value['ADD_DEDUCT_TEMPLATE_ID'] && $valueResult['WAGE_EMP_ID'] == $values['ID_Card'];
                                                                $setZero = $valueResult['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                                                                break;
                                                        }

                                                    }
                                                    echo Helper::displayDecimal($setZero);
                                                    $somrow[] = $setZero;

                                                    ?></td>
                                            <?php } ?>
                                            <td><?php echo Helper::displayDecimal(array_sum($somrow)); ?></td>
                                        </tr>
                                        <?php ?>
                                    <?php } ?>
                                    <tr>
                                        <td><B>รวมรับ</B></td>
                                        <?php foreach ($modelEmp as $key2 => $values) { ?>
                                            <td align="right"><?php
                                                $setZeroSum = "0.00";
                                                foreach ($modelSumTotal AS $key => $valueResultSum) {
                                                    switch ($valueResultSum['WAGE_EMP_ID']) {
                                                        case $valueResultSum['WAGE_EMP_ID'] == $values['ID_Card'];
                                                            $setZeroSum = $valueResultSum['SUM_ADD_DEDUCT_HIS'] + $valueResultSum['WAGE_SALARY'];
                                                            break;
                                                    }

                                                }
                                                echo Helper::displayDecimal($setZeroSum);
                                                ?></td>
                                        <?php } ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane tabselect" id="tab4">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <form class="form-horizontal" id="formSearchOT" action="reportdedeductworking"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid[]"
                                                        multiple="multiple">
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        <br>
                                        &nbsp;
                                        &nbsp;
                                        <label>รายการหัก</label>
                                        &nbsp;
                                        &nbsp;
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="idtemp[]"
                                                        multiple="multiple">
                                                      <option value="SSO1">ประกันสังคม</option>
                                                    <?php $working = ApiPayroll::getDataDeductTemp();
                                                    foreach ($working as $value) {
                                                        echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                    } ?>
                                                </select>
                                            </span>
                                        &nbsp;
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" name="selectmonth" class="form-control monthselect"
                                               style="width:130px">
                                        &nbsp;
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-4">
                            </div>

                        </div>
                        <?php if ($querySalaryTotalThisMonth4) { ?>
                            <div class="row" style="overflow: auto;">
                                <div class="row">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดรายงานเงินเดือน</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                                        //print_r($getMonth);
                                        $monthGetReport1 = $getMonth['monthresult'];
                                        $resultMonthReport1 = Datetime::mappingMonth($monthGetReport1);
                                        $yearGetReport1 = $getMonth['year'];
                                        ?>
                                        <h3>รายงานสรุปผลเงินเดือน (รายการหัก) จ่าย
                                            วันที่ <?php echo $resultMonthReport1;
                                            echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h3>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="exportpdfreportdedeductworking">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
                                       aria-describedby="example1_wrapper" width="80%">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="30%">
                                            รายการหัก
                                        </th>
                                        <?php foreach ($modelWorkingcompany as $key => $value) { ?>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-sort="ascending" width="10%">
                                                <?php echo $value ['short_name']; ?>
                                            </th>
                                        <?php } ?>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="30%">
                                            รวม
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($statusSSO) { ?>
                                        <tr>
                                            <td>ประกันสังคม</td>
                                            <?php

                                            foreach ($modelWorkingcompany as $key2 => $values) {
                                                ?>
                                                <td align="right"><?php
                                                    $sumSSO = '0.00';
                                                    foreach ($modelCommpaneySSO AS $key => $valueResultSSO) {
                                                        switch ($valueResultSSO['WAGE_WORKING_COMPANY']) {
                                                            case $valueResultSSO['WAGE_WORKING_COMPANY'] == $values['id'];
                                                                $sumSSO = $valueResultSSO['SUM_ADD_DEDUCT_HIS'];
                                                                break;
                                                        }

                                                    }
                                                    echo Helper::displayDecimal($sumSSO);
                                                    $arrSumSSO[] = $sumSSO;

                                                    ?></td>
                                            <?php } ?>
                                            <td><?php echo Helper::displayDecimal(array_sum($arrSumSSO)); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php foreach ($modelTemp as $key1 => $value) {
                                        $somrow = [];
                                        ?>
                                        <tr>
                                            <td><?php echo $value['ADD_DEDUCT_TEMPLATE_NAME']; ?></td>
                                            <?php foreach ($modelWorkingcompany as $key2 => $values) { ?>
                                                <td align="right"><?php
                                                    $setZero = "0.00";
                                                    foreach ($modelCommpaneyAdddeduct AS $key => $valueResult) {
                                                        switch ($valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] AND $valueResult['WAGE_WORKING_COMPANY']) {
                                                            case $valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $value['ADD_DEDUCT_TEMPLATE_ID'] && $valueResult['WAGE_WORKING_COMPANY'] == $values['id'];
                                                                // echo "//////".$valueResult['SUM_ADD_DEDUCT_HIS']."//////";
                                                                $setZero = $valueResult['SUM_ADD_DEDUCT_HIS'];
                                                                break;
                                                        }

                                                    }
                                                    echo Helper::displayDecimal($setZero);
                                                    $somrow[] = $setZero;

                                                    ?></td>
                                            <?php } ?>
                                            <td><?php echo Helper::displayDecimal(array_sum($somrow)); ?></td>
                                        </tr>
                                        <?php ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane tabselect" id="tab5">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <form class="form-horizontal" id="formSearchOT" action="reportdedetaildeduct"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid">
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        <br>
                                        &nbsp;
                                        &nbsp;
                                        <label>รายการหัก</label>
                                        &nbsp;
                                        &nbsp;
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="idtemp[]"
                                                        multiple="multiple">
                                                      <option value="SSO1">ประกันสังคม</option>
                                                    <?php $working = ApiPayroll::getDataDeductTemp();
                                                    foreach ($working as $value) {
                                                        echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                    } ?>
                                                </select>
                                            </span>
                                        &nbsp;
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" name="monthselect" class="form-control monthselect"
                                               style="width:130px">
                                        &nbsp;
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-4">
                            </div>

                        </div>
                        <?php if ($querySalaryTotalThisMonth5) { ?>
                            <div class="row">
                                <div class="row" style="overflow: auto;">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดรายงานเงินเดือน</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                                        //print_r($getMonth);
                                        $monthGetReport1 = $getMonth['monthresult'];
                                        $resultMonthReport1 = Datetime::mappingMonth($monthGetReport1);
                                        $yearGetReport1 = $getMonth['year'];
                                        ?>
                                        <h3>รายงานสรุปผลเงินเดือนรายบุคคล (รายการหัก) จ่าย
                                            วันที่ <?php echo $resultMonthReport1;
                                            echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h3>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="exportpdfreportdedetaildeduct">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
                                       aria-describedby="example1_wrapper">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="20%">
                                            ชื่อพนักงาน
                                        </th>
                                        <?php foreach ($modelEmp as $key => $value) { ?>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-sort="ascending">
                                                <?php echo $value ['Fullname']; ?>
                                            </th>
                                        <?php } ?>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                            colspan="1" aria-sort="ascending" width="10%">
                                            รวม
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($statusSSO) { ?>
                                        <tr>
                                            <td>ประกันสังคม</td>
                                            <?php

                                            foreach ($modelEmp as $key2 => $values) {
                                                ?>
                                                <td align="right"><?php
                                                    $sumSSO = '0.00';
                                                    foreach ($modelSSO AS $key => $valueResultSSO) {
                                                        switch ($valueResultSSO['WAGE_EMP_ID']) {
                                                            case $valueResultSSO['WAGE_EMP_ID'] == $values['ID_Card'];
                                                                $sumSSO = $valueResultSSO['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                                                                break;
                                                        }

                                                    }
                                                    echo Helper::displayDecimal($sumSSO);
                                                    $somrowsso[] = $sumSSO;

                                                    ?></td>
                                            <?php } ?>
                                            <td><?php echo Helper::displayDecimal(array_sum($somrowsso)); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php foreach ($modelTemp as $key1 => $value) {
                                        $somrow = [];
                                        ?>
                                        <tr>
                                            <td><?php echo $value['ADD_DEDUCT_TEMPLATE_NAME']; ?></td>
                                            <?php foreach ($modelEmp as $key2 => $values) { ?>
                                                <td align="right"><?php
                                                    $setZero = "0.00";
                                                    foreach ($modelAdddeductdtail AS $key => $valueResult) {
                                                        switch ($valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] AND $valueResult['WAGE_EMP_ID']) {
                                                            case $valueResult['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $value['ADD_DEDUCT_TEMPLATE_ID'] && $valueResult['WAGE_EMP_ID'] == $values['ID_Card'];
                                                                $setZero = $valueResult['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                                                                break;
                                                        }

                                                    }
                                                    echo Helper::displayDecimal($setZero);
                                                    $somrow[] = $setZero;

                                                    ?></td>
                                            <?php } ?>
                                            <td><?php echo Helper::displayDecimal(array_sum($somrow)); ?></td>
                                        </tr>
                                        <?php ?>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane tabselect" id="tab6">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <form class="form-horizontal" id="formSearchOT" action="reportdetailadddeductdetail"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid[]"
                                                        multiple="multiple">
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        <br>
                                        &nbsp;
                                        &nbsp;
                                        <label>รายการเพิ่ม</label>
                                        &nbsp;
                                        &nbsp;
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="idtemp[]"
                                                        multiple="multiple">
                                                        <option value="S1">เงืนเดือน</option>
                                                    <?php $working = ApiPayroll::listdata_add_deduct_template();
                                                    foreach ($working as $value) {
                                                        echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                    } ?>
                                                </select>
                                            </span>
                                        &nbsp;
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" class="form-control monthselect" name="monthselect"
                                               style="width:130px">
                                        &nbsp;
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-4">
                            </div>

                        </div>
                        <?php if ($querySalaryTotalThisMonth6) { ?>
                            <div class="row">
                                <div class="row">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดรายงานเงินเดือน</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดส่วนเพิ่มตามรายการ (รายการเพิ่ม) จ่าย วันที่ </h3>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="exportpdfreportdetailadddeductdetail">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>
                                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                                    <?php foreach ($modelTemp AS $valueTemp) {
                                        $ADD_DEDUCT_TEMPLATE_ID = $valueTemp['ADD_DEDUCT_TEMPLATE_ID'];
                                        ?>
                                        <tr>
                                            <td><h4>รายงานการเพิ่ม <?php echo $valueTemp['ADD_DEDUCT_TEMPLATE_NAME']; ?>
                                                    จ่ายวันที่ </h4></td>
                                        </tr>
                                        <?php
                                        $working_companyname = "";
                                        foreach ($modelWorkingcompany as $valueCompany) {

                                            if ($valueCompany['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $ADD_DEDUCT_TEMPLATE_ID) {
                                                $working_companyname = $valueCompany['working_companyname'];

                                                ?>
                                                <tr>
                                                    <td><h5>บริษัท <?php echo $working_companyname; ?>
                                                            [ <?php echo $valueCompany['short_name']; ?> ]</h5></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table id="example1"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example1_wrapper">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="20%">
                                                                    ชื่อ - สกุล
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="10%">
                                                                    แผนก
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="10%">
                                                                    จำนวนเงิน
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="10%">
                                                                    รายละเอียด
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $sumtotal = [];
                                                            foreach ($modelAdddeductdtail as $valueAdddeduct) {
                                                                if ($valueAdddeduct['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $ADD_DEDUCT_TEMPLATE_ID &&
                                                                    $valueAdddeduct['WAGE_WORKING_COMPANY'] == $valueCompany['working_companyid']
                                                                ) {
                                                                    // echo $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_TMP_ID'];
                                                                    // echo "<br>";
                                                                    // echo $valueAdddeduct['WAGE_WORKING_COMPANY'];
                                                                    $sumtotal[] = $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_AMOUNT'];; ?>
                                                                    <tr>
                                                                        <td><?php echo $valueAdddeduct['NAMEEMP']; ?></td>
                                                                        <td><?php echo $valueAdddeduct['departmentname']; ?></td>
                                                                        <td align="right"><?php echo Helper::displayDecimal($valueAdddeduct['ADD_DEDUCT_THIS_MONTH_AMOUNT']); ?></td>
                                                                        <td><?php echo $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_DETAIL']; ?></td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                <td colspan="2" align="right"><b>รวม</b></td>
                                                                <td align="right"><?php echo Helper::displayDecimal(array_sum($sumtotal)); ?></td>
                                                                <td></td>
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php }
                                        } ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane tabselect" id="tab7">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <form class="form-horizontal" id="formSearchOT" action="reportdetaildeductdetail"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid[]"
                                                        multiple="multiple">
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        <br>
                                        &nbsp;
                                        &nbsp;
                                        <label>รายการหัก</label>
                                        &nbsp;
                                        &nbsp;
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="idtemp[]"
                                                        multiple="multiple">
                                                      <option value="SSO1">ประกันสังคม</option>
                                                    <?php $working = ApiPayroll::getDataDeductTemp();
                                                    foreach ($working as $value) {
                                                        echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                                    } ?>
                                                </select>
                                            </span>
                                        &nbsp;
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" name="selectmonth" class="form-control monthselect"
                                               style="width:130px">
                                        &nbsp;
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-4">
                            </div>
                        </div>
                        <?php if ($querySalaryTotalThisMonth7) { ?>
                            <div class="row">
                                <div class="row">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดรายงานเงินเดือน</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <h3>รายละเอียดส่วนหักตามรายการ (รายการหัก) จ่าย วันที่ </h3>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="exportpdfreportdetaildeductdetail">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>
                                <?php if ($statusSSO) { ?>
                                    <h4>ประกันสังคม จ่ายวันที่</h4>
                                    <?php foreach ($modelWorkingcompanysso as $valuecomsso) {
                                        $modelidcompanyssp = $valuecomsso['working_companyid'];
                                        ?>
                                        <table width="100%" border="0" cellpadding="5" cellspacing="0">
                                            <tr>
                                                <td><h5><?php echo $valuecomsso['working_companyname']; ?>
                                                        [ <?php echo $valuecomsso['short_name']; ?> ]</h5></td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table id="example1"
                                                           class="table table-bordered table-hover dataTable"
                                                           role="grid" aria-describedby="example1_wrapper">
                                                        <thead>
                                                        <tr role="row">
                                                            <th class="sorting_asc" tabindex="0"
                                                                aria-controls="example1" rowspan="1" colspan="1"
                                                                aria-sort="ascending" width="20%">
                                                                ชื่อ - สกุล
                                                            </th>
                                                            <th class="sorting_asc" tabindex="0"
                                                                aria-controls="example1" rowspan="1" colspan="1"
                                                                aria-sort="ascending" width="10%">
                                                                แผนก
                                                            </th>
                                                            <th class="sorting_asc" tabindex="0"
                                                                aria-controls="example1" rowspan="1" colspan="1"
                                                                aria-sort="ascending" width="10%">
                                                                จำนวนเงิน
                                                            </th>
                                                            <th class="sorting_asc" tabindex="0"
                                                                aria-controls="example1" rowspan="1" colspan="1"
                                                                aria-sort="ascending" width="10%">
                                                                รายละเอียด
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <?php foreach ($modelSSO as $keysso => $valuemodelsso) {
                                                            if ($valuemodelsso['WAGE_WORKING_COMPANY'] == $modelidcompanyssp) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $valuemodelsso['NAMEEMP']; ?></td>
                                                                    <td><?php echo $valuemodelsso['departmentname']; ?></td>
                                                                    <td><?php echo $valuemodelsso['ADD_DEDUCT_THIS_MONTH_AMOUNT']; ?></td>
                                                                    <td><?php echo $valuemodelsso['ADD_DEDUCT_THIS_MONTH_DETAIL']; ?></td>
                                                                </tr>
                                                            <?php }
                                                        } ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <br>
                                    <?php } ?>
                                <?php } ?>
                                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                                    <?php foreach ($modelTemp AS $valueTemp) {
                                        $ADD_DEDUCT_TEMPLATE_ID = $valueTemp['ADD_DEDUCT_TEMPLATE_ID'];
                                        ?>
                                        <tr>
                                            <td><h4>รายงานการหัก <?php echo $valueTemp['ADD_DEDUCT_TEMPLATE_NAME']; ?>
                                                    จ่ายวันที่ </h4></td>
                                        </tr>
                                        <?php
                                        $working_companyname = "";
                                        foreach ($modelWorkingcompany as $valueCompany) {

                                            if ($valueCompany['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $ADD_DEDUCT_TEMPLATE_ID) {
                                                $working_companyname = $valueCompany['working_companyname'];

                                                ?>
                                                <tr>
                                                    <td><h5>บริษัท <?php echo $working_companyname; ?>
                                                            [ <?php echo $valueCompany['short_name']; ?> ]</h5></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table id="example1"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example1_wrapper">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="20%">
                                                                    ชื่อ - สกุล
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="10%">
                                                                    แผนก
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="10%">
                                                                    จำนวนเงิน
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example1" rowspan="1" colspan="1"
                                                                    aria-sort="ascending" width="10%">
                                                                    รายละเอียด
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $sumtotal = [];
                                                            foreach ($modelAdddeductdtail as $valueAdddeduct) {
                                                                if ($valueAdddeduct['ADD_DEDUCT_THIS_MONTH_TMP_ID'] == $ADD_DEDUCT_TEMPLATE_ID &&
                                                                    $valueAdddeduct['WAGE_WORKING_COMPANY'] == $valueCompany['working_companyid']
                                                                ) {
                                                                    // echo $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_TMP_ID'];
                                                                    // echo "<br>";
                                                                    // echo $valueAdddeduct['WAGE_WORKING_COMPANY'];
                                                                    $sumtotal[] = $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_AMOUNT'];; ?>
                                                                    <tr>
                                                                        <td><?php echo $valueAdddeduct['NAMEEMP']; ?></td>
                                                                        <td><?php echo $valueAdddeduct['departmentname']; ?></td>
                                                                        <td align="right"><?php echo Helper::displayDecimal($valueAdddeduct['ADD_DEDUCT_THIS_MONTH_AMOUNT']); ?></td>
                                                                        <td><?php echo $valueAdddeduct['ADD_DEDUCT_THIS_MONTH_DETAIL']; ?></td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                <td colspan="2" align="right"><b>รวม</b></td>
                                                                <td align="right"><?php echo Helper::displayDecimal(array_sum($sumtotal)); ?></td>
                                                                <td></td>
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php }
                                        } ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane tabselect" id="tab8">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-6">
                                <form class="form-horizontal" id="formSearchOT" action="reportbenefitfund"
                                      method="post">
                                    <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                                    <div class="box-body" style="display: inline-flex;">
                                        <label>บริษัท <span>*</span></label>
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid[]"
                                                        multiple="multiple">
                                                        <?php $working = ApiHr::getWorking_company();
                                                        foreach ($working as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        } ?>
                                                </select>
                                            </span>
                                        <br>
                                        <label>เลือกเดือน</label>
                                        &nbsp;
                                        &nbsp;
                                        <input type="text" name="selectmonth" class="form-control monthselect"
                                               style="width:130px">
                                        &nbsp;
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-sm">ค้นหา</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        <?php if ($querySalaryTotalThisMonth8) { ?>
                            <div class="row">
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <h3>รายละเอียดรายงานเงินเดือน</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-6">
                                        <?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                                        //print_r($getMonth);
                                        $monthGetReport8 = $getMonth['monthresult'];
                                        $resultMonthReport8 = Datetime::mappingMonth($monthGetReport8);
                                        $yearGetReport8 = $getMonth['year'];
                                        ?>
                                        <h3> รายงานเงินสะสม ประจำงวดจ่ายวันที่ <?php echo $resultMonthReport8;
                                            echo '&nbsp;&nbsp;' . $yearGetReport8; ?></h3>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="exportpdfreportbenefitfund">
                                        <button class="btn btn-primary btn-sm">ออก PDF</button>
                                    </a>
                                </div>
                                <?php
                                // echo "<pre>";
                                // print_r($modelbenefitbefore);
                                ?>
                                <?php foreach ($modelcompany as $value) { ?>
                                    <table width="100%" border="0" cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td><h5><?php echo $value['working_companyname']; ?>
                                                    [ <?php echo $value['short_name']; ?> ]</h5></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table id="example1" class="table table-bordered table-hover dataTable"
                                                       role="grid" aria-describedby="example1_wrapper">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1" aria-sort="ascending" width="20%">
                                                            ชื่อ - สกุล
                                                        </th>
                                                        <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1" aria-sort="ascending" width="10%">
                                                            เงินเดือน
                                                        </th>
                                                        <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1" aria-sort="ascending" width="10%">
                                                            เงินสะสม ณ เดือนปัจจุบัน
                                                        </th>
                                                        <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1" aria-sort="ascending" width="10%">
                                                            เงินสะสมยกมา
                                                        </th>
                                                        <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1" aria-sort="ascending" width="10%">
                                                            เงินสะสมรวมทั้งหมด
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $arrSumSalary = $arrSumBenefitamount = $arrSumBenefitBefore = $arrSumBenefitTotal = [];
                                                    foreach ($modelbenefit as $key => $values) {
                                                        if ($values['WAGE_WORKING_COMPANY'] == $value['working_companyid']) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $values['NAMEEMP']; ?></td>
                                                                <td align="right"><?php echo Helper::displayDecimal($values['WAGE_SALARY']);
                                                                    $arrSumSalary[] = $values['WAGE_SALARY'];
                                                                    ?></td>
                                                                <td align="right"><?php echo Helper::displayDecimal($values['BENEFIT_AMOUNT']);
                                                                    $arrSumBenefitamount[] = $values['BENEFIT_AMOUNT'];
                                                                    ?></td>
                                                                <td align="right"><?php
                                                                    $benefitbefore = '0.00';
                                                                    foreach ($modelbenefitbefore as $valuebefore) {
                                                                        if ($valuebefore['BENEFIT_EMP_ID'] == $values['BENEFIT_EMP_ID']) {
                                                                            $benefitbefore = $valuebefore['BENEFIT_TOTAL_AMOUNT'];
                                                                            $arrSumBenefitBefore[] = $valuebefore['BENEFIT_TOTAL_AMOUNT'];
                                                                        }
                                                                    }
                                                                    echo Helper::displayDecimal($benefitbefore);
                                                                    ?></td>
                                                                <td align="right"><?php echo Helper::displayDecimal($values['BENEFIT_TOTAL_AMOUNT']);
                                                                    $arrSumBenefitTotal[] = $values['BENEFIT_TOTAL_AMOUNT']
                                                                    ?></td>
                                                            </tr>
                                                        <?php } ?>

                                                    <?php } ?>
                                                    <tr>
                                                        <td><B>รวม</B></td>
                                                        <td align="right">
                                                            <B><?php echo Helper::displayDecimal(array_sum($arrSumSalary)); ?></B>
                                                        </td>
                                                        <td align="right">
                                                            <B><?php echo Helper::displayDecimal(array_sum($arrSumBenefitamount)); ?></B>
                                                        </td>
                                                        <td align="right">
                                                            <B><?php echo Helper::displayDecimal(array_sum($arrSumBenefitBefore)); ?></B>
                                                        </td>
                                                        <td align="right">
                                                            <B><?php echo Helper::displayDecimal(array_sum($arrSumBenefitTotal)); ?></B>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->