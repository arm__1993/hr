<div class="container_card" style="width:200px;float:left;margin-left:5px">
    <div class="card_block sizeA" 
    style="border: 1px solid green;
    height:311px;
    background-image:url('<?php echo $bgImp;?>');
    background-repeat: no-repeat;
    margin-top:5px; "
    >
        <img src="<?php echo $fullPathImg;?>" class="" alt="รูปพนักงาน" style="width:115px;margin-top:119px;margin-left:40px">
        <div class="emp_card_name syt1" style="font-size:16px;font-weight:bold;line-height: 20px;margin-top:5px;margin-left:30px"><?php echo $emp['Name'];?></div>
        <div class="emp_card_surename syt1" style="font-size:16px;font-weight:bold;line-height: 25px;margin-left:80px"><?php echo $emp['Surname'];?></div>
        <div class="emp_card_position_name syt1" style="font-size:16px;font-weight:bold;line-height: 30px;width:100%;text-align:center"><?php echo $emp['positionName'];?></div>
    </div>
    <div class="card_back_block sizeA" 
         style="border: 1px solid green;
        height:311px;
        background-image:url('<?php echo $bgImpBack;?>');
        background-repeat: no-repeat;
        margin-top:5px;"
        >
        <div class="text_back_card" style="font-size:10px;font-weight:bold;margin-top:40px;line-height: 18px;margin-left:5px">
            <div>1.กรุณาติดบัตรทุกครั้งเมื่อปฏิบัติงาน</div>
            <div>2.บัตรนี้ใช้ได้เฉพาะเจ้าของบัตรนี้เท่านั้น</div>
            <div>3.บัตรนี้เป็นสมบัติของกลุ่มนกเงือกบริการ</div>
            <div>ท่านผู้ได้เก็บได้โปรดนำส่งคืนกลุ่มนกเงือกบริการ</div>
            <div>4.หากรูดบัตรไม่ได้กรุณาติดต่อฝ่ายบุคคล</div>
            <div>ด้วยตัวเองเท่านั้น</div>
            <div>5.ห้ามดัดแปลงหรือแก้ไขบัตร ทุกกรณี</div>
            <div>6.บริษัทขอสงวนสิทธิ์ในการยกเลิกบัตร</div>
            <div>โดยไม่ต้องแจ้งล่วงหน้า</div>

            <img src="data:image/png;base64,<?php echo $emp_barcode;?>" style="width: 190px;margin-top: 40px;" class="" alt="barcode" >
        </div>
    </div>
</div>