<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "tax_income_type".
 *
 * @property integer $id
 * @property string $taxincome_type
 * @property integer $status_active
 * @property string $createby_user
 * @property string $create_datetime
 * @property string $updateby_user
 * @property string $update_datetime
 */
class TaxIncomeType extends \yii\db\ActiveRecord
{
    protected $_pageSize;

    function __construct() {
        $this->_pageSize = Yii::$app->params['PAGE_SIZE'];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_income_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taxincome_type'], 'required'],
            [['status_active'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['taxincome_type'], 'string', 'max' => 200],
            [['createby_user', 'updateby_user'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'taxincome_type' => 'Taxincome Type',
            'status_active' => 'Status Active',
            'createby_user' => 'Createby User',
            'create_datetime' => 'Create Datetime',
            'updateby_user' => 'Updateby User',
            'update_datetime' => 'Update Datetime',
        ];
    }

    /**
     * @inheritdoc
     * @return TaxIncomeTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaxIncomeTypeQuery(get_called_class());
    }
    public function search($params)
    {

        $query = TaxIncomeType::find()->where(['!=','status_active','99']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>$this->_pageSize,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'taxincome_type', $this->taxincome_type]);

        return $dataProvider;
    }
}
