<?php

namespace app\modules\wsservice;

/**
 * wsservice module definition class
 */
class Wsservice extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\wsservice\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
