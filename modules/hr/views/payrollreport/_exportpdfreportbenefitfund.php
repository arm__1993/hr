<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;
$modelbenefit = $session->get('modelbenefit');
$modelcompany = $session->get('modelcompany');
$monthselect = $session->get('monthselect');
$modelbenefitbefore = $session->get('modelbenefitbefore');


?>
<h3 style="text-align:center;">รายละเอียดรายงานเงินเดือน</h3>

<div class="row">
    <div class="row">
    </div>
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-6">
            <h3><?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                //print_r($getMonth);
                $monthGetReport1 = $getMonth['monthresult'];
                $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
                $yearGetReport1 = $getMonth['year'];
                ?>รายงานสรุปรวมผลเงินเดือน จ่าย วันที่ <?php echo $resultMonthReport1;
                echo '&nbsp;&nbsp;' . $yearGetReport1; ?></h3>
        </div>
    </div>
    <?php foreach ($modelcompany as $value) { ?>
        <table width="100%" border="0" cellpadding="5" cellspacing="0">
            <tr>
                <td><h5><?php echo $value['working_companyname']; ?>[ <?php echo $value['short_name']; ?> ]</h5></td>
            </tr>
            <tr>
                <td>
                    <table id="example1" class="table table-bordered table-hover dataTable" role="grid"
                           aria-describedby="example1_wrapper">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                aria-sort="ascending" width="20%">
                                ชื่อ - สกุล
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                aria-sort="ascending" width="10%">
                                เงินเดือน
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                aria-sort="ascending" width="10%">
                                เงินสะสม ณ เดือนปัจจุบัน
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                aria-sort="ascending" width="10%">
                                เงินสะสมยกมา
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                aria-sort="ascending" width="10%">
                                เงินสะสมรวมทั้งหมด
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $arrSumSalary = $arrSumBenefitamount = $arrSumBenefitBefore = $arrSumBenefitTotal = [];
                        foreach ($modelbenefit as $key => $values) {
                            if ($values['WAGE_WORKING_COMPANY'] == $value['working_companyid']) {
                                ?>
                                <tr>
                                    <td><?php echo $values['NAMEEMP']; ?></td>
                                    <td align="right"><?php echo Helper::displayDecimal($values['WAGE_SALARY']);
                                        $arrSumSalary[] = $values['WAGE_SALARY'];
                                        ?></td>
                                    <td align="right"><?php echo Helper::displayDecimal($values['BENEFIT_AMOUNT']);
                                        $arrSumBenefitamount[] = $values['BENEFIT_AMOUNT'];
                                        ?></td>
                                    <td align="right"><?php
                                        $benefitbefore = '0.00';
                                        foreach ($modelbenefitbefore as $valuebefore) {
                                            if ($valuebefore['BENEFIT_EMP_ID'] == $values['BENEFIT_EMP_ID']) {
                                                $benefitbefore = $valuebefore['BENEFIT_TOTAL_AMOUNT'];
                                                $arrSumBenefitBefore[] = $valuebefore['BENEFIT_TOTAL_AMOUNT'];
                                            }
                                        }
                                        echo Helper::displayDecimal($benefitbefore);
                                        ?></td>
                                    <td align="right"><?php echo Helper::displayDecimal($values['BENEFIT_TOTAL_AMOUNT']);
                                        $arrSumBenefitTotal[] = $values['BENEFIT_TOTAL_AMOUNT']
                                        ?></td>
                                </tr>
                            <?php } ?>

                        <?php } ?>
                        <tr>
                            <td><B>รวม</B></td>
                            <td align="right"><B><?php echo Helper::displayDecimal(array_sum($arrSumSalary)); ?></B>
                            </td>
                            <td align="right">
                                <B><?php echo Helper::displayDecimal(array_sum($arrSumBenefitamount)); ?></B></td>
                            <td align="right">
                                <B><?php echo Helper::displayDecimal(array_sum($arrSumBenefitBefore)); ?></B></td>
                            <td align="right">
                                <B><?php echo Helper::displayDecimal(array_sum($arrSumBenefitTotal)); ?></B></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <br>
    <?php } ?>

</div>
