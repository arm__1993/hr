<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 18:07
 */

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiPersonal
{

    public static function getTechnicianPerson($str_year,$technician_id)
    {
        //$sql_TcPersona หาประสิทธิภาพช่างรายบุคคล
       /* $sql_TcPersona = "SELECT a.technician_id, a.technician_name, SUM(c.novat_wage), SUM(c.time)
                            FROM erepair_send_all_mechanic as a 
                            inner join drepair_inform as b on b.cvhc_main_id = a.cvhc_main_id 
                            inner join drepair_inform_list as c on c.drepair_inform_id = b.id 
                            where a.erepair_job_type_id = 3 
                            and DATE_FORMAT(FROM_UNIXTIME(a.stop_time_job),'%Y-%m-%d') LIKE '%$str_year%' 
                            and a.technician_id = '$technician_id' and a.status = '3' and a.status_active = '1' and c.status_repair = '2' and c.status <> '99' ";*/

        $sql_TcPersona = "SELECT
                            a.technician_id ,
                            a.technician_name ,
                            SUM(c.novat_wage) ,
                            SUM(c.time)
                        FROM
                            erepair_send_all_mechanic as a
                        inner join drepair_inform as b on b.cvhc_main_id = a.cvhc_main_id
                        inner join drepair_inform_list as c on c.drepair_inform_id = b.id
                        where
                            a.erepair_job_type_id = 3
                        and YEAR(DATE_FORMAT(FROM_UNIXTIME(a.stop_time_job),'%Y-%m-%d'))=$str_year
                        and a.technician_id = '$technician_id'
                        and a. status = '3'
                        and a.status_active = '1'
                        and c.status_repair = '2'
                        and c. status <> '99' ";


        $technician_persona = Yii::$app->dbERP_service
            ->createCommand($sql_TcPersona)
            ->queryAll();
        return $technician_persona;
    }

    public static function getTechnicianName($technician_car)
    {

        //$sql_TcPersona2 หาชื่อ.....ประสิทธิภาพช่า
        $sql_TcPersona2 = 'SELECT 
        a.cvhc_main_id, 
        a.technician_id, 
        a.technician_name, 
        d.agroup_id 
        FROM erepair_send_all_mechanic a 
        inner join cvhc_main as b ON b.id = a.cvhc_main_id 
        inner join ahistory as c ON c.id = b.ahistory_id 
        inner join amodel as d ON d.id = c.amodel_id 

        where a.erepair_job_type_id = 3 
        and a.status = 3 and c.status_new = 33 
        and d.agroup_id in (' . $technician_car . ') 
        ORDER BY `d`.`agroup_id` ASC';
        $tc_manypeople = Yii::$app->dbERP_service
            ->createCommand($sql_TcPersona2)
            ->queryAll();

        $technician = array();
        foreach ($tc_manypeople as $people) {
            $technician[$people['technician_id']] = [$people['technician_name']];

        }

        return $technician;
    }


    public static function getTechnicianWageInYear($str_year)
    {
        // $sql_TcWage หาค่าแรงช่างโดยรวมของเดือนนั้นๆ
        /*$sql_TcWage = 'SELECT
        a.technician_id, 
        a.technician_name, 
        sum(c.time) as sumtime, 
        sum(c.novat_wage) as sumnovat_wage 

        FROM erepair_send_all_mechanic as a 

        inner join drepair_inform as b on b.cvhc_main_id = a.cvhc_main_id 
        inner join drepair_inform_list as c on c.drepair_inform_id = b.id 

        where a.erepair_job_type_id = 3 
        and a.status = 3 
        and DATE_FORMAT(FROM_UNIXTIME(a.stop_time_job),\'%Y-%m-%d\') 
        LIKE DATE_FORMAT(FROM_UNIXTIME(a.stop_time_job),' . $str_year . ') 
        and a.status_active = 1 
        and c.status_repair = 2 
        and c.status <> 99 group by `technician_id`';*/

        $sql_TcWage =" SELECT
            a.technician_id ,
            a.technician_name ,
            sum(c.time) as sumtime ,
            sum(c.novat_wage) as sumnovat_wage
        FROM
            erepair_send_all_mechanic as a
        INNER JOIN drepair_inform as b on b.cvhc_main_id = a.cvhc_main_id
        INNER JOIN drepair_inform_list as c on c.drepair_inform_id = b.id
        WHERE a.erepair_job_type_id = 3
        AND a. status = 3
        AND YEAR(DATE_FORMAT(FROM_UNIXTIME(a.stop_time_job) ,'%Y-%m-%d'))=$str_year
        AND a.status_active = 1
        AND c.status_repair = 2
        AND c. status <> 99
        GROUP BY technician_id ";

        $tc_wage = Yii::$app->dbERP_service
            ->createCommand($sql_TcWage)
            ->queryAll();


        $key_wage = array();
        foreach ($tc_wage as $wage) {
            $key_wage[$wage['technician_id']] = [$wage['sumtime'], $wage['sumnovat_wage']];

        }

        return $key_wage;
    }

    public static function getPersonalName($type_company,$technician_id,$str_start,$str_end)
    {
        /*$sql_persona = "SELECT technician_id, technician_name ,cvhc_main_id FROM erepair_send_all_mechanic
        where erepair_job_type_id = 3 and status = 3 
        and DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'%Y-%m-%d') 
        between DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'$str_start') 
        and DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'$str_end') 
        and technician_name = '$name' and branch_id = $type_company and status_active = 1";*/

        $sql_persona = "SELECT technician_id, technician_name ,cvhc_main_id FROM erepair_send_all_mechanic 
        where erepair_job_type_id = 3 and status = 3 
        and DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'%Y-%m-%d') 
        between DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'$str_start') 
        and DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'$str_end') 
        and technician_id = '$technician_id' and branch_id = '$type_company' and status_active = 1";

        $persona = Yii::$app->dbERP_service->createCommand($sql_persona)->queryAll();
        $NamePersona = [];
        foreach ($persona as $value) {
            $NamePersona[$value['cvhc_main_id']]['name'] = $value['technician_name'];
            $NamePersona[$value['cvhc_main_id']]['technician_id'] = $value['technician_id'];
        }

        return $NamePersona;

    }


    public static function getTime($time_start,$time_end)
    {
        //$sql_TimeAndId หาเวลา
        $sql_TimeAndId = "SELECT id as drepair_inform_id , a.cvhc_main_id as cvhc_main, TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME( a.end_time),'%Y-%m-%d %H:%i'), 
        DATE_FORMAT(FROM_UNIXTIME( a.start_time),'%Y-%m-%d %H:%i')) as time 
        FROM drepair_inform as a WHERE DATE_FORMAT(FROM_UNIXTIME(a.start_time),'%Y-%m-%d') 
        BETWEEN '$time_start' and '$time_end' ";
        $timeAndID = Yii::$app->dbERP_service->createCommand($sql_TimeAndId)->queryAll();
        $time=[];
        foreach ($timeAndID as $value) {
            $time[$value['drepair_inform_id']]['time'] = $value['time'];
            $time[$value['drepair_inform_id']]['cvhc_main'] = $value['cvhc_main'];
        }

        return $time;
    }


    public static function getAllWage($drepairID_MIN,$drepairID_MAX)
    {
        //$sql_WageAndId หาค่าแรง
        $sql_WageAndId = "SELECT id as id_drepair_inform_list, drepair_inform_id,novat_wage FROM `drepair_inform_list` 
        WHERE `drepair_inform_id` >= $drepairID_MIN and `drepair_inform_id` <= $drepairID_MAX 
        and status <> 99";
        $wageAndId = Yii::$app->dbERP_service->createCommand($sql_WageAndId)->queryAll();

        $wage=[];
        foreach ($wageAndId as $value) {
            $wage[$value['drepair_inform_id']][] = $value['novat_wage'];
        }
        return $wage;

    }


    public static function getTypeCar($type_company,$technician_TypeCar)
    {
        //$sql_technicianTypeCar หาช่างทั้งหมดที่ทำงานในประเภทรถต่างๆ (ช่างรถเล็ก , ช่างรถเล็ก)
        $sql_technicianTypeCar = "SELECT a.technician_id, a.technician_name, d.agroup_id 
        FROM erepair_send_all_mechanic a inner join cvhc_main as b 
        ON b.id = a.cvhc_main_id inner join ahistory as c 
        ON c.id = b.ahistory_id inner join amodel as d ON d.id = c.amodel_id 
        where a.erepair_job_type_id = 3 and a.status = 3 
        and c.status_new = 33 and d.agroup_id in ($technician_TypeCar) and a.branch_id = $type_company";
        $technicianTypeCar = Yii::$app->dbERP_service->createCommand($sql_technicianTypeCar)->queryAll();
        $typeCar=[];
        foreach ($technicianTypeCar as $value) {
            $typeCar[$value['technician_id']]['name'] = $value['technician_name'];
        }

        return $typeCar;

    }


    public static function getCompany($type_company,$str_start,$str_end)
    {
        //$sql_technicianCompany หาช่วงเวลาการทำงานของช่าง + บริษัท + cvhc_main_id
        $sql_technicianCompany = "SELECT cvhc_main_id,technician_id, technician_name 
        FROM erepair_send_all_mechanic 
        where erepair_job_type_id = 3 and status = 3 and DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'%Y-%m-%d') 
        between DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'$str_start') 
        and DATE_FORMAT(FROM_UNIXTIME(stop_time_job),'$str_end') 
        and branch_id = $type_company and status_active = 1";
        $technicianCompany = Yii::$app->dbERP_service->createCommand($sql_technicianCompany)->queryAll();
        $company=[];
        foreach ($technicianCompany as $value) {
            $company[$value['technician_id']]['cvhc_main_id'] = $value['cvhc_main_id'];
        }
        return $company;
    }



}