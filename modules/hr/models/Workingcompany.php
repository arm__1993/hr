<?php

namespace app\modules\hr\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
* This is the model class for table "working_company".
*
* @property integer $id
* @property string $code_name
* @property string $codeName_tax
* @property string $short_name
* @property string $name
* @property string $name_eng
* @property string $address
* @property string $Tel
* @property string $Fax
* @property string $soc_acc_number
* @property integer $status
* @property string $inv_number
* @property string $business_number
* @property string $numberbranch
* @property string $dealercode
* @property string $Code
* @property string $company_type
* @property string $branch_number
* @property string $branch_comment
* @property string $branch_salesman_cd
* @property string $BranchName_TIS
* @property string $show_in_report
* @property string $images_pdf_home
* @property string $images_pdf_isuzu
*/
class Workingcompany extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'working_company';
    }
    
    /**
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_OU');
    }
    
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
        [['code_name', 'short_name', 'name', 'address', 'company_type'], 'required'],
        [['address', 'images_pdf_home', 'images_pdf_isuzu'], 'string'],
        [['status'], 'integer'],
        [['code_name', 'codeName_tax'], 'string', 'max' => 2],
        [['short_name', 'Tel', 'Fax', 'inv_number', 'dealercode', 'Code'], 'string', 'max' => 20],
        [['name', 'branch_number', 'branch_comment'], 'string', 'max' => 250],
        [['name_eng', 'BranchName_TIS'], 'string', 'max' => 200],
        [['soc_acc_number'], 'string', 'max' => 13],
        [['business_number'], 'string', 'max' => 50],
        [['numberbranch', 'branch_salesman_cd'], 'string', 'max' => 10],
        [['company_type', 'show_in_report'], 'string', 'max' => 1],
        ];
    }
    
    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
        'id' => 'ID',
        'code_name' => 'Code Name',
        'codeName_tax' => 'Code Name Tax',
        'short_name' => 'Short Name',
        'name' => 'Name',
        'name_eng' => 'Name Eng',
        'address' => 'Address',
        'Tel' => 'Tel',
        'Fax' => 'Fax',
        'soc_acc_number' => 'Soc Acc Number',
        'status' => 'Status',
        'inv_number' => 'Inv Number',
        'business_number' => 'Business Number',
        'numberbranch' => 'Numberbranch',
        'dealercode' => 'Dealercode',
        'Code' => 'Code',
        'company_type' => 'Company Type',
        'branch_number' => 'Branch Number',
        'branch_comment' => 'Branch Comment',
        'branch_salesman_cd' => 'Branch Salesman Cd',
        'BranchName_TIS' => 'Branch Name  Tis',
        'show_in_report' => 'Show In Report',
        'images_pdf_home' => 'Images Pdf Home',
        'images_pdf_isuzu' => 'Images Pdf Isuzu',
        ];
    }
    public function companySearch($params)
    {
        $data = Workingcompany::find()
        ->where('status <> 99 ');
        $this->load($params);
        $data->andFilterWhere(['like','code_name',$this->code_name]);
        $data->andFilterWhere(['like','name',$this->name]);
        $data->andFilterWhere(['like','short_name',$this->short_name]);
        return $dataProvider = new ActiveDataProvider([
        'query' => $data,
        'pagination' => [
        'pageSize' => 10,
        ]
        ]);
        
    }
}