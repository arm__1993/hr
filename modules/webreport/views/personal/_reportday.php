<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;


AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>


<?php

$session = Yii::$app->session;

$totalTC = $session->get('totalTC');

$_arrRET = $session->get('_arrRET');

$namePersonas = $session->get('namePersonas');

$wagePersona = $session->get('wagePersona');

$timePersona = $session->get('timePersona');

?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานประสิทธิภาพช่างรายคน(รายวัน)</h3>

 <!-- Default box -->
    <div class="box">
              <h3 class="box-title">รายงานประสิทธิภาพช่างรายคน</h3>
              
                    <table bgcolor="#777">
                        <thead>
                            <tr>
                                <th bgcolor="#eee">
                                ประสิทธิภาพงานซ่อมของ
                                <?php echo $namePersonas['name']; ?>
                                </th>
                                <th bgcolor="#eee">
                                จันท์ - เสาร์
                                </th>
                                <th bgcolor="#eee">
                                อาทิตย์
                                </th>
                                <th bgcolor="#eee">
                                ลา (ชม.)
                                </th>
                                <th bgcolor="#eee">
                                OT (ชม.)
                                </th>
                                <th bgcolor="#eee">
                                รวมชั่วโมงการทำงาน
                                </th>
                            </tr>
                        </thead>

                        <tbody>                
                            <tr bgcolor="#fff">
                              <th bgcolor="#eee">จำนวนวันทำงาน (เดือน)</th>
                              <td>
                              <?php 
                                $workday = $_arrRET['total_workday'];
                                    if(empty($workday)){
                                        echo "0";
                                    }elseif ($workday) {
                                        echo $workday;
                                    }
                              ?>                                  
                              </td>
                              <td>
                              <?php 
                                $sunday = $_arrRET['total_sunday'];
                                    if(empty($sunday)){
                                        echo "0";
                                    }elseif ($sunday) {
                                        echo $sunday;
                                    }
                                ?>                                  
                              </td>
                              <td>
                              <?php
                                $leave = 8;
                                echo $leave;
                              ?>
                              </td>
                              <td>
                              <?php
                                $ot = 0;
                                echo $ot;
                              ?> 
                              </td>
                              <td>
                              <?php
                                    $workday = $_arrRET['total_workday'];
                                    $sunday = $_arrRET['total_sunday'];
                                    if(empty($workday||$sunday)){
                                        echo "0";
                                    }elseif ($workday||$sunday) {
                                        $workdays = $workday*8;
                                        $sundays = $sunday*8;
                                        $total_hour = ($workdays+$sundays)-$leave-$ot;
                                        echo ($total_hour);
                                    }
                                  ?>
                              </td>
                            </tr>
                            <tr bgcolor="#fff">
                              <th bgcolor="#eee">ชั่วโมงทำงานต่อวัน</th>
                              <td>8</td>
                              <td>8</td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
                            </tr>
                            <tr bgcolor="#fff">
                              <th bgcolor="#eee">จำนวนช่าง</th>
                              <td>1</td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
                            </tr>
                            <tr bgcolor="#fff">
                              <th bgcolor="#eee">จำนวนชั่วโมงทำงาน (เดือน)</th>
                              <td>
                                  <?php
                                    $workday = $_arrRET['total_workday'];
                                    if(empty($workday)){
                                        echo "0";
                                    }elseif ($workday) {
                                        $workdays = $workday*8;
                                        echo ($workdays);
                                    }
                                  ?>
                              </td>
                              <td> <?php 
                                $sunday = $_arrRET['total_sunday'];
                                    if(empty($sunday)){
                                        echo "0";
                                    }elseif ($sunday) {
                                        $sundays = $sunday*8;
                                        echo $sundays;
                                    }
                                ?>    
                              </td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td> 
                            </tr>                            
                            </tbody>
                    </table>
                        <h4>รายได้ค่าแรงของช่าง 1 คน&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                            echo ($wagePersona) ? Helper::displayDecimal($wagePersona) : 0;
                        ?><br>
                            ค่าแรงต่อชั่วโมงที่ขาย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        echo ($timePersona) ? Helper::displayDecimal($timePersona) : 0;
                        ?><br>
                            ค่าแรงต่อชั่วโมงที่ขายได้&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                            $percentwage = ($timePersona>0) ? ($wagePersona/$timePersona) : 0;
                            echo Helper::displayDecimal($percentwage);
                        ?><br>
                            ประสิทธิภาพงานซ่อมของช่าง 1 คน&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                            $performance = ($total_hour > 0) ? ($percentwage*100)/$total_hour : 0;
                            echo Helper::displayDecimal($performance).'%';
                        ?>
                        </h4>  

       
              <h3 class="box-title">รายงานประสิทธิภาพช่าง</h3>              
             
                    <table bgcolor="#777" >

                        <thead>
                            <tr>
                                <th bgcolor="#eee">
                                ที่
                                </th>
                                <th bgcolor="#eee">
                                ชื่อ - นามสกุล
                                </th>
                                <th bgcolor="#eee">
                                รวมชั่วโมงทำงาน
                                </th>
                                <th bgcolor="#eee">
                                รายได้ค่าแรง
                                </th>
                                <th bgcolor="#eee">
                                ประสิทธิภาพ
                                </th>
                                <th bgcolor="#eee">
                                ลา (ชม.)
                                </th>
                            </tr>
                        </thead>

                        <tbody > 
                        <?php
                        $item = 1;
                            foreach ($totalTC as $value) {                                
                                $fullname = ($value['name']);
                                $fullwage = ($value['wage']);
                                $checkTime = explode(":", $value['time']); 
                                $timeTc = ($checkTime[0] * 60) + $checkTime[1];
                                $fulltime = $timeTc; 
                            ?>
                                <tr bgcolor="#fff"> 
                                    <td><?php echo $item; ?></td>
                                    <td><?php echo $fullname; ?></td>
                                    <td><?php echo $fulltime; ?></td>
                                    <td><?php echo $fullwage; ?></td>
                                    <td>
                                    <?php
                                        $wageofsell = ($fulltime > 0) ? $fullwage/$fulltime : 0; 
                                        $percentfullwage = ($wageofsell*100)/240;
                                    echo Helper::displayDecimal($percentfullwage); ?>%
                                    </td>
                                    <td></td>                                 
                                </tr>
                            <?php $item++; } ?>
                        </tbody>                            
                    </table>
</div>
<!-- /.box -->








