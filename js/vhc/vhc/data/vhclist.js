$(document).ready(function () {
    ///ส่วนจ่าย///
    $("#btnSaveActivitylist").on("click", function () {
        var f = frmValidate('frmAddVhclist');
        if (!f) {
            // console.log('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy');
            showWarningInputForm();
        } else {
            $('.record_status').val('1');
            // console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            savelist(1);

        }
    });

    $('#btnAddNewVhclist').on("click", function () {
        initForm('btnSaveActivitylist');

    });

    ///ส่วนหัก///
    $("#btnSaveActivityserviceDeduct").on("click", function () {
        var f = frmValidate('frmDeduct');
        if (!f) {
            showWarningInputForm();
        } else {
            $('.record_status').val('2');
            savelist(2);
        }
    });

    $('#btnAddNewVhclist').on("click", function () {
        initForm('btnSaveActivitylist');
        $('#hide_activityedit_list').val('');
        $('#list_name').val('');
        $('#record_status_list').prop('checked', true);
    });
});

function deletevhclist(id, valselect) { //อันนี้ยกเลิกเน้อออออออออออออออออ
    // alert(id);
    $.ajax({
        url: 'deletevhclist',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showDeleteSuccess();
                $.pjax.reload({container: "#pjax_tb_list"});  //Reload GridView

                selecttab(valselect);
            } else {
                showDeleteError();
                console.log(data);

            }
        }
    });
}
function savelist(idselectform) {
    if (idselectform == 1) {
        var datavar = $('#frmAddVhcservice').serialize();
    } else {
        var datavar = $('#frmDeduct').serialize();
    }
    // console.log(datavar);
    $.ajax({
        url: 'savelist',
        data: datavar,
        type: 'POST',
        success: function (data) {
            if (parseInt(data) == 1) {
                showSaveSuccess();
                if (idselectform == 1) {
                    initForm('frmAddVhclist');
                } else {
                    initForm('frmDeduct');
                }

                selecttab(idselectform);
                //   $.pjax.reload({container: "#pjax_tb_vhcservicetype"});  //Reload GridView
            } else {
                showSaveError();
            }
        }
    });
}

function editlist(id) {//แก้ไขเน้อออออออออออ
    // console.log(id);
    initForm('frmAddVhclist');//idฟอมมมมม
    $.ajax({
        url: 'updatevhclist',
        data: {id: id,},
        type: 'POST',
        success: function (data) {
            //  console.log(data);
            //
            if(data.record_status==1){
                $('#record_status_list').prop('checked', true);
            }else {
                $('#record_status_list').prop('checked', false);
            }

            $('#hide_activityedit_list').val(data.id);
            $('#group_id').val(data.group_id);
            $('#list_name').val(data.list_name);
            $('#modalfrmAddVhclist').modal();
        }
    });
}

function savelist() {//อันนี้นี้เซฟเน้ออออออออออออออออ
    var datavar = $('#frmAddVhclist').serialize();

    console.log(datavar);
    $.ajax({
        url: 'savelist',
        data: datavar,
        type: 'POST',
        success: function (data) {
            console.log(data);
            if (parseInt(data) == 1) {
                showSaveSuccess();
                initForm('frmAddVhclist');
                $.pjax.reload({container: "#pjax_tb_list"});  //Reload GridView
            }
            else {
                showSaveError();
            }
        }
    });
}

function selecttab(val) {
    //alert("2222" + val);
    // console.log($("#tabselect" + val));
    $(".tabselect").removeClass("active");
    $("#tabselect" + val).addClass("active");
    $("#tab" + val).addClass("active");

    if (val == 1) {
        $.pjax.reload({container: "#pjax_tb_adddeducttemp"}); //Reload GridView
    } else {
        $.pjax.reload({container: "#pjax_tb_list"}); //Reload GridView
    }


}