<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "ot_requestdetail_return".
 *
 * @property integer $id
 * @property string $id_card
 * @property integer $ot_requestdetail_id
 * @property integer $return_id
 * @property string $return_name
 * @property string $return_total
 * @property string $return_datetime
 * @property integer $is_used
 * @property string $used_datetime
 */
class OtRequestdetailReturn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ot_requestdetail_return';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_card', 'ot_requestdetail_id', 'return_id', 'return_name', 'return_total', 'return_datetime', 'is_used'], 'required'],
            [['ot_requestdetail_id', 'return_id', 'is_used'], 'integer'],
            [['return_total'], 'number'],
            [['return_datetime', 'used_datetime'], 'safe'],
            [['id_card'], 'string', 'max' => 13],
            [['return_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_card' => 'Id Card',
            'ot_requestdetail_id' => 'Ot Requestdetail ID',
            'return_id' => 'Return ID',
            'return_name' => 'Return Name',
            'return_total' => 'Return Total',
            'return_datetime' => 'Return Datetime',
            'is_used' => 'Is Used',
            'used_datetime' => 'Used Datetime',
        ];
    }
}
