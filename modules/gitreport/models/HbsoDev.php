<?php

namespace app\modules\gitreport\models;

use Yii;

/**
 * This is the model class for table "hbso_dev".
 *
 * @property integer $id
 * @property string $dev_fullname
 * @property string $dev_nickname
 */
class HbsoDev extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hbso_dev';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbGit_Report');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dev_fullname'], 'string', 'max' => 250],
            [['dev_nickname'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dev_fullname' => 'Dev Fullname',
            'dev_nickname' => 'Dev Nickname',
        ];
    }

    /**
     * @inheritdoc
     * @return HbsoDevQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HbsoDevQuery(get_called_class());
    }
}
