-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- โฮสต์: localhost
-- เวลาในการสร้าง: 07 ม.ค. 2017  11:14น.
-- เวอร์ชั่นของเซิร์ฟเวอร์: 5.5.49-0ubuntu0.14.04.1
-- รุ่นของ PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- ฐานข้อมูล: `ERP_service`
--

-- --------------------------------------------------------

--
-- โครงสร้างตาราง `config_timestandard_sa`
--

CREATE TABLE IF NOT EXISTS `config_timestandard_sa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code_timestandard` char(2) CHARACTER SET utf8 NOT NULL COMMENT 'เลขโค๊ช',
  `detail_code` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT 'อธิบายความหมายโค๊ช',
  `time_standard` smallint(5) unsigned NOT NULL COMMENT 'เวลามาตรฐานของโค๊ช',
  `year_timestandard` smallint(5) unsigned NOT NULL COMMENT 'ปีมาตรฐานของโค๊ช',
  `status_timestandard` tinyint(3) unsigned NOT NULL COMMENT '1=ใช้งาน 99=ยกเลิก',
  `time_create` datetime NOT NULL COMMENT 'วันเวลาสร้างครั้งเเรก',
  `emp_create` int(11) NOT NULL COMMENT 'ผู้สร้างครั้งแรก',
  `updatetime_by` datetime NOT NULL COMMENT 'วันเวลาอัพเดตรข้อมูล',
  `update_by` int(11) NOT NULL COMMENT 'ผู้อัพเดตรข้อมูล',
  PRIMARY KEY (`id`),
  KEY `code_timestandard` (`code_timestandard`,`year_timestandard`,`emp_create`,`update_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
