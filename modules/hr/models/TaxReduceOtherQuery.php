<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[TaxReduceOther]].
 *
 * @see TaxReduceOther
 */
class TaxReduceOtherQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaxReduceOther[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaxReduceOther|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
