<?php

namespace app\modules\gitreport\models;

use Yii;

/**
 * This is the model class for table "hbso_issue".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $milestone_id
 * @property integer $dev_id
 * @property integer $status_id
 * @property integer $issue_total
 * @property integer $for_year
 * @property integer $for_month
 * @property string $create_datetime
 * @property string $create_by
 * @property string $update_datetime
 * @property string $update_by
 */
class HbsoIssue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hbso_issue';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbGit_Report');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'milestone_id', 'dev_id', 'status_id', 'issue_total', 'for_year', 'for_month'], 'integer'],
            [['create_datetime', 'update_datetime'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'milestone_id' => 'Milestone ID',
            'dev_id' => 'Dev ID',
            'status_id' => 'Status ID',
            'issue_total' => 'Issue Total',
            'for_year' => 'For Year',
            'for_month' => 'For Month',
            'create_datetime' => 'Create Datetime',
            'create_by' => 'Create By',
            'update_datetime' => 'Update Datetime',
            'update_by' => 'Update By',
        ];
    }

    /**
     * @inheritdoc
     * @return HbsoIssueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HbsoIssueQuery(get_called_class());
    }
}
