<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use app\api\Common;

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


/*** DEFAULT ROUTING **/
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags() ?>
    <title>บริษัท อีซูซุเชียงราย จำกัด</title>
    <?php $this->head() ?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->homeUrl;?>/css/hr/hr.css">
</head>


    <body class="hold-transition skin-green-light layout-top-nav">
    <?php $this->beginBody() ?>
    <!-- Site wrapper -->





    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="bb-alert alert alert-success" style="display:none;">
            <span>The examples populate this alert with dummy content</span>
        </div>
        <?php echo $content; ?>
    </div>
    <!-- /.content-wrapper -->
    <!-- =============================================== -->


    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.9.8
        </div>
        <strong>Copyright &copy; 2017 บริษัท อีซูซุเชียงราย จำกัด</strong> All rights reserved.
    </footer>


    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>