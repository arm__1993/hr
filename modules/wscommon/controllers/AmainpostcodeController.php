<?php

namespace app\modules\wscommon\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wscommon\models\Amainpostcode;
use yii\helpers\ArrayHelper;

class AmainpostcodeController extends ActiveController
{
 
    public $modelClass = 'app\modules\wscommon\models\Amainpostcode';


    public function behaviors()
    {
        return
            ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                ],
            ],
        ],
        parent::behaviors());
    }
    
    public function actionSearchpostcode()
    {

       
        $provinceid = \Yii::$app->request->get('provinceid');
        $amphurid = \Yii::$app->request->get('amphurid');
        $postcode = new Amainpostcode();
        $postcodeSearch = $postcode->searchpostcode($amphurid,$provinceid);
        return $postcodeSearch;
        //return 1;
    }


}
