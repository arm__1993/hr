<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;

use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPayroll;

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/accounting.config.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/hr.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/homedefault.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/slip/personalslip.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/taxhist/taxpersonal.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/sso/ssohistory.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/personalsalary/histdeduct.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);

//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/default/uploadcroppie.js?t='.time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/default/croppie.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/default/index.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/default/croppie.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$imgURL = Yii::$app->request->baseUrl . '/images/global';
$img33url = Yii::$app->request->baseUrl . '/images/wshr/pageNew.png';


$session = Yii::$app->session;
$session->open();  //open session
$USER_ID = $session->get('idcard');


//
//$front = imagecreatefrompng('upload/emp_img/employee_card/page.png');  //หน้าใหม่
//header('Content-type: image/jpeg');
//$emp_name =  Yii::$app->session->get('fullname'); //'วัชรพันธ์' . ' ' . 'ผัดดี'; // ชื่อ
//$Position = Yii::$app->session->get('positionname'); //'Developer'; // ตำแหน่งงาน
//$blackF = imagecolorallocate($front, 0, 0, 0);
//imagettftext($front, 35, 0, 230, 640, $blackF, "fonts/thsarabunnew_bold-webfont.ttf", $emp_name);//หน้าชื่อสกุล
//imagettftext($front, 30, 0, 235, 730, $blackF, "fonts/thsarabunnew_bold-webfont.ttf", $Position);//หน้าตำแหน่ง
//
//header('Content-type: image/png');
//$dir = 'upload/emp_img/emp_Card/'.$Position;
//if (!file_exists($dir)) {
//    mkdir($dir, 0777, true);
//}
//imagepng($front, 'upload/emp_img/emp_Card/'.'1'.'/page.png');
//imagedestroy($front);


?>


<style>
    .img-box {
        background-image: url("<?php echo $img33url ?>");
        background-size: 638px 1004px;
        /*left:300px;*/
        /*top:150px;*/
    }

</style>

<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-danger">
                <div class="box-body box-profile">
                    <!--                    <img class="profile-user-img img-responsive img-circle"-->
                    <!--                         src="--><?php //echo Yii::$app->session->get('photo'); ?><!--"-->
                    <!--                         alt="User profile picture">-->
                    <img class="profile-user-img img-responsive img-circle"
                         src="<?php echo Yii::$app->request->BaseUrl . '/upload/emp_img/Pictures_HyperL/' . $login . '.png' ?>"
                         style="margin:auto; display:block; text-align:center;">

                    <h3 class="profile-username text-center"><?php echo Yii::$app->session->get('fullname'); ?></h3>

                    <p class="text-muted text-center"><?php echo Yii::$app->session->get('positionname'); ?></p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>บริษัท</b> <a
                                    class="pull-right"><?php echo Yii::$app->session->get('companyname'); ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>ตำแหน่ง</b> <a
                                    class="pull-right"><?php echo Yii::$app->session->get('positionname'); ?></a>
                        </li>
                    </ul>

                    <button type="button" class="btn btn-danger btn-block btn-flat"><i class="fa fa-phone"></i><strong>
                            <?php echo Yii::$app->session->get('tel'); ?></strong></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">เกี่ยวกับฉัน</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-user margin-r-5"></i> ID Card</strong>
                    <p class="text-muted">
                        <?php echo \app\api\Utility::displayIDcard(Yii::$app->session->get('idcard')); ?>
                    </p>
                    <hr>
                    <strong><i class="fa fa-signal margin-r-5"></i> อายุ </strong>
                    <p class="text-muted">
                        <?php echo DateTime::calculateAge(Yii::$app->session->get('birthday')); ?> ปี
                    </p>
                    <hr>
                    <strong><i class="fa fa-book margin-r-5"></i> การศึกษา</strong>

                    <p class="text-muted">
                        <?php echo ApiHr::getEducation(); ?>
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> ที่อยู่ปัจจุบัน</strong>
                    <p class="text-muted"><?php echo ApiHr::getCurAddress(); ?></p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> ภูมิลำเนา</strong>
                    <p class="text-muted"><?php echo ApiHr::getHomeAddress(); ?></p>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <!--<li><a href="#activity" data-toggle="tab">ข้อมูลส่วนตัว</a></li>-->
                    <li class="active"><a href="#timeline" data-toggle="tab">ประวัติการทำงาน</a></li>
                    <li><a href="#E_Card" id="E_cop" data-toggle="tab" data-have-contend="no">สร้างบัตรพนักงาน</a></li>
                    <li><a href="#slip" data-toggle="tab" onclick="loadPersonalslip(this)" data-have-contend="no">สลิปเงินเดือน</a>
                    </li>
                    <li><a href="#ssohistory" data-toggle="tab" id="ssohistory_tab10" onclick="loadSsohistory(this)"
                           data-have-contend="no">ประวัตินำส่งประกันสังคม</a></li>
                    <li><a href="#taxhistory" data-toggle="tab" onclick="loadPersonaltaxtsearch(this)"
                           data-have-contend="no">ประวัตินำส่งภาษี</a></li>
                    <!-- <li><a href="#histdeduct" data-toggle="tab"  onclick="loadPersonalhisdeduct(this)"  data-have-contend="no">ประวัติการหักเงิน</a></li> -->
                    <li><a href="#settings" data-toggle="tab">ตั้งค่า & เปลี่ยนรหัสผ่าน</a></li>
                </ul>
                <div class="tab-content">
                    <input type="hidden" value="<?php echo $USER_ID; ?>" id="id_card_ref"/>
                    <!--
                    <div class="tab-pane" id="activity">
                            ฟหกดฟหก
                    </div>
                    -->
                    <!-- /.tab-pane -->
                    <div class="active tab-pane" id="timeline">
                        <!-- The timeline -->
                        <ul class="timeline timeline-inverse">
                            <!-- timeline time label -->
                            <?php
                            $data = ApiPayroll::Getworkhis();
                            foreach ($data as $key => $value) {
                                ?>
                                <li class="time-label">
                                <span class="bg-red">
                                ตั้งแต่ <?php echo DateTime::CalendarDate($value['work_since_full'], "d/m/Y"); ?>
                                    - <?php echo DateTime::CalendarDate($value['work_until_full'], "d/m/Y"); ?>
                                </span>
                                </li>
                                <!-- /.timeline-label -->
                                <!-- timeline item -->
                                <li>
                                    <i class="fa fa-check-circle bg-blue"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header"><a
                                                    href="#">ชื่อตำแหน่ง <?php echo $value['old_work_position'] ?></a>
                                            ชื่อบริษัท <?php echo $value['old_work_company'] ?></h3>

                                        <div class="timeline-body"> <?php echo $value['work_detail'] ?>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                            <!-- END timeline item -->


                            <!-- END timeline item -->
                            <li>
                                <i class="fa fa-clock-o bg-gray"></i>
                            </li>
                        </ul>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="slip">
                        <div class="box-body">
                            <h2>รอสักครู่..</h2>
                            <?php //echo Yii::$app->request->baseUrl;?>
                        </div>
                    </div>
                    <div class="tab-pane" id="E_Card">
                        <div class="box-body">


                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="col-md-12" style="padding-top:30px;">
                                        <strong>Select Image:</strong>
                                        <br/>
                                        <input type="file" id="upload">
                                        <br/>
                                        <button class="btn btn-success upload-result">Upload Image</button>
                                    </div>


                                    <div class="col-md-12 text-center">
                                        <div id="upload-demo" style="width:350px"></div>
                                        <input name="name" type="hidden" class="form-control" id="inputName"
                                               value="<?php echo Yii::$app->session->get('fullname'); ?>">
                                        <input name="Last" type="hidden" class="form-control" id="Last"
                                               value="<?php echo Yii::$app->session->get('positionname'); ?>">
                                    </div>
                                </div>

                                <div class="col-md-6 " style="">
                                    <div class="img-box"
                                         style="width:638px; padding-top:206px;padding-left:232px;height:1004px;margin-top:1px;">
                                        <div id="upload-demo-i">

                                        </div>

                                        <b>
                                            <div id="inputNameview" style="padding-top:60px;font-size: 50px"></div>
                                            <div id="Lastview"
                                                 style=" padding-left: 25px; padding-top:30px;font-size: 40px"></div>
                                        </b>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                    <div class="tab-pane" id="ssohistory">

                        <div class="box-body">
                            <h2>รอสักครู่..</h2>
                            <?php //echo Yii::$app->request->baseUrl;?>
                        </div>
                    </div>
                    <div class="tab-pane" id="taxhistory">
                        <div class="box-body">
                            <h2>รอสักครู่..</h2>
                        </div>
                    </div>
                    <div class="tab-pane" id="histdeduct">
                        <div class="box-body">
                            <h2>รอสักครู่..</h2>
                            <?php //echo Yii::$app->request->baseUrl;?>
                        </div>
                    </div>
                    <div class="tab-pane" id="settings">
                        <form class="form-horizontal" id="formChangePwd" action="#">
                            <div class="form-group">
                                <label for="oldpassword" class="col-sm-4 control-label">รหัสผ่านเดิม</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="oldpassword" name="oldpassword"
                                           placeholder="รหัสผ่านเดิม">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="newpassword" class="col-sm-4 control-label">รหัสผ่านใหม่</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="newpassword" name="newpassword"
                                           placeholder="รหัสผ่านใหม่">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cnewpassword" class="col-sm-4 control-label">รหัสผ่านใหม่
                                    (ยืนยันอีกครั้ง)</label>

                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="cnewpassword" name="cnewpassword"
                                           placeholder="รหัสผ่านใหม่ (ยืนยันอีกครั้ง)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="button" id="saveChangePwd" class="btn btn-primary"><i
                                                class="fa fa-save"></i> ยืนยัน
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->





</section>