<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\modules\webreport\apiwebreport\ApiReport;
use app\api\Common;

AppAsset::register($this);

//$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    // $('#linklogout, #alogout').on("click",function(){
    //    bootbox.confirm({
    //         size: "small",
    //         message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
    //         callback: function(result){
    //             if(result==1) {
    //                 window.location.href='index.php?r=login/logout';
    //             }
    //         }
    //       });
    // });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>


<h3><i class="fa fa-fw fa-file-text"></i>รายงานรายได้ค่าแรงศูนย์บริการ</h3>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <form method="post" action="receivewage">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                <div class="col-md-12 col-md-offset-1">
                    <label>บริษัท</label>
                    <div class="btn-group">
                        <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key=>$value) {
                                    $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                ?>
                        </select>
                    </div>
                    &nbsp;&nbsp;

                    <label>เลือกปี</label>
                    <div class="btn-group">
                    <span class="multiselect-native-select">
                        <select class="form-control" name="year[]" id="example-getting-started" multiple="multiple">
                                <?php
                                $get_year = ApiReport::getAppYear();
                                foreach ($get_year as $key=>$value) {
                                    $sel_year = ($selected_year==$key) ? ' selected="selected" ' : '';
                                   echo '<option value="'.$key.'" '.$sel_year.'>'.$value.'</option>';
                                }
                                ?>
                        </select>
                    </span>
                    </div>
                    &nbsp;&nbsp;
                    <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                    </div>
                    &nbsp;&nbsp;
                    <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/receivewage/btn_report">
                            <button type="button" class="btn btn-success">
                                <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                            </button>
                        </a>
                    </div>
                </div>
                <!-- /.col-md-12 -->
            </form>
        </div>
    </div>
    <!-- /.box-body -->


    <?php if($query) { ?>
    <?php
    $arrMonth = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
    ?>

    <?php if (!empty($checkarray)) { ?>
        <?php
        /*<!--------------------table--------------------->*/

        $arrKeyYear = array_keys($checkarray);
        $arrValue = [];
        foreach ($arrKeyYear as $item) {
            foreach ($checkarray[$item] as $val) {
                $arrValue[$item][] = $val;
            }
        } ?>
    
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA TABLE -->
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">ตารางเปรียบเทียบค่าแรงต่อปี</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-striped" font color="#000000">
                                <thead>
                                <tr>
                                    <th>เดือน</th>
                                    <?php for ($col = 0; $col <= count($arrKeyYear) - 1; $col++) { ?>
                                        <th>รายได้ค่าแรงต่อปี <?php print_r($arrKeyYear[$col]); ?>
                                        </th>
                                    <?php } ?>
                                    <th> ยอดเป้าค่าแรงปี <?php echo (date("Y"))?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($row = 0; $row < 12; $row++) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $arrMonth[$row] ?>
                                        </td>
                                        <?php for ($col = 0; $col < count($arrKeyYear); $col++) { ?>
                                            <td>
                                                <?php if (isset($arrValue[$arrKeyYear[$col]][$row])) { ?>
                                                    <?php echo(Helper::displayDecimal($arrValue[$arrKeyYear[$col]][$row])) ?>
                                                <?php } else if (!isset($arrValue[$arrKeyYear[$col]][$row])) { ?>
                                                    <?php echo "0.00 " ?>
                                                <?php } ?>
                                            </td>
                                        <?php } ?>
                                        <td>
                                            <?php if (isset($arrDataAVG[$row])) { ?>
                                                <?php echo(Helper::displayDecimal($arrDataAVG[$row])) ?>
                                            <?php } else if (!isset($arrDataAVG[$row])) { ?>
                                                <?php echo "0.00 " ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box-success-->
                </div>
            </div>
        </section>
    <?php } else { ?>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA TABLE -->
                      

                    <div class="box box-success">
                        <div class="box-header">

                            <h3 class="box-title">ตารางเปรียบเทียบค่าแรงต่อปี</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>เดือน</th>
                                    <th>รายได้ค่าแรงต่อปี</th>
                                    <th> ยอดเป้าค่าแรงปี</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($row = 0; $row < 12; $row++) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $arrMonth[$row] ?>
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            0
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    
                    <!-- /.box-success -->
                </div>
            </div>
        </section>
    <?php } ?>

    <?php if (!empty($checkarray)) { ?>
        <?php
        /*<!--------------------highchart------------------->*/
        $arrData = $arrAVG = $arrDataSeries = [];
        $arrKeyYear = array_keys($checkarray);
        foreach ($arrKeyYear as $item) {

            $arrhighchart = [];
            foreach ($checkarray[$item] as $val) {
                $arrhighchart[] = (float)$val;

            }

            $arrData = [
                'type' => 'column',
                'name' => $item,
                'data' => $arrhighchart,
            ];

            array_push($arrDataSeries, $arrData); //Add array to Series
        }

        foreach ($arrDataAVG as $val) {
            $arrAVG[] = (float)$val;

        }

        //array average
        $avgLine = [
            'type' => 'spline',
            'name' => 'ยอดเป้าค่าแรง',
            'data' => $arrAVG,
            'marker' => [
                'lineWidth' => 2,
                'lineColor' => 'Highcharts.getOptions().colors[3]',
                'fillColor' => 'red',
            ]
        ];

        array_push($arrDataSeries, $avgLine);//Add AVG to series
        ?>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">

                                        <?php
                                        echo Highcharts::widget([
                                            'options' => [
                                                'title' => [
                                                    'text' => ''
                                                ],
                                                'xAxis' => [
                                                    'categories' => $arrMonth,
                                                ],
                                                'labels' => [
                                                    'items' => [
                                                        'html' => 'Total fruit consumption',
                                                        'style' => [
                                                            'left' => '50px',
                                                            'top' => '18px',
                                                            'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                        ]
                                                    ]
                                                ],
                                                'credits' => [
                                                    'enabled' => false
                                                ],
                                                'series' => $arrDataSeries
                                            ]
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box-info -->
                </div>
            </div>
        </section>

    <?php } else { ?>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                   
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php
                                        echo Highcharts::widget([
                                            'options' => [
                                                'title' => [
                                                    'text' => ''
                                                ],
                                                'xAxis' => [
                                                    'categories' => ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม']
                                                ],
                                                'labels' => [
                                                    'items' => [
                                                        'html' => 'Total fruit consumption',
                                                        'style' => [
                                                            'left' => '50px',
                                                            'top' => '18px',
                                                            'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                        ]
                                                    ]
                                                ],
                                                'credits' => [
                                                    'enabled' => false
                                                ],
                                                'series' => [
                                                    [
                                                        'type' => 'column',
                                                        'name' => '2014',
                                                        'data' => [3, 2, 1, 3, 4]
                                                    ],
                                                    [
                                                        'type' => 'column',
                                                        'name' => '2015',
                                                        'data' => [2, 3, 5, 7, 6]
                                                    ],
                                                    [
                                                        'type' => 'column',
                                                        'name' => '2016',
                                                        'data' => [4, 3, 3, 9, 0]
                                                    ],
                                                    [
                                                        'type' => 'spline',
                                                        'name' => 'ยอดเป้าค่าแรง',
                                                        'data' => [3, 2.67, 3, 6.33, 3.33],
                                                        'marker' => [
                                                            'lineWidth' => 2,
                                                            'lineColor' => 'Highcharts.getOptions().colors[3]',
                                                            'fillColor' => 'white',
                                                        ]
                                                    ],
                                                ]
                                            ]
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    <!-- /.box-info -->
                </div>
            </div>
        </section>
    <?php } ?>
                    
    <div class="row">
        
        <div class="col-md-12 col-md-offset-3">

            <h4>ยอดรายได้ค่าแรงเทียบกับปีที่ผ่านมา สะสม <?php print_r(count($arrDataAVG)); ?> เดือน  <?php echo ($Wages) ? $Wages : 0 ?> % </h4>
            <h4>ยอดรายได้ค่าแรงเทียบเป้าสะสม <?php print_r(count($arrDataAVG)); ?> เดือน <?php echo ($Targets) ? $Targets : 0 ?>%</h4>

        </div>

    </div>
     <?php } ?>                 
</div>


<!-- /.box-body -->
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>