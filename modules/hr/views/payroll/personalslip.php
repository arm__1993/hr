<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 6/6/2017 AD
 * Time: 9:55 AM
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use kartik\date\DatePicker;


use app\modules\hr\apihr\ApiHr;


$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<section class="content">
    <!-- Default box -->
    <!--<div class="box box-danger">-->
    <!--<div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">ข้อมูลพนักงาน</a>
            </li>
            <li>เพิ่มข้อมูลพนักงาน</li>
            <li class="active">ประกันสังคมและเงินสะสม</li>
        </ul>
    </div>-->
    <div class="box-body">
        <input type="hidden" id="idcardset" value="1509901325106">
        <form class="form-horizontal">
            <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf">
                <div class="row">
                    <div class="col-md-12" id="showMonth">
                        <center>
                            <!-- <label>เลือกเดือนที่จ่าย</label>
                            &nbsp;
                            <div class="btn-group">
                                <select class="form-control" name="selectmonth" id="selectmonth" >
                                    <?php
                                    for ($i = 1; $i <= 12; $i++) {
                                        $sel = ($i == date('m')) ? 'selected="selected"' : '';
                                        echo '<option value="' . $i . '" ' . $sel . '>' . \app\api\DateTime::convertMonth($i) . '</option>';
                                    }
                                    ?>
                                </select>
                            </div> -->
                            &nbsp;&nbsp;
                            <div class="btn-group">
                                <div class="btn-group">
                                    <select class="form-control" name="selectyearslip" id="selectyearslip">
                                        <?php
                                            foreach($data as $item){
                                                echo '<option value="' . $item['Year'] . '">' .$item['Year']. '</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                         
                        </center>
                    </div>
                </div>
        </form>
        <div id='viewslipbyyear'></div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
