<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/4/2016 AD
 * Time: 11:02 AM
 */


namespace app\modules\wssale\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use app\modules\wssale\models\Book;
use yii\helpers\ArrayHelper; //vendor/yiisoft/yii2/helpers/ArrayHelper


class GeoController extends ActiveController
{

    // adjust the model class to match your model
    public $modelClass = 'app\modules\wssale\models\Book';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}


	/*public function actionSearchme($f,$m)
    {
        //$f = \Yii::$app->request->get('f');
        return [
            'A'=>1,
            'B'=>2,
            'C'=>3,
            'D'=>4,
            'F'=>$f,
            'M'=>$m,
        ];
    }*/

    public function actionSearchme()
    {
       /* $f = \Yii::$app->request->get('f');
        $book = new Book();
        $bookSearch = $book->searchbytitle($f);
        return $bookSearch;*/
         $f = \Yii::$app->request->get('f');
        $book = new Book();
        $bookSearch = $book->searchtitlebysql($f);
        return $bookSearch;
    }


    public function actionSearchtitlebysql()
    {
        $f = \Yii::$app->request->get('f');
        $book = new Book();
        $bookSearch = $book->searchtitlebysql($f);
        return $bookSearch;
    }

    public function actionSearchtitlequerybuilder()
    {
        $f = \Yii::$app->request->get('f');
        $book = new Book();
        $bookSearch = $book->searchtitlequerybuilder($f);
        return $bookSearch;
    }
    public function actionSearchbysql()
    {
        $f = \Yii::$app->request->get('f');
        $book = new Book();
        $bookSearch = $book->searchbysql($f);
        return $bookSearch;
    }



}