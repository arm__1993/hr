<?php

namespace app\modules\vhc\models\serviceold;

use Yii;

/**
 * This is the model class for table "wagelist".
 *
 * @property integer $id
 * @property string $wage_model_id
 * @property string $wage
 * @property string $wagename
 * @property string $parts_id
 * @property string $partsname
 * @property string $MDL_CD
 * @property string $partslist_id
 * @property string $abrand_id
 * @property string $agroup_id
 * @property string $amodel_id
 * @property string $model_name
 * @property string $crepair_id
 * @property string $crepairlist_id
 * @property string $year
 * @property string $toyear
 * @property string $color
 * @property double $unit
 * @property string $add_time
 * @property string $emp_id
 * @property string $status
 * @property string $stagroup
 * @property integer $frequency_part
 */
class Wagelist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wagelist';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wage_model_id', 'wage', 'wagename', 'parts_id', 'MDL_CD', 'partslist_id', 'abrand_id', 'agroup_id', 'amodel_id', 'model_name', 'crepair_id', 'crepairlist_id', 'year', 'toyear', 'color', 'unit', 'add_time', 'emp_id', 'status', 'stagroup', 'frequency_part'], 'required'],
            [['unit'], 'number'],
            [['frequency_part'], 'integer'],
            [['wage_model_id', 'wage', 'parts_id', 'MDL_CD', 'color', 'add_time'], 'string', 'max' => 20],
            [['wagename', 'model_name'], 'string', 'max' => 250],
            [['partsname'], 'string', 'max' => 255],
            [['partslist_id', 'abrand_id', 'agroup_id', 'amodel_id', 'crepair_id', 'crepairlist_id'], 'string', 'max' => 50],
            [['year'], 'string', 'max' => 4],
            [['toyear'], 'string', 'max' => 5],
            [['emp_id'], 'string', 'max' => 13],
            [['status', 'stagroup'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wage_model_id' => 'Wage Model ID',
            'wage' => 'Wage',
            'wagename' => 'Wagename',
            'parts_id' => 'Parts ID',
            'partsname' => 'Partsname',
            'MDL_CD' => 'Mdl  Cd',
            'partslist_id' => 'Partslist ID',
            'abrand_id' => 'Abrand ID',
            'agroup_id' => 'Agroup ID',
            'amodel_id' => 'Amodel ID',
            'model_name' => 'Model Name',
            'crepair_id' => 'Crepair ID',
            'crepairlist_id' => 'Crepairlist ID',
            'year' => 'Year',
            'toyear' => 'Toyear',
            'color' => 'Color',
            'unit' => 'Unit',
            'add_time' => 'Add Time',
            'emp_id' => 'Emp ID',
            'status' => 'Status',
            'stagroup' => 'Stagroup',
            'frequency_part' => 'Frequency Part',
        ];
    }
}
