<?php

namespace app\modules\hr\apihr;


use yii\data\ActiveDataProvider;
use app\modules\hr\models\Workingcompany;
use app\modules\hr\models\Department;
use app\modules\hr\models\Section;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Position;
use app\modules\hr\models\PositionLevel;
use app\modules\hr\models\WorkType;
use app\modules\hr\models\RelationPosition;
use yii\helpers\VarDumper;
use app\api\DBConnect;
use Yii;

class ApiOrganize
{


    public static function getExistPosition()
    {
        $model = RelationPosition::find()->select('position_id')->distinct()->orderBy('id ASC')->asArray()->all();
        $arrList = [];
        if (is_array($model) || is_object($model))
            foreach ($model as $item) {
                $arrList[$item['position_id']] = $item['position_id'];
            }
        return $arrList;
    }

    public static function getExistLevel()
    {

        $model = Position::find()->select('Level')->distinct()->orderBy('Level ASC')->asArray()->all();
        $arrList = [];
        if (is_array($model) || is_object($model))
            foreach ($model as $item) {
                $arrList[$item['Level']] = $item['Level'];
            }
        return $arrList;
    }

    public static function getExistWorktype()
    {
        $model = Position::find()->select('WorkType')->distinct()->orderBy('WorkType ASC')->asArray()->all();
        $arrList = [];
        if (is_array($model) || is_object($model))
            foreach ($model as $item) {
                $arrList[$item['WorkType']] = $item['WorkType'];
            }
        return $arrList;
    }

    public static function getWorking_company($id)
    {
        $data = Workingcompany::find()
            ->where('status <> 99 ')
            ->andWhere('id =' . $id)
            ->asArray()
            ->all();

        return $data;
    }

    public static function getDepartment($id_company)
    {
        if ($id_company != '') {
            $where = 'company = ' . $id_company . ' ';
        } else {
            $where = '1';
        }
        $data = Department::find()
            ->select(['id', 'name', 'code_name'])
            ->where($where)
            ->asArray()
            ->all();

        return $data;
    }

    public static function getSection($id_department)
    {
        if ($id_department != '') {
            $where = 'department = ' . $id_department;
        } else {
            $where = '1';
        }
        $data = Section::find()
            ->select(['id', 'name', 'code_name'])
            ->where($where . ' AND status <> 99')
            ->asArray()
            ->all();

        return $data;
    }

    public static function getPositionLevel()
    {
        $data = PositionLevel::find()
            ->select(['level_id', 'level_code', 'level_name'])
            ->where('level_status <> 99')
            ->asArray()
            ->all();

        return $data;
    }

    public static function getWorkType()
    {
        $data = WorkType::find()
            ->select(['code_name', 'id', 'name', 'code_name'])
            ->where('status <> 99')
            ->asArray()
            ->all();

        return $data;
    }

    public static function searchGridposition($_WorkCompany, $_Department, $_Section)
    {
        $where = '';
        if ($_WorkCompany != '') {
            $where .= 'position.workCompany = ' . $_WorkCompany . ' ';
        }
        if ($_Department != '') {
            $where .= 'AND ' . ' position.department = ' . $_Department . ' ';
        }
        if ($_Section != '') {
            $where .= 'AND ' . ' position.section = ' . $_Section . ' ';
        }
        $data = Position::find()
            ->select([
                "position.id as id",
                "position.name  as Name",
                "working_company.name as companyname",
                "department.name as departmentname",
                "section.name as sectionname",
                "position.PositionCode"
            ])
            ->from("position")
            ->innerJoin("working_company", "position.workCompany = working_company.id")
            ->innerJoin("department", "position.department = department.id")
            ->innerJoin("section", "position.section = section.id")
            ->where($where)
            ->andWhere("position.status <> 99");


        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 100,
            ]
        ]);
    }

    public static function searchGridpositioncount($_WorkCompany, $_Department, $_Section)
    {

        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];


        $where = '';
        $groupBy = '';
        if ($_WorkCompany != '') {
            $where .= 'position.workCompany = ' . $_WorkCompany . ' ';
            $groupBy .= 'companyname' . ',' . 'departmentname' . ',' . 'section.name';
        }
        if ($_Department != '') {
            $where .= 'AND ' . ' position.department = ' . $_Department . ' ';
        }
        if ($_Section != '') {
            $where .= 'AND ' . ' position.section = ' . $_Section . ' ';
        }
        if ($_WorkCompany == '') {
            $groupBy .= 'companyname' . ',' . 'departmentname' . ',' . 'section.name';
        }


        $data = Position::find()
            ->select([
                "position.name  as Name",
                "working_company.name as companyname",
                "department.name as departmentname",
                "section.name as sectionname",


                " Count(" . $db['ou'] . ".position.id) AS STATUS_19",
                "(SUM(IF(ISNULL(Organic_chart.Emp_id),0,1)) - SUM(IF(" . $db['ct'] . ".emp_data.Prosonnal_Being = 2, 1 , 0))) AS pass",
                " SUM(IF(" . $db['ct'] . ".emp_data.Prosonnal_Being = 2, 1 , 0)) AS notpass",
                "SUM(IF(position.recruit_status = 1, if(ISNULL(Organic_chart.Emp_id),1,0), 0 )) AS STATUS1",
                "SUM(IF(position.recruit_status = 2, 1, 0 )) AS STATUS_2",
                "SUM(IF(position.recruit_status = 99, 1, 0 )) AS STATUS_99"


            ])
            ->from($db['ou'] . ".position")
            ->leftJoin($db['ou'] . ".Organic_chart", $db['ou'] . ".position.id = " . $db['ou'] . ".Organic_chart.Posision_id")
            ->leftJoin($db['ct'] . ".emp_data", $db['ou'] . ".Organic_chart.Emp_id = " . $db['ct'] . ".emp_data.DataNo")
            ->innerJoin($db['ou'] . ".working_company", $db['ou'] . ".position.workCompany = " . $db['ou'] . ".working_company.id")
            ->innerJoin($db['ou'] . ".department", $db['ou'] . ".position.department = " . $db['ou'] . ".department.id")
            ->innerJoin($db['ou'] . ".section", $db['ou'] . ".position.section = " . $db['ou'] . ".section.id")
            ->where($where)
            ->andWhere("position.status <> 99")
            ->groupBy($groupBy)
            ->having('STATUS1 > 0 ')
            ->orderBy($groupBy);



        return $dataProvider = new ActiveDataProvider([
            'query' => $data,
            'pagination' => [
                'pageSize' => 100,
            ]
        ]);
    }

    public static function searchGridpositioncountpdf($_WorkCompany, $_Department, $_Section)
    {
        $db['ou'] = DBConnect::getDBConn()['ou'];
        $db['ct'] = DBConnect::getDBConn()['ct'];
        $db['pl'] = DBConnect::getDBConn()['pl'];


        $where = '';
        $groupBy = '';
        if ($_WorkCompany != '') {
            $where .= $db['ou'] . '.position.workCompany = ' . $_WorkCompany . ' AND ' .$db['ou'] . '.position.Status != 99'.' ';
            //  $groupBy .= $db['ou'].'.companyname' . ',' .$db['ou']. '.departmentname' . ',' .$db['ou']. '.sectionname';
            $groupBy .= 'companyname' . ',' . 'departmentname' . ',' . 'section.name';
        }
        if ($_Department != '') {
            $where .= 'AND ' . $db['ou'] . ' .position.department = ' . $_Department . ' ';
        }
        if ($_Section != '') {
            $where .= 'AND ' . $db['ou'] . '. position.section = ' . $_Section . ' ';
        }
        if ($_WorkCompany == '') {
            // $groupBy .= $db['ou'].'.companyname';
        }
        if ($_WorkCompany == '' && $_Department == '' && $_Section == '') {
            $where .= $db['ou'] . '.position.Status != 99 ';
            $groupBy .= 'companyname' . ',' . 'departmentname' . ',' . 'section.name';
        }

        $sql = "SELECT
        $db[ou].position.name  as Name,
        $db[ou].working_company.name as companyname,
        $db[ou].section.name as sectionname,
        $db[ou].department.name as departmentname,
         
         
      
          Count($db[ou].position.id) AS STATUS_19,
          Sum(IF(ISNULL(Organic_chart.Emp_id),0,1)) - Sum(IF($db[ct].emp_data.Prosonnal_Being = 2, 1 , 0)) AS pass,
          Sum(IF($db[ct].emp_data.Prosonnal_Being = 2, 1 , 0)) AS notpass,
          Sum(IF($db[ou].position.recruit_status = 1, IF(ISNULL($db[ou].Organic_chart.Emp_id),1,0), 0 )) AS STATUS1,
          Sum(IF($db[ou].position.recruit_status = 2, 1, 0 )) AS STATUS_2,
          Sum(IF($db[ou].position.recruit_status = 99, 1, 0 )) AS STATUS_99

        FROM  $db[ou].position


        LEFT JOIN $db[ou].Organic_chart ON $db[ou].position.id = $db[ou].Organic_chart.Posision_id
        LEFT JOIN $db[ct].emp_data ON $db[ou].Organic_chart.Emp_id = $db[ct].emp_data.DataNo
   
        INNER JOIN  $db[ou].section ON ($db[ou].position.section = $db[ou].section.id)
        INNER JOIN  $db[ou].department ON ($db[ou].position.department = $db[ou].department.id)
        INNER JOIN  $db[ou].working_company ON ($db[ou].position.workCompany = $db[ou].working_company.id)
      
      WHERE $where
      GROUP BY  $groupBy
      HAVING STATUS1 > 0
      ORDER BY  $groupBy
      ";

        $model = Yii::$app->dbERP_easyhr_checktime->createCommand($sql)->queryAll();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;




        return $model;
    }

    public static function CheckEditCompany()
    {
        $data = Position::find()
            ->asArray()
            ->all();
        $arrList = [];
        foreach ($data as $item) {
            $arrList[$item['WorkCompany']] = $item['WorkCompany'];
        }
        return $arrList;
    }

    public static function CheckEditDepartment($var)
    {
        $data = Position::find()->where(['=', 'WorkCompany', $var])
            ->groupBy('Department')
            ->asArray()
            ->all();
        $arrList = [];
        foreach ($data as $item) {
            $arrList[$item['Department']] = $item['Department'];
        }
        return $arrList;
    }

    public static function CheckEditSection($var)
    {
        $data = Position::find()->where(['=', 'section', $var])
            ->groupBy('section')
            ->asArray()
            ->all();
        return $data;
    }

    public static function PositionsVacant()
    {
        $Reposition = new RelationPosition();

        $data = Relationposition::find()
            //->joinWith('position',true,'RIGHT JOIN')
            ->select([
                    'COUNT(Organic_chart.Posision_id) as Position_typeAll',
                    'SUM(if(emp_data.Prosonnal_Being = 1,1,0)) as Position_typepersonnel',
                    'SUM(if(emp_data.Prosonnal_Being = 2,1,0)) as Position_typecloseposition',
                    'SUM(if(Organic_chart.active_status = 3,1,0)) as Position_typeclose',
                    'SUM(if(position.Status = 0,1,0)) as Position_typeapprentice',
                    //'(COUNT(relation_position.position_id)-(SUM(if(emp_data.Prosonnal_Being = 1,1,0))+SUM(if(Organic_chart.active_status  = 3,1,0)))) as Position_typeFree',
                    'SUM(if(Organic_chart.active_status  = 2,1,0)) as Position_typeFree',
                    'position.PositionCode as Position_PositionCode',
                    'position.Name as Position_Name',
                    'position.WorkCompany as Position_WorkCompany',
                    'position.Department as Position_Department',
                    'position.Section as Position_Section',
                    'position.WorkType as Position_WorkType',
                    'position.Level as Position_Level',
                    'working_company.name as companyName',
                    'department.name as departmentName',
                    'section.name as sectionName'
                ]
            )
            ->join(
                'RIGHT JOIN', 'position', 'relation_position.position_id = position.id'
            )
            ->join(
                'INNER JOIN', 'ERP_easyhr_checktime.emp_data', 'relation_position.id_card = emp_data.ID_Card'
            )
            ->join(
                'INNER JOIN', 'Organic_chart', 'Organic_chart.Posision_id = position.id'
            )
            ->join('INNER JOIN', 'working_company', 'position.WorkCompany = working_company.id')
            ->join('INNER JOIN', 'department', 'position.department = department.id')
            ->join('INNER JOIN', 'section', 'position.section = section.id')
            ->Where(
                ['!=', 'position.Status', '99']
            )
            ->limit(12, 20)
            ->groupBy(['relation_position.position_id'])
            ->asArray()
            ->all();
        return $data;
    }


    public static function getPositionByCodeposition($codeposition)
    {
        $model = Position::find()
            ->where('PositionCode = "' . $codeposition . '"')
            ->asArray()
            ->one();

        return $model;

    }

    public static function getruning($code)
    {
        /*
        $model = Position::find()
                        ->select(['runNumber'])
                        ->where('PositionCode like "'.$code.'%"')
                        ->orderBy('runNumber DESC')
                        ->asArray()
                        ->one();
        return ($model!=null)?$model:0;
        */
        $model = Position::find()
            ->select(['runNumber'])
            ->where('PositionCode like "' . $code . '%" ')
            ->orderBy('runNumber ASC')
            ->asArray()
            ->all();
        $arrCodeList = [];
        foreach ($model as $item) $arrCodeList[] = (int)$item['runNumber'];

        //sort array ASC and loop through
        asort($arrCodeList);
        $arrCodeSort = [];
        $idx = 0;
        $codeNext = null;
        $walkLoop = false;
        foreach ($arrCodeList as $item) {
            $arrCodeSort[$idx] = $item;
            if ($idx > 0) {
                if (($item - $arrCodeSort[$idx - 1]) > 1) {
                    $code = $arrCodeSort[$idx - 1];
                    $codeNext = $code + 1;
                    $walkLoop = true;
                    array_splice($arrCodeSort, $idx, 0, $codeNext);
                    break;
                }
            }
            $idx++;
        }

        //if not found OR sequent series
        if (!$walkLoop) {
            $code = array_pop($arrCodeList);
            $codeNext = $code + 1;
        }

        //IF string lengh is 1 SET format  lead with ZERO
        $retCode = (strlen($codeNext) == 1) ? str_pad($codeNext, 2, '0', STR_PAD_LEFT) : $codeNext;

        return $retCode;
    }


}

?>
