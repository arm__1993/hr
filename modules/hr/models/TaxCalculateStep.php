<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "tax_calculate_step".
 *
 * @property int $id รหัส
 * @property string $emp_idcard รหัสบัตรประชาชนพนักงานจ่ายภาษี
 * @property string $pay_date รอบเดือนปีจ่ายเงินเดือน
 * @property int $tax_calculate_id รหัสอ้างอิงไปยัง tax_calculate FK  ==>tax_calculate.id
 * @property int $tax_income_structure_id รหัสอ้างอิงไปยัง tax_income_structure FK  ==>tax_income_structure.id
 * @property string $income_range ช่วงรายได้ที่จะคำนวณภาษี
 * @property string $tax_rate
 * @property string $income_step จำนวนเงินได้ในช่วง
 * @property string $gross_step_tax จำนวนภาษีเงินได้ในช่วง
 * @property string $total_accumulate_tax จำนวนภาษีสะสมรวมแต่ละช่วง
 */
class TaxCalculateStep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tax_calculate_step';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tax_calculate_id', 'tax_income_structure_id'], 'integer'],
            [['tax_rate', 'income_step', 'gross_step_tax', 'total_accumulate_tax'], 'number'],
            [['emp_idcard'], 'string', 'max' => 13],
            [['pay_date'], 'string', 'max' => 12],
            [['income_range'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_idcard' => 'Emp Idcard',
            'pay_date' => 'Pay Date',
            'tax_calculate_id' => 'Tax Calculate ID',
            'tax_income_structure_id' => 'Tax Income Structure ID',
            'income_range' => 'Income Range',
            'tax_rate' => 'Tax Rate',
            'income_step' => 'Income Step',
            'gross_step_tax' => 'Gross Step Tax',
            'total_accumulate_tax' => 'Total Accumulate Tax',
        ];
    }
}
