<?php

namespace app\modules\hr\controllers;

use app\api\Helper;
use app\modules\hr\apihr\ApiPersonal;
use app\modules\hr\apihr\ApiSearchtaxpersonal;
use app\modules\hr\models\Department;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\Empaddress;
use app\modules\hr\models\Empfamily;
use app\modules\hr\models\Children;
use app\modules\hr\models\OrganicChart;
use app\modules\hr\models\Salarystep;
use app\modules\hr\models\TaxIncome;
use app\modules\hr\models\Education;
use app\modules\hr\models\EmpMoneyBonds;
use app\modules\hr\models\EmpMoneyBondsTel;
use app\modules\hr\models\EmpMoneyBondsPayment;
use app\modules\hr\models\EmpPromiseType;
use app\modules\hr\models\EmpPromise;
use app\modules\hr\models\EmpPromiseMapping;
use app\modules\hr\models\EmpPromiseMappingCondition;
use app\modules\hr\models\WorkHistory;
use app\modules\hr\models\BankThailand;
use app\modules\hr\models\Position;
use app\modules\hr\models\Empsalary;
use app\api\Configpass;
use app\api\Utility;
use app\api\DateTime;
use app\api\DBConnect;

use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

use yii\db\Expression;
use yii\web\Session;
use app\modules\hr\controllers\MasterController;
use Yii;
use mPDF;

class PersonalController extends MasterController
{
    public $layout = 'hrlayout';
    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEditpositionsaraly()
    {
        $getValue = Yii::$app->request->get();
        $con = DBConnect::getDBConn();
        $modeEmppositionsaraly = Empsalary::find()
            ->select('EMP_SALARY.*,position.WorkType')
            ->join('INNER JOIN', $con['ou'] . '.position', 'EMP_SALARY.EMP_SALARY_POSITION_CODE = ' . $con['ou'] . '.position.PositionCode')
            ->where(['EMP_SALARY_ID' => $getValue['data']])
            ->asArray()
            ->one();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $modeEmppositionsaraly;
    }

    /******************************************
     *   แสดงหน้า form บันทึก
     *
     *******************************************/
    public function actionFormaddnewemp()
    {
        $data = [];
        $data['title_curent'] = "เพิ่มข้อมูลพนักงาน";

        $data['liveWith'] = $this->getLiveWithOption();
        $data['residentWith'] = $this->getResidentOption();

        //relation
        $rel = [];
        $rel['option_father_be'] = $this->createBeNameDropdownOptions(1, "", 'th');
        $rel['option_mother_be'] = $this->createBeNameDropdownOptions(2, "", 'th');
        $rel['option_parent_status'] = $this->createSelOptParentStatus("");
        $rel['option_spouse_status'] = $this->createSelOptSpouseStatus("");
        //--
        //time
        $rel['option_dayoff'] = $this->getOptionDay();
        $rel['Prosonnal_Being'] = $this->getOptioPersonalStatus();
        $rel['Main_EndWork_Reason'] = $this->getOptionMainEndWorkReason();
        $rel['startWorkTime_h'] = $this->getHourTime();
        $rel['startWorkTime_m'] = $this->getMinTime();
        $rel['endWorkTime_h'] = $this->getHourTime();
        $rel['endWorkTime_m'] = $this->getMinTime();
        $rel['beginLunch_h'] = $this->getHourTime();
        $rel['beginLunch_m'] = $this->getMinTime();
        $rel['endLunch_h'] = $this->getHourTime();
        $rel['endLunch_m'] = $this->getMinTime();
        $rel['job_type'] = $this->getOptionJobType();
        $arrEduLevel = ApiPersonal::getArrayEducationLevel();


        return $this->render('formaddnewemp', [
            'data' => $data,
            'rel' => $rel,
            'arrEduLevel' => $arrEduLevel,
            'mode' => 'add', //set mode to add new

        ]);
    }

    /******************************************
     *   แสดงรายการ พนักงาน เพื่อมาแก้ไข
     *
     *******************************************/
    public function actionEditemp()
    {

        $param = Yii::$app->request->queryParams;

        $Empdata = new Empdata();
        $EmpdataProvider = $Empdata->search($param);

        return $this->render('editemp', [
            'dataProvider' => $EmpdataProvider,
            'Empdata' => $Empdata
        ]);


    }

    /******************************************
     *   ตรวจสอบชื่อล็อกอินว่าซ้ำหรือไม่
     *
     *******************************************/
    public function actionCheckusername()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $u = trim($postValue['u']);
            $dataNo = trim($postValue['data_no']);

            $model = Empdata::find()->where([
                'username' => $u,
            ])->asArray()->one();

            $found = 0;
            if ($model !== null && $model['DataNo'] !== $dataNo) {
                $found = 1;
            }

            return $found;
        }
    }

    /******************************************
     *  Save && Update  บันทึกข้อมูลพนักงาน
     *
     *******************************************/
    public function actionAddnewemp()
    {

        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = "";
        $resOut['data_no'] = "";
        $isNew = true;

        try {

            //begin tranc
            //$transaction = Yii::$app->db->beginTransaction();
            $transaction = Empdata::getDb()->beginTransaction();
            $transEmpaddress = Empaddress::getDb()->beginTransaction();
            $transEmpfamily = Empfamily::getDb()->beginTransaction();
            $transChildren = Children::getDb()->beginTransaction();
            $transTaxIncome = TaxIncome::getDb()->beginTransaction();
            $transEducation = Education::getDb()->beginTransaction();
            $transEmpsalary = Empsalary::getDb()->beginTransaction();
            $transOrganize = OrganicChart::getDb()->beginTransaction();

            /**************************************
             *  Parse data
             *
             ***************************************/
            $imgEmp = $_FILES['fileEmp_upload'];

            $imgMap = $_FILES['fileMap_upload'];


            $strJson = Yii::$app->request->post();
            $objIn = json_decode($strJson['allData'], true);
            $objAddr = json_decode($strJson['addData'], true);
            $objFamily = json_decode($strJson['familyData'], true);
            $objChildren = json_decode($strJson['children'], true);
            $objTaxIncome = json_decode($strJson['taxIncome'], true);
            $objEdu = json_decode($strJson['edu'], true);
            $objEmpPosition = json_decode($strJson['eposition'], true);

            $objTime = json_decode($strJson['time'], true);

            $objBondsman = json_decode($strJson['bondsman'], true);
            $objWorkHistory = json_decode($strJson['workHistory'], true);

            $objWageChannel = json_decode($strJson['wagechannel'], true);

            $imgEmpupload = json_decode($strJson['gVarCircleImg'], true);


            $emp_Card_ID = $objIn['ID_Card'];
            $emp_Card_ID = Utility::removeDashIDcard($emp_Card_ID);
            $dataempimgcircle = $imgEmpupload;



            $imageName = [];
            if (!is_array($dataempimgcircle)) {
                $path2 = Yii::$app->params;
                $path = $path2['emp_pic_path'];

                list($type, $dataempimgcircle) = explode(';', $dataempimgcircle);
                list($a, $dataempimgcircle) = explode(',', $dataempimgcircle);

                $dataempimgcircle = base64_decode($dataempimgcircle);
                $imageName = $emp_Card_ID . '.png';
                file_put_contents($path . $imageName, $dataempimgcircle);
            }
            if (is_array($dataempimgcircle)) {
                $imageName = $emp_Card_ID . '.png';
            }



            //remove dash
            $objWageChannel['Salary_BankNo'] = ($objWageChannel['Salary_BankNo']) ? Utility::removeDashIDcard($objWageChannel['Salary_BankNo']) : $objWageChannel['Salary_BankNo'];
            $objWageChannel['promtpay_number'] = ($objWageChannel['promtpay_number']) ? Utility::removeDashIDcard($objWageChannel['promtpay_number']) : $objWageChannel['promtpay_number'];


            $Emp_name = $objIn['Name'] . ' ' . $objIn['Surname'];

            $objIn['ID_Card'] = Utility::removeDashIDcard($objIn['ID_Card']);
            $dataNo = " ";

            unset($objFamily['_csrf']);
            // echo '<pre>';print_r($objEdu);echo '</pre>';
            // exit();
            /**************************************
             *  Check is new emp
             *
             ***************************************/
            if (isset($strJson['dataNo']) && $strJson['dataNo'] != "") {
                $dataNo = $strJson['dataNo'];
                $isNew = false;
            }
            /**************************************
             *  validate primary key
             *
             ***************************************/
            $haveEmp = $this->validateEmp($objIn, $dataNo);

            if ($haveEmp) {
                throw new ErrorException("ไม่สามารถบันทึกข้อมูลได้  เนื่องจากเลขบัตรประชาชนซ้ำ");
            }


            /**************************************
             *  save general tab
             *
             ***************************************/
            $objInMain['objIn'] = $objIn;
            $objInMain['imgEmp'] = $imgEmp;

            $rGlSave = $this->saveGenaralTab($objInMain, $objWageChannel, $dataNo, $imageName);

            if (!$rGlSave['status']) {
                throw new ErrorException($rGlSave['msg']);
            } else {
                $dataNo = $rGlSave['lastId'];
                $resOut['dataNo'] = $dataNo;
            }
            //--

            /**************************************
             *  save address tab
             *
             ***************************************/
            $objAddrMain['objAddr'] = $objAddr;
            $objAddrMain['imgMap'] = $imgMap;

            $rAddrSave = $this->saveAddrTab($objAddrMain, $dataNo, $isNew);

            if (!$rAddrSave['status']) {
                throw new ErrorException($rAddrSave['msg']);
            }
            //--

            /**************************************
             *  save family tab
             *
             ***************************************/
            $objInMain['objFamily'] = $objFamily;
            $rFamilySave = $this->saveFamily($objInMain, $dataNo);

            if (!$rFamilySave['status']) {
                throw new ErrorException($rFamilySave['msg']);
            }

            //save children 
            $rChildSave = $this->saveChildren($objChildren, $objIn['ID_Card']);

            if (!$rChildSave['status']) {
                throw new ErrorException($rChildSave['msg']);
            }
            //--

            /**************************************
             *  save taxincome tab
             *
             ***************************************/
            $rTaxincomeSave = $this->saveTaxIncome($objTaxIncome, $dataNo);

            if (!$rTaxincomeSave['status']) {
                throw new ErrorException($rTaxincomeSave['msg']);
            }
            //--
            /**************************************
             *  save education tab
             *
             ***************************************/
            $rEduSave = $this->saveEducation($objEdu, $objIn['ID_Card']);

            if (!$rEduSave['status']) {
                throw new ErrorException($rEduSave['msg']);
            }
            //--


            /**************************************
             *  save time tab
             *
             ***************************************/
            //without change
            $empBFUpdate = Empdata::findOne($dataNo);
            $empOutUpdate = ($empBFUpdate->Prosonnal_Being == $objTime['Prosonnal_Being']) ? false : true;
            //


            $rTime = $this->saveTime($objTime, $dataNo);
            if (!$rTime['status']) {
                throw new ErrorException($rEduSave['msg']);
            }

            //update  การลาออก Main_EndWork_Reason
            //echo '<pre>';print_r($objTime);echo '</pre>';
            if ($objTime['Prosonnal_Being'] == 3 && $empOutUpdate) {

                $rEmpOut = $this->saveEmpOut($objTime, $dataNo);
                if (!$rEmpOut['status']) {
                    throw new ErrorException($rEduSave['msg']);
                }
            }

            //--
            //--

            /**************************************
             *  save bondsman tab
             *
             ***************************************/
            if (!empty($objBondsman)) {
                $rBondsman = $this->saveBondsman($objBondsman, $dataNo);
                if (!$rBondsman['status']) {
                    throw new ErrorException($rBondsman['msg']);
                }
            }
            // echo 'test';
            //throw new ErrorException('error:test tranc');

            /**************************************
             *  save promise
             *
             ***************************************/
            if ($isNew) {
                $arrType = [];
                if ($objTime['job_type'] == 'รายเดือน') {
                    $arrType[] = 2;
                }
                if ($objTime['job_type'] == 'รายวัน') {
                    $arrType[] = 1;
                }
                $rPremise = $this->savePermise($dataNo, $objIn['ID_Card'], $arrType);
            }
            //--


            //$objWorkHistory
            /**************************************
             *  save workhistory
             *
             ***************************************/
            //echo 'xxx<pre>';print_r($objWorkHistory);echo '</pre>';exit();
            if (!empty($objWorkHistory)) {
                $rWorkHis = $this->saveWorkHistory($objWorkHistory, $objIn['ID_Card']);
                if (!$rWorkHis['status']) {
                    throw new ErrorException($rWorkHis['msg']);
                }
            }
            //--


            //--
            /**************************************
             *  save position tab
             *
             ***************************************/
            $rEmpposSave = $this->saveEmpposition($objEmpPosition, $objIn['ID_Card'], $dataNo, $Emp_name);

            if (!$rEmpposSave['status']) {
                throw new ErrorException($rEmpposSave['msg']);
            }
            //--

            $transEmpaddress->commit();
            $transEmpfamily->commit();
            $transChildren->commit();
            $transTaxIncome->commit();
            $transEducation->commit();
            $transaction->commit();
            $transEmpsalary->commit();
            $transOrganize->commit();


        } catch (ErrorException $e) {

            $transEmpaddress->rollBack();
            $transEmpfamily->rollBack();
            $transChildren->rollBack();
            $transTaxIncome->rollBack();
            $transEducation->rollBack();
            $transEmpsalary->rollBack();
            $transaction->rollBack();
            $transOrganize->rollBack();
            //echo '<pre>';print_r($e);echo '</pre>';

            $resOut['status'] = false;
            $resOut['msg'] = "เกิดข้อผิดพลาด<br>" . $e->getMessage();
            $resOut['line'] = $e->getLine();
            $resOut['file'] = $e->getFile();


        } catch (Exception $e) {

            $transEmpaddress->rollBack();
            $transEmpfamily->rollBack();
            $transChildren->rollBack();
            $transTaxIncome->rollBack();
            $transEducation->rollBack();
            $transaction->rollBack();
            $transaction->rollBack();

            $resOut['status'] = false;
            $resOut['msg'] = "เกิดข้อผิดพลาด<br>" . $e->getMessage();
            $resOut['line'] = $e->getLine();
            $resOut['file'] = $e->getFile();
        }


        return json_encode($resOut);
    }


    /**************************************
     *  validate ID Card
     *
     ***************************************/


    public function actionValididcard()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postVal = Yii::$app->request->post();
            $_idcard = $postVal['idCard'];
            $dataNo = $postVal['dataNo'];
            $who = $postVal['who'];
            $objIn['ID_Card'] = Utility::removeDashIDcard($_idcard);

            $haveEmp = $this->validateEmp($objIn, $dataNo, $who);
            if ($haveEmp) {
                return "เลขบัตรประชาชนซ้ำ";
            } else {
                return "0";
            }
        }
    }

    public function actionBankthailand(Type $var = null)
    {
        $data = BankThailand::find()->asArray()->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }


    /******************************************
     *   แสดง form แก้ไข
     *
     *******************************************/
    public function actionEditempform()
    {

        $getVal = Yii::$app->request->get();
        //echo 'getVal:<pre>';print_r($getVal);echo '</pre>';

        $dataNo = $getVal['dataNo'];
        $data = [];
        $data['title_curent'] = "ปรับปรุงข้อมูลพนักงาน";

        /******************************************
         *   TAB ทั่วไป
         *
         *******************************************/
        $empModel = Empdata::findOne($dataNo);


        $optional = [];
        $sexToNum = ($empModel['Sex'] == 'ชาย') ? 1 : 2;
        $empModel['ID_Card'] = Utility::displayIDcard($empModel['ID_Card']);
        $optional['option_bename_th'] = $this->createBeNameDropdownOptions($sexToNum, $empModel['Be'], 'th');
        //$optional['option_bename_en'] = $this->createBeNameDropdownOptions($sexToNum,$empModel['Be'],'en');

        /******************************************
         *   TAB address
         *
         *******************************************/
        $addrInf = [];
        $addrModels = Empaddress::find()->where(['=', 'ADDR_DATA_NO', $getVal['dataNo']])->all();
        $arMap = [1 => 'home_addr', 2 => 'personal_card_addr', 3 => 'contact_addr'];

        foreach ($addrModels as $no => $addrModel) {
            $addrInf[$arMap[$addrModel['ADDR_TYPE']]] = $addrModel;
        }
        // echo '<pre>';print_r($addrInf);echo '</pre>';exit();
        //--

        $data['liveWith'] = $this->getLiveWithOption($empModel['Live_with']);
        $data['residentWith'] = $this->getResidentOption($empModel['Resident_type']);

        /******************************************
         *   TAB family
         *
         *******************************************/

        $familyModel = Empfamily::find()->where(['=', 'emp_data_id', $getVal['dataNo']])->one();
        $familyModel['father_idcard'] = Utility::displayIDcard($familyModel['father_idcard']);
        $familyModel['mother_idcard'] = Utility::displayIDcard($familyModel['mother_idcard']);
        $familyModel['spouse_idcard'] = Utility::displayIDcard($familyModel['spouse_idcard']);
        //relation
        $rel = [];
        $rel['option_father_be'] = $this->createBeNameDropdownOptions(1, $familyModel['father_be'], 'th');
        $rel['option_mother_be'] = $this->createBeNameDropdownOptions(2, $familyModel['mother_be'], 'th');
        $rel['option_parent_status'] = $this->createSelOptParentStatus($familyModel['parent_status']);
        $rel['option_spouse_status'] = $this->createSelOptSpouseStatus($familyModel['spouse_status']);
        $spSex = ($familyModel['spouse_sex'] == 'ชาย') ? 1 : 2;
        $rel['option_spous_be'] = $this->createBeNameDropdownOptions($spSex, $familyModel['spouse_be'], 'th');
        $rlSex = ($familyModel['relative_be'] == 'ชาย') ? 1 : 2;
        $rel['option_relative_be'] = $this->createBeNameDropdownOptions($rlSex, $familyModel['relative_be'], 'th');
        //--
        //--
        /******************************************
         *   TAB Taxincome
         *
         *******************************************/
        $taxIncomeModel = TaxIncome::find()->where(['=', 'emp_id', $getVal['dataNo']])->one();
        //echo  ">>>>>".$taxIncomeModel->tax_personal.'<<<<<';
        //echo '<pre>';print_r($taxIncomeModel);echo '</pre>';
        //--
        //
        /******************************************
         *   TAB time
         *
         *******************************************/

        $rel['option_dayoff'] = $this->getOptionDay($empModel['DayOff']);
        $rel['Prosonnal_Being'] = $this->getOptioPersonalStatus($empModel['Prosonnal_Being']);
        $rel['Main_EndWork_Reason'] = $this->getOptionMainEndWorkReason($empModel['Main_EndWork_Reason']);

        $startWorkTime_slt = explode(":", $empModel['startWorkTime']);
        $rel['startWorkTime_h'] = $this->getHourTime($startWorkTime_slt[0]);
        $rel['startWorkTime_m'] = $this->getMinTime($startWorkTime_slt[1]);

        $endWorkTime_slt = explode(":", $empModel['endWorkTime']);
        $rel['endWorkTime_h'] = $this->getHourTime($endWorkTime_slt[0]);
        $rel['endWorkTime_m'] = $this->getMinTime($endWorkTime_slt[1]);

        $beginLunch_slt = explode(":", $empModel['beginLunch']);
        $rel['beginLunch_h'] = $this->getHourTime($beginLunch_slt[0]);
        $rel['beginLunch_m'] = $this->getMinTime($beginLunch_slt[1]);

        $endLunch_slt = explode(":", $empModel['endLunch']);
        $rel['endLunch_h'] = $this->getHourTime($endLunch_slt[0]);
        $rel['endLunch_m'] = $this->getMinTime($endLunch_slt[1]);

        $rel['job_type'] = $this->getOptionJobType($empModel['job_type']);

        $arrEduLevel = ApiPersonal::getArrayEducationLevel();


        $pass =  $empModel['password'];




        return $this->render('formaddnewemp', [
            'data' => $data,
            'passedit' => $pass,
            'empModel' => $empModel,
            'addrInf' => $addrInf,
            'family' => $familyModel,
            'optional' => $optional,
            'dataNo' => $getVal['dataNo'],
            'rel' => $rel,
            'arrEduLevel' => $arrEduLevel,
            'taxIncomeModel' => $taxIncomeModel,
            'mode' => 'edit',//set mode to edit
        ]);
    }

    /******************************************
     *   view only แสดง form แก้ไข
     *
     *******************************************/
    public function actionViewempform()
    {

        //echo "view";
        $getVal = Yii::$app->request->get();
        //echo 'getVal:<pre>';print_r($getVal);echo '</pre>';
        $data = [];
        $data['title_curent'] = "ปรับปรุงข้อมูลพนักงาน";

        /******************************************
         *   TAB ทั่วไป
         *
         *******************************************/
        $empModel = Empdata::findOne($getVal['dataNo']);
        $empModel['ID_Card'] = Utility::displayIDcard($empModel['ID_Card']);
        /******************************************
         *   TAB address
         *
         *******************************************/
        $addrInf = [];
        $addrModels = Empaddress::find()->where(['=', 'ADDR_DATA_NO', $getVal['dataNo']])->all();
        $arMap = [1 => 'home_addr', 2 => 'personal_card_addr', 3 => 'contact_addr'];

        foreach ($addrModels as $no => $addrModel) {
            $addrInf[$arMap[$addrModel['ADDR_TYPE']]] = $addrModel;
        }
        //--

        /******************************************
         *   TAB family
         *
         *******************************************/

        $familyModel = Empfamily::find()->where(['=', 'emp_data_id', $getVal['dataNo']])->one();

        $familyModel['father_idcard'] = Utility::displayIDcard($familyModel['father_idcard']);
        $familyModel['mother_idcard'] = Utility::displayIDcard($familyModel['mother_idcard']);
        $familyModel['spouse_idcard'] = Utility::displayIDcard($familyModel['spouse_idcard']);
        //relation
        $rel = [];
        $rel['option_father_be'] = $this->createBeNameDropdownOptions(1, $familyModel['father_be'], 'th');
        $rel['option_mother_be'] = $this->createBeNameDropdownOptions(2, $familyModel['mother_be'], 'th');
        $rel['option_parent_status'] = $this->createSelOptParentStatus($familyModel['parent_status']);
        $rel['option_spouse_status'] = $this->createSelOptSpouseStatus($familyModel['spouse_status']);
        $spSex = ($familyModel['spouse_sex'] == 'ชาย') ? 1 : 2;
        $rel['option_spous_be'] = $this->createBeNameDropdownOptions($spSex, $familyModel['spouse_be'], 'th');
        $rlSex = ($familyModel['relative_be'] == 'ชาย') ? 1 : 2;
        $rel['option_relative_be'] = $this->createBeNameDropdownOptions($rlSex, $familyModel['relative_be'], 'th');
        //--
        //--
        /******************************************
         *   TAB Taxincome
         *
         *******************************************/
        $taxIncomeModel = TaxIncome::find()->where(['=', 'emp_id', $getVal['dataNo']])->one();
        //--


        $optional = [];
        $sexToNum = ($empModel['Sex'] == 'ชาย') ? 1 : 2;
        $optional['option_bename_th'] = $this->createBeNameDropdownOptions($sexToNum, $empModel['Be'], 'th');
        //$optional['option_bename_en'] = $this->createBeNameDropdownOptions($sexToNum,$empModel['Be'],'en');
        $optional['dontouch'] = "dontouch";
        $optional['readonly'] = "readonly";
        $optional['viewOnly'] = true;
        $optional['displayNone'] = "empDisplayNone";
        $optional['disabled'] = "disabled";

        $data['liveWith'] = $this->getLiveWithOption($empModel['Live_with']);
        $data['residentWith'] = $this->getResidentOption($empModel['Resident_type']);

        /******************************************
         *   TAB time
         *
         *******************************************/

        $rel['option_dayoff'] = $this->getOptionDay($empModel['DayOff']);
        $rel['Prosonnal_Being'] = $this->getOptioPersonalStatus($empModel['Prosonnal_Being']);
        $rel['Main_EndWork_Reason'] = $this->getOptionMainEndWorkReason($empModel['Main_EndWork_Reason']);

        $startWorkTime_slt = explode(":", $empModel['startWorkTime']);
        $rel['startWorkTime_h'] = $this->getHourTime($startWorkTime_slt[0]);
        $rel['startWorkTime_m'] = $this->getMinTime($startWorkTime_slt[1]);

        $endWorkTime_slt = explode(":", $empModel['endWorkTime']);
        $rel['endWorkTime_h'] = $this->getHourTime($endWorkTime_slt[0]);
        $rel['endWorkTime_m'] = $this->getMinTime($endWorkTime_slt[1]);

        $beginLunch_slt = explode(":", $empModel['beginLunch']);
        $rel['beginLunch_h'] = $this->getHourTime($beginLunch_slt[0]);
        $rel['beginLunch_m'] = $this->getMinTime($beginLunch_slt[1]);

        $endLunch_slt = explode(":", $empModel['endLunch']);
        $rel['endLunch_h'] = $this->getHourTime($endLunch_slt[0]);
        $rel['endLunch_m'] = $this->getMinTime($endLunch_slt[1]);

        $rel['job_type'] = $this->getOptionJobType($empModel['job_type']);

        $arrEduLevel = ApiPersonal::getArrayEducationLevel();

        return $this->render('formaddnewemp', [
            'data' => $data,
            'empModel' => $empModel,
            'addrInf' => $addrInf,
            'optional' => $optional,
            'dataNo' => $getVal['dataNo'],
            'family' => $familyModel,
            'rel' => $rel,
            'arrEduLevel' => $arrEduLevel,
            'taxIncomeModel' => $taxIncomeModel,
            'mode' => 'view',  //set mode to view only
        ]);
    }

    /******************************************
     *   แสดง form  bondsman
     *
     *******************************************/
    public function actionBondsmanform()
    {

        $getVal = Yii::$app->request->get();

        /******************************************
         *   TAB ทั่วไป
         *
         *******************************************/
        $empModel = Empdata::findOne($getVal['emp_id']);

        $optional = [];

        $bmModel = $mMB = EmpMoneyBonds::find()->where(['=', 'emp_data_id', $getVal['emp_id']])->one();
        $bmTel = EmpMoneyBondsTel::find()->where(['=', 'emp_data_id', $getVal['emp_id']])->all();

        if ($getVal['viewOnly'] == 1) {
            $optional['dontouch'] = "dontouch";
            $optional['readonly'] = "readonly";
            $optional['viewOnly'] = true;
            $optional['displayNone'] = "empDisplayNone";
            $optional['disabled'] = "disabled";
        }

        $sexToNum = $bmModel['bondsman_sex'];
        $optional['option_bename_th'] = $this->createBeNameDropdownOptions($sexToNum, $bmModel['bondsman_prefix'], 'th');

        return Yii::$app->controller->renderPartial('bondsman', [
            'empModel' => $empModel,
            'optional' => $optional,
            'bmModel' => $bmModel,
            'bmTel' => $bmTel
        ]);
    }

    /******************************************
     *   แสดง ตาราง  ประวัติจ่ายเงิน
     *
     *******************************************/
    public function actionMoneybondspayment()
    {
        //
        $getVal = Yii::$app->request->get();
        $bmTel = EmpMoneyBondsPayment::find()->where(['=', 'emp_data_id', $getVal['emp_id']])->all();

        return Yii::$app->controller->renderPartial('bondspayment', [
            'bmTel' => $bmTel
        ]);

    }

    /******************************************
     *   แสดง tab promise
     *
     *******************************************/
    public function actionEmppromisetb()
    {

        $getVal = Yii::$app->request->get();

        $empModel = Empdata::findOne($getVal['emp_id']);

        $optional = [];

        $empProms = EmpPromise::find()->where(['=', 'emp_data_id', $getVal['emp_id']])->all();


        if ($getVal['viewOnly'] == 1) {
            $optional['dontouch'] = "dontouch";
            $optional['readonly'] = "readonly";
            $optional['viewOnly'] = true;
            $optional['displayNone'] = "empDisplayNone";
            $optional['disabled'] = "disabled";
        }


        return Yii::$app->controller->renderPartial('promisetab', [
            'empModel' => $empModel,
            'optional' => $optional,
            'empProms' => $empProms
        ]);
    }

    /******************************************
     *   แสดง tab workhistory
     *
     *******************************************/
    public function actionEmpworkhistorytb()
    {

        $getVal = Yii::$app->request->get();

        $empModel = Empdata::findOne($getVal['emp_id']);

        $optional = [];

        $empWorkHis = $this->getWorkHistory($empModel->ID_Card);
        //WorkHistory

        if ($getVal['viewOnly'] == 1) {
            $optional['dontouch'] = "dontouch";
            $optional['readonly'] = "readonly";
            $optional['viewOnly'] = true;
            $optional['displayNone'] = "empDisplayNone";
            $optional['disabled'] = "disabled";
        }

        $rel['reason_out_type'] = $this->getOptionMainEndWorkReason();

        return Yii::$app->controller->renderPartial('workhistorytab', [
            'empModel' => $empModel,
            'optional' => $optional,
            'empWorkHis' => $empWorkHis,
            'rel' => $rel
        ]);
    }

    public function actionCaldiffdate()
    {

        $getVal = Yii::$app->request->get();
        //echo '<pre>';print_r($getVal);echo '</pre>';exit();

        $t1 = new \DateTime($this->toggleToDataPicker($getVal['date1']));
        $t2 = new \DateTime($this->toggleToDataPicker($getVal['date2']));
        $interval = $t1->diff($t2);
        //$interval->format('%y ปี').' '.$interval->format('%m เดือน');

        return $interval->format('%y ปี') . ' ' . $interval->format('%m เดือน');
    }

    public function getWorkHistory($idcard)
    {
        $resOut = [];
        $wHis = WorkHistory::find()
            ->where(['=', 'emp_id', $idcard])
            ->andWhere(['in', 'status', [1, 2, 3]])
            ->all();

        foreach ($wHis as $no => $wh) {

            $resOut[$wh->status][] = $wh;

        }

        return $resOut;
    }

    public function actionSearchposition()
    {

        $str = Yii::$app->request->get();

        $empData = [];
        // $empData[] = ['id' => 1,'text' => 'aaa'];
        // $empData[] = ['id' => 2,'text' => 'bbb'];

        $posObjs = Position::find()
            ->where(['like', 'PositionCode', $str['q']])
            ->orWhere(['like', 'Name', $str['q']])
            ->all();

        foreach ($posObjs as $no => $posObj) {
            $empData[] = [
                'id' => $posObj->PositionCode,
                'text' => $posObj->PositionCode . ' ' . $posObj->Name,
                'name' => $posObj->Name,
                'code' => $posObj->PositionCode

            ];
        }

        return json_encode($empData);
    }


    /******************************************
     *   ปรินต์ promise
     *
     *******************************************/
    public function actionPrintpromise()
    {

        $getVal = Yii::$app->request->get();

        $mPromise = EmpPromise::findOne($getVal['promiseId']);

        //parse word

        //--


        //$mpdf = new mPDF('th', 'Tharlon-Regular');
        $mpdf = new \Mpdf\Mpdf();

        // $mpdf->useFixedNormalLineHeight = false;
        // $mpdf->useFixedTextBaseline = false;
        // $mpdf->adjustFontDescLineheight = 8.14;

        $mpdf->useFixedNormalLineHeight = true;
        $mpdf->useFixedTextBaseline = true;
        $mpdf->normalLineheight = 1.53;

        $fullText = $this->parsePromiseWord($mPromise[$getVal['type']], $getVal);

        $mpdf->WriteHTML($fullText);
        $mpdf->Output();
        exit;
    }

    public function parsePromiseWord($text, $condition = array())
    {


        //echo '<pre>';print_r($rWord);echo "</pre>"; exit();

        $words = EmpPromiseMapping::find()->all();

        // $objConds = $objWhere = EmpPromiseMappingCondition::find()->all();
        // $cons = [];
        // foreach($objConds as $no => $objCond){
        //     $cons[$objCond->id] = $objCond;
        // }


        foreach ($words as $no => $objWord) {

            if ($objWord->db_name == "") {
                //fixable word
                $text = str_replace($objWord->param_text, $objWord->default_text, $text);

            } else {

                //check exist word
                $matchs = [];
                $patt = "/" . $objWord->param_text . "/";
                preg_match($patt, $text, $matchs);

                if (count($matchs) > 0) {

                    //have word

                    //get from database
                    $sql = "SELECT ";
                    if ($objWord->sel_command == "") {
                        $sql .= " " . $objWord->field_name . " ";
                    } else {
                        $sql .= " " . $objWord->sel_command . " AS " . $objWord->field_name . " ";
                    }

                    $sql .= " FROM ";
                    $sql .= " " . $objWord->db_name . "." . $objWord->tb_name . " ";

                    //get condition
                    $where = ' WHERE 1 ';
                    $arIn = explode(',', $objWord->condition_id);
                    $objWhere = EmpPromiseMappingCondition::find()->where(['IN', 'id', $arIn])->all();

                    foreach ($objWhere as $wNo => $cond) {
                        if ($cond->param_name == '' || $cond->param_name == null) {
                            $where .= " AND " . $cond->field_name . " = '" . $cond->default_value . "' ";
                        } else {
                            $where .= " AND " . $cond->field_name . " = '" . $condition[$cond->param_name] . "' ";
                        }

                    }
                    //--
                    $sql .= $where;
                    $sql .= " LIMIT 1";

                    $rWords = Yii::$app
                        ->dbERP_easyhr_checktime
                        ->createCommand($sql)
                        ->queryAll();

                    foreach ($rWords as $wNo => $valInf) {

                        $text = str_replace($objWord->param_text, $valInf[$objWord->field_name], $text);
                    }
                }
                //--


            }

        }

        return $text;
    }


    public function actionAddpromise()
    {

        $getVal = Yii::$app->request->get();

        $empModel = Empdata::findOne($getVal['emp_id']);

        $arrType = [];
        $arrType[] = $getVal['promiseType'];

        $rPremise = $this->savePermise($getVal['emp_id'], $empModel['ID_Card'], $arrType);

        //echo "<pre>";print_r($rPremise);echo '</pre>';
        return json_encode($rPremise);
    }

    public function actionShowaddpromise()
    {
        $getVal = Yii::$app->request->get();

        $haveType = json_decode($getVal['havePromise'], true);
        //check 
        $promNow = EmpPromiseType::find()->all();
        if (count($promNow) == count($haveType)) {
            $html = "<h2>เพิ่มสัญญาครบทุกประเภทแล้ว</h2>";
        } else {
            $html = "<select id='selPromiseType' class='form-control' >" . $this->getOptionPromise('', $haveType) . "</select>";
        }
        //--

        return $html;
    }

    public function actionSignpromsie()
    {

        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = '';

        $getVal = Yii::$app->request->get();

        $resOut = $this->toggleSignpromise($getVal['promiseId'], true);

        return json_encode($resOut);
    }

    public function actionGetcontentpromise()
    {

        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = '';

        $getVal = Yii::$app->request->get();

        //$resOut = $this->toggleSignpromise($getVal['promiseId'],true);
        $mPromise = EmpPromise::findOne($getVal['promiseId']);

        $resOut['detail'] = $mPromise->promise_detail;
        $resOut['bond'] = $mPromise->promise_bond;
        $resOut['attach'] = $mPromise->promise_attach;

        return json_encode($resOut);

    }

    public function actionEditcontentpromise()
    {

        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = '';

        $getVal = Yii::$app->request->post();

        $cont = json_decode($getVal['content'], true);

        //echo '<pre>';print_r($cont);echo '</pre>';exit();

        $mPromise = EmpPromise::findOne($getVal['promiseId']);
        //echo '<pre>';print_r($mPromise);echo '</pre>';exit();
        $mPromise->promise_detail = $cont['promise_detail'];
        $mPromise->promise_bond = $cont['promise_bond'];
        $mPromise->promise_attach = $cont['promise_attach'];

        $mPromise->save();


        return json_encode($resOut);

    }

    public function toggleSignpromise($promiseId, $set)
    {

        $resOut = [];
        $resOut['status'] = true;
        $resOut['msg'] = '';

        $prom = EmpPromise::findOne($promiseId);
        $prom->promise_sign_status = 1;

        if (!$prom->save()) {

            $resOut['status'] = false;
            $resOut['msg'] .= "update promise_sign_status error \n";
        }


        return $resOut;
    }

    public function actionDelpromise()
    {

        $getVal = Yii::$app->request->get();
        $inIds = json_decode($getVal['promise_ids']);

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        $res = EmpPromise::deleteAll(['in', 'id', $inIds]);

        return json_encode($resOut);

    }

    public function actionGetchildren()
    {

        $getVal = Yii::$app->request->get();

        $dataSet = [];

        //$getVal['id_card'] = '3570600084825';
        $getVal['id_card'] = Utility::removeDashIDcard($getVal['id_card']);
        if (isset($getVal['id_card']) && $getVal['id_card'] != "") {
            $mChilds = Children::find()->where(['=', 'emp_id', $getVal['id_card']])->all();
            // echo '<pre>';print_r($mChilds);echo "</pre>";

            foreach ($mChilds as $no => $mChild) {

                $mChild->birthday = $this->toggleToDataPicker($mChild->birthday);
                $dataSet[] = $mChild->attributes;
            }
        }

        return json_encode(["data" => $dataSet]);
    }

    public function actionGeteducation()
    {

        $getVal = Yii::$app->request->get();

        $dataSet = [];
        $getVal['id_card'] = Utility::removeDashIDcard($getVal['id_card']);

        if (isset($getVal['id_card']) && $getVal['id_card'] != "") {
            $mEdus = Education::find()->where(['=', 'emp_id', $getVal['id_card']])->all();

            foreach ($mEdus as $no => $mEdu) {
                $dataSet[] = $mEdu->attributes;
            }
        }

        return json_encode(["data" => $dataSet]);
    }

    public function actionGetposition()
    {

        $getVal = Yii::$app->request->get();
        // print_r($getVal['id_card']);
        $dataSet = [];
        $getVal['id_card'] = Utility::removeDashIDcard($getVal['id_card']);

        if (isset($getVal['id_card']) && $getVal['id_card'] != "") {
            $mEdus = Empsalary::find()->where(['=', 'EMP_SALARY_ID_CARD', $getVal['id_card']])->all();

            foreach ($mEdus as $no => $mEdu) {
                $dataSet[] = $mEdu['EMP_SALARY_ID'];  //counter
                $dataSet[] = $mEdu['statusSSO'];  //isPaidSSO
                $dataSet[] = $mEdu['statusMainJob'];    //isMainPosition
                $dataSet[] = $mEdu['statusCalculate'];  //isPaidSalary
                $dataSet[] = $mEdu['EMP_SALARY_WORKING_COMPANY']; //companyName
                $dataSet[] = $mEdu['EMP_SALARY_POSITION_CODE'];  //positionCode
                $dataSet[] = 'Position Name';   //positionName
                $dataSet[] = '01/10/2017';  //StartEffectDate
                $dataSet[] = '31/10/2017';  //EndEffectDate
                $dataSet[] = 'LvMid';   //salaryChart
                $dataSet[] = '4';   //salaryType
                $dataSet[] = '4.0'; //salaryStep
                $dataSet[] = '15000'; //salaryAmt
                $dataSet[] = '1';     //status
                $dataSet[] = 'x';
                $dataSet[] = '56';    //CompanyID
                $dataSet[] = '72';    //DepartmentID
                $dataSet[] = 'Production';  //DepartmentName
                $dataSet[] = '251';   //SectionID
                $dataSet[] = 'Development'; //SectionName
                $dataSet[] = 'Adithep'; //ChangeBy
                $dataSet[] = '1.5';   //StepAdded

            }
        }

        return json_encode(["data" => $dataSet]);
    }


    public function actionPosition()
    {
        return $this->render('position');
    }

    public function actionIncomehistory()
    {
        return $this->render('incomehistory');
    }

    public function actionTimeattendance()
    {
        return $this->render('timeattendance');
    }

    public function actionWorkhistory()
    {
        return $this->render('workhistory');
    }

    public function actionPermit()
    {
        return $this->render('permit');
    }

    public function actionSearchemp()
    {
        return $this->render('searchemp');
    }

    public function actionSeachbenameth()
    {
        $getValue = Yii::$app->request->get();
        $typeGender = $getValue['typeGender'];
        $getdata = ApiPersonal::getBenameth($typeGender);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $getdata;
    }

    public function actionSeachbenameen()
    {
        $getValue = Yii::$app->request->get();
        $typeGender = $getValue['typeGender'];
        $getdata = ApiPersonal::getBenameen($typeGender);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $getdata;
    }

    public function saveGenaralTab($objInMain, $objWageChannel, $dataNo, $imageName)
    {

        $objIn = $objInMain['objIn'];
        $imgEmp = $objInMain['imgEmp'];

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        //convert data
        $objIn['ID_Card'] = Utility::removeDashIDcard($objIn['ID_Card']);
        $objIn['Birthday'] = $this->toggleToDataPicker($objIn['Birthday']);
        $objIn['DateOfIssuePassport'] = $this->toggleToDataPicker($objIn['DateOfIssuePassport']);
        $objIn['DateOfExpiryPassport'] = $this->toggleToDataPicker($objIn['DateOfExpiryPassport']);
        $objIn['DateOfIssueIdcrad'] = $this->toggleToDataPicker($objIn['DateOfIssueIdcrad']);
        $objIn['DateOfExpiryIdcrad'] = $this->toggleToDataPicker($objIn['DateOfExpiryIdcrad']);

        //--

        /**************************************
         *   copy imp image && replace_file_name
         *
         ***************************************/

        if (isset($imgEmp) && $imgEmp['name'] != "") {

            $spliName = explode(".", $imgEmp['name']);
            $imgEmp['name'] = ($objIn['ID_Card'] != '') ? $objIn['ID_Card'] . "." . $spliName[count($spliName) - 1] : $imgEmp['name'];

            $cpImgEmp = $this->copyImage($imgEmp, '/upload/personal/');

            if ($cpImgEmp['status'] == false) {
                //throw new ErrorException($cpImgEmp['msg']);
                $resOut['status'] = false;
                $resOut['msg'] = $cpImgEmp['msg'];
                return $resOut;
            }

            $objIn['Pictures_HyperL'] = $imgEmp['name'];
            $objIn['image_card'] = $imageName;


        }

        //hash password 
        if (isset($objIn['password']) && $objIn['password'] != '') {
            //$objIn['password'] = Yii::$app->getSecurity()->generatePasswordHash($objIn['password']);
            $objIn['password'] = $this->salt($objIn['password'], 'a', 'a');
        }
        //--
        //$objIn['Blood_G'] = 'Aaaaaaaaaaaaaaaaaaaa';

//        echo '--------';
//        print_r($dataNo);
//        echo '--------';

        if ($dataNo == ' ') {



            //save general
            $newEmp = new Empdata();
            $getAttr = $newEmp->attributes;
            $newEmp->attributes = array_merge($getAttr, $objIn, $objWageChannel);

        } if ($dataNo != ' ') {



            //update general
            if (isset($objIn['password']) && $objIn['password'] == '') {
                unset($objIn['password']);
            }


            $newEmp = Empdata::findOne($dataNo);


            $newEmp = $this->createDataUpdate($newEmp, $objIn);

            //update Wage Channel
            $getAttr = $newEmp->attributes;

            $newEmp->attributes = array_merge($getAttr, $objWageChannel);

            //TODO : update action, update wage channel;
        }


        if ($newEmp->validate()) {

            $afterSave = $newEmp->save(false);
            if (!$afterSave) {
                $resOut['status'] = false;
                $resOut['msg'] = "save genaral tab error!!!";
            } else {
                $resOut['lastId'] = $newEmp->DataNo;
            }
        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($newEmp->errors);
        }


        return $resOut;
    }

    public function createValiMsg($ar)
    {
        $txt = "";
        foreach ($ar as $f => $msgArrs) {

            foreach ($msgArrs as $no => $msg) {
                $txt .= "- " . $msg . "<br>";
            }
        }

        return $txt;
    }

    public function saveAddrTab($objInMain, $dataNo, $isNew)
    {

        $objIn = $objInMain['objAddr'];
        $imgMap = $objInMain['imgMap'];

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        /**************************************
         *   copy map image && replace_file_name
         *
         ***************************************/

        if (isset($imgMap) && $imgMap['name'] != "") {

            $spliName = explode(".", $imgMap['name']);

            $imgMap['name'] = $dataNo . "." . $spliName[count($spliName) - 1];

            $cpImgEmp = $this->copyImage($imgMap, '/upload/map/');

            if ($cpImgEmp['status'] == false) {
                //throw new ErrorException($cpImgEmp['msg']);
                $resOut['status'] = false;
                $resOut['msg'] = $cpImgEmp['msg'];

                return $resOut;
            }

            //$objIn['ADDR_MAP_DESC'] = $imgMap['name'];

        }

        $arMap = [1 => 'home_addr', 2 => 'personal_card_addr', 3 => 'contact_addr'];
        //echo 'objIn:<pre>';print_r($objIn);echo '</pre>';
        foreach ($arMap as $k => $vName) {

            $indexName = $arMap[$k];
            $curObj = $objIn[$indexName];
            $curObj['ADDR_MAP_DESC'] = $imgMap['name'];

            //custom data 
            if ($k == 3) {

                $copyType = 0;
                if (isset($curObj['isCopyPersonalAddr']) && $curObj['isCopyPersonalAddr'] == 1) {

                    foreach ($arMap as $cType => $cName) {
                        if ($cName == $curObj['copy_addr']) {
                            $copyType = $cType;
                            break;
                        }
                    }

                }

                $curObj['ADDR_REF_MAIN'] = $copyType;

                unset($curObj['isCopyPersonalAddr']);
                unset($curObj['copy_addr']);

            } else {
                $curObj['ADDR_REF_MAIN'] = (!isset($curObj['ADDR_REF_MAIN']) || $curObj['ADDR_REF_MAIN'] == "") ? 0 : $curObj['ADDR_REF_MAIN'];
            }
            //--

            $curAddr = EmpAddress::find()
                ->where(['=', 'ADDR_DATA_NO', $dataNo])
                ->andWhere(['=', 'ADDR_TYPE', $k])
                ->one();

            if (empty($curAddr)) {
                //new
                $addrHomeNew = new Empaddress();
                $getAttr = $addrHomeNew->attributes;
                //$curObj['ADDR_REF_MAIN'] = (!isset($curObj['ADDR_REF_MAIN']) || $curObj['ADDR_REF_MAIN'] == "") ? 0:$curObj['ADDR_REF_MAIN'];
                $curObj['ADDR_DATA_NO'] = $dataNo;
                $curObj['ADDR_TYPE'] = $k;


                $addrHomeNew->attributes = array_merge($getAttr, $curObj);

            } else {




                //update
                $addrHomeNew = Empaddress::findOne($curAddr->id);
                //$curObj['ADDR_REF_MAIN'] = (!isset($curObj['ADDR_REF_MAIN']) || $curObj['ADDR_REF_MAIN'] == "") ? 0:$curObj['ADDR_REF_MAIN'];
                $addrHomeNew = $this->createDataUpdate($addrHomeNew, $curObj);
            }
            //echo '<pre>';print_r($addrHomeNew);echo '</pre>';exit();
            if ($addrHomeNew->validate()) {

                if (!$addrHomeNew->save(false)) {

                    $resOut['status'] = false;
                    $resOut['msg'] .= "save " . $indexName . " address tab error \n";
                }

            } else {
                $resOut['status'] = false;
                //echo '--><pre>';print_r($addrHomeNew->errors);echo '</pre>';
                $resOut['msg'] .= $this->createValiMsg($addrHomeNew->errors);
            }
        }

        //
        $emp = Empdata::findOne($dataNo);


        if (!isset($objIn['liveWith']['Live_with'])) {
            $emp->Live_with = 'ไม่ระบุ';
        } else {
            $emp->Live_with = ($objIn['liveWith']['Live_with'] == 'อื่นๆ') ? $objIn['liveWith']['liveWithOtherDesc'] : $objIn['liveWith']['Live_with'];
        }

        if (!isset($objIn['residentWith']['Resident_type'])) {
            $emp->Resident_type = 'ไม่ระบุ';
        } else {
            $emp->Resident_type = ($objIn['residentWith']['Resident_type'] == 'อื่นๆ') ? $objIn['residentWith']['residentWithOtherDesc'] : $objIn['residentWith']['Resident_type'];
        }


        $emp->Latitude = $objIn['Latitude'];
        $emp->Longitude = $objIn['Longitude'];

        $emp->Tel_Num = $objIn['Tel_Num'];//Utility::removeDashIDcard($objIn['Tel_Num']);
        $emp->Mobile_Num = $objIn['Mobile_Num'];//Utility::removeDashIDcard($objIn['Mobile_Num']);

        //main address
        $emp->Address = $objIn['home_addr']['ADDR_NUMBER'];
        $emp->Moo = $objIn['home_addr']['ADDR_GROUP_NO'];
        $emp->Road = $objIn['home_addr']['ADDR_ROAD'];
        $emp->Village = $objIn['home_addr']['ADDR_VILLAGE'];
        $emp->SubDistrict = $objIn['home_addr']['ADDR_SUB_DISTRICT'];
        $emp->District = $objIn['home_addr']['ADDR_DISTRICT'];
        $emp->Province = $objIn['home_addr']['ADDR_PROVINCE'];
        $emp->geography = $objIn['home_addr']['ADDR_GEOGRAPHY'];
        $emp->Postcode = $objIn['home_addr']['ADDR_POSTCODE'];

        if (isset($imgMap['name']) && $imgMap['name'] != '') {
            $emp->Pic_map = $imgMap['name'];
        }

        if ($emp->validate()) {

            if (!$emp->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "update Live_with,Resident_type  address tab error \n";
            }

        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($emp->errors);
        }

        return $resOut;
    }

    public function saveFamily($objInMain, $dataNo)
    {

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        $objFamily = $objInMain['objFamily'];

        $objFamily['father_idcard'] = Utility::removeDashIDcard($objFamily['father_idcard']);
        $objFamily['mother_idcard'] = Utility::removeDashIDcard($objFamily['mother_idcard']);
        $objFamily['spouse_idcard'] = Utility::removeDashIDcard($objFamily['spouse_idcard']);

        $objFamily['father_birthday'] = $this->toggleToDataPicker($objFamily['father_birthday']);
        $objFamily['mother_birthday'] = $this->toggleToDataPicker($objFamily['mother_birthday']);
        $objFamily['spouse_birthday'] = $this->toggleToDataPicker($objFamily['spouse_birthday']);


        $eFamily = Empfamily::find()->where(['=', 'emp_data_id', $dataNo])->one();

        if ($objFamily['relative_status'] == 'อื่นๆ') {
            $objFamily['relative_status'] = $objFamily['relative_status_desc'];
        }

        unset($objFamily['relative_status_desc']);

        if (isset($eFamily)) {
            //update
            $eFamily = $this->createDataUpdate($eFamily, $objFamily);

        } else {
            //new record
            $eFamily = new Empfamily();
            $objFamily['emp_data_id'] = $dataNo;

            $getAttr = $eFamily->attributes;
            $eFamily->attributes = array_merge($getAttr, $objFamily);
        }


        if ($eFamily->validate()) {

            if (!$eFamily->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "(1)save family tab error \n";
            }

        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($eFamily->errors);
        }

        //update emp_data
        $emp = Empdata::findOne($dataNo);
        $emp->Father = $objFamily['father_name'] . " " . $objFamily['father_surname'];
        $emp->Father_Job = $objFamily['father_business'];
        $emp->Mother = $objFamily['mother_name'] . " " . $objFamily['mother_surname'];
        $emp->Mother_Job = $objFamily['mother_business'];
        $emp->Parent_Status = $objFamily['parent_status'];
        $emp->Status = $objFamily['spouse_status'];
        $emp->Spouse_prefix = $objFamily['spouse_be'];
        $emp->Spouse_Name = $objFamily['spouse_name'];
        $emp->Spouse_Surname = $objFamily['spouse_surname'];
        $emp->Spouse_Birthday = $objFamily['spouse_birthday'];
        $emp->Spouse_career = $objFamily['spouse_business'];
        $emp->Spouse_workplace = $objFamily['spouse_place'];
        $emp->Emergency_Call_Person = $objFamily['relative_name'] . " " . $objFamily['relative_surname'];
        $emp->Emergency_Relation = $objFamily['relative_status'];
        $emp->Emergency_Phone_Num = $objFamily['relative_phone'];


        if ($emp->validate()) {

            if (!$emp->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "(2)update family tab error \n";
            }

        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($emp->errors);
        }


        return $resOut;

    }

    public function saveChildren($objMain, $id_card)
    {

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        //all
        if (isset($objMain['allChild']) && $objMain['allChild'] != '') {
            foreach ($objMain['allChild'] as $no => $vObj) {

                $vObj['birthday'] = $this->toggleToDataPicker($vObj['birthday']);

                if ($vObj['id'] != '') {
                    //update
                    $moChild = Children::findOne($vObj['id']);
                    $vObj['emp_id'] = $id_card;

                    $moChild = $this->createDataUpdate($moChild, $vObj);
                    // echo '<pre>';print_r($moChild);echo "</pre>";
                    // exit();

                    if ($moChild->validate()) {

                        if (!$moChild->save(false)) {

                            $resOut['status'] = false;
                            $resOut['msg'] .= "save children error \n";
                        }

                    } else {
                        $resOut['status'] = false;
                        $resOut['msg'] = $this->createValiMsg($moChild->errors);
                    }

                } else {

                    //insert
                    $moChild = new Children();
                    $getAttr = $moChild->attributes;
                    $vObj['emp_id'] = $id_card;
                    $moChild->attributes = array_merge($getAttr, $vObj);


                    if ($moChild->validate()) {

                        if (!$moChild->save(false)) {

                            $resOut['status'] = false;
                            $resOut['msg'] .= "save children error \n";
                        }

                    } else {
                        $resOut['status'] = false;
                        $resOut['msg'] = $this->createValiMsg($moChild->errors);
                    }

                }


            }
        }

        //delete
        if (isset($objMain['del']) && $objMain['del'] != '') {
            Children::deleteAll(['in', 'id', $objMain['del']]);

        }

        //update emp_data
        if (isset($objMain['childs']) && $objMain['childs'] != '') {
            //update emp_data
            $emp = Empdata::find()->where(['=', 'ID_Card', $id_card])->one();
            $emp->Childs = $objMain['childs'];


            if ($emp->validate()) {

                if (!$emp->save(false)) {

                    $resOut['status'] = false;
                    $resOut['msg'] .= "update emp_data.Childs tab error \n";
                }

            } else {
                $resOut['status'] = false;
                $resOut['msg'] = $this->createValiMsg($emp->errors);
            }
        }
        //--

        return $resOut;

    }

    public function saveEducation($objMain, $id_card)
    {

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";
        //all
        if (isset($objMain['allEdu']) && $objMain['allEdu'] != '') {
            foreach ($objMain['allEdu'] as $no => $vObj) {

                if ($vObj['id'] != '') {
                    //update
                    $mol = Education::findOne($vObj['id']);
                    $vObj['emp_id'] = $id_card;
                    $mol = $this->createDataUpdate($mol, $vObj);

                    if ($mol->validate()) {

                        if (!$mol->save(false)) {

                            $resOut['status'] = false;
                            $resOut['msg'] .= "save education error \n";
                        }
                    } else {
                        $resOut['status'] = false;
                        $resOut['msg'] = $this->createValiMsg($mol->errors);
                    }
                } else {

                    //insert
                    $mol = new Education();
                    $getAttr = $mol->attributes;
                    $vObj['emp_id'] = $id_card;
                    $mol->attributes = array_merge($getAttr, $vObj);
                    if ($mol->validate()) {
                        if (!$mol->save(false)) {
                            $resOut['status'] = false;
                            $resOut['msg'] .= "save education error \n";
                        }

                    } else {
                        $resOut['status'] = false;
                        $resOut['msg'] = $this->createValiMsg($mol->errors);
                    }
                }
            }
        }

        //delete
        if (isset($objMain['del']) && $objMain['del'] != '') {
            Education::deleteAll(['in', 'id', $objMain['del']]);

        }
        return $resOut;
    }


    public function saveEmpposition($objMain, $id_card, $Emp_id, $Emp_name)
    {

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        //Get All Salary Step
        $modelSalaryStep = Salarystep::find()->orderBy('SALARY_STEP_ID ASC')->asArray()->all();
        $arrAllSalaryStep = [];
        foreach ($modelSalaryStep as $item) {
            $arrAllSalaryStep[$item['SALARY_STEP_CHART_NAME']][$item['SALARY_STEP_LEVEL']] = $item;
        }


        //Get All Position
        $modelPosition = Position::find()->where([
            'Status' => 1,
        ])->orderBy('id ASC')->asArray()->all();
        $arrAllPosition = [];
        foreach ($modelPosition as $item) {
            $arrAllPosition[$item['PositionCode']] = $item;
        }


        //DELETE Empsalary by ID Card
        Empsalary::deleteAll([
            'EMP_SALARY_ID_CARD' => $id_card,
        ]);

        //Update Organic_chart
        OrganicChart::updateAll([
            'Emp_id' => null,
            'active_status' => 2,
            'Emp_name' => null,
            'Emp_IDCard' => null,
            'positionCode' => null,
            'update_date' => new Expression('NOW()'),
        ], [
            'Emp_IDCard' => $id_card
        ]);

        if (isset($objMain['allPosition']) && $objMain['allPosition'] != '') {

            foreach ($objMain['allPosition'] as $no => $vObj) {

                $k1 = trim($vObj['salaryChart']);
                $k2 = trim($vObj['salaryType']);

                //Map Array Salary Step
                $arrSalaryStep = $arrAllSalaryStep[$k1][$k2];

                $positionCode = $vObj['positionCode'];

                //Get Organic_chart ID
                $Organic_chartID = $vObj['IDRefOrgChart'];

                //Map Array Position
                $arrPosition = $arrAllPosition[$positionCode];

                //Array Main Position
                //$arrMainPosition = [];
                if ($vObj['isMainPosition'] == 1) {  //IF this position is main position
//                    $arrMainPosition = [
//                        'id_card' => $id_card,
//                        'data_no' => $Emp_id,
//                        'company_id' => $vObj['CompanyID'],
//                        'department_id' => $vObj['DepartmentID'],
//                        'section_id' => $vObj['SectionID'],
//                        'position_code' => $positionCode,
//                        'position_name' => $vObj['positionName'],
//                    ];

                    /*** update emp_data **/
                    $emp_data = Empdata::findOne($Emp_id);
                    if ($emp_data !== null) {

                        $_being =  $emp_data->Prosonnal_Being;

                        $emp_data->Code = $positionCode;
                        $emp_data->Position = $vObj['positionName'];
                        $emp_data->Working_Company = $vObj['CompanyID'];
                        $emp_data->Department = $vObj['DepartmentID'];
                        $emp_data->Section = $vObj['SectionID'];
                        $emp_data->Prosonnal_Being = ($_being > 0) ? $_being :  2; //ทดลองงาน
                        $emp_data->save();
                    } else {
                        $resOut['status'] = false;
                        $resOut['msg'] .= " $Emp_id Not found, emp data error \n";
                    }

                }


                $model = new Empsalary();
                $model->EMP_SALARY_ID_CARD = $id_card;
                $model->EMP_SALARY_POSITION_CODE = $positionCode;
                $model->EMP_SALARY_POSITION_LEVEL = $arrPosition['Level'];
                $model->EMP_SALARY_WORKING_COMPANY = $vObj['CompanyID'];
                $model->EMP_SALARY_DEPARTMENT = $vObj['DepartmentID'];
                $model->EMP_SALARY_SECTION = $vObj['SectionID'];
                $model->EMP_SALARY_CHART = $vObj['salaryChart'];
                $model->EMP_SALARY_STEP = $vObj['salaryStep'];
                $model->EMP_SALARY_LEVEL = $vObj['salaryType'];
                $model->EMP_SALARY_WAGE = $vObj['salaryAmt'];
                $model->EMP_SALARY_CHART_ID = $arrSalaryStep['SALARY_STEP_ID'];
                $model->EMP_SALARY_GUARANTEE_MONEY = null;
                $model->status = 1;
                $model->statusSSO = $vObj['isPaidSSO'];
                $model->statusCalculate = $vObj['isPaidSalary'];
                $model->statusMainJob = $vObj['isMainPosition'];
                $model->start_effective_date = DateTime::DateToMysqlDB($vObj['StartEffectDate']);
                $model->end_effective_date = DateTime::DateToMysqlDB($vObj['EndEffectDate']);
                $model->EMP_SALARY_CREATE_DATE = new Expression('NOW()');
                $model->EMP_SALARY_CREATE_BY = $this->idcardLogin;
                $model->EMP_SALARY_UPDATE_DATE = null;
                $model->EMP_SALARY_UPDATE_BY = null;
                $model->SALARY_STEP_ADD = $vObj['StepAdded'];
                $model->SALARY_CHANGE_ID = $vObj['WhatChangeID'];
                $model->SALARY_CHANGE_NAME = $vObj['WhatChangeName'];
                $model->id_ref_orgchart = $Organic_chartID;


                $savePos = false;
                if ($model->validate()) {
                    if (!$model->save(false)) {
                        $resOut['status'] = false;
                        $resOut['msg'] .= "save position error \n";
                    } else {
                        $savePos = true;
                    }

                } else {
                    $resOut['status'] = false;
                    $resOut['msg'] = $this->createValiMsg($model->errors);
                }


                //update to organize chart with Organic_chart ID;
                if ($savePos) {

                    /*** update Organize chart **/
                    $orgChart = OrganicChart::findOne(['id' => $Organic_chartID]);



                    if ($orgChart === null) {
                        $resOut['status'] = false;
                        $resOut['msg'] .= " $Organic_chartID Not found, organize chart error \n";

                    } else {
                       // print_r($orgChart);
                        //Update Organize Chart
                        $orgChart->Emp_id = $Emp_id;
                        $orgChart->create_date = new Expression('NOW()');
                        $orgChart->update_date = new Expression('NOW()');
                        $orgChart->active_status = 1; //Active
                        $orgChart->Emp_name = $Emp_name;
                        $orgChart->Emp_IDCard = $id_card;
                        $orgChart->positionCode = $vObj['positionCode'];
                        $orgChart->save();

                    }




                }


            } //End of for loop

        }

        //delete
        if (isset($objMain['del']) && $objMain['del'] != '') {
            Empsalary::deleteAll(['in', 'id', $objMain['del']]);
        }
        return $resOut;
    }


    public function saveTaxIncome($objMain, $emp_id)
    {

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        // echo '<pre>';print_r($objMain);echo '</pre>';
        // exit();
        //unset($objMain['tax_income_child']);

        $mTaxincome = TaxIncome::find()->where(['=', 'emp_id', $emp_id])->one();

        if (isset($mTaxincome)) {
            //update
            $mTaxincome = $this->createDataUpdate($mTaxincome, $objMain);

        } else {
            //new record
            $mTaxincome = new TaxIncome();
            $objMain['emp_id'] = $emp_id;

            $getAttr = $mTaxincome->attributes;
            $mTaxincome->attributes = array_merge($getAttr, $objMain);
        }


        if ($mTaxincome->validate()) {

            if (!$mTaxincome->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "save tax income tab error \n";
            }

        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($mTaxincome->errors);
        }


        return $resOut;

    }

    public function saveTime($objMain, $emp_id)
    {

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        // echo '<pre>';print_r($objMain);echo '</pre>';
        // exit();
        //unset($objMain['tax_income_child']);

        //startWorkTime
        $objMain['startWorkTime'] = $objMain['startWorkTime_h'] . ':' . $objMain['startWorkTime_m'];
        unset($objMain['startWorkTime_h']);
        unset($objMain['startWorkTime_m']);

        //endWorkTime
        $objMain['endWorkTime'] = $objMain['endWorkTime_h'] . ':' . $objMain['endWorkTime_m'];
        unset($objMain['endWorkTime_h']);
        unset($objMain['endWorkTime_m']);

        //beginLunch
        $objMain['beginLunch'] = $objMain['beginLunch_h'] . ':' . $objMain['beginLunch_m'];
        unset($objMain['beginLunch_h']);
        unset($objMain['beginLunch_m']);

        //endLunch
        $objMain['endLunch'] = $objMain['endLunch_h'] . ':' . $objMain['endLunch_m'];
        unset($objMain['endLunch_h']);
        unset($objMain['endLunch_m']);

        //$objMain['job_type'] = 'full-time';
        $objMain['Start_date'] = $this->toggleToDataPicker($objMain['Start_date']);
        $objMain['Probation_Date'] = $this->toggleToDataPicker($objMain['Probation_Date']);
        $objMain['End_date'] = $this->toggleToDataPicker($objMain['End_date']);

        $mEmp = Empdata::findOne($emp_id);

        if (isset($mEmp)) {
            //update
            $mEmp = $this->createDataUpdate($mEmp, $objMain);

        } else {
            //new record
            // $mEmp = new Empdata();
            // $objMain['emp_id'] = $emp_id;

            // $getAttr    = $mEmp->attributes;
            // $mEmp->attributes = array_merge($getAttr,$objMain);
        }


        if ($mEmp->validate()) {

            if (!$mEmp->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "save tax income tab error \n";
            }

        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($mEmp->errors);
        }


        return $resOut;

    }

    public function saveEmpOut($objMain, $emp_id)
    {

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        //update emp
        //echo '<pre>';print_r($objMain);echo '</pre>';

        $mEmp = Empdata::findOne($emp_id);
        //
        if (isset($mEmp)) {
            //update
            $arUpdate = [];
            $arUpdate['username'] = '#####Out Off###';
            $arUpdate['Prosonnal_Being'] = 3;

            $mEmp = $this->createDataUpdate($mEmp, $arUpdate);

        }
        //echo '<pre>';print_r($mEmp);echo '</pre>';exit();
        if ($mEmp->validate()) {

            if (!$mEmp->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "save tax income tab error \n";
            }

        } else {

            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($mEmp->errors);
        }


        $sql = "SELECT p.Name AS positionName,p.PositionCode, p.WorkCompany ";
        $sql .= " FROM ";
        $sql .= " emp_data e";
        $sql .= " INNER JOIN ERP_easyhr_OU.relation_position rp ON(e.ID_Card = rp.id_card)";
        $sql .= " INNER JOIN ERP_easyhr_OU.position p ON(rp.position_id = p.id)";
        $sql .= " WHERE  ";
        $sql .= " e.DataNo = '" . $emp_id . "' ";

        $rPosition = Yii::$app
            ->dbERP_easyhr_checktime
            ->createCommand($sql)
            ->queryOne();

        //echo '<pre>';print_r($rPosition);echo '</pre>';                
        $sql = "SELECT Name FROM ERP_easyhr_OU.working_company WHERE id = '" . $rPosition['WorkCompany'] . "' ";

        $rWorkcom = Yii::$app
            ->dbERP_easyhr_checktime
            ->createCommand($sql)
            ->queryOne();


        //insert work history
        $wHis = new WorkHistory();

        $dataSet = [];
        $dataSet['emp_id'] = $mEmp->ID_Card;
        $dataSet['old_work_company'] = $rWorkcom['Name'];
        $dataSet['position_code'] = $rPosition['PositionCode'];//Position
        $dataSet['old_work_position'] = $rPosition['positionName'];
        $dataSet['work_since_full'] = $mEmp->Start_date;
        $dataSet['work_until_full'] = $mEmp->End_date;
        $dataSet['reason_out_type'] = $mEmp->Main_EndWork_Reason;
        $dataSet['reason_out_detail'] = $mEmp->EndWork_Reason;
        $dataSet['status'] = 2;

        $getAttr = $wHis->attributes;
        $wHis->attributes = array_merge($getAttr, $dataSet);


        if ($wHis->validate()) {

            if (!$wHis->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "save tax income tab error \n";
            }

        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($wHis->errors);
        }


        return $resOut;

    }

    public function saveBondsman($objMain, $emp_id)
    {

        //EmpMoneyBonds

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        // echo '<pre>';print_r($objMain);echo '</pre>';
        // exit();
        //unset($objMain['tax_income_child']);
        $bondsman_tel = $objMain['boundsman_tel'];
        unset($objMain['boundsman_tel']);
        unset($objMain['bondsman_sel_main_tel']);

        $objMain['bondsman_idcard'] = Utility::removeDashIDcard($objMain['bondsman_idcard']);

        $objMain['start_date_pay'] = $this->toggleToDataPicker($objMain['start_date_pay']);
        $objMain['end_date_pay'] = $this->toggleToDataPicker($objMain['end_date_pay']);
        $objMain['bondsman_birthday'] = $this->toggleToDataPicker($objMain['bondsman_birthday']);

        if ($objMain['start_pay_bonds'] == '1') {
            $objMain['pay_money'] = $objMain['money_bonds'];
            $objMain['first_money'] = 0;
        }

        $mMB = EmpMoneyBonds::find()->where(['=', 'emp_data_id', $emp_id])->one();

        if (isset($mMB)) {
            //update
            $mMB = $this->createDataUpdate($mMB, $objMain);

        } else {
            //new record
            $mMB = new EmpMoneyBonds();
            $objMain['emp_data_id'] = $emp_id;

            $getAttr = $mMB->attributes;
            $mMB->attributes = array_merge($getAttr, $objMain);
        }

        //tel
        $mTels = EmpMoneyBondsTel::find()->where(['=', 'emp_data_id', $emp_id])->all();
        if (!empty($mTels)) {
            //delete
            Yii::$app
                ->dbERP_easyhr_checktime
                ->createCommand()
                ->delete('emp_money_bonds_tel', ['emp_data_id' => $emp_id])
                ->execute();

        }


        if (count($bondsman_tel) > 0) {

            $dataTels = [];
            foreach ($bondsman_tel as $no => $objTel) {
                $activceMain = ($objTel['main']) ? '1' : '0';
                $dataTels[] = [
                    //'id' => '',
                    'emp_data_id' => $emp_id,
                    'emp_money_bonds_id' => $mMB->id,
                    'tel' => $objTel['tel'],
                    'main_active' => $activceMain
                ];
            }

            //echo '<pre>';print_r($dataTels);echo '</pre>';
            Yii::$app->dbERP_easyhr_checktime->createCommand()->batchInsert('emp_money_bonds_tel',
                [
                    //'id',
                    'emp_data_id',
                    'emp_money_bonds_id',
                    'tel',
                    'main_active'

                ], $dataTels)->execute();
        }


        if ($mMB->validate()) {

            if (!$mMB->save(false)) {

                $resOut['status'] = false;
                $resOut['msg'] .= "save bondsman tab error \n";
            }

        } else {
            $resOut['status'] = false;
            $resOut['msg'] = $this->createValiMsg($mMB->errors);
        }

        if ($resOut['status'] && $objMain['start_pay_bonds'] == '1') {

            $resPay = $this->bondPayment($emp_id);
            $resOut['status'] = $resPay['status'];
            $resOut['msg'] = $resPay['msg'];

        }

        return $resOut;

    }

    public function bondPayment($empId, $date = '')
    {

        $resOut = [];
        $resOut['status'] = true;
        $resOut['active_pay'] = true;
        $resOut['msg'] = "";


        if ($date == '') {
            $date = date('y-m-d');
        }

        //get month
        $exp = explode('-', $date);
        $payMonth = $exp[1];


        $mMB = EmpMoneyBonds::find()->where(['=', 'emp_data_id', $empId])->one();
        $payLast = EmpMoneyBondsPayment::find()
            ->where(['=', 'emp_data_id', $empId])
            ->orderBy(['id' => SORT_DESC])
            ->one();


        //check have bond 
        $check = $this->checkBondPayment($mMB, $payLast, $payMonth);
        $resOut['active_pay'] = $check['active_pay'];
        $desc = $check['desc'];

        if ($check['active_pay']) {

            $emp = EmpData::findOne($empId);

            $bonds_bring_forward = 0.00;
            $bonds_balance = 0.00;


            if (empty($payLast)) {
                //new pay

                $bonds_bring_forward = ((float)$mMB->money_bonds - (float)$mMB->first_money);
                $bonds_balance = $bonds_bring_forward - (float)$mMB->pay_money;

            } else {

                $bonds_bring_forward = $payLast->bonds_balance;
                $bonds_balance = $bonds_bring_forward - (float)$mMB->pay_money;
            }

            $payNow = new EmpMoneyBondsPayment();
            $payNow->money_bonds_id = $mMB->id;
            $payNow->emp_data_id = $empId;
            $payNow->emp_idcard = $emp->ID_Card;
            $payNow->bonds_date = $date;
            $payNow->bonds_list = $desc;
            $payNow->bonds_bring_forward = $bonds_bring_forward;
            $payNow->bonds_amount = $mMB->pay_money;
            $payNow->bonds_balance = $bonds_balance;
            $payNow->create_date = date('y-m-d');


            if ($payNow->validate()) {

                if (!$payNow->save(false)) {

                    $resOut['status'] = false;
                    $resOut['msg'] .= "save bondsman payment  error \n";
                }

            } else {
                $resOut['status'] = false;
                $resOut['msg'] = $this->createValiMsg($payNow->errors);
            }
        } else {


            $resOut['active_pay'] = false;
            $resOut['msg'] = $check['msg'];;
        }


        return $resOut;

    }

    public function checkBondPayment($mMB, $payLast, $payMonth)
    {

        $resOut = [];
        $resOut['active_pay'] = true;
        $resOut['desc'] = '';
        $resOut['msg'] = '';

        //check have bond 
        if ($mMB->start_money_bonds == 0) {

            $resOut['active_pay'] = false;
            $resOut['msg'] = 'ไม่มีเงินค้ำ';

        } else {

            if ($mMB->start_pay_bonds == 2) {

                //check balance
                if (!empty($payLast) && $payLast->bonds_balance <= 0) {
                    //ครบ
                    $resOut['active_pay'] = false;
                    $resOut['msg'] = 'ชำระงวดสุดท้ายครบแล้ว';

                } else {

                    $resOut['active_pay'] = true;
                    $resOut['desc'] = 'เงินค้ำประกันประจำเดือน ' . DateTime::mappingMonth($payMonth);
                }

            } else {

                //จ่ายครบครั้งแรก
                if (empty($payLast)) {
                    $resOut['active_pay'] = true;
                    $resOut['desc'] = 'จ่ายเงินค้ำประกันครบ';
                } else {
                    $resOut['active_pay'] = false;
                    $resOut['msg'] = 'ชำระครั้งแรกครบแล้ว';
                }
            }
        }

        return $resOut;
    }

    //
    public function saveWorkHistory($objMain, $emp_id)
    {

        //EmpMoneyBonds
        $loop = array(
            1 => 'type1',
            3 => 'type3'
        );

        //echo '<pre>';print_r($objMain);echo '</pre>';

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        foreach ($loop as $setStatus => $textStatus) {

            if (isset($objMain[$textStatus]) && count($objMain[$textStatus]) > 0) {
                foreach ($objMain[$textStatus] as $no => $objWh) {
                    // 
                    $objWh['status'] = $setStatus;


                    $objWh['work_since_full'] = $this->toggleToDataPicker($objWh['work_since_full']);
                    $objWh['work_until_full'] = $this->toggleToDataPicker($objWh['work_until_full']);


                    if ($objWh['id'] == '') {
                        //new record
                        $mWh = new WorkHistory();
                        $objWh['emp_id'] = $emp_id;

                        $getAttr = $mWh->attributes;
                        $mWh->attributes = array_merge($getAttr, $objWh);

                    } else {
                        $idRun = (int)$objWh['id'];
                        $mWh = WorkHistory::findOne($idRun);
                        $mWh = $this->createDataUpdate($mWh, $objWh);

                    }

                    //save
                    if ($mWh->validate()) {

                        if (!$mWh->save(false)) {

                            $resOut['status'] = false;
                            $resOut['msg'] .= "save work history tab error \n";
                        }

                    } else {
                        $resOut['status'] = false;
                        $resOut['msg'] .= $this->createValiMsg($mWh->errors);
                    }


                }
            }
        }


        //delete
        if (isset($objMain['del']) && count($objMain['del']) > 0) {
            $res = WorkHistory::deleteAll(['in', 'id', $objMain['del']]);
        }


        return $resOut;

    }


    public function savePermise($emp_id, $id_card, $premiseType = array('1', '2', '3'))
    {

        //EmpMoneyBonds

        $resOut = array();
        $resOut['status'] = true;
        $resOut['msg'] = "";

        $mPt = EmpPromiseType::find()->where(['in', 'promise_type', $premiseType])->all();
        //echo "<pre>";print_r($mPt);echo "</pre>";
        $objPermise = [];
        foreach ($mPt as $no => $prType) {
            $objPermise[] = array(

                'emp_promise_type_id' => $prType->id,
                'emp_data_id' => $emp_id,
                'emp_idcard' => $id_card,
                'promise_code' => "",
                'promise_name' => $prType->permise_name,
                'promise_detail' => $prType->promise_detail,
                'promise_bond' => $prType->promise_bond,
                'promise_attach' => $prType->promise_attach,
                'promise_sign_status' => 0,
                'promise_sign_date' => date('Y-m-d h:i:s')

            );

            $sPromise = new EmpPromise();
            $sPromise->emp_promise_type_id = $prType->id;
            $sPromise->emp_data_id = $emp_id;
            $sPromise->emp_idcard = $id_card;
            $sPromise->promise_code = "";
            $sPromise->promise_name = $prType->permise_name;
            $sPromise->promise_detail = $prType->promise_detail;
            $sPromise->promise_bond = $prType->promise_bond;
            $sPromise->promise_attach = $prType->promise_attach;
            $sPromise->promise_sign_status = 0;
            $sPromise->promise_sign_date = date('Y-m-d h:i:s');

            $sPromise->save();

        }

        if (!empty($objPermise)) {

            //   $rInsert =   Yii::$app->dbERP_easyhr_checktime->createCommand()->batchInsert('emp_promise',
            //         [
            //             //'id',
            //             'emp_promise_type_id',
            //             'emp_data_id',
            //             'emp_idcard',
            //             'promise_code',
            //             'promise_name',
            //             'promise_detail',
            //             'promise_bond',
            //             'promise_attach',
            //             'promise_sign_status',
            //             'promise_sign_date'

            //         ],$objPermise)->execute();


            //update run code
            //$insAbove = EmpPromise::find()->where(['=', 'emp_data_id'])->all();
            Yii::$app->dbERP_easyhr_checktime->createCommand("UPDATE emp_promise SET promise_code=LPAD(id, 6, '0') WHERE emp_data_id=:emp_data_id")
                ->bindValue(':emp_data_id', $emp_id)
                ->execute();
            //
        }

        //echo '<pre>';print_r($rInsert);echo '</pre>';

        return $resOut;

    }

    public function actionTestsave()
    {

        //    $moChildxx     = Children::findOne(5);
        //     //$moChild->school = 'testabcd';
        //     //$moChild = new Children();
        //     $moChildxx->name = 'testabcd';

        //     $moChildxx->save(false);
        // $premiseType = [1,2];
        // $mPt = EmpPromiseType::find()->where(['in','promise_type',$premiseType])->all();
        // echo "<pre>";print_r($mPt);echo "</pre>";

        // $objPermise = [];
        // $objPermise[] = array(

        //         'emp_promise_type_id' => 1,
        //         'emp_data_id' => 1,
        //         'emp_idcard' => '',
        //         'promise_code' => "",
        //         'promise_name' => '',
        //         'promise_detail' => "aaa{{test}}xxxx",
        //         'promise_bond' => '',
        //         'promise_attach' => '',
        //         'promise_sign_status' => 0,
        //         'promise_sign_date' => date('Y-m-d h:i:s')

        //     );

        // $rInsert =   Yii::$app->dbERP_easyhr_checktime->createCommand()->batchInsert('emp_promise', 
        //         [
        //             //'id', 
        //             'emp_promise_type_id', 
        //             'emp_data_id',
        //             'emp_idcard',
        //             'promise_code',
        //             'promise_name',
        //             'promise_detail',
        //             'promise_bond',
        //             'promise_attach',
        //             'promise_sign_status',
        //             'promise_sign_date'

        //         ],$objPermise)->execute();

        $prom = new EmpPromise();
        $prom->emp_data_id = 1;
        $prom->emp_promise_type_id = 1;
        $prom->promise_detail = "xxxx{{test}}xxxx";
        $prom->save();

        return '';
    }

    public function copyImage($fileObj, $pathSub)
    {
        $res = array();
        $res['status'] = true;
        $res['msg'] = "";
        $res['output'] = array();

        $urlUpload = dirname(dirname(dirname(dirname(__FILE__))));

        //$sDir       = "/var/www/html/wseasyerp/upload/personal/";
        $sDir = $urlUpload . $pathSub;//"/upload/personal/";
        $filename = $fileObj['name'];
        $tmp_name = $fileObj['tmp_name'];

        if (move_uploaded_file($tmp_name, $sDir . $filename)) {
            $res['status'] = true;
        } else {

            $res['status'] = false;
            $res['msg'] = "copy file error.";
        }

        return $res;
    }

    function setDefault($arrIn)
    {

        $arOut = array();
        foreach ($arrIn as $fk => $val) {
            $arOut[$fk] = "";
        }

        return $arOut;
    }

    function getFieldWithDefault($model)
    {

        $resOut = array();
        foreach ($model->attributes as $kf => $val) {

            $type = $model->getTableSchema()->columns[$kf]->type;

            if ($type == 'string' || $type == 'text') {
                $resOut[$kf] = "";
            } else if ($type == 'integer') {
                $resOut[$kf] = 0;
            } else {
                $resOut[$kf] = $val;
            }
        }
        return $resOut;

    }

    function craetFieldName($inObj)
    {
        $resObj = array();
        foreach ($inObj as $fName => $value) {
            $resObj[] = $fName;
        }

        return $resObj;
    }

    function createBeNameDropdownOptions($beType, $sel = "", $ln = 'th')
    {

        $out = "";
        $getdata = ApiPersonal::getBenameth($beType);

        //echo "<pre>";print_r($getdata);echo "</pre>";

        foreach ($getdata as $k => $val) {

            $select = ($val['title_name_th'] == $sel) ? "selected" : "";
            $txt = ($ln == 'th') ? $val['title_name_th'] : $val['title_name_en'];
            $out .= "<option value='" . $txt . "' " . $select . " >" . $txt . "</option>";
        }

        return $out;
    }

    function createDataUpdate($model, $data)
    {

        foreach ($data as $k => $v) {
            $model[$k] = $v;
        }

        return $model;
    }

    function validateEmp($data, $dataNo = "", $who = "")
    {

        $model = null;
        if ($who == 'ID_Card') {  //check id card employee
            $model = Empdata::find()
                ->where(['ID_Card' => $data['ID_Card']])
                ->one();

            if ($model === null) return false; //not found
            if ($model != null && $model->DataNo == $dataNo) return false; //found that is me
            if ($model != null && $model->DataNo != $dataNo) return true;
        } else {
            $c = [];
            //$cond = array_merge([$who=>$data['ID_Card']],$c);
            $model = Empfamily::find()
                ->where(['father_idcard' => $data['ID_Card']])
                ->orWhere(['mother_idcard' => $data['ID_Card']])
                ->orWhere(['spouse_idcard' => $data['ID_Card']])
                ->one();
            if ($model === null) return false; //not found
            if ($model != null && $model->emp_data_id == $dataNo) return false; //found that is me
            if ($model != null && $model->emp_data_id != $dataNo) return true;
        }
    }


    private function getLiveWithOption($chkNow = "")
    {

        $arOut = [];
        $arOut[] = ['name' => 'Live_with', 'text' => 'บิดา มารดา', 'value' => 'บิดา มารดา', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Live_with', 'text' => 'ครอบครัวตนเอง', 'value' => 'ครอบครัวตนเอง', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Live_with', 'text' => 'ญาติ', 'value' => 'ญาติ', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Live_with', 'text' => 'เพื่อน', 'value' => 'เพื่อน', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Live_with', 'text' => 'แฟน', 'value' => 'แฟน', 'checked' => '', 'type' => '1'];

        //--
        //--
        //add before other
        $arOut[] = [
            'name' => 'Live_with',
            'text' => 'อื่นๆ (ระบุ)',
            'value' => 'อื่นๆ',
            'checked' => false,
            'type' => '2',
            'option' => ['']
        ];

        //is check
        if ($chkNow != "") {
            $isSet = false;
            foreach ($arOut as $k => $vl) {
                if ($vl['value'] == $chkNow) {
                    $arOut[$k]['checked'] = "checked";
                    $isSet = true;
                }
            }
            if (!$isSet) {
                $arOut[count($arOut) - 1]['checked'] = "checked";
            }
        } else {
            $arOut[count($arOut) - 1]['checked'] = "checked";
        }

        return $arOut;
    }

    private function getResidentOption($chkNow = "")
    {

        $arOut = [];
        $arOut[] = ['name' => 'Resident_type', 'text' => 'บ้านตนเอง', 'value' => 'บ้านตนเอง', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Resident_type', 'text' => 'บ้านเช่า', 'value' => 'บ้านเช่า', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Resident_type', 'text' => 'บ้านญาติ', 'value' => 'บ้านญาติ', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Resident_type', 'text' => 'บ้านเพื่อน', 'value' => 'บ้านเพื่อน', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Resident_type', 'text' => 'บ้านแฟน', 'value' => 'บ้านแฟน', 'checked' => '', 'type' => '1'];
        $arOut[] = ['name' => 'Resident_type', 'text' => 'หอพัก', 'value' => 'หอพัก', 'checked' => '', 'type' => '1'];


        //--
        //--
        //add before other
        $arOut[] = [
            'name' => 'Resident_type',
            'text' => 'อื่นๆ (ระบุ)',
            'value' => 'อื่นๆ',
            'checked' => false,
            'type' => '2',
            'option' => ['']
        ];

        //is check
        if ($chkNow != "") {
            $isSet = false;
            foreach ($arOut as $k => $vl) {
                if ($vl['value'] == $chkNow) {
                    $arOut[$k]['checked'] = "checked";
                    $isSet = true;
                }
            }
            if (!$isSet) {
                $arOut[count($arOut) - 1]['checked'] = "checked";
            }
        } else {
            $arOut[count($arOut) - 1]['checked'] = "checked";
        }

        return $arOut;
    }

    public function createSelOptParentStatus($sel)
    {

        $arrData = [];
        $arrData[] = ['value' => 'อยู่ด้วยกัน', 'text' => 'อยู่ด้วยกัน'];
        $arrData[] = ['value' => 'หย่า', 'text' => 'หย่า'];
        $arrData[] = ['value' => 'แยกกันอยู่', 'text' => 'แยกกันอยู่'];
        $arrData[] = ['value' => 'บิดาเสียชีวิต', 'text' => 'บิดาเสียชีวิต'];
        $arrData[] = ['value' => 'มารดาเสียชีวิต', 'text' => 'มารดาเสียชีวิต'];
        $arrData[] = ['value' => 'เสียชีวิตทั้งคู่', 'text' => 'เสียชีวิตทั้งคู่'];

        $htmlOut = "";
        foreach ($arrData as $no => $vObj) {

            $select = ($sel == $vObj['value']) ? "selected" : "";
            $htmlOut .= '<option value="' . $vObj['value'] . '" ' . $select . ' >' . $vObj['text'] . '</option>';
        }

        return $htmlOut;

    }

    public function createSelOptSpouseStatus($sel)
    {

        $arrData = [];
        $arrData[] = ['value' => 'โสด', 'text' => 'โสด'];
        $arrData[] = ['value' => 'อยู่ด้วยกัน', 'text' => 'อยู่ด้วยกัน'];
        $arrData[] = ['value' => 'สมรสแล้ว', 'text' => 'สมรสแล้ว'];
        $arrData[] = ['value' => 'สมรสแต่ไม่จดทะเบียน', 'text' => 'สมรสแต่ไม่จดทะเบียน'];
        $arrData[] = ['value' => 'หย่า', 'text' => 'หย่า'];
        $arrData[] = ['value' => 'คู่สมรสเสียชีวิต', 'text' => 'คู่สมรสเสียชีวิต'];

        $htmlOut = "";
        foreach ($arrData as $no => $vObj) {

            $select = ($sel == $vObj['value']) ? "selected" : "";
            $htmlOut .= '<option value="' . $vObj['value'] . '" ' . $select . ' >' . $vObj['text'] . '</option>';
        }

        return $htmlOut;

    }

    public function salt($string, $pos = 'a', $stype = 'a')
    {
        //global $saltkey,$config;
        $setconfig = Configpass::setpass();
        $config['saltkey'] = $setconfig;
        $saltkey = $config['saltkey'];
        $stringA = sha1($string);

        if ($pos == 'a') {
            $pos = rand(10, 38);
        }

        if ($stype == 'b') {
            $salt = md5($saltkey);
            $stype = 'b';
            $slen = 32;
        } else {
            $salt = sha1($saltkey);
            $stype = 'n';
            $slen = 40;
        }
        $afterstr = substr($stringA, $pos);
        $startbeginning = -(strlen($afterstr));
        $beforestr = substr($stringA, 0, $startbeginning);
        $salted = $beforestr . $salt . $afterstr . $stype . $pos;


        return $salted;
    }

    public function getDay()
    {
        $thai_day_arr = array("อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์");
        return $thai_day_arr;
    }

    public function getOptionDay($sel = '')
    {
        $html = "<option value=''>เลือก</option>";

        $days = $this->getDay();
        foreach ($days as $no => $txt) {
            $selected = ($txt == $sel) ? "selected" : "";
            $html .= '<option ' . $selected . ' value="' . $txt . '">' . $txt . '</option>';
        }

        return $html;
    }

    public function getOptioPersonalStatus($sel = '')
    {
        $html = "<option value=''>เลือก</option>";
        $status = array('1' => 'ผ่านงาน', '2' => 'ทดลองงาน', '3' => 'ลาออก', '4' => 'พนักงานรายวัน');
        foreach ($status as $val => $txt) {
            $selected = ($val == $sel) ? "selected" : "";
            $html .= '<option ' . $selected . ' value="' . $val . '">' . $txt . '</option>';
        }

        return $html;
    }

    public function getOptionMainEndWorkReason($sel = '')
    {
        $html = "<option value=''>เลือก</option>";
        $status = array(
            'ไม่ผ่านทดลองงาน',
            'ลาออกไม่ปรกติ',
            'ลาออก',
            'ให้ออก',
            'ไล่ออก',
            'หมดระยะเวลาฝึกงาน'
        );
        foreach ($status as $no => $txt) {
            $selected = ($txt == $sel) ? "selected" : "";
            $html .= '<option ' . $selected . ' value="' . $txt . '">' . $txt . '</option>';
        }

        return $html;
    }

    public function getOptionJobType($sel = '')
    {
        //$html = "<option value=''>เลือก</option>";
        $html = "";
        $status = array(
            'รายเดือน',
            'รายวัน'
        );
        foreach ($status as $no => $txt) {
            $selected = ($txt == $sel) ? "selected" : "";
            $html .= '<option ' . $selected . ' value="' . $txt . '">' . $txt . '</option>';
        }

        return $html;
    }

    public function getOptionPromise($sel = '', $notIn = array())
    {
        $html = "";
        $proTypes = EmpPromiseType::find()->where(['not in', 'id', $notIn])->all();

        foreach ($proTypes as $no => $proType) {
            $selected = ($proType->id == $sel) ? "selected" : "";
            $html .= '<option ' . $selected . ' value="' . $proType->id . '" data-protype="' . $proType->promise_type . '" >' . $proType->permise_name . '</option>';
        }

        return $html;
    }


    public function getHourTime($iStatus = null)
    {
        $hour = null;
        for ($i = 0; $i < 24; $i++) {
            $selected = '';
            if ($iStatus == $i) $selected = 'selected';
            if ($i < 10) $i = "0" . $i;
            $hour .= '<option ' . $selected . ' value="' . $i . '" >' . $i . '</option>';
        }
        return $hour;
    }

    public function getMinTime($selectedMinute = null)
    {
        $minutes = null;
        for ($i = 0; $i < 60; $i++) {
            $selected = '';
            if ($selectedMinute == $i) $selected = 'selected';
            if ($i < 10)
                $i = "0" . $i;
            $minutes .= '<option ' . $selected . ' value="' . $i . '" >' . $i . '</option>';
            $i = $i + 4;
        }
        return $minutes;
    }

    public function actionTesttoggledate()
    {

        $param = Yii::$app->request->queryParams;

        return PersonalController::toggleToDataPicker($param['date']);
    }

    public static function toggleToDataPicker($date)
    {

        $format = ['-', '/'];
        $dateOut = $date;

        $sp = [];
        $inFormate = false;
        foreach ($format as $no => $f) {
            $sp = explode($f, $date);
            if (count($sp) > 1) {
                $inFormate = true;
                break;
            }
        }

        if ($inFormate) {
            if (strlen($sp[0]) == 4) {
                $dateOut = $sp[2] . '/' . $sp[1] . '/' . $sp[0];
            } else {
                $dateOut = $sp[2] . '-' . $sp[1] . '-' . $sp[0];
            }
        }

        return $dateOut;

    }

    ///////  ค้นหาข้อมูลพนักงาน  ///////

    public function actionSearchempdata()
    {

        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            //print_r($postValue);
            //exit;
            $selectworking = $postValue['selectworking'];
            $selectdepartment = $postValue['selectdepartment'];
            $selectsection = $postValue['selectsection'];
            $nameEmp = $postValue['nameEmp'];
            $lastNameEmp = $postValue['lastNameEmp'];
            $nickName = $postValue['nickName'];

            $personal_begin1 = $postValue['personal_begin1'];
            $personal_begin2 = $postValue['personal_begin2'];
            $Personal_daily = $postValue['Personal_daily'];
            $personal_begin3 = $postValue['personal_begin3'];


            $start_date = ($postValue['start_date']) ? DateTime::DateToMysqlDB($postValue['start_date']) : null;
            $end_date = ($postValue['end_date']) ? DateTime::DateToMysqlDB($postValue['end_date']) : null;

            $db['ou'] = DBConnect::getDBConn()['ou'];
            $db['ct'] = DBConnect::getDBConn()['ct'];
            $db['pl'] = DBConnect::getDBConn()['pl'];

            /*
            $sql = "SELECT $db[ct].emp_data.* ,
                           CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                           $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD as EMP_SALARY_ID_CARD,
                           $db[pl].EMP_SALARY.EMP_SALARY_POSITION_CODE as EMP_SALARY_POSITION_CODE,
                           $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY as EMP_SALARY_WORKING_COMPANY,
                           $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT as EMP_SALARY_DEPARTMENT,
                           $db[pl].EMP_SALARY.EMP_SALARY_SECTION as EMP_SALARY_SECTION,
                           $db[pl].EMP_SALARY.status as EMP_SALARY_status,
                           $db[ou].working_company.name as  working_companynamem,
                           $db[ou].department.name as departmentname,
                           $db[ou].section.name as sectionname,
                           $db[ou].position.Name as positionname
                           
                    FROM $db[ct].emp_data 
                    INNER JOIN $db[pl].EMP_SALARY ON $db[pl].EMP_SALARY.EMP_SALARY_ID_CARD = $db[ct].emp_data.ID_Card
                    INNER JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[pl].EMP_SALARY.EMP_SALARY_WORKING_COMPANY
                    INNER JOIN $db[ou].department ON $db[ou].department.id = $db[pl].EMP_SALARY.EMP_SALARY_DEPARTMENT
                    INNER JOIN $db[ou].section ON $db[ou].section.id = $db[pl].EMP_SALARY.EMP_SALARY_SECTION
                    INNER JOIN $db[ou].position ON $db[ou].position.PositionCode = $db[pl].EMP_SALARY.EMP_SALARY_POSITION_CODE
                    WHERE $db[pl].EMP_SALARY.status != '99'
                    
                    ";
                */

            $sql = "SELECT
                    $db[ct].emp_data.* ,
                    CONCAT($db[ct].emp_data.Name,' ',$db[ct].emp_data.Surname) as Fullname,
                    $db[ou].working_company.name as  working_companynamem,
                    $db[ou].department.name as departmentname,
                    $db[ou].section.name as sectionname
                    FROM $db[ct].emp_data
                    LEFT JOIN $db[ou].working_company ON $db[ou].working_company.id = $db[ct].emp_data.Working_Company
                    LEFT JOIN $db[ou].department ON $db[ou].department.id = $db[ct].emp_data.Department
                    LEFT JOIN $db[ou].section ON $db[ou].section.id = $db[ct].emp_data.Section
                    WHERE 1=1 AND $db[ct].emp_data.Code != 'EX50001' ";


            //with company / department /section
            $sql .= ($selectworking != '') ? "AND $db[ct].emp_data.Working_Company= '$selectworking' " : '';
            $sql .= ($selectdepartment != '') ? "AND $db[ct].emp_data.Department= '$selectdepartment' " : '';
            $sql .= ($selectsection != '') ? "AND $db[ct].emp_data.Section= '$selectsection' " : '';


            if ($nameEmp != '' && $lastNameEmp != '' && $nickName != '') {
                $sql .= "AND $db[ct].emp_data.Name LIKE '%$nameEmp%'
                             AND $db[ct].emp_data.Surname LIKE '%$lastNameEmp%'
                             AND $db[ct].emp_data.Nickname LIKE '%$nickName%'";
            } else if ($nameEmp != '' && $lastNameEmp != '') {
                $sql .= "AND $db[ct].emp_data.Name LIKE '%$nameEmp%'
                             AND $db[ct].emp_data.Surname LIKE '%$lastNameEmp%'";
            } else if ($nameEmp != '') {
                $sql .= "AND $db[ct].emp_data.Name LIKE '%$nameEmp%'";
            }

            if ($personal_begin1 != '' && $personal_begin2 != '' && $personal_begin3 != '') {
                $sql .= "AND  $db[ct].emp_data.Prosonnal_Being  IN ($personal_begin1,$personal_begin2,$personal_begin3)";
            } else if ($personal_begin1 != '' && $personal_begin2 != '') {
                $sql .= "AND  $db[ct].emp_data.Prosonnal_Being  IN ($personal_begin1,$personal_begin2)";
            } else if ($personal_begin2 != '' && $personal_begin3 != '') {
                $sql .= "AND  $db[ct].emp_data.Prosonnal_Being  IN ($personal_begin2,$personal_begin3)";
            } else if ($personal_begin1 != '' && $personal_begin3 != '') {
                $sql .= "AND  $db[ct].emp_data.Prosonnal_Being  IN ($personal_begin1,$personal_begin3)";
            } else if ($personal_begin1 != '') {
                $sql .= "AND  $db[ct].emp_data.Prosonnal_Being  IN ($personal_begin1)";
            } else if ($personal_begin2 != '') {
                $sql .= "AND  $db[ct].emp_data.Prosonnal_Being  IN ($personal_begin2)";
            } else if ($personal_begin3 != '') {
                $sql .= "AND  $db[ct].emp_data.Prosonnal_Being  IN ($personal_begin3)";
            }


            $sql .= ($Personal_daily != '') ? "AND  $db[ct].emp_data.Personal_daily  = '$Personal_daily' " : '';
            $sql .= "ORDER BY $db[ct].emp_data.Start_date ASC";

            // echo $sql;
            // exit;

            $connection = \Yii::$app->dbERP_easyhr_checktime;
            $data = $connection->createCommand($sql)->queryAll();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $data;
        }

    }

    /**
     * @return string message
     */
    public function actionChangepassword()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost && (Yii::$app->session->get('pwd') != '')) {
            $old = Yii::$app->request->post('oldpassword');
            $new = Yii::$app->request->post('newpassword');
            $cnew = Yii::$app->request->post('cnewpassword');

            if (strcmp($old, Yii::$app->session->get('pwd')) !== 0) {
                $msg = 'รหัสผ่านเดิมไม่ถูกต้อง';
            } else if (strcmp($new, $cnew) !== 0) {
                $msg = 'รหัสผ่านใหม่และยืนยันรหัสผานไม่ตรงกัน';
            } else {
                $pwdSalt = $this->salt($cnew, 'a', 'a');
                $EmpData = Empdata::findOne(Yii::$app->session->get('datano'));
                $EmpData->password = $pwdSalt;
                $EmpData->UPDATE_TIME = new Expression('NOW()');
                $EmpData->UPDATE_BY = Yii::$app->session->get('fullname');
                if ($EmpData->save())
                    $msg = "1";
                else
                    $msg = "การบันทึกข้อมูลผิดพลาด";

            }
            return $msg;
        } else {
            return 'พบข้อผิดพลาด';
        }
    }


    public function actionSearchtaxpersonal()
    {
        if (Yii::$app->request->isAjax) {
            $postValue = Yii::$app->request->post();
            $id_card = $postValue['idcard'];
            $monthselect = $postValue['monthselect'];
            $yearselect = $postValue['yearselect'];
            $monthyearresult = $monthselect . "-" . $yearselect;

            $modelSearch = ApiSearchtaxpersonal::searchtaxpersonal($id_card, $monthyearresult);

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $modelSearch;

        }
    }

    public function actionPersonaltaxsearch()
    {
        $getValue = Yii::$app->request->get();
        $session = Yii::$app->session;
        $session->open();  //open session
        $USER_ID = $session->get('idcard');
        return Yii::$app->controller->renderPartial('personaltaxsearch');
    }

    public function actionGetyeartax()
    {
        $getValue = Yii::$app->request->get();
        $session = Yii::$app->session;
        $session->open();  //open session
        $USER_ID = $session->get('idcard');


    }

    /*    public function actionSearchtumbon()
        {
            $getValue = Yii::$app->request->get();
            $cond = $getValue['term']['term'];
            $sql="SELECT
                    d.district_id as id,
                    CONCAT('ต.',d.district_name,'__อ.',a.amphur_name,'__จ.',p.province_name,'__',c.PostCode) as text
                    FROM district as d
                    INNER JOIN amphur as a ON d.amphur_id=a.amphur_id
                    INNER JOIN province as p ON d.province_id=p.province_id
                    INNER JOIN postcode as c ON a.amphur_id=c.amphur_id
                    WHERE d.district_name LIKE '%".$cond."%'
                    ORDER BY d.district_name, a.amphur_name, p.province_name ";
            $connection = \Yii::$app->dbERP_easyhr_checktime;
            $data = $connection->createCommand($sql)->queryAll();
             //print_r($data);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $data;
        }*/

    public function actionSearchtumbon()
    {
        $getValue = Yii::$app->request->get();
        $cond = $getValue['term'];
        $sql = "SELECT 
                d.district_id as id,
                CONCAT('',d.district_name,'__',a.amphur_name,'__',p.province_name,'__',c.PostCode) as text
                FROM district as d 
                INNER JOIN amphur as a ON d.amphur_id=a.amphur_id 
                INNER JOIN province as p ON d.province_id=p.province_id
                INNER JOIN postcode as c ON a.amphur_id=c.amphur_id
                WHERE d.district_name LIKE '%" . $cond . "%'
                ORDER BY d.district_name, a.amphur_name, p.province_name ";
        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $model = $connection->createCommand($sql)->queryAll();
        $data = [];
        foreach ($model as $item) {
            $data[] = [
                'id' => $item['id'],
                'text' => $item['text']
            ];
        }

        echo json_encode($data);
        exit;
    }

    public function actionGetTAPPinfo($id)
    {

        $sql = "SELECT 
                d.district_id , d.district_name, a.amphur_name, p.province_name, c.PostCode
                FROM district as d 
                INNER JOIN amphur as a ON d.amphur_id=a.amphur_id 
                INNER JOIN province as p ON d.province_id=p.province_id
                INNER JOIN postcode as c ON a.amphur_id=c.amphur_id
                WHERE d.d.district_id = '$id' ";
        $connection = \Yii::$app->dbERP_easyhr_checktime;
        $data = $connection->createCommand($sql)->queryAll();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }


    public function actionLoadempposition()
    {
        if (Yii::$app->request->isAjax) {
            $getValue = Yii::$app->request->get();


            $emp_id = $getValue['dataNo'];
            $id_card = $getValue['idCard'];

            $db['ou'] = DBConnect::getDBConn()['ou'];
            $db['ct'] = DBConnect::getDBConn()['ct'];
            $db['pl'] = DBConnect::getDBConn()['pl'];

            if (empty($emp_id) || empty($id_card)) {
                throw new \Exception('ไม่พบรหัสพนักงาน');
            }

            $sql = "SELECT a.*,b.id as orgchart_id,a.*,c.name as company_name,d.name as department_name,s.name as section_name,e.Name as positionName
                    FROM $db[pl].EMP_SALARY as a
                    INNER JOIN $db[ou].Organic_chart as b  ON a.EMP_SALARY_ID_CARD = b.Emp_IDCard
                    INNER JOIN $db[ou].working_company as c  ON a.EMP_SALARY_WORKING_COMPANY = c.id
                    INNER JOIN $db[ou].department as d  ON a.EMP_SALARY_DEPARTMENT=d.id
                     INNER JOIN $db[ou].position as e  ON a.EMP_SALARY_POSITION_CODE=e.PositionCode
                    INNER JOIN $db[ou].section as s ON a.EMP_SALARY_SECTION=s.id
                    WHERE b.Emp_id=$emp_id
                    GROUP BY a.EMP_SALARY_POSITION_CODE
                   ";
            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $model = $connection->createCommand($sql)->queryAll();


            $i = 1;
            foreach ($model as $item) {
                //for($i=0;$i<=3;$i++){
                $obj = [];
                $obj['counter'] = $i; // $mEdu['EMP_SALARY_ID'];  //counter
                $obj['isPaidSSO'] = $item['statusSSO'];//$mEdu['statusSSO'];  //isPaidSSO
                $obj['isMainPosition'] = $item['statusMainJob'];//$mEdu['statusMainJob'];    //isMainPosition
                $obj['isPaidSalary'] = $item['statusCalculate']; //$mEdu['statusCalculate'];  //isPaidSalary
                $obj['companyName'] = $item['company_name'];//$mEdu['EMP_SALARY_WORKING_COMPANY']; //companyName
                $obj['positionCode'] = $item['EMP_SALARY_POSITION_CODE'];//$mEdu['EMP_SALARY_POSITION_CODE'];  //positionCode
                $obj['positionName'] = $item['positionName'];   //positionName
                $obj['StartEffectDate'] = DateTime::CalendarDate($item['start_effective_date']);  //StartEffectDate
                $obj['EndEffectDate'] = DateTime::CalendarDate($item['end_effective_date']);  //EndEffectDate
                $obj['salaryChart'] = $item['EMP_SALARY_CHART'];   //salaryChart
                $obj['salaryType'] = $item['EMP_SALARY_LEVEL'];   //salaryType
                $obj['salaryStep'] = $item['EMP_SALARY_STEP']; //salaryStep
                $obj['salaryAmt'] = $item['EMP_SALARY_WAGE']; //salaryAmt
                $obj['status'] = $item['status'];     //status
                $obj['action'] = 'action';
                $obj['CompanyID'] = $item['EMP_SALARY_WORKING_COMPANY'];    //CompanyID
                $obj['DepartmentID'] = $item['EMP_SALARY_DEPARTMENT'];    //DepartmentID
                $obj['DepartmentName'] = $item['department_name'];;  //DepartmentName
                $obj['SectionID'] = $item['EMP_SALARY_SECTION'];   //SectionID
                $obj['SectionName'] = $item['section_name']; //SectionName

                $obj['StepAdded'] = $item['SALARY_STEP_ADD'];   //StepAdded
                $obj['WhatChangeID'] = $item['SALARY_CHANGE_ID']; //WhatChangeID
                $obj['WhatChangeName'] = $item['SALARY_CHANGE_NAME']; //WhatChangeName
                $obj['IDRefOrgChart'] = $item['orgchart_id'];

                $castObj = (object)$obj;
                $dataSet[] = $castObj;


            }


            echo json_encode($dataSet);
            exit;
        }

    }


}
