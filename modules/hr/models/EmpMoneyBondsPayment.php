<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "emp_money_bonds_payment".
 *
 * @property integer $id
 * @property integer $money_bonds_id
 * @property integer $emp_data_id
 * @property string $emp_idcard
 * @property string $bonds_date
 * @property string $bonds_list
 * @property double $bonds_bring_forward
 * @property double $bonds_amount
 * @property double $bonds_balance
 * @property string $create_date
 */
class EmpMoneyBondsPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_money_bonds_payment';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['money_bonds_id', 'emp_data_id'], 'integer'],
            [['bonds_date', 'create_date'], 'safe'],
            [['bonds_bring_forward', 'bonds_amount', 'bonds_balance'], 'number'],
            [['emp_idcard'], 'string', 'max' => 13],
            [['bonds_list'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'money_bonds_id' => 'Money Bonds ID',
            'emp_data_id' => 'Emp Data ID',
            'emp_idcard' => 'Emp Idcard',
            'bonds_date' => 'Bonds Date',
            'bonds_list' => 'Bonds List',
            'bonds_bring_forward' => 'Bonds Bring Forward',
            'bonds_amount' => 'Bonds Amount',
            'bonds_balance' => 'Bonds Balance',
            'create_date' => 'Create Date',
        ];
    }
}
