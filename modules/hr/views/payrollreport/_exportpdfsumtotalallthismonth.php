<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 3/11/2017 AD
 * Time: 18:05
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$session = Yii::$app->session;
$monthselect = $session->get('monthselect');
$reportSalaryTotal = $session->get('reportSalaryTotal');

?>
<style>
    .container{
        font-family: "THSarabun";
        font-size: 16px;
    }
    p {
        font-family: "THSarabun";
        font-size: 16px;
    }
    h3 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }


    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }
    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;
    }
    .rpt tr.odd { background: #f3f3f3; }
    .rpt tr.even { background: #FFF; }


</style>
<h3 style="text-align:center;">รายละเอียดรายงาน เงินเดือน</h3>
<div class="row container">

        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-6">
            <h3><?php $getMonth = Datetime::convertFormatMonthYear($monthselect);
                    //print_r($getMonth);
                    $monthGetReport1 = $getMonth['monthresult'];
                    $resultMonthReport1 = Datetime::mapMonth($monthGetReport1);
                    $yearGetReport1 = $getMonth['year'];
            ?>รายงานสรุปรวมผลเงินเดือน รอบเงินเดือน  <?php echo $resultMonthReport1; echo '&nbsp;&nbsp;'.$yearGetReport1; ?></h3>
            </div>
        </div>
        <table  class="rpt"  cellpadding="5" style="border-collapse: collapse;" cellspacing="0"  width="100%" align="center">
            <thead>
                <tr role="row">
                    <th aria-sort="ascending" >
                    ชื่อบริษัท
                    </th>
                    <th aria-sort="ascending" >
                    เงินเดือน   
                    </th>
                    <th aria-sort="ascending" >
                    ส่วนเพิ่ม
                    </th>
                    <th aria-sort="ascending" >
                    รวมรับ
                    </th>
                    <th aria-sort="ascending" >
                    ส่วนหัก
                    </th>
                    <th aria-sort="ascending" >
                    รวมรับทั้งสิ้น
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $r = 1;
                $TotalSalary = $TotalAdd = $TotalEarn = $TotalDeduct = $TotalNet = 0;
                foreach($reportSalaryTotal as $value){
                    $cls = ($r%2==0) ? 'odd' : 'even';
                    $sumSalaryAddDeduc = $value['SUMSALARY'] + $value['SUM_WAGE_TOTAL_ADDITION'];
                    $TotalSalary += $value['SUMSALARY'];
                    $TotalAdd += $value['SUM_WAGE_TOTAL_ADDITION'];
                    $TotalEarn += $sumSalaryAddDeduc;
                    $TotalDeduct += $value['WAGE_TOTAL_DEDUCTION'];
                    $TotalNet += $sumSalaryAddDeduc-$value['WAGE_TOTAL_DEDUCTION'];
                    ?>
                <tr class="<?php echo $cls;?>">
                    <td width="30%"><?php  echo $value['COMPANY_NAME']?></td>
                    <td width="10%" align="right"><?php  echo Helper::displayDecimal($value['SUMSALARY']);?></td>
                    <td width="10%" align="right"><?php  echo Helper::displayDecimal($value['SUM_WAGE_TOTAL_ADDITION']);?></td>
                    <td width="10%" align="right"><?php
                            echo Helper::displayDecimal($sumSalaryAddDeduc);
                    ?></td>
                    <td width="10%" align="right"><?php  echo Helper::displayDecimal($value['WAGE_TOTAL_DEDUCTION']);?></td>
                    <td width="10%" align="right"><?php  echo Helper::displayDecimal($sumSalaryAddDeduc-$value['WAGE_TOTAL_DEDUCTION']);?></td>
                </tr>
                <?php  $r++; } ?>
            </tbody>
            <tfoot>
                <tr role="row">
                    <th aria-sort="ascending" >
                        รวม
                    </th>
                    <th aria-sort="ascending" >
                        <?php  echo Helper::displayDecimal($TotalSalary);?>
                    </th>
                    <th aria-sort="ascending" >
                        <?php  echo Helper::displayDecimal($TotalAdd);?>
                    </th>
                    <th aria-sort="ascending" >
                        <?php  echo Helper::displayDecimal($TotalEarn);?>
                    </th>
                    <th aria-sort="ascending" >
                        <?php  echo Helper::displayDecimal($TotalDeduct);?>
                    </th>
                    <th aria-sort="ascending" >
                        <?php  echo Helper::displayDecimal($TotalNet);?>
                    </th>
                </tr>
            </tfoot>
        </table>
</div>