<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use app\api\DateTime;

use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiDuringrepair;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$TimeStatusDaily = $session->get('TimeStatusDaily');
$selected_company = $session->get('selected_company');
$selected_months = $session->get('selected_months');
$selected_technician = $session->get('selected_technician');


?>


<h3><i class="fa fa-fw fa-file-text"></i>รายงานสถิติระยะเวลาใบงานเกินเวลามาตรฐาน ต่อวัน</h3>
<h5><?php  $namecompany = ApiReport::getNameCompanyPDF($selected_company) ;
        echo $namecompany['0']['name']; ?></h5>
<section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <!--<div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>-->
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table  bgcolor="#777"id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row" class="odd">
                                                 <th bgcolor="#eee"  class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    วันที่
                                                </th>
                                                <th bgcolor="#eee"  class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                ยอดเข้าบริการ
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                A1(รอรับแจ้งซ่อม)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                A1(คิดเป็น%)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                B1(รอจ่ายงานให้ช่าง)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                B1(คิดเป็น%)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                C1(รอปิดงานซ่อม)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                C1(คิดเป็น%)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                D1(ปิดงานซ่อม)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                D1(คิดเป็น%)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                E1(ชำระเงิน)
                                                </th>
                                                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                E1(คิดเป็น%)
                                                </th>
                                            </tr>
                                    </thead>
                                   
                                     <?php 
                                                 foreach ($TimeStatusDaily as $key => $value) {
                                            ?>
                                            <tr role="row" class="odd">
                                                <td align = "center"><?php
                                                echo $TimeStatusDaily[$key]['TimeSelect'];
                                                ?>
                                                </td>
                                                <td align = "center"><?php echo $TimeStatusDaily[$key]['CountCar'];
                                                     if($TimeStatusDaily[$key]['CountCar'] == 0){
                                                    $countCar = 1 ;
                                                }else
                                                {
                                                    $countCar = $TimeStatusDaily[$key]['CountCar'];
                                                };
                                                ?></td>
                                                <td align = "center"><?php echo $TimeStatusDaily[$key]['Time_DiffA'];?></td>
                                                <td align = "center"><?php $presentOfTimeStatusDaliyA =  ($TimeStatusDaily[$key]['Time_DiffA']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyA)));?></td>
                                                <td align = "center"><?php echo $TimeStatusDaily[$key]['Time_DiffB'];?></td>
                                                <td align = "center"><?php $presentOfTimeStatusDaliyB =  ($TimeStatusDaily[$key]['Time_DiffB']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyB)));?></td>
                                                <td align = "center"><?php echo $TimeStatusDaily[$key]['Time_DiffC'];?></td>
                                                <td align = "center"><?php $presentOfTimeStatusDaliyC =  ($TimeStatusDaily[$key]['Time_DiffC']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyC)));?></td>
                                                <td align = "center"><?php echo $TimeStatusDaily[$key]['Time_DiffD'];?></td>
                                                <td align = "center"><?php $presentOfTimeStatusDaliyD =  ($TimeStatusDaily[$key]['Time_DiffD']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyD)));?></td>
                                                <td align = "center"><?php echo $TimeStatusDaily[$key]['Time_DiffE'];?></td>
                                                <td align = "center"><?php $presentOfTimeStatusDaliyE =  ($TimeStatusDaily[$key]['Time_DiffE']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyE)));?></td>
                                            </tr>
                                            <?php  } ?>                           
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
<?php
session_start();
session_destroy();
?>