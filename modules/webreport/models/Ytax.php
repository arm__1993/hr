<?php

namespace app\modules\webreport\models;

use Yii;

/**
 * This is the model class for table "ytax".
 *
 * @property integer $id
 * @property string $datePay
 * @property string $pricewage
 */
class Ytax extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ytax';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['datePay'], 'safe'],
            [['pricewage'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datePay' => 'Date Pay',
            'pricewage' => 'Pricewage',
        ];
    }


    public function pricewage()
    {
        $sql = Yii::$app->db
            ->createCommand("SELECT * FROM ytax")
            ->queryAll();
            return $sql;
    }

}
