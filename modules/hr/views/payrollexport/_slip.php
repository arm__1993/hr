<?php

use yii\helpers\Html;
use app\api\DateTime;
use app\api\Helper;

$dateslip = explode("-", $date);
?>

<style>
    .container {
        font-family: "THSarabun";
        font-size: 20px;
    }

    p {
        font-family: "THSarabun";
        font-size: 18px;
    }

    h3 {
        font-family: "THSarabun";
        font-size: 30px;
        font-weight: bold;
    }

    h4 {
        font-family: "THSarabun";
        font-size: 40px;
        font-weight: bold;
    }

    h6 {
        font-family: "THSarabun";
        font-size: 20px;
        font-weight: bold;
    }

    h5 {
        font-family: "THSarabun";
        font-size: 30px;
        font-weight: bold;
    }

    B {
        font-family: "THSarabun";
        font-weight: bold;
    }

    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .rpt tr.odd {
        background: #f3f3f3;
    }

    .rpt tr.even {
        background: #FFF;
    }

    .triangle {

        border: 1.2px solid black; /* ใส่เส้นขอบแบบทึบสีดำขนาด 1px */
    }

    .noborder th {
        border: 0px;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        font-family: "THSarabun";
        font-size: 16px;
    }

    .noborder td {
        border: 0px;
        padding: 5px;
        font-family: "THSarabun";
        font-size: 16px;
    }

</style>
<div class="triangle">
    <table width="100%" height="100%" border="0" cellpadding="2" cellspacing="0">
        <tr>
            <td>
                <table width="100%" height="100%" border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td><h3>กลุ่มนกเงือก</h3></td>
                        <td style='text-align: right;'><h6>ใบแจ้งเงินเดีีอน</h6></td>
                    </tr>
                    <tr>
                        <td style='text-align: right;'></td>
                        <td style='text-align: right;'><h6>(pay slip)</h6></td>
                    </tr>
                </table>
                <table width="100%" height="100%" border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td><h6><?php echo $data[0]['wc_name']; ?></h6></td>
                    </tr>
                </table>
                <table width="100%" height="100%" border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="20%"><h6> ชื่อ-สกุล : <?php echo $data_emp[0]['full_name']; ?></h6></td>
                        <td width="20%"><h6>ตำแหน่ง :<?php echo $data[0]['WAGE_POSITION_NAME']; ?></h6></td>
                        <td width="20%"><h6>แผนก :<?php echo $data[0]['dm_name']; ?></h6></td>
                        <td width="20%"><h6>ประจำเดือน :
                                <B><?php echo DateTime::mappingMonth($dateslip[0]) . ' ' . $dateslip[1] ?></B></h6></td>
                    </tr>
                </table>
                <table width="100%" height="100%" border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="100%" style='text-align:center'><h6>รายการเพิ่ม</h6></td>
                        <td width="100%" style='text-align:center'><h6>รายการหัก</h6></td>
                        <td width="100%"></td>
                    </tr>
                    <tr>
                        <td width="100%" valign='top'>
                            <table class="rpt" cellpadding="5" style="border-collapse: collapse;" cellspacing="0"
                                   width="100%" align="center" style="text-align:center">
                                <tr bgcolor="#DCDCDC">
                                    <td width="20%">ลำดับ</td>
                                    <td width="30%">รายการ</td>
                                    <td width="30%">รายละเอียด</td>
                                    <td width="20%">จำนวน</td>
                                </tr>
                                <?php
                                $count = 1;
                                $sum = 0;
                                foreach ($ADDITION as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $value['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] ?></td>
                                        <td><?php echo '' ?></td>
                                        <td style='text-align: right;'
                                            s><?php echo Helper::displayDecimal($value['ADD_DEDUCT_THIS_MONTH_AMOUNT']); ?></td>
                                    </tr>
                                    <?php
                                    $sum += $value['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                                    $count++;
                                } ?>
                                <tr>

                                    <td AlIGN="center" colspan="3" style='text-align:center' colspan='3'>
                                        รวมรายการเพิ่ม
                                    </td>
                                    <td style='text-align: right;'><?php echo Helper::displayDecimal($sum); ?></td>
                                </tr>

                            </table>
                        </td>
                        <td width="100%" valign='top'>
                            <table class="rpt" cellpadding="5" style="border-collapse: collapse;" cellspacing="0"
                                   width="100%" align="center" style="text-align:center">
                                <tr bgcolor="#DCDCDC">
                                    <td width="20%">ลำดับ</td>
                                    <td width="30%">รายการ</td>
                                    <td width="30%">รายละเอียด</td>
                                    <td width="20%">จำนวน</td>
                                </tr>
                                <?php
                                $count = 1;
                                $sum = 0;
                                foreach ($DEDUCTION as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $value['ADD_DEDUCT_THIS_MONTH_TMP_NAME'] ?></td>
                                        <td><?php echo '' ?></td>
                                        <td style='text-align: right;'><?php echo Helper::displayDecimal($value['ADD_DEDUCT_THIS_MONTH_AMOUNT']); ?></td>
                                    </tr>
                                    <?php
                                    $sum += $value['ADD_DEDUCT_THIS_MONTH_AMOUNT'];
                                    $count++;
                                } ?>
                                <tr>
                                    <td style='text-align:center' colspan='3'>รวมรายการหัก</td>
                                    <td style='text-align: right;'><?php echo Helper::displayDecimal($sum); ?></td>
                                </tr>
                            </table>
                        </td>
                        <td width="100%" valign='top' height="250" style="padding: 2px 5px 2px 5px;">
                            <table class="rpt" cellpadding="5" style="border-collapse: collapse;" cellspacing="0"
                                   width="100%" style="text-align:left">
                                <tr>
                                    <td>
                                        <h5>เงินเดือน
                                            :<?php echo Helper::displayDecimal($data[0]['WAGE_SALARY']); ?></h5>
                                        <p>เงินสะสมเดือนนี้
                                            : <?php echo Helper::displayDecimal($BENEFIT_FUND[0]['BENEFIT_AMOUNT']); ?></p>
                                        <p>ผลรวมเงินสะสมทั้งหมด
                                            : <?php echo Helper::displayDecimal($BENEFIT_FUND[0]['BENEFIT_TOTAL_AMOUNT']); ?></p>
                                        <p>หมายเหตุ: <?php echo '-'; ?></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan='2' valign='top'>
                            <table width="100%" class="rpt" border="0" cellpadding="1" cellspacing="0">
                                <tr>
                                    <td width='33%' style="font-weight: bold;text-align: center;">
                                        รายได้สะสม <?php echo 'มกราคม'; ?>-ปัจจุบัน
                                    </td>
                                    <td width='33%' style="font-weight: bold;text-align: center;">
                                        ภาษีสะสม <?php echo 'มกราคม'; ?>-ปัจจุบัน
                                    </td>
                                    <td width='33%' style="font-weight: bold;text-align: center;">
                                        ประกันสังคมสะสม <?php echo 'มกราคม'; ?>-ปัจจุบัน
                                    </td>
                                </tr>
                                <tr>
                                    <td style='border:0.5px solid black; text-align:center'><?php echo Helper::displayDecimal($data_summary[0]['income_total_amount']) ?></td>
                                    <td style='border:0.5px solid black; text-align:center'><?php echo Helper::displayDecimal($data_summary[0]['tax_total_amount']) ?></td>
                                    <td style='border:0.5px solid black; text-align:center'><?php echo Helper::displayDecimal($data_summary[0]['sso_total_amount']) ?></td>
                                </tr>
                            </table>
                        </td>
                        <td>

                            <table class="rpt" cellpadding="5" style="border-collapse: collapse;" cellspacing="0"
                                   width="100%" style="text-align:center">
                                <tr>

                                    <td>
                                        <h3 style="text-align:left"> รายรับทั้งสิ้น </h3>
                                        <h4> <?php echo Helper::displayDecimal($data[0]['WAGE_NET_SALARY']) ?></h4>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;" colspan="2">
                            <table class="noborder" border="0" cellpadding="5" style="border-collapse: collapse;" cellspacing="0" width="100%" style="text-align:center">
                                <tr>
                                    <th>เงินเดือนเดิม</th>
                                    <th>ลักษณะการเปลี่ยนแปลง</th>
                                    <th>ตำแหน่ง</th>
                                    <th>เงินเดือนใหม่</th>
                                    <th>เปลี่ยนแปลง</th>
                                    <th>หมายเหตุ</th>
                                </tr>
                                <tr>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>

                </table>

            </td>
        </tr>
    </table>
</div>
