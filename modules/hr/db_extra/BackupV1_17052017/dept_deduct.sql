-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 21, 2017 at 03:30 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `dept_deduct`
--

CREATE TABLE `dept_deduct` (
  `id` int(11) UNSIGNED NOT NULL,
  `division_id` int(11) DEFAULT NULL COMMENT 'รหัสแผนก',
  `dept_id` int(11) DEFAULT NULL COMMENT 'รหัสแผนก',
  `company_id` int(11) DEFAULT NULL COMMENT 'รหัสบริษัท',
  `for_year` int(11) UNSIGNED DEFAULT NULL COMMENT 'รายการสำหรับปี',
  `for_month` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'รายการสำหรับเดือน',
  `subject` varchar(250) DEFAULT NULL COMMENT 'หัวข้อ',
  `description` text COMMENT 'รายละเอียด',
  `is_approved` tinyint(3) DEFAULT NULL COMMENT 'สถานะการ approved 0=not approved, 1= approved',
  `approved_datetime` datetime DEFAULT NULL COMMENT ' วันเวลาที่ approved',
  `approved_by` char(13) DEFAULT NULL COMMENT 'approved โดย',
  `dept_deduct_status` tinyint(3) DEFAULT NULL COMMENT '1= นำเสนอโดยหัวหน้าหน่วยงาน,2 = ฝ่ายบุคคลตรวจสอบตีกลับ, 3=ฝ่ายบุคคลอนุมัติ',
  `create_by` char(13) DEFAULT NULL COMMENT 'สร้างรายการโดย',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันเวลาที่สร้างรายการ',
  `update_by` char(13) DEFAULT NULL COMMENT 'อัพเดทโดย',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่อัพเดท'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dept_deduct`
--
ALTER TABLE `dept_deduct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dept_id` (`dept_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `for_year` (`for_year`),
  ADD KEY `for_month` (`for_month`),
  ADD KEY `create_by` (`create_by`),
  ADD KEY `division_id` (`division_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dept_deduct`
--
ALTER TABLE `dept_deduct`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
