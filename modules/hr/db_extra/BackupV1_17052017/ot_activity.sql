-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2017 at 04:34 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_activity`
--

CREATE TABLE `ot_activity` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Record ID, Primary Key, Auto Increment',
  `activity_name` varchar(250) DEFAULT NULL COMMENT 'ชื่อกิจกรรมโอที',
  `status_active` tinyint(3) DEFAULT NULL COMMENT 'สถานะ active=1, inactive =0, ',
  `createby_user` varchar(30) DEFAULT NULL COMMENT 'สร้างข้อมูลโดยผู้ใช้',
  `create_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาสร้างข้อมูล',
  `updateby_user` varchar(30) DEFAULT NULL COMMENT 'ปรับปรุงข้อมูลโดยผู้ใช้',
  `update_datetime` datetime DEFAULT NULL COMMENT 'วันที่เวลาที่ปรับปรุงข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ot_activity`
--

INSERT INTO `ot_activity` (`id`, `activity_name`, `status_active`, `createby_user`, `create_datetime`, `updateby_user`, `update_datetime`) VALUES
(1, 'Accounting', 1, NULL, '2016-12-29 22:07:06', NULL, NULL),
(2, 'Taxation', 1, NULL, '2016-12-29 22:07:50', NULL, NULL),
(3, 'Payroll', 1, NULL, '2016-12-29 22:08:22', NULL, NULL),
(4, 'Value Added Tax', 1, NULL, '2016-12-29 22:09:00', NULL, NULL),
(5, 'Payment', 1, NULL, '2016-12-29 22:09:15', NULL, NULL),
(6, 'Auditing', 1, 'admin', '2017-01-31 22:46:15', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_activity`
--
ALTER TABLE `ot_activity`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ot_activity`
--
ALTER TABLE `ot_activity`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Record ID, Primary Key, Auto Increment', AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
