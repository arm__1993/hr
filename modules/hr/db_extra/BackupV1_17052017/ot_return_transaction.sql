-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 03, 2017 at 09:15 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `ot_return_transaction`
--

CREATE TABLE `ot_return_transaction` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'รหัส',
  `id_card` char(13) NOT NULL COMMENT 'รหัสบัตรประชาชนพนักงาน',
  `total_amount` decimal(10,2) NOT NULL COMMENT 'จำนวนผลตอบแทนที่ได้รับ',
  `circle_payroll` varchar(20) NOT NULL COMMENT 'รอบเดือนปีที่จ่ายเงิน',
  `ot_requestmaster_id` int(11) NOT NULL COMMENT 'รหัสตารางการเสนอขอโอที',
  `ot_requestdetail_id` int(11) NOT NULL COMMENT 'รหัสรายละเอียดการขออนุมัติโอที',
  `return_id` int(11) NOT NULL COMMENT 'รหัสลำดับการขอค่าตอบแทน',
  `return_name` varchar(100) NOT NULL COMMENT 'ชื่อค่าตอบแทน',
  `transaction_datetime` datetime NOT NULL COMMENT 'วันที่ เวลา ที่เกิด transaction'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ot_return_transaction`
--
ALTER TABLE `ot_return_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_card` (`id_card`),
  ADD KEY `ot_requestmaster_id` (`ot_requestmaster_id`),
  ADD KEY `ot_requestdetail_id` (`ot_requestdetail_id`),
  ADD KEY `return_id` (`return_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ot_return_transaction`
--
ALTER TABLE `ot_return_transaction`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
