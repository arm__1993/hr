<?php

namespace app\modules\hr\controllers;

use app\modules\hr\apihr\ApiHr;
use app\api\Utility;
use app\api\Helper;

use yii\db\Transaction;
use yii\helpers\Json;
use yii\base\ErrorException;
use yii\db\Expression;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\api\DBConnect;
use app\modules\hr\controllers\MasterController;
use Yii;

class PayrollextraController extends MasterController
{

    public $layout = 'hrlayout';
    // public $idcardLogin;

    // /**
    //  * function init() check session active or session login, if not redirect to login page
    //  * @return \yii\web\Response
    //  */
    // public function init()
    // {
    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    /**
 * @return string
 */
    public function actionInputform()
    {
        return $this->render('inputform');
    }

    /**
     * @return string
     */
    public function actionShowdeduction()
    {
        return $this->render('showdeduction');
    }


    /**
     * @return string
     */
    public function actionMakededuction()
    {
        return $this->render('makededuction');
    }


    /**
     * @return string
     */
    public function actionConfirmdeduction()
    {
        return $this->render('confirmdeduction');
    }


    /**
     * @return string
     */
    public function actionExportfile()
    {
        return $this->render('exportfile');
    }

    /**
     * @return string
     */
    public function actionReportdeduction()
    {
        return $this->render('reportdeduction');
    }


}
