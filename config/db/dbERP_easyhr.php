<?php

return [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host='.$DB_HOST.';dbname=ERP_easyhr', //maybe other dbms such as psql,...
        'username' => $DB_USER,
        'password' => $DB_PWD,
		'charset' => 'utf8',
		];