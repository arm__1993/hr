<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[TaxConfigDeducttion]].
 *
 * @see TaxConfigDeducttion
 */
class TaxConfigDeducttionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaxConfigDeducttion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaxConfigDeducttion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
