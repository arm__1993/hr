<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\modules\hr\apihr\ApiHr;
use app\api\Helper;
use app\api\Utility;
use app\api\DateTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;



$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/org_configmaster.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/sso_incomestructure.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //sso
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/benefit_incomestructure.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //benefit

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css


?>
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li>ตั้งค่าข้อมูลองค์กร</li>
                <li class="active">ตั้งค่าทั่วไป</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#orgconfigmaster" data-toggle="tab">ตั้งค่าข้อมูลกลาง</a></li>
                    <li><a href="#org1" data-toggle="tab">ตั้งค่าข้อมูลอัตราเงินประกันสังคม</a></li>
                    <li><a href="#org2" data-toggle="tab">ตั้งค่าข้อมูลอัตราเงินสะสม</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="orgconfigmaster">
                        <?php $form = ActiveForm::begin([
                            'id' => 'frmOrgConfigMaster',
                            'layout' => 'horizontal',
                            'options' => ['class' => 'form-verticle'],
                            'fieldConfig' => [
                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                'horizontalCssClasses' => [
                                    'label' => 'col-sm-6',
                                    'offset' => 'col-sm-offset-4',
                                    'wrapper' => 'col-sm-6',
                                    'error' => '',
                                    'hint' => '',
                                ],
                            ],
                            //'action' => 'saveotconfig',
                            //'enableAjaxValidation' => true,
                            //'validationUrl' => 'validateotconfig',
                        ]);
                        ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php echo $form->field($OrgConfigMaster, 'work_day_inmonth')->textInput(['maxlength' => 5]) ?>
                                <?php echo $form->field($OrgConfigMaster, 'work_hour_inday')->textInput(['maxlength' => 5]) ?>
                                <?php

                                echo $form->field($OrgConfigMaster, 'work_start_hour_inday')->textInput(['maxlength' => 8]);
                                echo $form->field($OrgConfigMaster, 'work_end_hour_inday')->textInput(['maxlength' => 8]);

                                /*echo $form->field($OrgConfigMaster, 'work_start_hour_inday')->widget(\yii\widgets\MaskedInput::className(), [
                                    'mask' => '99:99:99',
                                ]);*/
                                /*echo $form->field($OrgConfigMaster, 'work_end_hour_inday')->widget(\yii\widgets\MaskedInput::className(), [
                                    'mask' => '99:99:99',
                                ]);*/
                                //echo $form->field($OrgConfigMaster, 'work_start_hour_inday')->dropDownList($listData, [ 'prompt' => 'กรุณาเลือก']);
                                ?>
                                <?php echo $form->field($OrgConfigMaster, 'sso_rate')->textInput(['maxlength' => 4]) ?>
                                <?php echo $form->field($OrgConfigMaster, 'saving_rate')->textInput(['maxlength' => 4]) ?>
                                <?php echo $form->field($OrgConfigMaster, 'day_start_salary')->textInput(['maxlength' => 4]) ?>
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <?php

                                    echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'name' => 'btnSaveOrgMaster', 'id' => 'btnSaveOrgMaster'])
                                    /*AjaxSubmitButton::begin([
                                        'label' => 'Add',
                                        'useWithActiveForm' => 'configtime-table',
                                        'ajaxOptions' => [
                                            'type' => 'POST',
                                            'success' => new \yii\web\JsExpression("function(data) {
                                            console.log(data);
                                               // if (data.status == true)
                                                //{
                                                   // closeTopbar();

                                                //}
                                            }"),
                                        ],
                                        'options' => ['class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'add-button'],
                                    ]);
                                    AjaxSubmitButton::end();*/

                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="tab-pane" id="org1">
                        <div class="row">
                            <div class="pull-right" style="padding-right: 15px;">
                                <?php
                                Modal::begin([
                                    'id' => 'modalfrmSsoStructure',
                                    'header' => '<strong>ฟอร์มบันทึกข้อมูลโครงสร้างการคิดเงินประกันสังคม</strong>',
                                    'toggleButton' => [
                                        'id' => 'btnAddNewSsoStructure',
                                        'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มโครงสร้างการคิดเงินประกันสังคม ',
                                        'class' => 'btn btn-success'
                                    ],
                                    'closeButton' => [
                                        'label' => '<i class="fa fa-close"></i>',
                                        //'class' => 'close pull-right',
                                        'class' => 'btn btn-success btn-sm pull-right'
                                    ],
                                    'size' => 'modal-lg'
                                ]);
                                ?>
                                <form role="form" id="frmSsoStructure">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="box-body">
                                                    <label>ช่วงเงินได้ เริ่มต้น <span>*</span></label>
                                                    <input id="income_floor" name="income_floor" data-required="true"
                                                           class="form-control" type="text" placeholder="ช่วงเงินได้ เริ่มต้น" maxlength="8"  data-inputmask="'alias': 'decimal'" data-mask>
                                                </div>

                                                <div class="box-body">
                                                    <label>เริ่มใช้ตั้งแต่ปี <span>*</span></label>
                                                    <input id="from_year" name="from_year" data-required="true"
                                                           class="form-control" type="text" placeholder="เริ่มใช้ตั้งแต่ปี"  maxlength="8" data-inputmask='"mask": "9999"' data-mask>
                                                </div>
                                                <div class="box-body">
                                                    <label>อัตราเงินประกันสังคม <span>*</span></label>
                                                    <input id="sso_rate" name="sso_rate" data-required="true"
                                                           class="form-control" type="text" placeholder="อัตราเงินประกันสังคม" maxlength="4"  data-inputmask="'alias': 'decimal'" data-mask>
                                                </div>
                                                <!-- /.box-body -->
                                                <div class="box-body">
                                                    <label>สถานะ</label>
                                                    <select class="form-control" id="ssoincome_status" name="ssoincome_status">
                                                        <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                            Active
                                                        </option>
                                                        <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                            In Active
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="box-body">
                                                    <label>ช่วงเงินได้ สิ้นสุด <span>*</span></label>
                                                    <input id="income_ceil" name="income_ceil" data-required="true"
                                                           class="form-control" type="text" placeholder="ช่วงเงินได้ สิ้นสุด" maxlength="8" data-inputmask="'alias': 'decimal'" data-mask>
                                                </div>
                                                <div class="box-body">
                                                    <label>จนถึงปี <span>ไม่ใส่หมายถึงตลอดไป</span></label>
                                                    <input id="to_year" name="to_year"
                                                           class="form-control" type="text" placeholder="จนถึงปี" maxlength="4" data-inputmask='"mask": "9999"' data-mask>
                                                </div>
                                                <div class="box-body">
                                                    <label>การคิดคำนวณ (%/บาท)</label>
                                                    <select class="form-control" id="is_calculate_percent" name="is_calculate_percent">
                                                        <option value="<?php echo Yii::$app->params['PAYROLL']['PAY_SSO_PERC']; ?>">
                                                            <?php echo Yii::$app->params['PAYROLL']['PAY_SSO_PERC_LABEL']; ?>
                                                        </option>
                                                        <option value="<?php echo Yii::$app->params['PAYROLL']['PAY_SSO_BAHT'];  ?>">
                                                            <?php echo Yii::$app->params['PAYROLL']['PAY_SSO_BAHT_LABEL']; ?>
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="box-body">
                                                    <label>หมายเหตุ <span>*</span></label>
                                                    <input id="remark" name="remark"
                                                           class="form-control" type="text" placeholder="หมายเหตุ">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="box-body">
                                            <?php echo Html::hiddenInput('hide_ssostructureedit', null, ['id' => 'hide_ssostructureedit']); ?>
                                            <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveSSOincomeStructure']); ?>
                                        </div>
                                    </div>
                                </form>
                                <?php
                                Modal::end();
                                ?>
                            </div>
                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <br/>
                                    <?php

                                    Pjax::begin(['id' => 'pjax_ssostructure']);
                                    echo GridView::widget([
                                        'id' => 'gridssostructure',
                                        'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                        'dataProvider' => $SsoIncomeProvider,
                                       // 'filterModel' => $SsoIncomeStructure,
                                        //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            //'id',
                                            [
                                                'attribute' => 'income_floor',
                                                'label' => 'ช่วงเงินได้ เริ่มต้น',
                                                'value' => function($data) {
                                                    return Helper::displayDecimal($data->income_floor);
                                                },
                                                'contentOptions' => ['style' => 'max-width: 60px;text-align:right;']
                                            ],
                                            [
                                                'attribute' => 'income_ceil',
                                                'label' => 'ช่วงเงินได้ สิ้นสุด',
                                                'value' => function($data) {
                                                    return Helper::displayDecimal($data->income_ceil);
                                                },
                                                'contentOptions' => ['style' => 'max-width: 60px;text-align:right;']
                                            ],
                                            [
                                                'attribute' => 'sso_rate',
                                                'label' => 'อัตราเงินประกันสังคม',
                                                'value' => 'sso_rate',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;text-align:right;']
                                            ],
                                            [
                                                'attribute' => 'is_calculate_percent',
                                                'label' => 'การคิดคำนวณ',
                                                //'value' => 'is_calculate_percent',
                                                'format' => 'raw',
                                                'filter' => false,
                                                'value' => function ($data) {
                                                    return ($data->is_calculate_percent==Yii::$app->params['PAYROLL']['PAY_SSO_PERC']) ?  Yii::$app->params['PAYROLL']['PAY_SSO_PERC_LABEL'] : Yii::$app->params['PAYROLL']['PAY_SSO_BAHT_LABEL'];
                                                },
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],
                                            [
                                                'attribute' => 'from_year',
                                                'label' => 'เริ่มใช้ตั้งแต่ปี',
                                                'value' => 'from_year',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],
                                            [
                                                'attribute' => 'to_year',
                                                'label' => 'จนถึงปี',
                                                'value' => 'to_year',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],

                                            [
                                                'attribute' => 'statuc_active',
                                                'label' => 'สถานะ',
                                                'format' => 'image',
                                                'value' => function ($data) {
                                                    return Utility::dispActive($data->status_active);
                                                },
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 50px;text-align:center']
                                            ],

                                            [
                                                'attribute' => 'createby_user',
                                                'value' => 'createby_user',
                                                'label' => 'บันทึกโดยผู้ใช้',
                                                //'format' => 'raw',
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'attribute' => 'create_datetime',
                                                'label' => 'บันทึกเมื่อวันที่เวลา',
                                                'value' => function ($data) {
                                                    return DateTime::ThaiDateTime($data->create_datetime);
                                                },
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'headerOptions' => ['width' => '100'],
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{update}  &nbsp; {delete}',
                                                'buttons' => [
                                                    'update' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                            'title' => 'แก้ไข',
                                                            'onclick' => '(function($event) {
                                                                    editSSOincomeStructure(' . $data->id . ');
                                                            })();'
                                                        ]);
                                                    },

                                                    'delete' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                            'title' => 'ลบ',
                                                            'onclick' => '(function($event) {
                                                                      bootbox.confirm({
                                                                        size: "small",
                                                                        message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการนี้? </h4>",
                                                                        callback: function(result){
                                                                            if(result==1) {
                                                                                deleteSSOincomeStructure(' . $data->id . ');
                                                                            }
                                                                        }
                                                                      });

                                                                })();'
                                                        ]);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]);
                                    Pjax::end();  //end pjax_gridcorclub

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="org2">
                        <div class="row">
                            <div class="pull-right" style="padding-right: 15px;">
                                <?php
                                Modal::begin([
                                    'id' => 'modalfrmBenefitStructure',
                                    'header' => '<strong>ฟอร์มบันทึกข้อมูลโครงสร้างการคิดเงินสะสม</strong>',
                                    'toggleButton' => [
                                        'id' => 'btnAddNewBenefitStructure',
                                        'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มโครงสร้างการคิดเงินสะสม ',
                                        'class' => 'btn btn-success'
                                    ],
                                    'closeButton' => [
                                        'label' => '<i class="fa fa-close"></i>',
                                        //'class' => 'close pull-right',
                                        'class' => 'btn btn-success btn-sm pull-right'
                                    ],
                                    'size' => 'modal-lg'
                                ]);
                                ?>
                                <form role="form" id="frmBenefitStructure">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="box-body">
                                                    <label>อายุงานเริ่มต้น (ปี) <span>*</span></label>
                                                    <input id="workage_floor" name="workage_floor" data-required="true"
                                                           class="form-control" type="text" placeholder="อายุงานเริ่มต้น (ปี)" maxlength="8"  data-inputmask="'alias': 'decimal'" data-mask>
                                                </div>

                                                <div class="box-body">
                                                    <label>เริ่มใช้ตั้งแต่ปี <span>*</span></label>
                                                    <input id="be_from_year" name="be_from_year" data-required="true"
                                                           class="form-control" type="text" placeholder="เริ่มใช้ตั้งแต่ปี"  maxlength="8" data-inputmask='"mask": "9999"' data-mask>
                                                </div>
                                                <div class="box-body">
                                                    <label>อัตราเงินสะสมได้รับ <span>*</span></label>
                                                    <input id="benefit_rate" name="benefit_rate" data-required="true"
                                                           class="form-control" type="text" placeholder="อัตราเงินสะสมได้รับ" maxlength="4"  data-inputmask="'alias': 'decimal'" data-mask>
                                                </div>
                                                <!-- /.box-body -->
                                                <div class="box-body">
                                                    <label>สถานะ</label>
                                                    <select class="form-control" id="benefitincome_status" name="benefitincome_status">
                                                        <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                            Active
                                                        </option>
                                                        <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                            In Active
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="box-body">
                                                    <label>อายุงานสิ้นสุด (ปี) <span>*</span></label>
                                                    <input id="workage_ceil" name="workage_ceil" data-required="true"
                                                           class="form-control" type="text" placeholder="อายุงานสิ้นสุด" maxlength="8" data-inputmask="'alias': 'decimal'" data-mask>
                                                </div>
                                                <div class="box-body">
                                                    <label>จนถึงปี <span>ไม่ใส่หมายถึงตลอดไป</span></label>
                                                    <input id="be_to_year" name="be_to_year"
                                                           class="form-control" type="text" placeholder="จนถึงปี" maxlength="4" data-inputmask='"mask": "9999"' data-mask>
                                                </div>

                                                <div class="box-body">
                                                    <label>หมายเหตุ <span>*</span></label>
                                                    <input id="be_remark" name="be_remark"
                                                           class="form-control" type="text" placeholder="หมายเหตุ">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="box-body">
                                            <?php echo Html::hiddenInput('hide_benefitstructureedit', null, ['id' => 'hide_benefitstructureedit']); ?>
                                            <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-primary', 'id' => 'btnSaveBenefitincomeStructure']); ?>
                                        </div>
                                    </div>
                                </form>
                                <?php
                                Modal::end();
                                ?>
                            </div>
                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <br/>
                                    <?php

                                    Pjax::begin(['id' => 'pjax_benefitstructure']);
                                    echo GridView::widget([
                                        'id' => 'gridbenefitstructure',
                                        'summary' => "<div class='text-right'>แสดง <strong> {begin} - {end} </strong>  จากทั้งหมด <strong> {totalCount}</strong>  รายการ จำนวน <strong> {pageCount}</strong>  หน้า</div>",
                                        'dataProvider' => $BenefitIncomeProvider,
                                        'filterModel' => $BenefitIncomeStructure,
                                        //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            //'id',
                                            [
                                                'attribute' => 'workage_floor',
                                                'label' => 'อายุงานเริ่มต้น',
                                                'value' => 'workage_floor',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],
                                            [
                                                'attribute' => 'workage_ceil',
                                                'label' => 'อายุงานสิ้นสุด',
                                                'value' => 'workage_ceil',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],
                                            [
                                                'attribute' => 'benefit_rate',
                                                'label' => 'อัตราเงินสะสมได้รับ',
                                                'value' => 'benefit_rate',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],
                                            [
                                                'attribute' => 'from_year',
                                                'label' => 'เริ่มใช้ตั้งแต่ปี',
                                                'value' => 'from_year',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],
                                            [
                                                'attribute' => 'to_year',
                                                'label' => 'จนถึงปี',
                                                'value' => 'to_year',
                                                //'format' => 'raw',
                                                'filter' => true,
                                                'contentOptions' => ['style' => 'max-width: 60px;']
                                            ],

                                            [
                                                'attribute' => 'statuc_active',
                                                'label' => 'สถานะ',
                                                'format' => 'image',
                                                'value' => function ($data) {
                                                    return Utility::dispActive($data->status_active);
                                                },
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 50px;text-align:center']
                                            ],

                                            [
                                                'attribute' => 'createby_user',
                                                'value' => 'createby_user',
                                                'label' => 'บันทึกโดยผู้ใช้',
                                                //'format' => 'raw',
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'attribute' => 'create_datetime',
                                                'label' => 'บันทึกเมื่อวันที่เวลา',
                                                'value' => function ($data) {
                                                    return DateTime::ThaiDateTime($data->create_datetime);
                                                },
                                                'filter' => false,
                                                'contentOptions' => ['style' => 'width: 150px;']
                                            ],
                                            [
                                                'headerOptions' => ['width' => '100'],
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{update}  &nbsp; {delete}',
                                                'buttons' => [
                                                    'update' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                            'title' => 'แก้ไข',
                                                            'onclick' => '(function($event) {
                                                                    editBenefitincomeStructure(' . $data->id . ');
                                                            })();'
                                                        ]);
                                                    },

                                                    'delete' => function ($url, $data) {
                                                        return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                            'title' => 'ลบ',
                                                            'onclick' => '(function($event) {
                                                                      bootbox.confirm({
                                                                        size: "small",
                                                                        message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการนี้? </h4>",
                                                                        callback: function(result){
                                                                            if(result==1) {
                                                                                deleteBenefitincomeStructure(' . $data->id . ');
                                                                            }
                                                                        }
                                                                      });

                                                                })();'
                                                        ]);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]);
                                    Pjax::end();  //end pjax_gridcorclub

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->