<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 17/10/2559
 * Time: 11:03
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;

use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;

$objPHPExcel = new \PHPExcel();
$date_rpt = date('Ymd');

$session = Yii::$app->session;

$customers = $session->get('customers');
$totalVIN = $session->get('totalVIN');
$year_rageEnd = $session->get('year_rageEnd');
$year_rageStart = $session->get('year_rageStart');
$technician_car = $session->get('technician_car');

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('AngsanaUPC')
    ->setSize(16);

// Add some data  วิว


$objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
$objWorkSheet->setCellValue('A1', 'ชื่อ - นามสกุล');
$objWorkSheet->setCellValue('B1', ' เบอร์โทร');
$objWorkSheet->setCellValue('C1', 'ที่อยู่');
$objWorkSheet->setCellValue('D1', 'รุ่นรถ');
$objWorkSheet->setCellValue('E1', 'เลขเครื่อง');
$objWorkSheet->setCellValue('F1', 'แซสซิส');
$objWorkSheet->setCellValue('G1', 'ทะเบียน');
$objWorkSheet->setCellValue('H1', 'วันที่เข้าสุดท้าย');
$objWorkSheet->setCellValue('I1', 'เลขกิโลเมตรล่าสุด');
//$objWorkSheet->setCellValue('J1', 'เลขกิโลเมตรล่าสุด');

$_srow = 2; // เริ่มใส่ข้อมูลบรรทัดที่ 2
$i = 0;
// $value = count($customers);
// //foreach($result as $row) {
// for($_srow=2;$_srow<=$value
// ;$_srow++)
foreach ($customers as $key => $value) 
{
    $objWorkSheet->setCellValue('A' . $_srow, ($value['name']));
    $objWorkSheet->setCellValue('B' . $_srow, ($value['tel']));
    $objWorkSheet->setCellValue('C' . $_srow, ($value['address']));
    $objWorkSheet->setCellValue('D' . $_srow, ($value['model_name']));
    $objWorkSheet->setCellValue('E' . $_srow, ($value['engin_id']));
    $objWorkSheet->setCellValue('F' . $_srow, ($value['chassis']));
    $objWorkSheet->setCellValue('G' . $_srow, ($value['register']));
    $objWorkSheet->setCellValue('H' . $_srow, ($value['date']));
    $objWorkSheet->setCellValue('I' . $_srow, ($value['mile']));
    //$objWorkSheet->setCellValue('J' . $_srow, ($customers[$i]['amount_car']));
    $i++;
    $_srow++;
}

//วิา
/*// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A4', 'Miscellaneous glyphs')
    ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');*/



// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('รายงานการเข้ารับบริการของลูกค้า');//ชื่อในไฟล
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel2007)
// foreach(range('A','B') as $columnID) {
//     $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
//         ->setAutoSize(true);
// }
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="รายงานรอบการเข้ารับบริการของลูกค้า'.$date_rpt.'.xlsx"');//=ชื่อไฟล์
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
<?php
session_start();
session_destroy();
?>