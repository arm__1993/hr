<?php
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;
use app\modules\gitreport\apigitreport\ApiGitReport;
use app\modules\gitreport\apigitreport\Issue;
use app\api\DateTime;

//print_r( Yii::$app->params['payroll']);

$img = Yii::$app->request->BaseUrl . '/images/global/';


//simulate data



/*
$arrProject = [
    '1' => 'ERP EasyService',
    '2' => 'ERP EasySale',
    '3' => 'ERP EasyAccount',
    '4' => 'ERP EasyCrm',
    '5' => 'ERP EasyHr',
    '6' => 'ERP EasyLoan',
];
*/


//TODO :: เพิ่ม status Testing
//TODO :: เพิ่มมุมมอง ไตรมาส และ ปี
//TODO :: เพิ่มมุมมอง ราย project
//เพิ่ม status testing





//getAll Project
$arrAllProject = [];
foreach($allProject as $project) {
    $arrAllProject[$project->id] = $project->project_name;
}


//all milestone
$arrAllMileStone = [];
foreach($allMileStone as $milestone) {
    $arrAllMileStone[$milestone->id] = $milestone->milestone_name;
}


$date_range = '01/02/2017 - 28/02/2017';
$last_update = date('d/m/Y H:i:s');


//all issue status
$arrIssueStatus = [];
foreach($allStatus as $status) {
    $arrIssueStatus[$status->id] = $status->status_name;
}

//all dev
$arrAllDev = [];
foreach($allDev as $dev) {
    $arrAllDev[$dev->id] = $dev->dev_nickname;
}

//get all issue
$arrAllIssue = $arrTotalIssue = $arrIssueInMilestone = $arrIssueInProject = $arrIssueInDev = $arrDevInProject = [];
foreach($allIssue as $issue) {
    $arrAllIssue[$issue->project_id][$issue->milestone_id][$issue->dev_id][$issue->status_id] = $issue->issue_total;
    $arrTotalIssue[$issue->status_id] += $issue->issue_total;
    $arrIssueInMilestone[$issue->milestone_id][$issue->status_id] += $issue->issue_total;
    $arrIssueInProject[$issue->project_id][$issue->status_id] += $issue->issue_total;
    $arrIssueInDev[$issue->dev_id][$issue->status_id] += $issue->issue_total;
    $arrDevInProject[$issue->dev_id][$issue->project_id] += 1;
}


$_totalIssue = array_sum($arrTotalIssue);
$_totalIssue_noassign       = ($arrTotalIssue[1]) ? $arrTotalIssue[1] : 0;
$_totalIssue_backlog        = ($arrTotalIssue[2]) ? $arrTotalIssue[2] : 0;
$_totalIssue_inprogress     = ($arrTotalIssue[3]) ? $arrTotalIssue[3] : 0;
$_totalIssue_testing        = ($arrTotalIssue[4]) ? $arrTotalIssue[4] : 0;
$_totalIssue_uat            = ($arrTotalIssue[5]) ? $arrTotalIssue[5] : 0;
$_totalIssue_ready          = ($arrTotalIssue[6]) ? $arrTotalIssue[6] : 0;
$_totalIssue_installed      = ($arrTotalIssue[7]) ? $arrTotalIssue[7] : 0;


//$_total_dev_issue[1]
/*echo '<pre>';
print_r( $arrIssueInDev[3]);
echo '</pre>';*/

/*
1	Noassign/Backlog
2	Inprogress
3	Testing
4	UAT
5	Ready
6	Installed


1	Kritsada Wongnunta	kenz
2	Adithep Chaisan	aeedy
3	Bunyut Punyagon	golf
4	Nattawut Jumpar	kwan
5	Pacharapol Kanuei	em

*/



//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/gitreport/gitreport.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/gitreport/protovis-r3.2.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/gitreport/bullet.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$script = <<< CSS
 body {
  margin: 0;
  display: table;
  height: 100%;
  width: 100%;
  font: 14px/134% Helvetica Neue, sans-serif;
}

#center {
  display: table-cell;
  vertical-align: middle;
}

#fig {
  position: relative;
  margin: auto;
}
CSS;
$this->registerCss($script);




$total_day = date('t');
$today = date('d');

/** ======================================== */
//Golf 3
$golf_total = (count($arrIssueInDev[3]) > 0) ? array_sum($arrIssueInDev[3]) : 0;
$golf_complete = (count($arrIssueInDev[3]) > 0) ? $arrIssueInDev[3][5]+$arrIssueInDev[3][6]+ $arrIssueInDev[3][7] : 0;
$golf_imcomplete = $golf_total - $golf_complete;
$golf_in_project = (count($arrDevInProject[3]) > 0) ? count($arrDevInProject[3]) : 0;
$_performance = ($total_day > 0)  ? ($today * 28) / $total_day : 0;


//kwan 4
$kwan_total = (count($arrIssueInDev[4]) > 0) ? array_sum($arrIssueInDev[4]) : 0;
$kwan_complete = (count($arrIssueInDev[4]) > 0) ? $arrIssueInDev[4][5]+$arrIssueInDev[4][6]+ $arrIssueInDev[4][7] : 0;
$kwan_imcomplete = $kwan_total - $kwan_complete;
$kwan_in_project = (count($arrDevInProject[4]) > 0) ? count($arrDevInProject[4]) : 0;


//em 5
$em_total = (count($arrIssueInDev[5]) > 0) ? array_sum($arrIssueInDev[5]) : 0;
$em_complete = (count($arrIssueInDev[5]) > 0) ? $arrIssueInDev[5][5]+$arrIssueInDev[5][6]+ $arrIssueInDev[5][7] : 0;
$em_imcomplete = $em_total - $em_complete;
$em_in_project = (count($arrDevInProject[5]) > 0) ? count($arrDevInProject[5]) : 0;


/*echo '<pre>';
print_r( $arrIssueInDev[5]);
echo '</pre>';*/

?>
<section class="content">
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Dashboard (UPDATE : <?php echo $last_update; ?>)</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo count($arrAllProject); ?></h3>
                            <p> Project ทั้งหมด</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <?php $date_range; ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?php echo $_totalIssue; ?></h3>
                            <p>Issue ทั้งหมด</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <?php $date_range; ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-fuchsia">
                        <div class="inner">
                            <h3><?php echo $_totalIssue_testing; ?></h3>
                            <p> Testing Issue</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <?php $date_range; ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo $_totalIssue_uat; ?></h3>
                            <p> UAT Issue</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <?php $date_range; ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php echo $_totalIssue_installed; ?></h3>
                            <p>Installed Issue</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <?php $date_range; ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3><?php echo $_totalIssue_noassign; ?></h3>
                            <p>Issue No Assign</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <?php $date_range; ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>


                <div class="col-lg-1 col-xs-6">
                </div>
                <!-- ./col -->
            </div>
        </div>

    </div>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Issue/Status</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-7">
                    <?php

                    echo Highcharts::widget([
                        'scripts' => [
                            //'modules/exporting',
                            'themes/sand-signika',
                        ],
                        'options' => [
                            'credits' => ['enabled' => false],
                            'chart' => ['type' => 'pie',
                                'options3d' => ['enabled' => true,
                                    'alpha' => 55, //adjust for tilt
                                    'beta' => 0, // adjust for turn
                                ]
                            ],
                            'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                                'pie' => [
                                    'allowPointSelect' => true,
                                    'cursor' => 'pointer',
                                    'innerSize' => 100,
                                    'depth' => 45
                                ]
                            ],
                            'title' => ['text' => 'Issue/Status'],
                            'series' => [[// mind the [[ instead of [
                                'type' => 'pie',
                                'name' => 'Type Issue',
                                'data' => [
                                    [
                                        'name' => 'No Assign',
                                        'y' => $_totalIssue_noassign,
                                        'color' => '#dd4b39'
                                    ],
                                    [
                                        'name' => 'Back Log',
                                        'y' => $_totalIssue_backlog,
                                        'color' => '#605ca8'
                                    ],
                                    [
                                        'name' => 'In Progreess',
                                        'y' => $_totalIssue_inprogress,
                                        'color' => '#0073b7'
                                    ],
                                    [
                                        'name' => 'Testing',
                                        'y' => $_totalIssue_testing,
                                        'color' => '#d81b60'
                                    ],

                                    [
                                        'name' => 'UAT',
                                        'y' => $_totalIssue_uat,
                                        'color' => '#00c0ef'
                                    ],
                                    [
                                        'name' => 'Ready',
                                        'y' => $_totalIssue_ready,
                                        'color' => '#f012be'
                                    ],
                                    [
                                        'name' => 'Installed',
                                        'y' => $_totalIssue_installed,
                                        'color' => '#00a65a'
                                    ],
                                    /*
                                ['In Progress', 30],
                                ['Ready', 30],
                                ['UAT', 10],
                                ['Installed', 30],
                                    */
                                ],
                            ]], //mind the ]] instead of ]
                        ]
                    ]);


                    ?>
                </div>
                <div class="col-sm-5">
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-aqua">
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Issue / Status</h3>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><a href="#">No Assign <span
                                                class="pull-right badge bg-black"><?php echo $_totalIssue_noassign; ?></span></a>
                                </li>
                                <li><a href="#">Back Log <span
                                                class="pull-right badge bg-purple"><?php echo $_totalIssue_backlog; ?></span></a>
                                </li>
                                <li><a href="#">In Progress <span
                                                class="pull-right badge bg-blue"><?php echo $_totalIssue_inprogress; ?></span></a>
                                </li>
                                <li><a href="#">Testing <span
                                                class="pull-right badge bg-maroon"><?php echo $_totalIssue_testing; ?></span></a>
                                </li>
                                <li><a href="#">Ready <span
                                                class="pull-right badge bg-fuchsia"><?php echo $_totalIssue_ready; ?></span></a>
                                </li>
                                <li><a href="#">UAT <span
                                                class="pull-right badge bg-aqua"><?php echo $_totalIssue_uat; ?></span></a>
                                </li>
                                <li><a href="#">Installed <span
                                                class="pull-right badge bg-green"><?php echo $_totalIssue_installed; ?></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.box -->

    <div class="row">
        <?php

        foreach($allProject as $project)
        {
            $percent_project = ApiGitReport::calculatePercentProject($arrIssueInProject[$project->id]);

        ?>
        <div class="col-sm-4">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $project->project_name;?> </h3>
                    <span class="pull-right-container">
                         <small class="label pull-right bg-yellow"> Total <?php echo $percent_project;?>%</small>
                    </span>
                </div>
                <div class="box-body">
                    <?php
                    foreach ($allMileStone as $milestone)
                    {
                        if($milestone->project_id == $project->id)
                        {
                            $percent_milestone = ApiGitReport::calculatePercentMileStone($arrIssueInMilestone[$milestone->id]);
                        ?>
                        <p><code><?php echo $milestone->milestone_name;?></code> <small><?php echo DateTime::CalendarDate($milestone->milestone_start_date). ' - '.DateTime::CalendarDate($milestone->milestone_end_date); ?></small></p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                 aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent_milestone;?>%">
                                <span class="sr-only"><?php echo $percent_milestone;?>% Complete (success)</span>
                            </div>
                        </div>
                        <?php
                       }
                    }
                    ?>
                </div>

            </div>
        </div>
        <?php } ?>

    </div>


    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"> รายคน</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-green">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?php echo $img; ?>user6-128x128.jpg" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Bunyut Punyagon (Golf)</h3>
                            <h5 class="widget-user-desc">Developer</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><a href="#">Projects <span class="pull-right badge bg-blue"><?php echo $golf_in_project;?></span></a></li>
                                <li><a href="#">Issue <span
                                                class="pull-right badge bg-aqua"><?php echo $golf_total; ?></span></a>
                                </li>
                                <li><a href="#">Completed Issue <span
                                                class="pull-right badge bg-green"><?php echo $golf_complete; ?></span></a>
                                </li>
                                <li><a href="#">Incomplete <span
                                                class="pull-right badge bg-red"><?php echo $golf_imcomplete; ?></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>

                <div class="col-md-4">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-yellow">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?php echo $img; ?>user7-128x128.jpg" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Nattawut Jumpar (Kwan)</h3>
                            <h5 class="widget-user-desc">Developer</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><a href="#">Projects <span class="pull-right badge bg-blue"><?php echo $kwan_in_project;?></span></a></li>
                                <li><a href="#">Issue <span
                                                class="pull-right badge bg-aqua"><?php echo $kwan_total; ?></span></a>
                                </li>
                                <li><a href="#">Completed Issue <span
                                                class="pull-right badge bg-green"><?php echo $kwan_complete; ?></span></a>
                                </li>
                                <li><a href="#">Incomplete <span
                                                class="pull-right badge bg-red"><?php echo $kwan_imcomplete; ?></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>

                <div class="col-md-4">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-blue">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?php echo $img; ?>user8-128x128.jpg" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pacharapol Kanuei (Em)</h3>
                            <h5 class="widget-user-desc">Developer</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><a href="#">Projects <span class="pull-right badge bg-blue"><?php echo $em_in_project;?></span></a></li>
                                <li><a href="#">Issue <span
                                                class="pull-right badge bg-aqua"><?php echo $em_total; ?></span></a>
                                </li>
                                <li><a href="#">Completed Issue <span
                                                class="pull-right badge bg-green"><?php echo $em_complete; ?></span></a>
                                </li>
                                <li><a href="#">Incomplete <span
                                                class="pull-right badge bg-red"><?php echo $em_imcomplete; ?></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>

            </div>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"> ประสิทธิภาพรายคน</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <div id="center">
                        <div id="fig">
                            <script type="text/javascript+protovis">

                                var format = pv.Format.number();

                                var vis = new pv.Panel()
                                    .data(bullets)
                                    .width(400)
                                    .height(30)
                                    .margin(20)
                                    .left(100);

                                var bullet = vis.add(pv.Layout.Bullet)
                                    .orient("left")
                                    .ranges(function(d) d.ranges)
                                    .measures(function(d) d.measures)
                                    .markers(function(d) d.markers);

                                bullet.range.add(pv.Bar);
                                bullet.measure.add(pv.Bar);

                                bullet.marker.add(pv.Dot)
                                    .shape("triangle")
                                    .fillStyle("white");

                                bullet.tick.add(pv.Rule)
                                  .anchor("bottom").add(pv.Label)
                                    .text(bullet.x.tickFormat);

                                bullet.anchor("left").add(pv.Label)
                                    .font("bold 12px sans-serif")
                                    .textAlign("right")
                                    .textBaseline("bottom")
                                    .text(function(d) d.title);

                                bullet.anchor("left").add(pv.Label)
                                    .textStyle("#666")
                                    .textAlign("right")
                                    .textBaseline("top")
                                    .text(function(d) d.subtitle);

                                vis.render();



                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <span id="work_golf" title="<?php echo $golf_complete; ?>"></span>
    <span id="work_kwan" title="<?php echo $kwan_complete; ?>"></span>
    <span id="work_em" title="<?php echo $em_complete; ?>"></span>
</section><!-- /.content -->