<?php

namespace app\modules\webreport\controllers;

use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii\web\Controller;
use yii\web\Session;
use yii\db\Expression;
use yii;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiTime;
use mPDF;

class TimeController extends \yii\web\Controller
{
	public $layout = 'webreportlayouts';

    public function actionIndex()
    {
        $postYear = Yii::$app->request->post();
        $GET_year = $postYear['reservation'];
        print_r($GET_year);
        exit();
        return $this->render('index');
    }

    public function actionTime_repair()
    {
        return $this->render('time_repair',['query'=>false,]);
    }

    public function actionOrder_repair()
    {

        return $this->render('order_repair',['query'=>false,]);
    }

    public function actionTime_average() 
    {

        $postValue = Yii::$app->request->post();
        $select_company = ($postValue['company']);
        $months = ($postValue['reservation']);
        $select_Time = DateTime::FormatDateFromCalendarRange($months);

        $Time_start = $select_Time['start_date'];
        $Time_End = $select_Time['end_date'];


        $select_technician = ($postValue['technician']);
        if ($select_technician == '1') {
        $technician_TypeCar = '3,4';
        } elseif ($select_technician == '2') {
        $technician_TypeCar = '1,2';
        } elseif ($select_technician == '3') {
        $technician_TypeCar = '1,2,3,4';
        }

        $datacktime_start = ApiTime::getDateCkeTimeRangStart($select_company,$Time_start,$Time_End,$technician_TypeCar);


        $datackpricefree = ApiTime::getDataTimeRangCkPriceFree($Time_start,$Time_End);




        if(!empty($datacktime_start)){

            $id_cvchmain= [];
            foreach ($datackpricefree AS $value)
            {
                $id_cvchmain [] = $value['id_cvch'];
            }
            $IN_IDcvhcmainfree = implode(",",$id_cvchmain);


            $List_DrepairinformWagefree = ApiTime::getDataRangListDrepairinformWagefree($IN_IDcvhcmainfree,$Time_start,$Time_End);


            $id_drepairinformfordrepair_informfree = [];
            foreach ($List_DrepairinformWagefree AS $value)
            {
                $id_drepairinformfordrepair_informfree[] = $value['id_drepair_inform'];
            }
            $IN_get_IdDrepairFree = implode(",",$id_drepairinformfordrepair_informfree);



            $id_drepairinformfordrepair_inform = [];
            foreach ($datacktime_start AS $value)
            {
                $id_drepairinformfordrepair_inform[] = $value['id_drepair_inform'];
            }
            $IN_get_IdDrepair = implode(",",$id_drepairinformfordrepair_inform);


            $List_Drepairinformlist = ApiTime::getDrepairinformlist($IN_get_IdDrepair);
            $List_Drepairinformlistfree = ApiTime::getDrepairinformlist($IN_get_IdDrepairFree);


            $arrWageDistance = ApiReport::ARRAY_WAGE_GROUP_CHECK_DISTANCE;
            $arrWageGenaral = ApiReport::ARRAY_WAGE_GROUP_REPAIR_GENERAL;
            $drepair_inform_id_wage_distance = $drepair_inform_id_wage_genaral = [];

            foreach ($List_Drepairinformlist as $repair_id_header){

                $_repair_id_header = $repair_id_header['repair_id_header'];
                $_wage = $repair_id_header['wage'];
                foreach ($arrWageDistance as $key => $value) {
                    if($_repair_id_header==$key && in_array($_wage, $value) ) {
                        $drepair_inform_id_wage_distance[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                    }
                }

                foreach ($arrWageGenaral as $key => $value) {
                    if($repair_id_header['repair_id_header']==$key && in_array($_wage, $value) ) {
                        $drepair_inform_id_wage_genaral[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                    }
                }
            }


            $id_drepairinformfordrepair_informFree = [];
            foreach ($List_Drepairinformlistfree AS $value)
            {
                $id_drepairinformfordrepair_informFree[$value['drepair_inform_id']] = $value['drepair_inform_id'];
            }



            $drepair_inform_id_wage_distance = array_unique($drepair_inform_id_wage_distance);//เช็คระยะไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง
            $drepair_inform_id_wage_genaral = array_unique($drepair_inform_id_wage_genaral);//ซ่อมทั่วไปไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง

            $drepair_inform_id_wage_intersect =array_intersect($drepair_inform_id_wage_distance,$drepair_inform_id_wage_genaral); // $drepair_inform_id_wage_intersect ซ่อมทั่วไป+เช็คระยะ
            $drepair_inform_id_wage_intersect_Notfree = array_diff_key($drepair_inform_id_wage_intersect,$id_drepairinformfordrepair_informFree);

            ///////////// เช็คระยะอย่างเดี่ยว /////////////
            $drepair_inform_id_wage_distance_only = array_diff_key($drepair_inform_id_wage_distance,$drepair_inform_id_wage_intersect_Notfree);
            ///////////// ซ่อมทั่วไปอย่างเดียว /////////////
            $drepair_inform_id_wage_genaral_only = array_diff_key($drepair_inform_id_wage_genaral,$drepair_inform_id_wage_intersect_Notfree);


            ////////
            $IN_get_IdDrepairFree = implode(",",$id_drepairinformfordrepair_informFree);
            $IN_get_IdDrepairwage_distance = implode(",",$drepair_inform_id_wage_distance_only);
            $IN_get_IdDrepairwage_genaral = implode(",",$drepair_inform_id_wage_genaral_only);
            $IN_get_IdDrepairintersect_Notfree = implode(",",$drepair_inform_id_wage_intersect_Notfree);


            $resultDreparirFreeRangTimeAVG = ApiTime::resultDrepairRangTimeAVG($IN_get_IdDrepairFree,$select_company,$Time_start,$Time_End);
            $resultDreparirDistanceRangTimeAVG = ApiTime::resultDrepairRangTimeAVG($IN_get_IdDrepairwage_distance,$select_company,$Time_start,$Time_End);
            $resultDreparirGenaralRangTimeAVG  = ApiTime::resultDrepairRangTimeAVG($IN_get_IdDrepairwage_genaral,$select_company,$Time_start,$Time_End);
            $resultDreparirIntersectRangTimeAVG = ApiTime::resultDrepairRangTimeAVG($IN_get_IdDrepairintersect_Notfree,$select_company,$Time_start,$Time_End);

            $dateRang  = ApiTime::getDayRangtime($Time_start,$Time_End);
            //////////////  ฟรี  //////////////


            $getWageFreeTimeAvg = [];
            foreach ($resultDreparirFreeRangTimeAVG AS $value){
                $startTime = trim($value['time_start']);
                $getWageFreeTimeAvg[$startTime] = $value['SumDifftime'];
            }
            $dateRangTimeAvgFree = [];

            foreach ($dateRang AS $key => $valueTime)
            {
                if (array_key_exists($key,$getWageFreeTimeAvg))
                {
                     $dateRangTimeAvgFree[$key] = $getWageFreeTimeAvg[$key];
                }
                else
                {
                     $dateRangTimeAvgFree[$key] = 0;
                }
            }

            //////////////  เช็คระยะ //////////////
            $getWageDistanceTimeAvg = [];
            foreach ($resultDreparirDistanceRangTimeAVG AS $value){
                $startTime = trim($value['time_start']);
                $getWageDistanceTimeAvg[$startTime] = $value['SumDifftime'];
            }

            $dateRangTimeAvgDistance = [];
            foreach ($dateRang AS $key => $valueTime)
            {
                if (array_key_exists($key,$getWageDistanceTimeAvg))
                {
                    $dateRangTimeAvgDistance[$key] = $getWageDistanceTimeAvg[$key];
                }
                else
                {
                    $dateRangTimeAvgDistance[$key] = 0;
                }
            }

            //////////////  ทั่วไป //////////////
            $getWageGenaralTimeAvg = [];
            foreach ($resultDreparirGenaralRangTimeAVG AS $value){
                $startTime = trim($value['time_start']);
                $getWageGenaralTimeAvg[$startTime] = $value['SumDifftime'];
            }

            $dateRangTimeAvgGenaral = [];
            foreach ($dateRang AS $key => $valueTime)
            {
                if (array_key_exists($key,$getWageGenaralTimeAvg))
                {
                    $dateRangTimeAvgGenaral[$key] = $getWageGenaralTimeAvg[$key];
                }
                else
                {
                    $dateRangTimeAvgGenaral[$key] = 0;
                }
            }

            //////////////  ทั่วไป+เช็คระยะ //////////////
            $getWageIntersectTimeAvg = [];
            foreach ($resultDreparirIntersectRangTimeAVG AS $value){
                $startTime = trim($value['time_start']);
                $getWageIntersectTimeAvg[$startTime] = $value['SumDifftime'];
            }

            $dateRangTimeAvgIntersect = [];
            foreach ($dateRang AS $key => $valueTime)
            {
                if (array_key_exists($key,$getWageIntersectTimeAvg))
                {
                    $dateRangTimeAvgIntersect[$key] = $getWageIntersectTimeAvg[$key];
                }
                else
                {
                    $dateRangTimeAvgIntersect[$key] = 0;
                }
            }


           
            $resultdateRangTimeAvgDistance = $resultdateRangTimeAvgIntersect = $resultdateRangTimeAvgGenaral = $resultdateRangTimeAvgFree = [];
            foreach ($dateRangTimeAvgDistance AS $key => $value){
                                
                $valueTimeAvgDistance = ApiTime::getAvgTimeReport1($value);
                  $resultdateRangTimeAvgDistance [$key] =   Helper::displayDecimal($valueTimeAvgDistance);
            }
            foreach ($dateRangTimeAvgIntersect AS $key => $value){
                                
                $valueTimeAvgIntersect = ApiTime::getAvgTimeReport1($value);
                  $resultdateRangTimeAvgIntersect [$key] =   Helper::displayDecimal($valueTimeAvgIntersect);
            }
            foreach ($dateRangTimeAvgGenaral AS $key => $value){
                                
                $valueRangTimeAvgGenaral = ApiTime::getAvgTimeReport1($value);
                  $resultdateRangTimeAvgGenaral [$key] =   Helper::displayDecimal($valueRangTimeAvgGenaral);
            }
            foreach ($dateRangTimeAvgFree AS $key => $value){
                                
                $valueRangTimeAvgFree = ApiTime::getAvgTimeReport1($value);
                  $resultdateRangTimeAvgFree [$key] =   Helper::displayDecimal($valueRangTimeAvgFree);
            }

            $totalResultSession = (array_merge_recursive($resultdateRangTimeAvgDistance,$resultdateRangTimeAvgIntersect,$resultdateRangTimeAvgGenaral,$resultdateRangTimeAvgFree));
            
            $session = Yii::$app->session;

            $session->set('totalResultSession', $totalResultSession);
            $session->set('selected_company', $select_company);
            $session->set('selected_startmonth', $months);
            $session->set('select_technician', $select_technician);


            return $this->render('time_repair',[
                'dateRangTimeAvgFree' => $dateRangTimeAvgFree,
                'dateRangTimeAvgDistance' => $dateRangTimeAvgDistance,
                'dateRangTimeAvgGenaral' => $dateRangTimeAvgGenaral,
                'dateRangTimeAvgIntersect' => $dateRangTimeAvgIntersect,
                'query'=>true,
                'selected_company'=>$select_company,
                'selected_startmonth'=>$months,
                'select_technician'=>$select_technician,]);

        }
        else
        {
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('time_repair',[
                'query'=>false,
                'selected_company'=>$select_company,
                'selected_startmonth'=>$months,
                'select_technician'=>$select_technician,]);
        }


    }



    public function actionCount_orderrepair()
    {
        $postValue =Yii::$app->request->post();

        $select_company = $postValue['company'];
        $select_year = $postValue['year'];
        $select_technician = $postValue['technician'];

        if ($select_technician == '1') {
            $technician_TypeCar = '3,4';
        } elseif ($select_technician == '2') {
            $technician_TypeCar = '1,2';
        }

        $datacktime_start = ApiTime::getDateCkeTimeStart($select_company,$select_year,$technician_TypeCar);

        $datackpricefree = ApiTime::getDataCkPriceFree($select_year);
        if(!empty($datacktime_start)){

        $id_cvchmain= [];
        foreach ($datackpricefree AS $value)
        {
            $id_cvchmain [] = $value['id_cvch'];
        }
        $IN_IDcvhcmainfree = implode(",",$id_cvchmain);


        $List_DrepairinformWagefree = ApiTime::getDataListDrepairinformWagefree($IN_IDcvhcmainfree,$select_year);

        $id_drepairinformfordrepair_informfree = [];
        foreach ($List_DrepairinformWagefree AS $value)
        {
            $id_drepairinformfordrepair_informfree[] = $value['id_drepair_inform'];
        }
        $IN_get_IdDrepairFree = implode(",",$id_drepairinformfordrepair_informfree);



        $id_drepairinformfordrepair_inform = [];
        foreach ($datacktime_start AS $value)
        {
            $id_drepairinformfordrepair_inform[] = $value['id_drepair_inform'];
        }
        $IN_get_IdDrepair = implode(",",$id_drepairinformfordrepair_inform);


        $List_Drepairinformlist = ApiTime::getDrepairinformlist($IN_get_IdDrepair);
        $List_Drepairinformlistfree = ApiTime::getDrepairinformlist($IN_get_IdDrepairFree);


        $arrWageDistance = ApiReport::ARRAY_WAGE_GROUP_CHECK_DISTANCE;
        $arrWageGenaral = ApiReport::ARRAY_WAGE_GROUP_REPAIR_GENERAL;
        $drepair_inform_id_wage_distance = $drepair_inform_id_wage_genaral = [];

        foreach ($List_Drepairinformlist as $repair_id_header){

            $_repair_id_header = $repair_id_header['repair_id_header'];
            $_wage = $repair_id_header['wage'];
            foreach ($arrWageDistance as $key => $value) {
                if($_repair_id_header==$key && in_array($_wage, $value) ) {
                    $drepair_inform_id_wage_distance[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                }
            }

            foreach ($arrWageGenaral as $key => $value) {
                if($repair_id_header['repair_id_header']==$key && in_array($_wage, $value) ) {
                    $drepair_inform_id_wage_genaral[$repair_id_header['drepair_inform_id']] = $repair_id_header['drepair_inform_id'];
                }
            }
        }


        $id_drepairinformfordrepair_informFree = [];
        foreach ($List_Drepairinformlistfree AS $value)
        {
            $id_drepairinformfordrepair_informFree[$value['drepair_inform_id']] = $value['drepair_inform_id'];
        }



        $drepair_inform_id_wage_distance = array_unique($drepair_inform_id_wage_distance);//เช็คระยะไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง
        $drepair_inform_id_wage_genaral = array_unique($drepair_inform_id_wage_genaral);//ซ่อมทั่วไปไอดีที่อยู่ใน drepair_inform_idแต่ยังไม่ได้กรอง

        $drepair_inform_id_wage_intersect =array_intersect($drepair_inform_id_wage_distance,$drepair_inform_id_wage_genaral); // $drepair_inform_id_wage_intersect ซ่อมทั่วไป+เช็คระยะ
        $drepair_inform_id_wage_intersect_Notfree = array_diff_key($drepair_inform_id_wage_intersect,$id_drepairinformfordrepair_informFree);

        ///////////// เช็คระยะอย่างเดี่ยว /////////////
        $drepair_inform_id_wage_distance_only = array_diff_key($drepair_inform_id_wage_distance,$drepair_inform_id_wage_intersect_Notfree);
        ///////////// ซ่อมทั่วไปอย่างเดียว /////////////
        $drepair_inform_id_wage_genaral_only = array_diff_key($drepair_inform_id_wage_genaral,$drepair_inform_id_wage_intersect_Notfree);


        ////////
        $IN_get_IdDrepairFree = implode(",",$id_drepairinformfordrepair_informFree);
        $IN_get_IdDrepairwage_distance = implode(",",$drepair_inform_id_wage_distance_only);
        $IN_get_IdDrepairwage_genaral = implode(",",$drepair_inform_id_wage_genaral_only);
        $IN_get_IdDrepairintersect_Notfree = implode(",",$drepair_inform_id_wage_intersect_Notfree);

        $resultDreparirFree = ApiTime::resultDrepair($IN_get_IdDrepairFree,$select_company,$select_year);
        $resultDreparirDistance = ApiTime::resultDrepair($IN_get_IdDrepairwage_distance,$select_company,$select_year);
        $resultDreparirGenaral  = ApiTime::resultDrepair($IN_get_IdDrepairwage_genaral,$select_company,$select_year);
        $resultDreparirIntersect = ApiTime::resultDrepair($IN_get_IdDrepairintersect_Notfree,$select_company,$select_year);



            $session = Yii::$app->session;

            $session->set('resultDreparirFree', $resultDreparirFree);
            $session->set('resultDreparirDistance', $resultDreparirDistance);
            $session->set('resultDreparirGenaral', $resultDreparirGenaral);
            $session->set('resultDreparirIntersect', $resultDreparirIntersect);
            $session->set('selected_company', $select_company);
            $session->set('selected_startmonth', $select_year);
            $session->set('select_technician', $select_technician);

            return $this->render('order_repair',[
                'resultDreparirFree' => $resultDreparirFree,
                'resultDreparirDistance' => $resultDreparirDistance,
                'resultDreparirGenaral' => $resultDreparirGenaral,
                'resultDreparirIntersect' => $resultDreparirIntersect,
                'query'=>true,
                'selected_company'=>$select_company,
                'selected_startmonth'=>$select_year,
                'select_technician'=>$select_technician,]);

        }
        else
        {
            Yii::$app->session->setFlash('warning', "ไม่พบข้อมูลที่ค้นหา");
            return $this->render('order_repair',[
                'query'=>false,
                'selected_company'=>$select_company,
                'selected_startmonth'=>$select_year,
                'selected_year'=>$select_technician,]);
        }



    }
    public function actionBtn_reporttime()/*05-10-2559 th mpdf*/
        {
            /*$session = Yii::$app->session;
            $company = $session->get('SESSION_company');
            $year = $session->get('SESSION_year');*/


            //foreach($this->_globalCompay as $company) {
            //echo $company->id;
            //}
            /*$mpdf->AddPage('A4');*/
            //$mpdf = new mPDF('th', 'Tharlon-Regular');
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($this->renderPartial('_reporttime'));
            $mpdf->Output();
            exit;

        }

    public function actionBtn_reportorder()/*05-10-2559 th mpdf*/
        {
            /*$session = Yii::$app->session;
            $company = $session->get('SESSION_company');
            $year = $session->get('SESSION_year');*/


            //foreach($this->_globalCompay as $company) {
            //echo $company->id;
            //}
            /*$mpdf->AddPage('A4');*/
            //$mpdf = new mPDF('th', 'Tharlon-Regular');
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($this->renderPartial('_reportorder'));
            $mpdf->Output();
            exit;

        }
}
