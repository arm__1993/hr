
<?php

/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 13/10/2559
 * Time: 16:30
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use app\api\ApiReport;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiRepair;


HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);
?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานเวลารับแจ้งซ่อมเฉลี่ยต่อคัน รายเดือน ตามใบงานซ่อม</h3>


    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>        
        
        <div class="box-body">

                <div class="row">

                <form method="post" action="receive_avg_repair_report">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                    <div class="col-md-12">
                        <center>
                        <label>บริษัท</label>
                        &nbsp;
                        <div class="btn-group">
                            <select class="form-control" name="company" id="company">
                                <?php
                                $arrCompany = ApiReport::getAllCompay();
                                foreach ($arrCompany as $key=>$value) {
                                    $sel = ($selected_company==$key) ? ' selected="selected" ' : '';
                                    echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                            <label>เลือกปี</label>
                            &nbsp;
                            <div class="btn-group">
                                <select class="form-control" name="year" id="year">
                                    <?php
                                    $get_year = ApiReport::getAppYear();
                                    foreach ($get_year as $key=>$value) {
                                        $sel = ($selected_year==$key) ? ' selected="selected" ' : '';
                                        echo '<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>&nbsp;&nbsp;
                        <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                        </div>
                        &nbsp;
                        <div class="btn-group">
                        <a href="<?php echo SITE_URL;?>/repair/btn_reportavgr">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                        </button>
                        </a>
                        </div>
                    </center>
                    </div>
                    <!--/col-md-12-->
                </form>
            </div>
            <!--/row-->
        </div>
        <!--/box-body-->

<?php if($query) {  ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-warning">
                 <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" >
                                <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                                        เดือน
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                        เวลาเฉลี่ยต่อคัน (นาที) งานเช็คระยะ
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                        เวลาเฉลี่ยต่อคัน (นาที) งานเช็คระยะ + งานซ่อม
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                        เวลาเฉลี่ยต่อคัน (นาที) งานซ่อม
                                        </th>
                                    </tr>
                                </thead>

                                <tbody> 
                            <?php   for ($i=1; $i <= 12 ; $i++) {
                                ?>
                                    <tr role="row">
                                        <td><?php  $arrMonth = DateTime::convertMonth($i) ;
                                        echo $arrMonth;
                                            ?>
                                        </td>
                                        <td><?php $SumTimeA = $dataTime_Wage_Distance_Only[$i-1]['SumDifftimeTime'];
                                                 $Countcar = $dataTime_Wage_Distance_Only[$i-1]['CountCar'];
                                                 $avgTimeA = ApiRepair::avgTimePerCar($SumTimeA,$Countcar) ;
                                                 $avgTimeA = DateTime::calculateTimeElapsed($avgTimeA);
                                            echo ApiRepair::getFormatTime($avgTimeA['h'],$avgTimeA['m'],$avgTimeA['s']);
                                                  ?></td>
                                        <td><?php $SumTimeB = $dataTime_Wage_intersect[$i-1]['SumDifftimeTime'];
                                            $Countcar = $dataTime_Wage_intersect[$i-1]['CountCar'];
                                            $avgTimeB = ApiRepair::avgTimePerCar($SumTimeB,$Countcar) ;
                                            $avgTimeB = DateTime::calculateTimeElapsed($avgTimeB);
                                            echo ApiRepair::getFormatTime($avgTimeB['h'],$avgTimeB['m'],$avgTimeB['s']);
                                             ?></td>
                                        <td><?php $SumTimeC = $dataTime_Wage_Genaral_Only[$i-1]['SumDifftimeTime'];
                                            $Countcar = $dataTime_Wage_Genaral_Only[$i-1]['CountCar'];
                                            $avgTimeC = ApiRepair::avgTimePerCar($SumTimeC,$Countcar) ;
                                            $avgTimeC = DateTime::calculateTimeElapsed($avgTimeC);
                                            echo ApiRepair::getFormatTime($avgTimeC['h'],$avgTimeC['m'],$avgTimeC['s']); ?></td>
                                    </tr>
                            <?php  }  ?>                                                           
                                </tbody>
                            </table>                                                        
                            </div>
                            <!-- col-sm-12 -->
                        </div>
                        <!-- row -->
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-warning-->
            <!-- AREA TABLE -->
            </div>        
    </section>

        <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php
                                        if(!empty($dataTime_Wage_Distance_Only)){
                                            $dataASumDiffTimeA = [];
                                            foreach ($dataTime_Wage_Distance_Only as $value)
                                            {
                                                $AvgTimeA = ApiRepair::avgTimePerCar($value['SumDifftimeTime'],$value['CountCar']);
                                                $AvgTimeA = DateTime::calculateTimeElapsed($AvgTimeA);
                                                if(strlen($AvgTimeA['s']) ==1) {
                                                    $AvgTimeA['s'] = str_pad($AvgTimeA['s'],2,"0",STR_PAD_LEFT);
                                                }
                                                $FormatTimeA  = $AvgTimeA['m'].".".$AvgTimeA['s'];
                                                $dataASumDiffTimeA[] = floatval($FormatTimeA);


                                            }
                                        }
                                        if(!empty($dataTime_Wage_intersect)){
                                            $dataASumDiffTimeB = [];
                                            foreach ($dataTime_Wage_intersect as $value)
                                            {
                                                $AvgTimeB= ApiRepair::avgTimePerCar($value['SumDifftimeTime'],$value['CountCar']);
                                                $AvgTimeB = DateTime::calculateTimeElapsed($AvgTimeB);
                                                if(strlen($AvgTimeB['s']) ==1) {
                                                    $AvgTimeB['s'] = str_pad($AvgTimeB['s'],2,"0",STR_PAD_LEFT);
                                                }
                                                $FormatTimeB  = $AvgTimeA['m'].".".$AvgTimeB['s'];
                                                $dataASumDiffTimeB[] = floatval($FormatTimeB);


                                            }
                                        }
                                        if(!empty($dataTime_Wage_Genaral_Only)){
                                            $dataASumDiffTimeC = [];
                                            foreach ($dataTime_Wage_Genaral_Only as $value)
                                            {
                                                $AvgTimeC = ApiRepair::avgTimePerCar($value['SumDifftimeTime'],$value['CountCar']);
                                                $AvgTimeC = DateTime::calculateTimeElapsed($AvgTimeC);
                                                if(strlen($AvgTimeC['s']) ==1) {
                                                    $AvgTimeC['s'] = str_pad($AvgTimeC['s'],2,"0",STR_PAD_LEFT);
                                                }
                                                $FormatTimeC  = $AvgTimeC['m'].".".$AvgTimeC['s'];
                                                $dataASumDiffTimeC[] = floatval($FormatTimeC);


                                            }
                                        }



                                            echo Highcharts::widget([
                                                'options' => [
                                                    'title' => [
                                                        'text' => ''
                                                    ],
                                                    'xAxis' => [
                                                        'type' => 'category',
                                                        'categories' => ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']
                                                    ],
                                                    'yAxis' => [
                                                        'title' => [
                                                            'text' => ''
                                                        ]
                                                    ],
                                                    'legend' => [
                                                        'enabled' => true
                                                    ],
                                                    'tooltip' => [
                                                        'headerFormat' => '<span style="font-size:11px">{series.name}</span><br>',
                                                        'pointFormat' => '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
                                                    ],
                                                    'credits' => [
                                                        'enabled' => false
                                                    ],
                                                    'series' => [
                                                        [
                                                            'borderWidth' => 0,
                                                            'dataLabels' => [
                                                                'enabled' => true,
                                                                'format' => '{point.y:.2f}'
                                                            ],
                                                            'type' => 'column',
                                                            'name' => 'เวลาเฉลี่ยต่อคัน (นาที) งานเช้คระยะ',
                                                            'data' => $dataASumDiffTimeA,
                                                        ],
                                                        [
                                                            'borderWidth' => 0,
                                                            'dataLabels' => [
                                                                'enabled' => true,
                                                                'format' => '{point.y:.2f}'
                                                            ],
                                                            'type' => 'column',
                                                            'name' => 'เวลาเฉลี่ยต่อคัน (นาที) งานเช้คระยะ + งานซ่อม',
                                                            'data' => $dataASumDiffTimeB,
                                                        ],
                                                        [
                                                            'borderWidth' => 0,
                                                            'dataLabels' => [
                                                                'enabled' => true,
                                                                'format' => '{point.y:.2f}'
                                                            ],
                                                            'type' => 'column',
                                                            'name' => 'เวลาเฉลี่ยต่อคัน (นาที) งานซ่อม',
                                                            'data' => $dataASumDiffTimeC,
                                                        ],
                                                        ],
                                                    ],
                                            ]);
                                          ?>
                                    </div>
                                </div><!--row-->
                        </div>
                    </div><!--box-body-->
            </div><!--box box-info-->
            <!-- AREA CHART -->
            </div>
        </div>
    </section>
<?php } ?>
</div><!--box-->
<?php if (Yii::$app->session->hasFlash('warning')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        <?= Yii::$app->session->getFlash('warning') ?>
    </div>
<?php endif; ?>