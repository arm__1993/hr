<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 11/29/2016 AD
 * Time: 18:07
 */

namespace app\modules\webreport\apiwebreport;
use app\modules\webreport\models\Hbso;
use app\modules\webreport\models\Asellcompany;
use app\modules\webreport\models\TargetYear;
use app\modules\webreport\models\Ytax;
use app\modules\webreport\models\Workingcompany;
use yii;

class ApiSpares
{
	public static function getNoNumber($type_company,$str_start,$str_end)
	{
		//$sql_NoNumber =  แบบไม่มีเลขรถ
        $sql_NoNumber = "SELECT ytax.ref_id,ytax.day,ytax_list_parts.pw_code,ytax_list_parts.pw_name,ytax_list_parts.unit,
                (ytax_list_parts.novat_cap*ytax_list_parts.unit) as sum_novatcap,ytax_list_parts.receive               
                FROM ytax
                inner join ytax_list_parts on ytax_list_parts.ytax_id = ytax.id                
                WHERE ytax.ref_status = 2 
                and ytax.ybranch = $type_company
                and ytax.day between '$str_start' and '$str_end'
                and ytax_list_parts.status != 99
                and ytax_list_parts.receive != 0
                and ytax.status != 99";

        $NoNumber = Yii::$app->dbERP_service->createCommand($sql_NoNumber)->queryAll();


    return $NoNumber;
	}
	public static function getNumber($type_company,$str_start,$str_end)
	{  
		//$sql_Number = แบบมีเลขรถ
        $sql_Number = "SELECT ytax.ref_id,ahistory.model_name,ahistory.engin_id,ahistory.register, ytax_list_parts.pw_code,ytax_list_parts.pw_name,ytax_list_parts.unit, 
                (ytax_list_parts.novat_cap*ytax_list_parts.unit) as sum_novatcap,ytax_list_parts.receive 
                FROM ytax inner join ytax_list_parts on ytax_list_parts.ytax_id = ytax.id 
                inner join ahistory on ahistory.id = ytax.ahistory_id 
                WHERE ytax.ref_status IN (1,3) and ytax.ybranch = $type_company 
                and ytax.day between '$str_start' and '$str_end' and ytax_list_parts.status != 99 
                and ahistory.status_new != 99
                and ytax_list_parts.receive != 0
                and ytax.status != 99";
        $Number = Yii::$app->dbERP_service->createCommand($sql_Number)->queryAll();


    return $Number;
	}

}