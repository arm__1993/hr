-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 22, 2017 at 04:42 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.27-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ERP_easyhr`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_family`
--

CREATE TABLE `emp_family` (
  `id` int(11) NOT NULL,
  `emp_data_id` int(11) DEFAULT NULL,
  `parent_status` varchar(50) DEFAULT NULL COMMENT 'สถานะบิดา-มารดา',
  `father_tax_status` int(11) DEFAULT NULL COMMENT 'สถานะใช้สิทธิ์ลดหย่อน 1=ลด,0=ไม่ลด',
  `father_be` varchar(100) DEFAULT NULL COMMENT 'รหัสคำนำหน้า',
  `father_name` varchar(200) DEFAULT NULL COMMENT 'ชื่อบิดา',
  `father_surname` varchar(200) DEFAULT NULL COMMENT 'นามสกุลบิดา',
  `father_idcard` varchar(14) DEFAULT NULL COMMENT 'รหัสบัตรประจำตัวประชาชนบิดา',
  `father_birthday` date DEFAULT NULL COMMENT 'วันเดือนปีที่เกิดบิดา',
  `father_insurance` varchar(100) DEFAULT NULL COMMENT 'เบี้ยประกันสุขภาพบิดา',
  `father_business` varchar(200) DEFAULT NULL COMMENT 'อาชีพบิดา',
  `father_place` varchar(250) DEFAULT NULL COMMENT 'father_place',
  `father_phone` varchar(50) DEFAULT NULL,
  `mother_tax_status` int(11) DEFAULT NULL COMMENT 'สถานะใช้สิทธิ์ลดหย่อน 1=ลด,0=ไม่ลด',
  `mother_be` varchar(100) DEFAULT NULL COMMENT 'รหัสคำนำหน้า',
  `mother_name` varchar(200) DEFAULT NULL COMMENT 'ชื่อมารดา',
  `mother_surname` varchar(200) DEFAULT NULL COMMENT 'นามสกุลมารดา',
  `mother_idcard` varchar(14) DEFAULT NULL,
  `mother_birthday` date DEFAULT NULL,
  `mother_insurance` varchar(100) DEFAULT NULL COMMENT 'เบี้ยประกันสุขภาพมารดา',
  `mother_business` varchar(200) DEFAULT NULL COMMENT 'อาชีพมารดา',
  `mother_place` varchar(200) DEFAULT NULL COMMENT 'สถานที่ทำงาน',
  `mother_phone` varchar(50) DEFAULT NULL,
  `spouse_status` varchar(100) DEFAULT NULL COMMENT 'สถานะภาพ',
  `spouse_be` varchar(100) DEFAULT NULL COMMENT 'คำนำหน้า ',
  `spouse_sex` varchar(10) DEFAULT NULL COMMENT 'เพศ 1= ชาย, 2= หญิง',
  `spouse_name` varchar(200) DEFAULT NULL COMMENT 'ชื่อคู่สมรส',
  `spouse_surname` varchar(200) DEFAULT NULL COMMENT 'นามสกุลคู่สมรส',
  `spouse_idcard` varchar(13) DEFAULT NULL COMMENT 'หมายเลขบัตรประชาชน คู่สมรส',
  `spouse_birthday` date DEFAULT NULL,
  `spouse_insurance` varchar(50) DEFAULT NULL COMMENT 'เบี้ยประกันสุขภาพ คู่สมสรส',
  `spouse_business` varchar(250) DEFAULT NULL COMMENT 'อาชีพ คู่สมรส',
  `spouse_place` varchar(250) DEFAULT NULL COMMENT 'สถานที่ทำงาน คู่สมรส',
  `spouse_phone` varchar(50) DEFAULT NULL,
  `relative_be` varchar(100) DEFAULT NULL,
  `relative_status` varchar(200) DEFAULT NULL COMMENT 'ความสัมพันธ์ผู้ติดต่อฉุกเฉิน',
  `relative_sex` varchar(10) DEFAULT NULL COMMENT 'เพศของผู้ติดต่อฉุกเฉิน 1:ชาย, 2:หญิง',
  `relative_name` varchar(200) DEFAULT NULL COMMENT 'ชื่อผู้ติดต่อ',
  `relative_surname` varchar(200) DEFAULT NULL COMMENT 'นามสกุลผู้ติดต่อ',
  `relative_phone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_family`
--
ALTER TABLE `emp_family`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_family`
--
ALTER TABLE `emp_family`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
