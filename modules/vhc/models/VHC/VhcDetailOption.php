<?php

namespace app\modules\vhc\models\VHC;

use Yii;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_detail_option".
 *
 * @property integer $id
 * @property string $name
 * @property integer $value_option
 * @property string $comment
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcDetailOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_detail_option';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value_option'], 'required'],
            [['value_option', 'status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['name', 'comment'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value_option' => 'Value Option',
            'comment' => 'Comment',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
