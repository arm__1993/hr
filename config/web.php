<?php


/** @var   SET Global USER/PWD */
/* ============================================================== */
const PRD_MODE = false;  //change this if production mode;
const REMOTE_HOST = false;  //change if remote host
const DB_DEMO_PREFIX = ''; //prefix for database DEMO_
const DB_PRD_PREFIX = ''; //prefix for database PRODUCTION

if(PRD_MODE) {
    $DB_USER    = "root";
    $DB_PWD     = "";
    $DB_HOST    = "localhost"; //IP DATABASE Server
}
else {
    $DB_USER    = "root";
    $DB_PWD     = "";
    $DB_HOST    = "localhost";
}


/* ============================================================== */

$params = require(__DIR__ . '/params/params.php');
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],

    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ThisisWebservice2016',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            //'errorAction' => 'site/error',
            'errorAction' => '/hr/error/index',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        //'db' => require(__DIR__ . '/db.php'),
        'dbERP_service'                 => require(__DIR__ . '/db/dbERP_service.php'),
        'dbERP_maincusdata'             => require(__DIR__ . '/db/dbERP_maincusdata.php'),
        'dbERP_new_cusdata'             => require(__DIR__ . '/db/dbERP_new_cusdata.php'),
        'dbERP_Easysale_icmba'          => require(__DIR__ . '/db/dbERP_Easysale_icmba.php'),
        'dbERP_easyhr_OU'               => require(__DIR__ . '/db/dbERP_easyhr_OU.php'),
        'dbERP_easyhr_TIME_ATTENDANCE'  => require(__DIR__ . '/db/dbERP_easyhr_TIME_ATTENDANCE.php'),
        'dbERP_easyhr_checktime'        => require(__DIR__ . '/db/dbERP_easyhr_checktime.php'),
        'dbERP_easyhr_PAYROLL'          => require(__DIR__ . '/db/dbERP_easyhr_PAYROLL.php'),
        'dbGit_Report'                  => require(__DIR__ . '/db/dbERP_hbsogitreport.php'),



        //'dbERP_easyhr' => require(__DIR__ . '/dbERP_easyhr.php'),


        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    //'controller' => ['wssale/book','wssale/customer'],
                    'controller' => require(__DIR__ . '/controllerWebservice.php'),
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                    // 'extraPatterns' => [
                    //     'GET searchme' => 'searchme',
                    //     'GET searchcustomer' => 'searchcustomer',
                    //     'GET customerid' => 'customerid',
                    //     'PUT updatecustomer' => 'updatecustomer',
                    //     'POST createcustomer' => 'createcustomer',
                    // ],
                    'extraPatterns' => require(__DIR__ . '/extraPatterns.php'),

                ],
                '/' => 'site/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
                '<module:\w+><controller:\w+>/<action:update|delete>/<id:\d+>' => '<module>/<controller>/<action>',

            ],
        ]

    ],
    /* params */
    'params'=> $params,

    /** Add Module Name */
    'modules' => [
        'wssale' => [
            'class' => 'app\modules\wssale\Wssale',
        ],
        'wsservice' => [
            'class' => 'app\modules\wsservice\Wsservice',
        ],
        'wscommon' => [
            'class' => 'app\modules\wscommon\Wscommon',
        ],
        'webreport' => [
            'class' => 'app\modules\webreport\Webreport',
        ],

        'gitreport'=> [
            'class' => 'app\modules\gitreport\Gitreport',
        ],
        'auth' => [
            'class' => 'app\modules\auth\Auth',
        ],
        'hr' => [
            'class' => 'app\modules\hr\Hr',
        ],
        'cronjob' => [
            'class' => 'app\modules\cronjob\Cronjob',
        ],
        'vhc' => [
            'class' => 'app\modules\vhc\Vhc',
        ],
        'baymanagement' => [
            'class' => 'app\modules\baymanagement\Baymanagement',
        ],

    ],

];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;