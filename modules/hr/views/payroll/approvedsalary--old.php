<?php
/**
* Created by PhpStorm.
* User: adithep
* Date: 2/22/2017 AD
* Time: 10:32
*/
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\controllers\payroll;
use app\modules\hr\apihr\ApiPayroll;


$imghr = Yii::$app->request->baseUrl . '/images/wshr';
AppAsset::register($this);
// $this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-1.12.4.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/approvedsalary.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
//$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
// //$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);

$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL().$basePath.'/'.$moduleID;

define('SITE_URL', $siteURL);


$script = <<< JS
$(document).ready(function() {
setTimeout(function() {
$('#message').fadeOut('slow');
}, 3000);


$("#example2").DataTable({
"lengthChange": false,
"searching": true,
"paging": true,
"info": true,
"pageLength" : 10,
}
);

$("#tablelistdata").DataTable({
"lengthChange": false,
"searching": true,
"paging": true,
"info": true,
"pageLength" : 10,
}
);
});
JS;

$this->registerCssFile(Yii::$app->request->baseUrl."/css/hr/jquery-ui.css");
//$this->registerCssFile(Yii::$app->request->baseUrl."/fonts/01thaifontcss.css");


$this->registerJs($script);
?>
<style type="text/css">
      .modal-wide .modal-dialog {
               width: 90%; /* or whatever you wish */
        }
</style>

<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li class="active">ยืนยันการจ่ายเงินเดือน</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <form>
           <div class="row">
                    <div class="col-md-1">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp"  class="col-sm-4 control-label">บริษัท</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="selectworking" id="selectworking"
                                            onchange="getCompanyForDepartment(this);" ;>
                                        <option value="">เลือกบริษัท</option>
                                        <?php $working = ApiHr::getWorking_company();
                                        foreach ($working as $value) {
                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                        } ?>
                                    </select>
                                <span id="urlGetDepartment" title="<?php echo \yii\helpers\Url::toRoute('payroll/getdepartment'); ?>"></span>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp"  class="col-sm-3 control-label">แผนก</label>
                                <div class="col-sm-9">
                                        <select class="form-control" name="selectdepartment" id="selectdepartment"
                                            onchange="getDepartmentForSection(this);">
                                        <option value=""> เลือกแผนก</option>
                                        </select>
                                    <span id="urlGetSection" title="<?php echo \yii\helpers\Url::toRoute('payroll/getsection'); ?>"></span>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="numberPassportEmp"  class="col-sm-3 control-label">ฝ่าย</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="selectsection" id="selectsection">
                                        <option value=""> เลือกฝ่าย</option>
                                    </select>
                                </div>
                        </div>
                    </div>
            </div>
            <div class="row">
            &nbsp;
            </div>
            <div class="row">
                    <div class="col-md-1">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="namefullemp"  class="col-sm-4 control-label">ชื่อ</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="namefullemp">
                                <input type="hidden" class="form-control" id="id_emp">
                                </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    &nbsp;
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button type="button" class="btn  btn-primary btn-sm" id="seachdatalist"><i class="fa fa-search"></i> ค้นหา</button>
                        <button type="reset"  class="btn  btn-danger btn-sm"><i class="fa fa-reply"></i> ล้างข้อมูล</button>
                    </div>
                </div>
            </div>
            </form>
            <div class="box box-info" style="display:none" id="showsearch">
                <div class="box-header with-border">
                    <h3 class="box-title">แสดงรายการ</h3>
                </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" >
                            <thead>
                                    <tr>
                                    <th width="3%"><center>ลำดับ</center></th>
                                    <th width="10%"><center>สถานะยืนยัน</center></th>
                                    <th width="8%"><center>ชื่อ-สกุล</center></th>
                                    <th width="10%"><center>ตำแหน่ง</center></th>
                                    <th width="15%"><center>ฝ่าย</center></th>
                                    <th width="15%"><center>แผนก</center></th>
                                    <th width="10%"><center>บริษัท</center></th>
                                    <th width="15%"><center>ผู้ยืนยัน</center></th>
                                    <th width="15%"><center>วันที่ยืนยัน</center></th>
                                </tr>
                            </thead>
                            <tbody id="tblticket">
                            </tbody>
                        </table>
                    </div>
                </div>
           </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->
             <div class="modal fade modal-wide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
               <div class="modal-dialog">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 id="myModalLabel" class="modal-title">ข้อมูลพนักงานที่ตรวจสอบ</h4>
                   </div>
                   <div class="modal-body" width="100%" class="table table-bordered table-hover dataTable">
                    <div class="row">
                        <input type="hidden" id="id_wage_confirm" value="">
                        <div class="col-md-6">
                            <h5><lable id="nameworking"></lable></h5>
                        </div>
                    </div>
                     <div class="row">
                        &nbsp;
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-2">
                            <h5><lable id="nameemp"></lable></h5>
                        </div>
                        <div class="col-md-3">
                             <h5><lable id="positionemp"></lable></h5>
                        </div>
                        <div class="col-md-3">
                            <h5><lable id="diviemp"></lable></h5>
                        </div>
                        <div class="col-md-3">
                            <h5><lable id="wageemp"></lable></h5>
                        </div>
                    </div>
                    <div class="row">
                        &nbsp;
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        <label class="text-center">รายการเพิ่ม</label>
                       <table width="80%" class="table table-bordered table-hover dataTable"  >
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>รายการ</th>
                                    <th>รายละเอียด</th>
                                    <th>จำนวน</th>
                                </tr>
                             </thead>
                             <tbody id="tbadddeduct">
                             </tbody>
                             <tfoot>
                                  <tr>
                                    <td colspan="3" class="text-right"><span >รวมรายการเพิ่ม</span></td>
                                    <td><label id="sumTotalDeduc"></label></td>
                                </tr>
                             </tfoot>
                        </table>
                        </div>
                        <div class="col-md-4">
                        <label class="text-center">รายการหัก</label>
                       <table width="80%" class="table table-bordered table-hover dataTable" >
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>รายการ</th>
                                    <th>รายละเอียด</th>
                                    <th>จำนวน</th>
                                </tr>
                             </thead>
                             <tbody id="tbdededuct">

                             </tbody>
                             <tfoot>
                                  <tr>
                                    <td colspan="3" class="text-right"><span >รวมรายการหัก</span></td>
                                    <td><label id="sumTotaldeDeduc"></label></td>
                                </tr>
                             </tfoot>
                        </table>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>รายการหัก</label>
                                <textarea class="form-control" rows="3" style="width: 70%" readonly></textarea>
                            </div>
                            <div class="form-group">
                                <label>เงินสะสมที่บริษัทจ่ายให้</label>
                                <textarea class="form-control" rows="3" style="width: 80%" readonly>

                                </textarea>
                                  <!--เงินสะสมประจำเดือน กันยายน  300 บาท ยอดรวมทั้งหมด  300 บาท-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>รายได้สะสม มกราคม-ปัจุบัน</label>
                                    <input type="text" class="form-control" value="0.00" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>ภาษีสะสม มกราคม-ปัจุบัน</label>
                                    <input type="text" class="form-control" value="0.00" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <label>ประกันสังคมสะสม มกราคม-ปัจุบัน</label>
                                    <input type="text" class="form-control" value="0.00" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                             <div class="form-group">
                                <label>รายรับทั้งสิ้น</label>
                                <textarea class="form-control" rows="3" style="width: 80%" readonly id="sumallnet">13,120.00
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    &nbsp;
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">
                               <div class="col-sm-6">
                                <div class="form-group">
                                    <label>ยืนยันโดย : <u>อนุพงศ์  สมจิตต์ (ฟรี)<input type="hidden" id="id_card_confirm" value="1579900080649"></u></label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>วันที่ยืนยัน : <u><?php echo date("d-m-Y")?></u></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-block btn-success" id="submit_confirm">ยืนยันการตรวจสอบรายการ</button>
                        </div>
                    </div>
                   </div>
                 </div><!-- /.modal-content -->
               </div><!-- /.modal-dialog -->
             </div><!-- /.modal -->