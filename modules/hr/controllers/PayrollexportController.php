<?php

namespace app\modules\hr\controllers;

use app\api\DateTime;
use app\modules\hr\apihr\ApiPayroll;
use app\modules\hr\apihr\ApiPDF;
use app\modules\hr\apihr\ApiSalary;
use app\modules\hr\models\Adddeductdetail;
use app\modules\hr\models\Adddeducthistory;
use app\modules\hr\models\Adddeductthismonth;
use app\modules\hr\models\Empdata;
use app\modules\hr\models\PNDRECORD;
use app\modules\hr\models\SsoPaidHead;
use app\modules\hr\models\SsoPaidDetail;
use app\modules\hr\models\SummaryMoneyWage;
use app\modules\hr\models\WageHistory;
use app\modules\hr\models\Wagethismonth;
use app\modules\hr\models\Workingcompany;
use yii;

use yii\data\ActiveDataProvider;
use app\modules\hr\apihr\ApiSSOExport;
use app\modules\hr\apihr\ApiPreexportSSO;
use app\modules\hr\apihr\ApiExportbank;
use app\modules\hr\apihr\ApiHr;
use app\api\DBConnect;
use mPDF;
use yii\helpers\Json;
use yii\web\Response;
use ZipArchive;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use app\modules\hr\controllers\MasterController;

class PayrollexportController extends MasterController
{
    public $layout = 'hrlayout';
    public $PDF_Path = 'upload/PDF/';
    // public $idcardLogin;

    // /**
    // * function init() check session active or session login, if not redirect to login page
    // * @return \yii\web\Response
    // */
    // public function init()
    // {

    //     $session = Yii::$app->session;
    //     if(!$session->has('fullname') || !$session->has('idcard')) {
    //         return $this->redirect(array('/auth/default/logout'),302);
    //     }

    //     $this->idcardLogin = $session->get('idcard');
    // }


    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionExporttobank()
    {
        return $this->render('exporttobank');
    }

    public function actionExporttosso()
    {

        return $this->render('exporttosso');
    }


    private function mkdir($dir)
    {
        if (!file_exists($this->PDF_Path)) {
            $old = umask(0000);
            mkdir($this->PDF_Path, 0777, true);
            umask($old);
            return true;
        }

        if (!file_exists($this->PDF_Path . $dir)) {
            $old = umask(0000);
            mkdir($this->PDF_Path . $dir, 0777, true);
            umask($old);
            return true;
        }
    }


    public function actionExporttorevenue()
    {
        $arrCompany = ApiHr::getWorking_company();
        $arrEmp = ApiHr::getempdataall();
        return $this->render('exporttorevenue', [
            'arrCompany' => $arrCompany,
            'arrEmp' => $arrEmp,
        ]);
    }


    public function actionGeneratewht()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $postValue = Yii::$app->request->post();
            $month_pay = $postValue['month_pay'];
            $sign_date = DateTime::DateToMysqlDB($postValue['sign_date']);
            $company_id = $postValue['company_id'];
            $emp_id = $postValue['emp_id'];


            //check and create dir
            $dir = ApiPDF::flip_month($month_pay);
            $this->mkdir($dir);

            $emp_data = ApiPDF::get_emp_data_all();
            $all_company = ApiHr::getWorking_company();
            $wht_data = ApiPDF::get_wht_data($month_pay, $company_id, $emp_id);

//            $emp_data = Empdata::find()->select([
//                'DataNo','Be','Name','Surname','ID_Card',
//                'Address','Moo','Road','Village','SubDistrict','District','Province','Postcode'
//            ])->asArray()->orderBy([
//                'DataNo'=>SORT_ASC
//            ])->all();

            //$emp_data = yii\helpers\ArrayHelper::index($emp,'ID_Card');


        }
        print_r($postValue);
        exit();
    }

    public function actionGeneratepnd1()
    {

    }

    public function actionGeneratetvi50()
    {

    }

    public function actionGeneratepnd1a()
    {

    }


    public function actionExporttocertificate()
    {
        return $this->render('exporttocertificate');
    }

    // public function actionExportpdfcertificate()
    // {

    //     $mpdf = new mPDF('th', 'Tharlon-Regular');
    //     $mpdf->WriteHTML($this->renderPartial('_exporttocertificate'));
    //     $mpdf->Output();
    //     exit;

    // }

    public function actionExportcompany()
    {
        $postValue = Yii::$app->request->post('data');
        /*           echo "<pre>";
                   print_r($postValue);
                  echo "<br>";
                  exit();*/
        $id_type = $postValue['type_data'];
        $id_company = $postValue['selectworking'];
        $month = $postValue['selectmonth'];
        $year = $postValue['selectyear'];
        $sso_date = $postValue['ssoDate'];
        ApiSSOExport::update($id_company, $month, $year, $sso_date);
        ApiSSOExport::type_data($id_type);
        $checknull = ApiSSOExport::find_company($id_company, $month, $year);


        if ($checknull == 0) {
         //   throw  new \Exception('ไม่มี บริษัท');
            echo 5;
            exit();
        }
        ApiSSOExport::find_detail($id_company);
        ApiSSOExport::builddata_head1($id_company, $month, $year);
        ApiSSOExport::builddata1($id_company);
        ApiSSOExport::builddata_head2($id_company, $month, $year);
        ApiSSOExport::builddata2($id_company);
        ApiSSOExport::setpath($id_company, $month, $year);
        $filepart = ApiSSOExport::writefile($id_company, $id_type, $month, $year);
        $checkstructure = ApiSSOExport::checkstructure($filepart);
        if ($checkstructure == 0) {
            return 0;
        }
        return $filepart;
    }

    public function actionDownloadfile()
    {
        $postValue = Yii::$app->request->get('data');
        $postValue = Json::decode($postValue, true);
        $id_type = $postValue['type_data'];
        $id_company = $postValue['selectworking'];
        $month = $postValue['selectmonth'];
        $year = $postValue['selectyear'];
        ApiSSOExport::download($id_company, $month, $year);
    }


    // public function actionPredata()
    // {
    //     // $for_month = date('m');
    //     // $for_year = date('Y');
    //     $for_month = '04';
    //     $for_year = '2014';
    //     $company_id = '43';
    //     $this->getdata($for_month,$for_year,$company_id);
    // }
    // public function getdata($month,$year,$branch)
    // {
    //     $objModelHead = new SsoPaidHead();
    //     $objModelDetail = new SsoPaidDetail();
    //     $objApiPreexportSSO = new ApiPreexportSSO();
    //     $arrCompany = $arrWagehistory = [];
    //     $arr = [];
    //     $objApiPreexportSSO->setCompany();
    //     $dataCompany = $objApiPreexportSSO->getCompany($branch);
    //     $objApiPreexportSSO->setWagehistory($branch,$month,$year);
    //     $dataWagehistory = $objApiPreexportSSO->getWagehistory($branch);
    //     $objApiPreexportSSO->setadddeducthistiry($branch,$month,$year);
    //     $dataadddeducthistiry = $objApiPreexportSSO->getadddeducthistiry($dataWagehistory['WAGE_EMP_ID']);
    //     $arr['sso_paid_head']='0';
    //     $arr['for_month']='0';
    //     $arr['for_year']='0';
    //     $arr['RECORD_TYPE']='1';
    //     $arr['ACC_NO']= str_replace("-","",$dataCompany['soc_acc_number']);
    //     $arr['BRANCH_NO']=$branch;
    //     $arr['PAID_PERIOD']=$month.substr($year,2,2);
    //     $arr['RATE']= ApiHr::getOrgMasterConfig()->getSavingRate();
    //     $arr['TOTAL_EMPLOYEE']=str_pad($dataadddeducthistiry['TOTAL_EMPLOYEE'],6,"0",STR_PAD_LEFT);
    //     $arr['TOTAL_WAGES']=str_pad($dataadddeducthistiry['TOTAL_WAGES'],15,"0",STR_PAD_LEFT);
    //     $arr['TOTAL_PAID']=str_pad($dataadddeducthistiry['TOTAL_PAID'],14,"0",STR_PAD_LEFT);
    //     $arr['TOTAL_PAID_BY_EMPLOYEE']=str_pad($dataadddeducthistiry['TOTAL_PAID_BY_EMPLOYEE'],12,"0",STR_PAD_LEFT);
    //     $arr['TOTAL_PAID_BY_EMPLOYER']=str_pad($dataadddeducthistiry['TOTAL_PAID_BY_EMPLOYER'],12,"0",STR_PAD_LEFT);
    //     $arr['YEARS']=$year;
    //     $arr['MONTHS']=$month;
    //     $arr['DEPARTMENT_ID']='0';
    //     $arr['COMPANY_ID']=$branch;
    //     $arr['PAID_DATE']='';
    //     $arr['COMPANY_NAME']=$dataCompany ['name'];
    //     $head = ApiPreexportSSO::InseartHead($arr);
    //     $detail = $objApiPreexportSSO->getadddeducthistiryASdetail();
    //     $arrDetail = [];
    //     foreach($detail  as $item){
    //         $type = '';
    //         $dataEmp = $objApiPreexportSSO->getWagehistoryByEmp($item['ADD_DEDUCT_THIS_MONTH_EMP_ID']);
    //         //003=นาย,004=นางสาว,005=นาง
    //         if($dataEmp['Be']=='นาย'){
    //             $type = '003';
    //         }
    //         if($dataEmp['Be']=='นาง'){
    //             $type = '005';
    //         }
    //         if($dataEmp['Be']=='นางสาว'){
    //             $type = '004';
    //         }
    //         $arrDetail['ID_Header'] = $head;
    //         $arrDetail['RECORD_TYPE'] = '2';
    //         $arrDetail['SSO_ID'] = $item['ADD_DEDUCT_THIS_MONTH_EMP_ID'];
    //         $arrDetail['PREFIX'] = $type;
    //         $arrDetail['FNAME'] = $dataEmp['Name'];
    //         $arrDetail['LNAME'] = $dataEmp['Surname'];
    //         $arrDetail['WAGES'] = str_pad(str_replace(".","",$dataEmp['WAGE_SALARY']),14,"0",STR_PAD_LEFT);
    //         $arrDetail['PAID_AMOUNT'] = str_pad(str_replace(".","",$dataEmp['WAGE_TOTAL_DEDUCTION']),12,"0",STR_PAD_LEFT);
    //         $arrDetail['BLANK'] = $item[''];
    //         $arrDetail['COMPANY_ID']  = $branch;
    //         if($dataEmp != null){
    //             ApiPreexportSSO::InseartDetail($arrDetail);
    //         }
    //     }
    // }
    // public function actionSamplepdf() {
    //     $id_company = '43';
    //     $y='2014';
    //     $m='4';
    //     $data = ApiSSOExport::sps10($id_company,$y,$m);
    //     $company = ApiSSOExport::getcompany($id_company);
    //     $mpdf = new mPDF('th', 'Tharlon-Regular');
    //     $stylesheet = file_get_contents('/media/lt0109/source1/htdoc/wseasyerp/css/hr/SSOexpoet/ssoexport.css');
    //     $mpdf->AddPage('L');
    //     $mpdf->WriteHTML($this->renderPartial('_reportView',['data'=>$data,'company'=>$company]));
    //     $mpdf->AddPage('P');
    //     $mpdf->WriteHTML($stylesheet,1);
    //     $detaildata = ApiSSOExport::find_detail($id_company);
    //     $mpdf->WriteHTML($this->renderPartial('_reportViewssodetail',['data'=>$detaildata,'company'=>$company,'dataH'=>$data]));
    //     $mpdf->Output();
    //     exit;
    // }
    // public function actionTvi50pdf() {

    //     $modeldata=ApiSSOExport::addducthestory('1579900499011','10-2016');
    //     $modelEmp = ApiSSOExport::emp_data('1579900499011');
    //     $modelCompany = ApiSSOExport::getcompany($modelEmp[0]['Working_Company']);
    //     $mpdf = new mPDF('th', 'Tharlon-Regular', 0, '', 15, 15, 5, 8, 8, 8);
    //     //$stylesheet = file_get_contents('/media/lt0109/source1/htdoc/wseasyerp/css/hr/tvi50.css');
    //     //$mpdf->WriteHTML($stylesheet,1);
    //     $mpdf->AddPage('P');
    //     $mpdf->WriteHTML($this->renderPartial('_tvi50',['modeldata'=>$modeldata,'emp_data'=>$modelEmp,'company'=>$modelCompany]));
    //     $mpdf->Output();
    //     exit;
    // }
    // public function actionPnd1pdf() {
    //     $data = ApiSSOExport::getTaxPndHead('4','2017','56');
    //     $dataDetail = ApiSSOExport::getTaxPndDetail($data[0]['id']);
    //     $arrayDetail = ApiSSOExport::manageArray($dataDetail);
    //     $mpdf = new mPDF('th', 'Tharlon-Regular', 0, '', 15, 8, 5, 8, 8, 8);
    //     $stylesheet = file_get_contents(\Yii::$app->basePath.'/css/hr/tvi50.css');
    //     $mpdf->WriteHTML($stylesheet,1);
    //     $mpdf->AddPage('P');
    //     $mpdf->WriteHTML($this->renderPartial('_pnd1',['data'=>$data]));
    //     $mpdf->AddPage();
    //     $mpdf->SetImportUse();
    //     $pagecount = $mpdf->SetSourceFile(\Yii::$app->basePath.'/images/wshr/pnd1_2.pdf');
    //     $tplIdx = $mpdf->ImportPage($pagecount);
    //     $mpdf->UseTemplate($tplIdx);
    //     $tplIdx = $mpdf->ImportPage($pagecount);
    //     $mpdf->UseTemplate($tplIdx);

    //     $datepay = date("d/m/",strtotime($data[0][signature_date])).(date("Y",strtotime($data[0][signature_date]))+543);
    //     $count_page = 1;
    //     $all_page = count($arrayDetail);
    //     foreach($arrayDetail as $item){
    //          $mpdf->AddPage('L');
    //          $mpdf->WriteHTML($this->renderPartial('_pnd1_attachment',['data'=>$item,'date'=>$datepay,'count_page'=>$count_page,'all_page'=>$all_page,'datahead'=>$data]));
    //          $count_page++;
    //     }
    //     $mpdf->Output();
    //     exit;
    // }
    // public function actionPnd1apdf() {
    //     $obj_Api = new ApiSSOExport();
    //     $dataid = ApiSSOExport::idTaxPndHead('2017','56');
    //     $data = ApiSSOExport::getPad1aTaxPndHead('2017','56');
    //     $dataAmount = $obj_Api->getPad1aTaxPndamount($dataid);
    //     $detail = $obj_Api->getDatadetail();
    //     $arrayDetail = ApiSSOExport::manageArray($detail);

    //     $mpdf = new mPDF('th', 'Tharlon-Regular', 0, '', 15, 8, 5, 8, 8, 8);
    //     $stylesheet = file_get_contents(\Yii::$app->basePath.'/css/hr/tvi50.css');
    //     $mpdf->WriteHTML($stylesheet,1);
    //     $mpdf->AddPage('P');
    //     $mpdf->WriteHTML($this->renderPartial('_pnd1a',['data'=>$data,'data_amount'=>$dataAmount]));
    //     $mpdf->AddPage();
    //     $mpdf->SetImportUse();
    //     $pagecount = $mpdf->SetSourceFile(\Yii::$app->basePath.'/images/wshr/pnd1_2a.pdf');
    //     $tplIdx = $mpdf->ImportPage($pagecount);
    //     $mpdf->UseTemplate($tplIdx);
    //     $mpdf->AddPage('L');
    //     $mpdf->WriteHTML($this->renderPartial('_pnd1a_attachment',['arrayDetail'=>$arrayDetail]));
    //     $mpdf->Output();
    //     exit;
    // }
    // public function actionForceDownloadPdf(){
    //     $mpdf=new mPDF();
    //     $mpdf->WriteHTML($this->renderPartial('mpdf'));

    //     $mpdf->Output('MyPDF.pdf', 'D');
    //     exit;
    // }
    public function actionExportexceltobank()
    {
        $postValue = Yii::$app->request->post();
        $arrCompany = $postValue['arrComexport'];
        $dateExport = $postValue['datepayexport'];

        //Create Excel file
        $exportexcel = ApiExportbank::exportExceltobank($arrCompany, $dateExport);

        $result = 0;
        if ($exportexcel) {
            $result = 1;

            //** MOVE DATA TO history **/
            /* ------------------------------------------------------------- */
            /*  (1) ดึงข้อมูลจาก WAGE THIS MONTH to WAGE_HISTORY
            /* ------------------------------------------------------------- */
            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            //$transaction = $connection->beginTransaction();

            $WageThisMonth = Wagethismonth::find()
                ->where(' WAGE_NET_SALARY != 0.00')
                ->andWhere([
                    'IN', 'WAGE_WORKING_COMPANY', $arrCompany
                ])
                ->orderBy('WAGE_WORKING_COMPANY,WAGE_DEPARTMENT_ID,WAGE_SECTION_ID ASC')
                ->asArray()
                ->all();

            $_table = 'WAGE_HISTORY';
            $_arrColumns = WageHistory::getTableSchema()->columnNames;
            $arr_data = [];
            $eff = 0;
            $_pay_date = null;
            foreach ($WageThisMonth as $item) {
                $arr_data[] = [
                    $_arrColumns[0] => $item['WAGE_ID'],
                    $_arrColumns[1] => $item['WAGE_THIS_MONTH_ROW_NUM'],
                    $_arrColumns[2] => $item['WAGE_EMP_ID'],
                    $_arrColumns[3] => $item['WAGE_FIRST_NAME'],
                    $_arrColumns[4] => $item['WAGE_LAST_NAME'],
                    $_arrColumns[5] => $item['WAGE_PAY_DATE'],
                    $_arrColumns[6] => $item['WAGE_POSITION_CODE'],
                    $_arrColumns[7] => $item['WAGE_POSITION_NAME'],
                    $_arrColumns[8] => $item['WAGE_SECTION_ID'],
                    $_arrColumns[9] => $item['WAGE_DEPARTMENT_ID'],
                    $_arrColumns[10] => $item['WAGE_WORKING_COMPANY'],
                    $_arrColumns[11] => $item['WAGE_BANK_NAME'],
                    $_arrColumns[12] => $item['WAGE_ACCOUNT_NUMBER'],
                    $_arrColumns[13] => $item['WAGE_GET_MONEY_TYPE'],
                    $_arrColumns[14] => $item['WAGE_SALARY_CHART'],
                    $_arrColumns[15] => $item['WAGE_SALARY_LEVEL'],
                    $_arrColumns[16] => $item['WAGE_SALARY_STEP'],
                    $_arrColumns[17] => $item['WAGE_SALARY'],
                    $_arrColumns[18] => $item['WAGE_SALARY_BY_CHART'],
                    $_arrColumns[19] => $item['WAGE_TOTAL_ADDITION'],
                    $_arrColumns[20] => $item['WAGE_EARN_PLUS_ADD'],
                    //$_arrColumns[21] => $item['Social_Salary'],
                    $_arrColumns[21] => $item['WAGE_THIS_MONTH_TAX'],
                    $_arrColumns[22] => $item['WAGE_TOTAL_DEDUCTION'],
                    $_arrColumns[23] => $item['WAGE_EARN_MINUS_DEDUCT'],
                    $_arrColumns[24] => $item['WAGE_NET_SALARY'],
                    $_arrColumns[25] => $item['WAGE_THIS_MONTH_CONFIRM'],
                    $_arrColumns[26] => $item['WAGE_THIS_MONTH_CONFIRM_BY'],
                    $_arrColumns[27] => $item['WAGE_THIS_MONTH_CONFIRM_DATE'],
                    $_arrColumns[28] => $item['WAGE_THIS_MONTH_EMPLOYEE_LOCK'],
                    $_arrColumns[29] => $item['WAGE_THIS_MONTH_DIRECTOR_LOCK'],
                    $_arrColumns[30] => $item['WAGE_THIS_MONTH_STATUS'],
                    $_arrColumns[31] => $item['WAGE_THIS_MONTH_BANK_CONFIRM_STATUS'],
                    $_arrColumns[32] => $item['WAGE_THIS_MONTH_BANK_CONFIRM_DATE'],
                    $_arrColumns[33] => $item['WAGE_THIS_MONTH_BANK_CONFIRM_BY'],
                    $_arrColumns[34] => $item['WAGE_REMARK'],
                    $_arrColumns[35] => $item['WAGE_CREATE_DATE'],
                    $_arrColumns[36] => $item['WAGE_CREATE_BY'],
                    $_arrColumns[37] => $item['WAGE_UPDATE_DATE'],
                    $_arrColumns[38] => $item['WAGE_UPDATE_BY'],
                    $_arrColumns[39] => $item['is_endofmonth'],
                    $_arrColumns[40] => $item['cal_times'],
                    $_arrColumns[41] => $item['cal_date'],
                    $_arrColumns[42] => $item['wage_calculate_id'],
                    $_arrColumns[43] => $item['COMPANY_NAME'],
                    $_arrColumns[44] => $item['DEPARTMENT_NAME'],
                    $_arrColumns[45] => $item['SECTION_NAME'],
                    $_arrColumns[46] => $item['DIRECTOR_LOCK_COMFIRM_BY'],
                    $_arrColumns[47] => $item['DIRECTOR_LOCK_COMFIRM_DATE'],
                    $_arrColumns[48] => $item['WTH_AMOUNT'],
                    $_arrColumns[49] => $item['WTH_AMOUNT_TAX'],
                    $_arrColumns[50] => $item['BNF_AMOUNT'],
                    $_arrColumns[51] => $item['PND_TOTAL_INCOME'],
                    $_arrColumns[52] => $item['PND_AMOUNT_REDUCE'],
                    $_arrColumns[53] => $item['PND_AMOUNT_TAX'],
                    $_arrColumns[54] => $item['Social_Salary'],

                ];

                $arrIdcard[$item['WAGE_EMP_ID']] = $item['WAGE_EMP_ID'];
                $_pay_date = $item['WAGE_PAY_DATE'];
            }

            $eff = $connection->createCommand()->batchInsert($_table, $_arrColumns, $arr_data)->execute();

            //** MOVE DATA TO history **/
            /* ------------------------------------------------------------- */
            /*  (2) Move  ADD_DEDUCT_THIS_MONTH  TO ADD_DEDUCT_HISTORY
            /* ------------------------------------------------------------- */

            $DeductTemplate = ApiPayroll::getArrayAdddeductTemplate();
            $AddDeductDetail = Adddeductthismonth::find()->where([
                'ADD_DEDUCT_DETAIL_PAY_DATE' => $_pay_date,
            ])->andWhere([
                'IN', 'ADD_DEDUCT_DETAIL_EMP_ID', $arrIdcard
            ])->asArray()->all();

            $_table = 'ADD_DEDUCT_HISTORY';
            $_arrColumns = Adddeducthistory::getTableSchema()->columnNames;
            $arr_data = [];
            $arrAddDeDuctDetailID = [];
            foreach ($AddDeductDetail as $item) {
                $arrAddDeDuctDetailID[$item['ADD_DEDUCT_DETAIL_ID']] = $item['ADD_DEDUCT_DETAIL_ID'];
                $arrDeductTemplate = $DeductTemplate[$item['ADD_DEDUCT_ID']];
                $arr_data[] = [
                    $_arrColumns[0] => null,
                    $_arrColumns[1] => $item['ADD_DEDUCT_DETAIL_ID'],
                    $_arrColumns[2] => $item['ADD_DEDUCT_DETAIL_EMP_ID'],
                    $_arrColumns[3] => $item['ADD_DEDUCT_DETAIL_PAY_DATE'],
                    $_arrColumns[4] => $item['ADD_DEDUCT_ID'],
                    $_arrColumns[5] => $arrDeductTemplate['ADD_DEDUCT_TEMPLATE_NAME'],
                    $_arrColumns[6] => $item['ADD_DEDUCT_DETAIL_ID'],
                    $_arrColumns[7] => $item['ADD_DEDUCT_DETAIL'],
                    $_arrColumns[8] => $item['ADD_DEDUCT_DETAIL_AMOUNT'],
                    $_arrColumns[9] => $arrDeductTemplate['ADD_DEDUCT_TEMPLATE_TYPE'],
                    $_arrColumns[10] => $item['ADD_DEDUCT_DETAIL_STATUS'],
                    $_arrColumns[11] => 1,
                    $_arrColumns[12] => $item['ADD_DEDUCT_DETAIL_CREATE_DATE'],
                    $_arrColumns[13] => $item['ADD_DEDUCT_DETAIL_CREATE_BY'],
                    $_arrColumns[14] => $item['ADD_DEDUCT_DETAIL_UPDATE_DATE'],
                    $_arrColumns[15] => $item['ADD_DEDUCT_DETAIL_UPDATE_BY'],
                    $_arrColumns[16] => 1,
                    $_arrColumns[17] => null,
                    $_arrColumns[18] => null,
                    $_arrColumns[19] => null,
                ];
            }

            $eff = $connection->createCommand()->batchInsert($_table, $_arrColumns, $arr_data)->execute();


            //** MOVE DATA TO history **/
            /* ------------------------------------------------------------- */
            /*  (3) UPDATE STATUS / Update status ADD_DEDUCT_DETAIL
            /* ------------------------------------------------------------- */

            Adddeductdetail::updateAll([
                'ADD_DEDUCT_DETAIL_HISTORY_STATUS' => 2,
                'ADD_DEDUCT_DETAIL_STATUS' => 1,
                'ADD_DEDUCT_DETAIL_UPDATE_BY' => Yii::$app->session->get('idcard'),
                'ADD_DEDUCT_DETAIL_UPDATE_DATE' => new yii\db\Expression('NOW()'),
            ], [
                'IN', 'ADD_DEDUCT_DETAIL_ID', $arrAddDeDuctDetailID
            ]);


            /* ------------------------------------------------------------- */
            /*  (4) Clear Table ADD_DEDUCT_THIS_MONTH / WAGE_THIS_MONTH
            /* ------------------------------------------------------------- */
            $connection = \Yii::$app->dbERP_easyhr_PAYROLL;
            $connection->createCommand()->truncateTable('ADD_DEDUCT_THIS_MONTH')->execute();
            $connection->createCommand()->truncateTable('WAGE_THIS_MONTH')->execute();

        }

        return $result;
    }

    public function actionDownloadtobank()
    {
        $getValue = Yii::$app->request->get();
        //$arrCompany = $getValue['arrComexport'];
        $dt = $getValue['datepayexport'];

        $dateExport = str_replace(['/', '-'], '', $dt);
        $basePath = Yii::$app->basePath;
        $zipNamefile = "Salary_" . $dateExport;
        $destination = $basePath . "/upload/exportsalaryzip/" . $zipNamefile . ".zip";
        $source = $basePath . "/upload/exportsalary/";
        $arrNamefile = [];


        $cdir = scandir($source);
        foreach ($cdir as $key => $values) {
            if (!in_array($values, array(".", ".."))) {
                if (is_dir($source . DIRECTORY_SEPARATOR . $values)) {
                    $arrNamefile[$values] = dirToArray($source . DIRECTORY_SEPARATOR . $values);
                } else {
                    $arrNamefile[] = $values;
                }
            }
        }

        $zip = new ZipArchive;
        if ($zip->open($destination, ZipArchive::CREATE) === TRUE) {
            foreach ($arrNamefile as $key => $value) {
                // $PATH = $source.$value.'.xlsx';
                $PATH = $source . $value;
                $zip->addFile($PATH, basename($PATH));
            }
            $zip->close();

            $modelsendzipfile = ApiExportbank::sendzipfilesalary($zipNamefile, $dateExport);
            //downloaded complete and remove file
            $files = glob($source . '*.xls'); //get all file names
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file); //delete file
            }
        }

    }


    //public function actionConfirmexporttobank()
    //{
    //$postValue = Yii::$app->request->post();
    //$date = date("Y-m-d");
    //$arrCom = $postValue['arrCom'];
    //           print_r($arrCom);
    //           exit;
    //$checkConfirmstatus = ApiExportbank::getconfrimexportexcel($date, $arrCom); ////เช็คคอนเฟริม
    //$moveToWagehistory = ApiSalary::moveWageThismountToHistory($arrCom); //ย้ายไป ทั้งสอง wagehis addeduchis

    // $insertTextToExportSSO ;//insertsso

    //2ค้ำประกันและภาษีตัดออกย้ายไปสร้างเงินเดือน

    //return true;
    //}

    public function actionSlippdf()
    {
        $session = Yii::$app->session;
        $idcard = $session->get('idcard');
        $data_get = Yii::$app->request->get('data');
        //$mpdf = new mPDF('th', 'Tharlon-Regular', 0, '', 15, 8, 5, 8, 8, 8);
        //$mpdf = new mPDF();
        $mpdf = new \Mpdf\Mpdf();
        //$stylesheet = file_get_contents(\Yii::$app->basePath.'/css/hr/tvi50.css');
        //$mpdf->WriteHTML($stylesheet,1);
        $db = DBConnect::getDBConn();
        $sql_config = "SELECT * FROM `payroll_config_template`";
        $data_config = Yii::$app
            ->dbERP_easyhr_PAYROLL
            ->createCommand($sql_config)
            ->queryAll();
        //----------------------------------------------------------------------------------------------------------------------------

        $sql_summary = "SELECT *  FROM summary_money_wage  where emp_idcard  =  '$idcard'  AND  pay_date = '$data_get' ";
        $data_summary = Yii::$app
            ->dbERP_easyhr_PAYROLL
            ->createCommand($sql_summary)
            ->queryAll();

        //----------------------------------------------------------------------------------------------------------------------------

        $sql = "SELECT wh.*,wc.name as wc_name,dm.name as dm_name,ss.name as ss_name FROM WAGE_HISTORY as wh ";
        $sql .= "INNER JOIN " . $db['ou'] . ".working_company as wc on wc.id = wh.WAGE_WORKING_COMPANY ";
        $sql .= "INNER JOIN " . $db['ou'] . ".department as dm on dm.id = wh.WAGE_DEPARTMENT_ID ";
        $sql .= "INNER JOIN " . $db['ou'] . ".section as ss on ss.id = wh.WAGE_SECTION_ID ";
        $sql .= "where wh.WAGE_EMP_ID = '$idcard' AND wh.WAGE_PAY_DATE ='" . $data_get . "' ";
        $sql .= "group by wh.WAGE_EMP_ID = '$idcard' AND wh.WAGE_PAY_DATE ='" . $data_get . "' ";
        $data_WAGE_HISTORY = Yii::$app
            ->dbERP_easyhr_PAYROLL
            ->createCommand($sql)
            ->queryAll();

        //----------------------------------------------------------------------------------------------------------------------------

        $sql_ADD_DEDUCT = "SELECT * FROM ADD_DEDUCT_HISTORY where ADD_DEDUCT_THIS_MONTH_PAY_DATE = '$data_get' AND ADD_DEDUCT_THIS_MONTH_EMP_ID = '$idcard'";
        $data_ADD_DEDUCT_HISTORY = Yii::$app
            ->dbERP_easyhr_PAYROLL
            ->createCommand($sql_ADD_DEDUCT)
            ->queryAll();

        //----------------------------------------------------------------------------------------------------------------------------

        $sql_totle = "SELECT sum(IF(ADD_DEDUCT_THIS_MONTH_TMP_ID=" . $data_config[0]['sso_template_id'] . ",ADD_DEDUCT_THIS_MONTH_AMOUNT,0)) as sso_totle, ";
        $sql_totle .= "sum(IF(ADD_DEDUCT_THIS_MONTH_TMP_ID=" . $data_config[0]['taxincome_template_id'] . ",ADD_DEDUCT_THIS_MONTH_AMOUNT,0)) as tax_totle ";
        $sql_totle .= "FROM ADD_DEDUCT_HISTORY where ADD_DEDUCT_THIS_MONTH_PAY_DATE LIKE '%" . explode("-", $data_get)[1] . "' AND ADD_DEDUCT_THIS_MONTH_EMP_ID = '$idcard' ";
        $sql_totle .= "AND CAST(SUBSTRING(ADD_DEDUCT_THIS_MONTH_PAY_DATE, 1, 2) as int) <= " . (int)explode("-", $data_get)[0] . "";

        $data_ADD_DEDUCT_HISTORY_TOTLE = Yii::$app
            ->dbERP_easyhr_PAYROLL
            ->createCommand($sql_totle)
            ->queryAll();

        //----------------------------------------------------------------------------------------------------------------------------

        $sql_emp = "SELECT CONCAT(Be,' ',Name,' ',Surname) as full_name FROM `emp_data` WHERE ID_Card=" . $idcard;
        $data_Emp = Yii::$app
            ->dbERP_easyhr_checktime
            ->createCommand($sql_emp)
            ->queryAll();

        //----------------------------------------------------------------------------------------------------------------------------

        $sql_wage_totle = "SELECT SUM(WAGE_NET_SALARY) as wage_totle FROM WAGE_HISTORY ";
        $sql_wage_totle .= "where WAGE_EMP_ID = '$idcard' AND WAGE_PAY_DATE LIKE '%" . explode("-", $data_get)[1] . "' AND SUBSTRING(WAGE_PAY_DATE, 1, 2) <= " . explode("-", $data_get)[0] . " ";
        $data_wage_totle = Yii::$app
            ->dbERP_easyhr_PAYROLL
            ->createCommand($sql_wage_totle)
            ->queryAll();

        //----------------------------------------------------------------------------------------------------------------------------

        $sql_BENEFIT = "SELECT * FROM `BENEFIT_FUND` where BENEFIT_EMP_ID = '$idcard' AND BENEFIT_SAVING_DATE = '$data_get'";
        $data_BENEFIT_FUND = Yii::$app
            ->dbERP_easyhr_PAYROLL
            ->createCommand($sql_BENEFIT)
            ->queryAll();

        //----------------------------------------------------------------------------------------------------------------------------

        $data_ADD_DEDUCT_HISTORY_TOTLE[0] = array_merge($data_ADD_DEDUCT_HISTORY_TOTLE[0], $data_wage_totle[0]);

        $ADDITION = [];
        $DEDUCTION = [];
        foreach ($data_ADD_DEDUCT_HISTORY as $value) {
            if ($value['ADD_DEDUCT_THIS_MONTH_TYPE'] == '1') {
                $ADDITION[] = $value;
            } else {
                $DEDUCTION[] = $value;
            }
        }

        $mpdf->title = 'สลิปเงินเดือน : ' . $data_get;
        $mpdf->AddPage('P');
        $mpdf->WriteHTML($this->renderPartial('_slip', ['data' => $data_WAGE_HISTORY, 'data_emp' => $data_Emp, 'data_summary' => $data_summary, 'ADDITION' => $ADDITION, 'DEDUCTION' => $DEDUCTION, 'date' => $data_get, 'BENEFIT_FUND' => $data_BENEFIT_FUND, 'data_totle' => $data_ADD_DEDUCT_HISTORY_TOTLE]));
        // $mpdf->AddPage();
        // //$mpdf->SetImportUse();
        // // $pagecount = $mpdf->SetSourceFile(\Yii::$app->basePath.'/images/wshr/pnd1_2.pdf');
        // // $tplIdx = $mpdf->ImportPage($pagecount);
        // // $mpdf->UseTemplate($tplIdx);
        // // $tplIdx = $mpdf->ImportPage($pagecount);
        // //$mpdf->UseTemplate($tplIdx);

        // $datepay = date("d/m/",strtotime($data[0][signature_date])).(date("Y",strtotime($data[0][signature_date]))+543);
        // $count_page = 1;
        // $all_page = count($arrayDetail);
        // foreach($arrayDetail as $item){
        //      $mpdf->AddPage('L');
        //      $mpdf->WriteHTML($this->renderPartial('_pnd1_attachment',['data'=>$item,'date'=>$datepay,'count_page'=>$count_page,'all_page'=>$all_page,'datahead'=>$data]));
        //      $count_page++;
        // }
        $mpdf->Output();
        exit;
    }

    public function actionGetreportsalarythismonth()
    {
        $postValue = Yii::$app->request->post();

        $Workingreport = $postValue['Workingreport'];
        $arrIdtemp = $postValue['arrIdtemp'];
        $monthselectreport = $postValue['monthselectreport'];
        $dateselectreport = $postValue['dateselectreport'];
        $modelReportSalary = ApiExportbank::getreportwagethismonth($Workingreport, $monthselectreport);
        $modelReportWorkingcompany = ApiHr::getworkingcompanyByid($Workingreport);
        $modelReportTemp = ApiExportbank::gettempthismonth($arrIdtemp);
        $arrIdcard = [];
        foreach ($modelReportSalary as $value) {
            $arrIdcard[] = $value['WAGE_EMP_ID'];
        }
        $modelReportAdddeducthismonth = ApiExportbank::getreportadddeductthismonth($arrIdcard, $arrIdtemp);
        $modelReportGetEmp = ApiHr::getempdataInidcard($arrIdcard);
        // echo "<pre>";
        // print_r($modelReportSalary);
        // exit;
        return $this->render('exporttobank', ['modelReportSalary' => $modelReportSalary,
            'modelReportWorkingcompany' => $modelReportWorkingcompany,
            'modelReportTemp' => $modelReportTemp,
            'modelReportAdddeducthismonth' => $modelReportAdddeducthismonth,
            'modelReportGetEmp' => $modelReportGetEmp,
            'activeShow' => true,]);
    }

    public function printobj($id) //print_r
    {
        echo '<pre>';
        print_r($id);
        echo '</pre>';
        //exit();

    }

    public function actionExporttextfilepnd()
    {
        $postValue = Yii::$app->request->post();

        $month_paypnd = (string)$postValue['month_paypnd'];
        $sso_date = $postValue['sso_date'];
        $company_idpnd = $postValue['company_idpnd'];


        $month = ApiPDF::flip_monthpnd($month_paypnd);
        $year = ApiPDF::flip_yearpnd($month_paypnd);
        $datemonth = ApiPDF::flip_datepnd($sso_date);


        $model1 = ApiPDF::pdf_pnd1($company_idpnd, $month_paypnd, 'all');


        $pay_date = [];
        $emp_idcard1 = [];
        foreach ($model1 as $row3) {

            $pay_date[$row3['pay_date']] = $row3['pay_date'];
            $emp_idcard1[$row3['emp_idcard']] = $row3['emp_idcard'];
        }

        $model = SummaryMoneyWage::find()
            ->select('*')
            ->from('summary_money_wage')
            ->innerJoin('tax_calculate', 'tax_calculate.emp_idcard = summary_money_wage.emp_idcard')
            ->where([
                'summary_money_wage.emp_idcard' => $emp_idcard1,
                'summary_money_wage.pay_date' => $pay_date,


            ])
            ->andWhere('tax_calculate.total_tax_month > 0 ')
            ->asArray()
            ->all();


        $all_company = ApiHr::getWorking_company();
        $arryItem = [];
        $arryItem2 = [];
        $total_monthly = [];
        $emp_data = ApiPDF::get_emp_data_all();
        foreach ($model as $row) {

            $arryItem[$row['tax_amount']] = $row['tax_amount'];
            $total_monthly[$row['income_amount']] = $row['income_amount'];
            $company_data = $all_company[$row['company_id']];
            $day = $row['months'];
            $arryItem2[$row['emp_idcard']] = $row['emp_idcard'];


        }


        ///เดรียมข้อมูล
        $FORMTYPE = '00';
        $COMPIN = $company_data['inv_number'];
        $COMTIN = '0000000000';
        $BRANO = '0000';
        // $PIN = $arryItem2;
        $TIN = '0000000000';
        $INCOMECODE = 1;
        $TAXRATE = '0';
        $TAXCONDITION = 1;
        $address2 = '';
        $arr_companypnd = array();

        foreach ($model as $k => $v) {
            $_emp_data = $emp_data[$v['emp_idcard']];
            $company_data = $all_company[$v['company_id']];


            if ($_emp_data['Postcode'] == 'ไม่ระ') {
                $Postcode = ' ';
            } else {
                $Postcode = $_emp_data['Postcode'];
            }


            $searchingPNDRECORD = PNDRECORD::find()
                ->where([
                    //  'working_company_id' => $company_data['id'],
                    'TAXMONTH' => $month,
                    'TAXYEAR' => $year,
                    'PIN' => $_emp_data['string_idcard'],
                ])
                ->asArray()->all();

            if (!empty($searchingPNDRECORD)) {
                return false;
            } else {
                // self::printobj($_emp_data['string_idcard']);//1

                $modelsinsert = new PNDRECORD();
                $modelsinsert->id = null;
                $modelsinsert->FORMTYPE = $FORMTYPE;
                $modelsinsert->COMPIN = $COMPIN;
                $modelsinsert->COMTIN = $COMTIN;
                $modelsinsert->BRANO = $BRANO;
                $modelsinsert->PIN = $_emp_data['string_idcard'];
                $modelsinsert->TIN = $TIN;
                $modelsinsert->PER_N1 = $_emp_data['Be'];
                $modelsinsert->NAME1 = $_emp_data['Name'];
                $modelsinsert->SUR_N1 = $_emp_data['Surname'];
                $modelsinsert->ADDRESS1 = $_emp_data['address2'];
                $modelsinsert->ADDRESS2 = $address2;
                $modelsinsert->POSCOD = $Postcode;
                $modelsinsert->TAXMONTH = $month;
                $modelsinsert->TAXYEAR = (string)$year;
                $modelsinsert->INCOMECODE = (string)$INCOMECODE;
                $modelsinsert->PAYDATE = (string)$datemonth;
                $modelsinsert->TAXRATE = $TAXRATE;
                $modelsinsert->PAYMENT = $v['income_amount'];
                $modelsinsert->TAX = $v['tax_amount'];
                $modelsinsert->TAXCONDITION = (string)$TAXCONDITION;
                $modelsinsert->working_company_id = $company_data['id'];
                $modelsinsert->isNewRecord = true;
//                if ($modelsinsert->save(false) !== false) {
//                    echo true;
//                } else {
//                    $errors = $modelsinsert->errors;
//                    echo "model can't validater <pre>";
//                    print_r($errors);
//                    echo "</pre>";
//                }
//                $modelsinsert->save();
                //   self::printobj($modelsinsert);//1

            }
            $showpndcompne = $company_data['id'];
            $showpnd[$showpndcompne][$_emp_data['string_idcard']] = $FORMTYPE .
                '|' . $COMPIN .
                '|' . $COMTIN .
                '|' . $BRANO .
                '|' . $_emp_data['string_idcard'] .
                '|' . $TIN .
                '|' . $_emp_data['Be'] .
                '|' . $_emp_data['Name'] .
                '|' . $_emp_data['Surname'] .
                '|' . $_emp_data['address2'] .
                '|' . $address2 .
                '|' . $Postcode .
                '|' . $month .
                '|' . $year .
                '|' . $INCOMECODE .
                '|' . $datemonth .
                '|' . $TAXRATE .
                '|' . $v['income_amount'] .
                '|' . $v['tax_amount'] .
                '|' . $TAXCONDITION .
                "\n";

        }


            //array_push($arr_companypnd, $showpnd);
//self::printobj($showpnd);
//self::printobj($showpnd);
//
            //$nametxt = $showpndcompne.'-'.$_emp_data['string_idcard'].'-'.$month_paypnd. '.txt' ;
        $dir = 'upload/downloadpnd/'.$month_paypnd;
        if (!file_exists($dir)) {
            mkdir($dir,0777, true);
        }

        foreach ($showpnd as $k => $v){
           // self::printobj($k);

            //$information_head = implode("", $v);
            $path = Yii::$app->basePath . '/upload/downloadpnd/'.$month_paypnd.'/PND-'.$k.'.txt' ;
            $handle = fopen($path, 'wd');
            foreach ($v as $str) {
                //$str = $information_head;
                fwrite($handle, $str);
            }

            fclose($handle);


        }

        $paths = $this->Zipping($month_paypnd);
        echo $paths;
       // self::printobj(count($showpnd));
        exit;

            $dir = 'upload/downloadpnd/' . $month_paypnd;
            if (!file_exists($dir)) {
                mkdir($dir, 0777,true);
            }
            $information_head = implode("", $showpnd);
            $path = Yii::$app->basePath . '/upload/downloadpnd/'.$month_paypnd.'/'.$nametxt;
            $handle = fopen($path, 'wd');
            $str = $information_head;
            fwrite($handle, $str);
            fclose($handle);

          //  self::printobj($nametxt);
       // }
     //   self::printobj($month_paypnd);
       // $nametxt = $arr_company.'-'.$month_paypnd ;

//        $information_head = implode("", $arr_companypnd);
//        $path = Yii::$app->basePath . '/upload/downloadpnd/' .$month_paypnd. '.txt';
//        $handle = fopen($path, 'wd');
//        $str = $information_head;
//        fwrite($handle, $str);
//        fclose($handle);
     //   $paths = $this->Zipping($month_paypnd);
      //  echo $paths;

    }

    private function Zipping($month_paypnd)
    {
        $basePath = Yii::$app->basePath;

        $zipNamefile = $month_paypnd;
        $destination = $basePath . "/upload/downloadpnd/" . $zipNamefile . ".zip";

        $source = $basePath . "/upload/downloadpnd";

        $rootPath = realpath($source);


      //  $destination2 = $basePath . "/upload/downloadpnd";


        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($destination, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Initialize empty "delete list"
        $filesToDelete = array();

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );


        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);

                // Add current file to "delete list"
                // delete it later cause ZipArchive create archive only after calling close function and ZipArchive lock files until archive created)
                if ($file->getFilename() != 'important.txt') {
                    $filesToDelete[] = $filePath;
                }
            }
        }

        // Zip archive will be created only after closing object


        $zip->close();



        foreach ($filesToDelete as $file) {

              unlink($file);//gendelet all

        }

        $paths = Yii::$app->request->baseUrl . '/upload/downloadpnd/'.$month_paypnd.'.zip';

        return  $paths;

    }
    public function actionUnfileexporttextfilepnd()
    {
        $month_paypnd = Yii::$app->request->post();
        $basePath = Yii::$app->basePath;



        foreach ($month_paypnd as $k => $v){
            $t = explode('_zip', $k);
          $name = $t[0].'.zip';
            $un = explode('/', $name);

            $destination = $basePath . "/upload/downloadpnd/".$un[4];

            unlink($destination);
        }
    }





}