<?php
/* @var $this yii\web\View */
use app\modules\hr\apihr\ApiHr;


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use app\bundle\AppAsset;
use app\api\Helper;
use app\api\DateTime;
use app\api\Utility;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

//use app\api\AjaxSubmitButton;

$imghr = Yii::$app->request->baseUrl . '/images/wshr';
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/validator.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/global/global.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/savebtitel.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/savebbloodgroup.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/savebstatuspersonal.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/savebstatuseducation.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/savebleveleducation.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/savebdegreeeducation.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/master/saveinstitutioneducation.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //activity
?>


<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">เครื่องมือและการตั้งค่า</a>
                </li>
                <li>ตั้งค่าข้อมูลองค์กร</li>
                <li class="active">ตั้งค่าข้อมูลพื้ยฐานส่วนบุคคล</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active tabselect" id="tabselect1"><a href="#tab1" data-toggle="tab">ข้อมูลคำนำหน้า</a></li>
                        <li class="tabselect" id="tabselect2"><a href="#tab2" data-toggle="tab">ข้อมูลกลุ่มเลือด</a></li>
                        <li class="tabselect" id="tabselect3"><a href="#tab3" data-toggle="tab">ข้อมูลสถานะภาพการสมรส</a></li>
                        <li class="tabselect" id="tabselect4"><a href="#tab4" data-toggle="tab">ข้อมูลสถานะทางการศึกษา</a></li>
                        <li class="tabselect" id="tabselect5"><a href="#tab5" data-toggle="tab">ข้อมูลระดับการศึกษา</a></li>
                        <li class="tabselect" id="tabselect6"><a href="#tab6" data-toggle="tab">ข้อมูลวุฒิการศึกษา</a></li>
                        <li class="tabselect" id="tabselect7"><a href="#tab7" data-toggle="tab">ข้อมูลสถานศึกษา</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active tabselect" id="tab1">
                            <div class="row">
                               <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmBtitel',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลคำนำหน้า</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewBtitle',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลคำนำหน้า ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                    <form id="frmBtitle" onsubmit="return getdatesubmit1();" data-toggle="validator" method="post" >
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>เพศ <span>*</span></label>
                                                <input id="gender1" name="gender" data-required="true"  type="radio" value="1">ชาย
                                                <input id="gender2" name="gender" data-required="true"  type="radio" value="2">หญิง
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           <div class="box-body">
                                                <label>เลือกภาษา<span>*</span></label>
                                                <input id="language1" name="language" data-required="true" class="language" onclick="selectlanguage(1)"  type="radio" value="1" required="true">ไทย
                                                <input id="language2" name="language" data-required="true" class="language" onclick="selectlanguage(2)" type="radio" value="2" required="true">อังกฤษ
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>คำนำหน้า(ภาษาไทย)<span>*</span></label>
                                                <input id="title_name_th" name="title_name_th" data-required="true"
                                                class="form-control" type="text"  required="true" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>คำนำหน้า(ภาษาอังกฤษ)<span>*</span></label>
                                                <input id="title_name_en" name="title_name_en" data-required="true"
                                                class="form-control" type="text"  required="true" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control status_active" id="status_active"
                                                        name="status_active">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <?php echo Html::hiddenInput('hide_activitybtitel', null, ['id' => 'hide_activitybtitel']); ?>
                                                <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php 
                                        Pjax::begin(['id' => 'pjax_tb_betitel']);
                                        echo GridView::widget([
                                            'id'=>'G1',
                                            'dataProvider' => $BtitleProvider,
                                            'filterModel' => $BtitleSearch,
                                            //'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                            'columns' => [
                                                
                                                [
                                                    'class' =>'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '20px'],
                                                ],
                                                [
                                                    'attribute' => 'title_name_th',
                                                    'label' => 'คำนำหน้าภาษาไทย',
                                                    //'value' => 'title_name_th',
                                                    'value' => function ($data) {
                                                        if($data->title_name_th){
                                                            return $data->title_name_th;
                                                        }else{
                                                            return '';
                                                        }
                                                    },
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'title_name_en',
                                                    'label' => 'คำนำหน้าภาษาอังกฤษ',
                                                    //'value' => 'title_name_en',
                                                    'value' => function ($data) {
                                                        if($data->title_name_en){
                                                            return $data->title_name_en;
                                                        }else{
                                                            return '';
                                                        }
                                                    },
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'status_active',
                                                    'label' => 'สถานะการใช้งาน',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5px']
                                                ],
                                                 [
                                                    'attribute' => 'gender',
                                                    'label' => 'เพศ',
                                                    'value' => function ($data) {
                                                        if($data->gender == Yii::$app->params['MALE']){
                                                            return "ชาย";
                                                        }else{
                                                            return "หญิง";
                                                        }
                                                       
                                                    },
                                                    'contentOptions' => ['style' => 'width:20px'],
                                                    'filter' => false,
                                                ],
                                                [
                                                    'attribute' => 'createby_user',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                        //print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                               [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => 'create_datetime',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                        'headerOptions' => ['width' => '100'],
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{update}  &nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        editbtitel(' . $data->id. ');
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->title_name_th.$data->title_name_en . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletebtitel(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                    ],

                                            ]
                                        ]);
                                        Pjax::end();
                                    ?>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="row">
                               <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmBbloodgroup',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลการหมู่เลือด</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewBloodgroup',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลการหมู่เลือด ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                    <form id="frmBbloodgroup" onsubmit="return getdatesubmit2();" data-toggle="validator" method="post" >
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>หมู่เลือด<span>*</span></label>
                                                <input id="bloodgroup" name="bloodgroup" data-required="true"
                                                class="form-control" type="text"  required="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control status_active" id="status_active2"
                                                        name="status_active">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <?php echo Html::hiddenInput('hide_activitybtitel', null, ['id' => 'hide_activitybtitel2']); ?>
                                                <button type="submit"  class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                  <?php 
                                        Pjax::begin(['id' => 'pjax_tb_bbloodgroup']);
                                        echo GridView::widget([
                                            'dataProvider' => $BbloodgroupProvider,
                                            'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                            'columns' => [
                                                
                                                [
                                                    'class' =>'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '20px'],
                                                ],
                                                [
                                                    'attribute' => 'bloodgroup',
                                                    'label' => 'หมู่เลือด',
                                                    'value' => 'bloodgroup',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'status_active',
                                                    'label' => 'สถานะการใช้งาน',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5px']
                                                ],
                                                [
                                                    'attribute' => 'createby_user',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                        //print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                               [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => 'create_datetime',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                        'headerOptions' => ['width' => '100'],
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{update}  &nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        editbloodgroup(' . $data->id. ');
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->bloodgroup . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletebloodgroup(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                    ],

                                            ]
                                        ]);
                                        Pjax::end();
                                    ?> 
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="row">
                               <div class="pull-right" style="padding-right: 15px;">
                                     <?php
                                    Modal::begin([
                                        'id' => 'modalfrmStatuspersonal',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลสถานะภาพการสมรส</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewStatuspersonal',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลสถานะภาพการสมรส',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                    <form id="frmStatuspersonal" onsubmit="return getdatesubmit3();" data-toggle="validator" method="post" >
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะภาพการสมรส<span>*</span></label>
                                                <input id="status_personal" name="status_personal" data-required="true"
                                                class="form-control" type="text"  required="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control status_active" id="status_active3"
                                                        name="status_active">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <?php echo Html::hiddenInput('hide_activitybtitel', null, ['id' => 'hide_activitybtitel3']); ?>
                                                <button type="submit"  class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                   <?php 
                                        Pjax::begin(['id' => 'pjax_tb_bstatuspersonal']);
                                        echo GridView::widget([
                                            'id'=>'G3',
                                            'dataProvider' => $BstatuspersonalProvider,
                                            'filterModel' => $BstatuspersonalSearch,
                                            'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                            'columns' => [
                                                
                                                [
                                                    'class' =>'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '20px'],
                                                ],
                                                [
                                                    'attribute' => 'status_personal',
                                                    'label' => 'ข้อมูลสถานะภาพการสมรส',
                                                    'value' => 'status_personal',
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'status_active',
                                                    'label' => 'สถานะการใช้งาน',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5px']
                                                ],
                                                [
                                                    'attribute' => 'createby_user',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                        //print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                               [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => 'create_datetime',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                        'headerOptions' => ['width' => '100'],
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{update}  &nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        editstatuspersonal(' . $data->id. ');
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->status_personal . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletestatuspersonal(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                    ],

                                            ]
                                        ]);
                                        Pjax::end();
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4">
                            <div class="row">
                               <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmStatuseducation',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลสถานะทางการศึกษา</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewStatuseducation',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลสถานะทางการศึกษา',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                     <form id="frmBstatuseducation" onsubmit="return getdatesubmit4();" data-toggle="validator" method="post" >
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะทางการศึกษา<span>*</span></label>
                                                <input id="status_education" name="status_education" data-required="true"
                                                class="form-control" type="text"  required="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control status_active" id="status_active4"
                                                        name="status_active">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <?php echo Html::hiddenInput('hide_activitybtitel', null, ['id' => 'hide_activitybtitel4']); ?>
                                                <button type="submit"  class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                      <?php 
                                        Pjax::begin(['id' => 'pjax_tb_bstatuseducation']);
                                        echo GridView::widget([
                                            'dataProvider' => $BStatuseducationProvider,
                                            'filterModel' => $BStatuseducationSearch,
                                            'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                            'columns' => [
                                                
                                                [
                                                    'class' =>'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '20px'],
                                                ],
                                                [
                                                    'attribute' => 'status_education',
                                                    'label' => 'ข้อมูลสถานะภาพการศึกษา',
                                                    'value' => 'status_education',
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'status_active',
                                                    'label' => 'สถานะการใช้งาน',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5px']
                                                ],
                                                [
                                                    'attribute' => 'createby_user',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                        //print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                               [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => 'create_datetime',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                        'headerOptions' => ['width' => '100'],
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{update}  &nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        editstatuseducation(' . $data->id. ');
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->status_education . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletestatuseducationl(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                    ],

                                            ]
                                        ]);
                                        Pjax::end();
                                    ?>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab5">
                            <div class="row">
                               <div class="pull-right" style="padding-right: 15px;">
                                    <?php
                                    Modal::begin([
                                        'id' => 'modalfrmleveleducation',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลระดับการศึกษา</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewLeveleducation',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลระดับการศึกษา ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                     <form id="frmBleveleducation" onsubmit="return getdatesubmit5();" data-toggle="validator" method="post" >
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>ระดับการศึกษา<span>*</span></label>
                                                <input id="level_education" name="level_education" data-required="true"
                                                class="form-control" type="text"  required="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control status_active" id="status_active5"
                                                        name="status_active">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <?php echo Html::hiddenInput('hide_activitybtitel', null, ['id' => 'hide_activitybtitel5']); ?>
                                                <button type="submit"  class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                   <?php 
                                        Pjax::begin(['id' => 'pjax_tb_bleveleducation']);
                                        echo GridView::widget([
                                            'dataProvider' => $BLeveleducationProvider,
                                            'filterModel' => $BLeveleducationSearch,
                                            'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                            'columns' => [
                                                
                                                [
                                                    'class' =>'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '20px'],
                                                ],
                                                [
                                                    'attribute' => 'level_education',
                                                    'label' => 'ข้อมูลระดับการศึกษา',
                                                    'value' => 'level_education',
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'status_active',
                                                    'label' => 'สถานะการใช้งาน',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5px']
                                                ],
                                                [
                                                    'attribute' => 'createby_user',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                        //print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                               [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => 'create_datetime',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                        'headerOptions' => ['width' => '100'],
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{update}  &nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        editleveleducation(' . $data->id. ');
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->level_education . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deleteleveleducationl(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                    ],

                                            ]
                                        ]);
                                        Pjax::end();
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab6">
                            <div class="row">
                               <div class="pull-right" style="padding-right: 15px;">
                                      <?php
                                    Modal::begin([
                                        'id' => 'modalfrmdegreeeducation',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลวุฒิการศึกษา</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewdegreeeducation',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลวุฒิการศึกษา ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                     <form id="frmBdegreeeducation" onsubmit="return getdatesubmit6();" data-toggle="validator" method="post" >
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>วุฒิการศึกษา<span>*</span></label>
                                                <input id="degree_education" name="degree_education" data-required="true"
                                                class="form-control" type="text"  required="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control status_active" id="status_active6"
                                                        name="status_active">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <?php echo Html::hiddenInput('hide_activitybtitel', null, ['id' => 'hide_activitybtitel6']); ?>
                                                <button type="submit"  class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                   <?php 
                                        Pjax::begin(['id' => 'pjax_tb_bdegreeeducation']);
                                        echo GridView::widget([
                                            'dataProvider' => $BdegreeeducationProvider,
                                            'filterModel' => $BdegreeeducationSearch,
                                            'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                            'columns' => [
                                                
                                                [
                                                    'class' =>'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '20px'],
                                                ],
                                                [
                                                    'attribute' => 'degree_education',
                                                    'label' => 'ข้อมูลวุฒิการศึกษา',
                                                    'value' => 'degree_education',
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'status_active',
                                                    'label' => 'สถานะการใช้งาน',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5px']
                                                ],
                                                [
                                                    'attribute' => 'createby_user',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                        //print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                               [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => 'create_datetime',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                        'headerOptions' => ['width' => '100'],
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{update}  &nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        editdegreeeducation(' . $data->id. ');
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->degree_education . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deletedegreeeeducation(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                    ],

                                            ]
                                        ]);
                                        Pjax::end();
                                    ?>
                                </div>
                            </div>
                        </div>
                         <div class="tab-pane" id="tab7">
                            <div class="row">
                               <div class="pull-right" style="padding-right: 15px;">
                                     <?php
                                    Modal::begin([
                                        'id' => 'modalfrminstitutioneducation',
                                        'header' => '<strong>ฟอร์มบันทึกข้อมูลสถานศึกษา</strong>',
                                        'toggleButton' => [
                                            'id' => 'btnAddNewinstitutioneducation',
                                            'label' => '<i class="fa fa-plus-circle"></i>  เพิ่มข้อมูลสถานศึกษา ',
                                            'class' => 'btn btn-success'
                                        ],
                                        'closeButton' => [
                                            'label' => '<i class="fa fa-close"></i>',
                                            //'class' => 'close pull-right',
                                            'class' => 'btn btn-success btn-sm pull-right'
                                        ],
                                        'size' => 'modal-sm'
                                    ]);
                                    ?>
                                     <form id="frmBinstitutioneducation" onsubmit="return getdatesubmit7();" data-toggle="validator" method="post" >
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam; ?>" value="<?php echo Yii::$app->request->csrfToken; ?>" />
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>วุฒิการศึกษา<span>*</span></label>
                                                <input id="institution_education" name="institution_education" data-required="true"
                                                class="form-control" type="text"  required="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>สถานะ</label>
                                                <select class="form-control status_active" id="status_active7"
                                                        name="status_active">
                                                    <option value="<?php echo Yii::$app->params['ACTIVE_STATUS']; ?>">
                                                        Active
                                                    </option>
                                                    <option value="<?php echo Yii::$app->params['INACTIVE_STATUS']; ?>">
                                                        In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <?php echo Html::hiddenInput('hide_activitybtitel', null, ['id' => 'hide_activitybtitel7']); ?>
                                                <button type="submit"  class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    <?php
                                    Modal::end();
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-sm-12">
                                     <?php 
                                        Pjax::begin(['id' => 'pjax_tb_binstitutioneducation']);
                                        echo GridView::widget([
                                            'dataProvider' => $BInstitutionProvider,
                                            'filterModel' => $BInstitutioneducationSearch,
                                            'summary' => '<div class="text-right">แสดง <strong>{begin} - {end}</strong> จากทั้งหมด <strong>{totalCount}</strong> จำนวน <strong>{pageCount}</strong> หน้า </div>',
                                            'columns' => [
                                                
                                                [
                                                    'class' =>'yii\grid\SerialColumn',
                                                    'headerOptions' => ['width' => '20px'],
                                                ],
                                                [
                                                    'attribute' => 'institution_education',
                                                    'label' => 'ข้อมูลสถานศึกษา',
                                                    'value' => 'institution_education',
                                                    'contentOptions' => ['style' => 'width:20px']
                                                ],
                                                [
                                                    'attribute' => 'status_active',
                                                    'label' => 'สถานะการใช้งาน',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_STATUS',
                                                    'format' => 'image',
                                                    'value' => function ($data) {
                                                        return Utility::dispActive($data->status_active);
                                                    },
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width:5px']
                                                ],
                                                [
                                                    'attribute' => 'createby_user',
                                                    //'value' => 'ADD_DEDUCT_TEMPLATE_CREATE_BY',
                                                    'value' => function ($data) {
                                                        $modelResultemp = ApiHr::getEmpNameForCreateByInIdcard($data->createby_user);
                                                        //print_r($modelResultemp);
                                                        return $modelResultemp['0']['Fullname'];
                                                    },
                                                    'label' => 'บันทึกโดยผู้ใช้',
                                                    //'format' => 'raw',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                               [
                                                    'attribute' => 'create_datetime',
                                                    'label' => 'บันทึกเมื่อวันที่เวลา',
                                                    'value' => 'create_datetime',
                                                    'filter' => false,
                                                    'contentOptions' => ['style' => 'width: 150px;']
                                                ],
                                                [
                                                        'headerOptions' => ['width' => '100'],
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{update}  &nbsp; {delete}',
                                                        'buttons' => [
                                                            'update' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/edit-icon.png">', 'javascript:;', [
                                                                    'title' => 'แก้ไข',
                                                                    'onclick' => '(function($event) {
                                                                        editinstitutioneducation(' . $data->id. ');
                                                                })();'
                                                                ]);
                                                            },

                                                            'delete' => function ($url, $data) {
                                                                return Html::a('<img src="' . Yii::$app->request->baseUrl . '/images/global/delete-icon.png">', 'javascript:;', [
                                                                    'title' => 'ลบ',
                                                                    'onclick' => '(function($event) {
                                                                        bootbox.confirm({
                                                                            size: "small",
                                                                            message:"<h4 class=\"btalert\">คุณแน่ใจว่าจะลบรายการ ' . $data->institution_education . '? </h4>",
                                                                            callback: function(result){
                                                                                if(result==1) {
                                                                                    deleteinstitutioneeducation(' . $data->id. ');
                                                                                }
                                                                            }
                                                                        });

                                                                    })();'
                                                                ]);
                                                            },
                                                        ],
                                                    ],

                                            ]
                                        ]);
                                        Pjax::end();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
</section><!-- /.content -->
