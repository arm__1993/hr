<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/22/2017 AD
 * Time: 10:22
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\bundle\AppAsset;
use app\api\Common;
use app\api\DateTime;
use app\api\Helper;

use app\modules\hr\apihr\ApiHr;
use app\modules\hr\apihr\ApiPayroll;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/master-utility-function.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/hr/bootstrap-multiselect.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]); //css
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/report/accountreport.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]); //route
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/hr/payroll/MonthPicker.min.js?t=' . time(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/MonthPicker.min.css");
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/hr/jquery-ui.css");


$script = <<< JS
$(document).ready(function() {
    setTimeout(function() {
    $('#message').fadeOut('slow');
    }, 3000);

// $("#example2").DataTable({
//       "lengthChange": false,
//       "searching": true,
//       "paging": false,
//       "info": false,
//       "pageLength" : 10,
//     }
//     );

});
JS;

$this->registerJs($script);

?>

<style>

    .rpt table {
        border-collapse: collapse;
    }
    .rpt th {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        text-align: center;
        font-weight: bold;
        background-color: #D5D8DC;
        white-space: nowrap;
    }
    .rpt td {
        border: 0.5px solid #2b2b2b;
        padding: 5px;
        font-size: 12px;
        white-space: nowrap;
    }
    .rpt tr.odd { background: #f3f3f3; }
    .rpt tr.even { background: #FFF; }


</style>
<span id="activetab" title="2"></span>
<input type="hidden" id="tabselectsubmit" value="<?php if ($tab) {
    echo $tab;
} else {
    echo 1;
} ?>">
<section class="content">
    <!-- Default box -->
    <div class="box box-danger">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">ข้อมูลเงินเดือน</a>
                </li>
                <li>ออกรายงาน</li>
                <li class="active">บัญชี</li>
            </ul><!-- /.breadcrumb -->
            <!-- /section:basics/content.searchbox -->
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active tabselect" id="tabselect1"><a href="#tab1"
                                                                    data-toggle="tab">รายงานสรุปผลเงินเดือน</a></li>
                    <li id="tabselect2" class="tabselect"><a href="#tab2" data-toggle="tab">สรุปส่วนเพิ่ม</a></li>
                    <li id="tabselect3" class="tabselect"><a href="#tab3" data-toggle="tab">รายละเอียดส่วนเพิ่ม</a></li>
                    <li id="tabselect4" class="tabselect"><a href="#tab4" data-toggle="tab">สรุปส่วนหัก</a></li>
                    <li id="tabselect5" class="tabselect"><a href="#tab5" data-toggle="tab">รายละเอียดส่วนหัก</a></li>
                    <li id="tabselect6" class="tabselect"><a href="#tab6"
                                                             data-toggle="tab">รายละเอียดส่วนเพิ่มตามรายการ</a></li>
                    <li id="tabselect7" class="tabselect"><a href="#tab7"
                                                             data-toggle="tab">รายละเอียดส่วนหักตามรายการ</a></li>
                    <li id="tabselect8" class="tabselect"><a href="#tab8" data-toggle="tab">รายงานเงินสะสม</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active tabselect" id="tab1">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                        <label>รอบเงินเดือน</label>
                                        &nbsp;&nbsp;
                                        <div class="btn-group">
                                            <input type="text" class="form-control monthpicker" name="rpt1_monthselect" data-to="rpt1_company" id="rpt1_monthselect" value="<?php echo date('m-Y'); ?>">
                                        </div>

                                        <label>บริษัท <span>*</span></label> &nbsp;
                                        <span class="multiselect-native-select">
                                                <select class="form-control companyid" name="companyid[]"
                                                        id="rpt1_company" multiple="multiple" required>
                                                        <?php
                                                        foreach ($Company as $value) {
                                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                                        }
                                                        ?>
                                                </select>
                                            </span>
                                        &nbsp;&nbsp;
                                        <button type="button" id="btnShowRpt1" class="btn btn-primary btn-sm btnselect"
                                                title="1"><i class="fa fa-search"></i> ค้นหา
                                        </button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report1"></div>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab2">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rpt2_monthselect"  class="col-sm-6 control-label">รอบเงินเดือน</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control monthpicker" name="rpt2_monthselect" data-to="rpt2_company" id="rpt2_monthselect" value="<?php echo date('m-Y'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>บริษัท * </label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt2_company"  name="rpt2_company[]" multiple="multiple"></select>
                                </span>
                            </div>
                            <div class="col-md-3">
                                <label>รายการเพิ่ม</label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt2_tempate" name="rpt2_tempate[]" multiple="multiple">
                                            <?php
                                            foreach ($AddDeductTemplate as $value) {
                                                echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btnShowRpt2" class="btn btn-primary btn-sm btnselect"><i class="fa fa-search"></i> ค้นหา </button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report2"></div>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab3">

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rpt3_monthselect"  class="col-sm-6 control-label">รอบเงินเดือน</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control monthpicker" name="rpt3_monthselect" data-to="rpt3_company" id="rpt3_monthselect" value="<?php echo date('m-Y'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>บริษัท * </label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" name="companyid" id="rpt3_company" >
                                            <?php
                                            foreach ($Company as $value) {
                                                echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-3">
                                <label>รายการเพิ่ม</label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt3_tempate" name="rpt3_tempate[]" multiple="multiple">
                                            <?php
                                            foreach ($AddDeductTemplate as $value) {
                                                echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btnShowRpt3" class="btn btn-primary btn-sm btnselect"><i class="fa fa-search"></i> ค้นหา </button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report3"></div>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab4">

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rpt4_monthselect"  class="col-sm-6 control-label">รอบเงินเดือน</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control monthpicker" name="rpt4_monthselect" data-to="rpt4_company" id="rpt4_monthselect" value="<?php echo date('m-Y'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>บริษัท * </label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt4_company"  name="rpt4_company[]" multiple="multiple"></select>
                                </span>
                            </div>
                            <div class="col-md-3">
                                <label>รายการหัก</label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt4_tempate" name="rpt4_tempate[]" multiple="multiple">
                                            <?php
                                            foreach ($DeductTemplate as $value) {
                                                echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btnShowRpt4" class="btn btn-primary btn-sm btnselect"><i class="fa fa-search"></i> ค้นหา </button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report4"></div>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab5">

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rpt5_monthselect"  class="col-sm-6 control-label">รอบเงินเดือน</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control monthpicker" name="rpt5_monthselect" data-to="rpt5_company" id="rpt5_monthselect" value="<?php echo date('m-Y'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>บริษัท * </label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" name="companyid" id="rpt5_company" >
                                            <?php
                                            foreach ($Company as $value) {
                                                echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-3">
                                <label>รายการหัก</label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt5_tempate" name="rpt5_tempate[]" multiple="multiple">
                                            <?php
                                            foreach ($DeductTemplate as $value) {
                                                echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btnShowRpt5" class="btn btn-primary btn-sm btnselect"><i class="fa fa-search"></i> ค้นหา </button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report5"></div>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab6">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rpt2_monthselect"  class="col-sm-6 control-label">รอบเงินเดือน</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control monthpicker" name="rpt6_monthselect" data-to="rpt6_company" id="rpt6_monthselect" value="<?php echo date('m-Y'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>บริษัท * </label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt6_company"  name="rpt6_company[]" multiple="multiple"></select>
                                </span>
                            </div>
                            <div class="col-md-3">
                                <label>รายการเพิ่ม</label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt6_tempate" name="rpt6_tempate[]" multiple="multiple">
                                            <?php
                                            foreach ($AddDeductTemplate as $value) {
                                                echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btnShowRpt6" class="btn btn-primary btn-sm btnselect"><i class="fa fa-search"></i> ค้นหา </button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report6"></div>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab7">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rpt7_monthselect"  class="col-sm-6 control-label">รอบเงินเดือน</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control monthpicker" name="rpt7_monthselect" data-to="rpt7_company" id="rpt7_monthselect" value="<?php echo date('m-Y'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>บริษัท * </label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt7_company"  name="rpt7_company[]" multiple="multiple"></select>
                                </span>
                            </div>
                            <div class="col-md-3">
                                <label>รายการหัก</label>
                                <span class="multiselect-native-select">
                                    <select class="form-control companyid" id="rpt7_tempate" name="rpt2_tempate[]" multiple="multiple">
                                            <?php
                                            foreach ($DeductTemplate as $value) {
                                                echo '<option value="' . $value['ADD_DEDUCT_TEMPLATE_ID'] . '">' . $value['ADD_DEDUCT_TEMPLATE_NAME'] . '</option>';
                                            }
                                            ?>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btnShowRpt7" class="btn btn-primary btn-sm btnselect"><i class="fa fa-search"></i> ค้นหา </button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report7"></div>
                        </div>
                    </div>
                    <div class="tab-pane tabselect" id="tab8">
                        <div class="row">
                            <div class="col-md-3 text-right">พนักงาน</div>
                            <div class="col-md-3">
                                <select class="form-control select2" name="emp_idcard" id="emp_idcard" required >
                                        <?php
                                        foreach($empdata as $value){
                                            ?>
                                            <option value="<?php echo $value['value']; ?>"><?php echo $value['label']; ?></option>
                                        <?php }?>
                                    </select>
                            </div>
                            <div class="col-md-3"><button type="button" id="btnShowRpt8" class="btn btn-primary btn-sm btnselect"><i class="fa fa-search"></i> ค้นหา</button></div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive" id="dv_report8"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
</section><!-- /.content -->