<?php

namespace app\modules\hr\models;

/**
 * This is the ActiveQuery class for [[TaxIncomeType]].
 *
 * @see TaxIncomeType
 */
class TaxIncomeTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaxIncomeType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaxIncomeType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
