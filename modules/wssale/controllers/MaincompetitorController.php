<?php

namespace app\modules\wssale\controllers;
use app\modules\wssale\models\Maincompetitor;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;


class MaincompetitorController extends ActiveController
{

    public $modelClass = 'app\modules\wssale\models\Maincompetitor';


	public function behaviors()
	{
	    return
            ArrayHelper::merge([
	        [
	            'class' => Cors::className(),
	            'cors' => [
	                'Origin' => ['*'],
	                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
	            ],
	        ],
	    ],
        parent::behaviors());
	}
	

	function actionCarband()
	    {
        //return 1;
     	
        $listcompany = new Maincompetitor();
        $dataCompany = $listcompany->carband();
        return $dataCompany;
    }


	function actionCarbandtoselectcompanyformobile()
	    {
        //return 1;
     	$NameCarband = $_GET['NameCarband'];
        $listcompany = new Maincompetitor();
        $dataSelectcompany = $listcompany->carbandtoselectcompanyformobile($NameCarband);
        return $dataSelectcompany;
    }


}
