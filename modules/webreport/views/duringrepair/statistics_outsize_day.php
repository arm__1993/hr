<?php
/**
 * Created by PhpStorm.
 * User: MI6
 * Date: 20/10/2559
 * Time: 11:33
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiDuringrepair;
use yii\web\JsExpression;
use miloschuman\highcharts\HighchartsAsset;
use app\api\Common;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/dataTables.bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/jquery.datetimepicker.full.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/webreport/daterangepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/dataTables.bootstrap.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/daterangepicker.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
  $(function () {
    $("#example1").DataTable({
      "lengthChange": false,
      "searching": true,
      "paging": true,
      "info": true,
      "pageLength" : 10,
    });
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
  });

 $('#reservation').daterangepicker({
        locale :{
            fromLabel: 'From',
            format:'DD/MM/YYYY',
            separator: ' - ',
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
            toLabel: 'To',
            lang:'th',
            applyLabel: 'นำไปใช้',
            cancelLabel: 'ยกเลิก',
            yearOffset:'543',
            customRangeLabel: 'Custom',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
        }
    });

});
JS;

$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;
$basePath = Yii::$app->request->baseUrl;
$moduleID = $module = Yii::$app->controller->module->id;
$siteURL = Common::siteURL() . $basePath . '/' . $moduleID;

define('SITE_URL', $siteURL);
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานสถิติระยะเวลาใบงานเกินเวลามาตรฐาน ต่อวัน</h3>

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <div class="row">
            <form method="post" action="time_daily">
                <input type="hidden" value="<?= Yii::$app->request->csrfToken; ?>" name="_csrf">
                <div class="col-md-12 text-center">

                    <label>บริษัท</label>
                    &nbsp;
                    <div class="btn-group">
                        <select class="form-control" name="company" id="company">
                            <?php
                            $arrCompany = ApiReport::getAllCompay();
                            foreach ($arrCompany as $key => $value) {
                                $sel = ($selected_company == $key) ? ' selected="selected" ' : '';
                                echo '<option value="' . $key . '" ' . $sel . '>' . $value . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    &nbsp;&nbsp;
                    <label>ช่วงวันที่</label>
                    &nbsp;
                    <div class="btn-group">
                        <div class="input-group" style="width: 250px">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="reservation" id="reservation"
                                   value="<?php print_r($selected_months);?>" data-provide="datepicker" data-date-language="th">
                        </div>
                    </div>
                    &nbsp;&nbsp;
                    <label>เลือกประเภทรถ</label>
                    &nbsp;
                    <div class="btn-group">
                        <input type="radio" name="technician" value="3,4" checked> รถเล็ก
                        &nbsp;
                        <input type="radio" name="technician" value="1,2"> รถใหญ่
                    </div>


                    <br>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-fw fa-search"></i>ค้นหา
                        </button>
                    </div>
                    &nbsp;
                    <div class="btn-group">
                        <a href="<?php echo SITE_URL; ?>/duringrepair/btn_reportoday">
                            <button type="button" class="btn btn-success">
                                <i class="fa fa-fw fa-file-pdf-o"></i> ออก PDF
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
 <!-- -->

    <!-- ./box-body -->
    <?php if ($query) { ?>
     <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <!--<div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>-->
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row" class="odd">
                                                 <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    วันที่
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                ยอดเข้าบริการ
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                A1(รอรับแจ้งซ่อม)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                A1(คิดเป็น%)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                B1(รอจ่ายงานให้ช่าง)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                B1(คิดเป็น%)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                C1(รอปิดงานซ่อม)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                C1(คิดเป็น%)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                D1(ปิดงานซ่อม)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                D1(คิดเป็น%)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                E1(ชำระเงิน)
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                E1(คิดเป็น%)
                                                </th>
                                            </tr>
                                    </thead>
                                   
                                     <?php 
                                                 foreach ($TimeStatusDaily as $key => $value) {
                                            ?>
                                            <tr role="row" class="odd">
                                                <td><?php
                                                echo $TimeStatusDaily[$key]['TimeSelect'];
                                                ?>
                                                </td>
                                                <td><?php echo $TimeStatusDaily[$key]['CountCar'];
                                                     if($TimeStatusDaily[$key]['CountCar'] == 0){
                                                    $countCar = 1 ;
                                                }else
                                                {
                                                    $countCar = $TimeStatusDaily[$key]['CountCar'];
                                                };
                                                ?></td>
                                                <td><?php echo $TimeStatusDaily[$key]['Time_DiffA'];?></td>
                                                <td><?php $presentOfTimeStatusDaliyA =  ($TimeStatusDaily[$key]['Time_DiffA']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyA)));?></td>
                                                <td><?php echo $TimeStatusDaily[$key]['Time_DiffB'];?></td>
                                                <td><?php $presentOfTimeStatusDaliyB =  ($TimeStatusDaily[$key]['Time_DiffB']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyB)));?></td>
                                                <td><?php echo $TimeStatusDaily[$key]['Time_DiffC'];?></td>
                                                <td><?php $presentOfTimeStatusDaliyC =  ($TimeStatusDaily[$key]['Time_DiffC']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyC)));?></td>
                                                <td><?php echo $TimeStatusDaily[$key]['Time_DiffD'];?></td>
                                                <td><?php $presentOfTimeStatusDaliyD =  ($TimeStatusDaily[$key]['Time_DiffD']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyD)));?></td>
                                                <td><?php echo $TimeStatusDaily[$key]['Time_DiffE'];?></td>
                                                <td><?php $presentOfTimeStatusDaliyE =  ($TimeStatusDaily[$key]['Time_DiffE']/$countCar)*100;
                                                print_r(Helper::displayDecimal(ApiDuringrepair::getCheckCountCarForPresent($presentOfTimeStatusDaliyE)));?></td>
                                            </tr>
                                            <?php  } ?>                           
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
        
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                        data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="row">
       <?php
        $chart_selected_day = []; 
        $chart_selected_dataA = []; 
        $chart_selected_dataB = []; 
        $chart_selected_dataC = []; 
        $chart_selected_dataD = []; 
        $chart_selected_dataE = []; 
        foreach ($TimeStatusDaily as $key => $value) {
                    if($TimeStatusDaily[$key]['CountCar'] == 0){
                            $countCar = 1 ;
                    }else{
                            $countCar = $TimeStatusDaily[$key]['CountCar'];
                    }; 
            
                $chart_selected_day[] = $TimeStatusDaily[$key]['TimeSelect'];

                $chart_selected_dataA[] =  ($TimeStatusDaily[$key]['Time_DiffA']/$countCar)*100;
                                               
                $chart_selected_dataB[] =  ($TimeStatusDaily[$key]['Time_DiffB']/$countCar)*100;
                                              
                $chart_selected_dataC[] =  ($TimeStatusDaily[$key]['Time_DiffC']/$countCar)*100;
                                            
                $chart_selected_dataD[]=  ($TimeStatusDaily[$key]['Time_DiffD']/$countCar)*100;

                $chart_selected_dataE[] =  ($TimeStatusDaily[$key]['Time_DiffE']/$countCar)*100;
                                          
         } 
         ?>               
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php $arrMonth = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'); ?>
                                        <?php
                                        echo Highcharts::widget([
                                            'options' => [
                                                'title' => [
                                                    'text' => ''
                                                ],
                                                'xAxis' => [
                                                    'categories' => $chart_selected_day
                                                ],
                                                'labels' => [
                                                    'items' => [
                                                        'html' => 'Total fruit consumption',
                                                        'style' => [
                                                            'left' => '50px',
                                                            'top' => '18px',
                                                            'color' => "(Highcharts.theme && Highcharts.theme.textColor) || 'black'",
                                                        ]
                                                    ]
                                                ],
                                                'credits' => [
                                                    'enabled' => false
                                                ],
                                                'series' => [
                                                    [
                                                        'type' => 'column',
                                                        'name' => 'A1',
                                                        'data' => $chart_selected_dataA,
                                                        'dataLabels' => [
                                                            'enabled' => true,
                                                            'format' => '{point.y:.1f}%'
                                                        ]
                                                    ],
                                                    [
                                                        'type' => 'column',
                                                        'name' => 'B1',
                                                        'data' => $chart_selected_dataB,
                                                        'dataLabels' => [
                                                            'enabled' => true,
                                                            'format' => '{point.y:.1f}%'
                                                        ]
                                                    ],
                                                    [
                                                        'type' => 'column',
                                                        'name' => 'C1',
                                                        'data' => $chart_selected_dataC,
                                                        'dataLabels' => [
                                                            'enabled' => true,
                                                            'format' => '{point.y:.1f}%'
                                                        ]
                                                    ],
                                                    [
                                                        'type' => 'column',
                                                        'name' => 'D1',
                                                        'data' => $chart_selected_dataD,
                                                        'dataLabels' => [
                                                            'enabled' => true,
                                                            'format' => '{point.y:.1f}%'
                                                        ]
                                                    ],
                                                    [
                                                        'type' => 'column',
                                                        'name' => 'E1',
                                                        'data' => $chart_selected_dataE,
                                                        'dataLabels' => [
                                                            'enabled' => true,
                                                            'format' => '{point.y:.1f}%'
                                                        ]
                                                    ],

                                                ]
                                            ]
                                        ]);
                                        ?>
                                    </div>
                                </div><!--row-->
                            </div>
                        </div><!--box-body-->
                    </div><!--box box-info-->
                    <!-- AREA CHART -->
                </div>
            </div>
        </section>
    <?php } ?>

</div>
<!-- /.box-->

