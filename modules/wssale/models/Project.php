<?php

namespace app\modules\wssale\models;

use Yii;

/**
 * This is the model class for table "PROJECT".
 *
 * @property integer $ID
 * @property integer $WORKING_COMPANY_ID
 * @property string $PROJECT_NAME
 * @property string $START_DATE
 * @property string $STOP_DATE
 * @property string $QUALITY
 * @property string $SIZE
 * @property string $LOCATION
 * @property string $DISTRICT_ID
 * @property string $AMPHUR_ID
 * @property string $PROVINCE_ID
 * @property integer $TARGET_CUTINT
 * @property integer $TARGET_BOOKING
 * @property integer $TARGET_SELL
 * @property integer $TARGET_OTHER1
 * @property integer $TARGET_OTHER2
 * @property string $STATUS
 * @property string $CREATE_DATE
 * @property string $CREATE_IDCARD
 * @property string $CREATE_POSCODE
 * @property string $UPDATE_DATE
 * @property string $UPDATE_IDCARD
 * @property string $UPDATE_POSCODE
 * @property string $REMARK
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PROJECT';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_Easysale_icmba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WORKING_COMPANY_ID', 'PROJECT_NAME', 'START_DATE', 'STOP_DATE', 'QUALITY', 'SIZE', 'LOCATION', 'DISTRICT_ID', 'AMPHUR_ID', 'PROVINCE_ID', 'TARGET_CUTINT', 'TARGET_BOOKING', 'TARGET_SELL', 'TARGET_OTHER1', 'TARGET_OTHER2', 'STATUS', 'CREATE_DATE', 'CREATE_IDCARD', 'CREATE_POSCODE', 'UPDATE_DATE', 'UPDATE_IDCARD', 'UPDATE_POSCODE', 'REMARK'], 'required'],
            [['WORKING_COMPANY_ID', 'TARGET_CUTINT', 'TARGET_BOOKING', 'TARGET_SELL', 'TARGET_OTHER1', 'TARGET_OTHER2'], 'integer'],
            [['START_DATE', 'STOP_DATE', 'CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['REMARK'], 'string'],
            [['PROJECT_NAME'], 'string', 'max' => 255],
            [['QUALITY', 'LOCATION', 'DISTRICT_ID', 'AMPHUR_ID', 'PROVINCE_ID'], 'string', 'max' => 50],
            [['SIZE', 'CREATE_POSCODE', 'UPDATE_POSCODE'], 'string', 'max' => 30],
            [['STATUS'], 'string', 'max' => 2],
            [['CREATE_IDCARD', 'UPDATE_IDCARD'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'WORKING_COMPANY_ID' => 'ไอดีบริษัท',
            'PROJECT_NAME' => 'ชื่อโปรเจ็ค',
            'START_DATE' => 'วันที่เริ่มต้น',
            'STOP_DATE' => 'วันที่สิ้นสุด',
            'QUALITY' => 'ลักษณะโปรเจ็ค(จัดเอง,จัดร่วม,เข้าร่วม)',
            'SIZE' => 'ขนาดโปรเจ็ค(1=S,2=M,3=L,4=XL)',
            'LOCATION' => 'สถานที่/พื้นที่จัดงาน',
            'DISTRICT_ID' => 'ตำบล',
            'AMPHUR_ID' => 'อำเภอ',
            'PROVINCE_ID' => 'จังหวัด',
            'TARGET_CUTINT' => 'เป้าสนใจ',
            'TARGET_BOOKING' => 'เป้าจอง',
            'TARGET_SELL' => 'เป้าขาย',
            'TARGET_OTHER1' => 'เป้าอื่นๆ',
            'TARGET_OTHER2' => 'เป้าอื่นๆ',
            'STATUS' => 'สถานะ',
            'CREATE_DATE' => 'สร้างข้อมูล',
            'CREATE_IDCARD' => 'รหัสบัตรประชาชนของผู้สร้างข้อมูล',
            'CREATE_POSCODE' => 'ตำแหน่งพนักงานที่สร้างข้อมูล',
            'UPDATE_DATE' => 'เวลาที่แก้ไขข้อมูล',
            'UPDATE_IDCARD' => 'รหัสบัตรประชาชนของผู้แก้ไขข้อมูล',
            'UPDATE_POSCODE' => 'ตำแหน่งพนักงานที่แก้ไขข้อมูล',
            'REMARK' => 'สาเหตุที่ยกเลิกโปรเจค',
        ];
    }
}
