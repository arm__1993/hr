<?php

namespace app\modules\vhc\model\VHC;

use Yii;
use app\modules\vhc\models\MasterModel;
/**
 * This is the model class for table "vhc_detail".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property integer $status
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class VhcDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vhc_detail';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_service');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'name', 'status', 'create_by', 'update_by', 'create_date', 'update_date'], 'required'],
            [['group_id', 'status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['create_by', 'update_by'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'name' => 'Name',
            'status' => 'Status',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
