<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "Detail_Record".
 * 
 * @property integer $ID_Detail
 * @property integer $ID_Header
 * @property string $RECORD_TYPE
 * @property string $SSO_ID
 * @property string $PREFIX
 * @property string $FNAME
 * @property string $LNAME
 * @property string $WAGES
 * @property string $PAID_AMOUNT
 * @property string $BLANK
 * @property string $COMPANY_ID
 * @property string $pay_date
 */
class DetailRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Detail_Record';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_PAYROLL');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_Header', 'RECORD_TYPE', 'SSO_ID', 'PREFIX', 'FNAME', 'LNAME', 'WAGES', 'PAID_AMOUNT', 'COMPANY_ID','pay_date'], 'required'],
            [['ID_Header'], 'integer'],
            [['RECORD_TYPE'], 'string', 'max' => 1],
            [['SSO_ID'], 'string', 'max' => 13],
            [['PREFIX', 'COMPANY_ID'], 'string', 'max' => 3],
            [['FNAME'], 'string', 'max' => 30],
            [['LNAME'], 'string', 'max' => 35],
            [['WAGES'], 'string', 'max' => 14],
            [['pay_date'], 'string', 'max' => 100],
            [['PAID_AMOUNT'], 'string', 'max' => 12],
            [['BLANK'], 'string', 'max' => 27],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_Detail' => 'Id  Detail',
            'ID_Header' => 'Id  Header',
            'RECORD_TYPE' => 'Record  Type',
            'SSO_ID' => 'Sso  ID',
            'PREFIX' => 'Prefix',
            'FNAME' => 'Fname',
            'LNAME' => 'Lname',
            'WAGES' => 'Wages',
            'PAID_AMOUNT' => 'Paid  Amount',
            'BLANK' => 'Blank',
            'COMPANY_ID' => 'Company  ID',
            'pay_date' => 'pay date',
        ];
    }
}
