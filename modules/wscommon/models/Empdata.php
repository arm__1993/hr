<?php

namespace app\modules\wscommon\models;

use Yii;

class Empdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_easyhr_checktime');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BNo', 'Code', 'Old_Code', 'TIS_id', 'Pictures_HyperL', 'IDCard_HyperL', 'HomeDoc_HyperL', 'Be', 'password', 'password_old', 'password_test', 'password_backup_by_kenz', 'Spouse_workplace', 'Main_EndWork_Reason', 'EndWork_Reason', 'Footnote'], 'string'],
            [['Name', 'Surname', 'username', 'password', 'password_test', 'password_backup_by_kenz', 'ID_Card', 'Spouse_prefix', 'Spouse_career', 'Spouse_workplace', 'Moo', 'Road', 'Live_with', 'Resident_type', 'work_type_id', 'Emergency_Call_Person', 'Emergency_Relation', 'Emergency_Phone_Num', 'socialBenifitPercent', 'socialBenefitStatus', 'benefitFundPercent', 'benifitFundStatus', 'to_salary_status', 'UPDATE_BY', 'status_confirmData'], 'required'],
            [['Birthday', 'Spouse_Birthday', 'Start_date', 'Probation_Date', 'End_date', 'UPDATE_TIME'], 'safe'],
            [['Foot_Step_Num', 'SalaryViaBank', 'Prosonnal_Being', 'status_confirmData'], 'integer'],
            [['Name', 'Surname', 'Nickname', 'Father', 'Father_Job', 'Mother', 'Mother_Job', 'Spouse_Name', 'Spouse_Surname', 'Spouse_career', 'Relative_Person', 'Road', 'Live_with', 'Salary_Bank', 'Emergency_Call_Person'], 'string', 'max' => 250],
            [['username', 'E_Mail', 'HighSchool', 'Adv_HighSchool', 'Adv_Major', 'Bechelor', 'Bec_Major', 'Master', 'Master_Major'], 'string', 'max' => 200],
            [['Sex', 'Weight', 'Height', 'Spouse_prefix', 'Childs', 'Moo', 'Level', 'DayOff'], 'string', 'max' => 10],
            [['Nationality', 'Religion', 'Social_Card', 'Parent_Status', 'Status', 'Salary_BankNo', 'Guarantee_Money'], 'string', 'max' => 20],
            [['Blood_G', 'socialBenifitPercent', 'socialBenefitStatus', 'benefitFundPercent', 'benifitFundStatus'], 'string', 'max' => 2],
            [['ID_Card'], 'string', 'max' => 14],
            [['Relative_Person_Tel', 'Resident_type', 'Tel_Num', 'Mobile_Num', 'Team', 'Emergency_Phone_Num'], 'string', 'max' => 50],
            [['Address', 'Village', 'SubDistrict', 'District', 'Province', 'geography', 'Working_Company', 'Position', 'Department', 'Section', 'Emergency_Relation', 'UPDATE_BY'], 'string', 'max' => 100],
            [['Postcode', 'startWorkTime', 'beginLunch', 'endLunch', 'endWorkTime'], 'string', 'max' => 5],
            [['work_type_id'], 'string', 'max' => 11],
            [['can_la', 'use_check'], 'string', 'max' => 3],
            [['to_salary_status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'DataNo' => 'Data No',
            'BNo' => 'Bno',
            'Code' => 'Code',
            'Old_Code' => 'Old  Code',
            'TIS_id' => 'Tis ID',
            'Pictures_HyperL' => 'Pictures  Hyper L',
            'IDCard_HyperL' => 'Idcard  Hyper L',
            'HomeDoc_HyperL' => 'Home Doc  Hyper L',
            'Be' => 'Be',
            'Name' => 'ชื่อ',
            'Surname' => 'นามสกุล',
            'Nickname' => 'ชื่อเล่น',
            'username' => 'Username',
            'password' => 'Password',
            'password_old' => 'Password Old',
            'password_test' => 'Password Test',
            'password_backup_by_kenz' => 'Password Backup By Kenz',
            'Sex' => 'เพศ',
            'Birthday' => 'วันเกิด',
            'Nationality' => 'สัญชาติ',
            'Religion' => 'ศาสนา',
            'Weight' => 'น้ำหนัก',
            'Height' => 'ส่วนสูง',
            'Blood_G' => 'กรุ๊ปเลือด',
            'Foot_Step_Num' => 'เบอร์รองเท้า',
            'ID_Card' => 'หมายเลขบัตรประชาชน',
            'Social_Card' => 'หมายเลขประกันสังคม',
            'Father' => 'ชื่อ นามสกุล บิดา',
            'Father_Job' => 'อาชีพ บิดา',
            'Mother' => 'ชื่อ นามสกุล มารดา',
            'Mother_Job' => 'อาชีพมารดา',
            'Parent_Status' => 'สถานะภาพ บิดามารดา',
            'Status' => 'สถานะภาพโสด / สมรส / หย่าร้าง',
            'Spouse_prefix' => 'คำนำหน้าชื่อ คู่สมรส',
            'Spouse_Name' => 'ชื่อคู่สมรส',
            'Spouse_Surname' => 'นามสกุลคู่สมรส',
            'Spouse_Birthday' => 'วันเกิดคู่สมรส',
            'Spouse_career' => 'อาชีพคู่สมรส',
            'Spouse_workplace' => 'สถานที่ทำงานคู่สมรส',
            'Childs' => 'ลูก',
            'Relative_Person' => 'ชื่อ นามสกุลญาติ',
            'Relative_Person_Tel' => 'เบอร์โทรญาติ',
            'Address' => 'ที่อยู่',
            'Moo' => 'หมู่ที่',
            'Road' => 'ถนน',
            'Village' => 'หมู่บ้าน',
            'SubDistrict' => 'ตำบล',
            'District' => 'อำเภอ',
            'Province' => 'จังหวัด',
            'geography' => 'ภูมิภาค',
            'Postcode' => 'รหัสไปรษณีย์',
            'Live_with' => 'พักอยู่กับ',
            'Resident_type' => 'ประเภทที่พัก',
            'Tel_Num' => 'เบอร์โทรบ้าน',
            'Mobile_Num' => 'เบอร์มือถือ',
            'E_Mail' => 'email',
            'HighSchool' => 'ที่เรียน ปวช',
            'Adv_HighSchool' => 'ปวส',
            'Adv_Major' => 'ปวส ที่เรียนสาขา',
            'Bechelor' => 'ที่เรียน ป.ตรี',
            'Bec_Major' => 'สาขา ป.ตรี',
            'Master' => 'ที่เรียน โท',
            'Master_Major' => 'สาขา โท',
            'Working_Company' => 'บริษัท',
            'work_type_id' => 'Work Type ID',
            'Position' => 'ตำแหน่ง',
            'Department' => 'แผนก',
            'Section' => 'ฝ่าย',
            'Team' => 'ทีมที่สังกัด',
            'Level' => 'ระดับของตำแหน่ง',
            'Salary_Bank' => 'ธนาคารและสาขาที่รับเงินเดือน',
            'Salary_BankNo' => 'หมายเลขบัญชี',
            'SalaryViaBank' => 'รับเงินเดือนผ่านธนาคาร (0 ไม่รับ 1 รับ)',
            'Guarantee_Money' => 'เงินค้ำประกัน',
            'Prosonnal_Being' => '2:ทดลองงาน 1:ผ่านงาน 3:ลาออก',
            'DayOff' => 'วันหยุดประจำสัปดาห์',
            'Start_date' => 'วันที่เริ่มทำงาน',
            'Probation_Date' => 'วันที่ผ่านการทดลองงาน',
            'End_date' => 'วันที่ลาออก',
            'startWorkTime' => 'Start Work Time',
            'beginLunch' => 'Begin Lunch',
            'endLunch' => 'End Lunch',
            'endWorkTime' => 'End Work Time',
            'Main_EndWork_Reason' => 'เหตุผลหลักการลาออก',
            'EndWork_Reason' => 'เหตุผลในการลาออก',
            'Footnote' => 'หมายเหตุ',
            'Emergency_Call_Person' => 'บุคคลที่ติดต่อกรณีฉุกเฉิน',
            'Emergency_Relation' => 'ความสัมพันธ์ (บุคคลที่ติดต่อกรณีฉุกเฉิน)',
            'Emergency_Phone_Num' => '้เบอร์โทรของบุคคลที่ติดต่อกรณีฉุกเฉิน',
            'can_la' => 'สร้างไว้ชั่วคราว',
            'use_check' => 'สร้างไว้ชั่วคราว',
            'socialBenifitPercent' => '% ประกันสังคม',
            'socialBenefitStatus' => 'สถานะ 0ไม่คิด 1 คิด',
            'benefitFundPercent' => '% เงินสะสม',
            'benifitFundStatus' => 'สถานะ 0 ไม่คิด 1 คิด',
            'to_salary_status' => '1 คิดเงินเดือน 2 ไม่คิดเงินเดือน',
            'UPDATE_TIME' => 'Update  Time',
            'UPDATE_BY' => 'Update  By',
            'status_confirmData' => 'สถานะการยืนยันการบันทึกข้อมูล 0:ยืนยัน, 1:ไม่ยืนยัน',
        ];
    }


    public function searcpositionloginmobile($username) // param
    {
        $sql = "select ID_Card from emp_data  where username = :username  ";
        //return $sql;

        $showusername = Yii::$app->dbERP_easyhr_checktime
       ->createCommand($sql)
       ->bindParam(':username',$username)->queryOne();
        return $showusername;

    }



}
