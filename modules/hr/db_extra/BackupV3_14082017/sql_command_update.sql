ALTER TABLE `tax_calculate` ADD `wage_type` TINYINT(3) NOT NULL COMMENT 'ประเภทการจ่ายเงินเดือน 1=ราย7วัน,2=ราย 15วัน,3=รายเดือน' AFTER `for_month`, ADD `wage_multiply` TINYINT(3) NOT NULL COMMENT 'ตัวคูณเพื่อคำนวณภาษีตามประเภทการจ่าย 52=ราย 7วัน,24=ราย15วัน,12= รายเดือน' AFTER `wage_type`, ADD INDEX (`wage_type`);



/** Update 2017-07-17  @aeedy **/
ALTER TABLE `payroll_config_wage` ADD `is_endofmonth` TINYINT(3) NOT NULL COMMENT 'flag บอกว่ารายการนี้คำนวณภาษี ณ สิ้นเดือน, 1=ใช่, 0=ไม่ใช่' AFTER `wage_type_name`;

ALTER TABLE `ADD_DEDUCT_THIS_MONTH` ADD `is_endofmonth` TINYINT(3) NULL COMMENT 'flag บอกว่ารายการนี้คำนวณภาษี ณ สิ้นเดือน, 1=ใช่, 0=ไม่ใช่' AFTER `ADD_DEDUCT_THIS_MONTH_UPDATE_BY`, ADD `cal_times` TINYINT(3) NULL COMMENT 'จำนวนครั้ง คำนวณรายได้' AFTER `is_endofmonth`, ADD `cal_date` DATE NULL COMMENT 'วันที่คำนวณรายได้' AFTER `cal_times`;
ALTER TABLE `ADD_DEDUCT_HISTORY` ADD `is_endofmonth` TINYINT(3) NULL COMMENT 'flag บอกว่ารายการนี้คำนวณภาษี ณ สิ้นเดือน, 1=ใช่, 0=ไม่ใช่' AFTER `ADD_DEDUCT_THIS_MONTH_UPDATE_BY`, ADD `cal_times` TINYINT(3) NULL COMMENT 'จำนวนครั้ง คำนวณรายได้' AFTER `is_endofmonth`, ADD `cal_date` DATE NULL COMMENT 'วันที่คำนวณรายได้' AFTER `cal_times`;
ALTER TABLE `WAGE_THIS_MONTH` ADD `is_endofmonth` TINYINT(3) NULL COMMENT 'flag บอกว่ารายการนี้คำนวณภาษี ณ สิ้นเดือน, 1=ใช่, 0=ไม่ใช่' AFTER `WAGE_UPDATE_BY`, ADD `cal_times` TINYINT(3) NULL COMMENT 'จำนวนครั้ง คำนวณรายได้' AFTER `is_endofmonth`, ADD `cal_date` DATE NULL COMMENT 'วันที่คำนวณรายได้' AFTER `cal_times`;
ALTER TABLE `WAGE_HISTORY` ADD `is_endofmonth` TINYINT(3) NULL COMMENT 'flag บอกว่ารายการนี้คำนวณภาษี ณ สิ้นเดือน, 1=ใช่, 0=ไม่ใช่' AFTER `WAGE_UPDATE_BY`, ADD `cal_times` TINYINT(3) NULL COMMENT 'จำนวนครั้ง คำนวณรายได้' AFTER `is_endofmonth`, ADD `cal_date` DATE NULL COMMENT 'วันที่คำนวณรายได้' AFTER `cal_times`;


/** Update 2017-08-01  @aeedy **/
ALTER TABLE `ot_requestmaster` ADD `wage_pay_date` CHAR(7) NOT NULL AFTER `request_no`, ADD INDEX `wage_pay_date` (`wage_pay_date`);


