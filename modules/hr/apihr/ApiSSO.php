<?php


/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 2/1/2017 AD
 * Time: 10:50
 */

namespace app\modules\hr\apihr;
use yii\db\Expression;
use yii\db\Transaction;
use yii;

use app\api\Helper;
use app\api\DateTime;
use app\api\Utility;
use app\modules\hr\models\SocialSalary;
use app\modules\hr\models\Empdata;
class ApiSSO
{
        public static function SaveCalculateSSO($arrValue)
        {
//            print_r($arrValue);
//            exit;
                //$connection = \Yii::$app->ERP_easyhr_PAYROLL;

                    Yii::$app->dbERP_easyhr_PAYROLL->createCommand()
                            ->batchInsert('SOCIAL_SALARY', ['SOCIAL_SALARY_DATE',
                                'SOCIAL_SALARY_EMP_ID',
                                'SOCIAL_SALARY_PERCENT',
                                'SOCIAL_SALARY_AMOUNT',
                                'SOCIAL_SALARY_STATUS',
                                'SOCIAL_SALARY_TRANSACTION_BY',
                                'SOCIAL_SALARY_TRANSACTION_DATE'],
                                $arrValue)
                                ->execute();
            return true;

        }

        public static function ListSSOThisMonth($arrIDCard,$MonthYear)
        {
             
            $SOCIAL_SALARY_EMP_ID = [];
            foreach($arrIDCard as $value){
                $SOCIAL_SALARY_EMP_ID[] = $value['empIdCard'];
            }
        
        $resultYear = DateTime::selectMonthHaveZero($MonthYear);
        $resultMonthSearch = date($resultYear['monthresult']."-".$resultYear['year']);
            $model = Socialsalary::find()
                    ->where('SOCIAL_SALARY.SOCIAL_SALARY_STATUS != "99" 
                            AND  SOCIAL_SALARY.SOCIAL_SALARY_DATE = :resultMonthSearch')
                    ->andWhere(['IN', 'SOCIAL_SALARY_EMP_ID',$SOCIAL_SALARY_EMP_ID])
                    ->addParams([':resultMonthSearch' => $resultMonthSearch,])
                    ->asArray()
                    ->all();
            return $model;

        }
        

       
       public static function ListYearSSOIDCrad($idcard)
    {
        $model  = Socialsalary::find()
                            ->select(['SOCIAL_SALARY_DATE'])
                            ->where('SOCIAL_SALARY_EMP_ID = :SOCIAL_SALARY_EMP_ID')
                            ->addParams([':SOCIAL_SALARY_EMP_ID' => $idcard])
                            ->asArray()
                            ->all();

        return $model;
    }

    public static function ListDataSSO($id_card,$year)
    {
        $year = '%'.$year;
        $model  = Socialsalary::find()
                            ->where('SOCIAL_SALARY_EMP_ID = :SOCIAL_SALARY_EMP_ID
                                 AND SOCIAL_SALARY_DATE  like :year')
                            ->addParams([':SOCIAL_SALARY_EMP_ID' => $id_card,
                                         ':year' => $year,
                            ])
                            ->asArray()
                            ->all();
        //print_r($model);
        return $model;
    }

    public static function SumListDataSSOThisYear($id_card,$year)
    {
        $year = '%'.$year;
        $model  = Socialsalary::find()
                            ->select(['SOCIAL_SALARY_EMP_ID,SUM(SOCIAL_SALARY_AMOUNT) as SUMSOCIAL_SALARY_ID'])
                            ->where('SOCIAL_SALARY_EMP_ID = :SOCIAL_SALARY_EMP_ID
                                 AND SOCIAL_SALARY_DATE  like :year')
                            ->addParams([':SOCIAL_SALARY_EMP_ID' => $id_card,
                                         ':year' => $year,
                            ])
                            ->asArray()
                            ->all();

        // $model =Socialsalary::find()
        //                     ->select('SOCIAL_SALARY_EMP_ID,SOCIAL_SALARY_AMOUNT')
        //                     ->where('SOCIAL_SALARY_ID = :SOCIAL_SALARY_ID')
        //                     ->addParams([':SOCIAL_SALARY_ID' => $modelMax['0']['maxidbenefit'],])
        //                     ->asArray()
        //                     ->all();
        //print_r($model);
        return $model;
    }

    public static function SumListDataSSOThisAll($id_card)
    {
        $model  = Socialsalary::find()
                            ->select(['SOCIAL_SALARY_EMP_ID,SUM(SOCIAL_SALARY_AMOUNT) as SUMALLSOCIAL_SALARY_ID'])
                            ->where('SOCIAL_SALARY_EMP_ID = :SOCIAL_SALARY_EMP_ID')
                            ->addParams([':SOCIAL_SALARY_EMP_ID' => $id_card,])
                            ->asArray()
                            ->all();

        // $model =Socialsalary::find()
        //                     ->select('SOCIAL_SALARY_EMP_ID,BENEFIT_TOTAL_AMOUNT')
        //                     ->where('SOCIAL_SALARY_ID = :SOCIAL_SALARY_ID')
        //                     ->addParams([':SOCIAL_SALARY_ID' => $modelMax['0']['maxidbenefit'],])
        //                     ->asArray()
        //                     ->all();
        //print_r($model);
        return $model;
    }

    public static function getdatassoidcard($id_card)
    {
        $model = Empdata::find()
                        ->select('socialBenefitStatus,socialBenifitPercent')
                        ->where('ID_Card = :id_card')
                        ->addParams([':id_card' => $id_card,])
                        ->asArray()
                        ->one();
        // print_r($model);
        // exit;
        return $model;
    }
}

?>

