<?php
/**
 * Created by PhpStorm.
 * User: adithep
 * Date: 7/21/2016 AD
 * Time: 10:56 AM
 */


namespace app\modules\wssale\controllers;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;


class CustomerController extends ActiveController
{

    // adjust the model class to match your model
    public $modelClass = 'app\modules\wssale\models\Book';
    public function behaviors()
    {
        return
            ArrayHelper::merge([
                [
                    'class' => Cors::className(),
                    'cors' => [
                        'Origin' => ['*'],
                        'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    ],
                ],
            ],
            parent::behaviors());
    }


    public function actionSearchcustomer()
    {
        return [
            'M'=>1,
            'N'=>2,
            'O'=>3,
            'P'=>4
        ];
    }



    public function actionCustomerID()
    {
        return [
            'A'=>1,
            'B'=>2,
            'C'=>3,
            'D'=>4
        ];
    }
}



