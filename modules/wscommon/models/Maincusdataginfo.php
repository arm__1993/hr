<?php

namespace app\modules\wscommon\models;

use Yii;

/**
 * This is the model class for table "MAIN_CUS_GINFO".
 *
 * @property integer $CusNo
 * @property string $Picture
 * @property string $Be
 * @property string $BeType
 * @property string $Cus_Name
 * @property string $Cus_Surename
 * @property string $Cus_Nickname
 * @property string $Cus_Big_Picture
 * @property string $Cus_Small_Picture
 * @property string $Sex
 * @property string $DateOfBirth
 * @property string $Cus_IDNo_Type
 * @property string $Cus_IDNo
 * @property string $Married_Status
 * @property string $Time_for_talk
 * @property string $Date_Receive
 * @property string $Data_Received
 * @property string $Data_Received_Num
 * @property string $1stSourceData
 * @property string $Remark
 * @property string $Updater
 * @property string $UpdaterNO
 * @property string $UpdateT
 * @property string $company_type
 * @property string $cus_branch_id
 * @property string $cus_branch_txt
 */
class Maincusdataginfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MAIN_CUS_GINFO';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbERP_new_cusdata');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BeType', 'Cus_Name', 'Cus_Surename', 'Cus_Nickname', 'Sex', 'Cus_IDNo_Type', 'Time_for_talk', 'Date_Receive', 'Data_Received', 'Data_Received_Num', 'Remark', 'Updater', 'UpdaterNO', 'company_type', 'cus_branch_id', 'cus_branch_txt'], 'required'],
            [['Date_Receive', 'UpdateT'], 'safe'],
            [['Remark'], 'string'],
            [['Picture', 'Cus_Big_Picture', 'Cus_Small_Picture', 'Data_Received', '1stSourceData', 'Updater'], 'string', 'max' => 255],
            [['Be', 'Cus_Name', 'Cus_Surename', 'Cus_Nickname'], 'string', 'max' => 100],
            [['BeType'], 'string', 'max' => 5],
            [['Sex', 'Cus_IDNo_Type', 'Cus_IDNo'], 'string', 'max' => 20],
            [['DateOfBirth', 'Married_Status'], 'string', 'max' => 10],
            [['Time_for_talk', 'cus_branch_id', 'cus_branch_txt'], 'string', 'max' => 250],
            [['Data_Received_Num', 'UpdaterNO'], 'string', 'max' => 15],
            [['company_type'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CusNo' => 'รหัสลูกค้า auto run',
            'Picture' => 'รูปภาพลูกค้า ',
            'Be' => 'คำนำหน้า ',
            'BeType' => '1= บุคคล , 2 = ชมรม / สมาคม / นิติบุคล ',
            'Cus_Name' => 'ชื่อ ',
            'Cus_Surename' => 'นามสกุล ',
            'Cus_Nickname' => 'ชื่อเล่น ',
            'Cus_Big_Picture' => 'Cus  Big  Picture',
            'Cus_Small_Picture' => 'Cus  Small  Picture',
            'Sex' => 'เพศ ',
            'DateOfBirth' => 'วันเกิด ',
            'Cus_IDNo_Type' => 'เลขบัตรประชาชน , พาสปอรต์',
            'Cus_IDNo' => 'เลขบัตรประชาชน , พาสปอรต์',
            'Married_Status' => 'สถานะ , โสด,หย่าร้าง, สมรส',
            'Time_for_talk' => 'เวลาที่ลูกค้าสะดวกคุย',
            'Date_Receive' => 'วันที่รับข้อมูล ',
            'Data_Received' => 'ผู้รับข้อมูล ',
            'Data_Received_Num' => 'ผู้รับข้อมูล id card',
            '1stSourceData' => 'แยกว่ามาจาก BU ไหน แหล่งข้อมูลครั้งแรก ',
            'Remark' => 'หมายเหตุ ',
            'Updater' => 'ชื่อผู้ป้อนข้อมูล ',
            'UpdaterNO' => 'id card ผู้ป้อนข้อมูล ',
            'UpdateT' => 'Update T',
            'company_type' => '1=สำนักงานใหญ่,2=สาขา',
            'cus_branch_id' => 'เลขสาขาลูกค้า',
            'cus_branch_txt' => 'ชื่อสาขาลูกค้า',
        ];
    }
    public function insertmaincusdataginfo($Be,$BeType,$Cus_Name,$Cus_Surename,$Cus_Nickname,$Sex,$DateOfBirth,$Cus_IDNo_Type,$Cus_IDNo,$Married_Status,$Date_Receive,$Data_Received,$Data_Received_Num,$stSourceData,$Remark,$Updater,$UpdaterNO,$UpdateT,$company_type,$cus_branch_id,$cus_branch_txt)
    {
 
        $sql = "insert into MAIN_CUS_GINFO (`CusNo`,
                                            `Picture`,
                                            `Be`,
                                            `BeType`,
                                            `Cus_Name`,
                                            `Cus_Surename`,
                                            `Cus_Nickname`,
                                            `Cus_Big_Picture`,
                                            `Cus_Small_Picture`,
                                            `Sex`,
                                            `DateOfBirth`,
                                            `Cus_IDNo_Type`,
                                            `Cus_IDNo`,
                                            `Married_Status`,
                                            `Time_for_talk`,
                                            `Date_Receive`,
                                            `Data_Received`,
                                            `Data_Received_Num`,
                                            `1stSourceData`,
                                            `Remark`,
                                            `Updater`,
                                            `UpdaterNO`,
                                            `UpdateT`,
                                            `company_type`,
                                            `cus_branch_id`,
                                            `cus_branch_txt`)
                                    VALUES (NULL,
                                            '',
                                            :Be,
                                            :BeType,
                                            :Cus_Name,
                                            :Cus_Surename,
                                            :Cus_Nickname,
                                            '',
                                            '',
                                            :Sex,
                                            :DateOfBirth,
                                            :Cus_IDNo_Type,
                                            :Cus_IDNo,
                                            :Married_Status,
                                            '',
                                            :Date_Receive,
                                            :Data_Received,
                                            :Data_Received_Num,
                                            :stSourceData,
                                            :Remark,
                                            :Updater,
                                            :UpdaterNO,
                                            :UpdateT,
                                            :company_type,
                                            :cus_branch_id,
                                            :cus_branch_txt) ";
        Yii::$app->dbERP_new_cusdata
       ->createCommand($sql)
       ->bindParam(':Be',$Be)
       ->bindParam(':BeType',$BeType)
       ->bindParam(':Cus_Name',$Cus_Name)
       ->bindParam(':Cus_Surename',$Cus_Surename)
       ->bindParam(':Cus_Nickname',$Cus_Nickname)
       ->bindParam(':Sex',$Sex)
       ->bindParam(':DateOfBirth',$DateOfBirth)
       ->bindParam(':Cus_IDNo_Type',$Cus_IDNo_Type)
       ->bindParam(':Cus_IDNo',$Cus_IDNo)
       ->bindParam(':Married_Status',$Married_Status)
        
       ->bindParam(':Date_Receive',$Date_Receive)
       ->bindParam(':Data_Received',$Data_Received)
       ->bindParam(':Data_Received_Num',$Data_Received_Num)
       ->bindParam(':stSourceData',$stSourceData)
       ->bindParam(':Remark',$Remark)
       ->bindParam(':Updater',$Updater)
       ->bindParam(':UpdaterNO',$UpdaterNO)
       ->bindParam(':UpdateT',$UpdateT)
       ->bindParam(':company_type',$company_type)
       ->bindParam(':cus_branch_id',$cus_branch_id)
       ->bindParam(':cus_branch_txt',$cus_branch_txt)
       ->execute();
        $LastId = Yii::$app->dbERP_new_cusdata->getLastInsertID();
        return $LastId;
        
    }

    public function searchcustomerdataforsalemobile($Cus_Name,$Cus_Surename,$Cus_IDNo) // param
    {   

       // $sql = "select * from MAIN_CUS_GINFO where Cus_Name LIKE '%$Cus_Name%'
       //  AND Cus_Surename LIKE '%$Cus_Surename%' AND Cus_IDNo LIKE '%$Cus_IDNo%'
       //  "; 
        $Cus_Name = "%".$Cus_Name."%";
        $Cus_Surename = "%".$Cus_Surename."%";
        $Cus_IDNo = "%".$Cus_IDNo."%";
        $datacustomer = Yii::$app->dbERP_new_cusdata
       ->createCommand("select MAIN_CUS_GINFO.CusNo,MAIN_CUS_GINFO.Cus_Name,MAIN_CUS_GINFO.Cus_Surename,MAIN_CUS_GINFO.Cus_IDNo ,
            (SELECT  SUBSTR(MAIN_ADDRESS.ADDR_VILLAGE,INSTR(MAIN_ADDRESS.ADDR_VILLAGE,'_')+1 ) 
            FROM MAIN_ADDRESS INNER JOIN MAIN_CUS_GINFO ON MAIN_ADDRESS.ADDR_CUS_NO = MAIN_CUS_GINFO.CusNo
        WHERE MAIN_ADDRESS.ADDR_MAIN_ACTIVE = 1) AS ADDR_VILLAGE,
        (SELECT  SUBSTR(MAIN_ADDRESS.ADDR_NUMBER,INSTR(MAIN_ADDRESS.ADDR_NUMBER,'_')+1 ) 
            FROM MAIN_ADDRESS INNER JOIN MAIN_CUS_GINFO ON MAIN_ADDRESS.ADDR_CUS_NO = MAIN_CUS_GINFO.CusNo
        WHERE MAIN_ADDRESS.ADDR_MAIN_ACTIVE = 1) AS ADDR_NUMBER,
        (SELECT  SUBSTR(MAIN_ADDRESS.ADDR_SUB_DISTRICT,INSTR(MAIN_ADDRESS.ADDR_SUB_DISTRICT,'_')+1 ) 
            FROM MAIN_ADDRESS INNER JOIN MAIN_CUS_GINFO ON MAIN_ADDRESS.ADDR_CUS_NO = MAIN_CUS_GINFO.CusNo
        WHERE MAIN_ADDRESS.ADDR_MAIN_ACTIVE = 1) AS ADDR_SUB_DISTRICT,
        (SELECT  SUBSTR(MAIN_ADDRESS.ADDR_DISTRICT,INSTR(MAIN_ADDRESS.ADDR_DISTRICT,'_')+1 )  
            FROM MAIN_ADDRESS INNER JOIN MAIN_CUS_GINFO ON MAIN_ADDRESS.ADDR_CUS_NO = MAIN_CUS_GINFO.CusNo
        WHERE MAIN_ADDRESS.ADDR_MAIN_ACTIVE = 1) AS ADDR_DISTRICT,
        (SELECT  SUBSTR(MAIN_ADDRESS.ADDR_PROVINCE,INSTR(MAIN_ADDRESS.ADDR_PROVINCE,'_')+1 )  
            FROM MAIN_ADDRESS INNER JOIN MAIN_CUS_GINFO ON MAIN_ADDRESS.ADDR_CUS_NO = MAIN_CUS_GINFO.CusNo
        WHERE MAIN_ADDRESS.ADDR_MAIN_ACTIVE = 1) AS ADDR_PROVINCE,
        (SELECT  MAIN_TELEPHONE.TEL_NUM
            FROM MAIN_TELEPHONE INNER JOIN MAIN_CUS_GINFO ON MAIN_TELEPHONE.TEL_CUS_NO = MAIN_CUS_GINFO.CusNo
        WHERE MAIN_TELEPHONE.TEL_MAIN_ACTIVE = 1) AS TEL_NUM 

        FROM MAIN_CUS_GINFO 
            where Cus_Name LIKE :Cus_Name
                AND Cus_Surename LIKE :Cus_Surename
                AND Cus_IDNo LIKE :Cus_IDNo")
       ->bindParam(':Cus_Name',$Cus_Name)
       ->bindParam(':Cus_Surename',$Cus_Surename)
       ->bindParam(':Cus_IDNo',$Cus_IDNo)
       ->queryAll();

      // echo $sql;
        return $datacustomer;

    }


    public function searchcustomerdataforshoweditmobile
($Cusno) // param
    {   
 
        $datacustomer = Yii::$app->dbERP_new_cusdata
       ->createCommand("select * from MAIN_CUS_GINFO where Cusno  = :Cusno")
       ->bindParam(':Cusno',$Cusno)
       ->queryAll();

      // echo $sql;
        return $datacustomer;

    }


}






























    //     public function insertmasterdatacus($Cus_Name,$Cus_Surename,$Cus_IDNo) // param
    // {   
    //     $Cus_Name = "%".$Cus_Name."%";
    //     $Cus_Surename = "%".$Cus_Surename."%";
    //     $Cus_IDNo = "%".$Cus_IDNo."%";
    //     $datacustomer = Yii::$app->dbERP_maincusdata
    //    ->createCommand("select * from MainCusData where Cus_Name LIKE :Cus_Name 
    //     AND Cus_Surename LIKE :Cus_Surename AND Cus_IDNo LIKE :Cus_IDNo
    //     ")
    //    ->bindParam(':Cus_Name',$Cus_Name)
    //    ->bindParam(':Cus_Surename',$Cus_Surename)
    //    ->bindParam(':Cus_IDNo',$Cus_IDNo)
    //    ->queryAll();
    //     return $datacustomer;

    // }

