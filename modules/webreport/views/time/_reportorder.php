<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

//use api report
use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiTime;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$resultDreparirFree = $session->get('resultDreparirFree');
$resultDreparirDistance = $session->get('resultDreparirDistance');
$resultDreparirGenaral = $session->get('resultDreparirGenaral');
$resultDreparirIntersect = $session->get('resultDreparirIntersect');
$select_company = $session->get('selected_company');
$select_year = $session->get('selected_startmonth');
$select_technician=$session->get('select_technician');

?>



<div class="box">
    <h3><i class="fa fa-fw fa-file-text"></i>รายงานจำนวนใบสั่งซ่อมแต่ละประเภทซ่อม</h3>

    <h3> <?php  $namecompany = ApiReport::getNameCompanyPDF($select_company) ;
        echo  $namecompany['0']['name']; ?>
    </h3>
    <h3>เลือกช่าง <?php
        if ($select_technician == '1') {
            echo  'ช่างรถเล็ก';
        } elseif ($select_technician == '2') {
            echo  'ช่างรถใหญ่';
        }

        ?></h3>


    <h4>
        งานเช็คระยะ
    </h4>
    <table id="example2" width="100%" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" bgcolor="#777" ALIGN="CENTER" >
        <thead>
        <tr role="row">
            <th  width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                ระยะเวลาในการซ่อม
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                ม.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                ก.พ.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                มี.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                เม.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                พ.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                มิ.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ก.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ส.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ก.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ต.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                พ.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ธ.ค.
            </th>
        </tr>
        </thead>
        <tbody>
        <tr role="row" class="even">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                1 วัน
            </th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                2 วัน
            </th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult2'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="even">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                3 วัน
            </th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult3'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                4-7 วัน
            </th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult47'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                8-14 วัน
            </th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult814'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                15-30 วัน
            </th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1530'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                30 วันขึ้นไป
            </th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResultmore30'];?></td>
            <?php } ?>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th>รวม</th>
            <?php foreach ($resultDreparirDistance AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMAllResult'];?></td>
            <?php } ?>
        </tr>
        </tfoot>
    </table>

    <br>
    <br>
    <br>
    <br>
    <h4>
        งานเช็คระยะ+งานซ่อมทั่วไป
    </h4>
    <table id="example2" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" width="100%" bgcolor="#777" ALIGN="CENTER">
        <thead>
        <tr role="row">
            <th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                ระยะเวลาในการซ่อม
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                ม.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                ก.พ.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                มี.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                เม.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                พ.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                มิ.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ก.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ส.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ก.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ต.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                พ.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ธ.ค.
            </th>
        </tr>
        </thead>

        <tbody>
        <tr role="row" class="even" >
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                1 วัน
            </th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                2 วัน
            </th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult2'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="even">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                3 วัน
            </th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult3'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                4-7 วัน
            </th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult47'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                8-14 วัน
            </th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult814'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                15-30 วัน
            </th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1530'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                30 วันขึ้นไป
            </th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResultmore30'];?></td>
            <?php } ?>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th>รวม</th>
            <?php foreach ($resultDreparirIntersect AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMAllResult'];?></td>
            <?php } ?>
        </tr>
        </tfoot>
    </table>

    <br>
    <br>
    <br>
    <br>
    <h4>
        งานซ่อมทั่วไป
    </h4>
    <table id="example2" width="100%" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" bgcolor="#777" ALIGN="CENTER"  >
        <thead>
        <tr role="row">
            <th  width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                ระยะเวลาในการซ่อม
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                ม.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                ก.พ.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                มี.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                เม.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                พ.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                มิ.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ก.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ส.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ก.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ต.ค.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                พ.ย.
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                ธ.ค.
            </th>
        </tr>
        </thead>
        <tbody>
        <tr role="row" class="even">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                1 วัน
            </th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                2 วัน
            </th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult2'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="even">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                3 วัน
            </th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult3'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                4-7 วัน
            </th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult47'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                8-14 วัน
            </th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult814'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                15-30 วัน
            </th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1530'];?></td>
            <?php } ?>
        </tr>
        <tr role="row" class="odd">
            <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                30 วันขึ้นไป
            </th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResultmore30'];?></td>
            <?php } ?>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th >รวม</th>
            <?php foreach ($resultDreparirGenaral AS $value) {?>
                <td bgcolor="#f0f8ff"><?php echo $value['SUMAllResult'];?></td>
            <?php } ?>
        </tr>
        </tfoot>
    </table>
    <br>
    <br>
    <br>
    <br>
    <h4>
        งานฟรี
    </h4>
    <div class="tab-pane" id="tab_4">
        <table id="example2" width="100%" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info" bgcolor="#777" ALIGN="CENTER"  >
            <thead>
            <tr role="row">
                <th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending">
                    ระยะเวลาในการซ่อม
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
                    ม.ค.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                    ก.พ.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                    มี.ค.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    เม.ย.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    พ.ค.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    มิ.ย.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    ก.ค.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    ส.ค.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    ก.ย.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    ต.ค.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    พ.ย.
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">
                    ธ.ค.
                </th>
            </tr>
            </thead>
            <tbody>
            <tr role="row" class="even">
                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                    1 วัน
                </th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1'];?></td>
                <?php } ?>
            </tr>
            <tr role="row" class="odd">
                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                    2 วัน
                </th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult2'];?></td>
                <?php } ?>
            </tr>
            <tr role="row" class="even">
                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                    3 วัน
                </th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult3'];?></td>
                <?php } ?>
            </tr>
            <tr role="row" class="odd">
                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                    4-7 วัน
                </th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult47'];?></td>
                <?php } ?>
            </tr>
            <tr role="row" class="odd">
                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                    8-14 วัน
                </th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult814'];?></td>
                <?php } ?>
            </tr>
            <tr role="row" class="odd">
                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                    15-30 วัน
                </th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResult1530'];?></td>
                <?php } ?>
            </tr>
            <tr role="row" class="odd">
                <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                    30 วันขึ้นไป
                </th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMDiffDateResultmore30'];?></td>
                <?php } ?>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <th>รวม</th>
                <?php foreach ($resultDreparirFree AS $value) {?>
                    <td bgcolor="#f0f8ff"><?php echo $value['SUMAllResult'];?></td>
                <?php } ?>
            </tr>
            </tfoot>
        </table>
</div>
<?php
session_start();
session_destroy();
?>