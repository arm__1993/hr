<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use app\api\DateTime;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use app\modules\webreport\apiwebreport\ApiReport;
AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$NameAndTime = $session->get('NameAndTime');
$checkStatus = $session->get('checkStatus');
$dateNOW = $session->get('dateNOW');
$selected_company = $session->get('selected_company');
$selected_technician = $session->get('selected_technician');
?>

<h3><i class="fa fa-fw fa-file-text"></i>รายงานรถค้างซ่อมสะสมประจำวัน</h3>
<h5><?php  $namecompany = ApiReport::getNameCompanyPDF($selected_company) ;
        echo $namecompany['0']['name']; ?></h5>
<h5 class="box-title">รายงานรถค้างซ่อมสะสมประจำวันที่ &nbsp; <?php echo date('d/m/Y');?></h5>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- AREA TABLE -->
            <div class="box box-success">
                 <div class="box-header">
                    <!--<div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>-->
                </div>
                <!-- /.box-header -->                
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row" class="odd"> 
                                        <th bgcolor="#f0f8ff"class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ลำดับที่
                                        </th>                 
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        เลขที่ใบซ่อม
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันที่เข้ารับบริการ
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        รุ่นรถ
                                        </th>
                                        <th  bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ทะเบียน
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันที่ปิดงานโดย SA
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ประเภทงานซ่อม
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        ช่างซ่อม
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        SAรับแจ้งซ่อม
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        วันนัดส่งมอบรถ
                                        </th>
                                        <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        สถานะที่ค้าง
                                        </th>
                                         <th bgcolor="#f0f8ff" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                        จำนวนวันค้างซ่อมสะสม/วัน
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody> 
                                    <?php
                                    $item = 1;
                                    foreach ($NameAndTime as $value) {
                                    ?>
                                    <tr role="row" class="odd">
                                        <td><?php print_r($item); ?></td>  
                                        <td>
                                        <?php echo ($value['id_repair']) ? $value['id_repair'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['guard_time']) ? DateTime::CalendarDate($value['guard_time']) : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['fullmodel_name']) ? $value['fullmodel_name'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['register']) ? $value['register'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['end_time']) ? DateTime::CalendarDate($value['end_time']) : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['technician_name']) ? $value['technician_name'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['name_sa']) ? $value['name_sa'] : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($value['app_time']) ? DateTime::CalendarDate(date("Y-m-d",$value['app_time'])) : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo ($checkStatus) ? $checkStatus : '-'; ?>
                                        </td>
                                        <td>
                                        <?php echo (int)(($dateNOW - $value['app_time'])/86400); ?>
                                        </td>
                                    </tr>                                    
                                    <?php $item++;} ?>                              
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--example2_wrapper-->
                </div><!--box-body-->
            </div><!--box box-success-->
            <!-- AREA TABLE -->
            </div>
        </div>
    </section>
<?php
session_start();
session_destroy();
?>