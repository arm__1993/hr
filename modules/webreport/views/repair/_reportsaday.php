<?php
/*05-10-2559 th mpdf*/
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\bundle\AppAsset;
use miloschuman\highcharts\Highcharts;
use yii\web\Session;
use app\api\Helper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use app\modules\webreport\apiwebreport\ApiReport;
use app\modules\webreport\apiwebreport\ApiRepair;

AppAsset::register($this);

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/highchart.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //java
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/bootbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/global/hornbill.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
$(document).ready(function() {
    $('#linklogout, #alogout').on("click",function(){
       bootbox.confirm({
            size: "small",
            message:"<h4 class=\"btalert\">คุณยืนยันจะออกจากระบบใช่หรือไม่?</h4>",
            callback: function(result){
                if(result==1) {
                    window.location.href='index.php?r=login/logout';
                }
            }
          });
    });
$('#example-getting-started').multiselect();
});
JS;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/webreport/bootstrap-multiselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->request->BaseUrl . "/css/webreport/bootstrap-multiselect.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
$this->registerJs($script);
$img = Yii::$app->request->BaseUrl . '/images/global';
$crlName = Yii::$app->controller->id;

?>

<?php

$session = Yii::$app->session;

$fullname = $session->get('fullname');
$sum = $session->get('sum');
$time = $session->get('time');
$avgdate = $session->get('avgdate');
$avgtime = $session->get('avgtime');
$selected_company = $session->get('selected_company');
$selected_months = $session->get('selected_months');


?>
<div class="box">
    <h3><i class="fa fa-fw fa-file-text"></i>รายงานยอดรับแจ้งซ่อม SA รายคน(รายวัน)</h3>
    <h3> <?php  $namecompany = ApiReport::getNameCompanyPDF($selected_company) ;
        echo  $namecompany['0']['name']; ?>
    </h3>
    <h3>ช่วงวันที่ <?php
        print_r($selected_months);
        ?></h3>
</div>


<table width="100%" bgcolor="#777" ALIGN="CENTER" id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
    <thead>
    <tr role="row">
        <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            ชื่อ SA
        </th>
        <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            จำนวนคันรับแจ้งซ่อม
        </th>
        <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            เฉลี่ยต่อวัน
        </th>
        <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            รวมเวลารับแจ้งซ่อม (นาที)
        </th>
        <th bgcolor="#eee" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            เวลาเฉลี่ยต่อคัน (นาที)
        </th>
    </tr>
    </thead>
    <?php if(!empty($fullname)) { ?>
        <tbody>
        <?php for($i=0;$i<count($fullname);$i++){ ?>
            <tr role="row">
                <td bgcolor="#f0f8ff"><?php echo($fullname[$i]) ? $fullname[$i] : 'ไม่มีข้อมูล';?></td>
                <td bgcolor="#f0f8ff"><?php echo($sum[$i]) ? $sum[$i] : 0; ?></td>
                <td bgcolor="#f0f8ff">
                    <?php echo ($avgdate[$i]) ? Helper::displayDecimal($avgdate[$i]) : 0; ?>
                </td>
                <td bgcolor="#f0f8ff"><?php echo($time[$i]) ? $time[$i] : 0; ?></td>
                <td bgcolor="#f0f8ff">
                    <?php echo ($avgtime[$i]) ? Helper::displayDecimal($avgtime[$i]) : 0; ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
        <?php
        $sumsum = array_sum($sum);
        $sumtime = array_sum($time);
        $sumavgtime = array_sum($avgtime);
        ?>
        <tfoot>
        <tr>
            <th>รวม</th>
            <td><?php echo($sumsum) ? $sumsum : 0;?></td>
            <td>-</td>
            <td><?php echo($sumtime) ? $sumtime : 0;?></td>
            <td>
                <?php echo($sumavgtime) ? Helper::displayDecimal($sumavgtime) : 0;?>
            </td>
        </tr>
        </tfoot>
    <?php } ?>
</table>
<?php
session_start();
session_destroy();
?>